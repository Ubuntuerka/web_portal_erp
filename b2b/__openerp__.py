# -*- coding: utf-8 -*-
##############################################################################
#
#    
#
#    
#
##############################################################################

{
    "name": "B2B module",
    "version": "1.0",
    "author": "Uuganbayar",
    "category": "document",
    "description": """This module document management.""",
    "depends" : [
                 'hr_contract',
                 'mn_web_portal',
                 ],
    'update_xml': [
        "views/b2b_view.xml",
        #"views/b2c.xml",
        "views/server.xml",
        "views/create_service.xml",
        #"views/update_service.xml",
        "views/menu_view.xml",
                   ],
    'test': [],
    'installable': True,
    'auto_install': False, 
}



