# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree


class B2C(models.Model):
    _name = 'b2c'

    purchase_id = fields.Char(u'Aпп худалдан авалтын id')
    user_id = fields.Integer(u'Апп хэрэглэгчийн id')
    sales_no = fields.Char(u'Талоны №')
    paid_date = fields.Datetime(u'Огноо')
    ttl_amt = fields.Float(u'Нийт дүн')
    order_type = fields.Selection([
        ('delivery', u'Хүргэлт'),
        ('pickup', u'Салбар'),
    ], string=u'Төрөл')
    used_user_point = fields.Integer(u'Оноо')
    used_promotion_id = fields.Integer(u'Купон, ваучерын id')
    used_promotion_amt = fields.Float(u'Купон, ваучерын дүн')
    ttl_discount_amt = fields.Float(u'Нийт хөнгөлөлтийн дүн')
    ttl_delivery_amt = fields.Float(u'Хүргэлтийн дүн')
    vat_amt = fields.Float(u'И-баримтийн дүн')
    user_vat_no = fields.Char(u'Хэрэглэгчийн дугаар')
    paid_amt = fields.Float(u'Төлсөн дүн')
    branch_code = fields.Char(u'Салбарын код')
    start_date = fields.Datetime(u'Огноо')
    order_created_date = fields.Datetime(u'Захиалга хйисэн огноо')
    o_pro_id = fields.One2many('ordered.product','b2c_id',string=u'Бүтээгдэхүүн')
    
    
'''class B2C1(models.Model):
    _name = 'b2c1'

    purchase_id = fields.Integer('Апп худалдан авалтын id')
    user_id = fields.Integer('Апп хэрэглэгчийн id')
    ttl_amt = fields.Integer('Нийт дүн')
    order_type = fields.Datetime('Төрөл')
    user_vat_no = fields.Char('Хэрэглэгчийн дугаар')
    
    ordered_products = fields.Char('Захиалсан бүтээгдэхүүн')
    product_code = fields.Char('Barcode')
    price = fields.Char('Бүтээгдэхүүний үнэ')
    quantity = fields.Integer('Бүтээгдэхүүний тоо')
    branch_code = fields.Char('Салбарын код')
    start_date = fields.Datetime('Огноо')
    order_created_date = fields.Datetime('Захиалга хйисэн огноо')
    o_pros_id = fields.One2many('ordered.products','b2c1_id',string=u'Бүтээгдэхүүн')'''
    
class OrderedProductLine(models.Model):
    _name = 'ordered.product'
    
    product_code = fields.Char(u'Barcode')
    price = fields.Integer(u'Бүтээгдэхүүний үнэ')
    quantity = fields.Float(u'Бүтээгдэхүүний тоо')
    b2c_id = fields.Many2one(u'b2c','Order id')
    
    
'''class OrderedProductsLine(models.Model):
    _name = 'ordered.products'
    
    product_code = fields.Char('Barcode')
    price = fields.Integer('Бүтээгдэхүүний үнэ')
    quantity = fields.Integer('Бүтээгдэхүүний тоо')
    b2c1_id = fields.Many2one('b2c1','Orders id')'''
    
class Service(models.Model):
    _name = 'service'

    service_code = fields.Integer(u'Сервис үйлчилгээний код')
    branch_code = fields.Integer(u'Салбарын код')
    start_date = fields.Datetime(u'Огноо')
    
    
class CreateService(models.Model):
    _name = 'create.service'

    order_id = fields.Char(u'Апп сервис захиалгын id')
    service_code = fields.Char(u'Сервис үйлчилгээний код')
    shts_code = fields.Char(u'Салбарын код')
    schedule_id = fields.Many2one('oil.service.schedule',string='Хуваарь')
    start_date = fields.Datetime(u'Үйлчилгээ авах огноо')
    end_date = fields.Datetime(u'Үйлчилгээ дуусах огноо')
    ttl_amt = fields.Integer(u'Нийт дүн')
    used_user_point = fields.Integer(u'Апп хэрэглэгчийн хэрэглэсэн онооны дүн')
    used_promotion_id = fields.Integer(u'Хэрэглэсэн купон, ваучерын id')
    used_promotion_amt = fields.Integer(u'Хэрэглэсэн купон, ваучерын дүн')
    pay_amt = fields.Integer(u'Төлөх дүн')
    order_created_date = fields.Datetime(u'Захиалга хйисэн огноо')
    status = fields.Selection([
        ('open', u'Ноорог'),
        ('send', u'Илгээх'),
        ('done', u'Зөвшөөрсөн'),
        ('cancel', u'Цуцлах'),
    ], string=u'Статус')
    reason = fields.Char('Шалтгаан')
    car_ids = fields.One2many('customer.car','service_id',string=u'Автомашины мэдээлэл')
    customer_id = fields.Char(u'Хэрэглэгчийн ID')
    customer_name = fields.Char(u'Хэрэглэгчийн нэр')
    
    
    @api.model
    def create(self, vals):
        schedule_obj = self.env['oil.service.schedule']
        rslt = super(CreateService, self).create(vals)
        if 'schedule_id' in vals:
            schedule_id = schedule_obj.browse(vals['schedule_id'])
            schedule_id.state = 'reserved'
        return rslt
        
    def write(self, vals):
        rslt = super(CreateService, self).write(vals)
        if 'status' in vals:
            if vals['status'] =='cancel':
                self.schedule_id.state='free'
        return rslt
        
    
'''class UpdateService(models.Model):
    _name = 'update.service'

    order_id = fields.Integer('Апп сервис захиалгын id')
    service_code = fields.Integer('Сервис үйлчилгээний код')
    branch_code = fields.Char('Салбарын код')
    status = fields.Datetime('Статус')
    reason = fields.Char('Шалтгаан')'''
    
class CustomerCar(models.Model):
    _name = 'customer.car'

    name = fields.Char(u'Улсын дугаар')
    manufactured_year = fields.Char(u'Үйлдвэрлэсэн он')
    mark = fields.Char(u'Марк')
    manufacturer = fields.Char(u'Үйлдвэрлэгч')
    engine_type = fields.Char(u'Моторын төрөл')
    service_id = fields.Many2one('create.service',string=u'Сервис мэдээлэл')
    
   