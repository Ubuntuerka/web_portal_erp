# -*- coding: utf-8 -*-
##############################################################################
# Shunkhlai LLC
#
#
#2023 Shunkhlai 30 years
#
##############################################################################

{
    "name": "Player statement",
    "version": "1.0",
    "author": "Uuganbayar.E",
    "category": "Register",
    "description": """Баг тамирчдын мэдүүлэг бүртгэл
                    Шунхлай ХХК-ын 30 жилийн үйл ажиллагаанд зориулж хөгжүүлэв
                    2023 он""",
    "depends" : [
                 ],
    'update_xml': [
                   "player_statement_view.xml",
                   'ps_sequence.xml',
                   'security/ps_security.xml',
                   'security/ir.model.access.csv',
                   ],
    'test': [],
    'installable': True,
    'auto_install': False, 
}