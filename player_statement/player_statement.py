# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import timedelta
from functools import partial
from datetime import datetime, time
import psycopg2
import pytz
from datetime import date, timedelta
from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError, ValidationError
from odoo.http import request
import odoo.addons.decimal_precision as dp

class sport_type(models.Model):
    _name = 'sport_type'
    _description = "Sports type"
    
    name = fields.Char(u'Спортын төрөл', required = True)
   
class ps_organization(models.Model):
    _name = 'ps_organization'
    _description = "Organization"
    
    name = fields.Char(u'Байгууллагын нэр', required = True)

class player_statement(models.Model):
    
    _name = 'player_statement'
    _description = "Player statement"
    
    name = fields.Char(u'Бүртгэлийн дугаар',copy=False, readonly=True, default=lambda self: self.env['ir.sequence'].next_by_code('player_statement'))
    date = fields.Date(u'Огноо', default=fields.Date.context_today, required = True)
    org_id = fields.Many2one('ps_organization', u'Байгууллагын нэр', required = True)
    sport_type_id = fields.Many2one('sport_type',u'Спортын төрөл', required = True)
    sex = fields.Selection([('male',u'Эрэгтэй'),
                              ('women',u'Эмэгтэй'),],string='Хүйс',default='male', required = True)
    worker = fields.Char(u'Хариуцсан ажилтан', required = True)
    mobile = fields.Char(u'Утас', required = True)
    line_ids = fields.One2many('player_statement_line','player_statement_id',string='Тамирчин')
    


class player_statement_line(models.Model):
    
    _name = 'player_statement_line'
    _description = "Player statement line"
    
    
    name = fields.Char(u'Овог нэр', required = True)
    player_statement_id = fields.Many2one('player_statement',string=u'Бүртгэлийн дугаар')
    job_name = fields.Char(u'Албан тушаал', required = True)
    photo = fields.Binary(u'Зураг', required = True)
    register_number = fields.Char(u'Регистрийн дугаар', required = True)
    ndd = fields.Char(u'НДД', required = True)
    ndd_year = fields.Char(u'НД төлсөн жил', required = True)
    zereg = fields.Char(u'Спортын зэрэг, цол', required = True)
    role = fields.Char(u'Тоглолтын үүрэг', required = True)
    #health = fields.Char(u'Эрүүл мэнд', required = True)
    date = fields.Date(u'Огноо', related='player_statement_id.date', store=True, readonly=False, tracking=True)
    org_id = fields.Many2one('ps_organization', u'Байгууллагын нэр', related='player_statement_id.org_id', store=True, readonly=False, tracking=True)
    sport_type_id = fields.Many2one('sport_type',u'Спортын төрөл', related='player_statement_id.sport_type_id', store=True, readonly=False, tracking=True)
    sex = fields.Selection([('male',u'Эрэгтэй'),
                              ('women',u'Эмэгтэй'),],string='Хүйс',default='male', related='player_statement_id.sex', store=True, readonly=False, tracking=True)
    worker = fields.Char(u'Хариуцсан ажилтан', related='player_statement_id.worker', store=True, readonly=False, tracking=True)
    mobile = fields.Char(u'Утас', related='player_statement_id.mobile', store=True, readonly=False, tracking=True)
    is_done = fields.Boolean(u'Бүрэн эсэх')
    note = fields.Char(u'Тайлбар')
        


    
    
    

