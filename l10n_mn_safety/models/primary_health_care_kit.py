# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import date

class PrimaryHealthRegister(models.Model):
    _name = 'primary.health.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('primary.health.register'),required=True)
    name = fields.Char(string='№')
    gtba_shts_name = fields.Many2one('shts.register',string='GTBA SHTS Name')
    note = fields.Char(string='Note')
    line_ids = fields.One2many('primary.health.register.line','primary_id',string='Line')
    
class PrimaryHealthRegisterLine(models.Model):
    _name = 'primary.health.register.line'
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('primary.health.register'),required=True)
    
    primary_health_care_kit_amount = fields.Char(string='Primary health care kit amount')
    product_id = fields.Many2one('product.product',string='Product')
    is_save_date = fields.Boolean(string='Is save date')
    primary_id = fields.Many2one('primary.health.register',string='Primary health register')