# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import date

class SafetyTrainingRegister(models.Model):
    _name = 'safety.training.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('safety.training.register'),required=True)
    name = fields.Char(string='№')
    number = fields.Char(string='Number')
    document_name = fields.Char(string='Document name')
    department_id = fields.Many2one('hr.department',string='Department')
    owner_id = fields.Many2one('hr.employee',string='Owner')
    location = fields.Many2many('shts.register','location_shts_rel','shts_id','location',string='Location')
    workable_field = fields.Many2many('shts.register','workable_field_shts_rel','shts_id','workable_field',string='Workable field')
    variant =fields.Char(string='Variant')
    confirm_date =fields.Date(string='Confirm date')
    start_date =fields.Date(string='Start date')
    is_new =fields.Boolean(string='Is new')