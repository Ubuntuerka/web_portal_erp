# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import date

class DocumentSet(models.Model):
    _name = 'document.set.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('document.set.register'),required=True)
    place_unit_name = fields.Many2one('shts.register',string='Place unit name')
    line_ids = fields.One2many('document.set.register.line','parent_id',string='Line')

    
class DocumentSetRegisterLine(models.Model):
    _name = 'document.set.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('document.set.register.line'),required=True)
    name = fields.Char(string='Name')
    contract_partner_id = fields.Many2one('res.partner',string='Contract partner')
    start_date = fields.Date(string='Start date')
    end_date = fields.Date(string='End date')
    do_partner_id = fields.Many2one('res.partner',string='Do partner')
    desc = fields.Text(string='Description')
    parent_id = fields.Many2one('document.set.register',string='Document set register')
    