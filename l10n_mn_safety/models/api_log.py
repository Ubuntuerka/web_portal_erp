# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree

DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


class ApiLog(models.Model):
    _name = 'api.log'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'date desc'

    date = fields.Datetime(string='Date', required=True, readonly=True, default=lambda self: fields.datetime.now())
    user_id = fields.Many2one('res.users', string='User', readonly=True,
                                  default=lambda self: self.env.user,
                                  required=True)
    model = fields.Char(string='model', readonly=True)
    note = fields.Text(string='Note', readonly=True)
