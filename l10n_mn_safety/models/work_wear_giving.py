# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import date

class WorkWearGiving(models.Model):
    _name = 'work.wear.giving.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('work.wear.giving.register'),required=True)
    name = fields.Char(string='№')
    category = fields.Char(string='Category')
    company_id = fields.Many2one('hr.department',string='Company')
    gtba_shts_name = fields.Many2one('shts.register',string='GTBA SHTS name')
    job = fields.Many2one('hr.job',string='Job')
    last_name = fields.Many2one('hr.employee',string='Last name')
    wearsize = fields.Char(string='Wear size')
    