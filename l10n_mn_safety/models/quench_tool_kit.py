# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import date

class QuenchToolRegister(models.Model):
    _name = 'quench.tool.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('quench.tool.register'),required=True)
    name = fields.Char(string='№')
    place_unit_name = fields.Many2one('shts.register',string='Place unit name')
    line_ids = fields.One2many('quench.tool.register.line','parent_id',string='Line')
    
    
class QuenchToolRegisterLine(models.Model):
    _name = 'quench.tool.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('quench.tool.register.line'),required=True)
    product_id = fields.Many2one('product.product',string='Product')
    qty = fields.Float(string='Quantity')
    parent_id = fields.Many2one('quench.tool.register', string='Quench tool register')
    