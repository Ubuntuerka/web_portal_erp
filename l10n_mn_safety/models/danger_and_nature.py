# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import date

class DangerAndNatureRegister(models.Model):
    _name = 'danger.and.nature.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('danger.and.nature.register'),required=True)
    number = fields.Char(string='№')
    department_id = fields.Many2one('hr.department',string='Department')
    last_name = fields.Many2one('hr.employee',string='Last name',domain="[('department_id', '=', department_id)]")
    page_number = fields.Char(string='Page number')
    to_return_date = fields.Date(string='To return date')
    where_location = fields.Char(string='Where location')
    to_mend_host = fields.Char(string='To mend host')
    danger_check =fields.Text(string='Danger check')
    accepted_employee =fields.Char(string='Accepted employee')
    zalruulalt_percent = fields.Float(string='Zalruulalt percent')