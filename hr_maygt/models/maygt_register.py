# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree




class Maygt(models.Model):
    _name = 'maygt'

    name = fields.Char(u'Файлын нэр', required=True)
    file = fields.Binary(string=u'Файл')

