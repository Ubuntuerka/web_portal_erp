# -*- coding: utf-8 -*-
##############################################################################
#
#
#
#
#
##############################################################################

{
    "name": "Maygt module",
    "version": "1.0",
    "author": "Uuganbayar",
    "category": "document",
    "description": """This module rules management module.""",
    "depends": [
        'hr',
    ],
    'update_xml': [
        #'security/rules_security_view.xml',
        'views/maygt_register_view.xml',
        #'security/ir.model.access.csv',
        'views/menu_view.xml',
        'wizard/application_employee_view.xml',
    ],
    'test': [],
    'installable': True,
    'auto_install': False,
}