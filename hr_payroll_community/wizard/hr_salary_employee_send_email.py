# See LICENSE file for full copyright and licensing details.
"""Model for the track operation."""

from odoo import models, fields, _
from odoo.exceptions import Warning

import xlrd, base64, xlwt
from io import StringIO
from odoo import api, fields, models

class HrSalaryEmployeeSendEmail(models.TransientModel):
    _name = 'hr.salary.employee.send.email'
    _inherits = {'mail.compose.message':'composer_id'}
    _description = 'Action send done'

    is_email = fields.Boolean('Email', default=lambda self: self.env.company.invoice_is_email)
    salary_without_email = fields.Text(compute='_compute_salary_without_email', string='invoice(s) that will not be sent')
    hr_payslip_ids = fields.Many2many('hr.payslip', 'hr_payslip_send_rel', string='Hr payslip')
    composer_id = fields.Many2one('mail.compose.message', string='Composer', required=True, ondelete='cascade')
    template_id = fields.Many2one(
        'mail.template', 'Use template', index=True,
        domain="[('model', '=', 'hr.payslip')]"
        )

    @api.model
    def default_get(self, fields):
        res = super(ActionSendDone, self).default_get(fields)
        res_ids = self._context.get('active_ids')
        composer = self.env['mail.compose.message'].create({
            'composition_mode': 'comment' if len(res_ids) == 1 else 'comment',
        })
        res.update({
            'composer_id': composer.id,
        })
        return res

    @api.onchange('hr_payslip_ids')
    def _compute_composition_mode(self):
        for wizard in self:
            wizard.composer_id.composition_mode = 'mass_mail' if len(wizard.freight_ids) == 1 else 'comment'

    @api.onchange('template_id')
    def onchange_template_id(self):
        for wizard in self:
            if wizard.composer_id:
                wizard.composer_id.template_id = wizard.template_id.id
                wizard.composer_id.onchange_template_id_wrapper()

    @api.onchange('is_email')
    def onchange_is_email(self):
        if self.is_email:
            if not self.composer_id:
                res_ids = self._context.get('active_ids')
                self.composer_id = self.env['mail.compose.message'].create({
                    'composition_mode': 'comment' if len(res_ids) == 1 else 'comment',
                    'template_id': self.template_id.id
                })
            self.composer_id.onchange_template_id_wrapper()

    @api.onchange('is_email')
    def _compute_salary_without_email(self):
        for wizard in self:
            if wizard.is_email and len(wizard.freight_ids) > 1:
                salarys = self.env['hr.payslip'].search([
                    ('id', 'in', self.env.context.get('active_ids')),
                    ('partner_id.email', '=', False)
                ])
                if salarys:
                    wizard.salary_without_email = "%s\n%s" % (
                        _("The following invoice(s) will not be sent by email, because the customers don't have email address."),
                        "\n".join([i.name for i in invoices])
                        )
                else:
                    wizard.salary_without_email = False
            else:
                wizard.salary_without_email = False

    def _send_email(self):
        if self.is_email:
            self.composer_id.send_mail()
            if self.env.context.get('mark_invoice_as_sent'):
                self.mapped('hr_payslip_ids').write({'salary_sent': True})


    def send_action(self):
        self.ensure_one()
        # Send the mails in the correct language by splitting the ids per lang.
        # This should ideally be fixed in mail_compose_message, so when a fix is made there this whole commit should be reverted.
        # basically self.body (which could be manually edited) extracts self.template_id,
        # which is then not translated for each customer.
        if self.composition_mode == 'mass_mail' and self.template_id:
            active_ids = self.env.context.get('active_ids', self.res_id)
            active_records = self.env[self.model].browse(active_ids)
            langs = active_records.mapped('employee_id.user_id.partner_id.lang')
            default_lang = get_lang(self.env)
            for lang in (set(langs) or [default_lang]):
                active_ids_lang = active_records.filtered(lambda r: r.employee_id.user_id.partner_id.lang == lang).ids
                self_lang = self.with_context(active_ids=active_ids_lang, lang=lang)
                self_lang.onchange_template_id()
                self_lang._send_email()
        else:
            self._send_email()
        return {'type': 'ir.actions.act_window_close'}

    def save_as_template(self):
        self.ensure_one()
        self.composer_id.save_as_template()
        self.template_id = self.composer_id.template_id.id
        action = _reopen(self, self.id, self.model, context=self._context)
        action.update({'name': _('Send Salary')})
        return action