# -*- coding:utf-8 -*-

from odoo import api, fields, models


class InsuranceType(models.Model):
    """
    Employee contract based on the visa, work permits
    allows to configure different Salary structure
    """
    _name = 'insurance.type'
    _description = 'Employee insurance type'
    
    company_id = fields.Many2one('res.company',string='Company')
    name = fields.Char(string='Insurance type')
    code = fields.Char(string='Insurance code')
    
