# -*- coding:utf-8 -*-

from odoo import api, fields, models


class HrContract(models.Model):
    """
    Employee contract based on the visa, work permits
    allows to configure different Salary structure
    """
    _inherit = 'hr.contract'
    _description = 'Employee Contract'
    
    struct_id = fields.Many2one('hr.payroll.structure', string='Salary Structure')
    contract_type=fields.Selection([('nuuts_hadgalah',u'НУУЦ ХАДГАЛАХ ГЭРЭЭ'),
                                        ('togtwortoi_ajillah',u'ТОГТВОРТОЙ АЖИЛЛАХ НӨХЦӨЛ БҮХИЙ ГЭРЭЭ'),
                                        ('NHO',u'ХӨДӨЛМӨРИЙН ГЭРЭЭ-НХО'),
                                        ('erhlegch',u'ХӨДӨЛМӨРИЙН ГЭРЭЭ-Эрхлэгч'),
                                        ('office_gtba',u'ХӨДӨЛМӨРИЙН ГЭРЭЭ-Оффис,ГТБА'),
                                        ('SHTS',u'ХӨЛСӨӨР АЖИЛЛАХ ГЭРЭЭ'),
                                        ('ed_hurungu_nho',u'ЭД ХӨРӨНГИЙН БҮРЭН ХАРИУЦЛАГЫН ГЭРЭЭ НХО'),
                                        ('ed_hurungu',u'ЭД ХӨРӨНГИЙН БҮРЭН ХАРИУЦЛАГЫН ГЭРЭЭ')], 'Гэрээний төрөл', required=True)
    insurance_type_id = fields.Many2one('insurance.type',string='Insurance type')
    
    hr_manager_name = fields.Char('Хүний нөөцийн менежер')
    matrits = fields.Many2one('zereg.dev.matrits',string=u'Матриц')
    add_amount = fields.Integer(string=u'Нэмэгдэлийн дүн')
    schedule_pay = fields.Selection([
        ('monthly', 'Monthly'),
        ('quarterly', 'Quarterly'),
        ('semi-annually', 'Semi-annually'),
        ('annually', 'Annually'),
        ('weekly', 'Weekly'),
        ('bi-weekly', 'Bi-weekly'),
        ('bi-monthly', 'Bi-monthly'),
    ], string='Scheduled Pay', index=True, default='monthly',
    help="Defines the frequency of the wage payment.")
    resource_calendar_id = fields.Many2one(required=True, help="Employee's working schedule.")
    decree_id = fields.Many2one('hr.decree')
    state = fields.Selection([
        ('draft', 'New'),
        ('run','Бусад гэрээ'),
        ('open', 'Running'),
        ('close', 'Expired'),
        ('cancel', 'Cancelled')
        ], string='Status', group_expand='_expand_states', copy=False,
        tracking=True, help='Status of the contract', default='draft')
    text = fields.Text('Байршил')
    
    orchin = fields.Selection([
                            ('ger', 'Гэр'),
                            ('baishin', 'Байшин'),
                            ('oron_suuts', 'Орон сууц'),
                            ], string=u'Орчин сонгох'
                            )

    def get_all_structures(self):
        """
        @return: the structures linked to the given contracts, ordered by hierachy (parent=False first,
                 then first level children and so on) and without duplicata
        """
        structures = self.mapped('struct_id')
        if not structures:
            return []
        # YTI TODO return browse records
        return list(set(structures._get_parent_structure().ids))

    def get_attribute(self, code, attribute):
        return self.env['hr.contract.advantage.template'].search([('code', '=', code)], limit=1)[attribute]

    def set_attribute_value(self, code, active):
        for contract in self:
            if active:
                value = self.env['hr.contract.advantage.template'].search([('code', '=', code)], limit=1).default_value
                contract[code] = value
            else:
                contract[code] = 0.0
                
                
    @api.constrains('employee_id', 'state', 'kanban_state', 'date_start', 'date_end')
    def _check_current_contract(self):
        """ Two contracts in state [incoming | open | close] cannot overlap """
        for contract in self.filtered(lambda c: c.state not in ['draft', 'cancel', 'run'] or c.state == 'draft' and c.kanban_state == 'done'):
            domain = [
                ('id', '!=', contract.id),
                ('employee_id', '=', contract.employee_id.id),
                '|',
                    ('state', 'in', ['run' ,'open', 'close']),
                    '&',
                        ('state', '=', 'draft'),
                        ('kanban_state', '=', 'done') # replaces incoming
            ]

           
            
            
                      
    def action_print(self):
        if self.contract_type == 'nuuts_hadgalah':
            return self.env.ref('l10n_mn_hr.report_geree_nuuts_hadgalah_data_data').report_action(self)
        
        if self.contract_type == 'togtwortoi_ajillah':
            return self.env.ref('l10n_mn_hr.report_geree_togtwortoi_ajillah_datas_new').report_action(self)

        if self.contract_type == 'NHO':
            return self.env.ref('l10n_mn_hr.report_geree_NHO_data_new').report_action(self)

        if self.contract_type == 'erhlegch':
            return self.env.ref('l10n_mn_hr.report_geree_erhlegch_data_new').report_action(self)

        if self.contract_type == 'office_gtba':
            return self.env.ref('l10n_mn_hr.report_geree_office_gtba_data_new').report_action(self)

        if self.contract_type == 'SHTS':
            return self.env.ref('l10n_mn_hr.report_geree_SHTS_data_new').report_action(self)
        
        if self.contract_type == 'ed_hurungu':
            return self.env.ref('l10n_mn_hr.report_geree_ed_hurungu_data_new').report_action(self)

    


    @api.onchange('decree_id')
    def onchange_hr_contract(self):
        res = {}
        if self.decree_id:
            empl_obj = self.env['hr.decree']
            res['employee_id'] = self.decree_id.hr_employee_id.id
            res['company_id'] = self.decree_id.company_id.id
            res['department_id'] = self.decree_id.department_id.id
            res['job_id'] = self.decree_id.position_id.id
            return {'value':res}


class HrContractAdvandageTemplate(models.Model):
    _name = 'hr.contract.advantage.template'
    _description = "Employee's Advantage on Contract"

    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    lower_bound = fields.Float('Lower Bound', help="Lower bound authorized by the employer for this advantage")
    upper_bound = fields.Float('Upper Bound', help="Upper bound authorized by the employer for this advantage")
    default_value = fields.Float('Default value for this advantage')
