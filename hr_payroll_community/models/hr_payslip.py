# -*- coding:utf-8 -*-
# -*- coding:utf-8 -*-

import babel
from collections import defaultdict
from datetime import date, datetime, time
from datetime import timedelta
from dateutil.relativedelta import relativedelta
from pytz import timezone
from pytz import utc

from odoo import api, fields, models, tools, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_utils

from datetime import date
from datetime import timedelta
from datetime import datetime

# This will generate 16th of days
ROUNDING_FACTOR = 16
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
class HrPayslip(models.Model):
    _name = 'hr.payslip'
    _description = 'Pay Slip'

    struct_id = fields.Many2one('hr.payroll.structure', string='Structure',
        readonly=True, states={'draft': [('readonly', False)]},
        help='Defines the rules that have to be applied to this payslip, accordingly '
             'to the contract chosen. If you let empty the field contract, this field isn\'t '
             'mandatory anymore and thus the rules applied will be all the rules set on the '
             'structure of all contracts of the employee valid for the chosen period')
    name = fields.Char(string='Payslip Name', readonly=True,
        states={'draft': [('readonly', False)]})
    m1_id = fields.Many2one('mochlog.register',string=u'Цалингийн мөчлөг')
    salary_sent = fields.Boolean(string='Salary sent')
    number = fields.Char(string='Reference', readonly=True, copy=False,
        states={'draft': [('readonly', False)]})
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True, readonly=True,
        states={'draft': [('readonly', False)]})
    date_from = fields.Date(string='Date From', readonly=True, required=True,
        default=lambda self: fields.Date.to_string(date.today().replace(day=1)), states={'draft': [('readonly', False)]})
    date_to = fields.Date(string='Date To', readonly=True, required=True,
        default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()),
        states={'draft': [('readonly', False)]})
    busad_nemegdel = fields.Integer(string=u'Бусад нэмэгдэл')
    # this is chaos: 4 states are defined, 3 are used ('verify' isn't) and 5 exist ('confirm' seems to have existed)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('verify', 'Waiting'),
        ('done', 'Done'),
        ('cancel', 'Rejected'),
    ], string='Status', index=True, readonly=True, copy=False, default='draft',
        help="""* When the payslip is created the status is \'Draft\'
                \n* If the payslip is under verification, the status is \'Waiting\'.
                \n* If the payslip is confirmed then status is set to \'Done\'.
                \n* When user cancel payslip the status is \'Rejected\'.""")
    line_ids = fields.One2many('hr.payslip.line', 'slip_id', string='Payslip Lines', readonly=True,
        states={'draft': [('readonly', False)]})
    company_id = fields.Many2one('res.company', string='Company', readonly=True, copy=False,
        default=lambda self: self.env['res.company']._company_default_get(),
        states={'draft': [('readonly', False)]})
    worked_days_line_ids = fields.One2many('hr.payslip.worked_days', 'payslip_id',
        string='Payslip Worked Days', copy=True, readonly=True,
        states={'draft': [('readonly', False)]})
    input_line_ids = fields.One2many('hr.payslip.input', 'payslip_id', string='Payslip Inputs',
        readonly=True, states={'draft': [('readonly', False)]}, copy=True)
    paid = fields.Boolean(string='Made Payment Order ? ', readonly=True, copy=False,
        states={'draft': [('readonly', False)]})
    note = fields.Text(string='Internal Note', readonly=True, states={'draft': [('readonly', False)]})
    contract_id = fields.Many2one('hr.contract', string='Contract', readonly=True,
        states={'draft': [('readonly', False)]})
    details_by_salary_rule_category = fields.One2many('hr.payslip.line',
        compute='_compute_details_by_salary_rule_category', string='Details by Salary Rule Category')
    credit_note = fields.Boolean(string='Credit Note', readonly=True,
        states={'draft': [('readonly', False)]},
        help="Indicates this payslip has a refund of another")
    payslip_run_id = fields.Many2one('hr.payslip.run', string='Payslip Batches', readonly=True,
        copy=False, states={'draft': [('readonly', False)]})
    payslip_count = fields.Integer(compute='_compute_payslip_count', string="Payslip Computation Details")
    
    @api.onchange('m1_id')
    def onchange_date(self):
        res = {}
        list = []
        if self.m1_id:
            res['date_from'] = self.m1_id.start_date
            res['date_to'] = self.m1_id.end_date
            
        return {'value':res}

    def _compute_details_by_salary_rule_category(self):
        for payslip in self:
            payslip.details_by_salary_rule_category = payslip.mapped('line_ids').filtered(lambda line: line.category_id)

    def _compute_payslip_count(self):
        for payslip in self:
            payslip.payslip_count = len(payslip.line_ids)

    @api.constrains('date_from', 'date_to')
    def _check_dates(self):
        if any(self.filtered(lambda payslip: payslip.date_from > payslip.date_to)):
            raise ValidationError(_("Payslip 'Date From' must be earlier 'Date To'."))

    def action_payslip_draft(self):
        return self.write({'state': 'draft'})
    
    def action_send_email(self):
        """ Open a window to compose an email, with the edi invoice template
            message loaded by default
        """
        self.ensure_one()
        template = self.env.ref('hr_payroll_community.email_template_edi_send_salary', raise_if_not_found=False)
        lang = get_lang(self.env)
        if template and template.lang:
            lang = template._render_template(template.lang, 'hr.payslip', self.id)
        else:
            lang = lang.code
        compose_form = self.env.ref('hr_payroll_community.action_send_email_salary_wizard_form', raise_if_not_found=False)
        ctx = dict(
            default_model='hr.payslip',
            default_res_id=self.id,
            default_use_template=bool(template),
            default_template_id=template and template.id or False,
            default_composition_mode='comment',
            mark_invoice_as_sent=True,
            custom_layout="mail.mail_notification_light",
            force_email=True
        )
        return {
            'name': _('Send Salary'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'hr.salary.employee.send.email',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': ctx,
        }

    def action_payslip_done(self):
        self.compute_sheet()
        return self.write({'state': 'done'})

    def action_payslip_cancel(self):
        if self.filtered(lambda slip: slip.state == 'done'):
            raise UserError(_("Cannot cancel a payslip that is done."))
        return self.write({'state': 'cancel'})

    def refund_sheet(self):
        for payslip in self:
            copied_payslip = payslip.copy({'credit_note': True, 'name': _('Refund: ') + payslip.name})
            copied_payslip.compute_sheet()
            copied_payslip.action_payslip_done()
        formview_ref = self.env.ref('hr_payroll_community.view_hr_payslip_form', False)
        treeview_ref = self.env.ref('hr_payroll_community.view_hr_payslip_tree', False)
        return {
            'name': ("Refund Payslip"),
            'view_mode': 'tree, form',
            'view_id': False,
            'res_model': 'hr.payslip',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': "[('id', 'in', %s)]" % copied_payslip.ids,
            'views': [(treeview_ref and treeview_ref.id or False, 'tree'), (formview_ref and formview_ref.id or False, 'form')],
            'context': {}
        }

    def check_done(self):
        return True

    def unlink(self):
        if any(self.filtered(lambda payslip: payslip.state not in ('draft', 'cancel'))):
            raise UserError(_('You cannot delete a payslip which is not draft or cancelled!'))
        return super(HrPayslip, self).unlink()

    # TODO move this function into hr_contract module, on hr.employee object
    @api.model
    def get_contract(self, employee, date_from, date_to):
        """
        @param employee: recordset of employee
        @param date_from: date field
        @param date_to: date field
        @return: returns the ids of all the contracts for the given employee that need to be considered for the given dates
        """
        # a contract is valid if it ends between the given dates
        clause_1 = ['&', ('date_end', '<=', date_to), ('date_end', '>=', date_from)]
        # OR if it starts between the given dates
        clause_2 = ['&', ('date_start', '<=', date_to), ('date_start', '>=', date_from)]
        # OR if it starts before the date_from and finish after the date_end (or never finish)
        clause_3 = ['&', ('date_start', '<=', date_from), '|', ('date_end', '=', False), ('date_end', '>=', date_to)]
        clause_final = [('employee_id', '=', employee.id), ('state', '=', 'open'), '|', '|'] + clause_1 + clause_2 + clause_3
        return self.env['hr.contract'].search(clause_final).ids

    def compute_sheet(self):
        for payslip in self:
            ooa_amount = 0
            if  payslip.busad_nemegdel > 0.0:
                ooa_amount += payslip.busad_nemegdel
                for line in payslip.input_line_ids:
                    if line.code == "OOA":
                        line.amount = ooa_amount
                    
            number = payslip.number or self.env['ir.sequence'].next_by_code('salary.slip')
            # delete old payslip lines
            payslip.line_ids.unlink()
            # set the list of contract for which the rules have to be applied
            # if we don't give the contract, then the rules to apply should be for all current contracts of the employee
            contract_ids = payslip.contract_id.ids or \
                self.get_contract(payslip.employee_id, payslip.date_from, payslip.date_to)
            lines = [(0, 0, line) for line in self._get_payslip_lines(contract_ids, payslip.id)]
            payslip.write({'line_ids': lines, 'number': number})
            
           
            
              
        return True
    
    
    @api.model
    def get_worked_day_lines(self, contracts, date_from, date_to):
        """
        @param contract: Browse record of contracts
        @return: returns a list of dict containing the input that should be applied for the given contract between date_from and date_to
        """
        res = []
        # fill only if the contract as a working schedule linked
        for contract in contracts.filtered(lambda contract: contract.resource_calendar_id):
            day_from = datetime.combine(fields.Date.from_string(date_from), time.min)
            day_to = datetime.combine(fields.Date.from_string(date_to), time.max)

            # compute leave days
            leaves = {}
            calendar = contract.resource_calendar_id
            tz = timezone(calendar.tz)
            day_leave_intervals = contract.employee_id.list_leaves(day_from, day_to, calendar=contract.resource_calendar_id)
            for day, hours, leave in day_leave_intervals:
                holiday = leave.holiday_id
                current_leave_struct = leaves.setdefault(holiday.holiday_status_id, {
                    'name': holiday.holiday_status_id.name or _('Global Leaves'),
                    'sequence': 5,
                    'code': holiday.holiday_status_id.name or 'GLOBAL',
                    'number_of_days': 0.0,
                    'number_of_hours': 0.0,
                    'contract_id': contract.id,
                })
                current_leave_struct['number_of_hours'] += hours
                work_hours = calendar.get_work_hours_count(
                    tz.localize(datetime.combine(day, time.min)),
                    tz.localize(datetime.combine(day, time.max)),
                    compute_leaves=False,
                )
                if work_hours:
                    current_leave_struct['number_of_days'] += hours / work_hours

            # compute worked days
            work_data = contract.employee_id.get_work_days_data(day_from, day_to, calendar=contract.resource_calendar_id)
            attendances = {
                'name': _("Normal Working Days paid at 100%"),
                'sequence': 1,
                'code': 'WORK100',
                'number_of_days': work_data['days'],
                'number_of_hours': work_data['hours'],
                'contract_id': contract.id,
            }

            res.append(attendances)
            res.extend(leaves.values())
        return res

    @api.model
    def get_inputs(self, contracts, date_from, date_to):
        res = []
        hour_obj = self.env['hr.hour.record']
        prepayment_obj = self.env['hr.payroll.prepayment']
        decree_obj = self.env['hr.decree']
        contract_obj = self.env['hr.contract']
        qty_obj = self.env['shift.working.register.line']
        shift_working = self.env['shift.working.register']
        food_money = self.env['food.money.register']
        department = self.env['hr.department']
        legal_leaves_obj = self.env['hr.payroll.legal.leaves']
        mochlog_obj = self.env['mochlog.register']
        structure_ids = contracts.get_all_structures()
        rule_ids = self.env['hr.payroll.structure'].browse(structure_ids).get_all_rules()
        sorted_rule_ids = [id for id, sequence in sorted(rule_ids, key=lambda x:x[1])]
        inputs = self.env['hr.salary.rule'].browse(sorted_rule_ids).mapped('input_ids')
        prepayment_ids = None
        date_froms = datetime.strptime(str(date_from), DATE_FORMAT)
        date_tos = datetime.strptime(str(date_to), DATE_FORMAT)
        prepayment_id = prepayment_obj.search([('state','=','confirm')])
        for contract in contracts:
            hour = ''
            if contract.employee_id.department_id.id !=False:
                hour_id = hour_obj.search([('start_date','>=',date_from),('end_date','<=',date_to),('state','=','confirmed'),('company_id','=',contract.company_id.id),('department','=',contract.employee_id.department_id.id)]) 
                if hour_id:
                    hour = hour_id             
            else:
                hour_id = hour_obj.search([('start_date','>=',date_from),('end_date','<=',date_to),('state','=','confirmed'),('company_id','=',contract.company_id.id)])
                if hour_id:
                    hour = hour_id
            
            for input in inputs:
                working_time = 0.0
                night_time = 0.0
                pre_amount = 0.0
                shift_holiday = 0.0
                total_work_time = 0.0
                total_work_day = 0.0
                shift_working_time = 0.0
                zdper = 0.0
                work_time = 0.0
                more_time = 0.0
                work_day = 0.0
                leaves_obj = 0.0
                total_time = 0.0
                if  input.code == 'WDT':
                    if hour !='':
                        for line in hour.line_id:
                            if line.employee_id.id == contract.employee_id.id and hour.start_date >=date_from and hour.end_date <= date_to:
                                working_time = line.work_time+line.free_time1+line.night_time
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':working_time,
                         'contract_id': contract.id,
                    }
                elif input.code == 'AS':

                    if prepayment_id !=None:
                        for line in prepayment_id:
                            for pre in line.line_id:
                                line_start_date = datetime.strptime(str(line.start_date), DATETIME_FORMAT) + timedelta(hours=8)
                                if pre.name.id==contract.employee_id.id and line_start_date >=date_froms and line.end_date <= date_tos:
                                    pre_amount = pre.hand_give_amount
                            
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':pre_amount,
                         'contract_id': contract.id,
                    }
                elif input.code == 'NT':
                    if hour !='':
                        for line in hour.line_id:
                            if line.employee_id.id == contract.employee_id.id and hour.start_date >=date_from and hour.end_date <= date_to:
                                night_time = line.night_time
                        input_data = {
                             'name': input.name,
                             'code': input.code,
                             'amount':night_time,
                             'contract_id': contract.id,
                        }
                
                elif input.code =='ZDP':
                    if hour !='':
                        zdper = contract.zereg_id.per
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':zdper,
                         'contract_id': contract.id,
                    }
                elif input.code =='SH':
                    if hour !='':
                        for line in hour.line_id:
                            if line.employee_id.id == contract.employee_id.id:
                                shift_holiday = line.shift_holiday
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':shift_holiday,
                         'contract_id': contract.id,
                    }
                elif input.code == 'FT':
                    if hour !='':
                        for line in hour.line_id:
                            if line.employee_id.id== contract.employee_id.id and hour.start_date >=date_from and hour.end_date <= date_to:
                                total_work_time = line.due_time
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':total_work_time,
                         'contract_id': contract.id,
                    }
                elif input.code == 'FD':
                    if hour !='':
                        for line in hour.line_id:
                            if line.employee_id.id== contract.employee_id.id and hour.start_date >=date_from and hour.end_date <= date_to:
                                total_work_day = line.due_day
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':total_work_day,
                         'contract_id': contract.id,
                    }
                elif input.code == 'FDSH':
                    if hour !='':
                        for line in hour.line_id:
                            if line.employee_id.id== contract.employee_id.id and hour.start_date >=date_from and hour.end_date <= date_to:
                                total_work_day = line.due_day
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':total_work_day,
                         'contract_id': contract.id,
                    }
                elif input.code == 'IT':
                    #insurance_type_id = contract.search([('hr_employee_id','=',contract.insurance_type_id.code)])
                    if not contract.insurance_type_id.code:
                        raise UserError(_('Тухайн ажилтны гэрээн дээрх даатгуулагчийн төрөл талбарын код байгаа эсэхийг шалгана уу!!'))
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':contract.insurance_type_id.code,
                         'contract_id': contract.id,
                    }
                elif input.code == 'WHF':
                    if hour !='':
                        for line in hour.line_id:
                            if line.employee_id.id== contract.employee_id.id and hour.start_date >=date_from and hour.end_date <= date_to:
                                work_time = line.total_time
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':work_time,
                         'contract_id': contract.id,
                    }
                elif input.code == 'MTA':
                    if hour !='':
                        for line in hour.line_id:
                            if line.employee_id.id== contract.employee_id.id and hour.start_date >=date_from and hour.end_date <= date_to:
                                more_time = line.more_time
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':more_time,
                         'contract_id': contract.id,
                    }
                elif input.code == 'MTSH':
                    if hour !='':
                        for line in hour.line_id:
                            if line.employee_id.id== contract.employee_id.id and hour.start_date >=date_from and hour.end_date <= date_to:
                                night_time = line.night_time
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':night_time,
                         'contract_id': contract.id,
                    }
                elif input.code == 'DCCM':
                    if hour !='':
                        for line in hour.line_id:
                            if line.employee_id.id== contract.employee_id.id and hour.start_date >=date_from and hour.end_date <= date_to:
                                work_day = line.work_day
                    input_data = {
                         'name': input.name,
                         'code': input.code,
                         'amount':work_day,
                         'contract_id': contract.id,
                    }
                elif input.code == 'PHA':
                    if contract.phone_add > 0.0:
                        input_data = {
                                     'name': input.name,
                                     'code': input.code,
                                     'amount':contract.phone_add,
                                     'contract_id': contract.id,
                                }
                elif input.code == 'DEC':
                    decree_amount = 0.0
                    decree_id = decree_obj.search([('hr_employee_id','=',contract.employee_id.id),('state','=','approved'),('decree_type_sel','in',('ajild_avah','jinhlen_ajiluulah','ajild_avah1'))],limit=1)
                    if decree_id:
                        if  decree_id.matrits.amount > 0.0:
                            decree_amount = decree_id.matrits.amount+decree_id.add_amount
                    else:
                        contract_id=contract_obj.search([('employee_id','=',contract.employee_id.id),('state','=','open'),('contract_type','=','SHTS')],limit=1)
                        if  contract_id.matrits.amount > 0.0:
                            decree_amount = contract_id.matrits.amount+contract_id.add_amount
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':decree_amount,
                                 'contract_id': contract.id,
                            }
                    

                elif input.code == 'SOS':
                    total_sum_amount = 0.0
                    total_amounts = 0.0
                    f_qty = 0
                    
                    self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.fuel_sale) as shathuun, sum(sl.gas) as hii, sum(sl.qty) as tos, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_id)
                                            WHERE 
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            
                                            group by sl.employee_id
                                    """%(contract.employee_id.id,date_from, date_to))
                    lins  = self._cr.dictfetchall()
                    
                   
                    
                    
                    if  contract.job_id.job_category in ('1','2','3'):
                         self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.fuel_sale) as shathuun, sum(sl.gas) as hii, sum(sl.qty) as tos, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_manager_id)
                                            WHERE 
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            
                                            group by sl.employee_id
                                        """%(contract.employee_id.id,date_from, date_to))
                         lins_erhlegch  = self._cr.dictfetchall()
                         if lins_erhlegch !=None:
                            for sline in lins_erhlegch:
                                f_qty +=sline['tos']
                                total_sum_amount +=f_qty * 25
                    
                    else:
                    
                        if lins !=None:
                            for sline in lins:
                                f_qty +=sline['tos']
                                if  contract.job_id.job_category in ('4','7'):
                                    total_sum_amount +=f_qty * 200
                                elif  contract.job_id.job_category =='6':
                                    total_sum_amount +=f_qty * 300
                    total_amounts = total_sum_amount 
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount': total_amounts,
                                 'contract_id': contract.id,
                            }
                elif input.code == 'SO':
                    total_sum_amount = 0.0
                    total_amounts = 0.0
                    f_qty = 0
                    
                    
                    if  contract.job_id.job_category in ('1','2','3'):
                         self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.fuel_sale) as shathuun, sum(sl.gas) as hii, sum(sl.qty) as tos, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_manager_id)
                                            WHERE 
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            
                                            group by sl.employee_id
                                        """%(contract.employee_id.id,date_from, date_to))
                         lins_erhlegch  = self._cr.dictfetchall()
                         if lins_erhlegch !=None:
                            for sline in lins_erhlegch:
                                f_qty +=sline['tos']
                                total_amounts_so = f_qty
                                
                    else:
                                   
                        self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.fuel_sale) as shathuun, sum(sl.gas) as hii, sum(sl.qty) as tos, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_id)
                                            WHERE 
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            
                                            group by sl.employee_id
                                        """%(contract.employee_id.id,date_from, date_to))
                        lins  = self._cr.dictfetchall()
                        if lins !=None:
                            for sline in lins:
                                f_qty +=sline['tos']
                    total_amounts_so = f_qty 
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount': total_amounts_so,
                                 'contract_id': contract.id,
                            }
                       
                elif input.code == 'SPS':
                    total_sum_gas_amount = 0.0
                    total_gas_amounts = 0.0
                    f_gas = 0.0
                    
                    self._cr.execute(""" select sum(gas) as hii from shift_working_register_line sl 
                                             LEFT JOIN shift_working_register s on (s.id = sl.shift_id) 
                                             where sl.employee_id = %s and s.shift_date >= '%s' and s.shift_date <= '%s'
                                    """%(contract.employee_id.id,date_from, date_to))
                    lins  = self._cr.dictfetchall()
                    
                   
                    
                    
                    if  contract.job_id.job_category in ('1','2','3'):
                         self._cr.execute(""" select sum(gas) as hii from shift_working_register_line sl 
                                             LEFT JOIN shift_working_register s on (s.id = sl.shift_manager_id) 
                                             where sl.employee_id = %s and s.shift_date >= '%s' and s.shift_date <= '%s'
                                    """%(contract.employee_id.id,date_from, date_to))
                         lins_erhlegch  = self._cr.dictfetchall()
                         if lins !=None:
                             for sline in lins_erhlegch:
                                 if sline['hii'] !=None:
                                     f_gas +=sline['hii']
                                     if f_gas>0:
                                         total_sum_gas_amount +=f_gas *3
                    
                    else:
                    
                        if lins !=None:
                            for sline in lins:
                                if sline['hii'] !=None:
                                    f_gas +=sline['hii']
                                    if f_gas>0:
                                        if  contract.job_id.job_category in ('4','7'):
                                            total_sum_gas_amount +=f_gas * 18
                                        elif  contract.job_id.job_category =='6':
                                            total_sum_gas_amount +=f_gas * 18
                    total_gas_amounts = total_sum_gas_amount 
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':total_gas_amounts,
                                 'contract_id': contract.id,
                            }
                elif input.code == 'SP':
                    total_sum_gas_amount = 0.0
                    total_gas_amounts = 0.0
                    f_gas = 0.0

                    if contract.job_id.job_category in ('1','2'):
                         self._cr.execute(""" select sum(gas) as hii from shift_working_register_line sl 
                                             LEFT JOIN shift_working_register s on (s.id = sl.shift_manager_id) 
                                             where sl.employee_id = %s and s.shift_date >= '%s' and s.shift_date <= '%s'
                                    """%(contract.employee_id.id,date_from, date_to))
                         lins_erhlegch  = self._cr.dictfetchall()
                         if lins_erhlegch !=None:
                            for sline in lins_erhlegch:
                                if sline['hii'] !=None:
                                    f_gas +=sline['hii']
                                    if f_gas>0:
                                         total_sum_gas_amount +=f_gas
                                
                    else:
                                   
                        self._cr.execute(""" select sum(gas) as hii from shift_working_register_line sl 
                                             LEFT JOIN shift_working_register s on (s.id = sl.shift_id) 
                                             where sl.employee_id = %s and s.shift_date >= '%s' and s.shift_date <= '%s'
                                        """%(contract.employee_id.id,date_from, date_to))
                        lins  = self._cr.dictfetchall()
                        if lins !=None:
                            for sline in lins:
                                if sline['hii'] !=None:
                                    f_gas +=sline['hii']
                                    if f_gas>0:
                                        total_sum_gas_amount +=f_gas
                    total_gas_amounts_sp = total_sum_gas_amount 
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':total_gas_amounts_sp,
                                 'contract_id': contract.id,
                            }    
                   
                elif input.code == 'SFS':
                    total_sum_fsale_amount = 0.0
                    total_fsale_amounts = 0.0
                    f_fuel_sale = 0.0
                    self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.fuel_sale) as shathuun, sum(sl.gas) as hii, sum(sl.qty) as tos, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_id)
                                            WHERE 
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            
                                            group by sl.employee_id
                                    """%(contract.employee_id.id,date_from, date_to))
                    lins  = self._cr.dictfetchall()
                    
                   
                    
                    
                    if    contract.job_id.job_category in ('1','2'):
                         self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.fuel_sale) as shathuun, sum(sl.gas) as hii, sum(sl.qty) as tos, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_manager_id)
                                            WHERE 
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            
                                            group by sl.employee_id
                                    """%(contract.employee_id.id,date_from, date_to))
                         lins_erhlegch  = self._cr.dictfetchall()
                         if lins_erhlegch !=None:
                            for sline in lins_erhlegch:
                                f_fuel_sale +=sline['shathuun']
                                total_sum_fsale_amount += f_fuel_sale * 0.8
                    
                    else:
                    
                        if lins !=None:
                            for sline in lins:
                                f_fuel_sale +=sline['shathuun']
                                if  contract.job_id.job_category == '4':
                                    total_sum_fsale_amount +=f_fuel_sale * 12
                                elif  contract.job_id.job_category =='6':
                                    total_sum_fsale_amount +=f_fuel_sale * 8
                                elif   contract.job_id.job_category in ('3','7'):
                                    total_sum_fsale_amount +=f_fuel_sale * 5.4
                    total_fsale_amounts = total_sum_fsale_amount 
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':total_fsale_amounts,
                                 'contract_id': contract.id,
                            }
                elif input.code == 'SF':
                    total_sum_fsale_amount = 0.0
                    total_fsale_amounts = 0.0
                    f_fuel_sale = 0.0
                    
                    if contract.job_id.job_category in ('1','2'):
                         self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.fuel_sale) as shathuun, sum(sl.gas) as hii, sum(sl.qty) as tos, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_manager_id)
                                            WHERE 
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            
                                            group by sl.employee_id
                                    """%(contract.employee_id.id,date_from, date_to))
                         lins_erhlegch  = self._cr.dictfetchall()
                         if lins_erhlegch !=None:
                            for sline in lins_erhlegch:
                                f_fuel_sale +=sline['shathuun']
                                total_fsale_amounts += f_fuel_sale
                                
                    else:
                                   
                        self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.fuel_sale) as shathuun, sum(sl.gas) as hii, sum(sl.qty) as tos, sum(sl.acc_qty) as acc
                                                FROM shift_working_register_line sl
                                                LEFT JOIN shift_working_register s on (s.id = sl.shift_id)
                                                WHERE 
                                                sl.employee_id = %s and s.shift_date >= '%s' 
                                                and s.shift_date <= '%s'  
                                                
                                                group by sl.employee_id
                                        """%(contract.employee_id.id,date_from, date_to))
                        lins  = self._cr.dictfetchall()
                        if lins !=None:
                            for sline in lins:
                                f_fuel_sale +=sline['shathuun']
                                total_fsale_amounts += f_fuel_sale
                    total_fsale_amounts_sf = total_fsale_amounts 
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':total_fsale_amounts_sf,
                                 'contract_id': contract.id,
                            }
                elif input.code == 'FM':
                    total_food_money_amount = 0.0
                    total_fmoney_amounts = 0.0
                    total_time = 0.0
                    if hour !='':
                        for line in hour.line_id:
                            if line.employee_id.id== contract.employee_id.id and hour.start_date >=date_from and hour.end_date <= date_to:
                                total_time = line.total_time
                                if  contract.department_id.food_money.amount:
                                    if contract.resource_calendar_id.is_shift == True:
                                       total_food_money_amount +=(contract.department_id.food_money.additional + contract.department_id.food_money.amount) * total_time/12
                                    else:
                                        total_food_money_amount +=(contract.department_id.food_money.additional + contract.department_id.food_money.amount) * total_time/8
                        
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':total_food_money_amount,
                                 'contract_id': contract.id,
                            }        
                elif input.code == 'SBS':
                    total_sum_acc_amount = 0.0
                    total_acc_amounts = 0.0
                    f_acc_qty = 0.0
                    
                    self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_id)
                                            WHERE 
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            
                                            group by sl.employee_id
                                    """%(contract.employee_id.id,date_from, date_to))
                    lins  = self._cr.dictfetchall()
                    
                   
                    
                    
                    if    contract.job_id.job_category in ('1','2','3'):
                         self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_manager_id)
                                            WHERE 
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            
                                            group by sl.employee_id
                                    """%(contract.employee_id.id,date_from, date_to))
                         lins_erhlegch  = self._cr.dictfetchall()
                         if lins_erhlegch !=None:
                            for sline in lins_erhlegch:
                                f_acc_qty +=sline['acc']
                                total_sum_acc_amount +=f_acc_qty * 200
                    
                    else:
                    
                        if lins !=None:
                            for sline in lins:
                                if sline['acc'] != None:
                                    f_acc_qty +=sline['acc']
                                    if   contract.job_id.job_category in ('4','7','6'):
                                        total_sum_acc_amount +=f_acc_qty * 500
                    total_acc_amounts = total_sum_acc_amount 
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':total_acc_amounts,
                                 'contract_id': contract.id,
                            }
                elif input.code == 'SB':
                    total_sum_acc_amount = 0.0
                    total_acc_amounts = 0.0
                    f_acc_qty = 0.0
                    
                    
                    if contract.job_id.job_category in ('1','2'):
                         self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_manager_id)
                                            WHERE 
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            
                                            group by sl.employee_id
                                    """%(contract.employee_id.id,date_from, date_to))
                         lins_erhlegch  = self._cr.dictfetchall()
                         if lins_erhlegch !=None:
                            for sline in lins_erhlegch:
                                f_acc_qty +=sline['acc']
                                total_sum_acc_amount += f_acc_qty  
                    else:
                                   
                        self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_id)
                                            WHERE 
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            
                                            group by sl.employee_id
                                        """%(contract.employee_id.id,date_from, date_to))
                        lins  = self._cr.dictfetchall()
                        if lins !=None:
                            for sline in lins:
                                if sline['acc'] != None:
                                    f_acc_qty +=sline['acc']
                                    total_sum_acc_amount +=f_acc_qty
                    total_acc_amounts_sb = total_sum_acc_amount 
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':total_acc_amounts_sb,
                                 'contract_id': contract.id,
                            }
                    
                elif input.code == 'HAL':
                    hal_amount = 0.0
                    print('tttttttt',date_from,date_to)
                    date_froms = datetime.strptime(str(date_from), DATE_FORMAT)
                    date_tos = datetime.strptime(str(date_to), DATE_FORMAT)
                    decree_id1 = decree_obj.search([('hr_employee_id','=',contract.employee_id.id),('state','=','approved'),('decree_type_sel', 'in', ['ajil_orlon', 'ajil_hawsran_shts']),('start_date','>=',date_froms),('end_date','<=',date_tos)],limit=1)
                    decree_id = decree_obj.search([('hr_employee_id','=',contract.employee_id.id),('state','=','approved'),('decree_type_sel','=','jinhlen_ajiluulah')],limit=1)
                    if decree_id:
  
                        
                        start_date = datetime.strptime(str(decree_id1.start_date), DATE_FORMAT)
                        end_date = datetime.strptime(str(decree_id1.end_date), DATE_FORMAT)
                        days_difference = float((end_date - start_date).days) * 24
                        print('aaaaaaaaaaaaa',days_difference)
                        huwi = 0.0
                        if  decree_id.matrits.amount > 0.0:
                            if decree_id1.huwi == '20':
                                huwi = 0.2
                            elif decree_id1.huwi == '25':
                                huwi = 0.25
                            elif decree_id1.huwi == '30':
                                huwi = 0.3
                            elif decree_id1.huwi == '40':
                                huwi = 0.4
                            elif decree_id1.huwi == '60':
                                huwi = 0.6
                            elif decree_id1.huwi == '80':
                                huwi = 0.8
                            elif decree_id1.huwi == '100':
                                huwi = 1
                            hal_amount = (decree_id.matrits.amount+decree_id.add_amount)*huwi* days_difference
                    else:
                        contract_id = contract_obj.search([('hr_employee_id','=',contract.employee_id.id),('state','=','open'),('contract_type','=','SHTS')],limit=1)
                        huwi = 0.0
                        if  contract_id.matrits.amount > 0.0:
                            if decree_id1.huwi == '20':
                                huwi = 0.2
                            elif decree_id1.huwi == '25':
                                huwi = 0.25
                            elif decree_id1.huwi == '30':
                                huwi = 0.3
                            elif decree_id1.huwi == '40':
                                huwi = 0.4
                            elif decree_id1.huwi == '60':
                                huwi = 0.6
                            elif decree_id1.huwi == '80':
                                huwi = 0.8
                            elif decree_id1.huwi == '100':
                                huwi = 1
                            hal_amount = (contract_id.matrits.amount+contract_id.add_amount)*huwi
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':hal_amount,
                                 'contract_id': contract.id,
                            }
                          
                elif input.code == 'OA':
                    oa_amount = 0.0
                    decree_id = decree_obj.search([('hr_employee_id','=',contract.employee_id.id),('state','=','approved'),('decree_type_sel','in',('shagnal','tetgemj'))])
                    if decree_id:
                        for dec_id in decree_id:
                            if  dec_id.amount > 0.0:
                                oa_amount += dec_id.amount
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':oa_amount,
                                 'contract_id': contract.id,
                            }
                elif input.code == 'OOA':
                    ooa_amount = 0.0
                    if self.id:
                        for sid in self.id:
                            if  sid.busad_nemegdel > 0.0:
                                    ooa_amount += sid.busad_nemegdel
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':ooa_amount,
                                 'contract_id': contract.id,
                            }
                
                elif input.code == 'AHJ':
                    ahj_amount = 0.0
                    leaves_id = legal_leaves_obj.search([('state','=','confirm'),('end_date','>=',date_from),('end_date','<=',date_to)])
                    if leaves_id:
                        for lin in leaves_id.line_id:
                            if lin.name.id == contract.employee_id.id:
                                if  lin.legal_leaves_day > 0.0:
                                    ahj_amount += lin.legal_leaves_day
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':ahj_amount,
                                 'contract_id': contract.id,
                            }
                elif input.code == 'HM':
                    hm_amount = 0.0
                    leaves_pay_id = legal_leaves_obj.search([('state','=','confirm'),('end_date','>=',date_from),('end_date','<=',date_to)])
                    if leaves_pay_id:
                        for lines in leaves_pay_id.line_id:
                            if lines.name.id == contract.employee_id.id:
                                if  lines.leaves_amount_payroll > 0.0:
                                    hm_amount += lines.leaves_amount_payroll
                    input_data = {
                                 'name': input.name,
                                 'code': input.code,
                                 'amount':hm_amount,
                                 'contract_id': contract.id,
                            }
                else:                    
                    input_data = {
                        'name': input.name,
                        'code': input.code,
                        'contract_id': contract.id,
                    }
                res += [input_data]
               
                
        return res

    @api.model
    def _get_payslip_lines(self, contract_ids, payslip_id):
        def _sum_salary_rule_category(localdict, category, amount):
            if category.parent_id:
                localdict = _sum_salary_rule_category(localdict, category.parent_id, amount)
            localdict['categories'].dict[category.code] = category.code in localdict['categories'].dict and localdict['categories'].dict[category.code] + amount or amount
            return localdict

        class BrowsableObject(object):
            def __init__(self, employee_id, dict, env):
                self.employee_id = employee_id
                self.dict = dict
                self.env = env

            def __getattr__(self, attr):
                return attr in self.dict and self.dict.__getitem__(attr) or 0.0

        class InputLine(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = fields.Date.today()
                self.env.cr.execute("""
                    SELECT sum(amount) as sum
                    FROM hr_payslip as hp, hr_payslip_input as pi
                    WHERE hp.employee_id = %s AND hp.state = 'done'
                    AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s""",
                    (self.employee_id, from_date, to_date, code))
                return self.env.cr.fetchone()[0] or 0.0

        class WorkedDays(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def _sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = fields.Date.today()
                self.env.cr.execute("""
                    SELECT sum(number_of_days) as number_of_days, sum(number_of_hours) as number_of_hours
                    FROM hr_payslip as hp, hr_payslip_worked_days as pi
                    WHERE hp.employee_id = %s AND hp.state = 'done'
                    AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s""",
                    (self.employee_id, from_date, to_date, code))
                return self.env.cr.fetchone()

            def sum(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[0] or 0.0

            def sum_hours(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[1] or 0.0

        class Payslips(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""

            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = fields.Date.today()
                self.env.cr.execute("""SELECT sum(case when hp.credit_note = False then (pl.total) else (-pl.total) end)
                            FROM hr_payslip as hp, hr_payslip_line as pl
                            WHERE hp.employee_id = %s AND hp.state = 'done'
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pl.slip_id AND pl.code = %s""",
                            (self.employee_id, from_date, to_date, code))
                res = self.env.cr.fetchone()
                return res and res[0] or 0.0

        #we keep a dict with the result because a value can be overwritten by another rule with the same code
        result_dict = {}
        rules_dict = {}
        worked_days_dict = {}
        inputs_dict = {}
        blacklist = []
        payslip = self.env['hr.payslip'].browse(payslip_id)
        for worked_days_line in payslip.worked_days_line_ids:
            worked_days_dict[worked_days_line.code] = worked_days_line
        for input_line in payslip.input_line_ids:
            inputs_dict[input_line.code] = input_line

        categories = BrowsableObject(payslip.employee_id.id, {}, self.env)
        inputs = InputLine(payslip.employee_id.id, inputs_dict, self.env)
        worked_days = WorkedDays(payslip.employee_id.id, worked_days_dict, self.env)
        payslips = Payslips(payslip.employee_id.id, payslip, self.env)
        rules = BrowsableObject(payslip.employee_id.id, rules_dict, self.env)

        baselocaldict = {'categories': categories, 'rules': rules, 'payslip': payslips, 'worked_days': worked_days, 'inputs': inputs}
        #get the ids of the structures on the contracts and their parent id as well
        contracts = self.env['hr.contract'].browse(contract_ids)
        if len(contracts) == 1 and payslip.struct_id:
            structure_ids = list(set(payslip.struct_id._get_parent_structure().ids))
        else:
            structure_ids = contracts.get_all_structures()
        #get the rules of the structure and thier children
        rule_ids = self.env['hr.payroll.structure'].browse(structure_ids).get_all_rules()
        #run the rules by sequence
        sorted_rule_ids = [id for id, sequence in sorted(rule_ids, key=lambda x:x[1])]
        sorted_rules = self.env['hr.salary.rule'].browse(sorted_rule_ids)

        for contract in contracts:
            employee = contract.employee_id
            localdict = dict(baselocaldict, employee=employee, contract=contract)
            for rule in sorted_rules:
                key = rule.code + '-' + str(contract.id)
                localdict['result'] = None
                localdict['result_qty'] = 1.0
                localdict['result_rate'] = 100
                #check if the rule can be applied
                if rule._satisfy_condition(localdict) and rule.id not in blacklist:
                    #compute the amount of the rule
                    amount, qty, rate = rule._compute_rule(localdict)
                    #check if there is already a rule computed with that code
                    previous_amount = rule.code in localdict and localdict[rule.code] or 0.0
                    #set/overwrite the amount computed for this rule in the localdict
                    tot_rule = amount * qty * rate / 100.0
                    localdict[rule.code] = tot_rule
                    rules_dict[rule.code] = rule
                    #sum the amount for its salary category
                    localdict = _sum_salary_rule_category(localdict, rule.category_id, tot_rule - previous_amount)
                    #create/overwrite the rule in the temporary results
                    result_dict[key] = {
                        'salary_rule_id': rule.id,
                        'contract_id': contract.id,
                        'name': rule.name,
                        'code': rule.code,
                        'category_id': rule.category_id.id,
                        'sequence': rule.sequence,
                        'appears_on_payslip': rule.appears_on_payslip,
                        'condition_select': rule.condition_select,
                        'condition_python': rule.condition_python,
                        'condition_range': rule.condition_range,
                        'condition_range_min': rule.condition_range_min,
                        'condition_range_max': rule.condition_range_max,
                        'amount_select': rule.amount_select,
                        'amount_fix': rule.amount_fix,
                        'amount_python_compute': rule.amount_python_compute,
                        'amount_percentage': rule.amount_percentage,
                        'amount_percentage_base': rule.amount_percentage_base,
                        'register_id': rule.register_id.id,
                        'amount': amount,
                        'employee_id': contract.employee_id.id,
                        'quantity': qty,
                        'rate': rate,
                    }
                else:
                    #blacklist this rule and its children
                    blacklist += [id for id, seq in rule._recursive_search_of_rules()]

        return list(result_dict.values())

    # YTI TODO To rename. This method is not really an onchange, as it is not in any view
    # employee_id and contract_id could be browse records
    def onchange_employee_id(self, date_from, date_to, employee_id=False, contract_id=False):
        #defaults
        res = {
            'value': {
                'line_ids': [],
                #delete old input lines
                'input_line_ids': [(2, x,) for x in self.input_line_ids.ids],
                #delete old worked days lines
                'worked_days_line_ids': [(2, x,) for x in self.worked_days_line_ids.ids],
                #'details_by_salary_head':[], TODO put me back
                'name': '',
                'contract_id': False,
                'struct_id': False,
            }
        }
        if (not employee_id) or (not date_from) or (not date_to):
            return res
        ttyme = datetime.combine(fields.Date.from_string(date_from), time.min)
        employee = self.env['hr.employee'].browse(employee_id)
        locale = self.env.context.get('lang') or 'en_US'
        res['value'].update({
            'name': _('Salary Slip of %s for %s') % (employee.name, tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale))),
            'company_id': employee.company_id.id,
        })

        if not self.env.context.get('contract'):
            #fill with the first contract of the employee
            contract_ids = self.get_contract(employee, date_from, date_to)
        else:
            if contract_id:
                #set the list of contract for which the input have to be filled
                contract_ids = [contract_id]
            else:
                #if we don't give the contract, then the input to fill should be for all current contracts of the employee
                contract_ids = self.get_contract(employee, date_from, date_to)

        if not contract_ids:
            return res
        contract = self.env['hr.contract'].browse(contract_ids[0])
        res['value'].update({
            'contract_id': contract.id
        })
        struct = contract.struct_id
        if not struct:
            return res
        res['value'].update({
            'struct_id': struct.id,
        })
        #computation of the salary input
        contracts = self.env['hr.contract'].browse(contract_ids)
        worked_days_line_ids = self.get_worked_day_lines(contracts, date_from, date_to)
        input_line_ids = self.get_inputs(contracts, date_from, date_to)
        res['value'].update({
            'worked_days_line_ids': worked_days_line_ids,
            'input_line_ids': input_line_ids,
        })
        return res

    @api.onchange('employee_id', 'date_from', 'date_to')
    def onchange_employee(self):
 
        if (not self.employee_id) or (not self.date_from) or (not self.date_to):
            return
 
        employee = self.employee_id
        date_from = self.date_from
        date_to = self.date_to
        contract_ids = []
 
        ttyme = datetime.combine(fields.Date.from_string(date_to), time.min)
        locale = self.env.context.get('lang') or 'en_US'
        self.name = _('Salary Slip of %s for %s') % (employee.name, tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale)))
        self.company_id = employee.company_id
 
        if not self.env.context.get('contract') or not self.contract_id:
            contract_ids = self.get_contract(employee, date_from, date_to)
            if not contract_ids:
                return
            self.contract_id = self.env['hr.contract'].browse(contract_ids[0])
 
        if not self.contract_id.struct_id:
            return
        self.struct_id = self.contract_id.struct_id
 
        #computation of the salary input
        contracts = self.env['hr.contract'].browse(contract_ids)
        worked_days_line_ids = self.get_worked_day_lines(contracts, date_from, date_to)
        worked_days_lines = self.worked_days_line_ids.browse([])
        for r in worked_days_line_ids:
            worked_days_lines += worked_days_lines.new(r)
        self.worked_days_line_ids = worked_days_lines
 
        input_line_ids = self.get_inputs(contracts, date_from, date_to)
        input_lines = self.input_line_ids.browse([])
        for r in input_line_ids:
            input_lines += input_lines.new(r)
        self.input_line_ids = input_lines
        return

    #===========================================================================
    # @api.onchange('contract_id')
    # def onchange_contract(self):
    #     if not self.contract_id:
    #         self.struct_id = False
    #     self.with_context(contract=True).onchange_employee()
    #     return
    #===========================================================================

    def get_salary_line_total(self, code):
        self.ensure_one()
        line = self.line_ids.filtered(lambda line: line.code == code)
        if line:
            return line[0].total
        else:
            return 0.0


class HrPayslipLine(models.Model):
    _name = 'hr.payslip.line'
    _inherit = 'hr.salary.rule'
    _description = 'Payslip Line'
    _order = 'contract_id, sequence'

    slip_id = fields.Many2one('hr.payslip', string='Pay Slip', required=True, ondelete='cascade')
    salary_rule_id = fields.Many2one('hr.salary.rule', string='Rule', required=True)
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True)
    contract_id = fields.Many2one('hr.contract', string='Contract', required=True, index=True)
    rate = fields.Float(string='Rate (%)', digits=dp.get_precision('Payroll Rate'), default=100.0)
    amount = fields.Float(digits=dp.get_precision('Payroll'))
    quantity = fields.Float(digits=dp.get_precision('Payroll'), default=1.0)
    total = fields.Float(compute='_compute_total', string='Total', digits=dp.get_precision('Payroll'), store=True)

    @api.depends('quantity', 'amount', 'rate')
    def _compute_total(self):
        for line in self:
            line.total = float(line.quantity) * line.amount * line.rate / 100

    @api.model_create_multi
    def create(self, vals_list):
        for values in vals_list:
            if 'employee_id' not in values or 'contract_id' not in values:
                payslip = self.env['hr.payslip'].browse(values.get('slip_id'))
                values['employee_id'] = values.get('employee_id') or payslip.employee_id.id
                values['contract_id'] = values.get('contract_id') or payslip.contract_id and payslip.contract_id.id
                if not values['contract_id']:
                    raise UserError(_('You must set a contract to create a payslip line.'))
        return super(HrPayslipLine, self).create(vals_list)


class HrPayslipWorkedDays(models.Model):
    _name = 'hr.payslip.worked_days'
    _description = 'Payslip Worked Days'
    _order = 'payslip_id, sequence'

    name = fields.Char(string='Description', required=True)
    payslip_id = fields.Many2one('hr.payslip', string='Pay Slip', required=True, ondelete='cascade', index=True)
    sequence = fields.Integer(required=True, index=True, default=10)
    code = fields.Char(required=True, help="The code that can be used in the salary rules")
    number_of_days = fields.Float(string='Number of Days')
    number_of_hours = fields.Float(string='Number of Hours')
    contract_id = fields.Many2one('hr.contract', string='Contract', required=True,
        help="The contract for which applied this input")


class HrPayslipInput(models.Model):
    _name = 'hr.payslip.input'
    _description = 'Payslip Input'
    _order = 'payslip_id, sequence'

    name = fields.Char(string='Description', required=True)
    payslip_id = fields.Many2one('hr.payslip', string='Pay Slip', required=True, ondelete='cascade', index=True)
    sequence = fields.Integer(required=True, index=True, default=10)
    code = fields.Char(required=True, help="The code that can be used in the salary rules")
    amount = fields.Float(help="It is used in computation. For e.g. A rule for sales having "
                               "1% commission of basic salary for per product can defined in expression "
                               "like result = inputs.SALEURO.amount * contract.wage*0.01.")
    contract_id = fields.Many2one('hr.contract', string='Contract', required=True,
        help="The contract for which applied this input")

class Mochlog(models.Model):
    _name = 'mochlog.register'
    
    name = fields.Char(string=u'Нэр')
    start_date = fields.Date(string=u'Эхлэх огноо')
    end_date = fields.Date(string=u'Дуусах огноо')
    is_active = fields.Boolean(string=u'Идэвхтэй')


class HrPayslipRun(models.Model):
    _name = 'hr.payslip.run'
    _description = 'Payslip Batches'

    name = fields.Char(required=True, readonly=True, states={'draft': [('readonly', False)]})
    slip_ids = fields.One2many('hr.payslip', 'payslip_run_id', string='Payslips', readonly=True,
        states={'draft': [('readonly', False)]})
    state = fields.Selection([
        ('draft', 'Draft'),
        ('close', 'Close'),
    ], string='Status', index=True, readonly=True, copy=False, default='draft')
    date_start = fields.Date(string='Date From', required=True, readonly=True,
        states={'draft': [('readonly', False)]})
    date_end = fields.Date(string='Date To', required=True, readonly=True,
        states={'draft': [('readonly', False)]})
    busad_nemegdel = fields.Integer(string=u'Бусад нэмэгдэл')
    credit_note = fields.Boolean(string='Credit Note', readonly=True,
        states={'draft': [('readonly', False)]},
        help="If its checked, indicates that all payslips generated from here are refund payslips.")
    m_id = fields.Many2one('mochlog.register',string=u'Цалингийн мөчлөг')
    

    def draft_payslip_run(self):
        return self.write({'state': 'draft'})

    def close_payslip_run(self):
        return self.write({'state': 'close'})
    
    
    def compute_busad_nemegdel(self):
        for slip_ids in self.slip_ids:
            
            if slip_ids.busad_nemegdel > 0:
              slip_ids.compute_sheet()
        return True
    
                
    @api.onchange('m_id')
    def onchange_date(self):
        res = {}
        list = []
        if self.m_id:
            res['date_start'] = self.m_id.start_date
            res['date_end'] = self.m_id.end_date
            
        return {'value':res}
               


class ResourceMixin(models.AbstractModel):
    _inherit = "resource.mixin"

    def get_work_days_data(self, from_datetime, to_datetime, compute_leaves=True, calendar=None, domain=None):
        """
            By default the resource calendar is used, but it can be
            changed using the `calendar` argument.

            `domain` is used in order to recognise the leaves to take,
            None means default value ('time_type', '=', 'leave')

            Returns a dict {'days': n, 'hours': h} containing the
            quantity of working time expressed as days and as hours.
        """
        resource = self.resource_id
        calendar = calendar or self.resource_calendar_id

        # naive datetimes are made explicit in UTC
        if not from_datetime.tzinfo:
            from_datetime = from_datetime.replace(tzinfo=utc)
        if not to_datetime.tzinfo:
            to_datetime = to_datetime.replace(tzinfo=utc)

        # total hours per day: retrieve attendances with one extra day margin,
        # in order to compute the total hours on the first and last days
        from_full = from_datetime - timedelta(days=1)
        to_full = to_datetime + timedelta(days=1)
        intervals = calendar._attendance_intervals(from_full, to_full, resource)
        day_total = defaultdict(float)
        for start, stop, meta in intervals:
            day_total[start.date()] += (stop - start).total_seconds() / 3600

        # actual hours per day
        if compute_leaves:
            intervals = calendar._work_intervals(from_datetime, to_datetime, resource, domain)
        else:
            intervals = calendar._attendance_intervals(from_datetime, to_datetime, resource)
        day_hours = defaultdict(float)
        for start, stop, meta in intervals:
            day_hours[start.date()] += (stop - start).total_seconds() / 3600

        # compute number of days as quarters
        days = sum(
            float_utils.round(ROUNDING_FACTOR * day_hours[day] / day_total[day]) / ROUNDING_FACTOR
            for day in day_hours
        )
        return {
            'days': days,
            'hours': sum(day_hours.values()),
        }