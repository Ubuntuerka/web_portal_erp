# -*- coding: utf-8 -*-
##############################################################################
#
#    
#
#    
#
##############################################################################

{
    "name": "Audit module",
    "version": "1.0",
    "author": "Uuganbayar",
    "category": "audit",
    "description": """This module audit module.""",
    "depends" : [
                 'hr',
                 "base",
                 ],
    'update_xml': [
                   "ul_tohirol_view.xml",
                   'ut_sequence.xml',
                   'views/ul_tohirol.xml',
                   'views/ul_tohirol_menu.xml',
                   'security/ul_toh_security.xml',
                   'security/ir.model.access.csv',
                   ],
    'test': [],
    'installable': True,
    'auto_install': False, 
}
