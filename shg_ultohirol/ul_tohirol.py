# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import timedelta
from functools import partial
from datetime import datetime, time
import psycopg2
import pytz
from datetime import date, timedelta
from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from odoo.http import request
import odoo.addons.decimal_precision as dp

class ugroup(models.Model):
    _name = 'ugroup'
    _description = "ultohirolbuleg"
    
    name = fields.Char(u'Үл тохиролын бүлэг', required = True)
   
class ul_medium(models.Model):
    _name = 'ul_medium'
    _description = "ultohirolmedium"
    
    name = fields.Char(u'Бүс', required = True)

class audit_sanal(models.Model):
    
    _name = 'audit_sanal'
    _description = "auditsanal"
    
    def _get_default_department(self):
        user = self.env['res.users'].browse(self.env.user.id)
        return user.department_id.id  
    
    date = fields.Date(u'Огноо', default=fields.Date.context_today, required = True)
    department_id = fields.Many2one('hr.department', u'Нэгж', default=lambda self: self.env.user.dep_id, required = True)
    user_id = fields.Many2one('res.users',u'Ажилтны нэр', default=lambda self: self.env.user,
                                      required=True, readonly=True)
    note = fields.Text(u'Санал хүсэлт', required = True)

   
        

class ultohirol(models.Model):
    _name = 'ultohirol'
    _description = "ultohirol"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'name desc'
    
    def _get_default_department(self):
        user = self.env['res.users'].browse(self.env.user.id)
        return user.department_id.id  

    #Аудитаар өгсөн үл тохирол

    state = fields.Selection([('draft', 'Ноорог'),  # Ноорог
                              ('registered', 'Бүртгэсэн'),
                              ('corrected', 'Залруулсан'),
                              ('done', 'Дууссан')], string='Төлөв', default='draft')
    name = fields.Char(u'Дугаар') #, default=lambda self: self.env['ir.sequence'].next_by_code('ultohirol')
    date = fields.Date(u'Огноо', readonly=True, default=fields.Date.context_today)
    # year = fields.Char(u'Жил')
    # month = fields.Char(u'Сар')
    # day = fields.Char(u'Өдөр')
    ul_user_id = fields.Many2one('res.users', string='Үл тохирол хариуцах ажилтан', required=True)
    # medium_id = fields.Many2one('ul_medium',u'Бүс')
    company_id = fields.Many2one('res.company', string='Компани')
    region_id = fields.Many2one('shts.region',string='Бүс')
    department_id = fields.Many2one('hr.department', u'Нэгж', required = True)
    group_id = fields.Many2one('ugroup',u'Бүлэг', required = True)
    note = fields.Text(u'Үл тохиролын тодорхойлолт')
    pic = fields.Binary(u'Зураг')
    zovlomj = fields.Text(u'Залруулах зөвлөмж')
    hariutsah_department = fields.Many2one('hr.department', u'Хариуцах газар/нэгж', required = True)
    # auditor_id = fields.Many2many('res.users', 'ultohirol_users_rel', 'user_id', 'ultoh_id', u'Аудитор', required = True)
    auditor_user_id = fields.Many2one('res.users', u'Аудитор', default=lambda self: self.env.user,
                                      required=True, readonly=True)
    zalruulah_date = fields.Date(u'Залруулах хугацаа', required=True)
    sungah_date = fields.Date(u'Залруулах хугацаа сунгах')
    burtgesen = fields.Many2one('res.users',u'Бүртгэсэн аудитор')
    utype = fields.Selection([('zovlomj',u'Тохиолдол'),
    ('zovlomj1',u'Зөвлөмж'),
    ('small',u'Жижиг'),
    ('big',u'Том')],u'Үл тохиролын төрөл', required = True)
    #Залруулагын тайлйан
    # zar_biy = fields.Selection([('zero',u'0%'),
    #                             ('fifty',u'50%'),
    #                             ('one_hundred',u'100%')],u'Залруулагын биелэлт %')
    # process = fields.Integer(u'Залруулагын биелэлт %')
    process_per = fields.Selection([('zero',u'0%'),
                                     ('fifty',u'50%'),
                                     ('one_hundred',u'100%')],u'Залруулагын биелэлт %', default='zero')
    # zal_date = fields.Date(u'Залруулагын огноо')
    zal_date1 = fields.Date(u'Залруулагын огноо')
    is_zalruulga = fields.Boolean(u'Хугацаандаа залруулсан эсэх')
    is_zal = fields.Boolean(u'Залруулсан эсэх',  compute='_compute_is_zal', store=True)
    
    znote = fields.Text(u'Залруулагын тайлбар')
    znote3 = fields.Text(u'Аудитор тайлбар')
    zurag = fields.Binary(u'Зураг')
    # aprocess = fields.Integer(u'Аудитын шалгасан %')
    aprocess_per = fields.Selection([('zero',u'0%'),
                                     ('fifty',u'50%'),
                                     ('one_hundred',u'100%')],u'Аудитын шалгасан %', default='zero')
    # b_date = fields.Date(u'Биелэлт шалгасан огноо')
    b_date1 = fields.Datetime(u'Биелэлт шалгасан огноо')
    bauditor_id = fields.Many2one('res.users',u'Биелэлт шалгасан аудитор')
    #Эргэн хяналтын аудитаар шалгасан байдал
    
    g_date = fields.Date(u'Газар дээр шалгах огноо')
    gauditor_id = fields.Many2one('res.users',u'Газар дээр шалгасан аудитор')
    # gprocess = fields.Integer(u'Газар дээрх залруулагын биелэлт %')
    gprocess_per = fields.Selection([('zero',u'0%'),
                                     ('fifty',u'50%'),
                                     ('one_hundred',u'100%')],u'Газар дээрх залруулагын биелэлт %', default='zero')
    shalgasan_date = fields.Date(u'Шалгасан огноо')

    is_hoh = fields.Boolean(u'Хөрөнгө оуулалттай холбоотой')
	
	#Нэмэлт талбарууд
    davtamj =  fields.Selection([('1','1'),('2','2'),('3','3'),('4','4'),('5','5'), ('6','6'),('7','7')],u'Давтамж')
    zal_job_id = fields.Many2one('hr.job',u'Албан тушаал')
    zal_user_id = fields.Many2one('res.users',u'Ажилтны нэр')
    is_horh = fields.Boolean(u'ХО холбоотой')
    note1 = fields.Text(u'Тайлбар')
    use_department_id = fields.Many2one('hr.department', u'Хяналтын нэгж')
    dift_date = fields.Integer(u'Залруулгын хугацаа хэтэрсэн хоног')

    def action_register(self):
        self.write({'state': 'registered'})

    @api.depends('process_per')
    def _compute_is_zal(self):
        for record in self:
            if record.process_per in ['zero', 'fifty']:
                record.is_zal = False
            else:
                record.is_zal = True

    def action_zalruulga(self):
        employee = self.env['hr.employee']
        for record in self:
            if record.process_per == 'one_hundred' and record.znote:
                emp = employee.search([('user_id', '=', record.env.user.id)], limit=1)
                # if not emp:
                #     raise UserError('Хэрэглэгчийн ажилтан олдсонгүй.')
                record.write({
                    'state': 'corrected',
                    'zal_user_id': record.env.user.id,
                    'zal_job_id': emp.job_id.id if emp.job_id else False,
                    'zal_date1': fields.Datetime.now()
                })
            if record.zalruulah_date and record.zal_date1:
                if record.zal_date1 <= record.zalruulah_date:
                    record.is_zalruulga = True
                else:
                    record.dift_date = (record.zal_date1 - record.zalruulah_date).days

    def action_done(self):
        self.write({'state': 'done', 'bauditor_id': self.env.user.id, 'b_date1': fields.Datetime.now()})

    def action_return(self):
        self.write({'state': 'draft'})
    def action_return1(self):
        self.write({'state': 'corrected'})

    def action_return2(self):
        self.write({'state': 'registered'})

    @api.onchange('date')
    def onchange_number(self):
        dugaar = ''
        if self.date:
            year = self.date.year
            ul_tohirol = self.search([('date', '>=', f'{year}-01-01'), ('date', '<=', f'{year}-12-31')], order='id desc', limit=1)
            if ul_tohirol.name:
                too1 = int(ul_tohirol.name[6:]) + 1
                too = str(too1).zfill(4)
                dugaar = 'UT' + str(year) + too
            else:
                dugaar = 'UT' + str(year) + '0001'
        self.name = dugaar

    def _add_zero(self, too):
        if too < 10:
            dugaar = "000" + str(too)
        elif too >= 10 and 100 > too:
            dugaar = "00" + str(too)
        elif too >= 100 and 1000 > too:
            dugaar = "0" + str(too)
        else:
            dugaar = too
        return dugaar

    # def onchange_date(self):
    #     if self.date:
    #         year1 = self.date[0:4]
    #         month1 = self.date[0:7]
    #         day1 = self.date[0:10]
    #
    #         self.year = year1
    #         self.month = month1
    #         self.day = day1

    # def onchange_zalruulah_date(self):
    #     if self.zalruulah_date:
    #         do_date = datetime.strptime(self.zalruulah_date, "%Y-%m-%d")
    #         futuredate = do_date + timedelta(days=14)
    #         self.sungah_date = futuredate
    #
    # def onchange_process(self):
    #     if self.process:
    #         zal_date = time.strftime('%Y-%m-%d')
    #         is_zalruulga = False
    #
    #         obj = self.browse()
    #         user = self.env['res.users'].browse()
    #         if obj.zalruulah_date:
    #            if obj.zalruulah_date >= zal_date:
    #               is_zalruulga = True
    #         self.write({
    #                     'zal_date':zal_date,
    #                     'is_zalruulga': is_zalruulga,
    #                     'zal_user_id': uid,
    #                     'zal_job_id': user.job_id.id,
    #                     })
    #         return True
    #
    # def onchange_aprocess(self):
    #     if self.aprocess:
    #         b_date = time.strftime('%Y-%m-%d')
    #
    #         self.b_date = b_date
    #         self.bauditor_id = uid
    #
    # def onchange_gprocess(self):
    #     if self.gprocess:
    #         g_date = time.strftime('%Y-%m-%d')
    #
    #         self.shalgasan_date =g_date
    #         self.gauditor_id = uid
    #
    # def action_zalruulga(self):
    #
    #     obj = self.browse()
    #     if obj.process_per and obj.znote:
    #        self.write({'is_zal':True})
    #     else:
    #         raise UserError(('Залруулгын биелэлт болон тайлбар оруулна уу!!'))
    #     return True
