# -*- coding: utf-8 -*-

from . import rest_api_tokens
from . import ir_model
from . import res_users_inherit
from . import hr_attendance_inherit
