# -*- coding: utf-8 -*-
{
    'name': 'Odoo REST API',
    'version': '13.0',
    'category': 'Extra Tools',
    'author': "TENGERSOFT LLC'",
    'support': 'www.tengersoft.mn',
    'license': 'OPL-1',
    'website': 'www.tengersoft.mn',
    'summary': """REST API MODULE""",
    'external_dependencies': {
        'python': ['simplejson'],
    },
    'depends': [
        'base',
        'web',
        'hr_attendance'
    ],
    'data': [
        'data/ir_configparameter_data.xml',
        'data/ir_cron_data.xml',
        'security/security_view.xml',
        'security/ir.model.access.csv',
        'views/ir_model_view.xml',
        'views/hr_attendance_inherit_view.xml',
        'views/res_users_view.xml',
    ],
    'images': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
