# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'MN POSAPI 3.0',
    'version': '1.0.1',
    'category': 'Sales',
    'sequence': 20,
    'website': 'https://www.tengersoft.mn',
    'description': """    
                Mongolian pos vat posapi 3.0connect module
    """,
    'depends': [
                'l10n_mn_product_category',
                'account',
                'base',
                ],
    'data': [
        'security/security_view.xml',
        'security/ir.model.access.csv',
        'views/vat_configure_view.xml',
        'views/vat_data_new_view.xml',
        'views/vat_district_view.xml',
        'views/menu_view.xml',
    ],
    
    'installable': True,
    'application': True,
    'qweb': [],
    'website': 'www.tengersoft.mn',
}