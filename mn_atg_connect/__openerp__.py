# -*- coding: utf-8 -*-
##############################################################################
#
#    
#
#    
#
##############################################################################

{
    "name": "Automatic tank gauging module",
    "version": "1.0",
    "author": "Uuganbayar",
    "category": "sale",
    "description": """This module Automatic tank gauging.""",
    "depends" : [
                 'mn_web_portal',
                 ],
    'update_xml': [
        "security/atg_security_view.xml",
        "security/ir.model.access.csv",
        "views/atg_view.xml",
        "views/menu_view.xml",
                   ],
    'test': [],
    'installable': True,
    'auto_install': False, 
}



