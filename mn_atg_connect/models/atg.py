# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-today OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime, date

import dateutil.parser
from lxml import etree
import time

from odoo.exceptions import RedirectWarning, UserError, ValidationError
from openerp import SUPERUSER_ID
from openerp import tools
from odoo import fields, api, models, tools, _
from openerp.tools.translate import _

import telnetlib
import socket
from . import tls_socket
import argparse
import math
#from tls_socket_lib import tls_socket

class atg_conf(models.Model):
    
    _name = 'atg_conf'
    _order = 'name desc'
    
 

    name = fields.Char(u'IP хаяг',required=True,default='192.168.')
    shts_id = fields.Many2one('shts.register',u'ШТС',required=True)
    port = fields.Char(u'Порт',required=True, default="10001")
    
    
class tank_level(models.Model):
    
    _name = 'tank_level'
    _order = 'tank_date desc'
    
 

    name = fields.Char(u'Өндөр',required=True)
    tank_date = fields.Datetime('Огноо', required=True, default=lambda self: fields.datetime.now())
    temp = fields.Integer(u'Темпратур')
    litr = fields.Float(u'Литр')
    can_id = fields.Many2one('can.register',string=u'Сав')
    density = fields.Float(u'Нягт')
    kg = fields.Float(u'Кг')
    
    def start(self):
        
        #shift_id -g garaas ogch test hiih
        shift_obj = self.env['shift.working.register']
        atg_conf_obj = self.env['atg_conf']
        scal_obj = self.env['scaling.register']
        
        shift_id = shift_obj.search([('id', '=', 118622)])
        conf_id = atg_conf_obj.search([('shts_id', '=', shift_id.shts_id.id)])
        if len(conf_id)>1:
            raise UserError(_(u"ШТС дээр 2 удаа ATG тохиргоо хийгдсэн байна шалгана уу!!!"))
        
        tls = tls_socket.tlsSocket(conf_id.name, int(conf_id.port))
        #tls = tls_socket.tlsSocket("192.168.230.151", 10001) # initial connection
        #response = tls.execute("I10100", 3) # get system status report
        #response = tls.execute("I20I00", 3) # get system status report
        response = tls.execute("I21400", 3) # get system status report
        
        DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
        i = 0
        y = 0
        for data in response.splitlines():
            if i == 0:
               ddate  = data[0:17]
               datetime_obj = datetime.strptime(ddate, '%m/%d/%y %I:%M %p')
               formatted_datetime_str = datetime_obj.strftime('%Y-%m-%d %H:%M:%S')
               print('Ognoo:',data[0:17])
               print('dateee',formatted_datetime_str,fields.datetime.now())
               i+= 1
            if y ==1:
               h = float(data[51:56])
               print('tank',data[5:11],'volume',data[27:33],'mass',data[35:44],'density',round(float(data[51:56])/1000,4),'height',data[57:64],'temp',data[73:78])
               can_obj = self.env['can.register']
               can_id = can_obj.search([('name','=',str(data[5:11]))])
                
               if len(can_id) > 1:
                   raise UserError(_(u'Савны нэр зөв эсэхээ шалгана уу!!! %s')% (data[5:11]))
                    
              
                       
               scal_id = scal_obj.search([('shift_id', '=', shift_id.id),('can_id', '=', can_id.id)])
               if scal_id:
                   
                   
                   scal_id.write({
                                         'name':float(data[57:64])/10,
                                        'self_weight_value':round(float(data[51:56])/1000,4),
                                        'temperature':data[73:78],
                                    })
               
            
            if data[0:12] == "TANK PRODUCT":
               y = 1
               
            
 

