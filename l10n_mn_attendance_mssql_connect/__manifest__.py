# -*- coding: utf-8 -*-
##############################################################################
#
#    ShineERP, Enterprise Management Solution    
#    Copyright (C) 2014-2020 ShineERP Co.,ltd (<http://www.serp.mn>). All Rights Reserved
#
#    Address :
#    Email : info@argun.mn
#    Phone : 976 + 70007333
#
##############################################################################

{

    "name" : "Attendance device Mssql connect",
    "version" : "1.0",
    "author" : "Tengersoft Developer Team",
    "category" : "Localization/HR",
    "description": """
        Mongolian HR device mssql connect
    """,
    
    "depends" : [
                 'hr',
                 'hr_payroll_community',
                 'hr_attendance',
                 ],
    "demo_xml" : [],
    "update_xml" : [
                    'security/ir.model.access.csv',
                    'views/attendance_mssql_connect_view.xml',
                    'views/hr_inherit_view.xml',
                    'views/cron.xml',
                    'views/menu_view.xml',
                    ],
    "auto_install": False,
    "installable": True,
    
}