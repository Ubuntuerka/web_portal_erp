# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models
from odoo import tools, _

_logger = logging.getLogger(__name__)

from datetime import date
from datetime import timedelta
from datetime import datetime
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
import pymssql
from odoo.tools import format_datetime

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class HrAttendance(models.Model):
    _inherit='hr.attendance'
    
    def name_get(self):
        result = []
        for attendance in self:
            if not attendance.check_out:
                checkin_time = format_datetime(self.env, attendance.check_in, dt_format=False)
                result.append((attendance.id, _("%(check_in)s") % {
                    'check_in': checkin_time[10:],
                }))
            else:
                checkin_time = format_datetime(self.env, attendance.check_in, dt_format=False)
                checkout_time = format_datetime(self.env, attendance.check_out, dt_format=False)
                result.append((attendance.id, _("%(check_in)s : %(check_out)s") % {
                    'check_in': checkin_time[10:],
                    'check_out': checkout_time[10:],
                }))
        return result

class HrEmployee(models.Model):
    """Freight Operaton Model."""

    _inherit = 'hr.employee'
    _description = 'Hr employee'
    
    emp_code = fields.Char(string='Emp code')
    

class HrEmployeePublic(models.Model):
    """Freight Operaton Model."""

    _inherit = 'hr.employee.public'
    _description = 'Hr employee'
    
    emp_code = fields.Char(string='Emp code')
    


class AttendanceMssqlConnect(models.Model):
    _name = 'attendance.mssql.connect'
    
    name = fields.Char(string='Config name',required=True)
    company_id = fields.Many2one('res.company',string='Company',required=True,default=lambda self: self.env['res.company']._company_default_get('attendance.config') )
    port = fields.Integer(string='Mssql Port',required=True)
    ip_address = fields.Char(string='Mssql Ip address',required=True)
    username = fields.Char(string='Mssql username',required=True)
    password = fields.Char(string='Mssql password',required=True)
    db_name = fields.Char(string='Mssql database name',required=True)
    download_date = fields.Date(string='Download date',required=True,default=fields.Date.context_today)
    end_date = fields.Date(string='End Date',required=True,default=fields.Date.context_today)
    update_date = fields.Datetime(string='Update date',readonly=True)
    diff_time = fields.Integer(string='Diff time',required=True)
    desc = fields.Text(string='Description',readonly=True)
    
    
    def get_download_data(self):
        attendance_obj = self.env['hr.attendance']
        leave_obj = self.env['hr.leave']
        leave_type_obj = self.env['hr.leave.type']
        leave_type_id = leave_type_obj.search([('new_type','=','work')],limit=1)
        employee_obj = self.env['hr.employee']
        if self.ids!=[]:
            obj = self.browse(self.ids)
            connect = pymssql.connect(database = obj.db_name, user = obj.username, password = obj.password,host = obj.ip_address,port=obj.port)
            cursors = connect.cursor()
        else:
            obj = self.search([])
            connect = pymssql.connect(database = obj.db_name, user = obj.username, password = obj.password,host = obj.ip_address,port=obj.port)
            cursors = connect.cursor()
        desc_data = {}
        desc_data.update({'connect': cursors})
        end_date = datetime.now().strftime("%Y-%m-%d")
        for empl in self.env['hr.employee'].search([('id','!=',1),('emp_code','!=',False)]):
            if empl.emp_code !=False:
                days = []
                idate = datetime.strptime(str(obj.download_date),DATE_FORMAT)
                while idate <= datetime.strptime(str(obj.end_date),DATE_FORMAT):
                    days.append(idate)
                    idate += timedelta(days = 1)
                for day in days:
                    filt_in = datetime.strptime(str(day), '%Y-%m-%d %H:%M:%S').replace(hour=0, minute=0, second=1)
                    filt_out = datetime.strptime(str(day), '%Y-%m-%d %H:%M:%S').replace(hour=23, minute=59, second=59)
                    #print ('VVVVVVVVVVV',int(empl.emp_code),empl.emp_code, day, type(day.date()), type(filt_in), filt_out, type(now_date))
                    cursors.execute("""SELECT TOP 1 us.Badgenumber, att.checktime, att.CHECKTYPE FROM CHECKINOUT as att  LEFT JOIN USERINFO as us on us.USERID = att.USERID WHERE us.Badgenumber = '%s' and convert(varchar, att.checktime, 23) = '%s' ORDER BY att.checktime ASC""" % (empl.emp_code, str(day.date())))
                    row = cursors.fetchall()
                    #print ('XXXXXXXXXX', row)
                    cursors.execute("""SELECT TOP 1 us.Badgenumber, att.checktime, att.CHECKTYPE FROM CHECKINOUT as att  LEFT JOIN USERINFO as us on us.USERID = att.USERID WHERE us.Badgenumber = '%s' and convert(varchar, att.checktime, 23) = '%s' ORDER BY att.checktime DESC""" % (empl.emp_code, str(day.date())))
                    row_last = cursors.fetchall()
                    in_time = ' '
                    out_time = ' '
                    if row and row_last:
                        if row == []:
                            desc_data.update({'atten_data': row})
                        for line in row:
                            #emp_id = employee_obj.search([('emp_code','=',line[0])])
                            #if emp_id:
                            atten_time1 = datetime.strptime(str(line[1]), '%Y-%m-%d %H:%M:%S')
                            atten_time = atten_time1 - timedelta(hours=obj.diff_time)
                            in_time = atten_time
                        for line2 in row_last:
                            #emp_id = employee_obj.search([('emp_code','=',line[0])])
                            #if emp_id:
                            atten_time1 = datetime.strptime(str(line2[1]), '%Y-%m-%d %H:%M:%S')
                            atten_time2 = atten_time1 - timedelta(hours=obj.diff_time)
                            out_time = atten_time2
                        #print ('FFFFFFF', in_time, out_time)
                        event_ids = leave_obj.search([('date_from','=',str(in_time)),('state','=','validate'),('holiday_status_id','=',leave_type_id.id),('employee_id','=',empl.id),('date_to','=',str(out_time))])
                        if event_ids:
                            pass
                        else:
                            event_ids2 = leave_obj.search([('date_from','=',str(in_time)),('state','=','validate'),('holiday_status_id','=',leave_type_id.id),('employee_id','=',empl.id)])
                            if event_ids2:
                                event_ids2.write({
                                    'date_to':out_time
                                    })
                            else:
                                leave_ids = leave_obj.create({
                                    'employee_id':empl.id,
                                    'date_from':in_time,
                                    'date_to':out_time,
                                    'holiday_status_id':leave_type_id.id,
                                    'state':'confirm'
                                    })
                                leave_ids.action_approve()
                        atten_id5 = attendance_obj.search([('check_in','=',str(in_time)),('employee_id','=',empl.id),('check_out','=',str(out_time))])
                        if atten_id5:
                            pass
                        else:
                            atten_id6 = attendance_obj.search([('check_in','=',str(in_time)),('employee_id','=',empl.id)])
                            if atten_id6:
                                atten_id6.write({'check_out':out_time})
                            else:
                                atten_id7 = attendance_obj.search([('check_out','=',str(out_time)),('employee_id','=',empl.id)])
                                if atten_id7:
                                    atten_id7.write({'check_in':in_time})
                                else:
                                    attendance_obj.create({'employee_id':empl.id,
                                                 # 'company_id':empl.company_id.id,
                                                   'check_in':in_time,
                                                   'check_out':out_time})
                                    
                        obj.write({'update_date':fields.Datetime.now(),
                                    'desc':desc_data})
        return True