# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError




class OilBalanceReport(models.Model):
    _name = "oil.balance.report"
    _auto = False

    qty = fields.Integer(u'Тоо хэмжээ', readonly=True)
    shts_id = fields.Many2one('shts.register',u'ШТС')
    product_id = fields.Many2one('product.product',u'Бүтээгдэхүүн')

    _order = 'shts_id desc'

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view oil_balance_report as (
               SELECT id, qty,shts_id, product_id from product_qty
               group by product_id, qty,shts_id, id
            )""")
