# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class ShtsLastPrice(models.Model):
    _name = "shts.last.price"
    _auto = False

    company_id = fields.Many2one('res.company', string='Компани',
                                 default=lambda self: self.env['res.company']._company_default_get('shts.register'),
                                 required=True)
    shts_id = fields.Char(string='ШТС нэр', required=True)
    code = fields.Char(string='ШТС код', required=True)
    product_id = fields.Many2one('product.product', string='Бүтээгдэхүүн', required=True)
    region_id = fields.Many2one('shts.region', string='Бүс')
    price = fields.Float(string='Үнэ', required=True)
    type = fields.Selection([('gasol', u'Шатахуун'),
                             ('oil', u'Тос'),
                             ('service', u'Үйлчилгээ')], string='Төрөл', required=True)

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
        create or replace view shts_last_price as (		
        select r.id, r.company_id, r.name as shts_id, r.region_id, r.code, se.product_id, se.price, se.type
        from shts_register as r
        left join shts_register_line as se on (se.oil_shts_id = r.id or se.shts_id = r.id or se.service_shts_id = r.id)
            )""")
