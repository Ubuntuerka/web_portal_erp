# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class ShtsShiftStateReport(models.Model):
    _name = "shts.shift.state.report"
    _auto = False
    
    
    company_id = fields.Many2one('res.company',string='Company')
    shts_id = fields.Many2one('shts.register',string='Shts')
    code = fields.Char(string='Shts code')
    state = fields.Selection([('draft',u'Ноорог'),
                              ('count',u'Ээлж хаагдсан'),
                              ('confirm',u'Ээлж батлагдсан'),
                              ('cancel',u'Цуцлагдсан')],string='State')
    shift_date = fields.Date(string='Shift date')
    
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view shts_shift_state_report as (
                select shift.shts_id, shts.code, shift.state, shift.shift_date, shift.id, shift.company_id from 
                shift_working_register as shift left join shts_register as shts 
                on shts.id = shift.shts_id 
            )""")