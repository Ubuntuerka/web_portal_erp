# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class shts_uldegdel(models.Model):
    _name = "shts.uldegdel"
    _auto = False
    _order = 'type_price desc'


    qty = fields.Integer(u'Тоо хэмжээ', readonly=True)
    total = fields.Integer(u'Нийт дүн', readonly=True)
    type_price = fields.Char(u'Төрөл',readonly=True)
    code = fields.Char(u'Сер дугаар',readonly=True)
    partner_id1 = fields.Many2one('shts.register',u'Агуулах/ШТС',readonly=True)
    type_id = fields.Many2one('talon.type',u'Ангилал')
    month = fields.Char(u'Сар', readonly=True)
    year = fields.Char(u'Жил', readonly=True)
    date = fields.Char(u'Өдөр', readonly=True)
    buy_date = fields.Date(u'Огноо')

    

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view shts_uldegdel as (
                SELECT t.id, t.name as code, t.type_price, count(t.id)as qty, sum(t.type_price)as total,v.partner_id1,t.type_id,v.buy_date,
          to_char(v.buy_date, 'YYYY-MM') as month,
                      to_char(v.buy_date, 'YYYY') as year, to_char(v.buy_date, 'YYYY-MM-DD') as date
               FROM
                talon_inf t
                LEFT JOIN talon_view v on (t.view_id = v.id)
              WHERE 
                t.state in ('shts','gtba')
                group by t.id, t.name, t.type_price,v.partner_id1,t.type_id,v.buy_date
            )""")
        
class partner_uldegdel(models.Model):
    _name = "partner.uldegdel"
    _auto = False


    qty = fields.Integer(u'Тоо хэмжээ', readonly=True)
    total = fields.Integer(u'Нийт дүн', readonly=True)
    partner_id = fields.Many2one('res.partner',u'Харилцагч',readonly=True)
    category = fields.Char(u'Ангилал', readonly=True)
    code = fields.Char(u'Сер дугаар', readonly=True)
    month = fields.Char(u'Сар', readonly=True)
    day = fields.Char(u'Өдөр', readonly=True)
    warehouse_id = fields.Many2one('shts.register',u'Агуулах/ШТС',readonly=True)
 

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view partner_uldegdel as (
             SELECT t.id, t.name as code,count(t.id)as qty, sum(t.type_price)as total, v.partner_id as partner_id, 
                   p.name as category, to_char(t.doc_date, 'YYYY-MM') as month,to_char(t.doc_date, 'YYYY-MM-DD') as day, v.warehouse_id as warehouse_id
               FROM
                talon_inf t
                LEFT JOIN talon_view v on (t.view_id = v.id)
                LEFT JOIN talon_type p on (v.type_id = p.id)
              WHERE 
                t.state = 'buy' and v.is_internal =  False
                group by t.id, v.partner_id,p.name,v.warehouse_id
            )""")
        
class report_sale(models.Model):
    _name = "report.sale"
    _auto = False
    _order = 'type_price desc'

    category = fields.Char('Ангилал', readonly=True)
    qty = fields.Integer('Тоо хэмжээ', readonly=True)
    total = fields.Integer('Нийт дүн', readonly=True)
    warehouse_id = fields.Many2one('shts.register','ШТС',readonly=True)
    month = fields.Char('Сар', readonly=True)
    year = fields.Char('Жил', readonly=True)
    date = fields.Char('Өдөр', readonly=True)
    code = fields.Char('Сер дугаар', readonly=True)
    type_price = fields.Char('Үнэ', readonly=True)
    doc_date = fields.Date('Огноо')
    partner_id = fields.Many2one('res.partner','Харилцагч',readonly=True)
    company = fields.Selection([('1', u'Шунхлай ХХК'),('2', u'Шунхлай трейдинг'),('3', u'Шунхлай петролиум'),('4', u'Шунхлай говь')], u'Компани',readonly=True)
    company_id = fields.Many2one('res.company',u'Компани')
    

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view report_sale as (
                Select p.name as category, t.type_price, t.name as code, s.warehouse_id, t.doc_date, to_char(t.doc_date, 'YYYY-MM') as month,
                      to_char(t.doc_date, 'YYYY') as year, to_char(t.doc_date, 'YYYY-MM-DD') as date,t.id, 
                      count(t.id)as qty, sum(t.type_price)as total,s.partner_id as partner_id,sw.company_id
               from talon_inf t
                LEFT JOIN talon_sale s on (t.sale_id = s.id)
                LEFT JOIN talon_type p on (t.type_id = p.id)
                LEFT JOIN shts_register sw on (sw.id = s.warehouse_id)
              WHERE 
                t.state = 'sale' and p.active = True
                group by p.name, t.type_price, t.name, s.warehouse_id, t.doc_date,t.id,s.partner_id,sw.company_id

            )""")
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
