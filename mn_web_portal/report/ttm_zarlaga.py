# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class ttm_zarlaga(models.Model):
    _name = "ttm.zarlaga"
    _auto = False
    
    company_id = fields.Many2one('res.company',string='Компани')
    shts_id = fields.Many2one('shts.register', string='ШТС')
    shift_date = fields.Date(string=u'Ээлжийн огноо', required=True, default=fields.Date.context_today)
    eelj = fields.Char(string=u'Ээлж')
    region_id = fields.Many2one('shts.region',string=u'Штс бүс')
    shift_code = fields.Char(string=u'Ээлж код')
    expense_type = fields.Selection([('sale', u'Борлуулалт'),
                                    ('expense', u'Зардал'),
                                    ('bonus', u'Урамшуулал')], string=u'Зардалын төрөл', required=True)
    product_id = fields.Many2one('product.product', string='Бүтээгдэхүүн')
    work_id = fields.Many2one('hr.employee', string=u'Ажилтан', readonly=True)
    price = fields.Float(string='Үнэ', required=True)
    qty = fields.Float(string=u'Тоо')
    niit_dun = fields.Float(string='Нийт үнэ', required=True)
    prod_type = fields.Selection([('gasol',u'Шатахуун'),
                                  ('lpg',u'Хийн түлш'),
                                  ('oil',u'Тос'),
                                  ('service',u'Үйлчилгээ'),
                                  ('akk',u'Аккумулятор'),
                                  ('skytel',u'Скайтел нэгж')],string=u'Бүтээгдэхүүний төрөл')

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view ttm_zarlaga as (
            select s.id,s.company_id, s.shts_id, s.shift_date, s.name as eelj, ttm.expense_type, ttm.product_id, ttm.qty, ttm.price,s.region_id,
            ttm.price*ttm.qty as niit_dun, ttm.work_id,s.name as shift_code, pp.prod_type from shift_working_register s
            left join ttm_sale_register_line as ttm on (ttm.shift_id = s.id) 
            left join product_product as pp on (ttm.product_id = pp.id)
            )""")