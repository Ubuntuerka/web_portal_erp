# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class report_ttgall(models.Model):
    _name = "report_ttgall"
    _auto = False

    warehouse_id = fields.Many2one('shts.register',u'ШТС-ын нэр')
    product_id = fields.Many2one('product.product',u'Бүтээгдэхүүний төрөл')
    cask_line_id = fields.Many2one('bucket.register',u'Торх дугаар')
    fleet_id = fields.Many2one('car.register', u'Машины дугаар')
    p_density_check =  fields.Float(u'Падаан чанарын зөрүү',digits=(4, 4))
    h_density_check =  fields.Float(u'Хэмжилт чанарын зөрүү',digits=(4, 4))
    total_kg =  fields.Float(u'Ирсэн/кг/',digits=(1, 2))
    total_litr =  fields.Float(u'Ирсэн/литр/',digits=(1, 2))
    zuruu_kg =  fields.Float(u'Зөрүү/кг/',digits=(1, 2))
    zuruu_litr =  fields.Float(u'Зөрүү/литр/',digits=(1, 2))
    own_kg =  fields.Float(u'Зөвшөөрөгдөх алдаа',digits=(1, 2))
    niil_hariutsah =  fields.Float(u'Нийлүүлэгч хариуцах/кг/',digits=(1, 2))
    thh =  fields.Float(u'Тхх',digits=(1, 2))
    for_driver =  fields.Float(u'Тээвэрлэгч хариуцах/кг/',digits=(1, 2))
    month =  fields.Char('Сар', readonly=True)
    year =  fields.Char('Жил', readonly=True)
    date =  fields.Char('Өдөр', readonly=True)
    p_kg =  fields.Float(u'Падаан/кг/',digits=(1, 2))
    cask_size =  fields.Float(u'Падаан/литр/',digits=(1, 2))
    irvel_zohih_kg =  fields.Float(u'Ирвэл зохих/кг/',digits=(1, 2))
    irvel_zohih_litr =  fields.Float(u'Ирвэл зохих/литр/',digits=(1, 2))
    density_20 =  fields.Float(u'20 тем хувийн жин',digits=(4, 4))
    h_density =  fields.Float(u'Хэмжилтийн хувийн жин',digits=(4, 4))
    h_litr =  fields.Float(u'Хөөрөлт суулт/литр/',digits=(1, 2))
    h_mm =  fields.Float(u'Хөөрөлт суулт/мм/')
    h_temperature =  fields.Float(u'Хэмжилт температур',digits=(1, 2))
    p_mm =  fields.Integer(u'Падаан хөөрөлт суулт')
    p_liter =  fields.Float(u'Падаан харгалзах/литр/',digits=(1, 2))
    p_temperature =  fields.Float(u'Падаан температур')
    move_factor = fields.Float(u'Падаан хувийн жин',digits=(4, 4))
    #total_kg1 = fields.Float(u'Хэмжилт/кг/',digits=(1, 1))
    #total_litr1 = fields.Float(u'Хэмжилт/литр/',digits=(1, 1))
    hariu_litr = fields.Float(u'Хариу падаан/литр/',digits=(1, 2))
    hariu_kg = fields.Float(u'Хариу падаан/кг/',digits=(1, 2))


    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view report_ttgall as (
              select p.shts_id as warehouse_id,s.product_id,s.car_id as fleet_id,s.torkh_id as cask_line_id, 
              s.p_density_check,s.h_density_check,
              s.total_kg as total_kg, s.total_litr as total_litr, s.zuruu_kg, s.zuruu_litr, s.own_kg, s.drive_diff as for_driver,
       s.irvel_zohih_kg,s.irvel_zohih_litr, s.niil_hariutsah, s.thh, p.rec_date,to_char(p.rec_date, 'YYYY-MM') as month,
       to_char(p.rec_date, 'YYYY') as year, to_char(p.rec_date, 'YYYY-MM-DD') as date,
       s.kg as p_kg, s.litr as p_liter,s.h_litr,s.torkh_size as cask_size,avg(s.density_20),avg(s.temp) as p_temperature,s.self_weight as move_factor,
       s.huurult as p_mm, avg(s.rec_temp) as h_temperature, avg(s.rec_self_weight) as h_density, s.rec_huurult as h_mm, s.hariu_kg, s.hariu_litr, s.id
          from shts_income_line s
          LEFT JOIN shts_income p on (s.shts_income_id = p.id)
          group by  p.shts_id,s.product_id,s.car_id ,s.torkh_id, 
              s.p_density_check,s.h_density_check,
              s.total_kg, s.total_litr, s.zuruu_kg, s.zuruu_litr, s.own_kg, 
       s.irvel_zohih_kg,s.irvel_zohih_litr, s.niil_hariutsah, s.thh, p.rec_date,p.rec_date,
       s.kg, s.litr,s.h_litr,s.torkh_size,s.self_weight,
       s.huurult,  s.rec_huurult, s.hariu_kg, s.hariu_litr, s.id
            )""")
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: