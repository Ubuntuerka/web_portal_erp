# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class product_balance(models.Model):
    _name = "product.balance"
    _auto = False

    company_id = fields.Many2one('res.company', 'Компани',
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'shift.working.register'))
    shts_id = fields.Many2one('shts.register', u'ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id.id)
    shift_date = fields.Date(string=u'Ээлжийн огноо', required=True, default=fields.Date.context_today)
    shift_code = fields.Char(string=u'Ээлжийн код')
    state = fields.Selection([('draft', u'Ноорог'),
                              ('count', u'Ээлж хаагдсан'),
                              ('confirm', u'Батлагдсан'),
                              ('cancel', u'Буцаагдсан'),
                              ], string='Төлөв', default='draft', tracking=True)
    shift = fields.Integer(string=u'Ээлж')
    product_id = fields.Many2one('product.product', string='Бүтээгдэхүүн', required=True)
    can_id = fields.Many2one('can.register', 'Сав', required=True, domain="[('is_oil_can','=',False)]")
    name = fields.Float(string='Өндөр')
    temperature = fields.Char(string='Темпратур')
    self_weight_value = fields.Float(string=u'Хувийн жин', digits=(10,4))
    litr = fields.Float(string=u'литр')
    kg = fields.Float(string=u'кг')
    balance = fields.Float(string='Шугам хоолой литр')
    balance_kg = fields.Float(string='Шугам хоолой кг')
    niit_kg = fields.Float(string='Нийт кг')
    niit_litr = fields.Float(string='Нийт литр')


    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view product_balance as (
            select s.id, s.company_id, s.shts_id, s.shift_date, s.shift, s.state, r.product_id, r.can_id, r.name, 
            r.temperature, r.self_weight_value, r.litr, r.kg, c.balance, 
            r.litr + c.balance as niit_litr, 
            c.balance * r.self_weight_value as balance_kg, 
            r.kg + (c.balance * r.self_weight_value) as niit_kg,
            s.name as shift_code
            from shift_working_register as s 
            left join scaling_register as r on (r.shift_id = s.id)
            left join can_register as c on (r.can_id = c.id)
            where s.state in ('count','confirm')
            group by s.id, r.product_id, r.can_id, r.name, r.temperature, r.self_weight_value, r.litr, r.kg, c.balance,s.name
            )""")
