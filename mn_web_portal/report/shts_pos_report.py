# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class ShtsPosViewReport(models.Model):
    _name = 'shts.pos.view.report'
    _auto = False
    
    company_id = fields.Many2one('res.company',string='Компани')
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    shift_data = fields.Char(string=u'Ээлж')
    shift_code = fields.Char(string=u'Ээлж код')
    date = fields.Date(string=u'Огноо')
    amount = fields.Float(string=u'Дүн')
    payment_id = fields.Many2one('payment.type.register',string=u'Пос')
    
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view shts_pos_view_report as (
                select shift.id, shift.shts_id, shift.shift_date as date, pos.amount, pos.name as payment_id, shift.shift as shift_data, shift.name as shift_code, shift.company_id from 
                shift_working_register as shift left join shts_payment_register as pos 
                on pos.shift_id = shift.id left join payment_type_register as pr on pr.id = pos.name
                where shift.state in ('count','confirm') 
                group by  shift.shift_date,  shift.id, shift.shts_id,pos.amount, pos.name,shift.company_id,shift.shift, shift.name
            )""")
    


