# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class ShzaMbankTransCheckRegReport(models.Model):
    _name = "shza.mbank.trans.check.reg.report"
    _auto = False
    
    
    company_id = fields.Many2one('res.company',string='Company')
    shts_id = fields.Many2one('shts.register',string='Shts')
    shift_date = fields.Date(string='Shift date')
    shift_data = fields.Char(string=u'Ээлж')
    amount = fields.Float(string='Price')
    
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view shza_mbank_trans_check_reg_report as (
                select shift.shts_id, shift.shift_date, shift.id, cpi.amount,shift.shift as shift_data from 
                shift_working_register as shift left join center_payment_income as cpi 
                on cpi.shift_id = shift.id where cpi.type='mobile'
            )""")