# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class ShtsIncomeReport(models.Model):
    _name = 'shts.income.report'
    
    shts_id = fields.Many2one('shts.register',u'ШТС-ын нэр')
    product_id = fields.Many2one('product.product',u'Бүтээгдэхүүний төрөл')
    can_id = fields.Many2one('can.register',u'Торх дугаар',domain="[('is_oil_can','=',False)]")
    car_id = fields.Many2one('car.register', u'Машины дугаар')
    date = fields.Date(u'Огноо')
    p_temp = fields.Float(u'Падаан температур')
    p_density = fields.Float(u'Падаан хувийн жин',digits=(4, 4))
    p_liter = fields.Float(u'Падаан /литр/')
    p_kg = fields.Float(u'Падаан /кг/')
    p_mm = fields.Integer(u'Падааны хөөрөлт суулт')
    h_temp = fields.Float(u'Хэмжилт температур')
    h_density = fields.Float(u'Хэмжилтийн хувийн жин',digits=(4, 4))
    h_liter = fields.Float(u'Хэмжилт /литр/')
    h_kg = fields.Float(u'Хэмжилт /кг/')
    h_mm = fields.Integer(u'Хөөрөлт суулт')
    zuruu_kg = fields.Float(u'Зөрүү /кг/')
    driver_id = fields.Float(u'Тээвэрлэгч хариуцах /кг/')
   # income_id = fields.Many2one('shts.income.register','Орлого')
    
