# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class KassUldegdel(models.Model):
    _name = "kass.uldegdel"
    _auto = False

    qty = fields.Integer(u'Тоо хэмжээ', readonly=True)
    total = fields.Integer(u'Нийт дүн', readonly=True)
    type_price = fields.Char(u'Төрөл',readonly=True)
    type_id = fields.Many2one('talon.type',u'Ангилал',readonly=True)

    _order = 'type_price desc'

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view kass_uldegdel as (
               SELECT id, type_price, count(id)as qty, sum(type_price)as total, type_id from talon_inf
               WHERE state = 'print'
               group by type_price,type_id,id
            )""")
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
