# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class CenterPaymentIncomeReportGraph(models.Model):
    _name = 'center.payment.income.report.graph'
    _auto = False

    create_date = fields.Datetime(string='Үүсгэсэн огноо')
    bank_name = fields.Selection([('shu_card', u'Шунхлай карт'),
                                  ('hh_bank', u'ХХ банк'),
                                  ('haan_bank', u'Хаан банк'),
                                  ('has_bank', u'Хас банк'),
                                  ('golomt_bank', u'Голомт банк'),
                                  ('tur_bank', u'Төрийн банк'),
                                  ('OU_bank', u'ОУ-ын карт'),
                                  ('capital_bank', u'Капитал банк'),
                                  ('UB_bank', u'Улаанбаатар банк'),
                                  ('order_bank', u'Бусад банк')], string='Банкны нэр', required=True)
    reason = fields.Selection([('pos_not', u'Посгүй'),
                               ('server', u'Сүлжээ унасан'),
                               ('gemtsen', u'Карт гэмтсэн'),
                               ('paymenting', u'Эргэн төлөлт'),
                               ('forget', u'Пин код мартсан'),
                               ('noting', u'Пос машин ажиллахгүй'),
                               ('order', u'Бусад')], string='Шалтгаан', required=True)
    shts_id = fields.Many2one('shts.register', u'Хүсэлт гаргасан газар', required=True,
                              default=lambda self: self.env.user.warehouse_id.id)
    #card_name = fields.Char(string='Card name')
    amount = fields.Float(string='Дүн', required=True)

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute(""" create or replace view center_payment_income_report_graph as (
        select id, create_date, bank_name, reason, amount, shts_id from center_payment_income
        group by id, create_date, bank_name, reason, amount, shts_id 
        )""")
