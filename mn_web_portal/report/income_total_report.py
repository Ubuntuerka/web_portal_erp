# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class IncomeTotalReport(models.Model):
    _name = "income.total.report"
    _auto = False

    company_id = fields.Many2one('res.company', string='Компани',
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'income.total.report'),
                                 required=True)
    shts_id = fields.Many2one('shts.register', string='ШТС')
    shift_date = fields.Date(string='Огноо')
    product_id = fields.Many2one('product.product', string=u'Бүтээгдэхүүн')
    litr = fields.Float(string=u' Илүү/ дутуу литр', digits=(10, 2))
    kg = fields.Float(string=u'Илүү/ дутуу кг', digits=(10, 2))
    income_litr = fields.Float(string=u' Орлого литр', digits=(10, 2))
    income_kg = fields.Float(string=u' Орлого кг', digits=(10, 2))
    state = fields.Selection([('draft', u'Ноорог'),
                              ('send', u'Илгээсэн'),
                              ('confirm', u'Баталсан'),
                              ('check', u'Хянасан')], string=u'Төлөв', default='draft', tracking=True)
    report_zoruu_kg = fields.Float(string='кг')
    report_zoruu_litr = fields.Float(string='литр')
    exp_litr = fields.Float(string=u'Зарлага литр', digits=(10, 2))
    exp_kg = fields.Float(string=u'Зарлага кг', digits=(10, 2))
    hewiin_litr = fields.Float(string=u'Х/хорогдол литр', digits=(10, 2))
    hewiin_kg = fields.Float(string=u'Х/хорогдол кг', digits=(10, 2))
    sav_tsew_litr = fields.Float(string=u'СЦА литр', digits=(10, 2))
    sav_tsew_kg = fields.Float(string=u'СЦА кг', digits=(10, 2))

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
        create or replace view income_total_report as (
        select s.id, s.company_id, s.shts_id, s.start_date as shift_date, l.product_id, l.iluu_litr + l.dutuu_litr as litr, l.iluu_kg + l.dutuu_kg as kg, 
        l.income_litr,  l.income_kg, l.exp_litr, l.exp_kg, l.hewiin_litr, l.hewiin_kg, l.sav_tsew_litr, l.sav_tsew_kg, s.state
        from act_of_calculation_register as s 
        left join act_of_calculation_register_line as l on l.calculation_id = s.id
        )""")
