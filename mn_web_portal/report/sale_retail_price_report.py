# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class SaleRetailPriceReport(models.Model):
    _name = "sale.retail.price.report"
    _auto = False
    
    
    company_id = fields.Many2one('res.company',string='Company')
    shts_id = fields.Many2one('shts.register',string='Shts')
    shift_date = fields.Date(string='Shift date')
    total_amount = fields.Float(string='Retail price')
    
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view sale_retail_price_report as (
                select shift.shts_id, shift.shift_date, shift.id, spl.total_amount from 
                shift_working_register as shift left join shift_product_line as spl 
                on spl.shift_id = shift.id 
            )""")