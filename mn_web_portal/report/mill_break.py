# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class shts_last_price(models.Model):
    _name = "mile.break"
    _auto = False

    company_id = fields.Many2one('res.company', 'Компани',
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'mile.break'))
    shts_id = fields.Many2one('shts.register', u'ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id.id)
    shift_date = fields.Date(string=u'Ээлжийн огноо', required=True, default=fields.Date.context_today)
    eelj = fields.Char(string=u'Ээлж')
    can_id = fields.Many2one('can.register', string='Сав', domain="[('is_oil_can','=',False)]")
    name = fields.Char(string='Хошуу')
    product_id = fields.Many2one('product.product', string='Бүтээгдэхүүн')
    first_mile = fields.Float(string='Эхний милл')
    end_mile = fields.Float(string='Эцсийн милл')
    guilt = fields.Float(string='Гүйлт')
    zalin = fields.Float(string='Залин')
    state = fields.Selection([('draft', u'Ноорог'),
                              ('count', u'Ээлж хаагдсан'),
                              ('confirm', u'Батлагдсан'),
                              ('cancel', u'Буцаагдсан'),
                              ], string='Төлөв', default='draft', tracking=True)
    expense_litr = fields.Float(string='Зарлага литр')



    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view mile_break as (
            select s. id, s.company_id, s.shts_id, s.shift_date, s.name as eelj, s.state, m.diff_mile, m.end_mile - m.first_mile - m.zalin as expense_litr, m.can_id, m.name, m.product_id, 
            m.first_mile, m.end_mile, m.zalin, m.end_mile - m.first_mile as guilt
            from shift_working_register as s
            left join mile_target_register_line as m on (m.shift_id = s.id)
            where s.state in ('count','confirm') 
            )""")
