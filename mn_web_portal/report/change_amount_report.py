# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class ChangeAmountReportGraph(models.Model):
    _name = 'change.amount.report.graph'
    _auto = False

    name = fields.Char(string='Change price number',
                       default=lambda self: self.env['ir.sequence'].next_by_code('price.register'))
    shts_id = fields.Many2one('shts.register', 'Shts')
    start_date = fields.Datetime(string='Start date', default=fields.Date.context_today, required=True)
    reg_user_id = fields.Many2one('res.users', string='Register user', default=lambda self: self.env.user)
    type = fields.Selection([('gasol', 'Gasol'),  # Шатахуун
                             ('oil', 'Oil'),  # Тос
                             ('service', 'Service')], string='Type')  # Үйлчилгээ
    shift_date = fields.Date(string='Shift date', default=fields.Date.context_today)
    state = fields.Selection([('draft', 'Draft'),  # Ноорог
                              ('send', 'Send'),  # Илгээгдсэн
                              ('confirm', 'Confirm'),  # Батлагдсан
                              ('done', 'Done'),  # Дууссан
                              ('return', 'Return'),
                              ('cancel', 'Cancel')], string='State', default='draft')  # Буцаагдсан #Төлөв
    product_id = fields.Many2one('product.product', string='Product')  # Бүтээгдэхүүн
    old_price = fields.Float(string='Old Price', required=True, readonly=True)
    new_price = fields.Float(string='New Price', required=True)
    break_id = fields.Many2one('mile.target.register', string=u'Хошуу')
    mile_target_amount = fields.Float(string=u'Үнэ сольсон милл')
    first_mile = fields.Float(string=u'Эхний милл')
    shift = fields.Float(string=u'Ээлж')
    diff_mile = fields.Float(string=u'Зөрүү')

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
        create or replace view change_amount_report_graph as (
        select l.id, s.name, s.shts_id, s.reg_user_id, s.start_date, s.type, s.state, s.shift_date,
l.old_price, l.new_price, r.shift,r.product_id, r.break_id, r.first_mile, r.mile_target_amount,
r.mile_target_amount - r.first_mile as diff_mile
from price_register_shts as s
left join price_register_shts_line as l on (l.price_change_id = s.id)
left join price_gasol_register_shts as r on (r.price_change_id = s.id)
where s.state = 'done' 
group by l.id, s.name, s.shts_id, s.reg_user_id, s.start_date, s.type, s.state, s.shift_date,
l.old_price, l.new_price, r.shift,r.product_id, r.break_id, r.first_mile, r.mile_target_amount
        )""")
