# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
DATE_FORMAT = "%Y-%m-%d"

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

class ScalingRegister(models.Model):
    _name = 'scaling.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company','Company'
                                 ,default=lambda self: self.env['res.company']._company_default_get('scaling.register'))
    name = fields.Float(string='Height',required=True,digits=(10,2))
    reg_date = fields.Date(string='Register date',required=True)
    terminal_id = fields.Many2one('shts.terminal',string=u'Түгээгүүр',required=True)
    reg_user_id = fields.Many2one('res.users',string='Register user',required=True)
    product_id = fields.Many2one('product.product',string='Product',required=True)
    temperature = fields.Char(string='Temperature',required=True)
    shts_code = fields.Char(string=u'ШТС код')
    product_code = fields.Char(string=u'Бүтээгдэхүүний код')
    partner_code = fields.Char(string=u'Харилцагч код')
    can_code = fields.Char(string=u'Савны код')
    can_id = fields.Many2one('can.register','Can',required=True,domain="[('is_oil_can','=',False)]")
    from_company_code = fields.Char(string=u'Илгээх компани код')
    to_company_code = fields.Char(string=u'Хүлээн авах компани код')
    from_type = fields.Selection([('warehouse',u'Агуулах')],string='Гарах төрөл',default='warehouse')
    to_type = fields.Selection([('warehouse',u'Агуулах')],string='Хүрэх төрөл',default='warehouse')
    order_type = fields.Char(string=u'Захиалгын төрөл',default='service (manual)')
    qty = fields.Float(string='Litr',digits=(10,2))
    litr = fields.Float(string=u'Эцсийн үлдэгдэл литр',required=True,compute="calculate_litr",digits=(10,2))
    kg = fields.Float(string=u'Эцсийн үлдэгдэл кг',required=True,compute='calculate_kg',digits=(10,2))
    total_litr = fields.Float(string=u'Нийт зарлага литр',compute="calculate_total_litr",digits=(10,2))
    total_kg = fields.Float(string=u'Нийт зарлага кг',compute="calculate_total_kg",digits=(10,2))
    self_weight = fields.Float(string='',required=True,size=4,default=0)
    self_weight_value = fields.Float(string=u'Хувийн жин',required=True,size=5,default='0.0000',digits=(10,4))
    shift_id = fields.Many2one('shift.working.register',string='Shift register')
    total_amount = fields.Float(string=u'Нийт гүйлт',compute="action_total_compute")
    dhj_amount = fields.Float(string='ДХЖ',compute="action_total_compute",digits=(10,4))
    
    litr_n = fields.Float(string=u'Литр',related='litr',store=True)
    kg_n = fields.Float(string=u'Литр',related='kg',store=True)
    
    def write(self,vals):
        if 'self_weight_value' in vals:
            if vals.get('self_weight_value') > 1.0:
                raise UserError(_(u"Хувийн жин 1-ээс их байж болохгүйг анхаарна уу !!!"))
        
        scale =  super(ScalingRegister, self).write(vals)
        return scale
        
        
    
    def action_total_compute(self):
        for line in self:
            total = 0.0
            dhj_amount = 0.0
            shift_obj = self.env['shift.working.register']
            income_obj = self.env['shts.income']
            for li in line.shift_id.mile_target_ids:
                if line.can_id.id==li.can_id.id:
                    if li.product_id.id == line.product_id.id:
                        total +=li.diff_mile
            #shts_income_
            income_id = income_obj.search([('shts_id','=',line.shift_id.shts_id.id),('shift_date','=',line.shift_id.shift_date),('state','=','receive'),('shift','=',line.shift_id.shift)])
            date1 = line.shift_id.shift_date
            date11 = datetime.strptime(str(line.shift_id.shift_date), DATE_FORMAT)
            pdate = date11-timedelta(days=1)
            first_litr = 0.0    
            first_kg = 0.0
            if line.shift_id.shift == 2:
                #if line.shift_id.shift == 1:
                #    pre_shift = shift_obj.search([('shts_id','=',line.shift_id.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift_date asc")
                #else:
                pre_shift = shift_obj.search([('shts_id','=',line.shift_id.shts_id.id),('shift_date','=',date1),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift_date asc")
                for pr in pre_shift:
                    for prl in pr.scale_ids:
                        if prl.product_id.id == line.product_id.id and prl.can_id.id==line.can_id.id:
                            first_litr += prl.litr 
                            first_kg +=prl.kg
            elif line.shift_id.shift==1:
                if line.shift_id.shts_id.work_time==2:
                    pre_shift = shift_obj.search([('shts_id','=',line.shift_id.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                else:
                    pre_shift = shift_obj.search([('shts_id','=',line.shift_id.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                    if not pre_shift:
                        pre_shift = shift_obj.search([('shts_id','=',line.shift_id.shts_id.id),('shift_date','=',pdate),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                for pr in pre_shift:
                    for prl in pr.scale_ids:
                        if prl.product_id.id == line.product_id.id and prl.can_id.id==line.can_id.id:
                            first_litr += prl.litr 
                            first_kg +=prl.kg
            #pre_shift_id = shift_obj.search([('shts_id','=',line.shift_id.shts_id.id),('shift','=',shift),('shift_date','<',line.shift_id.shift_date)],limit=1,order="shift_date desc")
            
            
            income_litr = 0.0
            income_kg = 0.0
            for inc in income_id:
                for inline in inc.line_ids:
                    if line.product_id.id == inline.product_id.id:
                        if line.can_id.id == inline.can_id.id:
                            income_litr +=inline.total_litr
                            income_kg +=inline.total_kg
            
            if first_kg > 0.0 and line.kg > 0.0 and income_litr >0.0 and income_kg >0.0:
                dhj_amount = (first_kg+line.kg+income_kg)/(first_litr+line.litr+income_litr)
            elif first_kg > 0.0 and line.kg >0.0:
                dhj_amount = (first_kg+line.kg)/(first_litr+line.litr)
            elif first_kg == 0.0 and line.kg >0.0 and income_litr >0.0 and income_kg >0.0:
                dhj_amount = (line.kg+income_kg)/(line.litr+income_litr)
            elif line.kg > 0.0 and line.litr >0.0:
                dhj_amount = (line.kg)/(line.litr)
            #elif line.kg > 0.0:
            #    dhj_amount = (line.kg)/(line.litr)
           # else:
           #     dhj_amount = (first_litr+line.litr)/(first_kg+line.kg)
            line.dhj_amount = dhj_amount
            line.total_amount = total
    
    @api.onchange('name')
    def onchange_name(self):
        for line in self:
            line.calculate_litr()
            
    @api.onchange('self_weight_value')
    def onchange_self_weight(self):
        for line in self:
            line.calculate_kg()
            
    def calculate_total_litr(self):
        for line in self:
            total_litr = 0.0
            for li in line.shift_id.mile_target_ids:
                if line.can_id.id==li.can_id.id:
                    total_litr +=li.expense_litr
            line.total_litr = total_litr
            
    def calculate_total_kg(self):
        for line in self:
            total_litr = 0.0
            for li in line.shift_id.mile_target_ids:
                if line.can_id.id==li.can_id.id:
                    total_litr +=li.expense_litr
            if total_litr >0.0:
                line.total_kg = total_litr*line.dhj_amount    
            else:
               line.total_kg = 0 
    @api.depends('litr','self_weight_value')        
    def calculate_kg(self):
        for line in self:
            kg = 0.0
            if line.litr > 0.0:
                kg = round(line.litr * line.self_weight_value,2)
            line.kg = kg

    @api.depends('name')
    def calculate_litr(self):
        for line in self:
            amount = 0.0
            ondor_too = int(line.name/10)*10
            buhel_ondor = int(line.name)
            tootsoo = line.name-buhel_ondor
            zoruu = int(line.name) - ondor_too
            too = 0
            aa = 0
            aaa = 0
            if zoruu == 0:
                too = ondor_too - 10
                for tab in line.can_id.tablits_id:
                    for chart in tab.line_ids:
                       if line.shift_id.shift_date !=False:
                           if str(chart.tablits_id.start_date) <= str(line.shift_id.shift_date) <= str(chart.tablits_id.end_date) :
                               if too == chart.name:
                                  check_value = 1
                                  if tootsoo > 0:
                                       aa = too + 10
                                       aaa = chart.ten_mm
                                  else:
                                      amount = chart.ten_mm
                               
                               elif aa == chart.name:
                                  amount = aaa + (chart.one_mm - aaa)*tootsoo
                              
                           #else:
                           #     raise UserError(_(u"Таблицын хугацаагаа шалгана уу !!!"))
                       else:
                           amount = 0
                   #     
            else:
                if buhel_ondor == 10:
                   too = ondor_too - 10
                else:
                   
                   too = ondor_too
                for tab in line.can_id.tablits_id:
                    for chart in tab.line_ids:
                        if line.shift_id.shift_date !=False:
                            if str(chart.tablits_id.start_date) <= str(line.shift_id.shift_date) <= str(chart.tablits_id.end_date):
                                if too ==chart.name:
                                   check_value = 1
                                   if zoruu == 1:
                                      if tootsoo > 0:
                                           amount = chart.one_mm + (chart.two_mm - chart.one_mm)*tootsoo
                                      else: 
                                          amount = chart.one_mm              
                                   elif zoruu ==2:
                                       if tootsoo > 0:
                                           amount = chart.two_mm + (chart.three_mm - chart.two_mm)*tootsoo
                                       else: 
                                          amount = chart.two_mm 
                                   elif zoruu ==3:
                                       if tootsoo > 0:
                                           amount = chart.three_mm + (chart.four_mm - chart.three_mm)*tootsoo
                                       else: 
                                          amount = chart.three_mm 
                                   elif zoruu ==4:
                                       if tootsoo > 0:
                                           amount = chart.four_mm + (chart.five_mm - chart.four_mm)*tootsoo
                                       else: 
                                          amount = chart.four_mm   
                                   elif zoruu ==5:
                                       if tootsoo > 0:
                                           amount = chart.five_mm + (chart.six_mm - chart.five_mm)*tootsoo
                                       else: 
                                          amount = chart.five_mm
                                   elif zoruu ==6:
                                       if tootsoo > 0:
                                           amount = chart.six_mm + (chart.seven_mm - chart.six_mm)*tootsoo
                                       else: 
                                          amount = chart.six_mm
                                   elif zoruu ==7:
                                       if tootsoo > 0:
                                           amount = chart.seven_mm + (chart.eight_mm - chart.seven_mm)*tootsoo
                                       else: 
                                          amount = chart.seven_mm
                                   elif zoruu ==8:
                                       if tootsoo > 0:
                                           amount = chart.eight_mm + (chart.nine_mm - chart.eight_mm)*tootsoo
                                       else: 
                                          amount = chart.eight_mm
                                   elif zoruu ==9:
                                       if tootsoo > 0:
                                           amount = chart.nine_mm + (chart.ten_mm - chart.nine_mm)*tootsoo
                                       else: 
                                          amount = chart.nine_mm   
                        #else:
                        #    raise UserError(_(u"Таблицын хугацаагаа шалгана уу !!!"))
                
                
            line.litr = amount
    