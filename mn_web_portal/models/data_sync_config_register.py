# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class DataSyncConfigRegister(models.Model):
    _name = 'data.sync.config.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env['res.company']._company_default_get('data.sync.config.register'))
    name = fields.Char(string='Database name')
    user_id = fields.Char(string='User')
    password = fields.Char(string='Password')
    ip_address = fields.Char(string='Ip address')
    
    
    def action_data_sync(self):
        
        
        return True