# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date


class ShtsTerminal(models.Model):
    _name = 'shts.terminal'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string=u'Компани',
                                 default=lambda self: self.env['res.company']._company_default_get('shts.terminal'),required=True)
    name = fields.Char(string=u'Түгээгүүрийн нэр',required=True)