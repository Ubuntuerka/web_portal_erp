# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date


class PayementTypeRegister(models.Model):
    _name = 'payment.type.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('payment.type.register'),required=True)
    name = fields.Char(string='Payment type',required=True)
    discount = fields.Float(string=u'Хөнгөлөлт %')
    type = fields.Selection([('cash',u'Бэлэн'),
                             ('bank_card',u'Карт'),
                             ('loan',u'Зээл'),
                             ('talon',u'Талон'),
                             ('tov_card',u'Төвд карт'),
                             ('discount',u'Хөнгөлөлт'),
                             ('mobile',u'Шилжүүлэг')],string=u'Төрөл',required=True)
    