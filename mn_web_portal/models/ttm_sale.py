# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class TtmSaleRegister(models.Model):
    _name = 'ttm.sale.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company'
                                 ,default=lambda self: self.env['res.company']._company_default_get('ttm.sale.register'))
    
    reg_user_id = fields.Many2one('res.users',string='Regiser user',default=lambda self: self.env.user)
    total_expense = fields.Float(string='Expense')
    total_bonus = fields.Float(string='Bonus')
    total_sale = fields.Float(string='Total sale')
    date = fields.Date(string='Date', default=fields.Date.context_today)
    shts_id = fields.Many2one('shts.register','Shts')
    state = fields.Selection([('draft','Draft'),
                              ('confirm','Confirm')],string='State',default='draft')
    line_ids = fields.One2many('ttm.sale.register.line','expense_id',string='Expense line')
    
    def action_confirm(self):
        self.write({'state':'confirm'})
    
class TtmSaleRegisterLineService(models.Model):
    _name = 'ttm.sale.register.line.service'
    
    
    
    company_id = fields.Many2one('res.company',string='Компани'
                                 ,default=lambda self: self.env['res.company']._company_default_get('ttm.sale.register.line'))
    product_id = fields.Many2one('product.product','Бүтээгдэхүүн',required=True, domain=[('prod_type', 'in', ['service'])],)
    is_infor = fields.Boolean(string=u'ЛН Төлөв',default=False)
    name = fields.Char(string='Код')
    uom_id = fields.Many2one('uom.uom',string='хэмжих нэгж',required=True)
    qty = fields.Float(string='Тоо',required=True,default=1)
    price = fields.Float(string='Үнэ',required=True)
    litr = fields.Float(string='Litr',default=1)
    kg = fields.Float(string='Kg', default=1)
    can_id = fields.Many2one('can.register',string=u'Сав',domain="[('is_oil_can','=',True)]")
    total_price = fields.Float(string='Нийт үнэ',compute="action_total_price",required=True)
    partner_id = fields.Many2one('res.partner',string=u'Харилцагч')
    expense_id = fields.Many2one('ttm.sale.register',string='Expense')
    work_id = fields.Many2one('hr.employee',string=u'Ажилтан')
    new_work_id = fields.Many2one('res.partner',string=u'Ажилтан')
    expense_type_id = fields.Many2one('expense.type',string=u'Зардлын төрөл')
    expense_type = fields.Selection([('sale',u'Борлуулалт'),
                                     ('expense',u'Зардал'),
                                     ('bonus',u'Урамшуулал')],string='Зардалын төрөл',required=True,default='sale')
    
    type = fields.Selection([('ttm','TTM'),
                             ('tvt','TVT')],string='Төрөл')
    shift_id = fields.Many2one('shift.working.register',string='Shift register')
    
    def write(self,vals):
        for line in self:
            if 'qty' in vals:
                if vals['qty']==0.0:
                    raise UserError(_(u"Та бүтээгдэхүүний тоогоо оруулана уу!"))
        res = super(TtmSaleRegisterLineService, self.with_context()).write(vals)
        return res
    
    @api.onchange('name','product_id')
    def onchange_product_id(self):
        shts_obj = self.env['shts.register']
        for line in self:
            price = 0.0
            product = self.env['product.product'].search([('id','=',line.product_id.id)])
            for sht in line.shift_id.shts_id.service_line_ids:
                if product.id == sht.product_id.id:
                    price = sht.price
            line.name = line.product_id.default_code
            line.price = price
            line.uom_id = product.uom_id.id
            line.product_id = product.id
            line.total_price = line.qty * line.price
            line.action_total_price()
    
    def action_total_price(self):
        for line in self:
            price = 0.0
            #if line.qty==0.0:
            #    raise UserError(_(u"Та тоогоо оруулана уу!"))
            if line.qty >0.0 and line.price > 0.0:
                price = line.qty * line.price
            line.total_price = price
    
class TtmSaleRegisterLine(models.Model):
    _name = 'ttm.sale.register.line'
    
    @api.model
    def _product_id(self):
        product = []
        shts_obj = self.env['shts.register']
        shts_id = shts_obj.browse(self.env.user.warehouse_id.id)
        for line in shts_id.oil_line_ids:
            product.append(line.product_id.id)
        return [('id', 'in', product)]
    
    
    company_id = fields.Many2one('res.company',string='Company'
                                 ,default=lambda self: self.env['res.company']._company_default_get('ttm.sale.register.line'))
    is_infor = fields.Boolean(string=u'ЛН Төлөв',default=False)
    product_id = fields.Many2one('product.product','Product',required=True, domain=_product_id,)
    product_code = fields.Char(string=u'Бүтээгдэхүүн код')
    name = fields.Char(string='Code')
    uom_id = fields.Many2one('uom.uom',string='Uom',required=True)
    qty = fields.Float(string='Quantity',required=True)
    litr = fields.Float(string='Litr',default=1)
    kg = fields.Float(string='Kg', default=1)
    price = fields.Float(string='Price',required=True)
    work_id = fields.Many2one('hr.employee',string=u'Ажилтан')
    new_work_id = fields.Many2one('res.partner',string=u'Ажилтан')
    total_price = fields.Float(string='Total price',compute="action_total_price",required=True)
    expense_id = fields.Many2one('ttm.sale.register',string='Expense')
    transaction_type = fields.Char(string=u'Гүйлгээний төрөл',default='Issue')
    expense_type_id = fields.Many2one('expense.type',string=u'Зардлын төрөл')
    expense_type = fields.Selection([('sale',u'Борлуулалт'),
                                     ('expense',u'Зардал'),
                                     ('bonus',u'Урамшуулал'),
                                     ('loan',u'Зээл')],string='Expense type',required=True,default='sale')
    shts_code = fields.Char(string=u'ШТС код',compute="action_shts_id")
    can_id = fields.Many2one('can.register',string=u'Сав',domain="[('is_oil_can','=',True)]")
    partner_id = fields.Many2one('res.partner',string=u'Харилцагч')
    can_code = fields.Char(string=u'Савны код')
    reg_date = fields.Date(string=u'Ээлжийн огноо')
    partner_code = fields.Char(string=u'Харилцагч код',compute="action_product")
    from_company_code = fields.Char(string=u'Илгээх компани код')
    to_company_code = fields.Char(string=u'Хүлээн авах компани код')
    from_type = fields.Selection([('warehouse',u'Агуулах')],string='Гарах төрөл',default='warehouse')
    to_type = fields.Selection([('warehouse',u'Агуулах')],string='Хүрэх төрөл',default='warehouse')
    order_type = fields.Char(string=u'Захиалгын төрөл',default='service (manual)')
    
    type = fields.Selection([('ttm','TTM'),
                             ('tvt','TVT')],string='Type')
    shift_id = fields.Many2one('shift.working.register',string='Shift register')
    product = fields.Char(string=u'Бүтээгдэхүүн',compute="action_product")
    itemId = fields.Char(string=u'Санхүүгийн код',compute="action_product")
    petrol_station = fields.Char(string=u'ШТС',compute="action_shts_id")
    CustomerName = fields.Char(string=u'Харилцагчийн нэр',compute="action_product")
    
    
     
    def write(self,vals):
        qty_obj = self.env['product.qty']
        for line in self:
            if 'qty' in vals or 'product_id' in vals:
                qty_data_ids = qty_obj.search([('ttm_sale','=',line.id)])
                for li in qty_data_ids:
                    li.unlink()
            
                 
        res = super(TtmSaleRegisterLine, self.with_context()).write(vals)
        return res
    
    @api.onchange('product_id')
    def onchange_product_id(self):
        shts_obj = self.env['shts.register']
        for line in self:
            price = 0.0
            for sht in line.shift_id.shts_id.oil_line_ids:
                if line.product_id.id == sht.product_id.id:
                    price = sht.price
            line.price = price
            line.uom_id = line.product_id.uom_id.id
            line.name = line.product_id.default_code
            line.total_price = line.qty * line.price
            line.action_total_price()
    
    def action_total_price(self):
        for line in self:
            price = 0.0
            
            if line.qty >0.0 and line.price > 0.0:
                price = line.qty * line.price
            line.total_price = price
            
            
    @api.onchange('expense_type_id')
    def onchange_expense_type_id(self):
        
        for line in self:
            if line.expense_type_id.type == 'loan':
               line.expense_type = 'loan'
            else:
                line.expense_type = 'sale'
    
    def action_product(self):
        for line in self:
            line.product = line.product_id.name
            line.partner_code = line.partner_id.vat
            line.itemId = line.product_id.fin_code
            line.CustomerName = line.partner_id.name
            
    def action_shts_id(self):
        for line in self:
            line.petrol_station = line.shift_id.shts_id.name
            line.shts_code = line.shift_id.shts_id.shts_code
           
            
    

    
    
    
    
    