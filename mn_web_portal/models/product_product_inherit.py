# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.osv import expression

class UomUom(models.Model):
    _inherit = 'uom.uom'
    
    local_code = fields.Char(string='Local code')

class PriceHistory(models.Model):
    _name = 'price.history'
    
    company_id = fields.Many2one('res.company',string=u'Компани')
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн')
    pre_price = fields.Float(string=u'Хуучин үнэ')
    price = fields.Float(string=u'Үнэ')
    date = fields.Datetime(string=u'Өөрчлөгдсөн огноо')
    

class ProductQty(models.Model):
    _name = 'product.qty'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string=u'Компани'
                                 ,default=lambda self: self.env['res.company']._company_default_get('product.qty'),required=True)
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн')
    qty = fields.Float(string=u'Үлдэгдэл')
    litr_qty = fields.Float(string=u'Литр үлдэгдэл')
    kg_qty = fields.Float(string=u'Кг үлдэгдэл')
    reg_date = fields.Date(string=u'Огноо')
    type = fields.Selection([('in',u'Орлого'),
                             ('out',u'Зарлага')],string=u'Төрөл')
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    ttm_sale = fields.Many2one('ttm.sale.register.line',string='TTM sale', ondelete='cascade')
    income_id = fields.Many2one('ttm.income.register',string='Income', ondelete='cascade')
    

class ProductProduct(models.Model):
    _inherit = 'product.product'
    
    
    
    type = fields.Selection([
        ('consu', 'Consumable'),
        ('service', 'Service')], string='Product Type', default='service', required=True,
        help='A storable product is a product for which you manage stock. The Inventory app has to be installed.\n'
             'A consumable product is a product for which stock is not managed.\n'
             'A service is a non-material product you provide.')
    prod_type = fields.Selection([('gasol',u'Шатахуун'),
                                  ('lpg',u'Хийн түлш'),
                                  ('oil',u'Тос'),
                                  ('service',u'Үйлчилгээ'),
                                  ('akk',u'Аккумулятор'),
                                  ('skytel',u'Скайтел нэгж')],string='Type')
    gasol_type = fields.Selection([('2',u'ДТ Өвөл'),
                                   ('3',u'ДТ Зун'),
                                   ('1',u'Авто бинзен')
                                   ],string=u'Төрөл')
    shts_id = fields.One2many('shts.register','pro_id',string=u'ШТС')
    product_qty = fields.Float(
        u'Үлдэгдэл', compute='_compute_product_qty')
    product_litr_qty = fields.Float(
        u'Лирт үлдэгдэл', compute='_compute_product_litr')
    product_kg_qty = fields.Float(
        u'Кг үлдэгдэл', compute='_compute_product_litr')
    default_code = fields.Char('Internal Reference', index=True,required=True)
    qty_ids = fields.One2many('product.qty','product_id',string='Үлдэгдэл')
    vat_taxtype = fields.Boolean(string=u'НӨАТ чөлөөлөгдсөн эсэх')
    ebarimt_code = fields.Char(string=u'Е-баримт код')
    fin_code = fields.Char(string=u'Санхүүгийн код')
    
    
    _sql_constraints = [
        ('code_uniq', 'unique (default_code)', u'Бүтээгдэхүүний код давхардахгүй!')
    ]
    
    
    def name_get(self):
        # TDE: this could be cleaned a bit I think

        def _name_get(d):
            name = d.get('name', '')
            code = self._context.get('display_default_code', True) and d.get('default_code', False) or False
            if code:
                name = '%s [%s]' % (name,code)
            return (d['id'], name)

        partner_id = self._context.get('partner_id')
        if partner_id:
            partner_ids = [partner_id, self.env['res.partner'].browse(partner_id).commercial_partner_id.id]
        else:
            partner_ids = []
        company_id = self.env.context.get('company_id')

        # all user don't have access to seller and partner
        # check access and use superuser
        self.check_access_rights("read")
        self.check_access_rule("read")

        result = []

        # Prefetch the fields used by the `name_get`, so `browse` doesn't fetch other fields
        # Use `load=False` to not call `name_get` for the `product_tmpl_id`
        self.sudo().read(['name', 'default_code', 'product_tmpl_id'], load=False)

        product_template_ids = self.sudo().mapped('product_tmpl_id').ids

        if partner_ids:
            supplier_info = self.env['product.supplierinfo'].sudo().search([
                ('product_tmpl_id', 'in', product_template_ids),
                ('name', 'in', partner_ids),
            ])
            # Prefetch the fields used by the `name_get`, so `browse` doesn't fetch other fields
            # Use `load=False` to not call `name_get` for the `product_tmpl_id` and `product_id`
            supplier_info.sudo().read(['product_tmpl_id', 'product_id', 'product_name', 'product_code'], load=False)
            supplier_info_by_template = {}
            for r in supplier_info:
                supplier_info_by_template.setdefault(r.product_tmpl_id, []).append(r)
        for product in self.sudo():
            variant = product.product_template_attribute_value_ids._get_combination_name()

            name = variant and "%s (%s)" % (product.name, variant) or product.name
            sellers = []
            if partner_ids:
                product_supplier_info = supplier_info_by_template.get(product.product_tmpl_id, [])
                sellers = [x for x in product_supplier_info if x.product_id and x.product_id == product]
                if not sellers:
                    sellers = [x for x in product_supplier_info if not x.product_id]
                # Filter out sellers based on the company. This is done afterwards for a better
                # code readability. At this point, only a few sellers should remain, so it should
                # not be a performance issue.
                if company_id:
                    sellers = [x for x in sellers if x.company_id.id in [company_id, False]]
            if sellers:
                for s in sellers:
                    seller_variant = s.product_name and (
                        variant and "%s (%s)" % (s.product_name, variant) or s.product_name
                        ) or False
                    mydict = {
                              'id': product.id,
                              'name': seller_variant or name,
                              'default_code': s.product_code or product.default_code,
                              }
                    temp = _name_get(mydict)
                    if temp not in result:
                        result.append(temp)
            else:
                mydict = {
                          'id': product.id,
                          'name': name,
                          'default_code': product.default_code,
                          }
                result.append(_name_get(mydict))
        return result
    
    
    
    def action_open_qty(self):
        action = self.env.ref('mn_web_portal.action_product_qty').read()[0]
        action['domain'] = [('product_id', '=', self.id)]
        return action

    
    
    @api.depends('qty_ids')
    def _compute_product_qty(self):
        for line in self:
            qty = 0.0
            for li in line.qty_ids:
                qty +=li.qty
            line.product_qty = qty
    
    @api.depends('qty_ids')
    def _compute_product_litr(self):
        for line in self:
            qty_litr = 0.0
            qty_kg = 0.0
            for li in line.qty_ids:
                qty_litr +=li.litr_qty
                qty_kg +=li.kg_qty
            line.product_litr_qty = qty_litr
            line.product_kg_qty = qty_kg