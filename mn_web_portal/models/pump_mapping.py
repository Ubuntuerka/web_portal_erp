# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
import time
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from dateutil import relativedelta

class PumpMapping(models.Model):
    _name = 'pump.mapping'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'name desc'
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('pump.mapping'))
    pump = fields.Float(string='Pump')
    nozzle = fields.Float(string='Nozzle')
    prod = fields.Char(string='Product code')
    comport = fields.Float(string='Comport number')
    name = fields.Char(string='Disdplay name')
    shts_id = fields.Many2one('shts.register',string='Shts')
    shts_code = fields.Char(string='ШТС код',compute="_onchange_shts_id",store=True)
    
    
    @api.depends('shts_id')
    def _onchange_shts_id(self):
        for line in self:
            line.shts_code = line.shts_id.pos_number
            