# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date, datetime, time
from odoo.exceptions import Warning, UserError

from odoo.exceptions import RedirectWarning, UserError, ValidationError
from odoo.osv.expression import get_unaccent_wrapper
import re
from odoo.osv import expression
USER_PRIVATE_FIELDS = []

class InventoryProduct(models.Model):
    _name = 'inventory.product'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    def _action_desc(self):
        for line in self:
            line.desc = u"%s өдрийн тооллого"%line.inventory_date 
    
    company_id = fields.Many2one('res.company',string=u'Компани'
                                 ,default=lambda self: self.env['res.company']._company_default_get('inventory.product'),required=True)
    name = fields.Char(string=u'Дугаарлалт')
    desc = fields.Char(string=u'Тайлбар',default=_action_desc)
    date = fields.Date(string=u'Тооллогын огноо',required=True)
    type = fields.Selection([('gasol',u'Шатахуун'),
                             ('oil',u'Тос')],string='Type',default='gasol')
    reg_date = fields.Date(string=u'Бүртгэсэн огноо', default=fields.Date.context_today,required=True)
    inventory_date = fields.Date(string=u'Тооллогын огноо',required=True)
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    shift = fields.Integer(string=u'Ээлж',required=True)
    line_ids = fields.One2many('inventory.product.line','inventory_id',string=u'Line',ondelete="cascade")
    reg_user_id = fields.Many2one('res.users',string=u'Бүртгэсэн ажилтан',default=lambda self: self.env.user,required=True)
    is_shts_select = fields.Boolean(string='ШТС сонгох эсэх')
    state = fields.Selection([('draft',u'Ноорог'),
                              ('confirm',u'Тоолсон'),
                              ('done',u'Батлагдсан'),
                              ('cancel',u'Цуцлагдсан')],string=u'Төлөв',default='draft')

    def action_print(self):
        return self.env.ref('mn_web_portal.ttm_toollogo_print_view').report_action(self)
    
    def action_cancel(self):
        for line in self:
            line.write({'state':'cancel'})
    
    def action_draft(self):
        for line in self:
            for list in line.line_ids:
                list.unlink()
            line.write({'state':'draft'})
    
    def action_confirm(self):
        for line in self:
            diff = False
            shift_obj = self.env['shift.working.register']
            shift_id = shift_obj.search([('shts_id','=',line.shts_id.id),('state','in',('count','draft')),('shift_date','<=',str(line.inventory_date))])
            if shift_id:
                raise UserError(_(u'Ээлжээ батлана уу!'))
                    
            for li in line.line_ids:
                if li.diff_qty >0.0:
                    diff = True
                    
            if diff == True:
                compose_form = self.env.ref('mn_web_portal.inventory_wizard_view', raise_if_not_found=False)
                return {
                    'name': _(u'Тооллого батлах'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'inventory.wizard',
                    'views': [(compose_form.id, 'form')],
                    'view_id': compose_form.id,
                    'target': 'new',
                }
            else:
                
                line.write({'state':'done'})
            
    
    def inv_done(self):
        qty_obj = self.env['product.qty']
        for line in self:
            if line.line_ids:
                for lines in line.line_ids:
                    qty = 0.0
                    if lines.diff_qty > 0.0:
                        type = 'in'
                        qty = lines.diff_qty
                    elif lines.diff_qty < 0.0:
                        type = 'out'
                        qty = lines.diff_qty
                    if qty !=0.0:
                        qty_obj.create({'qty':qty,
                                        'product_id':lines.product_id.id,
                                        'shts_id':line.shts_id.id,
                                        'reg_date':line.inventory_date,
                                        'type':type,
                                        'company_id':line.company_id.id})
            else:
                raise UserError(_(u'Тооллогын мөр хоосон байгаа тул батлагдахгүй!'))
            line.write({'state':'done'})
    
    def inv_open(self):
        qty_line_obj = self.env['inventory.product.line']
        qty_obj = self.env['product.qty']
        shift_obj = self.env['shift.working.register']
        for line in self:
            if line.shts_id:
                qty_id = qty_obj.search([('company_id','=',line.company_id.id),('shts_id','=',self.shts_id.id)])
                shift_id = shift_obj.search([('shts_id','=',line.shts_id.id),('state','not in',('count','confirm')),('shift_date','<',str(line.inventory_date))])
                if shift_id:
                    raise UserError(_(u'Тооллого хийхэд хаагдаагүй ээлж байгаа тул шалгана уу!'))
                
                for li in line.shts_id.oil_line_ids:
                    qty = 0.0
                    self._cr.execute("""
                          SELECT sum(qty) as qty,shts_id,product_id
                          FROM product_qty
                          where company_id = %s and product_id = %s
                          and shts_id = %s and reg_date <= '%s'
                          group by shts_id,product_id
                    """%(line.company_id.id,li.product_id.id, line.shts_id.id,line.inventory_date))
                    qty_ids  = self._cr.dictfetchall()
                    for qt in qty_ids:
                        qty +=qt['qty']
                    qty_line_obj.create({
                                        'product_id':li.product_id.id,
                                        'inventory_id':line.id,
                                        'qty':qty,
                                        'last_qty':qty,
                                        'shts_id':line.shts_id.id,
                                        })
            self.write({'state':'confirm'})
        
class InventoryProductLine(models.Model):
    _name = 'inventory.product.line'

    company_id = fields.Many2one('res.company',string='Company'
                                 ,default=lambda self: self.env['res.company']._company_default_get('inventory.product.line'))
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн',required=True)
    qty = fields.Float(string=u'Тооллогын үлдэгдэл',required=True)
    last_qty = fields.Float(string=u'Байвал зохих')
    diff_qty = fields.Float(string=u'Зөрүү',compute="action_diff_qty")
    inventory_id = fields.Many2one('inventory.product',string=u'Inventory',ondelete="cascade")
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    temp = fields.Float(string='Temperature')
    self_weight = fields.Float(string='Self weight')
    product_code = fields.Char(string='Product code',compute="action_product_code")
    heigth = fields.Float(string='Height')
    
    
    def action_product_code(self):
        for line in self:
            line.product_code = line.product_id.code
    
    def action_diff_qty(self):
        for line in  self:
            line.diff_qty = line.qty - line.last_qty





