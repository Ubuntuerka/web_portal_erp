# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

from datetime import timedelta
from datetime import datetime
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class TablitsChangeRequest(models.Model):
    _name = 'tablits.change.request'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    company_id = fields.Many2one('res.company',u'Компани',required=True
                                 ,default=lambda self: self.env['res.company']._company_default_get('tablits.change.request'),)
    name = fields.Char(string=u'Хүсэлтийн дугаар',required=True)
    update_date = fields.Date(string=u'Шинэчлэгдсэн огноо')
    reg_date = fields.Date(string=u'Бүртгэсэн огноо',required=True, default=fields.Date.context_today)
    reg_user_id = fields.Many2one('res.users',u'Бүртгэсэн ажилтан',required=True,default=lambda self: self.env.user)
    be_tablits_id = fields.Many2one('tablits.register',u'Хуучин таблиц')
    new_tablits_id = fields.Many2one('tablits.register',u'Шинэ таблиц')
    can_id = fields.Many2one('can.register',u'Сав',domain="[('is_oil_can','=',False)]")
    desc = fields.Text(string=u'Тайлбар')
    state = fields.Selection([('draft',u'Ноорог'),
                              ('confirm',u'Батлагдсан')],string=u'Төлөв',default='draft')
    
    def action_confirm(self):
        for line in self:
            if line.can_id:
                line.be_tablits_id.write({'can_id':False,'active':False})
                line.can_id.write({'tablits_id':[6,0,(line.new_tablits_id.id)]})
        self.write({'state':'confirm'})
        
        
 
        

class TablitsRegister(models.Model):
    _name = 'tablits.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company','Company'
                                 ,default=lambda self: self.env['res.company']._company_default_get('tablits.register'),required=True)
    name = fields.Char(string='Tablits name',required=True)
    active = fields.Boolean(string=u'Идэвхтэй',default=True)
    update_date = fields.Date(string=u'Шинэчлэгдсэн огноо')
    start_date = fields.Date(string=u'Эхлэх хугацаа',required=True, default=fields.Date.context_today)
    end_date = fields.Date(string=u'Дуусах хугацаа',required=True, default=fields.Date.context_today)
    can_id = fields.Many2one('can.register',string='Can',required=True,domain="[('is_oil_can','=',False)]")
    line_ids = fields.One2many('tablits.register.line','tablits_id',string='Tablits line')
    shts_id = fields.Many2one('shts.register',u'ШТС',related='can_id.shts_id',readonly=True,store=True)
    desc = fields.Text(string=u'Тайлбар')
    
    
    def _auto_send_tablits_email(self):
        tablits_ids = self.search([])
        tablits_ids.action_tablits_send_email()
    
    def action_tablits_send_email(self):
        for line in self:
            now_date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            pre_days = datetime.strptime(str(now_date),DATETIME_FORMAT) + timedelta(days=30)
            if str(str(pre_days)[:10]) == str(str(line.end_date)[:10]):
                partner_obj = self.env['res.partner']
                partner_ids = partner_obj.search([('email','=','technology@shunkhlai.onmicrosoft.com')],limit=1)
                partner_list = [partner_ids.id]
                admin_email = partner_obj.search([('email','=','guurwebportal@gmail.com')],limit=1)
                mail_dict = {}
    
                current_login_user = self.env.user 
                email_subject = u"Таблицны хугацааны сунгалт"
                email_description = u"Сайн байна уу?\n Салбар: %s \n Савны нэр, код: %s \n Дуусах хугацаа:  %s \n Хүндэтгэсэн, Гүүр вэб портал"%(line.shts_id.name,line.can_id.name,line.end_date)
                if partner_list:
                    mail_dict ={
                             'subject'       : email_subject,
                             'email_from'    : admin_email.email,
                             'recipient_ids' : [(6,0,partner_list)],
                             'body_html'     : email_description,
                            }
                    if mail_dict:
                        mail_id = current_login_user.env['mail.mail'].create(mail_dict)
                    if mail_id:
                       mail_id.send()
                
    
    

class TablitsRegister(models.Model):
    _name = 'tablits.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company','Company'
                                 ,default=lambda self: self.env['res.company']._company_default_get('tablits.register'))
    
    name = fields.Float(string='MM')
    one_mm = fields.Float(string='1')
    two_mm = fields.Float(string='2')
    three_mm = fields.Float(string='3')
    four_mm = fields.Float(string='4')
    five_mm = fields.Float(string='5')
    six_mm = fields.Float(string='6')
    seven_mm = fields.Float(string='7')
    eight_mm = fields.Float(string='8')
    nine_mm = fields.Float(string='9')
    ten_mm = fields.Float(string='10')
    tablits_id = fields.Many2one('tablits.register',string='Tablits')
