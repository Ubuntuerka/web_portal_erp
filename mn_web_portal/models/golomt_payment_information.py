# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import timedelta
from datetime import date, datetime, time



DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

class GolomtPaymentInformation(models.Model):
    _name = 'golomt.payment.information'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    payment_type = fields.Selection([('cash', u'Бэлэн'),
                                     ('bank_card', u'Банк карт'),
                                     ('sh_card', u'Шунхлай карт'),
                                     ('loan', u'Зээл'),
                                     ('talon', u'Талон'),
                                     ('tov_card', u'Төвд карт'),
                                     ('discount', u'Хөнгөлөлт'),
                                     ('mobile', u'Мобайл банк/ Шилжүүлэг'),
                                     ('tender', u'Тендер /НӨАТ-гүй/'),
                                     ('monpay', u'МонПэй'),
                                     ('zalin', u'Залин'),
                                     ('socialpay', u'СошлПэй'),
                                     ('order', u'Бусад')], string=u'Төлбөрийн төрөл', required=True)
    sale_id = fields.Many2one('borluulaltin.medee', string=u'Sale ID', required=True)
    trace_no = fields.Char(string=u'Trace no')
    card_maskal = fields.Char(string=u'Картын дугаар маскалсан хэлбэр')
    car_number = fields.Char(string=u'Машины дугаар')
    partner_id = fields.Char(string=u'Харилцагчийн РД')
    partner_vat = fields.Char(string='Татвар',required=True)
    transaction_date = fields.Datetime(string=u'Гүйлгээ хийсэн огноо', required=True)
    payment_date = fields.Datetime(string=u'Гүйлгээ хийсэн огноо',compute="onchange_payment_date")
    total_amount = fields.Float(string=u'Нийт дүн', required=True)
    discount = fields.Float(string=u'Хөнгөлөлт', required=True)
    amount_paid = fields.Float(string=u'Төлсөн дүн', required=True)
    talon_serial_number = fields.Char(string=u'Талоны сериал дугаар')
    #settlement = fields.Many2one('settlement.view', string=u'settlement', required=True)
    qty = fields.Float(string=u'Тоо хэмжээ', compute="action_shts_code")
    shts_id = fields.Char(string=u'ШТС код',compute="action_shts_code")
    
    
    @api.onchange('transaction_date')
    def onchange_payment_date(self):
        
        for line in self:
            if line.transaction_date:
                transaction_date1 =  datetime.strptime(str(line.transaction_date), DATETIME_FORMAT) - timedelta(hours=8)
                
            line.payment_date = transaction_date1
    
                
    def action_shts_code(self):
        for line in self:
            shts_ids = False
            qty = 0
            if line.sale_id:
                shts_ids = line.sale_id.shts_code
                for product in line.sale_id.sales_product_ids:
                    qty += product.size
                
            line.shts_id = shts_ids
            line.qty = qty
    
    
            
            