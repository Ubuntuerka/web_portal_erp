# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class FireToolsRegister(models.Model):
    _name = 'fire.tools.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    shts_id = fields.Many2one('shts.register',string='Агуулах/ШТС',required=True,
                                    default=lambda self: self.env.user.warehouse_id)
    shift_date = fields.Date(string=u'Ээлжийн огноо', required=True, default=fields.Date.context_today)
    reg_user_id = fields.Many2one('res.users', string=u'Бүртгэсэн ажилтан', default=lambda self: self.env.user,
                                  required=True)
    hurz = fields.Integer(string=u'Хүрз',required=True)
    jootuu = fields.Integer(string=u'Жоотуу',required=True)
    esgii = fields.Integer(string=u'Эсгий',required=True)
    loom = fields.Integer(string=u'Лоом',required=True)
    horoo = fields.Integer(string=u'Хөрөө',required=True)
    gurwaljin_huwin = fields.Integer(string=u'Гурвалжин хувин',required=True)
    degee = fields.Integer(string=u'Дэгээ',required=True)
    elstei_sav = fields.Integer(string=u'Элстэй жижиг сав',required=True)
    elstei_tow_sav = fields.Integer(string=u'Элстэй том сав',required=True)
    galin_hor = fields.Integer(string=u'Галын хор',required=True)
    suh = fields.Integer(string=u'Сүх',required=True)
    utaa_medregch = fields.Integer(string=u'Утаа Мэдэрэгч',required=True)
    changa_yrigch = fields.Integer(string=u'Чанга яригч',required=True)
    usni_torh = fields.Integer(string=u'Усны торх',required=True)
    excuse = fields.Char(string=u'Тайлбар')
