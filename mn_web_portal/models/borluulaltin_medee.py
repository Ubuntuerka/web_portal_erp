# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class BorluulaltinMedee(models.Model):
    _name = 'borluulaltin.medee'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'shift_date desc, pos_number desc'
    
    
    #@api.onchange('partner_vat')
    def _onchange_partner_id(self):
        partner_obj = self.env['res.partner']
        for line in self:
            partners = False
            if line.partner_vat:
                partner_id = partner_obj.search([('vat','=',line.partner_vat)],limit=1)
                if partner_id:
                    partners = partner_id.id
            line.partner_id = partners

    ddtd = fields.Char(string=u'ДДТД', required=True)
    company_id = fields.Many2one('res.company', string='Компани', related='shts_id.company_id', store=True)
    terminal_id = fields.Many2one('terminal.register', string=u'Терминал ID')
    tugeeguur = fields.Char(string=u'Түгээгүүр')
    shts_id = fields.Many2one('shts.register',string=u'ШТС',compute="onchange_shts_id")
    pos_number = fields.Char(string=u'ПОС дугаар', required=True)
    total_amount = fields.Char(string='Нийт дүн', required=True)
    discount = fields.Float(string='Хөнгөлөлт', required=True)
    pay_amount = fields.Float(string='Төлсөн дүн', required=True)
    noat_amount = fields.Char(string=u'НӨАТ', required=True)
    suglaani_dugaar = fields.Char(string=u'Сугалааны дугаар')
    partner_id = fields.Many2one('res.partner', string='Харилцагч',compute="_onchange_partner_id")
    partner_vat = fields.Char(string=u'Регистрийн дугаар',required=True)
    employee_id = fields.Many2one('hr.employee', u'Ажилтан')
    employee_code = fields.Char(u'Ажилтан код', required=True)
    pump_number = fields.Char(string='Pump number')
    hoshuu = fields.Many2one('mile.target.register', string=u'Хошуу')
    hoshuu_code = fields.Char(string=u'Хошуу')
    comport = fields.Char(string=u'Comport', required=True)
    payment_ids = fields.One2many('golomt.payment.information', 'sale_id', string=u'Терминал ID', required=True)
    sales_product_ids = fields.One2many('golomt.sales.product.list', 'sale_id', string=u'Терминал ID', required=True)
    pump_ids = fields.One2many('settlement.pump','sale_id',string='PUMP',required=True)
    shift_date = fields.Datetime(string=u'Баримт хэвлэсэн огноо',default=lambda self: fields.datetime.now())
    shift = fields.Integer(string=u'Ээлж')
    shts_code = fields.Char(string=u'ШТС Код',required=True)
    #source_id = fields.Char(string=u'Пос-ын борлуулалтын ID')
    source = fields.Char(string=u'Пос-ын борлуулалтын ID')
    
    _sql_constraints = [
        ('source_uniq', 'unique (source)', u'Sourse ID давхцаж болохгүй тул та бүртгэлээ шалгана уу !'),
    ]
    
    
    @api.onchange('shts_code')
    def onchange_shts_id(self):
        shts_obj = self.env['shts.register']
        for line in self:
            shts_ids = False
            shts_id = shts_obj.search([('pos_number','=',line.shts_code)],limit=1)
            if shts_id:
                shts_ids = shts_id.id
            line.shts_id = shts_ids
            

        

class SettlementView(models.Model):
    _name = 'settlement.view'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'reg_date desc, shts_code desc'
    
    def _onchange_shts_id(self):
        shts_obj = self.env['shts.register']
        for line in self:
            shts_ids = False
            shts_id = shts_obj.search([('pos_number','=',line.shts_code)],limit=1)
            if shts_id:
                shts_ids = shts_id.id
            line.shts_id = shts_ids

    sales_payment_ids = fields.One2many('settlement.payment', 'settlement_payment', string=u'settlement', required=True)
    sales_pump_ids = fields.One2many('settlement.pump', 'settlement_pump', string=u'settlement', required=True)
    settlement_product_ids = fields.One2many('settlement.products', 'settlement_products', string=u'settlement', required=True)
    shts_code = fields.Char(string=u'ШТС Код')
    shts_id = fields.Many2one('shts.register', string=u'ШТС',compute="_onchange_shts_id")
    reg_date = fields.Datetime(string='Register date', default=lambda self: fields.datetime.now())
    
    
    
    

class SettlementPayment(models.Model):
    _name = 'settlement.payment'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    settlement_payment = fields.Many2one('settlement.view', 'settlement', required=True)
    payment_type = fields.Selection([('cash', u'Бэлэн'),
                                         ('bank_card', u'Банк карт'),
                                         ('sh_card', u'Шунхлай карт'),
                                         ('loan', u'Зээл'),
                                         ('talon', u'Талон'),
                                         ('tov_card', u'Төвд карт'),
                                         ('discount', u'Хөнгөлөлт'),
                                         ('mobile', u'Мобайл банк/ Шилжүүлэг'),
                                         ('tender', u'Тендер /НӨАТ-гүй/'),
                                         ('monpay', u'МонПэй'),
                                         ('socialpay', u'СошлПэй'),
                                         ('zalin', u'Залин'),
                                         ('order', u'Бусад')], string=u'Төлбөрийн төрөл', required=True)
    guilgeenii_too = fields.Float(string='Гүйлгээний тоо', required=True)
    guilgeenii_niit = fields.Float(string='Гүйлгээнй нийт', required=True)
    product_id = fields.Many2one('product.product', string='Бүтээгдэхүүн')
    code = fields.Char(string='Код', required=True)
    unit_price = fields.Float(string='Нэгж үнэ', required=True)
    size = fields.Float(string='Хэмжээ', required=True)
    total_amount = fields.Float(string='Нийт үнэ', required=True)


class SettlementPump(models.Model):
    _name = 'settlement.pump'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    def _onchange_pump(self):
        mapping_obj = self.env['pump.mapping']
        shts_obj = self.env['shts.register']
        for line in self:
            hoshuu = ''
            shts_id = shts_obj.search([('pos_number','=',line.shts_code)],limit=1)
            if shts_id:
               mapp_id = mapping_obj.search([('pump','=',line.pump),('nozzle','=',line.hoshuu_number),('shts_id','=',shts_id.id)],limit=1)
            
            
            if mapp_id:
                hoshuu = mapp_id.name
            line.hoshuu_name = hoshuu

    settlement_pump = fields.Many2one('settlement.view', 'settlement', required=True)
    sale_id = fields.Many2one('borluulaltin.medee', string=u'Sale ID')
    shts_id = fields.Many2one('shts.register', string=u'ШТС')
    shts_code = fields.Char(string=u'Салбар ID')
    
    comport = fields.Char(string=u'Comport', required=True)
    pump = fields.Char(string=u'Pump', required=True)
    
    guiltiin_mill = fields.Char(string=u'Гүйлгээний милл', required=True)
    hoshuu = fields.Many2one('mile.target.register', string=u'Хошуу')
    hoshuu_number = fields.Char(string=u'Хошууны дугаар')
    hoshuu_name = fields.Char(string=u'Хошуу',compute="_onchange_pump")


class SettlementProducts(models.Model):
    _name = 'settlement.products'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    settlement_products = fields.Many2one('settlement.view', 'settlement', required=True)
    product_id = fields.Many2one('product.product', string='Product')
    product_code = fields.Char(string='Product code')
    product_name = fields.Char(string='Product name')
    size = fields.Float(string='Хэмжээ', required=True)
    unit_price = fields.Float(string='Нэгж үнэ')
    total_amount = fields.Float(string='Нийт үнэ', required=True)

    #@api.onchange('product_code')
    #def _onchange_product_id(self):
        #for line in self:
           # line.product_id = line.product_id.default_code
            #line.product_name = line.product_id.name


