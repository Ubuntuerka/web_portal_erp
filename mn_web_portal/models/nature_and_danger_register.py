# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree
DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


class NatureAndDangerRegister(models.Model):
    _name = 'nature.and.danger.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'name desc'

    state = fields.Selection([('draft', u'Ноорог'),
                              ('send', u'Илгээгдсэн'),
                              ('allocated', u'Хуваарилагдсан '),
                              ('resolved', u'Шийдвэрлэгдсэн'),
                              ('confirm', u'Батлагдсан'),
                              ('return', u'Буцаагдсан')], string=u'Төлөв', default='draft', tracking=True)
    department_id = fields.Many2one('hr.department', 'Газар/нэгж', default=lambda self: self.env.user.dep_id)
    last_name = fields.Many2one('res.users', string=u'Ажилтны нэр', required=True, default=lambda self: self.env.user)
    name = fields.Char(string=u'Сери дугаар', required=True,
                       default=lambda self: self.env['ir.sequence'].next_by_code('nature.and.danger.register'))
    n_date = fields.Date(string=u'Илрүүлсэн огноо', required=True, default=fields.Date.context_today)
    location = fields.Char(string=u'Хаана', required=True)
    excuse = fields.Char(string=u'Шалтгаан', required=True)
    emp = fields.Many2one('res.users', string=u'Хариуцах эзэн')
    category = fields.Selection([('ayul', u'Аюул'),
                                 ('osol', 'Осолд дөхсөн тохиолдол'),
                                 ('gemtel', 'Гэмтэл'),
                                 ('j_osol', 'Жижиг осол'),
                                 ('n_osol', 'Ноцтой осол'),
                                 ('nas_baralt', 'Нас баралт'),
                                 ('baigal_orchinii_as', 'Байгал орчны асуудал')], string='Ангилал') # , compute='group_category'
    danger = fields.Selection([('huleeh_awah', u'Хүлээн авах'),
                               ('hadgalah', u'Хадгалах'),
                               ('teewerleh', 'Тээвэрлэх'),
                               ('tugeeh', 'Түгээх'),
                               ('order', u'Бусад')], string=u'Аюул') # , compute='group_danger'
    end_date = fields.Date(string=u'Дуусах огноо') # , compute='group_date'
    arga = fields.Char(string=u'Арга хэмжээ')
    s_shaltgaan = fields.Selection([('huchin', 'Хүний хүчин зүйл'),
                                    ('gadni', 'Гадны'),
                                    ('haldlaha', 'Халдлага'),
                                    ('tehnik', 'Техник шинжтэй'),
                                    ('orchin', 'Орчны')], string='С/шалтгаан')
    h_a_type = fields.Selection([('nbhh', 'НБХХ'),
                                 ('delete', 'Устгах'),
                                 ('orluulah', 'Орлуулах'),
                                 ('zahirgaa', 'Захиргааны'),
                                 ('еnjiner', 'Инженерийн')], string='Х.А.Хэмжээний төрөл')
    hamaarah = fields.Selection([('company', 'Компани'),
                                 ('gereet', 'Гэрээт'),
                                 ('partner', 'харилцагч'),
                                 ('users', 'Хэрэглэгч')], string='Хамаарах')
    confirm_user = fields.Many2one('res.users', string=u'Баталсан ажилтан')
    # location_ids = fields.One2many('location.register.view', 'location_id', required=True)
    location_name = fields.Many2one('location.register.view', string=u'Байршил', required=True)

    # @api.onchange('last_name')
    # def onchange_department_id(self):
    #     for line in self:
    #         line.department_id = line.last_name.department_id.id




    # def group_user(self):
    #     if self.user_has_groups('mn_web_portal.group_web_portal_habea_senior_manager'):
    #         self.emp = True
    #     else:
    #         self.emp = False

    # def group_category(self):
    #     if self.user_has_groups('mn_web_portal.group_web_portal_habea_senior_manager'):
    #         self.category = True
    #     else:
    #         self.category = False
    #
    # def group_danger(self):
    #     if self.user_has_groups('mn_web_portal.group_web_portal_habea_senior_manager'):
    #         self.danger = True
    #     else:
    #         self.danger = False
    #
    # def group_date(self):
    #     if self.user_has_groups('mn_web_portal.group_web_portal_habea_senior_manager'):
    #         self.end_date = True
    #     else:
    #         self.end_date = False

    # @api.model
    # def write(self, vals):
    #     huwaarilalt = self.env.user.has_group('mn_web_portal.group_web_portal_technology_manager_hab')
    #     if huwaarilalt == False:
    #         if 'emp' in vals:
    #             if not vals:
    #                 raise ValidationError(_('Хуваарилах ажилтан засна. Та менежертээ хандана уу!!!'))
    #         if 'category' in vals:
    #             if not vals:
    #                 raise ValidationError(_('Хуваарилах ажилтан засна. Та менежертээ хандана уу!!!'))
    #         if 'end_date' in vals:
    #             if not vals:
    #                 raise ValidationError(_('Хуваарилах ажилтан засна. Та менежертээ хандана уу!!!'))
    #     res = super(NatureAndDangerRegister, self).write(vals)
    #     return res

    # @api.model
    # def emp_user(self, cr, uid, ids, name, arg, context=None):
    #     emp_user = self.env['res.users'].search([('name','=','mn_web_portal.group_web_portal_technology_manager_hab')])
    #     self.emp = self.env.user.id in emp_user.group_web_portal_technology_manager_hab.users.ids
    #     print ('sssssss', self.emp, emp_user, emp_user.group_web_portal_technology_manager_hab.users.ids)

    def action_return(self):
        self.write({'state': 'draft'})

    def action_send(self):
        # for line in self:
        #
        #     partner_list = [line.last_name.partner_id.id]
        #     mail_dict = {}
        #
        #     current_login_user = self.env.user
        #     email_subject = u"Аюул, Байгал орчны асуудал мэдээлэх"
        #     email_description = u" Сайн байна уу?" \
        #                         u"\n %s танд захиалгат ажил бүртгэгдлээ. Хүсэлтийн дугаар: %s " \
        #                         u"\n Газар/Нэгж: %s " \
        #                         u"\n Төлөв: Илгээсэн " \
        #                         u"\n Хүндэтгэсэн, Гүүр вэб портал " \
        #                         % (self.last_name.name, self.name, self.department_id.name)
        #
        #     if partner_list:
        #         mail_dict = {
        #             'subject': email_subject,
        #             'email_from': self.emp.email,
        #             'recipient_ids': [(6, 0, partner_list)],
        #             'body_html': email_description,
        #         }
        #         if mail_dict:
        #             mail_id = current_login_user.env['mail.mail'].create(mail_dict)
        #         if mail_id:
        #             mail_id.send()
        self.write({'state': 'send'})

    def action_allocated(self):
        for line in self:
            partner_list = [line.emp.partner_id.id]
            mail_dict = {}

            current_login_user = self.env.user
            email_subject = u"Аюул, Байгал орчны асуудал бүртгэгдлээ"
            email_description = u" Сайн байна уу?\n %s танд Захиалгат ажил бүртгэгдлээ. Хүсэлтийн дугаар: %s \n Салбар: %s \n Ангилал: %s \n Төлөв: Хуваарилагдсан  \n Хүндэтгэсэн, Гүүр вэб портал " \
                                % (self.emp.name, self.name, self.department_id.name, self.category)

            if partner_list:
                mail_dict = {
                    'subject': email_subject,
                    'email_from': self.last_name.email,
                    'recipient_ids': [(6, 0, partner_list)],
                    'body_html': email_description,
                }
                if mail_dict:
                    mail_id = current_login_user.env['mail.mail'].create(mail_dict)
                if mail_id:
                    mail_id.send()
        self.write({'state': 'allocated'})

    def action_resolved(self):
        self.write({'state': 'resolved'})

    def action_confirm(self):
        self.write({'state': 'confirm', 'confirm_user': self.env.user.id})


class LocationRegisterView(models.Model):
    _name = 'location.register.view'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'name desc'

    # location_id = fields.Many2one('nature.and.danger.register','location_ids', required=True)
    name = fields.Char(string='Аюул дөхсөн газар')
