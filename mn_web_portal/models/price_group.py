# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class PriceGroup(models.Model):
    _name = 'price.group'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string=u'Компани')
    name = fields.Char(string=u'Үнийн')
    shts_ids = fields.Many2many('shts.register','shts_price_group_rel','shts_id','price_qroup_id',string=u'ШТС')
    