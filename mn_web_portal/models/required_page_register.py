# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class RequiredPageRegister(models.Model):
    _name = 'required.page.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    shts_id = fields.Many2one('shts.register', string='Агуулах/ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id)
    date = fields.Date(string=u'Огноо', required=True,  default=fields.Date.context_today)
    user_required = fields.Many2one('res.users', string=u'Шаардах бичсэн', required=True, default=lambda self: self.env.user)
    user_done = fields.Many2one('res.users', string=u'Шаардах баталсан')
    line_ids = fields.One2many('required.page.register.line', 'meaning_ids', string='product')


class RequiredPageRegisterLine(models.Model):
    _name = 'required.page.register.line'

    meaning_ids = fields.Many2one('required.page.register', string='line')
    meaning = fields.Char(string=u'Материалын нэр')
    total = fields.Float(string=u'Тоо ширхэг')
