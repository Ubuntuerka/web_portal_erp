# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class GolomtSalesProductList(models.Model):
    _name = 'golomt.sales.product.list'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    product_id = fields.Many2one('product.product',string='Бүтээгдэхүүн',domain="[('prod_type', '=', 'gasol')]")
    code = fields.Char(string='Код', required=True)
    unit_price = fields.Float(string='Нэгж үнэ', required=True)
    size = fields.Float(string='Хэмжээ', required=True)
    total_amount = fields.Float(string='Нийт үнэ', required=True)
    vat = fields.Boolean(string='Vat-тай эсэх', required=True)
    barcode = fields.Char(string='Зураасан код', required=True)
    sale_id = fields.Many2one('borluulaltin.medee', string=u'Sale ID', required=True)
    #settlement = fields.Many2one('settlement.view', string=u'settlement', required=True)