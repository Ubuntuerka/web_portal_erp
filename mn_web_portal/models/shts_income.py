# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from decimal import Decimal

class ShtsIncome(models.Model):
    _name = 'shts.income'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'reg_date desc, shift_date desc'

    
    @api.depends("shts_id")
    def _action_company_id(self):
        for line in self:
            company_id = False
            if line.shts_id.company_id:
                company_id= line.shts_id.company_id.id
            line.company_id = company_id
            
    company_id = fields.Many2one('res.company',string='Company',compute="_action_company_id")
    wagon_number = fields.Char(string='Вагон дугаар', size=12)
    shift = fields.Integer(string=u'Ээлж',required=True)
    pad_number = fields.Char(string=u'Падааны дугаар')
    check = fields.Selection([('sgs', 'SGS'),('amber', 'Amber'),('mvhavt','MVHAVT'),('gtbg','GTBG')], 'Check')
    achilt_torol = fields.Selection([('gar', u'Гар ачилт'),('avtomat', u'Автомат ачилт')], u'Ачилтын төрөл',required=True,default='gar')
    name = fields.Char(string=u'ЛН дугаар')
    company_code = fields.Char(string=u'Илгээх компани код')
    from_company_code = fields.Char(string=u'Илгээх компани код')
    to_company_code = fields.Char(string=u'Хүлээн авах компани код')
    from_type = fields.Selection([('warehouse',u'Агуулах')],string='Гарах төрөл',default='warehouse')
    to_type = fields.Selection([('warehouse',u'Агуулах')],string='Хүрэх төрөл',default='warehouse')
    order_type = fields.Selection([('transfer',u'Шилжүүлэх')],string=u'Захиалгын төрөл',default='transfer')
    order_origin = fields.Char(string=u'Order origin')
    warehouse_id = fields.Many2one('shts.register',string='Warehouse',required=True)
    shift_id = fields.Many2one('shift.working.register',string='Shift')
    partner_id = fields.Many2one('res.partner', string='Жолооч', required=True,domain=[('drive_true','=','True')])
    drive_id = fields.Many2one('hr.employee',string='Drive',domain=[('work_type','=','drive')])
    new_drive_id = fields.Many2one('res.partner',string='Drive',required=True)
    shts_id = fields.Many2one('shts.register',string='Shts',required=True,default=lambda self: self.env.user.warehouse_id)
    reg_date = fields.Datetime(string='Send date', default=fields.Date.context_today)
    send_date = fields.Date(string=u'Send date', default=fields.Date.context_today)
    rec_date = fields.Datetime(string='Receive date')
    shift_date = fields.Date(string='')
    type = fields.Selection([('in',u'Орлого'),
                             ('ex',u'Бусад зарлага')],string=u'Төрөл')
    reg_user_id = fields.Many2one('res.users',string='Reg user',default=lambda self: self.env.user)
    line_ids = fields.One2many('shts.income.line','shts_income_id',string='Line')
    state = fields.Selection([('draft',u'Ноорог'),
                              ('send','Send'),
                              ('compute','Compute'),
                              ('receive','Received'),
                              ('cancel','Cancel')],string=u'Төлөв',default='draft',tracking=True)
    receive_user_id = fields.Many2one('hr.employee',string='Received shts manager')
    
    def action_to_draft(self):
        for line in self:
            line.write({'state':'draft'})
    
    def action_return_rec(self):
        self.write({'state':'send'})
        for line in self.line_ids:
            line.write({'state':'send'})
    def action_compute(self):
        next_state = False
        shift_obj = self.env['shift.working.register']
        shift = shift_obj.search([('shift_date','=',self.shift_date),('shts_id','=',self.shts_id.id),('shift','=',self.shift)],limit=1)
        if shift:
            if shift.state !='draft':
                raise UserError(_(u"ШТС ноорог байх үед орлого авахыг анхаарна уу"))
        for line in self.line_ids:
            if line.can_id.line_ids:
                for li in line.can_id.line_ids:
                    if line.product_id.id!=li.product_id.id:
                        raise UserError(_(u"%s Та буруу сав сонгосон байгаа тул зөв саваа сонгоно уу!"%line.product_id.name))
            if line.kg !=line.hariu_kg:
                next_state = False
                

                
            if line.density_20 == 0.0 or line.density_20 >1.0 and line.rec_self_weight == 0.0 or line.rec_self_weight > 1.0:
                raise UserError(_(u"20 Темп хувийн жин эсвэл Х/А хувийн жингийн утгыг зөв оруулсан эсэхээ шалгана уу"))
            else:
                line.action_to_check()
                
                #line.write({'state':'receive'})
                if line.temp == line.rec_temp and line.self_weight == line.rec_self_weight and line.huurult == line.rec_huurult and line.drive_diff >0.0:
                    next_state = True
                else:
                    next_state = False
        
        if next_state == True:
            self.write({'state':'receive'})

        else:
            self.write({'state':'compute'})        
            for lines in self:
                partner_obj = self.env['res.partner']
                partner_ids = partner_obj.search([('email','=','gwp_technology@shunkhlai.mn')],limit=1)
                partner_list = [partner_ids.id]
                admin_email = partner_obj.search([('email','=','guurwebportal@gmail.com')],limit=1)
                
               # partner_list = [3]
                mail_dict = {}
                
                #if partner_id.email == False and partner_id.email == '':
                #      raise UserError(_(u'%s харилцагчид имэйл байхгүй байгаа тул шалгана уу!'%(partner_id.name)))
                #elif partner_id.email !=False or partner_id.email !='':
                     #partner_list.append(partner_id.id)
                current_login_user = self.env.user 
                email_subject = u"Шатахууны орлого батлах"
                email_description = u""" Сайн байна уу?\n
                                         ЛН Дугаар: %s \n
                                         Салбар: %s \n
                                         Илгээсэн огноо: %s \n
                                         Хүндэтгэсэн, 
                                         Гүүр веб портал
                                     """%(lines.name,lines.shts_id.name,lines.shift_date)
                if partner_list:
                    mail_dict ={
                             'subject'       : email_subject,
                             'email_from'    : admin_email.email,
                             'recipient_ids' : [(6,0,partner_list)],
                             'body_html'     : email_description,
                            }
                    if mail_dict:
                        mail_id = current_login_user.env['mail.mail'].create(mail_dict)
                    if mail_id:
                       mail_id.send()
    
    def action_return(self):
        self.write({'state':'draft'})
        for line in self.line_ids:
            line.write({'state':'draft'})
 
    def unlink(self):
        for line in self:
            if line.state in ('send','receive','cancel'):
                raise UserError(_(u"Ноорог биш тул устгах боломжгүйг анхаарна уу"))
            line.line_ids.unlink()
        return super(ShtsIncome, self).unlink()
    
    def action_send(self):
        seq_obj = self.env['ir.sequence']
        shift_obj = self.env['shift.working.register']
        self.write({'state':'send'})
        shift = shift_obj.search([('state','=','draft'),('shift','=',self.shift),('shift_date','=',self.send_date)])
        for lines in self.line_ids:
            if lines.self_weight==0.0:
                raise UserError(_(u"Хувийн жин оруулна уу"))
            #if line.temp == 0.0:
            #    raise UserError(_(u"Температур оруулаагүй байгаа тул оруулна уу"))
            lines.write({'state':'send',
        #=======================================================================
                         })
        # for line in self:
        #     partner_obj = self.env['res.partner']
        #     partner_ids = partner_obj.search([('email','=','gwp_technology@shunkhlai.mn')],limit=1)
        #     partner_list = [partner_ids.id]
        #     admin_email = partner_obj.search([('email','=','guurwebportal@gmail.com')],limit=1)
        #     
        #    # partner_list = [3]
        #     mail_dict = {}
        #     
        #     #if partner_id.email == False and partner_id.email == '':
        #     #      raise UserError(_(u'%s харилцагчид имэйл байхгүй байгаа тул шалгана уу!'%(partner_id.name)))
        #     #elif partner_id.email !=False or partner_id.email !='':
        #          #partner_list.append(partner_id.id)
        #     current_login_user = self.env.user 
        #     email_subject = u"Шатахууны орлого батлах"
        #     email_description = u""" Сайн байна уу?\n
        #                              ЛН Дугаар: %s \n
        #                              Салбар: %s \n
        #                              Илгээсэн огноо: %s \n
        #                          """%(self.name,self.shts_id.name,self.shift_date)
        #     if partner_list:
        #         mail_dict ={
        #                  'subject'       : email_subject,
        #                  'email_from'    : admin_email.email,
        #                  'recipient_ids' : [(6,0,partner_list)],
        #                  'body_html'     : email_description,
        #                 }
        #         if mail_dict:
        #             mail_id = current_login_user.env['mail.mail'].create(mail_dict)
        #         if mail_id:
        #            mail_id.send()
        #=======================================================================

        
    def action_rec(self):
        self.write({'state':'receive'})
        
        for line in self.line_ids:
            line.action_to_check()
            line.write({'state':'receive'})
            
            
    
    
        

class ShtsIncomeLine(models.Model):
    _name = 'shts.income.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']   
    
    
    company_id = fields.Many2one('res.company',string='Company')
    car_id = fields.Many2one('car.register',string=u'Машин',required=True)
    out_can_id = fields.Many2one('can.register',string=u'Гарсан сав')
    shts_income_id = fields.Many2one('shts.income',string='Shts income')
    oil_income_id = fields.Many2one('shts.income',string='Shts income')
    product_id = fields.Many2one('product.product',string='Product',domain=['|',('prod_type','=','lpg'),('prod_type','=','gasol')])
    torkh_id = fields.Many2one('bucket.register',string=u'Торх',domain="[('car_number_id','=',car_id)]")
    shift_id = fields.Many2one('shift.working.register',string='Shift')
    can_id = fields.Many2one('can.register',string='Can',domain="[('is_oil_can','=',False)]")
    temp = fields.Float(string='Temperature',digits=(10,1),default=0.0)
    self_weight = fields.Float(string='Self weight',digits=(10,4),default=0.0)
    hour = fields.Float(string=u'Цаг',default=0.0)
    litr = fields.Float(string='Litr',default=0.0)
    kg = fields.Float(string='Kg',default=0.0,digits=(10,2),)
    send_qty = fields.Float(string=u'Илгээсэн тоо',default=0.0)
    rec_qty = fields.Float(string=u'Хүлээн авсан тоо',default=0.0)
    diff_qty = fields.Float(string=u'Зөрүү',default=0.0)
    torkh_size = fields.Float(string=u'Литр',default=0.0)
    density_20 = fields.Float(string=u'20 Темп хувийн жин',digits=(10,4),default=0.0)
    huurult = fields.Float(string='Huurult',default=0.0)
    rec_temp = fields.Float(string='Receive temperature',digits=(10,1),default=0.0)
    rec_self_weight = fields.Float(string='Receive self weight',digits=(10,4),default=0.0)
    rec_litr = fields.Float(string='Receive litr',default=0.0)
    rec_kg = fields.Float(string='Receive kg',default=0.0)
    rec_huurult = fields.Float(string='Receive huurult',default=0.0)
    total_litr = fields.Float(string=u'Ирсэн /литр/',default=0.0)
    total_kg = fields.Float(string=u'Ирсэн /кг/',default=0.0)
    niil_hariutsah = fields.Float(string=u'Нийлүүлэгч хариуцах',default=0.0)
    p_density_check = fields.Float(string=u'Падааны чанарын зөрүү',default=0.0)
    h_density_check = fields.Float(string=u'Хэмжилтийн чанарын зөрүү',default=0.0)
    thh = fields.Float(string=u'ТХХ кг',default=0.0)
    zuruu_kg = fields.Float(string=u'Хасах кг',default=0.0)
    p_kg = fields.Float(string=u'Падаан (кг)',default=0.0)
    own_kg = fields.Float(string=u'Зөвшөөрөгдөх алдаа',default=0.0)
    p_litr = fields.Float(string=u'Харгалзах падаан литр',default=0.0)
    h_litr = fields.Float(string=u'Харгалзах хэмжилт литр',default=0.0)
    zuruu_litr = fields.Float(string=u'Зөрүү литр',default=0.0)
    hariu_kg = fields.Float(string=u'Хариу падаан (кг)',default=0.0,digits=(10,2),)
    hariu_litr = fields.Float(string=u'Хариу падаан (литр)',default=0.0,digits=(10,2),)
    irvel_zohih_kg = fields.Float(string=u'Ирвэл зохих кг',default=0.0,digits=(10,2))
    irvel_zohih_litr = fields.Float(string=u'Ирвэл зохих литр',default=0.0,digits=(10,2))
    total_diff = fields.Float(string='Total diff',default=0.0)
    drive_diff = fields.Float(string='Drive diff',default=0.0)
    drive_litr = fields.Float(string=u'Тээвэрлэгч хариуцах литр',default=0.0)
    balance_diff = fields.Float(string='Balance diff',default=0.0)
    is_button_click = fields.Boolean(string=u'Тооцоолсон')
    type = fields.Selection([('winter',u'Өвөл'),
                             ('summer',u'Зун')],string=u'Төрөл')
    state = fields.Selection([('draft','Draft'),
                              ('send','Send'),
                              ('compute','Compute'),
                              ('receive','Receive')],string='State',default='draft')
    
    def action_to_draft(self):
        self.write({'state':'draft'})
    
   
    @api.onchange('torkh_id')
    def action_compute_size(self):
        for line in self:
            if line.torkh_id:
                line.torkh_size = line.torkh_id.size

    
    @api.onchange('self_weight','litr')
    def action_compute_kg(self):
        for line in self:
            if line.self_weight < 1.0 and line.torkh_size > 0.0:
                
                kg = line.self_weight*line.torkh_size
                dec_kg = Decimal(kg)
                line.kg = round(dec_kg,2)
            elif line.self_weight >=1.0:
                raise UserError(_(u"Хувийн жин 1 ээс бага байх ёстой !!!"))
         
        
    
    
    #@api.onchange('huurult','torkh_id')
    def action_to_hoorolt_suult(self,torkh_id,huurult):
        too1 = 0
        ih = 0
        ih_mm = 0
        baga = 0
        baga_mm = 0
        if huurult == 0:
           too1 = 0
        if huurult > 0:
           self._cr.execute("""
                          select litr from huurult_register where torkh_id = %s and mm = %s
                    """%(torkh_id,huurult))
           line_obj  = self._cr.dictfetchall()
           
           if line_obj:
              too1 = line_obj[0]['litr']
           
           if too1 == 0:
             self._cr.execute("""
              select litr, mm from huurult_register 
             where torkh_id = %s and mm > %s order by litr asc
             limit 1
                """%(torkh_id,huurult))
             ih_line_obj  = self._cr.dictfetchall()
             if ih_line_obj:
                ih = ih_line_obj[0]['litr']
                ih_mm = ih_line_obj[0]['mm']
                
             
             self._cr.execute("""
              
              select litr, mm from huurult_register 
             where torkh_id = %s and mm < %s order by litr desc
             limit 1
                """%(torkh_id,huurult))
             line_obj  = self._cr.dictfetchall()
             if line_obj:
                baga = line_obj[0]['litr']
                baga_mm = line_obj[0]['mm']
             ih = ih - baga
             ih_mm = ih_mm - baga_mm
             baga_mm = huurult - baga_mm
             if ih_mm == 0:
                raise UserError(u'Хөөрөлтийн хязгаараас давсан байна. Админ хэрэглэгчид хандана уу')
             if ih_mm >0.0:
                 too1 = ih/ih_mm*baga_mm + baga
            
        else:
           huurult = huurult *-1 
           
           self._cr.execute("""
                  select suult_litr from huurult_register where torkh_id = %s and suult_mm = %s
                    """%(torkh_id,huurult))
           line_obj  = self._cr.dictfetchall()
           if line_obj:
              too1 = line_obj[0]['suult_litr']
           if too1 == 0:
             self._cr.execute("""
              
              select suult_litr, suult_mm from huurult_register 
             where torkh_id = %s and suult_mm > %s order by suult_litr asc
             limit 1
                """%(torkh_id,huurult))
             line_obj  = self._cr.dictfetchall()
             if line_obj:
                ih = line_obj[0]['suult_litr']
                ih_mm = line_obj[0]['suult_mm']
             
             self._cr.execute("""
              
              select suult_litr, suult_mm from huurult_register
             where torkh_id = %s and suult_mm < %s order by suult_litr desc
             limit 1 """%(torkh_id,huurult))
             line_obj  = self._cr.dictfetchall()
             if line_obj:
                baga = line_obj[0]['suult_litr']
                baga_mm = line_obj[0]['suult_mm']
             ih = ih - baga
             ih_mm = ih_mm - baga_mm
             baga_mm = huurult - baga_mm
             if ih_mm >0.0:
                 too1 = ih/ih_mm*baga_mm + baga 
           too1 = too1*-1       
                     
            
        return too1
    
    
    def action_to_check(self):
        p_liter = 0 #hargalzah
        h_liter = 0 #hargalzah
        zuruu = 0
        koef = 0.00
        koef1 = 0
        p_tem = 0
        h_tem = 0
        p_tem_check = 0
        h_tem_check = 0
        
        p_huviin_jin = self.self_weight
        h_huviin_jin = self.rec_self_weight
        p_kg = 0.0
        thh = 0.0
        irvel_zohih_kg = 0.0
        irvel_zohih_l = 0.0
        
        total_liter = 0 #irsen
        total_kg = 0 #irsen
        zuruu_kg = 0
        zuruu_l = 0
        km = 0
        norm = 0 
        own_kg = 0 #thh
        for_driver = 0
        hariu_kg = 0
        hariu_liter = 0
        niiluulegch_hariutsah = 0 #+- 0.0005 hetersen p_density_check * obj.cask_line_id.capacity
        if self.shts_income_id.achilt_torol == 'gar':
           if self.torkh_id.id == False:
               raise UserError(_('Торх оруулаагүй байгаа тул оруулна уу'))
           else:
               h_liter = self.action_to_hoorolt_suult(self.torkh_id.id,self.rec_huurult)
               p_liter = self.action_to_hoorolt_suult(self.torkh_id.id,self.huurult)
           
           zuruu = h_liter - p_liter
           self._cr.execute("""
                      select koef from zalruulga where zal_a <= %s and zal_b >= %s
                    """%(self.self_weight,self.self_weight))
           zalruulga_obj  = self._cr.dictfetchall()
           if zalruulga_obj:
              koef = zalruulga_obj[0]['koef']
           
           self._cr.execute("""
                  select koef from zalruulga where zal_a <= %s and zal_b >= %s
                    """%(self.rec_self_weight,self.rec_self_weight))
           zalruulga1_obj  = self._cr.dictfetchall()
           if zalruulga1_obj:
              koef1 = zalruulga1_obj[0]['koef']
           p_tem = round((self.temp -20) * koef + self.self_weight,4)
           p_tem_check  = p_tem - self.density_20
           
           h_tem = (self.rec_temp -20) * koef1 + self.rec_self_weight
           h_tem_check  = self.density_20 - h_tem
           #if p_tem_check > 0.0005 or p_tem_check< -0.0005:
           #   p_huviin_jin = self.self_weight - p_tem_check
           #if h_tem_check > 0.0005 or h_tem_check< -0.0005:
           #  h_huviin_jin = self.rec_self_weight - h_tem_check
            
           h_huviin_jin = self.rec_self_weight
              
           p_kg = round(p_huviin_jin * self.torkh_size,4)
           niiluulegch_hariutsah = p_kg - self.kg
           
           total_liter = self.torkh_size + zuruu
           total_kg_calc = round(total_liter*h_huviin_jin,4)
           dec_total_kg = Decimal(total_kg_calc)
           total_kg = round(dec_total_kg,2)
           
           zai = self.env['distance.register'].search([('warehouse_id','=',self.shts_income_id.warehouse_id.id),('shts_id','=',self.shts_income_id.shts_id.id)])
           if zai:
               if len(zai)>1:
                   raise UserError(_(u'Зай км 1-ээс олон оруулсан байна. Хариуцсан инженерт хандана уу'))
               km = zai.name
           else:
               raise UserError(_('Агуулахаас ШТС хүртэлх зай км оруулаагүй байна'))
           
           
           self._cr.execute("""
                  
                  SELECT gasol_type
                   FROM product_session_config 
                  WHERE product_id = '%s' and shts_id = %s
                  and start_date <= '%s' and end_date >= '%s'
                    """%(self.product_id.id,self.shts_income_id.shts_id.id,self.shts_income_id.shift_date,self.shts_income_id.shift_date))
           gasol_types =  self._cr.dictfetchall()
           
           if not gasol_types:
               raise UserError(_(u'Улирлын тохиргоо хийгдээгүй байна. Хариуцсан инженерт хандана уу'))
           self._cr.execute("""
                  
                  SELECT norm
                   FROM thhnorm 
                  WHERE types = '%s' and km_a <= %s and km_b >= %s
                        
                    """%(gasol_types[0]['gasol_type'],km,km))
           norm_obj  = self._cr.dictfetchall()
           if norm_obj:
              norm = norm_obj[0]['norm']
           
           thh = round(p_kg * norm/100,2)
           irvel_zohih_kg = p_kg - thh
           if h_huviin_jin>0.0:
               irvel_zohih_l = round(irvel_zohih_kg/h_huviin_jin,2)
           else:
                raise UserError(_(u'Хүлээн авах хувийн жин оруулаагүй байна'))
           zuruu_l = total_liter - irvel_zohih_l
           zuruu_kg = zuruu_l * h_huviin_jin
           for_driver = zuruu_kg + own_kg
           
           
          
           #for_driver = for_driver * -1
           if for_driver >= 0:
              hariu_kg = Decimal(p_kg)
              hariu_liter = float(hariu_kg)/h_huviin_jin
           else:
              hariu_kg = Decimal(p_kg + for_driver)
              hariu_liter = float(hariu_kg)/h_huviin_jin
       
       
        else:
           if self.torkh_id.id == False:
               raise UserError(_('Торх оруулаагүй байгаа тул оруулна уу'))
           else:
               h_liter = self.action_to_hoorolt_suult(self.torkh_id.id,self.rec_huurult)
               p_liter = self.action_to_hoorolt_suult(self.torkh_id.id,self.huurult)
           zuruu = h_liter - p_liter
           self._cr.execute("""
                  
                  select koef from zalruulga where zal_a <= %s and zal_b >= %s
                        
                    """%(self.self_weight,self.self_weight))
           zalruulga_obj  = self._cr.dictfetchall()
           if zalruulga_obj:
              koef = zalruulga_obj[0]['koef']
           
           self._cr.execute("""
                  
                  select koef from zalruulga where zal_a <= %s and zal_b >= %s
                        
                    """%(self.rec_self_weight,self.rec_self_weight))
           zalruulga1_obj  = self._cr.dictfetchall()
           if zalruulga1_obj:
              koef1 = zalruulga1_obj[0]['koef']
           
           p_tem = round((self.temp - 20) * koef + self.self_weight,4)
           p_tem_check  = p_tem - self.density_20
           
           h_tem = (self.rec_temp -20) * koef1 + self.rec_self_weight
           h_tem_check  = self.density_20 - h_tem
           if p_tem_check > 0.0005 or p_tem_check< -0.0005:
              p_huviin_jin = self.self_weight - p_tem_check
           if h_tem_check > 0.0005 or h_tem_check< -0.0005:
              h_huviin_jin = self.rec_self_weight - h_tem_check
           p_kg = round(p_huviin_jin * self.torkh_size,2)
           niiluulegch_hariutsah = p_kg - self.kg
           
           total_liter = self.torkh_size + zuruu
        
           dec_total_kg = (total_liter * self.rec_self_weight)
           print("aaaaaaaaa",dec_total_kg,total_liter,self.rec_self_weight)
           total_kg = round(dec_total_kg,2)
           
           own_kg = p_kg * 0.23/100
           
           irvel_zohih_kg = p_kg - own_kg
           irvel_zohih_l = round(irvel_zohih_kg/self.rec_self_weight,2)
           zuruu_l = total_liter - irvel_zohih_l
           zuruu_kg = zuruu_l * h_huviin_jin
           for_driver = zuruu_kg + own_kg
           #for_driver = for_driver * -1
           if for_driver >= 0:
               
               if p_kg >0.0:
                   
                  hariu_kg = Decimal(p_kg)
                  if h_huviin_jin >0.0 and hariu_kg >0:
                      
                      hariu_liter = float(hariu_kg)/h_huviin_jin
                  else:
                      hariu_liter = float(hariu_kg)
               else:
                  hariu_kg = Decimal(p_kg + for_driver)
                  if h_huviin_jin>0.0 and hariu_kg>0:
                      hariu_liter = float(hariu_kg)/h_huviin_jin
                  else:
                      hariu_liter = hariu_kg
        self.write({
                   'p_litr':p_liter,
                   'h_litr':h_liter,
                   'drive_diff':round(for_driver,2),
                   'niil_hariutsah':round(p_tem_check*self.torkh_size,2),
                   'p_density_check':round(p_tem_check,2),
                   'h_density_check':round(h_tem_check,2),
                   'thh':round(thh,2),
                   'hariu_litr':round(hariu_liter,2),
                   'hariu_kg':round(hariu_kg,2),
                   'p_kg':p_kg,
                   'zuruu_kg': round(zuruu_kg,2),
                   'zuruu_litr': round(zuruu_l,2),
                   'own_kg': own_kg,
                   'total_kg': total_kg, #irsen
                   'total_litr':total_liter, #irsen
                   'irvel_zohih_kg':round(irvel_zohih_kg,2),
                   'irvel_zohih_litr':round(irvel_zohih_l,2),
                   'is_button_click': True,
                   })
    
    
    