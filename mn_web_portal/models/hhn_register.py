# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date



class HhnRegister(models.Model):
    _name = 'hhn.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    type = fields.Selection([('income',u'Орлого'),
                             ('transit',u'Тээвэрлэлт'),
                             ('save',u'Хадгалалт')],string='Type',required=True)
    session = fields.Selection([('summer',u'Зун'),
                                ('winter',u'Өвөл')],string='Session',required=True)
    region = fields.Selection([('one',u'1-р бүс сэрүүн'),
                               ('two',u'2-р бүс дулаан'),
                               ('three',u'1-р бүс хүйтэн'),
                               ('four',u'2-р бүс хүйтэвтэр')],string=u'Бүс',required=True)
    name = fields.Float(string='Name',required=True)