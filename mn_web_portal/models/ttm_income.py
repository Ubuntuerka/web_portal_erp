# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
class TtmIncomeRegister(models.Model):
    _name = 'ttm.income.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'date desc'
    
    @api.depends("shts_id")
    def action_shts_company_id(self):
        for line in self:
            company_id = False
            if line.shts_id.company_id:
                company_id= line.shts_id.company_id.id
            line.company_id = company_id
    
    
    company_id = fields.Many2one('res.company',string=u'Компани'
                                 ,compute="action_shts_company_id")
    name = fields.Many2one('shts.register',string=u'ГТБА')
    invoice_number = fields.Char(string=u'Нэхэмжлэхийн дугаар')
    reg_user_id = fields.Many2one('res.users',string=u'Бүртгэсэн ажилтан',default=lambda self: self.env.user)
    date = fields.Date(string=u'Огноо', default=fields.Date.context_today)
    partner_id = fields.Many2one('res.partner', string='Жолооч', required=True,domain=[('drive_true','=','True')])
    drive_id = fields.Many2one('hr.employee',string=u'Жолооч')
    car_number_id = fields.Many2one('car.register',string=u'Машины дугаар')
    send_date = fields.Date(string=u'Илгээсэн огноо')
    confirm_date = fields.Datetime(string=u'Хүлээн авсан огноо')
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    shift = fields.Integer(string=u'Ээлж')
    shift_exp = fields.Integer(string=u'Ээлж')
    infor_number = fields.Char(string='Infor number')
    line_id = fields.One2many('ttm.income.register.line','income_id',string='Line')
    other_exp = fields.Boolean(string=u'Бусад зарлага')
    state = fields.Selection([('draft',u'Ноорог'),
                              ('send',u'Илгээсэн'),
                              ('return',u'Буцаагдсан'),
                              ('confirm',u'Хүлээн авсан')],string=u'Төлөв',default='draft')
    
    def action_draft(self):
        qty_obj = self.env['product.qty']
        for line in self:
            if line.state !='confirm':
                self.write({'state':'draft'})
            else:
                qty_ids = qty_obj.search([('income_id','=',line.id)])
                for qt in qty_ids:
                    qt.unlink()
                self.write({'state':'send'})
    
    
    def action_return(self):
        for line in self:
            if line.state!='confirm':
                self.write({'state':'return'})
            else:
                raise UserError(u"Батлагдсан тул буцаах боломжгүйг анхаарна уу!")
    
    def action_send(self):
        qty_obj = self.env['product.qty']
        if self.other_exp == True:
            for line in self.line_id:
                qty_data = qty_obj.search([('shts_id','=',self.name.id),('product_id','=',line.product_id.id),('reg_date','<=',self.send_date)])
                qty = 0.0
                if qty_data:
                    for qt in qty_data:
                        if qt.type == 'in':
                            qty +=qt.qty
                        else:
                            qty +=qt.qty
                if qty < line.send_qty:
                    raise UserError(_(u"%s %s барааны үлдэгдэл хүрэхгүй байгаа тул шалгана уу!")% (line.product_id.default_code, line.product_id.name))
                
                ttm_line = self.env['ttm.income.register.line']
                line.write({
                                         'receive_qty':line.send_qty,
                                    })
                
        self.write({'state':'send',
                   # 'send_date':fields.Date.today()
                    })
    
    def action_confirm(self):
        qty_obj = self.env['product.qty']
        shts_line_obj = self.env['shts.register.line']
        shift_obj = self.env['shift.working.register']
        shift = shift_obj.search([('shift_date','=',str(self.confirm_date)[:10]),('shts_id','=',self.shts_id.id),('shift','=',self.shift)],limit=1)
        if shift:
            if shift.state !='draft':
                raise UserError(_(u"ШТС ноорог байх үед орлого авахыг анхаарна уу"))
        else:
            raise UserError(_(u"ТТМ Орлогын огноонд ээлж үүсээгүй байгаа тул ээлж үүсгэнэ үү"))
        for line in self:
            line.write({
                        'state':'confirm',
                     #   'confirm_date':fields.Date.today()
                        })
            for list in line.line_id:
                shts_line_ids = shts_line_obj.search([('oil_shts_id','=',line.shts_id.id),('product_id','=',list.product_id.id)])
                if not shts_line_ids:
                    shts_line_obj.create({'oil_shts_id':line.shts_id.id,
                                          'shts_code':line.shts_id.pos_number,
                                          'product_id':list.product_id.id,
                                          'price':0.0,
                                          'product_code':list.product_id.default_code,
                                          'product_name':list.product_id.name,
                                          'barcode':list.product_id.ebarimt_code,
                                          'vattype':list.product_id.vat_taxtype,
                                          'uom':list.product_id.uom_id.local_code,
                                          'type':'oil'})
  
                    
                qty_obj.create({'qty':list.send_qty*(-1),
                                    'product_id':list.product_id.id,
                                    'shts_id':line.name.id,
                                    'reg_date':line.confirm_date,
                                    'type':'out',
                                    'income_id':line.id,
                                    'company_id':line.company_id.id})
                
                qty_obj.create({'qty':list.receive_qty,
                                    'product_id':list.product_id.id,
                                    'shts_id':line.shts_id.id,
                                    'reg_date':line.confirm_date,
                                    'type':'in',
                                    'income_id':line.id,
                                    'company_id':line.company_id.id})
            
    
    
class TtmIncomeRegisterLine(models.Model):
    _name = 'ttm.income.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    @api.model
    def _product_id(self):
        product = []
        shts_obj = self.env['shts.register']
        shts_id = shts_obj.browse(self.env.user.warehouse_id.id)
        for line in shts_id.oil_line_ids:
            product.append(line.product_id.id)
        return [('id', 'in', product)]
    
    
    company_id = fields.Many2one('res.company','Company'
                                 ,default=lambda self: self.env['res.company']._company_default_get('ttm.income.register.line'))
    product_id = fields.Many2one('product.product',string='Бүтээгдэхүүн',required=True, domain=_product_id, states={'draft': [('readonly', False)]},readonly=True)
    code = fields.Char(string=u'Код',required=True, states={'draft': [('readonly', False)]},readonly=True)
    uom_id = fields.Many2one('uom.uom',string=u'Хэмжих нэгж',required=True, states={'draft': [('readonly', False)]},readonly=True)
    send_qty = fields.Float(string=u'Илгээсэн тоо',required=True, states={'draft': [('readonly', False)]},readonly=True)
    receive_qty = fields.Float(string=u'Хүлээн авсан тоо')
    diff = fields.Float(string=u'Зөрүү',compute="_diff_qty",store=True)
    income_id = fields.Many2one('ttm.income.register',string='Income')
    state = fields.Selection([('draft','Draft'),
                              ('send','Send'),
                              ('confirm','Confirm')],string='State',default='draft')
    shift_id = fields.Many2one('shift.working.register',string='Shift register')
    
    basic_uom_qty = fields.Float(u'Тоо хэмжээ(л)',digits=(1, 1),help='Литр хэмжих нэгжээрх тоо хэмжээ')
    move_factor = fields.Float('Factor',digits=(4, 4))
    location_id = fields.Many2one('stock.location', 'Source Location', required=True, select=True, auto_join=True,
                                   states={'confirm': [('readonly', True)]}, help="Sets a location if you produce at a fixed location. This can be a partner location if you subcontract the manufacturing operations."),

    @api.depends('send_qty','receive_qty')
    def _diff_qty(self):
        for line in self:
            diff = 0.0
            if line.send_qty > 0.0 and line.receive_qty >= 0.0:
                diff = line.send_qty-line.receive_qty
            line.diff = diff

    @api.onchange('product_id')
    def onchange_product_id(self):
        for line in self:
            line.code = line.product_id.default_code
            line.uom_id = line.product_id.uom_id.id
            

