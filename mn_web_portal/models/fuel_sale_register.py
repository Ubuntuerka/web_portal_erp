# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class FuelSaleRegister(models.Model):
    _name = 'fuel.sale.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('fuel.sale.register'),required=True)
    reg_user_id = fields.Many2one('res.users',string='Register user',default=lambda self: self.env.user)
    date = fields.Date(string='Date', default=fields.Date.context_today)
    line_ids = fields.One2many('fuel.sale.register.line','fuel_sale_id',string='Fuel sale line')
    state = fields.Selection([('draft','Draft'),
                              ('done','Done')],string='State',default='draft')
    

    
    
class FuelSaleRegisterLine(models.Model):
    _name = 'fuel.sale.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env['res.company']._company_default_get('fuel.sale.register'))
    product_id = fields.Many2one('product.product',string='Product')
    date = fields.Date(string='Date', default=fields.Date.context_today)
    first_balance_height = fields.Float(string='First balance height')
    first_balance_temp = fields.Float(string='First balance temp')
    first_balance_litr = fields.Float(string='First balance litr')
    first_balance_kg = fields.Float(string='First balance kg')
    income_litr = fields.Float(string='Income litr')
    income_kg = fields.Float(string='Income kg')
    expense_dhj = fields.Float(string='Expense DHJ')
    expense_total = fields.Float(string='Expense total')
    last_balance_height = fields.Float(string='Last balance height')
    zalin = fields.Float(string='Zalin')
    expense_litr = fields.Float(string='Expense litr')
    expense_kg = fields.Float(string='Expense kg')
    have_litr = fields.Float(string='Have litr')
    have_kg = fields.Float(string='Have kg')
    last_balance_weight = fields.Float(string='Last balance weight')
    last_balance_temp = fields.Float(string='Last balance temp')
    last_balance_litr = fields.Float(string='Last balance litr')
    last_balance_kg = fields.Float(string='Last balance kg')
    diff_litr = fields.Float(string='Diff litr')
    diff_kg = fields.Float(string='Diff kg')
    fuel_sale_id = fields.Many2one('fuel.sale.register',string='Fuel sale')
    shift_id = fields.Many2one('shift.working.register',string='Shift register')
    
    