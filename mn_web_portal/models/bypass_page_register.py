# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class BypassPageRegister(models.Model):
    _name = 'bypass.page.register'

    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    #shts_id = fields.Many2one('shts.register', string='ШТС', required=True,
    #                          default=lambda self: self.env.user.warehouse_id)
    department_id = fields.Many2one('hr.department', 'Газар/нэгж', required=True)
    employee = fields.Many2one('hr.employee', string='Ажилтан')
    date = fields.Date(string=u'Огноо', required=True)
    amount = fields.Float(string=u'Тооцоо', required=True)
    description = fields.Char(string='Тайлбар')
    convert_employee = fields.Many2one('res.users', string='Шилжүүлсэн ажилтан')
    create_user_id = fields.Many2one('res.users', string=u'Үүсгэсэн ажилтан', readonly=True,
                                     default=lambda self: self.env.user)

    @api.onchange('employee')  # , 'rank'
    def onchange_employee_id(self):
        for line in self:
            line.department_id = line.employee.department_id.id
            line.company_id = line.employee.company_id.id

