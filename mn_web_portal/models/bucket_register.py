# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class BucketRegister(models.Model):
    _name = 'bucket.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company','Company',
                                 default=lambda self: self.env['res.company']._company_default_get('bucket.register'),required=True)
    name = fields.Char(string='Bucket name',required=True)
    size = fields.Float(string='Size',required=True)
    car_number_id = fields.Many2one('car.register','Car number')
    