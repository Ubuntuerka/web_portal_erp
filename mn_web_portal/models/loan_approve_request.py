# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree

DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


class LoanApproveRequest(models.Model):
    _name = 'loan.approve.request'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'reg_date desc, name desc'

    company_id = fields.Many2one('res.company', string='Company', required=True)
    name = fields.Char(string='Name', readonly=True,default=lambda self: self.env['ir.sequence'].next_by_code('loan.approve.request')) #
    send_date = fields.Date(string='Send date', required=True, default=lambda self: fields.datetime.now())
    reg_date = fields.Datetime(string='Register date', required=True, readonly=True, default=lambda self: fields.datetime.now())
    reg_user_id = fields.Many2one('res.users', string='Register user', readonly=True, default=lambda self: self.env.user,
                                  required=True)
    shift_date = fields.Date(string='Shift date')
    shts_id = fields.Many2one('shts.register', string='Shts',required=True,
                              default=lambda self: self.env.user.shts_ids)
    partner_id = fields.Many2one('res.partner', string='Partner', required=True)
    shift = fields.Selection([('1', u'1'),
                              ('2', u'2')], string=u'Shift')
    state = fields.Selection([('draft', 'Draft'),
                              ('send', 'Send'),
                              ('return', 'Return'),
                              ('done', 'Done')], string='State', default='draft')
    shift_code = fields.Many2one('shift.working.register', string='Code', readonly=True)
    confirm_date = fields.Datetime(string='Confirm date', readonly=True)
    confirm_user_id = fields.Many2one('res.users', string='Confirm user', readonly=True)
    mobile_phone = fields.Char(string='Mobile', readonly=False, related_sudo=False, required=True)
    line_ids = fields.One2many('loan.approve.request.line', 'approve_id', string='Loan approve request line')
    origin_id = fields.Char(string=u'Эх баримт ID')
    total_per_amount = fields.Float(string=u'Олгосон нийт дүн', compute="action_total_per_amount")
    

    @api.onchange('shts_id')
    def onchange_shts_id(self):
        self.company_id = self.shts_id.company_id.id
                
    def action_total_per_amount(self):
        for line in self:
            total_amount = 0.0
            for lins in line.line_ids:
                total_amount +=lins.per_amount
                
            line.total_per_amount = total_amount
            

    def action_send(self):

        for line in self.line_ids:
            if line.type == 'litr':
                if line.qty > 0.0:
                    self.write({'state': 'send'})
                    line.shts_id = self.shts_id.id
                else:
                    raise UserError(u" Зөвшөөрсөн литр утгыг заавал оруулна уу")
            elif line.type == 'amount':
                if line.total_amount > 0.0:
                    self.write({'state': 'send'})
                    line.shts_id = self.shts_id.id
                else:
                    raise UserError(u"Зөвшөөрсөн нийт дүн утгыг заавал оруулна уу")
            elif line.type == 'full':
                self.write({'state': 'send'})
                line.shts_id = self.shts_id.id



    def action_return(self):
        self.write({'state': 'draft'})
        
    def action_cancel(self):
        self.write({'state': 'return'})

    def action_done(self):
        shift_working_obj = self.env['shift.working.register']
        loan_obj = self.env['loan.document.register.line']
        expense_obj = self.env['expense.type']
        sh_pro_line_obj = self.env['shift.product.line']
        for li in self:
            if not li.shift_date and li.shift:
                raise UserError(u"Ээлжийн огноо болон ээлж талбарт утга оруулна уу!!")
            shift_id = shift_working_obj.search(
                [('state', '=', 'draft'), ('shts_id', '=', li.shts_id.id), ('shift', '=', li.shift),
                 ('shift_date', '=', li.shift_date)])
            pay_id = expense_obj.search([('company_id', '=', li.company_id.id), ('type', '=', 'loan')])
            if not pay_id:
                raise UserError(u"Зээл төрөлтэй төлбөрийн хэлбэр олдохгүй байна шалгана уу!!")
            if shift_id:
                li.shift_code = shift_id.id

                for line in li.line_ids:
                    t_amount = 0
                    pro_lines = sh_pro_line_obj.search([('shift_id', '=', shift_id.id),
                                                        ('product_id', '=', line.product_id.id)], limit=1)
                    if line.type == 'amount':
                        if line.total_amount > 0.0 and line.per_amount > 0.0:
                            line.zoruu = line.per_amount - line.total_amount
                            if line.zoruu > 0.0:
                                raise UserError(u"Олгосон нийт дүн их байгаа тул борлуулалтын менежертэй хандана уу!!")
                            else:
                                t_amount = line.per_amount
                                self.write({'state': 'done'})
                        else:
                            raise UserError(u"Зөвшөөрсөн болон Олгосон нийт дүнгийн утга оруулсан эсэхийг шалгана уу!!")
                    elif line.type == 'litr':
                        if line.qty > 0.0 and line.per_qty > 0.0:
                            line.zoruu = line.per_qty - line.qty
                            if line.zoruu > 0.0:
                                raise UserError(u"Олгосон литр их байгаа тул борлуулалтын менежертэй хандана уу!!")
                            else:
                                t_amount = line.per_qty * pro_lines.sale_price
                                self.write({'state': 'done'})
                        else:
                            raise UserError(u"Зөвшөөрсөн болон Олгосон литрын утга оруулсан эсэхийг шалгана уу!!")
                    elif line.type == 'full':
                        if line.per_amount > 0.0:
                            t_amount = line.per_amount
                            self.write({'state': 'done'})
                        else:
                            raise UserError(u"Олгосон нийт дүн талбарт утга оруулсан эсэхийг шалгана уу!!")

                    loan_id = loan_obj.search([('product_id', '=', line.product_id.id), ('can_id', '=', line.can_id.id),
                                               ('partner_id', '=', li.partner_id.id), ('litr', '=', line.per_qty),
                                              ('car_number_id', '=', line.car_number),('shift_id', '=', shift_id.id),('desc', '=', li.name)])
                    loan_id1 = loan_obj.search([('id', '!=', self.ids)], order="id desc", limit=1)
                    loan = int(loan_id1) + 1

                    if not loan_id:
                        loan_obj.create({
                            'product_id': line.product_id.id,
                            'product': line.product_id.name,
                            'partner_id': li.partner_id.id,
                            'company_id': li.company_id.id,
                            # 'litr': line.per_qty,
                            'car_number_id': line.car_number,
                            'shift_id': shift_id.id,
                            'shts_code': li.shts_id.code,
                            'drive_id': line.drive_id,
                            'can_id': line.can_id.id,
                            'price': pro_lines.sale_price,
                            'name': loan,
                            'desc': li.name,
                            'expense_type_id': pay_id.id,
                            'total_amount': t_amount,
                        })
                    else:
                        raise UserError(u"Тухайн зөвшөөрөл ээлж дээр аль хэдийн бүртгэгдсэн байна!!")

            else:

                raise UserError(u"Тухайн огноонд таарах ээлж байхгүй эсвэл ээлж ноорог төлөвт байхгүй!!")






class LoanApproveRequestLine(models.Model):
    _name = 'loan.approve.request.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    company_id = fields.Many2one('res.company', string='Company')
    product_id = fields.Many2one('product.product', string='Product', required=True, domain="[('prod_type','=','gasol')]")
    shts_id = fields.Many2one('shts.register', string='Shts')
    car_number = fields.Char(string='Car number', required=True)
    drive_id = fields.Char(string='Drive', required=True)
    can_id = fields.Many2one('can.register', string='Can') # ,domain=[('shts_id','=',self.shts_id)]
    price = fields.Float(string='Price')
    qty = fields.Float(string='Литр')
    type = fields.Selection([('full', u'Банк дүүргэх'),
                             ('amount', u'Мөнгөн дүн'),
                             ('litr', u'Литр оруулах')], string=u'Төрөл', required=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('send', 'Send'),
                              ('return', 'Return'),
                              ('done', 'Done')], string='State', default='draft',compute="action_state")
    per_qty = fields.Float(string=u'Олгосон литр')
    total_amount = fields.Float(string='Total amount') #, compute="action_total_price"
    pro_type = fields.Selection([('gasol', 'Gasol'),  # Шатахуун
                                 ('oil', 'Oil'), ], string='Product type')  # Тос
    per_amount = fields.Float(string=u'Олгосон нийт үнэ')
    zoruu = fields.Float(string=u'Зөрүү')
    approve_id = fields.Many2one('loan.approve.request', string='Loan request approve')

    @api.onchange('product_id')
    def onchange_product_id(self):
        for line in self.approve_id.shts_id.line_ids:
            if self.product_id.id == line.product_id.id:
                self.price = line.price

    @api.onchange('shts_id')
    def onchange_shts_id(self):
        for line in self.approve_id.shts_id:
            self.shts_id = line.id

    def action_state(self):
        for line in self:
            if line.approve_id:
                line.state = line.approve_id.state


    # def action_total_price(self):
    #     for line in self:
    #         if line.price > 0.0 and line.qty > 0.0:
    #             line.total_amount = round(line.qty * line.price, 2)
    #         else:
    #             line.total_amount = 0.0





