# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class ThhData(models.Model):
    _name = 'thh.data'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string=u'Компани'
                                 ,default=lambda self: self.env['res.company']._company_default_get('thh.data'),required=True)
    name = fields.Char(string=u'Нэр',required=True)
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True)
    start_period = fields.Integer(string=u'Эхлэх сар',required=True)
    end_period = fields.Integer(string=u'Дуусах сар',required=True)
    gasol_type = fields.Selection([('2',u'ДТ Өвөл'),
                                   ('3',u'ДТ Зун'),
                                   ('1',u'Авто бинзен')
                                   ],string=u'Төрөл',required=True)
    type = fields.Selection([('winter',u'Өвөл'),
                             ('summer',u'Зун')],string=u'Улирал',required=True)
    rec_value = fields.Float(string=u'Хүлээн авах үеийн',required=True,digits=(10, 4))
    save_value = fields.Float(string=u'Хадгалах үеийн /30 хоног/',required=True,digits=(10, 4))
    deal_value = fields.Float(string=u'Түгээх үеийн',required=True,digits=(10, 4))
    transport_value = fields.Float(string=u'Тээвэрлэлтийн үеийн',required=True,digits=(10, 4))
    reg_date = fields.Date(string=u'Бүртгэсэн огноо', default=fields.Date.context_today,required=True)
    
    
    