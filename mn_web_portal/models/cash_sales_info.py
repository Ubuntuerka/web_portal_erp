# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class CashSalesInfo(models.Model):
    _name = 'cash.sales.info'
    
    
    company_id = fields.Many2one('res.company',string=u'Компани')
    sale_type = fields.Many2one('expense.type',string=u'Төрөл')
    work_center = fields.Many2one('expense.type',string='Work center')
    partner_id = fields.Many2one('res.partner',string=u'Харилцагч')
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн')
    litr = fields.Float(string=u'Литр')
    kg = fields.Float(string=u'Кг')
    qty = fields.Float(string=u'Тоо')
    price = fields.Float(string=u'Нэгж')
    can_id = fields.Many2one('can.register',string=u'Сав')
    self_weight = fields.Float(string=u'Хувийн жин',digits=(10,4))
    total = fields.Float(string=u'Дүн')
    shift_id = fields.Many2one('shift.working.register',string=u'Ээлж')
    is_infor = fields.Boolean(string=u'ЛН Төлөв',default=False)
    product = fields.Char(string=u'Бүтээгдэхүүн',compute="action_product")
    itemId = fields.Char(string=u'Санхүүгийн код',compute="action_product")
    petrol_station = fields.Char(string=u'ШТС',compute="action_shts_id")
    LocationId = fields.Char(string=u'ШТС код',compute="action_shts_id")
    
        
    
    def action_product(self):
        for line in self:
            line.product = line.product_id.name
            line.itemId = line.product_id.fin_code
    
    
    def action_shts_id(self):
        for line in self:
            line.petrol_station = line.shift_id.shts_id.name
            line.LocationId = line.shift_id.shts_id.shts_code