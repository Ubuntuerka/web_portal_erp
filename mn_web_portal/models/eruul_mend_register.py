# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class EruulMend(models.Model):
    _name = 'eruul.mend'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    department_id = fields.Many2one('hr.department', 'Газар/нэгж')
    shts_id = fields.Many2one('shts.register', string='Агуулах/ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id)
    shift_date = fields.Date(string=u'Ээлжийн огноо',required=True, default=fields.Date.context_today)
    spirt = fields.Integer(string=u'Спиртийн уусмал', required=True) # True geh ym bol 1 gesen limit zaaj bolno
    iod = fields.Integer(string=u'Иодын уусмал', required=True, store=True)
    hovon = fields.Integer(string=u'Эмнэлгийн хөвөн', required=True)
    bint = fields.Integer(string=u'Эмнэлгийн бинт', required=True)
    boolt = fields.Integer(string=u'Уян боолт', required=True)
    ts_lent = fields.Integer(string=u'Цаасан лент', required=True)
    hu_lent = fields.Integer(string=u'Хуруу лент', required=True)
    beelii = fields.Integer(string=u'Эмнэлгийн бээлий', required=True)
    haalt = fields.Integer(string=u'Амны хаалт', required=True)
    tailbar = fields.Char(string=u'Тайлбар')
    reg_user_id = fields.Many2one('res.users', string=u'Бүртгэсэн ажилтан', default=lambda self: self.env.user,
                                  required=True)
