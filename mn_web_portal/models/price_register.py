# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
import time
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from dateutil import relativedelta
DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

class PriceChangeRegister(models.Model):
    _name = 'price.change.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'start_date desc'
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('price.register'))
    name = fields.Char(string='Change price number',default=lambda self: self.env['ir.sequence'].next_by_code('price.register'))
    category_id = fields.Many2many('product.category',string=u'Бүтээгдэхүүний ангилал')
    is_category = fields.Boolean(string=u'Бүтээгдэхүүний ангилал сонгох эсэх')
    product_id = fields.Many2many('product.product',string=u'Бүтээгдэхүүн',domain="['|',('categ_id','=',category_id),('prod_type','=',type)]")
    price_group_id = fields.Many2one('price.group',string=u'Үнийн грүпп',required=True)
    shts_id = fields.Many2many('shts.register','price_change_rel','price_id','shts_id','Shts')
    region_id = fields.Many2many('shts.region','price_region_rel','price_id','region_id','Region')
    is_shts = fields.Boolean(string='Is shts')
    shift_date = fields.Date(string='Shift date',default=fields.Date.context_today)
    date = fields.Date(string='Date', default=fields.Date.context_today)
    start_date = fields.Datetime(string='Start date',default=datetime.now().strftime('%Y-%m-%d 00:30:00'),required=True)
    confirm_date = fields.Datetime(string='Confirm date',readonly=True)
    reg_user_id = fields.Many2one('res.users',string='Register user',default=lambda self: self.env.user)
    is_type = fields.Boolean(string=u'Төрөл сонгох эсэх')
    type = fields.Selection([('gasol',u'Шатахуун'),
                             ('oil',u'Тос'),
                             ('service',u'Үйлчилгээ')],string='Type',default='gasol')
    line_ids = fields.One2many('price.change.register.line','price_change_id',strin='Price change line')
    oil_line_ids = fields.One2many('price.change.register.line','oil_change_id',string=u'Price oil change')
    line_gasol_ids = fields.One2many('price.gasol.register','price_change_id',string='Price gasol register')
    state = fields.Selection([('draft',u'Ноорог'),
                              ('send',u'Илгээгдсэн'),
                              ('confirm',u'Батлагдсан'),
                              ('done',u'Дууссан')],string=u'Төлөв',default='draft')
    
    
    @api.onchange('start_date')
    def onchange_shift_date(self):
        self.shift_date = self.start_date
        
    def action_return(self):
        shts_price_obj = self.env['price.register.shts']
        for lines in self.line_ids:
            if lines.shts_id:
                for lis in lines.shts_id.line_ids:
                    if lis.product_id.id == lines.product_id.id:
                        lis.write({'price':lines.old_price})
        price_id = shts_price_obj.search([('main_price_id','=',self.id)])
        for line in price_id:
            if line.state == 'send':
                line.write({'state':'cancel'})
            else:
                raise UserError(u"Үнэ өөрчлөгдсөн ШТС байгаа тул цуцлах боломжгүй.") 
        self.write({'state':'draft'})
    
    def unlink(self):
        if self.state not in ('draft'):
            raise UserError(u"Ноорог биш тул устгах боломжгүй.") 
        else:
            for li in self.line_ids:
                li.unlink()
            return super(PriceChangeRegister, self).unlink()
    
    def action_done(self):
        self.write({'state':'done'})
        
    
    def action_send(self):
        price_shts_line_obj = self.env['price.register.shts.line']
        price_shift_obj = self.env['price.register.shts']
        for line in self:
            if line.type == 'gasol':
                for shts in line.line_ids.shts_id:
                    price_shift_id = price_shift_obj.create({
                                            'shts_id':shts.id,
                                            'company_id':line.shts_id.company_id.id,
                                            'region_id':shts.region_id.id,
                                            'price_group_id':line.price_group_id.id,
                                            'is_type':line.is_type,
                                            'type':line.type,
                                            'state':'send',
                                            'main_price_id':self.id,
                                            'reg_user_id':line.reg_user_id.id,
                                            'date':line.date,
                                            'shift_date':line.shift_date,
                                            'start_date':line.start_date,
                                            })
                    for pro in line.line_ids:
                        for sh in pro.shts_id:
                            if shts.id == sh.id:
                                price_shts_line_obj.create({
                                                            'product_id':pro.product_id.id,
                                                            'price_change_id':price_shift_id.id,
                                                            'shts_id':shts.id,
                                                            'shift':pro.shift,
                                                            'old_price':pro.old_price,
                                                            'new_price':pro.new_price,
                                                            'type':pro.type,
                                                           # 'can_id':pro.can_id.id,
                                                            })
            elif line.type =='oil':
                for shts in line.price_group_id.shts_ids:
                    price_shift_id = price_shift_obj.create({
                                                'shts_id':shts.id,
                                                'company_id':line.shts_id.company_id.id,
                                                'region_id':shts.region_id.id,
                                                'price_group_id':line.price_group_id.id,
                                                'is_type':line.is_type,
                                                'type':line.type,
                                                'state':'send',
                                                'main_price_id':self.id,
                                                'reg_user_id':line.reg_user_id.id,
                                                'date':line.date,
                                                'shift_date':line.shift_date,
                                                'start_date':line.start_date,
                                                })
     
                    product = []
                    if self.oil_line_ids.product_id:
                        for pro in self.oil_line_ids.product_id:
                            product.append(pro.id)
                    shts_line_obj = self.env['shts.register.line']
                    if len(product)==1:
                        shts_line_ids = shts_line_obj.search([('product_id','=',product[0]),('oil_shts_id','=',shts.id)])
                    else:
                        shts_line_ids = shts_line_obj.search([('product_id','in',product),('oil_shts_id','=',shts.id)])
                  
                    
                    for lis in shts_line_ids:
                        line_obj = self.env['price.change.register.line']
                        price_shts_line_obj = self.env['price.register.shts.line']
                        line_data = line_obj.search([('product_id','=',lis.product_id.id),('oil_change_id','=',self.id)])
                        
                        if line_data:
                            
                            price_shts_line_obj.create({
                                                            'product_id':lis.product_id.id,
                                                            'oil_change_id':price_shift_id.id,
                                                            'shts_id':shts.id,
                                                            'shift':line_data.shift,
                                                            'old_price':lis.price,
                                                            'new_price':line_data.new_price,
                                                            'type':line_data.type,
                                                            #'can_id':line_data.can_id.id,
                                                            })   
                                   
            
            if line.line_ids:
                
                for lis in line.line_ids:
                    if lis.shift==0.0:
                        raise UserError(u"Ээлжийн утгыг оруулаагүй байгаа тул оруулана уу.") 
                    if lis.new_price == 0.0:
                        raise UserError(u"Өөрчлөх үнэ 0 байгаа тул та үнэ оруулна уу.") 
                    else:
                        self.write({'state':'send'})
            elif line.oil_line_ids:
                for lis in line.oil_line_ids:
                    if lis.shift==0.0:
                        raise UserError(u"Ээлжийн утгыг оруулаагүй байгаа тул оруулана уу.") 
                    
                    if lis.new_price == 0.0:
                        raise UserError(u"Өөрчлөх үнэ 0 байгаа тул та үнэ оруулна уу.") 
                    else:
                        self.write({'state':'send'})
            else:
               raise UserError(u"Мөр хоосон байгаа тул илгээх боломжгүй.")  
        
    def action_confirm(self):
        shts_line_obj = self.env['shts.register.line']
        shift_obj = self.env['shift.working.register']
        product_obj = self.env['shift.product.line']
        self.write({'state':'confirm',
                    'confirm_date':fields.Date.today(),
                    })

            
    def action_data_import(self):
        line_obj = self.env['price.change.register.line']
        shts_line_obj = self.env['shts.register.line']
        price_obj = self.env['price.gasol.register']
        shift_obj = self.env['shift.working.register']
        break_obj = self.env['mile.target.register']
        product_obj = self.env['product.product']
        if self.is_category==True and self.price_group_id:
            if self.type == 'gasol':
                shts_id = []
                if self.price_group_id.shts_ids:
                    for line in self.price_group_id.shts_ids:
                        shts_id.append(line.id)
                else:
                    raise UserError(u"Үнийн бүлэг дээр ШТС сонгогдоогүй байгаа тул сонгоно уу.") 
                categ_id = []
                for cat in self.category_id:
                    categ_id.append(cat.id)
                if len(shts_id)==1:
                    if len(categ_id)==1:
                        self.env.cr.execute(""" select sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id,crl.break_id, crl.can_id  from 
                                                     shts_register_line as sl left join shts_register as sh on sh.id = sl.shts_id
                                                     left join can_register as cr on cr.shts_id = sh.id
                                                     left join can_register_line as crl on crl.can_id = cr.id
                                                     left join product_product as pp on pp.id = crl.product_id 
                                                     left join product_template as pt on pt.id = pp.product_tmpl_id and sl.product_id = crl.product_id
                                                     where sl.type = '%s' and sh.id = %s and pt.categ_id = %s and crl.active = true
                                                     group by sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id ,crl.break_id , crl.can_id
                                                    """%(self.type,shts_id[0],categ_id[0]))
                        data = self._cr.dictfetchall()
                    else:
                        self.env.cr.execute(""" select sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id,crl.break_id, crl.can_id  from 
                                                     shts_register_line as sl left join shts_register as sh on sh.id = sl.shts_id
                                                     left join can_register as cr on cr.shts_id = sh.id
                                                     left join can_register_line as crl on crl.can_id = cr.id
                                                     left join product_product as pp on pp.id = crl.product_id 
                                                     left join product_template as pt on pt.id = pp.product_tmpl_id and sl.product_id = crl.product_id
                                                     where sl.type = '%s' and sh.id = %s and pt.categ_id in %s and crl.active = true
                                                     group by sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id ,crl.break_id , crl.can_id
                                                    """%(self.type,shts_id[0],typle(categ_id)))
                        data = self._cr.dictfetchall()
                else:
                    if len(categ_id)==1:
                        self.env.cr.execute(""" select sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id,crl.break_id, crl.can_id  from 
                                                     shts_register_line as sl left join shts_register as sh on sh.id = sl.shts_id
                                                      left join can_register as cr on cr.shts_id = sh.id
                                                     left join can_register_line as crl on crl.can_id = cr.id
                                                     left join product_product as pp on pp.id = sl.product_id 
                                                     left join product_template as pt on pt.id = pp.product_tmpl_id and sl.product_id = crl.product_id
                                                     where sl.type = '%s' and sh.id in %s and pt.categ_id = %s  and crl.active = true
                                                     group by sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id ,crl.break_id, crl.can_id 
                                                    """%(self.type,tuple(shts_id),categ_id[0]))
                        data = self._cr.dictfetchall()
                    else:
                        self.env.cr.execute(""" select sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id,crl.break_id, crl.can_id  from 
                                                     shts_register_line as sl left join shts_register as sh on sh.id = sl.shts_id
                                                     left join can_register as cr on cr.shts_id = sh.id
                                                     left join can_register_line as crl on crl.can_id = cr.id
                                                     left join product_product as pp on pp.id = crl.product_id 
                                                     left join product_template as pt on pt.id = pp.product_tmpl_id and sl.product_id = crl.product_id
                                                     where sl.type = '%s' and sh.id in %s and pt.categ_id in %s  and crl.active = true
                                                     group by sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id ,crl.break_id , crl.can_id
                                                    """%(self.type,tuple(shts_id),typle(categ_id)))
                        data = self._cr.dictfetchall()
                line_id = False
                for lines in data:
                    
                    line_data = line_obj.search([('can_id','=',lines['can_id']),('product_id','=',lines['product_id']),('old_price','=',lines['price']),('price_change_id','=',self.id)])
                    if line_data.id == False:
                        line_id = line_obj.create({'company_id': lines['company_id'],
                                         'shts_id': [(6, 0, [lines['shts_id']])],
                                         'product_id':lines['product_id'],
                                         'old_price':lines['price'],
                                         'new_price':0.0,
                                         'qty':0.0,
                                         'type':lines['type'],
                                         'date':self.start_date,
                                         'price_change_id':self.id,
                                         'can_id':lines['can_id'],
                                         })
                    else:
                        if line_id !=False:
                            line_id.write({'shts_id': [(4, lines['shts_id'])],})
                    
                    for sh in shts_id:
                        first_mile = 0.0
                        shift = shift_obj.search([('state','=','draft'),('shts_id','=',sh),('shift_date','=',str(self.start_date)[:10])])
                        if shift:
                            break_ids = break_obj.browse(lines['break_id'])
                            for li in shift.mile_target_ids:
                                if li.product_id.id == lines['product_id'] and li.name==break_ids.name:
                                    first_mile =li.first_mile

            elif self.type == 'oil':
                product = []
                if self.product_id:
                    for pro in self.product_id:
                        product.append(pro.id)
                elif self.category_id:
                    prod_ids = product_obj.search([('categ_id','=',self.category_id.id)])
                    for pro in prod_ids:
                        product.append(pro.id)
                shts_id = []
                line_id = False
                for line in self.price_group_id.shts_ids:
                    shts_id.append(line.id)
                if len(shts_id)==1:
                    if len(product)==1:
                        shts_line_ids = shts_line_obj.search([('product_id','=',product[0]),('oil_shts_id','=',shts_id[0])])
                    else:
                        shts_line_ids = shts_line_obj.search([('product_id','in',tuple(product)),('oil_shts_id','=',shts_id[0])])
                else:
                    if len(product)==1:
                        shts_line_ids = shts_line_obj.search([('product_id','=',product[0]),('oil_shts_id','in',tuple(shts_id))])
                    else:
                        shts_line_ids = shts_line_obj.search([('product_id','in',tuple(product)),('oil_shts_id','in',tuple(shts_id))])
                
                for lis in shts_line_ids:
                    line_data = line_obj.search([('product_id','=',lis.product_id.id),('old_price','=',lis.price),('type','=',self.type),('oil_change_id','=',self.id)])
                    
                    if not line_data:
                        line_id = line_obj.create({
                                          'shts_id':[(6, 0, [lis.oil_shts_id.id])],
                                          'product_id':lis.product_id.id,
                                          'date':self.start_date,
                                          'old_price':lis.price,
                                          'new_price':0.0,
                                          'type':self.type,
                                          'oil_change_id':self.id,
                                        })
                    else:
                        if line_id !=False:
                            line_id.write({'shts_id': [(4, lis.oil_shts_id.id)],})
                    
                
        elif self.product_id and self.price_group_id:
            shts_id = []
            if self.price_group_id.shts_ids:
                for line in self.price_group_id.shts_ids:
                    if line.id!=False:
                        shts_id.append(line.id)
            else:
                raise UserError(u"Үнийн бүлэг дээр ШТС сонгогдоогүй байгаа тул сонгоно уу.") 
            product = []
            for pro in self.product_id:
                product.append(pro.id)
            if self.type =='gasol':
                if len(shts_id)==1:
                    if len(product)==1:
                        self.env.cr.execute(""" select sl.price, sl.product_id, sl.type, sl.shts_id, crl.break_id from 
                                                can_register_line as crl
                                                left join can_register as cr on crl.can_id = cr.id
                                                left join shts_register_line as sl  on cr.shts_id = sl.shts_id
                                                left join product_product as pp on pp.id = crl.product_id  
                                                where sl.shts_id = %s and sl.product_id = %s and sl.product_id = crl.product_id and crl.active = true
                                                group by sl.price, sl.product_id, sl.type, sl.shts_id ,crl.break_id
                                                    """%(shts_id[0],product[0]))
                        data = self._cr.dictfetchall()
                    else:
                        self.env.cr.execute(""" select sl.price, sl.product_id, sl.type, sl.shts_id, crl.break_id from 
                                                can_register_line as crl
                                                left join can_register as cr on crl.can_id = cr.id
                                                left join shts_register_line as sl  on cr.shts_id = sl.shts_id
                                                left join product_product as pp on pp.id = crl.product_id  
                                                where sl.shts_id = %s and sl.product_id in %s and sl.product_id = crl.product_id and crl.active = true
                                                group by sl.price, sl.product_id, sl.type, sl.shts_id ,crl.break_id
                                                    """%(shts_id[0],tuple(product)))
                        data = self._cr.dictfetchall()
                else:
                    if len(product)==1:
                        self.env.cr.execute(""" select sl.price, sl.product_id, sl.type, sl.shts_id, crl.break_id from 
                                                can_register_line as crl
                                                left join can_register as cr on crl.can_id = cr.id
                                                left join shts_register_line as sl  on cr.shts_id = sl.shts_id
                                                left join product_product as pp on pp.id = crl.product_id  
                                                where sl.shts_id in %s and sl.product_id = %s and sl.product_id = crl.product_id and crl.active = true
                                                group by sl.price, sl.product_id, sl.type, sl.shts_id ,crl.break_id
                                                    """%(tuple(shts_id),product[0]))
                        
                        data = self._cr.dictfetchall()
                    else:
                        self.env.cr.execute("""select sl.price, sl.product_id, sl.type, sl.shts_id, crl.break_id from 
                                                can_register_line as crl
                                                left join can_register as cr on crl.can_id = cr.id
                                                left join shts_register_line as sl  on cr.shts_id = sl.shts_id
                                                left join product_product as pp on pp.id = crl.product_id  
                                                where sl.shts_id in %s and sl.product_id in %s and sl.product_id = crl.product_id and crl.active = true
                                                group by sl.price, sl.product_id, sl.type, sl.shts_id ,crl.break_id
                                                    """%(tuple(shts_id),tuple(product)))
                        data = self._cr.dictfetchall()
                line_id = False
                for lines in data:
                    line_data = line_obj.search([('product_id','=',lines['product_id']),('old_price','=',lines['price']),('price_change_id','=',self.id)])
                    if line_data.id == False:
                        line_id = line_obj.create({
                                         'shts_id': [(6, 0, [lines['shts_id']])],
                                         'product_id':lines['product_id'],
                                         'old_price':lines['price'],
                                         'break_id':lines['break_id'],
                                         'new_price':0.0,
                                         'qty':0.0,
                                         'type':lines['type'],
                                         'date':self.start_date,
                                         'price_change_id':self.id,
                                         })
                    else:
                        if line_id !=False:
                            line_id.write({'shts_id': [(4, lines['shts_id'])],})
                    for sh in shts_id:
                        first_mile = 0.0
                        shift = shift_obj.search([('state','=','draft'),('shts_id','=',sh),('shift_date','=',str(self.start_date)[:10])])
                        if shift:
                            break_ids = break_obj.browse(lines['break_id'])
                            for li in shift.mile_target_ids:
                                if li.product_id.id == lines['product_id'] and li.name==break_ids.name:
                                    first_mile =li.first_mile

            elif self.type == 'oil':
                shts_id = []
                if self.price_group_id.shts_ids:
                    for line in self.price_group_id.shts_ids:
                        shts_id.append(line.id)
                else:
                    raise UserError(u"Үнийн бүлэг дээр ШТС сонгогдоогүй байгаа тул сонгоно уу.")
                if len(shts_id)==1:
                    if len(product)==1:
                        shts_line_ids = shts_line_obj.search([('product_id','=',product[0]),('oil_shts_id','=',shts_id[0])])
                    else:
                        shts_line_ids = shts_line_obj.search([('product_id','in',product),('oil_shts_id','=',shts_id[0])])
                else:
                    if len(product)==1:
                        shts_line_ids = shts_line_obj.search([('product_id','=',product[0]),('oil_shts_id','in',tuple(shts_id))])
                    else:
                        shts_line_ids = shts_line_obj.search([('product_id','in',product),('oil_shts_id','in',tuple(shts_id))])
                line_id = False
                
                for lis in shts_line_ids:
                    line_data = line_obj.search([('product_id','=',lis.product_id.id),('old_price','=',lis.price),('type','=',self.type),('oil_change_id','=',self.id)])
                    if not line_data:
                        line_id = line_obj.create({
                                          'shts_id':[(6, 0, [lis.oil_shts_id.id])],
                                          'product_id':lis.product_id.id,
                                          'date':self.start_date,
                                          'old_price':lis.price,
                                          'new_price':0.0,
                                          'type':self.type,
                                          'oil_change_id':self.id,
                                        })
                    else:
                        if line_id !=False:
                            line_id.write({'shts_id': [(4, lis.oil_shts_id.id)],})
            
        elif self.type and self.price_group_id:
            if self.type =='gasol':
                if self.is_category == True:
                    region_id = []
                    if self.price_group_id.shts_ids:
                        for re in self.price_group_id.shts_ids:
                            region_id.append(re.id)
                        
                    else:
                        raise UserError(u"Үнийн бүлэг дээр ШТС сонгогдоогүй байгаа тул сонгоно уу.")
                    if len(region_id)==1:
                        self.env.cr.execute(""" select sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id from 
                                                 shts_register as sh left join 
                                                 shts_register_line as sl on sh.id = sl.shts_id left join  
                                                 product_product as pp on pp.id = sl.product_id left join
                                                 product_template as pt on pt.id = pp.product_tmpl_id
                                                 where sl.type = '%s' and sh.id = %s 
                                                 and pt.categ_id = %s 
                                                 group by sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id 
                                                """%(self.type,re.id,self.category_id.id))
                        data = self._cr.dictfetchall()
                    else:
                        self.env.cr.execute(""" select sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id from 
                                                 shts_register as sh left join 
                                                 shts_register_line as sl on sh.id = sl.shts_id left join  
                                                 product_product as pp on pp.id = sl.product_id left join
                                                 product_template as pt on pt.id = pp.product_tmpl_id
                                                 where sl.type = '%s' and sh.id in %s
                                                 and pt.categ_id = %s 
                                                 group by sl.price, sl.product_id, sl.type, sl.shts_id, sh.company_id 
                                                """%(self.type,tuple(region_id),self.category_id.id))
                        data = self._cr.dictfetchall()
                    
                    line_id = False
                    for lines in data:
                        
                        line_data = line_obj.search([('product_id','=',lines['product_id']),('old_price','=',lines['price']),('price_change_id','=',self.id)])
                        if line_data.id == False:
                            line_id = line_obj.create({
                                             'shts_id': [(6, 0, lines['shts_id'])],
                                             'product_id':lines['product_id'],
                                             'old_price':lines['price'],
                                             'new_price':0.0,
                                             'qty':0.0,
                                             'type':lines['type'],
                                             'date':self.start_date,
                                             'price_change_id':self.id,
                                             })
                        else:
                            if line_id !=False:
                                line_id.write({'shts_id': [(4, lines['shts_id'])],})
                        for sh in shts_id:
                            first_mile = 0.0
                            shift = shift_obj.search([('state','=','draft'),('shts_id','=',sh),('shift_date','=',str(self.start_date)[:10])])
                            if shift:
                                break_ids = break_obj.browse(lines['break_id'])
                                for li in shift.mile_target_ids:
                                    if li.product_id.id == lines['product_id'] and li.name==break_ids.name:
                                        first_mile =li.first_mile

            elif self.type =='oil':
                shts_id = []
                line_id = False
                if self.price_group_id.shts_ids:
                    for line in self.price_group_id.shts_ids:
                        shts_id.append(line.id)
                else:
                    raise UserError(u"Үнийн бүлэг дээр ШТС сонгогдоогүй байгаа тул сонгоно уу.")
                
                product_ids = product_obj.search([('prod_type','=','oil')])
                prod_ids = []
                for pro in product_ids:
                    prod_ids.append(pro.id)
                if len(shts_id)==1:
                    if len(prod_ids)==1:
                        shts_line_ids = shts_line_obj.search([('product_id','=',prod_ids[0]),('oil_shts_id','=',shts_id[0])])
                    else:
                        shts_line_ids = shts_line_obj.search([('product_id','in',tuple(prod_ids)),('oil_shts_id','=',shts_id[0])])
                else:
                    if len(prod_ids)==1:
                        
                        shts_line_ids = shts_line_obj.search([('product_id','=',prod_ids[0]),('oil_shts_id','in',tuple(shts_id))])
                    else:
                        shts_line_ids = shts_line_obj.search([('product_id','in',tuple(prod_ids)),('oil_shts_id','in',tuple(shts_id))])
                
                for lis in shts_line_ids:
                    line_data = line_obj.search([('product_id','=',lis.product_id.id),('old_price','=',lis.price),('type','=',self.type),('oil_change_id','=',self.id)])
                    if not line_data:
                        line_id = line_obj.create({
                                         
                                          'shts_id':[(6, 0, [lis.oil_shts_id.id])],
                                          'product_id':lis.product_id.id,
                                          'date':self.start_date,
                                          'old_price':lis.price,
                                          'new_price':0.0,
                                          'type':self.type,
                                          'oil_change_id':self.id,
                                        })
                    else:
                        if line_id !=False:
                            line_id.write({'shts_id': [(4, lis.oil_shts_id.id)],})
        
        return True
    
class PriceGasolRegister(models.Model):
    _name = 'price.gasol.register'
    
    can_id = fields.Many2one('can.register',string=u'Сав')
    company_id = fields.Many2one('res.company',string='Company')
    product_id = fields.Many2one('product.product',string='Product')
    price_change_id = fields.Many2one('price.change.register',string='Price change')
    shts_id = fields.Many2one('shts.register',string='Shts')
    shift = fields.Float(string=u'Ээлж')
    mile_target_amount = fields.Float(string=u'Үнэ сольсон милл')
    first_mile = fields.Float(string=u'Эхний милл')
    diff_mile = fields.Float(string=u'Зөрүү',compute="action_diff_mile")
    break_id = fields.Many2one('mile.target.register',string=u'Хошуу')
    date = fields.Datetime(string=u'Өөрчлөлт эхлэх огноо', default=fields.Date.context_today)
    state = fields.Selection([('draft','Draft'),
                              ('send','Send'),
                              ('confirm','Confirm'),
                              ('done','Done')],string='State',default='draft',compute="action_state")
    
    def action_diff_mile(self):
        for line in self:
            line.diff_mile = line.mile_target_amount - line.first_mile
    
    def action_state(self):
        for line in self:
            line.state = line.price_change_id.state
    
class PriceChangeRegisterLine(models.Model):
    _name = 'price.change.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('price.register'))
    shts_id = fields.Many2many('shts.register','price_change_line_shts','price_line_id','shts_id','Shts')
    price_change_id = fields.Many2one('price.change.register',string='Price change')
    oil_change_id = fields.Many2one('price.change.register',string='Price change')
    product_id = fields.Many2one('product.product','Product',required=True,domain="[('prod_type','=',type)]")
    old_price = fields.Float(string='Old Price')
    new_price = fields.Float(string='New Price',required=True)
    break_id = fields.Many2one('mile.target.register',string=u'Хошуу')
    qty = fields.Float(string='Quantity')
    shift = fields.Float(string=u'Ээлж')
    can_id = fields.Many2one('can.register',string=u'Сав')
    date = fields.Datetime(string=u'Өөрчлөлт эхлэх огноо', default=fields.Date.context_today)
    state = fields.Selection([('draft','Draft'),
                              ('send','Send'),
                              ('confirm','Confirm'),
                              ('done','Done')],string='State',default='draft',compute="action_state")
    mile_point_ids = fields.Many2many('mile.point','price_line_mile_point_rel','price_id','mile_id',string='Mile point')
    mile_target_amount = fields.Float(string=u'Үнэ сольсон милл')
    type = fields.Selection([('gasol',u'Шатахуун'),
                             ('oil',u'Тос'),
                             ('akk',u'Аккумулятор'),
                             ('service',u'Үйлчилгээ')],string=u'Төрөл',required=True)
    
    @api.onchange('product_id')
    def onchange_type(self):
        for line in self:
            line.type = line.product_id.prod_type
            if line.shts_id.line_ids:
                if li in line.shts_id.line_ids:
                    if li.product_id.id==line.product_id.id:
                        line.old_price = li.price
            
    
    
    def action_state(self):
        for line in self:
            if line.price_change_id:
                line.state = line.price_change_id.state
            elif line.oil_change_id:
                line.state = line.oil_change_id.state

class MilePoint(models.Model):
    _name = 'mile.point'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('mile.point'))
    name = fields.Many2one('mile.target.register',string='Break')
    mile_point = fields.Float(string='Mile point')
        
        
        