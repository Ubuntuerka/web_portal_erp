# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree
from dateutil.relativedelta import relativedelta
DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


class SpecialWorkPermitRegister(models.Model):
    _name = 'special.work.permit.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'name desc'

    name = fields.Char(string='Хүсэлтийн дугаар', readonly=True,
                       default=lambda self: self.env['ir.sequence'].next_by_code('special.work.permit.register'))
    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    shts_id = fields.Many2one('shts.register', string='Агуулах/ШТС', required=True,
                              default=lambda self: self.env.user.shts_ids)
    job_title = fields.Char(string='Хийгдэх ажилбарын нэр')
    job_title_name = fields.Many2one('job.title.register',string='Хийгдэх ажилбарын нэр')
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)
    explanation = fields.Char(string=u'Тайлбар')
    exp = fields.Text(string=u'Тайлбар')
    create_user_id = fields.Many2one('res.users', string=u'Зөвшөөрөл олгосон ажилтан', readonly=True,
                                     default=lambda self: self.env.user)
    contractor = fields.Selection([('basic_employee', u'Үндсэн ажилтан'),('contractor_employee', u'Гүйцэтгэгч ажилтан')], u'Гүйцэтгэгч төрөл')
    employee_id = fields.Many2one('hr.employee', string='Ажилтан')
    emp_id = fields.Many2one('res.users', string='Ажилтан')
    con_emp_id = fields.Char('Ажилтан')
    engineer_charge = fields.Many2one('hr.employee', string='Хариуцсан инженер', required=True)
    engineer_id = fields.Many2one('res.users', string='Хариуцсан инженер', required=True)


class JobTitleRegister(models.Model):
    _name = 'job.title.register'

    name = fields.Char(string='Хийгдэх ажилбарын нэр')
