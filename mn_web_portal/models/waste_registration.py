# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class WasteRegister(models.Model):
    _name = 'waste.register'

    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    shts_id = fields.Many2one('shts.register', string='Агуулах/ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id)
    date = fields.Date(string=u'Огноо', required=True)
    gtb_waste_kg = fields.Float(string=u'ГТБ-ээр бохирдсон хог хаягдал кг')
    ahiu_waste_kg = fields.Float(string=u'Ахуйн хог хаягдал кг')
    dahin_waste_kg = fields.Float(string=u'Дахин ашиглагдах хог хаягдал кг')
    aj_baiguulga = fields.Char(string=u'Ачилт хийсэн байгууллага', required=True)
