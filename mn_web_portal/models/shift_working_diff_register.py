# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
DATE_FORMAT = "%Y-%m-%d"

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

class ShiftWorkingDiffRegister(models.Model):
    _name = 'shift.working.diff.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company')
    shift_id = fields.Many2one('shift.working.register',string='Shift working')
    diff = fields.Float(string='Diff')
    employee_id = fields.Many2one('hr.employee',string='Employee')
    product_id = fields.Many2one('product.product',string='Product')
    shift_date = fields.Date(string='Shift date')
    shts_id = fields.Many2one('shts.register',string='Shts')