# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import Warning, UserError

from odoo.exceptions import RedirectWarning, UserError, ValidationError
from odoo.osv.expression import get_unaccent_wrapper
from datetime import timedelta
from datetime import datetime
import re
from odoo.osv import expression

USER_PRIVATE_FIELDS = []


class TechnicalCheckInfo(models.Model):
    _name = 'technical.check.info'

    company_id = fields.Many2one('res.company', string='Company'
                                 , default=lambda self: self.env['res.company']._company_default_get('talon.type'))  #
    shts_id = fields.Many2one('res.shts.id', string='SHTS')  #
    department_id = fields.Many2one('hr.department', string=u'Газар/Нэгж')
    user_id = fields.Many2one('res.users', string=u'Ажилтан')
    reg_user_id = fields.Many2one('res.users', string='Register user', readonly=True,
                                  default=lambda self: self.env.user,
                                  required=True)
    device = fields.Char(string='Device')  #
    nomer = fields.Float(string='Nomer')  #
    model = fields.Char(string='Model', required=True)  #
    commission_date = fields.Date(string='Commission date', size=8)  #
    seiral = fields.Char(string='Seiral')  #
    template = fields.Char(string='Template')  #
    seiral_2 = fields.Char(string='Seiral 2')  #
    equipping = fields.Char(string='Equipping')  #
    receipt_date = fields.Date(string='Хүлээн авсан огноо')  #
    device_name = fields.Many2one('device.register', string='Төхөөрөмжийн нэр')  #
    side = fields.Selection([
        ('a', 'A'),  #
        ('b', 'B'),  #
        ('ab', 'AB')], string='Side', default='a')  #
    verify_description = fields.Char(string='Verify description')  #
    aspect = fields.Selection([
        ('good', 'Good'),  #
        ('mean', 'Mean'),  #
        ('bad', 'Bad')], string='Aspect', default='good')  #
    evaluation = fields.Selection([
        ('new', 'Шинэ'),  #
        ('old', 'Хуучин')], string='Evaluation', default='new')  #
    description = fields.Char(string='Description')  #

    @api.onchange('department_id')
    def onchange_department(self):
        if self.department_id:
            self.user_id = False
            return {
                'domain': {'user_id': [('dep_id', '=', self.department_id.id)]}
            }
        else:
            return {'domain': {'user_id': []}}


class DeviceRegister(models.Model):
    _name = 'device.register'

    name = fields.Char('Төхөөрөмжийн нэр')