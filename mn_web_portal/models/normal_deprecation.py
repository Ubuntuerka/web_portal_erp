# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class NormalDeprecation(models.Model):
    _name = 'normal.deprecation'
    
    company_id = fields.Many2one('res.company',string='Company')
    product_id = fields.Many2one('product.product','Product')
    type = fields.Selection([('received','Received'),
                             ('give','Give'),
                             ('save','Save')],type='Type')
    amount = fields.Float(string='Amount')
    