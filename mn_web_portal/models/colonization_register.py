# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class ColonizationRegister(models.Model):
    _name = 'colonization.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    shts_id = fields.Many2one('shts.register', string='ШТС',
                              default=lambda self: self.env.user.warehouse_id)
    name = fields.Char(string='Дугаарлалт')
    employee_id = fields.Many2one('hr.employee', string=u'Ажилтан')
    reg_user_id = fields.Many2one('res.users', string=u'Бүртгэсэн ажилтан', default=lambda self: self.env.user,
                                  required=True)
    reg_date = fields.Datetime(string=u'Нээсэн огноо', required=True, default=lambda self: fields.datetime.now(),
                               readonly=True)
    type = fields.Selection([('emp',u'Ажилтан'),
                             ('shts',u'ШТС')],string=u'Шүүх төрөл',required=True)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)
    line_ids = fields.One2many('colonization.register.line', 'colonization_id', string='line')
    state = fields.Selection([('draft',u'Ноорог'),
                              ('check',u'Хянасан')],string=u'Төлөв',default='draft',tracking=True)
    
    def action_compute(self):
        line_obj = self.env['colonization.register.line']
        terminal_error_obj = self.env['shift.terminal.error']
        shift_obj = self.env['shift.working.register']
        act_obj = self.env['act.of.calculation.register']
        shift_diff_obj = self.env['shift.working.diff.register']
        employee_ids = []
        diff = 0.0
        diff_data = []
        last_data = []
        if self.type == 'emp':
            employee_ids.append(self.employee_id.id)
            shts_id = []
            shift_id = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date)])
            if shift_id:
                for list in shift_id:
                    for lines in list.shift_line_ids:
                        if lines.employee_id.id == self.employee_id.id:
                            shts_id.append(list.shts_id.id)
            if len(shts_id)==1:
                terminal_error_id = terminal_error_obj.search([('state','=','confirm'),('shts_id','=',shts_id[0]),('start_date','>=',self.start_date),('end_date','<=',self.end_date)])
                act_ids = act_obj.search([('state','=','check'),('shts_id','=',shts_id[0]),('start_date','>=',self.start_date),('end_date','<=',self.end_date)])
            elif len(shts_id)>1:
                terminal_error_id = terminal_error_obj.search([('state','=','confirm'),('shts_id','in',tuple(shts_id)),('start_date','>=',self.start_date),('end_date','<=',self.end_date)])
                act_ids = act_obj.search([('state','=','check'),('shts_id','in',tuple(shts_id)),('start_date','>=',self.start_date),('end_date','<=',self.end_date)])
            
            if act_ids:
                for lins in act_ids:
                    for list in lins.line_ids:
                        if list.dutuu_kg <0.0:
                            price = 0.0
                            for shline in lins.shts_id.line_ids:
                                if list.product_id.id == shline.product_id.id:
                                    price = shline.price
                            dhj = list.exp_kg/list.exp_litr
                            dt = {
                                'product_id':list.product_id.id,
                                'can_id':list.can_id.id,
                                'diff':list.dutuu_kg,
                                'shts_id':list.calculation_id.shts_id.id,
                                'dhj':dhj,
                                'price':price,
                                'loss_amount':list.dutuu_kg
                                }
                            diff_data.append(dt)
            if terminal_error_id:
                for datas in diff_data:
                    diff_computes = 0.0
                    diff_compute = 0.0
                    for line in terminal_error_id:
                        for erl in line.line_ids:
                            if erl.product_id.id == datas['product_id'] and datas['shts_id']==erl.shift_terminal_error_id.shts_id.id:
                                diff_compute += erl.per
                    diff_computes = datas['diff']+diff_compute
                    send_data = {
                                'diff':diff_computes,
                                'shts_id':datas['shts_id'],
                                'can_id':datas['can_id'],
                                'product_id':datas['product_id'],
                                'dhj':datas['dhj'],
                                'price':datas['price'],
                                'loss_amount':datas['loss_amount']
                                }
                    last_data.append(send_data)

        else:
            shift_id = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',self.shts_id.id)])
            if shift_id:
                for list in shift_id:
                    for lines in list.shift_line_ids:
                        if lines.employee_id.id:
                            if lines.employee_id.id not in employee_ids:
                                employee_ids.append(lines.employee_id.id)
            terminal_error_id = terminal_error_obj.search([('state','=','confirm'),('shts_id','=',self.shts_id.id),('start_date','>=',self.start_date),('end_date','<=',self.end_date)])
            act_ids = act_obj.search([('state','=','check'),('shts_id','=',self.shts_id.id),('start_date','>=',self.start_date),('end_date','<=',self.end_date)])
            if act_ids:
                for lins in act_ids:
                    
                    for list in lins.line_ids:
                        total_amount = 0.0
                        for emp in employee_ids:
                            
                            if list.dutuu_kg <0.0:
                                emp_diff = 0.0
                                shift_diff_ids = shift_diff_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',list.calculation_id.shts_id.id),('employee_id','=',emp),('product_id','=',list.product_id.id)])
                                for sdi in shift_diff_ids:
                                    #if sdi.diff < 0.0:
                                    emp_diff +=sdi.diff
                                    #if emp==4285 and list.product_id.id==4699:
                                       # print("aaaaaaaaaaaaaaaaaaaaaa",total_amount,emp)
                                if emp_diff < 0.0:
                                    total_amount +=emp_diff
                                    
                        if list.dutuu_kg <0.0 and list.product_id.id not in diff_data:
                            price = 0.0
                            dhj = list.exp_kg/list.exp_litr
                            for shline in lins.shts_id.line_ids:
                                if list.product_id.id == shline.product_id.id:
                                    price = shline.price
                            dt = {
                                'product_id':list.product_id.id,
                                'can_id':list.can_id.id,
                                'diff':list.dutuu_kg,
                                'shts_id':list.calculation_id.shts_id.id,
                                'dhj':dhj,
                                'price':price,
                                'loss_amount':list.dutuu_kg,
                                'total_amount':total_amount,
                                }
                            diff_data.append(dt)
            if terminal_error_id:
                for datas in diff_data:
                    diff_computes = 0.0
                    diff_compute = 0.0

                    for line in terminal_error_id:
                        for erl in line.line_ids:
                            if erl.product_id.id == datas['product_id'] and datas['shts_id']==erl.shift_terminal_error_id.shts_id.id:
                                diff_compute += erl.per
                                
                    diff_computes = datas['diff']+diff_compute
                    if datas['diff'] <0.0:
                        
                        send_data = {
                                    'diff':diff_computes,
                                    'shts_id':datas['shts_id'],
                                    'can_id':datas['can_id'],
                                    'product_id':datas['product_id'],
                                    'price':datas['price'],
                                    'dhj':datas['dhj'],
                                    'loss_amount':datas['loss_amount'],
                                    'total_amount':datas['total_amount'],
                                    }
                        last_data.append(send_data)
                    
                    
        for emp in employee_ids:
            if last_data:
                
                for ldata in last_data:
                    if ldata['diff'] <0.0:
                        emp_diff = 0.0
                        shift_diff_ids = shift_diff_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',ldata['shts_id']),('employee_id','=',emp),('product_id','=',ldata['product_id'])])
                        for sdi in shift_diff_ids:
                            emp_diff +=sdi.diff
                        compute = (emp_diff*100.0)/ldata['total_amount']
                        
                        amount = ((ldata['diff']/ldata['dhj'])/100.0*compute)*ldata['price']*(-1)
                        data = {
                                'employee_id':emp,
                                'colonization_id':self.id,
                                'diff':emp_diff,
                                'shts_id':ldata['shts_id'],
                                'can_id':ldata['can_id'],
                                'product_id':ldata['product_id'],
                                'dhj':ldata['dhj'],
                                'price':ldata['price'],
                                'percent':compute,
                                'amount':amount
                                }
                        if emp_diff < 0.0:
                            line_id = line_obj.search([('employee_id','=',emp),('product_id','=',ldata['product_id']),('can_id','=',ldata['can_id']),('colonization_id','=',self.id)])
                        
                            if not line_id:
                                line_obj.create(data)
                            else:
                                    line_id.write(data)

class ColonizationRegisterLine(models.Model):
    _name = 'colonization.register.line'

    colonization_id = fields.Many2one('colonization.register', string='colonization')
    employee_id = fields.Many2one('hr.employee', 'Ажилтан', required=True)
    product_id = fields.Many2one('product.product', string=u'Бүтээгдэхүүн')
    can_id = fields.Many2one('can.register', string=u'Сав')
    percent = fields.Float(string='Хувь')
    shts_id = fields.Many2one('shts.register', string='ШТС')
    diff = fields.Float(string=u'Кг зөрүү')
    last_diff = fields.Float(string=u'Бодит зөрүү')
    dhj = fields.Float(string=u'ДХЖ',digits=(10,4))
    price = fields.Float(string=u'Үнэ')
    amount = fields.Float(string=u'Мөнгөн дүн')
