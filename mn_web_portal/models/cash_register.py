# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class CashRegister(models.Model):
    _name = 'cash.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env['res.company']._company_default_get('cash.register'),required=True)
    name = fields.Char(string='Cash sequence')
    