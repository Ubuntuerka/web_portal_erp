# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class TerminalRegister(models.Model):
    _name = 'terminal.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('terminal.register'))
    shts_register = fields.Many2one('shts.register', string=u'ШТС', required=True,
                                    default=lambda self: self.env.user.warehouse_id)
    terminal_name = fields.Char(string=u'Терминал нэр', required=True)
    terminal_id = fields.Char(string=u'Терминал ID', required=True)
    last_send_date = fields.Datetime(string=u'Сүүлийн мэдээ илгээсэн огноо Пос')
    owner = fields.Many2one('res.partner', string='Эзэмшигч')
    active = fields.Boolean(string=u'Идэвхтэй эсэх')
    baraani_medeelel_tatsan_eseh = fields.Boolean(string=u'Барааны мэдээлэл татсан эсэх', required=True)
    terminal_address = fields.Char(string=u'Терминал хаяг', required=True)
    district = fields.Many2one('district.register', string=u'Сум дүүрэг', required=True)
    company_name = fields.Many2one('res.company', string='Компани')
    register_number = fields.Char(string=u'РД', required=True)


    _sql_constraints = [
        ('terminal_id_uniq', 'unique (terminal_id)', u'Терминал ID давхцаж байгаа тул өөр ID өгнө үү!!!!!'),
    ]