# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class shts_last_price(models.Model):
    _name = "shts.last.price"
    _auto = False

    company_id = fields.Many2one('res.company', string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('shts.register'),
                                 required=True)
    shts_id = fields.Char(string='Shts name', required=True)
    code = fields.Char(string='Shts code', required=True)
    product_id = fields.Many2one('product.product', string='Product', required=True)
    region_id = fields.Many2one('shts.region', string='Region')
    price = fields.Float(string='Price', required=True)
    type = fields.Selection([('gasol', u'Шатахуун'),
                             ('oil', u'Тос'),
                             ('service', u'Үйлчилгээ')], string='Type', required=True)


    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            create or replace view shts_last_price as (
            select s.id, s.company_id, s.name as shts_id, s.code, s.region_id, l.product_id, l.type, l.price  
            from shts_register as s
            left join shts_register_line as l on (l.id=s.id) where s.is_stock = False and s.active = True
            )""")
