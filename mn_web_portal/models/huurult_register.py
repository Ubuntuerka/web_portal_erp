# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date



class zalruulga(models.Model):
    _name = 'zalruulga'
    
    zal_a = fields.Float('Эхлэх')
    #2
    zal_b = fields.Float('Дуусах' , digits=(10, 4))
    #4
    koef = fields.Float('Коэффициент',digits=(10, 6))
    #6 oron
class thh_norm(models.Model):
    _name = 'thhnorm'
    
    types = fields.Selection([('1', 'Авто бинзен'),('2', 'Өвөл ДТ'),('3', 'Зун ДТ')], 'Төрөл')
    km_a = fields.Float('Эхлэх')
    km_b = fields.Float('Дуусах')
    norm = fields.Float('Норм',digits=(10, 3))

class HuurultRegister(models.Model):
    _name = 'huurult.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env['res.company']._company_default_get('huurult.register'))
    car_number_id = fields.Many2one('car.register',string='Car number')
    torkh_id = fields.Many2one('bucket.register',string=u'Торх')
    mm = fields.Float(string='Хөөрөлт мм')
    litr = fields.Float(string='Хөөрөлт литр')
    suult_mm = fields.Float(string=u'Суулт мм')
    suult_litr = fields.Float(string=u'Суулт литр')
    gasol_type = fields.Selection([('winter',u'Өвөл'),
                                   ('summer',u'Зун')
                                   ],string=u'Төрөл')
    norm_type = fields.Selection([('teeverlelt',u'Тээвэрлэлт'),
                                  ('save',u'Хадгалалт'),
                                  ('tugeelt',u'Түгээлт'),
                                  ('huleen_avalt',u'Хүлээн авалт')],string=u'Хэвийн хорогдлын норм')
    type = fields.Selection([('luuk_huurult',u'Лүүкэндээ хөөр суудаг'),
                             ('torh_huurult',u'Торхондоо хөөр суудаг')],string=u'Хөөрөлт төрөл')
    
    
    
    