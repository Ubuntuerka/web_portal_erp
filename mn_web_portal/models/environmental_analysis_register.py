# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class EnvironmentalAnalysisRegister(models.Model):
    _name = 'environmental.analysis.register'

    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    shts_id = fields.Many2one('shts.register', string='Агуулах/ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id)
    reg_date = fields.Datetime(string=u'Бүртгэсэн огноо', required=True, default=lambda self: fields.datetime.now(),
                               readonly=True)
    create_user_id = fields.Many2one('res.users', string=u'Үүсгэсэн ажилтан', readonly=True,
                                     default=lambda self: self.env.user)
    line_ids = fields.One2many('environmental.analysis.register.line', 'analysis_id', string='water line')


class EnvironmentalAnalysisRegisterLine(models.Model):
    _name = 'environmental.analysis.register.line'

    analysis_type = fields.Selection([('air', u'Агаарын шинжилгээ'),
                                      ('soil', u'Хөрсний шинжилгээ'),
                                      ('water', u'Усны шинжилгээ')], string='Шинжилгээний төрөл', required=True)

    analysis_id = fields.Many2one('environmental.analysis.register', string='analysis')
    examination_date = fields.Date(string=u'Шинжилгээний огноо', required=True)
    sampling_point = fields.Char(string=u'Дээж авсан цэг ', required=True)
    place_analysis = fields.Char(string=u'Шинжилгээ хийсэн газар', required=True)
    analyzed_indicator = fields.Char(string=u'Шинжлүүлсэн үзүүлэлт', required=True)
    analysis_amount = fields.Char(string=u'Шинжилгээний дүн', required=True)
    # found_analyzed = fields.Char(string=u'Шинжилгээнд илэрсэн үл тохирол', required=True)
    need_corrected = fields.Selection([('yes', u'Тийм'),
                                      ('no', u'Үгүй')], string=u'Залруулах шаардлагатай', required=True)
    improvement_measures = fields.Char(string=u'Сайжруулах арга хэмжээ')
