# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class SupplyActRegister(models.Model):
    _name = 'supply.act.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    shts_id = fields.Many2one('shts.register', string='Агуулах/ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id)
    meaning = fields.Char(string=u'Утга')
    date = fields.Date(string=u'Огноо', required=True)
    amount = fields.Float(string=u'Мөнгөн дүн')
