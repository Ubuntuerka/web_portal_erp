# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
import time
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from dateutil import relativedelta

class ShiftTerminalError(models.Model):
    _name = 'shift.terminal.error'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',required=True,states={'draft': [('readonly', False)]},readonly=True,default=lambda self: self.env['res.company']._company_default_get('shift.terminal.error'))
    name = fields.Char(string='Тохиргооны нэр',required=True,states={'draft': [('readonly', False)]},readonly=True)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True, default=fields.Date.context_today,states={'draft': [('readonly', False)]},readonly=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True, default=fields.Date.context_today,states={'draft': [('readonly', False)]},readonly=True)
    shts_id = fields.Many2one('shts.register',string='Shts',required=True,states={'draft': [('readonly', False)]},readonly=True,default=lambda self: self.env.user.warehouse_id)
    reg_date = fields.Date(string='Register date',required=True,states={'draft': [('readonly', False)]},readonly=True,default=fields.Date.context_today)
    reg_user_id = fields.Many2one('res.users',string='Register user',required=True,states={'draft': [('readonly', False)]},readonly=True,default=lambda self: self.env.user)
    state = fields.Selection([('draft',u'Draft'),
                              ('confirm',u'Confirm')],string='State',required=True,default='draft')
    line_ids = fields.One2many('shift.terminal.error.line','shift_terminal_error_id',string='Line error')
    
    @api.model
    def create(self, vals):
        if 'company_id' in vals and 'start_date' in vals and 'shts_id' in vals:
            new_datas = self.search([('company_id','=',vals['company_id']),('start_date','=',vals['start_date']),('shts_id','=',vals['shts_id'])])
            if len(new_datas.ids)>1:
                raise UserError(u'Түгээгүүрийн алдаа давхцаж бүртгэж болохгүй тул та бүртгэлээ шалгана уу !')
        if 'shts_id' in vals:
            if self.env.user.shts_ids:
                if vals['shts_id'] not in self.env.user.shts_ids.ids:
                        raise UserError(u'Та өөр штс сонгож үүсгэж байгаа тул зөв штс сонгоно уу!')
        terminal_error = super(ShiftTerminalError, self.with_context()).create(vals)
        return terminal_error
    
    
    def write(self, vals):
        normal_loss = super(ShiftTerminalError,self).write(vals)
        if 'shts_id' in vals:
            if self.env.user.shts_ids:
                if vals['shts_id'] not in self.env.user.shts_ids.ids:
                        raise UserError(u'Та өөр штс сонгож үүсгэж байгаа тул зөв штс сонгоно уу!')
        
        return normal_loss
    
    
    def action_confirm(self):
        for line in self:
            line.write({'state':'confirm'})
            
            
    def action_compute(self):
        for line in self:
            for lines in line.line_ids:
                per = 0.0
                if lines.product_id.gasol_type == '1':
                    error = ((20-lines.temp)*0.03)+lines.shahalt
                    per = (lines.diff*lines.error)/100.0

                else:
                    error = ((20-lines.temp)*0.01)+lines.shahalt
                    per = (lines.diff*lines.error)/100.0
                if per <0.0:
                    per = 0.0
                lines.per = per
                lines.error = error
    
    def action_download(self):
        line_obj = self.env['shift.terminal.error.line']
        can_obj = self.env['can.register']
        shift_working_obj = self.env['shift.working.register']
        for line in self:
            for shts in line.shts_id.line_ids:
                can_ids = can_obj.search([('shts_id','=',line.shts_id.id)])
                for can in can_ids.line_ids:
                    if shts.product_id.id==can.product_id.id and can.break_id.id:
                        diff = 0.0
                        shift_ids = shift_working_obj.search([('shts_id','=',line.shts_id.id),('shift_date','>=',line.start_date),('shift_date','<=',line.end_date)])
                        for lines in shift_ids.mile_target_ids:
                            if lines.name == can.break_id.name:
                                diff +=lines.diff_mile
                            
                        line_ids = line_obj.search([('product_id','=',shts.product_id.id),('can_id','=',can.can_id.id),('hoshuu_id','=',can.break_id.id),('shift_terminal_error_id','=',line.id)])
                        if not line_ids:
                            line_obj.create({
                                            'product_id':shts.product_id.id,
                                            'can_id':can.can_id.id,
                                            'diff':diff,
                                            'hoshuu_id':can.break_id.id,
                                            'shift_terminal_error_id':line.id,
                                            'company_id':line.company_id.id,
                                            })
                        else:
                            for list in line_ids:
                                list.write({
                                            'product_id':shts.product_id.id,
                                            'can_id':can.can_id.id,
                                            'diff':diff,
                                            'hoshuu_id':can.break_id.id,
                                            'company_id':line.company_id.id,
                                            })
    
    
class ShiftTerminalErrorLine(models.Model):
    _name = 'shift.terminal.error.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company')
    product_id = fields.Many2one('product.product',string='Product')
    can_id = fields.Many2one('can.register',string='Can')
    temp = fields.Float(string='Temperature')
    hoshuu_id = fields.Many2one('mile.target.register',string='Hoshuu')
    diff = fields.Float(string=u'Миллийн зөрүү')
    shahalt = fields.Float(string=u'Шахалт')
    hugatsaa = fields.Float(string=u'Хугацаа')
    per = fields.Float(string=u'Гүйцэтгэл')
    error = fields.Float(string='Алдаа')
    shift_terminal_error_id = fields.Many2one('shift.terminal.error',string='Shift terminal error',ondelete="cascade")
    
    