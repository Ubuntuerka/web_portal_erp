 # -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree
import socket
from . import tls_socket

import mysql.connector
#from myconnection import connect_to_mysql

DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

class ShiftWorkingRegister(models.Model):
    _name = 'shift.working.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'shift_date desc, name desc'
    
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('shift.working.register'))
    name = fields.Char(string='Code',copy=False)
    is_month_qty = fields.Boolean(string=u'Сарын эцсийн тооллого',compute="def_action_last_month_day")
    not_working = fields.Boolean(string=u'Үйл ажиллагаа явуулаагүй')
    end_date = fields.Datetime(string=u'Хаасан огноо')
    eelj_huleeltssen_date = fields.Datetime(string=u'Ээлж хүлээлцсэн огноо')
    shift_date = fields.Date(string=u'Ээлжийн огноо',required=True, default=fields.Date.context_today)
    shts_reg_user_id = fields.Many2one('hr.employee',string=u'Бүртгэсэн ажилтан',required=True)
    reg_user_id = fields.Many2one('res.users',string=u'Бүртгэсэн ажилтан',default=lambda self: self.env.user,required=True)
    reg_date = fields.Datetime(string=u'Нээсэн огноо',required=True, default=lambda self: fields.datetime.now(),readonly=True)
    shts_id = fields.Many2one('shts.register',u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    region_id = fields.Many2one('shts.region',u'ШТС Бүс',required=True)
    shift_line_ids = fields.One2many('shift.working.register.line','shift_id',string='Shift line')
    shift_manager_line_ids = fields.One2many('shift.working.register.line','shift_manager_id',string='Shift line')
    mile_target_ids = fields.One2many('mile.target.register.line','shift_id',string='Mile target')
    talon_sale_ids = fields.One2many('talon.sale','shift_id',string='Talon sale')
    expense_bonus_ids = fields.One2many('expense.bonus.register.line','shift_id',string='Expense bonus')
    loan_document_ids = fields.One2many('loan.document.register.line','shift_id',string='Loan document')
    fuel_sale_ids = fields.One2many('fuel.sale.register.line','shift_id',string='Fuel sale')
    ttm_sale_ids = fields.One2many('ttm.sale.register.line','shift_id',string='Ttm sale')
    tvt_sale_ids = fields.One2many('ttm.sale.register.line.service','shift_id',string='Tvt sale')
    ttm_income_ids = fields.One2many('ttm.income.register.line','shift_id',string='Ttm income')
    scale_ids = fields.One2many('scaling.register','shift_id',string='Scale register')
    is_oil_center = fields.Boolean(string='Oil center')
    income_id = fields.One2many('shts.income.line','shift_id',string='SHTS income')
    state = fields.Selection([('draft',u'Ноорог'),
                              ('count',u'Ээлж хаагдсан'),
                              ('confirm',u'Батлагдсан'),
                              ('cancel',u'Буцаагдсан'),
                              ],string='State',default='draft',tracking=True)
    center_payment_income_ids = fields.One2many('center.payment.income','shift_id',string='Center register')
    payment_shts_id = fields.One2many('shts.payment.register','shift_id',string='Payment shts')
    service_ids = fields.One2many('shift.working.register.line','shift_service_id',string='Service')
    tvt_ttm_total_amount = fields.Float(string=u'Нийт дүн',compute='action_tvt_total_amount',readonly=True)
    tvt_ttm_total_amount_new = fields.Float(string=u'Нийт дүн',related='tvt_ttm_total_amount',store=True)
    tvt_total_amount = fields.Float(string=u'Тос солих төвийн зарлагын нийт дүн',compute='action_tvt_total_amount',readonly=True)
    tvt_total_amount_new = fields.Float(string=u'Тос солих төвийн зарлагын нийт дүн',related='tvt_total_amount',store=True)
    ttm_total_amount = fields.Float(string=u'ТТМ, Аккумуляторын зарлагын нийт дүн',compute='action_tvt_total_amount',readonly=True)
    ttm_total_amount_new = fields.Float(string=u'ТТМ, Аккумуляторын зарлагын нийт дүн',related='ttm_total_amount',store=True)
    total_sale_amount = fields.Float(string=u'Нийт борлуулалтын орлого',compute='action_total_amount')
    total_sale_amount_new = fields.Float(string=u'Нийт борлуулалтын орлого',related='total_sale_amount',store=True)
    total_pay_amount = fields.Float(string=u'Нийт орлогын дүн',compute='action_total_amount',readonly=True)
    total_pay_amount_new = fields.Float(string=u'Нийт орлогын дүн',related='total_pay_amount',store=True)
    total_diff_amount = fields.Float(string='Зөрүү дүн',compute='action_total_amount',readonly=True)
    total_diff_amount_new = fields.Float(string='Зөрүү дүн',related='total_diff_amount',store=True)
    total_shts_amount = fields.Float(string=u'Нийт зарлагын шатахуун',compute='action_total_shts_amount',readonly=True)
    total_shts_amount_new = fields.Float(string=u'Нийт зарлагын шатахуун',related='total_shts_amount',store=True)
    shift_confirm_date = fields.Datetime(string='Shift confirm date',readonly=True)
    is_calculate = fields.Boolean(string=u'Тооцоолсон эсэх')
    is_shift_open = fields.Boolean(string=u'Ээлж нээсэн эсэх',default=True)
    is_download_atg = fields.Boolean(string=u'ATG мэдээлэл татсан эсэх')
    confirm_user_id = fields.Many2one('res.users',string=u'Батласан ажилтан',readonly=True)
    shift_product_ids = fields.One2many('shift.product.line','shift_id',string='Payment line')
    infor_data_lines = fields.One2many('cash.sales.info','shift_id',string='Cash')
    vat_amount = fields.Float(string=u'НӨАТ-ын баримт олгосон нийт дүн')
    eline_ids = fields.One2many('shift.working.diff.register','shift_id',string='Employee diff')
    shift = fields.Integer(string='shift')
    active = fields.Boolean(string='Active',default=True)
    _sql_constraints = [
        ('name_company_uniq', 'unique (name, company_id)', u'Өмнөх ээлж хаагдаагүй эсвэл ээлжийн код давхцаж болохгүй тул та бүртгэлээ шалгана уу !'),
    ]
    
    
    def def_action_last_month_day(self):
        last = False
        input_dt =  datetime.strptime(str(self.shift_date), DATE_FORMAT)

        next_month = input_dt.replace(day=28) + timedelta(days=4)
        res = next_month - timedelta(days=next_month.day)
        if str(res) == str(self.shift_date):
            last = True
        
        self.is_month_qty = last
        
        
    def action_download_atg_windbell(self):
        
        scaling_reg_obj = self.env['scaling.register']
        can_obj = self.env['can.register']
        
        mydb = mysql.connector.connect(
            host = "atg.shunkhlai.mn",
            user = "guur",
            password = "Q2d07.r0m",
            database = "wb_igasstationstockmgt"
        )
        
        # Creating an instance of 'cursor' class 
        # which is used to execute the 'SQL' 
        # statements in 'Python'
        if mydb and mydb.is_connected():
            cursor = mydb.cursor()
            
            # Show database
            
            result = cursor.execute("SELECT oilH, density, oilT, tankNo, oilName FROM t_atg_data where iotDevID = '%s'"% (self.shts_id.code))
             
            rows = cursor.fetchall()
            
            for rows in rows:
                
                can_id = can_obj.search([('shts_id','=',self.shts_id.id),('atg_number','=',rows[3])])
                if can_id:
                    sc_id = scaling_reg_obj.search([('shift_id','=',self.id),('can_id','=',can_id.id)])
                    if sc_id:
                        sc_id.write({
                                         'name':round(rows[0]/10,2),
                                        'self_weight_value':round(rows[1],4),
                                        'temperature':round(rows[2]),
                                    }) 
                    else:
                        raise UserError(_(u"Ээлж дээр %s савны мэдээлэл олдсонгүй шалгана уу!  ")% (can_id.name)) 
                else:
                    raise UserError(_(u"%s савны мэдээлэл олдсонгүй ATG болон Гүүр систем дээрх савны дугаар таарч байгаа эсэхийг шалгана уу!  ")% (rows[3])) 
            
            mydb.close()
        else:
        
            print("Could not connect server")
        
        self.write({'is_download_atg':True})

    def action_download_atg(self):
        
        atg_conf_obj = self.env['atg_conf']
        scal_obj = self.env['scaling.register']
        
        conf_id = atg_conf_obj.search([('shts_id', '=', self.shts_id.id)])
        if len(conf_id)>1:
            raise UserError(_(u"ШТС дээр 2 удаа савны түвшин хэмжигчийн тохиргоо хийгдсэн байна шалгана уу!!!"))
        
        if not conf_id:
            raise UserError(_(u"ШТС дээр савны түвшин хэмжигчийн тохиргоо хийгдсэн эсэхийг шалгана уу!!!"))
        
        tls = tls_socket.tlsSocket(conf_id.name, int(conf_id.port))
        #tls = tls_socket.tlsSocket("192.168.230.151", 10001) # initial connection
        #response = tls.execute("I10100", 3) # get system status report
        #response = tls.execute("I20I00", 3) # get system status report
        response = tls.execute("I21400", 3) # get system status report
        
        #i = 0
        y = 0
        for data in response.splitlines():
            #if i == 0:
               #ddate  = data[0:17]
               #datetime_obj = datetime.strptime(ddate, '%m/%d/%y %I:%M %p')
               #formatted_datetime_str = datetime_obj.strftime('%Y-%m-%d %H:%M:%S')
               #print('Ognoo:',data[0:17])
               #print('dateee',formatted_datetime_str,fields.datetime.now())
               #i+= 1
            if y ==1:
               
               #print('tank',data[5:11],'volume',data[27:33],'mass',data[35:44],'density',round(float(data[51:56])/1000,4),'height',data[57:64],'temp',data[73:78])
               can_obj = self.env['can.register']
               can_id = can_obj.search([('name','=',str(data[5:11]))])
               
               if not can_id:
                  raise UserError(_(u'Савны нэр түвшин хэмжигч дээр зөв оруулсан эсэхээ шалгана уу!!! %s')% (data[5:11]))
              
               if len(can_id) > 1:
                   raise UserError(_(u'%s сав 2 удаа бүртгэгдсэн байна шалгана уу!!! ')% (data[5:11]))
                    
              
                       
               scal_id = scal_obj.search([('shift_id', '=', self.id),('can_id', '=', can_id.id)])
               if scal_id:
                   
                   scal_id.write({
                                         'name':round(float(data[57:64])/10,2),
                                        'self_weight_value':round(float(data[51:56])/1000,4),
                                        'temperature':float(data[73:78]),
                                    })
               
            
            if data[0:12] == "TANK PRODUCT":
               y = 1
        self.write({'is_download_atg':True})
    
    
    def action_shift_working_diff_compute(self):
        shift_working_diff_register_obj = self.env['shift.working.diff.register']
        shts_income_obj = self.env['shts.income']
        for shift in self:
            count = 0
            date11 = datetime.strptime(str(shift.shift_date), DATE_FORMAT)
            pdate = date11-timedelta(days=1)
            signin_day_b = datetime.strptime(str(shift.shift_date), DATE_FORMAT)
            signin_day = signin_day_b.replace(hour=0, minute=0, second=1)
            signout_day = signin_day_b.replace(hour=23, minute=59, second=59)
            pro_type = ''
            for dil in shift.scale_ids:
                if dil.product_id.prod_type=='lpg':
                    pro_type = 'lpg'
                elif dil.product_id.prod_type=='gasol':
                    pro_type = 'gasol'
            if pro_type == 'gasol':
                income = shts_income_obj.search([('shts_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','not in',('cancel','draft'))])
            elif pro_type == 'lpg':
                income = gas_income_obj.search([('shts_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','not in',('cancel','draft'))])
            
            
            if pro_type == 'gasol':
                oth_exp = shts_income_obj.search([('warehouse_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','not in',('cancel','draft'))])
            elif pro_type == 'lpg':
                oth_exp = gas_income_obj.search([('warehouse_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','not in',('cancel','draft'))])
            
                    
            shift_day = shift.shift
            for pro in shift.shts_id.line_ids:
                
                for line in shift.scale_ids:
                    
                    if pro.product_id.id == line.product_id.id:
                        first_litr = 0.0
                        first_height = 0.0
                        first_self_weight = 0.0
                        first_temp = 0.0
                        first_kg = 0.0
                        income_litr = 0.0
                        income_kg = 0.0
                        income_dhj = 0.0
                        exp_litr = 0.0
                        exp_kg = 0.0
                        have_litr = 0.0
                        have_kg = 0.0
                        last_litr = 0.0
                        last_kg = 0.0
                        diff_litr = 0.0
                        diff_kg = 0.0
                        dhg_amount = 0.0
                        zarlaga_kg = 0.0
                        zarlaga_litr = 0.0
                        income_total = 0.0
                        exp_zalin = 0.0
                        last_self_weight = 0.0
                        last_temp = 0.0
                        zalin = 0.0
                        dhj = 0.0
                        oth_exp_litr = 0.0
                        oth_exp_kg = 0.0
                        count +=1
                        product_id = False
                        if line.can_id.id and line.product_id.id==pro.product_id.id:
                            product_id = line.product_id.name
                            if shift.shts_id.work_time==2:
                                if shift.shift == 1:
                                    pre_shift = self.search([('shts_id','=',shift.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                                else:
                                    pre_shift = self.search([('shts_id','=',shift.shts_id.id),('shift_date','=',shift.shift_date),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                                
                            elif shift.shts_id.work_time==1:
                                pre_shift = self.search([('shts_id','=',shift.shts_id.id),('shift_date','=',pdate),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                                
                            if pre_shift:
                                
                                for li in pre_shift.scale_ids:
                                    if li.can_id.id==line.can_id.id and pro.product_id.id==li.product_id.id:
                                        first_height = li.name
                                        first_self_weight = li.self_weight_value
                                        first_temp = float(li.temperature)
                                        first_kg += li.kg
                                        first_litr += li.litr
                                last_temp = float(line.temperature)
                                last_litr = float(line.litr)
                                last_kg = float(line.kg)
                                last_self_weight = line.self_weight_value
                                last_heigth = line.name
                                #dhj = line.dhj_amount
                            else:
                                #first_litr = self.action_calculate_litr(line.can_id.height, self.can_id)
                                #first_kg = first_litr*line.can_id.self_weight
                                #first_height = line.can_id.height
                                #first_self_weight = line.can_id.self_weight
                                #first_temp = float(line.can_id.temperature)
                                #last_temp = float(line.temperature)
                                #dhj = line.dhj_amount
                                last_litr = float(line.litr)
                                last_kg = float(line.kg)
                                last_self_weight = line.self_weight_value
                                last_heigth = line.name
                            
                            for li in income.line_ids:
                                if li.can_id.id == line.can_id.id:
                                    if li.product_id.id == pro.product_id.id:
                                        income_litr += li.hariu_litr
                                        income_kg += li.hariu_kg
                            
                            for oth in oth_exp.line_ids:
                                if oth.out_can_id.id == line.can_id.id:
                                    if oth.product_id.id == pro.product_id.id:
                                        oth_exp_litr += oth.torkh_size
                                        oth_exp_kg += oth.kg
                                
                        in_num = first_litr+income_litr+last_litr
                        if income_litr>0.0:
                            if in_num > 0.0:
                                income_dhj = round((first_kg+income_kg+last_kg)/in_num,4)
                            else:
                                income_dhj = 0.0
                        else:
                            if first_litr >0.0 and last_litr >0.0:
                                income_dhj = round((first_kg+last_kg)/(first_litr+last_litr),4)
                            else:
                                income_dhj = 0.0
                        for exp in shift.mile_target_ids:
                           if exp.can_id.id==line.can_id.id:
                                #exp_kg += exp.expense_kg
                                exp_litr += exp.expense_litr
                                zalin +=exp.zalin
                            
                        exp_kg = round(exp_litr*income_dhj,2)
                        have_kg = first_kg + income_kg - exp_kg - oth_exp_kg
                        have_litr = first_litr + income_litr - exp_litr - oth_exp_litr   
                        zarlaga_kg = exp_litr
                        diff_litr = last_litr - have_litr
                        diff_kg = last_kg - have_kg
                        count_work = 0
                        for emp in shift.shift_line_ids:
                            count_work +=1
                        for eline in shift.shift_line_ids:
                            if diff_kg !=0.0:
                                diff_value =diff_kg/count_work
                                shift_working_diff_id = shift_working_diff_register_obj.search([('employee_id','=',eline.employee_id.id),('product_id','=',pro.product_id.id),('shift_id','=',shift.id),('shts_id','=',shift.shts_id.id),('shift_date','=',shift.shift_date),('diff','=',diff_value)])
                                if not shift_working_diff_id:
                                    shift_working_diff_register_obj.create({
                                                                            'company_id':shift.company_id.id,
                                                                            'diff':diff_value,
                                                                            'shift_date':shift.shift_date,
                                                                            'employee_id':eline.employee_id.id,
                                                                            'product_id':pro.product_id.id,
                                                                            'shts_id':shift.shts_id.id,
                                                                            'shift_id':shift.id
                                                                            })

     

    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False, submenu=False):


        res = super(ShiftWorkingRegister, self).fields_view_get(
            view_id=view_id, view_type=view_type,
            toolbar=toolbar, submenu=submenu)

        group_id = self.user_has_groups('mn_web_portal.group_shift_create')

        doc = etree.XML(res['arch'])

        if group_id:

            if view_type == 'tree':

                nodes = doc.xpath("//tree[@string='Shift']")

                for node in nodes:

                    node.set('create', '1')

                res['arch'] = etree.tostring(doc)

        return res

    def action_loan_data_download(self):
        shift_obj = self.env['shift.working.register']
        loan_obj = self.env['loan.document.register.line']
        expense_obj = self.env['expense.type']
        for line in self:

            pdate = line.shift_date - timedelta(days=1)
            if self.shts_id.work_time == 2:
                if line.shift == 1:
                    pre_shift = shift_obj.search(
                        [('shts_id', '=', self.shts_id.id), ('shift_date', '=', pdate), ('shift', '=', 2),
                         ('state', 'in', ('draft', 'confirm', 'count'))], order="shift asc")
                else:
                    pre_shift = shift_obj.search(
                        [('shts_id', '=', self.shts_id.id), ('shift_date', '=', line.shift_date), ('shift', '=', 1),
                         ('state', 'in', ('draft', 'confirm', 'count'))], order="shift asc")
            elif self.shts_id.work_time == 1:
                pre_shift = shift_obj.search(
                    [('shts_id', '=', self.shts_id.id), ('shift_date', '=', pdate), ('shift', '=', 1),
                     ('state', 'in', ('draft', 'confirm', 'count'))], order="shift asc")

            if not pre_shift.eelj_huleeltssen_date:
                 raise UserError(_(u"Өмнөх ээлж дээр ээлж хүлээлцсэн огноо талбар бөглөнө үү!!!"))
             
            if not line.eelj_huleeltssen_date:
                raise UserError(_(u"Ээлж дээр ээлж хүлээлцсэн огноо талбар бөглөнө үү!!!"))
            
            if not line.shts_id.shts_code:
                 raise UserError(_(u"ШТС-ын бүртгэл дээр байгаа ШТС код талбарыг бөглөөгүй байна!!!"))
             
            time = line.eelj_huleeltssen_date - pre_shift.eelj_huleeltssen_date
            times = time.total_seconds()/3600
            if times > 24.5:
               raise UserError(_(u"2 ээлжийн хоорондох зай 1 өдрөөс их байна. Татах боломжгүй"))
            self.env.cr.execute("""select i.car_number, i.partner_vat,rp.id as rpid, m.id as borluult_id,
                        t.id as product_id ,l.unit_price, l.size, l.total_amount, m.shift_date 
                        from borluulaltin_medee as m
                        left join golomt_payment_information as i on m.id = i.sale_id
                        left join golomt_sales_product_list as l on m.id = l.sale_id
                        left  join product_product as t on l.code = t.default_code
                        left  join res_partner as rp on rp.vat = i.partner_vat
                        where i.payment_type = 'loan' and t.prod_type = 'gasol' and m.shts_code = '%s' and m.shift_date >= '%s' and m.shift_date <= '%s'
                        """ % (line.shts_id.shts_code, pre_shift.eelj_huleeltssen_date, line.eelj_huleeltssen_date))
            bor_data = self._cr.dictfetchall()
            pay_id = expense_obj.search([('company_id', '=', line.company_id.id), ('type', '=', 'loan')])
            
            for bor in bor_data:
                
                partner = False
                if not bor['rpid']:
                     if bor['partner_vat']:
                      self.env.cr.execute("""
                      SELECT id 
                      FROM res_partner 
                      where name like '%s' limit (1)
                        """%(str('%'+bor['partner_vat'])))
                      partner_obj  = self._cr.dictfetchall()
                      
                      if partner_obj:
                          for pr in partner_obj:
                            partner = pr['id']
                         
                else:
                    partner = bor['rpid']
                
                loan_id = loan_obj.search([('id','!=',self.ids)], order="id desc", limit=1)
                loan = int(loan_id) + 1
                
                loan_ids = loan_obj.search([('shift_id','=',line.id),('desc','=',bor['borluult_id']),('car_number_id','=',bor['car_number'])], limit=1)        
                if not loan_ids:
                    loan_obj.create({
                        'expense_type_id': pay_id.id,
                        'name': loan,
                        'desc': bor['borluult_id'],
                        'partner_id':partner,
                        'product_id':bor['product_id'],
                        'car_number_id':bor['car_number'],
                        'litr':bor['size'],
                        'price':bor['unit_price'],
                        'total_amount':bor['total_amount'],
                        'drive_id':'-',
                        'shift_id': line.id,
                    })

    
    def action_cash_sales_info(self):
        cash_obj = self.env['cash.sales.info']
        partner_obj = self.env['res.partner']
        shts_reg_line_obj = self.env['shts.register.line']
        can_register_obj = self.env['can.register']
        #for cline in self.infor_data_lines:
        #    cline.unlink()
        for line in self:
            self_weight = 0.0
            work_center = False
            
            exp_kg = 0.0
            exp_litr = 0.0

            for ttm in line.ttm_sale_ids:
                if ttm.expense_type_id.type =='cash':
                    partner = False
                    if ttm.partner_id.id:
                        partner = ttm.partner_id.id
                    else:
                        partners = partner_obj.search([('name','=',line.shts_id.name)],limit=1)
                        partner = partners.id
                    cash_id = cash_obj.search([('qty','=',ttm.qty),('product_id','=',ttm.product_id.id),('sale_type','=',ttm.expense_type_id.id),('can_id','=',ttm.can_id.id),('partner_id','=',partner),('shift_id','=',self.id)])
                    if cash_id.id ==False:
                        exp_kg +=ttm.kg
                        exp_litr +=ttm.litr
                        can_id = False
                        can_ids = can_register_obj.search([('shts_id','=',line.shts_id.id),('is_oil_can','=',True)],limit=1)
                        if can_ids:
                            can_id = can_ids.id
                        else:
                            raise UserError(_(u"Тосны сав үүсээгүй байгаа тул үүсгэнэ үү!!!"))
                        
                        cash_obj.create({
                                        'product_id':ttm.product_id.id,
                                        'can_id':can_id,
                                        'partner_id':partner,
                                        'litr':ttm.qty,
                                        'kg':0.0,
                                        'price':ttm.price,
                                        'total':ttm.total_price,
                                        'sale_type':ttm.expense_type_id.id,
                                        'self_weight':self_weight,
                                        'qty':ttm.qty,
                                        'work_center':work_center,
                                        'shift_id':self.id,
                                    })
                    else:
                        cash_id.write({
                                         'product_id':ttm.product_id.id,
                                        'can_id':can_id,
                                        'partner_id':partner,
                                        'litr':ttm.qty,
                                        'kg':0.0,
                                        'price':ttm.price,
                                        'total':ttm.total_price,
                                        'sale_type':ttm.expense_type_id.id,
                                        'self_weight':self_weight,
                                        'qty':ttm.qty,
                                        'work_center':work_center,
                                        'shift_id':self.id,
                                    })
            
            for pro in line.scale_ids:
                expense_litr = 0.0
                for exp in line.loan_document_ids:
                    if exp.can_id.id == pro.can_id.id and pro.product_id.id==exp.product_id.id and exp.expense_type_id.type =='loan':
                        expense_litr +=exp.litr
                for sline in line.shift_product_ids:
                    if pro.product_id.id == sline.product_id.id and pro.can_id.id==sline.can_id.id:
                        partner = False
                        partners = partner_obj.search([('name','=',line.shts_id.name)],limit=1)
                        partner = partners.id
                        cash_id = cash_obj.search([('litr','=',sline.sale_litr),('product_id','=',pro.product_id.id),('can_id','=',pro.can_id.id),('partner_id','=',partner),('shift_id','=',self.id)])
                        #pro_litr = 0
                        pro_kg = 0
                        #price = 0
                        expense_ids = self.env['expense.type'].search([('type','=','cash')],limit=1)
 
                        if cash_id.id == False:
                            self_weight = pro.dhj_amount
                            
                            pro_litr = sline.sale_litr-expense_litr
                            pro_kg = pro_litr*self_weight
                            price = sline.sale_price
                            total = sline.sale_price*pro_litr
                            if total>0.1:
                                cash_obj.create({
                                                'product_id':pro.product_id.id,
                                                'can_id':pro.can_id.id,
                                                'partner_id':partner,
                                                'litr':pro_litr,
                                                'kg':pro_kg,
                                                'price':price,
                                                'total':total,
                                                'sale_type':expense_ids.id,
                                                'self_weight':self_weight,
                                                'qty':0,
                                                'work_center':work_center,
                                                'shift_id':self.id,
                                            })
                        else:
                            pro_litr = sline.sale_litr-expense_litr
                            price = sline.sale_price
                            total = sline.sale_price*pro_litr
                            if total >0.0:
                                cash_id.write({
                                                'product_id':pro.product_id.id,
                                                'can_id':pro.can_id.id,
                                                'partner_id':partner,
                                                'litr':pro_litr,
                                                'kg':pro_kg,
                                                'price':price,
                                                'total':total,
                                                'sale_type':expense_ids.id,
                                                'self_weight':self_weight,
                                                'qty':0,
                                                'work_center':work_center,
                                                'shift_id':self.id,
                                                })
            
        
        return True
    

    
    
    def action_return(self):
        #for infor_line in self.infor_data_lines:
        #    infor_line.unlink()
            
        diff_data = self.env['shift.working.diff.register'].search([('shift_id','=',self.id)])
        for diff_ids in diff_data:
            diff_ids.unlink()
        for eline in self.eline_ids:
            eline.unlink()
        for ttm in self.ttm_sale_ids:
            pqty_obj = self.env['product.qty']
            ttm_ids = pqty_obj.search([('ttm_sale','=',ttm.id)])
            ttm_ids.unlink()
            
        for infor in self.infor_data_lines:
            infor.unlink()
            
        self.write({'state':'draft'})
    
    def action_total_shts_amount(self):
        for line in self:
            total_shts_amount = 0.0
            for lin in line.shift_product_ids:
                    total_shts_amount += lin.total_amount
            line.total_shts_amount = total_shts_amount
    
    
    def action_total_amount(self):
        for line in self:
            pay_amount = 0.0
            sale_amount = 0.0
            diff_amout = 0.0
            
            for li in line.payment_shts_id:
                pay_amount +=li.amount
            tvt_exp_amount = 0.0
            for tvt in line.ttm_sale_ids:
                if tvt.expense_type_id.type=='other':
                    tvt_exp_amount +=tvt.total_price
            sale_amount = line.tvt_ttm_total_amount+line.total_shts_amount-tvt_exp_amount
            line.total_sale_amount = sale_amount
            line.total_pay_amount = pay_amount
            diff_amout = pay_amount-sale_amount
            line.total_diff_amount = diff_amout
            # print('zzzzzzzzzzzzzzzzzz',line.total_sale_amount)
            
    def action_new_compute(self):
        can_obj = self.env['can.register.line']
        product_obj = self.env['shift.product.line']
        price_line_obj = self.env['price.change.register.line']
        shts_ids = [1385,1373,1360,1351,1347,1341,1339,1337,1335,1323,1319,1305,1298,1297,1296,1294]
        shift_ids = self.search([('shts_id','in',tuple(shts_ids))])
        for line in shift_ids:
            for list in line.scale_ids:
                for pro in line.shift_product_ids:
                    if list.product_id.id == pro.product_id.id:
                         product_ids = product_obj.search([('can_id','=',list.can_id.id),('product_id','=',list.product_id.id),('shift_id','=',line.id)])
                         if not product_ids:
                             product_obj.create({
                                                 'can_id':list.can_id.id,
                                                 'product_id':list.product_id.id,
                                                 'sale_price':pro.sale_price,
                                                 'sale_litr':list.total_litr,
                                                 'shift_id':line.id,
                                                 'total_amount':list.total_litr*pro.sale_price,
                                                 'change_price':list.total_litr,
                                                 'company_id':line.company_id.id,
                                                })

            
    def action_compute_shift(self):
        can_obj = self.env['can.register.line']
        for line in self:
            for list in line.shift_product_ids:
                if list.product_id:
                    can = can_obj.search([('product_id','=',list.product_id.id)],limit=1)
                    self.env.cr.execute(""" select cr.id from can_register as cr left join
                                            can_register_line as li on li.can_id = cr.id 
                                            where li.product_id = %s and cr.shts_id = %s limit 1
                                                """%(list.product_id.id,line.shts_id.id))
                    can_data = self._cr.dictfetchall()
                    for can in can_data:
                        list.write({'can_id':can['id']})
                    if list.sale_litr < 0.0:
                        list.write({'sale_litr':0.0})

    
    def action_compute_payment(self):
        price_line_obj = self.env['price.change.register.line']
        product_obj = self.env['shift.product.line']
        price_obj = self.env['price.change.register']
        payment_obj = self.env['payment.type.register']
        sh_payment_obj = self.env['shts.payment.register']
        mile_obj = self.env['mile.target.register.line']
        price_change_line_obj = self.env['price.gasol.register']
        sales_obj = self.env['borluulaltin.medee']
        payment = payment_obj.search([('company_id','=',self.company_id.id)])
        for pay in payment:
            sh_pay = sh_payment_obj.search([('name','=',pay.id),('shift_id','=',self.id)])
            if not sh_pay:
                sh_payment_obj.create({'name':pay.id,
                                       'amount':0.0,
                                       'company_id':self.company_id.id,
                                       'shift_id':self.id})
        
        for line in self:
            total_noat_amount = 0.0
            s_date = datetime.strptime(str(line.shift_date), DATE_FORMAT)
            s_date_f = s_date.replace(hour=0, minute=0, second=1)
            s_date_l = s_date.replace(hour=23, minute=59, second=59)
            sales_ids = sales_obj.search([('shift_date','>=',s_date_f),('shift_date','<=',s_date_l),('noat_amount','>',0.0),('shts_code','=',line.shts_id.shts_code)])
            
            if sales_ids:
                for sale in sales_ids:
                    total_noat_amount +=sale.pay_amount
                    
            for li in line.shift_product_ids:
                
                
                price = price_line_obj.search([('shts_id','in',line.shts_id.id),('product_id','=',li.product_id.id),('date','>=',s_date_f),('date','<=',s_date_l)])
                
                for pr in price:
                    if pr.state == 'confirm' or pr.state =='send':
                         if li.product_id.id == pr.product_id.id and li.sale_price != pr.new_price:
                            total_litr = 0.0
                            for lis in line.scale_ids:
                                if lis.can_id.id == li.can_id.id:
                                    total_litr +=lis.total_litr
                            sale_litr = 0.0
                            price_ids = price_change_line_obj.search([('can_id','=',li.can_id.id),('product_id','=',li.product_id.id),('price_change_id','=',pr.price_change_id.id)]) #ШТС үнэ солихоос авах эсвэл ШТС үнэ үүсгэхрүү савны мэдээлэл давхар шидэх
                            for tline in price_ids:
                                sale_litr += tline.diff_mile
                            if sale_litr>0.0:
                                li.write({'change_price':sale_litr})
                            if sale_litr >0.0:
                                
                                products = product_obj.search([('can_id','=',li.can_id.id),('product_id','=',pr.product_id.id),('sale_litr','=',total_litr-sale_litr),('sale_price','=',pr.new_price),('sale_litr','=',sale_litr),('shift_id','=',line.id),('company_id','=',line.company_id.id)])
                              
                                if not products:
                                     datas = product_obj.create({
                                                        'can_id':li.can_id.id, #oorchlolt
                                                        'product_id':pr.product_id.id,
                                                        'sale_price':pr.new_price,
                                                        'sale_litr':total_litr-sale_litr,
                                                        'shift_id':line.id,
                                                        'total_amount':sale_litr*pr.new_price,
                                                        'change_price':sale_litr,
                                                        'is_change_price':True,
                                                         'company_id':line.company_id.id,
                                                        })
                               

                                        
                         else:
                            for list in line.scale_ids:
                                 product_ids = product_obj.search([('can_id','=',list.can_id.id),('sale_litr','=',list.total_litr),('product_id','=',list.product_id.id),('shift_id','=',line.id)])
                                 if list.total_litr >0.0:
                                     if not product_ids:
                                         product_obj.create({
                                                                'can_id':list.can_id.id,
                                                                'product_id':list.product_id.id,
                                                                 'sale_price':list.product_id.price,
                                                                 'sale_litr':list.total_litr,
                                                                 'shift_id':line.id,
                                                                 'total_amount':list.total_litr*pr.new_price,
                                                                 'change_price':list.total_litr,
                                                                 'company_id':line.company_id.id,
                                                                    })
                                    
                    
                litrs = 0.0
                for loan in line.loan_document_ids:
                    if loan.product_id.id == li.product_id.id and loan.can_id.id==li.can_id.id:
                        if loan.expense_type_id.type !='other':
                            litrs += loan.litr
                        
                
                litr = li.sale_litr - litrs
                #if litr >0.0:
                #    li.write({'sale_litr':litr})
                #else:
               #     li.write({'sale_litr':0.0})
                        
                
            for list in line.ttm_sale_ids:
                s_date = datetime.strptime(str(line.shift_date), DATE_FORMAT)
                s_date_f = s_date.replace(hour=0, minute=0, second=1)
                s_date_l = s_date.replace(hour=23, minute=59, second=59)
                price = price_line_obj.search([('state','in',['confirm','done']),('shts_id','in',line.shts_id.id),('product_id','=',list.product_id.id),('date','>=',s_date_f),('date','<=',s_date_l)])
                if price:
                    for pr in price:
                        if pr.product_id.id==list.product_id.id and pr.new_price !=list.price:
                            list.write({'price':pr.new_price})
            for lists in line.tvt_sale_ids:
                s_date = datetime.strptime(str(line.shift_date), DATE_FORMAT)
                s_date_f = s_date.replace(hour=0, minute=0, second=1)
                s_date_l = s_date.replace(hour=23, minute=59, second=59)
                prices = price_line_obj.search([('state','in',['confirm','done']),('shts_id','in',line.shts_id.id),('product_id','=',lists.product_id.id),('date','>=',s_date_f),('date','<=',s_date_l)])
                if prices:
                    for prs in prices:
                        if prs.product_id.id==lists.product_id.id and pr.new_price !=list.price:
                            lists.write({'price':pr.new_price})
            count = 0              
            for li in line.payment_shts_id:
                if li.name.type=='loan':
                    amount = 0.0
                    tvt_amount = 0.0
                    tt_amount = 0.0
                    for loan in line.loan_document_ids:
                        if loan.expense_type_id.type == 'loan':
                            amount +=loan.total_amount
                    
                    for tvt_loan in line.tvt_sale_ids:
                        if tvt_loan.expense_type_id.type == 'loan':
                            tvt_amount +=tvt_loan.total_price
                    for tt_loan in line.ttm_sale_ids:
                        if tt_loan.expense_type_id.type == 'loan':
                            tt_amount +=tt_loan.total_price
                    li.amount = amount + tvt_amount + tt_amount
                    li.shts_code = line.shts_id.code
                    li.reg_date = line.shift_date
                    li.type = 'loan'
                elif li.name.type=='talon':
                    amount = 0.0
                    for talon in line.talon_sale_ids:
                        if talon.state=='confirm':
                            amount +=talon.talon_price
                    li.amount = amount
                    li.shts_code = line.shts_id.code
                    li.reg_date = line.shift_date
                    li.type = 'talon'
                elif li.name.type=='tov_card':
                    amount = 0.0
                    for tov in line.center_payment_income_ids:
                        if tov.type=='card':
                            amount +=tov.amount
                    li.amount = amount
                    li.shts_code = line.shts_id.code
                    li.reg_date = line.shift_date
                    li.type = 'tov_card'
                #elif li.name.type=='mobile':
                #    count +=1
                #    amount = 0.0
                #    for tov in line.center_payment_income_ids:
                #        if tov.type=='mobile':
                #            amount +=tov.amount
                #    if count == 1:
                #        li.amount = amount
                #        li.shts_code = line.shts_id.code
                #        li.reg_date = line.shift_date
                #        li.type = 'mobile'
                    
            line.write({'is_calculate':True,
                        'vat_amount':total_noat_amount,})
                    
    
    def action_tvt_total_amount(self):
        for line in self:
            total_amount = 0.0
            tvt_amount = 0.0
            ttm_amount = 0.0
            for lins in line.ttm_sale_ids:
                total_amount +=lins.total_price
                ttm_amount +=lins.total_price
            for lines in line.tvt_sale_ids:
                total_amount +=lines.total_price
                tvt_amount +=lines.total_price
                
            line.tvt_ttm_total_amount = total_amount
            line.tvt_total_amount = tvt_amount
            line.ttm_total_amount = ttm_amount
    
    @api.onchange('shts_id')
    def onchange_shts_id(self):
        self.region_id = self.shts_id.region_id.id
        self.company_id = self.shts_id.company_id.id
        if self.shts_id.is_oil_center:
            self.is_oil_center = True
        else:
            self.is_oil_center = False
            

    def write(self,vals):
        shts_obj = self.env['shts.register']
        shtswork =  super(ShiftWorkingRegister, self).write(vals)
        pr = self.search([('id','!=',self.ids),('shts_id','=',self.shts_id.id),('shift_date','<=',self.shift_date)],order='shift_date desc')
        if 'shift_date' in vals:
            if pr:
                counted = len(pr.ids)
                for pre_code in pr:
                   # if str(pre_code.shift_date) == str(vals['shift_date']):
                   #     raise UserError(u"Ижил огноонд үүсэхгүй тул анхаарна уу.")
                    #if str(pre_code.shift_date) < str(vals['shift_date']):
                    #    raise UserError(u"Өнөөдрийн огноо ээлжийн огнооноос бага байж болохгүй")
                    if pre_code.state=='count' or pre_code.state =='confirm':
                        pre = pre_code.name[16:]
                        if pre_code.shift_date == vals['shift_date']:
                            code = pre_code.name[:16]
                            pr = int(pre)+1
                            if len(str(pr))==1:
                                pr_dt = '-0%s'%pr
                            else:
                                pr_dt = '-%s'%pr
                        else:
                            shift_date = vals['shift_date'].replace('-','')
                            code = pre_code.name[:6]+'-%s'%shift_date
                            pr_dt = '-01'
                        self.write({'name':'%s'%code+str(pr_dt)})
                    else:
                        raise UserError(u"Өмнөх ээлж хаагдаагүй байгаа тул хаана уу.")
            else:
                shts_id = shts_obj.browse(self.shts_id.id)
                if shts_id:
                    shts = shts_id.code
                    if len(shts)==1:
                        shts_code = '0%s'%shts
                    elif len(shts)==2:
                        shts_code = '0%s'%shts
                    else:
                        shts_code = '%s'%shts
                last = '01'
                shift_date = str(vals['shift_date']).replace('-','')
                new_code = '%s'%shts_code+'-%s-'%shift_date+last
                
                self.write({'name':new_code})
                    

        
        return shtswork
    

    
    @api.model
    def create(self, vals):
        shts_obj = self.env['shts.register']
        pr = self.search([('id','=',self.ids),('shts_id','=',vals['shts_id']),('state','!=','draft')],order='shift_date desc',limit=1)
        pre = self.search([('id','=',self.ids),('shts_id','=',vals['shts_id']),('state','=','draft')],order='shift_date desc',limit=1)
        if pr:
            
            for pre_code in pr:
                shift = pre_code.shift
                if shift < pre_code.shts_id.work_time:
                    shift_dates = datetime.strptime(str(pre_code.shift_date),DATE_FORMAT)
                elif shift ==pre_code.shts_id.work_time:
                    shift_dates = datetime.strptime(str(pre_code.shift_date),DATE_FORMAT)+timedelta(hours=24)
                elif shift > pre_code.shts_id.work_time:
                    shift_dates = datetime.strptime(str(pre_code.shift_date),DATE_FORMAT)+timedelta(hours=24)
                    
                #vals['shift_date'] = str(shift_dates)[:10]
                #if str(pre_code.shift_date) == str(vals['shift_date']):
                #    raise UserError(u"Ижил огноонд үүсэхгүй тул анхаарна уу.")
                if str(pre_code.shift_date) >= str(vals['shift_date']):
                    
                    #if str(pre_code.shift_date) < str(vals['shift_date']):
                    #    raise UserError(u"Өнөөдрийн огноо ээлжийн огнооноос бага байж болохгүй")
                    if pre_code.state=='count':
                        pre = pre_code.name[16:]
                        if str(pre_code.shift_date) == str(vals['shift_date']):
                            code = pre_code.name[:15]
                            pr = int(pre)+1
                            if len(str(pr))==1:
                                pr_dt = '-0%s'%pr
                                shift +=1
                            else:
                                pr_dt = '-%s'%pr
                                shift +=1
                        else:
                            shift_date = vals['shift_date'].replace('-','')
                            code = pre_code.name[:6]+'-%s'%shift_date
                            pr_dt = '-01'
                            shift = 1
                    else:
                        raise UserError(u"Өмнөх ээлж хаагдаагүй байгаа тул хаана уу.")
                else:
                        pre = pre_code.name[16:]
                        if shift_dates == vals['shift_date']:
                            code = pre_code.name[:15]
                            pr = int(pre)+1
                            if len(str(pr))==1:
                                pr_dt = '-0%s'%pr
                                shift +=1
                            else:
                                pr_dt = '-%s'%pr
                                shift +=1
                        else:
                            shift_date = str(vals['shift_date'])[:10].replace('-','')
                            code = pre_code.name[:6]+'-%s'%shift_date
                            pr_dt = '-01'
                            shift = 1
                vals['name'] = '%s'%code+str(pr_dt)
                vals['shift'] = shift
                vals['shift_date'] = shift_dates
        
        else:
            
            shts_id = shts_obj.browse(vals['shts_id'])
            if shts_id:
                shts = shts_id.code
                if len(shts)==1:
                    shts_code = '0%s'%shts
                elif len(shts)==2:
                    shts_code = '0%s'%shts
                else:
                    shts_code = '%s'%shts
            last = '01'
            #shift_dates = datetime.strptime(str(fields.Date.today()),DATE_FORMAT)+timedelta(days=1)
            #vals['shift_date'] = str(shift_dates)[:10]
            shift_date = vals['shift_date'].replace('-','')
            vals['name'] = '%s'%shts_code+'-%s-'%shift_date+last
            last_id = self.search([('name','=',vals['name'])])
            if last_id.id!=False:
                if pre:
                    shift_datess = str(datetime.strptime(str(pre.shift_date),DATE_FORMAT)+timedelta(hours=pre.shts_id.work_time))[:10]
                    shift_dated = shift_datess.replace('-','')
                    vals['name'] = '%s'%shts_code+'-%s-'%shift_dated+last
                    in_date = datetime.strptime(str(shift_datess),DATE_FORMAT)
                    out_date = datetime.strptime(str(vals['shift_date']),DATE_FORMAT)
            #if in_date < out_date:
            #    raise UserError(u"Огноо алгасаж болохгүй тул анхаарна уу.")
        shtswork = super(ShiftWorkingRegister, self.with_context()).create(vals)
        return shtswork
    
    def add_zero(self, too):
        if len(str(too)) == 1 :
            oron = '00000' + str(too)
        elif len(str(too)) == 2 :
            oron = '0000' + str(too)
        elif len(str(too)) == 3 :
            oron = '000' + str(too)
        elif len(str(too)) == 4 :
            oron = '00' + str(too)
        elif len(str(too)) == 5 :
            oron = '0' + str(too)
        else :
            oron = too
        return oron
    
    def unlink(self):
        for line in self:
            if line.state in ('confirm'):
                raise UserError(_("You cannot delete an entry which has been posted once."))
            line.shift_line_ids.unlink()
            line.payment_shts_id.unlink()
            line.service_ids.unlink()
            line.talon_sale_ids.unlink()
            line.ttm_sale_ids.unlink()
        return super(ShiftWorkingRegister, self).unlink()
    
    def shift_cancel(self):
        self.write({'state':'cancel'})
        
    def action_done(self):
        inventory_obj = self.env['inventory.product']
        report_date_obj = self.env['report.date.register']
        for line in self:
            line.action_shift_working_diff_compute()
            report_date_ids = report_date_obj.search([('end_date','=',line.shift_date),('company_id','=',line.company_id.id)])
            if report_date_ids:
                inventory_ids = inventory_obj.search([('state','=','draft'),('inventory_date','=',line.shift_date),('shts_id','=',line.shts_id.id),('reg_date','=',str(report_date_ids.inventory_date)),('company_id','=',line.company_id.id)])
                if inventory_ids:
                    raise UserError(u"ТТМ Тооллогоо эхлүүлнэ үү!")

                
        self.write({'state':'confirm',
                    'shift_confirm_date':fields.datetime.now(),
                     'confirm_user_id':self.env.user.id,
                  })
        
    def action_next_shift_create(self):
        pr = self.search([('id','!=',self.ids),('shts_id','=',self.shts_id.id),('state','=','draft')],order='shift_date desc')
        if pr.id !=False:
            shift_date = datetime.strptime(str(pr.shift_date),DATE_FORMAT)+timedelta(hours=self.shts_id.work_time)
        else:
            shift_date = datetime.strptime(str(self.shift_date),DATE_FORMAT)+timedelta(hours=self.shts_id.work_time)
        
        
        next_ids = self.create(
                    {'company_id':self.company_id.id,
                     'shts_id':self.shts_id.id,
                     'region_id':self.region_id.id,
                     'reg_user_id':self.reg_user_id.id,
                     'shts_reg_user_id':self.shts_reg_user_id.id,
                     'shift_date':str(shift_date)[:10],
                     'state':'draft'
                     })
        next_ids.action_compute_payment()
        
    def shift_close(self):
        talon_obj = self.env['talon.inf']
        talon_lost_obj = self.env['talon.lost']
        talon_history_obj = self.env['talon.history']
        price_obj = self.env['price.register.shts.line']
        shts_income_obj = self.env['shts.income']
        qty_obj = self.env['product.qty']
        inventory_obj = self.env['inventory.product']
        report_date_obj = self.env['report.date.register']
        shts_reg_line_obj = self.env['shts.register.line']
        scaling_reg_obj = self.env['scaling.register']
        
        

        
        for scal in self.scale_ids:
            total_amount_litr = 0.0
            for loan in self.loan_document_ids:
                if scal.product_id.id == loan.product_id.id and loan.can_id.id==scal.can_id.id:
                    total_amount_litr +=loan.litr
                
                
                sc_id = scaling_reg_obj.search([('shift_id','=',self.id),('product_id','=',loan.product_id.id ),('can_id','=',loan.can_id.id)])
                if not sc_id:
                    raise UserError(_(u"Зээлийн падаан дээрх %s бүтээгдэхүүн болон %s %s сав зөрсөн байна шалгана уу!  ")% (loan.product_id.name,loan.can_id.name,loan.can_id.code))   
            if round(total_amount_litr,2) > round(scal.total_litr,2):
                raise UserError(_(u"Падааны бүртгэл дээрх саваа шалгана уу! \n Хэмжилтийн утгаарх зарлагын литрээс падааны бүртгэл дээрх литр их байна! %s %s %s %s ")% (round(total_amount_litr,2), round(scal.total_litr,2),scal.product_id.name,scal.can_id.name))
                      
                
        
        for ml in self.mile_target_ids:
            if ml.end_mile == 0.0:
                raise UserError(u"Эцсийн милл 0 байгаа тул шалгана уу!")
        
        for line in self:
            report_date_idss = report_date_obj.search([('inventory_date','=',line.shift_date),('company_id','=',line.company_id.id)])
            if report_date_idss:
                inventory_idss = inventory_obj.search([('state','in',('draft','confirm')),('shts_id','=',line.shts_id.id),('reg_date','=',str(report_date_idss.inventory_date)),('company_id','=',line.company_id.id)])
                if inventory_idss:
                    raise UserError(u"ТТМ Тооллогоо хаана үү!")
                
            report_date_ids = report_date_obj.search([('end_date','=',line.shift_date),('company_id','=',line.company_id.id)])
            if report_date_ids:
                if line.shts_id.work_time == line.shift:
                    shift_work_ids = self.search([('state','!=','confirm'),('shts_id','=',line.shts_id.id),('shift_date','<=',line.shift_date),('id','!=',line.id)])
                    if shift_work_ids:
                        raise UserError(u"Батлагдаагүй ээлж байгаа тул шалгана уу!")
                    inventory_ids = inventory_obj.search([('state','!=','cancel'),('inventory_date','=',str(line.shift_date)),('shts_id','=',line.shts_id.id),('reg_date','=',str(report_date_ids.inventory_date)),('company_id','=',line.company_id.id)])
                    if not inventory_ids:
                        inventory_obj.create({
                                            'state':'draft',
                                            'inventory_date':line.shift_date,
                                            'shts_id':line.shts_id.id,
                                            'reg_date':report_date_ids.inventory_date,
                                            'company_id':line.company_id.id,
                                            'reg_user_id':line.reg_user_id.id,
                                            })

            inventory_id = inventory_obj.search([('shts_id','=',line.shts_id.id),('state','in',['done','cancel']),('shift','=',line.shift),('reg_date','=',line.shift_date)])
            if inventory_id:
                raise UserError(u"Тооллого батална уу!")
            
            

            for oil_sale in line.ttm_sale_ids:
                qty_data = qty_obj.search([('shts_id','=',line.shts_id.id),('product_id','=',oil_sale.product_id.id),('reg_date','<=',line.shift_date)])
                qty = 0.0
                shts_reg_line_ids = shts_reg_line_obj.search([('oil_shts_id','=',line.shts_id.id),('product_id','=',oil_sale.product_id.id)])
                if shts_reg_line_ids:
                    if len(shts_reg_line_ids) > 1:
                        raise UserError(_(u"%s %s бараа 1ээс олон бүртгэгдсэн байна. Тохиргоо - ШТС бүртгэл - Тос хэсгээс шалгана уу!")% (oil_sale.product_id.default_code, oil_sale.product_id.name))
                        
                    if oil_sale.price != shts_reg_line_ids.price:
                        raise UserError(_(u"%s %s бүтээгдэхүүний зарах үнэ зөрүүтэй байна. Тохиргоо - ШТС бүртгэл - Тос хэсгээр орж шалгана уу!")% (oil_sale.product_id.default_code, oil_sale.product_id.name))
                if qty_data:
                    for qt in qty_data:
                        if qt.type == 'in':
                            qty +=qt.qty
                        else:
                            qty +=qt.qty
                if qty < oil_sale.qty:
                    raise UserError(_(u"%s %s барааны үлдэгдэл хүрэхгүй байгаа тул шалгана уу!")% (oil_sale.product_id.default_code, oil_sale.product_id.name))
                
                
                qty_data_ids = qty_obj.search([('ttm_sale','=',oil_sale.id),('shts_id','=',line.shts_id.id),('product_id','=',oil_sale.product_id.id),('reg_date','=',line.shift_date)])
                if not qty_data_ids:
                    qty_obj.create({'qty':oil_sale.qty*(-1),
                                        'product_id':oil_sale.product_id.id,
                                        'shts_id':line.shts_id.id,
                                        'reg_date':line.shift_date,
                                        'ttm_sale':oil_sale.id,
                                        'type':'out',
                                        'company_id':line.company_id.id})
                #else:
                #    qty_data_ids.write({'qty':oil_sale.qty*(-1)})
                    

            if line.total_diff_amount > line.shts_id.shift_diff_amount:
                raise UserError(u"Зөрүү дүн их байгаа тул штс ээлжийн зөрүү дүн зөв эсэхийг шалгана уу!")
            self.action_cash_sales_info()
            income_id = shts_income_obj.search([('shift_date','=',line.shift_date),('shts_id','=',line.shts_id.id),('state','=','compute'),('shift','=',self.shift)])
            if income_id:
                for inc in income_id:
                    if inc.state=='compute':
                        raise UserError(u"Ээлж дээр хүлээн авсан орлого байгаа тул батална уу.")
            price_id = price_obj.search([('shts_id','=',line.shts_id.id),('price_change_id.shift_date','=',line.shift_date),('shift','=',self.shift),('price_change_id.state','=','send')])
            if price_id:
                 raise UserError(u"Ээлж дээр үнэ солигдох хүсэлт байгаа тул шалгана уу.")
             
            if int(line.shift) < int(line.shts_id.work_time):
                shift_date = datetime.strptime(str(self.shift_date),DATE_FORMAT)
            elif int(line.shift) == int(line.shts_id.work_time):
                shift_date = datetime.strptime(str(self.shift_date),DATE_FORMAT)+timedelta(hours=24)
            elif int(line.shift) > int(line.shts_id.work_time):
                shift_date = datetime.strptime(str(self.shift_date),DATE_FORMAT)+timedelta(hours=24)
                
            pre_date = str(shift_date)[:10]
            pre = self.search([('id','!=',self.ids),('shts_id','=',self.shts_id.id),('shift_date','=',str(pre_date)),('state','!=','cancel')],order='shift_date desc',limit=1)
            for li in line.talon_sale_ids:
                for lin in range(1,int(li.qty)):
                    useg = str(li.type_id.type_letter)
                    start1 = self.add_zero(str(lin))
                    series = useg + str(start1)
                    talon = talon_obj.search([('name','=',series),('state','=','print')])
                    lost_id = talon_lost_obj.search([('name','=',series)])

            if line.not_working == True:
                self.write({'state':'count',
                            'end_date':fields.Datetime.now(),
                            })
                if pre.id == False:
                    next_ids = self.create({'company_id':self.company_id.id,
                                 'shts_id':self.shts_id.id,
                                 'region_id':self.region_id.id,
                                 'reg_user_id':self.reg_user_id.id,
                                 'shift_date':str(shift_date)[:10],
                                 'is_oil_center':self.is_oil_center,
                                 'state':'draft'
                                 })
                    next_ids.action_compute_payment()
                    next_ids.action_data_download()
            else:
                if line.total_sale_amount > 0.0:
                    count = 0
                    if not line.shift_manager_line_ids or not line.shift_line_ids and line.shts_id.is_oil_center == False:
             
                       raise UserError(u"Ээлжинд ажилласан эрхлэгч болон НХО мэдээлэл оруулсан эсэхээ шалгана уу.")
                    for lis in line.shift_manager_line_ids:
                        
                         count +=1
                         if count == 1:
                             self.write({'state':'count',
                                'end_date':fields.Datetime.now(),
                                })
                             if pre.id == False:
                                 next_ids = self.create({'company_id':self.company_id.id,
                                     'shts_id':self.shts_id.id,
                                     'region_id':self.region_id.id,
                                     'reg_user_id':self.reg_user_id.id,
                                     'shift_date':str(shift_date)[:10],
                                     'is_oil_center':self.is_oil_center,
                                     'state':'draft'
                                     })
                                 next_ids.action_compute_payment()
                                 next_ids.action_data_download()
                else:
                    raise UserError(u"Ээлж үйл ажиллагаа явуулаагүй эсэхийг шалгана уу.")
                
            ##else:
        #    
            
    
    def action_data_download(self):
        shts_obj = self.env['shts.register']
        scale_obj = self.env['scaling.register']
        can_obj = self.env['can.register']
        mile_target_obj = self.env['mile.target.register.line']
        mile_obj = self.env['mile.target.register']
        ttm_sale_obj = self.env['ttm.sale.register.line']
        payment_obj = self.env['payment.type.register']
        sh_payment_obj = self.env['shts.payment.register']
        shift_product_obj = self.env['shift.product.line']
        can = can_obj.search([('shts_id','=',self.shts_id.id),('is_oil_can','=',False)])
        shts_ids = shts_obj.search([('id','=',self.shts_id.id)])
        payment = payment_obj.search([('company_id','=',self.company_id.id)])
        
        
        
        for pay in payment:
            sh_pay = sh_payment_obj.search([('name','=',pay.id),('shift_id','=',self.id)])
            if not sh_pay:
                sh_payment_obj.create({'name':pay.id,
                                       'amount':0.0,
                                       'company_id':self.company_id.id,
                                       #'diff_amount':0.0,
                                       'shift_id':self.id})

                

        for canline in can:
            for cline in canline.line_ids:
                if cline.active == True:
                    dts = datetime.strptime(str(self.reg_date)[:19],DATETIME_FORMAT)
                    for tab in canline.tablits_id:
                        if str(tab.start_date) <= str(dts.date())<= str(tab.end_date):
                            scale_id = scale_obj.search([('shift_id','=',self.id),('product_id','=',cline.product_id.id),('can_id','=',canline.id)])
                            if not scale_id:
                                scale_obj.create({
                                                    'can_id':canline.id,
                                                    'product_id':cline.product_id.id,
                                                    'reg_date':self.shift_date,
                                                    'reg_user_id':self.reg_user_id.id,
                                                    'terminal_id':cline.terminal_id.id,
                                                    'shift_id':self.id,
                                                    'name':0.0,
                                                    'temperature':0.0,
                                                    'litr':0.0,
                                                    'kg':0.0,
                                                    'self_weight':0.0,
                                                    })
                            shift_product_id = shift_product_obj.search([('can_id','=',canline.id),('shift_id','=',self.id),('product_id','=',cline.product_id.id)])
                            if not shift_product_id:
                                for shts in self.shts_id.line_ids:
                                    if shts.product_id.id == cline.product_id.id:
                                        shift_product_obj.create({
                                                                  'can_id':canline.id,
                                                                  'product_id':cline.product_id.id,
                                                                  'sale_price':shts.price,
                                                                  'shift_id':self.id})
                        #else:
                        #    raise UserError(u"Таблицийн хугацаа дууссан эсэх эсвэл сав дээр таблиц сонгосон эсэх шалгана уу.")
                    mile_id = mile_target_obj.search([('shift_id','=',self.id),('product_id','=',cline.product_id.id),('name','=',cline.break_id.name)])
                    pre_mile_id = self.search([('id','!=',self.id),('shts_id','=',self.shts_id.id),('shift','<=',self.shts_id.work_time),('shift_date','<=',self.shift_date)], order="shift_date desc,id desc ", limit=1)
                    first_mile = 0.0
                    end_mile = 0.0
                    if self.shift_date == cline.start_date and cline.shift == self.shift:
                        first_mile = cline.first_mile
                    else:
                        if pre_mile_id:
                            for lins in pre_mile_id.mile_target_ids:
                                if lins.can_id.id == canline.id and lins.product_id.id==cline.product_id.id and lins.name==cline.break_id.name:
                                    first_mile = lins.end_mile
                        else:
                            first_mile = cline.first_mile
                    if not mile_id:
                        mile_target_obj.create({
                                                'can_id':canline.id,
                                                'product_id':cline.product_id.id,
                                                'name':cline.break_id.name,
                                                'first_mile':first_mile,
                                                'end_mile':first_mile,
                                                'zalin':0.0,
                                                'active':True,
                                                'shift_id':self.id,
                                                'company_id':cline.company_id.id,
                                                })
        self.write({'is_shift_open':True})
            
            

            
class ShiftProductLine(models.Model):
    _name = 'shift.product.line'
    
    company_id = fields.Many2one('res.company',string=u'Компани'
                                 ,default=lambda self: self.env['res.company']._company_default_get('shift.product.line'))
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн')
    sale_price = fields.Float(string=u'Нэгж үнэ')
    sale_litr = fields.Float(string=u'Борлуулсан литр',compute="action_total_sale_litr")
    can_id = fields.Many2one('can.register',string=u'Сав')
    total_kg = fields.Float(string=u'Борлуулсан кг')
    total_amount = fields.Float(string=u'Нийт дүн')
    change_price = fields.Float(string='Change price')
    shift_id = fields.Many2one('shift.working.register',string='Shift register')
    is_change_price = fields.Boolean(string='Is change price')
    
    def action_total_sale_litr(self):
        for line in self:
            litr = 0.0
            total = 0.0
            
            exp_change_litr = 0.0
            total_kg = 0.0
            exp_kg = 0
            kg = 0
            dhj = 0.0
            if line.change_price > 0.0:
                for li in line.shift_id.mile_target_ids:
                    if li.can_id.id !=False:
                        if line.product_id.id==li.product_id.id and li.can_id.id==line.can_id.id:
                            litr +=li.expense_litr
                            kg +=li.expense_kg
                    else:
                        if line.product_id.id==li.product_id.id and li.can_id.id==line.can_id.id:
                            litr +=li.expense_litr
                            kg +=li.expense_kg
                if line.is_change_price == True:
                    if line.change_price < litr:
                        exp_change_litr = line.change_price 
                else:
                    exp_change_litr =  line.change_price
                   
            else:
                for li in line.shift_id.mile_target_ids:
                    if li.can_id.id !=False:
                        if line.product_id.id==li.product_id.id and li.can_id.id==line.can_id.id:
                            litr +=li.expense_litr
                            kg +=li.expense_kg
                    else:
                        if line.product_id.id==li.product_id.id and li.can_id.id==line.can_id.id:
                            litr +=li.expense_litr
                            kg +=li.expense_kg
            
                            
            if line.is_change_price == False and line.change_price >0.0:
                exp_litr = 0.0
                for exp in line.shift_id.loan_document_ids:
                    if exp.can_id.id !=False:
                        if exp.product_id.id==line.product_id.id and exp.can_id.id==line.can_id.id and exp.price==line.sale_price:
                            if exp.expense_type_id.type == 'other':
                                exp_litr +=exp.litr
                                exp_kg +=exp.kg
                    else:
                        if exp.product_id.id==line.product_id.id  and exp.price==line.sale_price and exp.can_id.id==line.can_id.id:
                            if exp.expense_type_id.type == 'other':
                                exp_litr +=exp.litr
                                exp_kg +=exp.kg
                    
                sale_litrs = round(exp_change_litr-exp_litr,2)
                if sale_litrs>0.0:
                    line.sale_litr = sale_litrs
                #else:
                #    line.sale_litr = 0.0
                    
                total_kg = round(kg,2)
                if total_kg > sale_litrs:
                    com_kg = 0.0
                    for s_ids in line.shift_id.scale_ids:
                        if s_ids.product_id.id==line.product_id.id:
                            dhj = round(s_ids.dhj_amount,4)
                            com_kg = line.sale_litr*dhj
                    if com_kg>0.0:
                        line.total_kg = com_kg
                    else:
                        line.total_kg = 0.0
                else:
                    if total_kg>0.0:
                        com_kg = 0.0
                        for s_ids in line.shift_id.scale_ids:
                            if s_ids.product_id.id==line.product_id.id:
                                dhj = round(s_ids.dhj_amount,4)
                                com_kg = line.sale_litr*dhj
                        line.total_kg = com_kg
                    else:
                        line.total_kg = 0.0
            else:
                exp_litr = 0.0
                for exp in line.shift_id.loan_document_ids:
                    if exp.can_id.id !=False:
                        if exp.product_id.id==line.product_id.id and exp.can_id.id==line.can_id.id  and exp.price==line.sale_price:
                            if exp.expense_type_id.type == 'other':
                                exp_litr +=exp.litr
                                exp_kg +=exp.kg
                    else:
                        if exp.product_id.id==line.product_id.id  and exp.price==line.sale_price and exp.can_id.id==line.can_id.id:
                            if exp.expense_type_id.type == 'other':
                                exp_litr +=exp.litr
                                exp_kg +=exp.kg
                sale_litrs = round(litr - exp_litr- exp_change_litr,2)
                #else:
                    #sale_litrs = round(line.change_price,2)
                
                
                if sale_litrs>0.0:
                    line.sale_litr = sale_litrs
                #else:
                #    line.sale_litr = 0.0
                total_kg = round(kg-exp_kg,2)
                if total_kg > sale_litrs:
                    com_kg = 0.0
                    for s_ids in line.shift_id.scale_ids:
                        if s_ids.product_id.id==line.product_id.id and s_ids.can_id.id==line.can_id.id:
                            dhj = round(s_ids.dhj_amount,4)
                            com_kg = line.sale_litr*dhj
                    if com_kg>0.0:
                        line.total_kg = com_kg
                    else:
                        line.total_kg = 0.0
                else:
                    if total_kg>0.0:
                        com_kg = 0.0
                        for s_ids in line.shift_id.scale_ids:
                            if s_ids.product_id.id==line.product_id.id and s_ids.can_id.id==line.can_id.id:
                                dhj = round(s_ids.dhj_amount,4)
                                com_kg = line.sale_litr*dhj
                        line.total_kg = com_kg
                    else:
                        line.total_kg = 0.0
            
            if round(line.sale_litr,2) > 0.0 and line.sale_price >0.0:
                total = line.sale_price*line.sale_litr

            line.total_amount = total
    
            
        
class ShiftWorkingRegisterLine(models.Model):
    _name = 'shift.working.register.line'
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('shift.working.register.line'))
    name = fields.Many2one('mile.target.register',string='Code')
    shts_id = fields.Many2one('shts.register',string=u'ШТС',default=lambda self: self.env.user.warehouse_id.id)
    employee_id = fields.Many2one('hr.employee','Employee',required=True)
    department_id = fields.Many2one('hr.department','Department',required=True)
    job_id  = fields.Many2one('hr.job',u'Албан тушаал',required=True)
    product_id = fields.Many2one('product.product',string='Product')
    price = fields.Float(string='Price')
    total_price = fields.Float(string='Total price',required=True,compute="action_total_price")
    #total_price_new = fields.Float(string='Total price',required=True,related="total_price",store=True)
    time = fields.Float(string=u'Цаг/Өдөр/',required=True)
    n_time = fields.Float(string=u'Цаг/шөнө/',required=True)
    qty = fields.Float(string=u'ТТМ, л',required=True)
    gas = fields.Float(string=u'Хийн түлш, л')
    fuel_sale = fields.Float(string=u'Шатахуун, л')
    acc_qty = fields.Float(string=u'Акк, ш')
    shift_id = fields.Many2one('shift.working.register',string='Shift register')
    shift_service_id = fields.Many2one('shift.working.register',string='Shift register')
    shift_manager_id = fields.Many2one('shift.working.register',string='Shift register')
    
    
    
    def action_total_price(self):
        for line in self:
            total = 0.0
            if line.qty > 0.0 and line.price>0.0:
                total = line.qty *line.price
            line.total_price = total
    
    
    @api.onchange('employee_id')
    def onchange_employee_id(self):
        for line in self:
            line.department_id = line.employee_id.department_id.id
            line.job_id = line.employee_id.job_id.id
    
    @api.onchange('product_id')
    def onchange_product_id(self):
        for line in self:
            price = 0.0
            for sht in line.shift_id.shts_id.line_ids:
                if sht.product_id.id==line.product_id.id:
                    price = sht.price
            self.price = price
            
        
    
    

    
    