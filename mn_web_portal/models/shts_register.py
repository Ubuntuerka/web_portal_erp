# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

class ShiftTime(models.Model):
    _name = 'shift.time'

    name = fields.Char(string='Shift time')
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('shts.time'),required=True)
    

class ShtsRegister(models.Model):
    _name = 'shts.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('shts.register'),required=True)
    company_code = fields.Char(string='Company code')
    region = fields.Selection([('one',u'1-р бүс сэрүүн'),
                               ('two',u'2-р бүс дулаан'),
                               ('three',u'1-р бүс хүйтэн'),
                               ('four',u'2-р бүс хүйтэвтэр')],string=u'Бүс')
    districtCode = fields.Integer(string=u'Дүүргийн код')
    shts_code = fields.Char(string='ШТС код /голомт/')
    active = fields.Boolean(string=u'Төлөв')
    pos_number = fields.Char(string=u'Посын дугаар')
    user_id = fields.One2many('res.users','warehouse_id',string=u'User')
    name = fields.Char(string = 'Shts name',required=True)
    code = fields.Char(string = 'Shts code',required=True)
    working_hour = fields.Float(string='Working hour')
    work_time = fields.Integer(string=u'Ээлж',required=True,default=1)
    shift_diff_amount = fields.Float(string=u'Ээлжийн зөрүү',required=True)
    is_stock = fields.Boolean(string=u'Агуулах эсэх')
    is_oil_center = fields.Boolean(string=u'Авто сервис бол сонго')
    is_shts_gas = fields.Boolean(string=u'Хий зардаг эсэх')
    oil_code = fields.Char(string=u'Тосны төвийн API пос код')
    shts_pos_code = fields.Char(string='SHTS Pos API code')
    is_talon_sale = fields.Boolean(string='Talon sale')
    region_id = fields.Many2one('shts.region',string='Region')
    distance_id = fields.Many2one('distance.register',string='Distance')
    line_ids = fields.One2many('shts.register.line','shts_id',string='Shts line',domain="[('prod_type', '=', 'gasol')]")
    pro_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн')
    oil_line_ids = fields.One2many('shts.register.line','oil_shts_id',string='Shts oil line',domain="[('prod_type', '=', 'oil')]")
    oil_center_sale_ids = fields.One2many('shts.register.line','oil_center_shts_id',string='TUT products',domain="[('prod_type', '=', 'oil')]")
    service_line_ids = fields.One2many('shts.register.line','service_shts_id',string='Shts service line',domain="[('prod_type', '=', 'service')]")
    employee_ids = fields.Many2many('hr.employee','shts_register_employee_rel','shts_id','employee_id',string='Employee')
    shift_time = fields.Many2one('shift.time',string='Shift time')
    price_qroup_id = fields.Many2one('price.group',string=u'Үнийн грүпп')
    job_id = fields.Many2one(string=u'Албан тушаал')
    sales_rank = fields.Selection([('1',u'1-р зэрэглэл'),
                               ('2',u'2-р зэрэглэл'),
                               ('3',u'3-р зэрэглэл')],string=u'Борлуулалтын зэрэглэл')
    
    lat = fields.Char(string=u'Өргөрөг')
    long = fields.Char(string=u'Уртраг')
    address = fields.Char(string=u'Хаяг')
    phone = fields.Char(string=u'Утас')
    schedule_plan_id = fields.Many2one('schedule.plan', string='Schedule plan')
    oil_plan_ids = fields.One2many('oil.service.schedule', 'shts_id', string='Oil plan')
    pos_type = fields.Selection([('tdb',u'TDB'),
                               ('golomt',u'Голомт')],string=u'Банкны Посын төрөл')
    view_app = fields.Boolean(string=u'АПП дээр харуулах', default = False)
    
    
    _sql_constraints = [
        ('code_company_uniq', 'unique (code, name, company_id)', u'Нэр болон код давхцаж болохгүй тул та бүртгэлээ шалгана уу !'),
    ]
    
    
    
    def action_create_plan(self):
        self.ensure_one()
        ctx = dict(
                default_model='shts.register',
                default_res_id=self.id,
                )
        compose_form = self.env.ref('mn_web_portal.oil_service_schedule_create_view', raise_if_not_found=False)
        return {
            'name': _('Action create plan'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'oil.service.schedule.create',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': ctx,
        }
    
    def write(self, vals):
        employee_obj = self.env['hr.employee']
        #if 'employee_ids' in vals:
        '''for em in self.employee_ids:
                for line in vals['employee_ids'][0][2]:
                    emp = employee_obj.browse(line)
                    if emp.id != em.id:
                        print('aaaasddddd',em.write({'shts_id': [(3,self.id)],
                                   })  )'''
                
        rslt = super(ShtsRegister, self).write(vals)
        
        '''if 'employee_ids' in vals:
            for line in vals['employee_ids'][0][2]:
                emp = employee_obj.browse(line)
                emp.write({'shts_id': [(4,self.id)],
                           })'''
        if 'service_line_ids' in vals:
            for line in vals['service_line_ids']:
                if line[2]!=False:
                    line_ids = self.env['shts.register.line'].search([('product_id','=',line[2]['product_id']),('service_shts_id','=',self.id)])
                    if line_ids.product_id.id !=False:
                        for li in line_ids:
                            li.write({'shts_code':li.shts_id.shts_code})
                        if len(str(line_ids.id))==2:
                            raise UserError(u"ШТС дээр %s бүтээгдэхүүнийг давхардуулж бүртгэхгүйг анхаарна уу!"%line_ids.product_id.name)
        if 'oil_center_sale_ids' in vals:
            for line in vals['oil_center_sale_ids']:
                if line[2]!=False:
                    line_ids = self.env['shts.register.line'].search([('product_id','=',line[2]['product_id']),('oil_center_shts_id','=',self.id)])
                    if line_ids.product_id.id !=False:
                        for li in line_ids:
                            li.write({'shts_code':li.oil_shts_id.shts_code})
                        if len(str(line_ids.id))==2:
                            raise UserError(u"ШТС дээр %s бүтээгдэхүүнийг давхардуулж бүртгэхгүйг анхаарна уу!"%line_ids.product_id.name)

        return rslt
    
    

    
class ShtsRegisterLine(models.Model):
    _name = 'shts.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env['res.company']._company_default_get('shts.register.line'))
    product_id = fields.Many2one('product.product',string='Product',required=True)
    product_code = fields.Char(string='Product code')
    product_name = fields.Char(string='Product name')
    barcode = fields.Char(string='Barcode')
    vattype = fields.Boolean(string='Vat type')
    uom = fields.Char(string='Uom')
    price = fields.Float(string='Price',required=True)
    shts_code = fields.Char(string='Штс код')
    type = fields.Selection([('gasol',u'Шатахуун'),
                             ('oil',u'Тос'),
                             ('service',u'Үйлчилгээ'),
                             ('akk',u'Аккумулятор'),
                             ('skytel',u'Скайтел нэгж')],string='Type',required=True)
    shts_id = fields.Many2one('shts.register',string='Shts')
    oil_shts_id = fields.Many2one('shts.register',string='Shts')
    oil_center_shts_id = fields.Many2one('shts.register',string='Shts')
    service_shts_id = fields.Many2one('shts.register',string='Shts')
    
  
    
    _sql_constraints = [
        ('product_id_shts_uniq', 'unique(oil_shts_id, product_id)', u'ШТС дээр бүтээгдэхүүн давхардуулж бүртгэхгүйг анхаарна уу!'),
        ('product_id_oil_shts_uniq', 'unique(oil_center_shts_id, product_id)', u'ШТС дээр бүтээгдэхүүн давхардуулж бүртгэхгүйг анхаарна уу!'),
        ('product_id_service_shts_uniq', 'unique(service_shts_id, product_id)', u'ШТС дээр бүтээгдэхүүн давхардуулж бүртгэхгүйг анхаарна уу!'),
        ('product_shts_uniq', 'unique(shts_id, product_id)', u'ШТС дээр бүтээгдэхүүн давхардуулж бүртгэхгүйг анхаарна уу!')
    ]
    
    @api.onchange('shts_id')
    def _onchange_shts_id(self):
        for line in self:
            line.shts_code = line.shts_id.pos_number
    
    @api.onchange('product_id')
    def _onchange_product_id(self):
        for line in self:
            line.product_code = line.product_id.default_code
            line.product_name = line.product_id.name
            line.barcode = line.product_id.ebarimt_code
            line.vattype = line.product_id.vat_taxtype
            line.uom = line.product_id.uom_id.local_code
            if line.shts_id:
                line.shts_code = line.shts_id.pos_number
            if line.oil_shts_id:
                line.shts_code = line.oil_shts_id.pos_number
            if line.service_shts_id:
                line.shts_code = line.service_shts_id.pos_number
            
            
            
    
    #===========================================================================
    # @api.model
    # def create(self,vals):
    #     if vals['product_id']:
    #         line_ids = self.search([('product_id','=',vals['product_id']),('oil_shts_id','=',vals['oil_shts_id'])])
    #         count = 1
    #         for li in line_ids:
    #             count +=1
    #             if count == 2:
    #                 raise UserError(u"ШТС дээр %s бүтээгдэхүүнийг давхардуулж бүртгэхгүйг анхаарна уу!"%li.product_id.name)
    #     rslt = super(ShtsRegisterLine, self).write(vals)
    #     return rslt
    #===========================================================================
    
    
    def write(self, vals):
         rslt = super(ShtsRegisterLine, self).write(vals)
         if 'product_id' in vals:
            line_ids = self.search([('product_id','=',vals['product_id']),('oil_shts_id','=',self.id)])
            count = 0
            for li in line_ids:
                count +=1
                if count == 2:
                    raise UserError(u"ШТС дээр %s бүтээгдэхүүнийг давхардуулж бүртгэхгүйг анхаарна уу!"%li.product_id.name)
            else:
                if line_ids.product_id.id !=False:
                    if len(str(line_ids.id))==2:
                        raise UserError(u"ШТС дээр %s бүтээгдэхүүнийг давхардуулж бүртгэхгүйг анхаарна уу!"%line_ids.product_id.name)
             
         return rslt
    
class OilCenterStop(models.Model):
    _name = 'oil.center.stop'
    _description = 'Oil center stop'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company', string='Company', required=True)
    name = fields.Char(string='Oil center stop', required=True)
    

class SchedulePlan(models.Model):
    _name = 'schedule.plan'
    _description = 'Schedule plan'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('schedule.plan'))
    name = fields.Char(string='Schedule plan', required=True)
    duration = fields.Float(string='Duration')
    line_ids = fields.One2many('schedule.plan.line', 'plan_id', string='Schedule plan line')


class SchedulePlanLine(models.Model):
    _name = 'schedule.plan.line'
    _description = 'Schedule plan'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('schedule.plan.line'))
    name = fields.Char(string='Duration', required=True)
    start_time = fields.Float(string='Start time',required=True)
    end_time = fields.Float(string='End time',required=True)
    days = fields.Selection([('1','Monday'),
                             ('2','Tuesday'),
                             ('3','Wednesday'),
                             ('4','Thursday'),
                             ('5','Friday'),
                             ('6','Saturday'),
                             ('7','Sunday')],string='Days',required=True)
    plan_id = fields.Many2one('schedule.plan',string='Schedule plan')
    


class OilServiceSchedule(models.Model):
    _name = 'oil.service.schedule'
    _description = 'Oil service schedule'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'plan_date asc'
    
    company_id = fields.Many2one('res.company',string='Company', required=True)
    name = fields.Char(string='Schedule name')
    shts_code = fields.Char(string='Салбарын код')
    stop_id = fields.Many2one('oil.center.stop', string='Stop')
    shts_id = fields.Many2one('shts.register',string='Shts')
    plan_date = fields.Date(string='Plan date')
    duration = fields.Char(string='Duration')
    start_time = fields.Float(string='Start time')
    end_time = fields.Float(string='End time')
    state = fields.Selection([('free','Free'),
                              ('reserved','Reserved')],string='State')
    
    
    
    
    
    
    
