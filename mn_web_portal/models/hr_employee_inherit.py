# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date



class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    
    main_shts_id = fields.Many2one('shts.register',string=u'Үндсэн ШТС')
    shts_id = fields.Many2many('shts.register','hr_employee_rel','emp_id','shts_id',string='Shts')
    last_name = fields.Char(string='Last name')
    code = fields.Char(string='Employee code', readonly=True)
    work_type = fields.Selection([('work',u'ШТС'),
                                  ('drive',u'Жолооч'),
                                  ('shza',u'ШЗА'),
                                  ('office',u'Төв оффис'),
                                  ('other',u'ГТБА')
                                  ],string=u'Төрөл',required=True,default='work')

class HrEmployeePublic(models.Model):
    _inherit = 'hr.employee.public'
    
    main_shts_id = fields.Many2one('shts.register',string=u'Үндсэн ШТС')
    shts_id = fields.Many2many('shts.register','hr_employee_rel','emp_id','shts_id',string='Shts')
    last_name = fields.Char(string='Last name')
    code = fields.Char(string='Employee code', readonly=True)
    work_type = fields.Selection([('work',u'ШТС'),
                                  ('drive',u'Жолооч'),
                                  ('shza',u'ШЗА'),
                                  ('office',u'Төв оффис'),
                                  ('other',u'ГТБА')
                                  ],string=u'Төрөл',required=True,default='work')