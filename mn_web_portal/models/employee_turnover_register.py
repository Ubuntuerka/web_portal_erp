# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date, datetime, timedelta, time
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class EmployeeTurnoverRegister(models.Model):
    _name = 'employee.turnover.register'
    _order = 'name desc'

    state = fields.Selection([('draft', u'Илгээх'),
                              ('approve', u'Батлагдсан'),
                              ('cancel', u'Буцаасан')], string=u'Төлөв', default='draft', tracking=True)
    name = fields.Char(string='Хүсэлтийн дугаар', readonly=True,default=lambda self: self.env['ir.sequence'].next_by_code('employee.turnover.register'))
    hr_employee_id = fields.Many2one('hr.employee', 'Ажилтны нэр', track_visibility='onchange',required=True)
    department_id = fields.Many2one('hr.department', 'Хэлтэс')
    job_id = fields.Many2one('hr.job', 'Албан тушаал')
    
    to_shts_id = fields.Many2one('shts.register', string='Шилжих ШТС')
    to_department_id = fields.Many2one('hr.department', 'Шилжих Хэлтэс')
    to_job_id = fields.Many2one('hr.job', 'Шилжих албан тушаал')
    to_date = fields.Date('Шилжсэн огноо', default=fields.Date.context_today)
    shift_reason = fields.Text('Shift reason', track_visibility='onchange')
    rec_user_id = fields.Many2one('res.users', 'Бүртгэсэн ажилтан', default=lambda self: self.env.user, required=True, readonly=True)
    explanation = fields.Char(string=u'Тайлбар')
    
    @api.onchange('hr_employee_id')
    def onchange_movement(self):
        res = {}
        if self.hr_employee_id:
            res['department_id'] = self.hr_employee_id.department_id.id
            res['job_id'] = self.hr_employee_id.job_id.id
            return {'value': res}

    def action_to_draft(self):
        self.write({'state': 'draft'})
        return True

    def action_return(self):
        self.write({'state': 'return'})
        return True
    
    def action_to_approve(self):
        self.write({'state': 'approve'})

        if self.to_shts_id:

            self.hr_employee_id.write({
                'shts_id':  [(4,self.to_shts_id.id)]
                           })
            self.to_shts_id.write({
                'employee_ids':  [(4,self.hr_employee_id.id)]
                           })





