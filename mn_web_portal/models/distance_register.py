# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class DistanceRegister(models.Model):
    _name = 'distance.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('distance.register'),required=True)
    warehouse_id = fields.Many2one('shts.register',string='Warehouse',required=True,domain=[('is_stock','=',True)])
    shts_id = fields.Many2one('shts.register',string='Shts',required=True)
    name = fields.Float(string='Distance',required=True)