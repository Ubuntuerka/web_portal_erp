# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
import re

class CarRegister(models.Model):
    _name = 'car.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env['res.company']._company_default_get('car.register'),required=True)
    name = fields.Char(string='Car number',required=True,size=7)
    location = fields.Char(string='Location',required=True)
    size = fields.Float(string='Size',required=True)
    is_tracker = fields.Boolean(string=u'Чиргүүл эсэх')
    parent_id = fields.Many2one('car.register',string=u'Машин')
    year = fields.Integer(string=u'Он')
    phone = fields.Integer(string=u'Утасны дугаар')
    code = fields.Char(string=u'Код')
    car_mark = fields.Char(string=u'Машины марк')
    tracker = fields.Char(string=u'Чиргүүл')
    aral_number = fields.Char(string=u'Аралын дугаар')
    drive_id = fields.Many2one('hr.employee',string=u'Жолооч')
    state_id = fields.Selection([('001',u'Техник ажилд гарахад бэлэн'),
                                 ('002',u'Хот дотор тээвэр хийсэн'),
                                 ('003',u'Засвартай'),
                                 ('004',u'Ажилд гарсан'),
                                 ('005',u'Дугаарын хязгаарлалт'),
                                 ('006',u'Жолоочоос шалтгаалсан сул зогсолт'),
                                 ('007',u'Ээлжийн амралттай')],string=u'Төлөв')
    type = fields.Selection([('main',u'Үндсэн'),
                             ('contract',u'Гэрээт'),
                             ('rent',u'Түрээсийн')],string='Type',default='main',required=True)
    bucket_ids = fields.One2many('bucket.register','car_number_id',string=u'Торх',required=True)
    
    _sql_constraints = [
        ('name_uniq', 'unique(name)', u'Машины дугаар давхцахгүй !'),
    ]
    @api.model
    def create(self, vals):
        if vals['name']:
            if len(str(vals['name']))==7 and vals['is_tracker']==False:
                number = vals['name'][:4]
                useg = vals['name'][4:]
                numbers = ["1","2","3","4","5","6","7","8","9","0"]
                try:
                    if type(int(number))!=int:
                        raise UserError(u"Эхний 4 орон тоо биш байгаа тул шалгана уу.")
                    elif useg[0] in numbers and useg[1] in numbers and useg[2] in numbers:
                        raise UserError(u"Сүүлийн 3 орон үсэг биш байгаа тул шалгана уу.")
                    else:
                        vals['name']=vals['name'][:4]+(vals['name'][4:]).upper()
                except ValueError:
                    raise UserError(u"Таны оруулсан дугаар зөв байгаа эсэхийг шалгана уу.")
            elif len(str(vals['name']))==6 and vals['is_tracker']==True:
                number = vals['name'][:4]
                useg = vals['name'][4:]
                numbers = ["1","2","3","4","5","6","7","8","9","0"]
                try:
                    if type(int(number))!=int:
                        raise UserError(u"Эхний 4 орон тоо биш байгаа тул шалгана уу.")
                    elif useg[0] in numbers and useg[1] in numbers:
                        raise UserError(u"Сүүлийн 3 орон үсэг биш байгаа тул шалгана уу.")
                    else:
                        vals['name']=vals['name'][:4]+(vals['name'][4:]).upper()
                except ValueError:
                    raise UserError(u"Таны оруулсан дугаар зөв байгаа эсэхийг шалгана уу.")
            else:
                raise UserError(u"7 тэмдэгтээс их болон бага байж болохгүй.")
            
        res = super(CarRegister, self.with_context()).create(vals)
        return res
    
    def write(self, vals):
        for line in self:
            if 'name' in vals:
                if len(str(vals['name']))==7 and line.is_tracker==False:
                    number = vals['name'][:4]
                    useg = vals['name'][4:]
                    numbers = ["1","2","3","4","5","6","7","8","9","0"]
                    try:
                        if type(int(number))!=int:
                            raise UserError(u"Эхний 4 орон тоо биш байгаа тул шалгана уу.")
                        elif useg[0] in numbers and useg[1] in numbers and useg[2] in numbers:
                            raise UserError(u"Сүүлийн 3 орон үсэг биш байгаа тул шалгана уу.")
                        else:
                            vals['name']=vals['name'][:4]+(vals['name'][4:]).upper()
                    except ValueError:
                        raise UserError(u"Таны оруулсан дугаар зөв байгаа эсэхийг шалгана уу.")
                elif len(str(vals['name']))==6 and line.is_tracker==True:
                    number = vals['name'][:4]
                    useg = vals['name'][4:]
                    numbers = ["1","2","3","4","5","6","7","8","9","0"]
                    try:
                        if type(int(number))!=int:
                            raise UserError(u"Эхний 4 орон тоо биш байгаа тул шалгана уу.")
                        elif useg[0] in numbers and useg[1] in numbers:
                            raise UserError(u"Сүүлийн 3 орон үсэг биш байгаа тул шалгана уу.")
                        else:
                            vals['name']=vals['name'][:4]+(vals['name'][4:]).upper()
                    except ValueError:
                        raise UserError(u"Таны оруулсан дугаар зөв байгаа эсэхийг шалгана уу.")
                else:
                    raise UserError(u"7 тэмдэгтээс их болон бага байж болохгүй.")
                
            res = super(CarRegister, line.with_context()).write(vals)
        return res       
                    
                    
                    
                    
        
    