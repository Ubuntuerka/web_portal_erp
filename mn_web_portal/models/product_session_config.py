# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
import time
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from dateutil import relativedelta

class ProductSessionConfig(models.Model):
    _name = 'product.session.config'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    name = fields.Char(string='Config name',required=True,states={'draft': [('readonly', False)]},readonly=True)
    company_id = fields.Many2one('res.company',string='Company',required=True,default=lambda self: self.env['res.company']._company_default_get('product.session.config'),states={'draft': [('readonly', False)]},readonly=True)
    reg_user_id = fields.Many2one('res.users',string='Register user',required=True,default=lambda self: self.env.user,states={'draft': [('readonly', False)]},readonly=True)
    start_date = fields.Date(string='Start date',required=True, default=fields.Date.context_today,states={'draft': [('readonly', False)]},readonly=True)
    end_date = fields.Date(string='End date',required=True, default=fields.Date.context_today,states={'draft': [('readonly', False)]},readonly=True)
    product_id = fields.Many2one('product.product',domain="[('prod_type','=','gasol')]",string='Product',required=True,states={'draft': [('readonly', False)]},readonly=True)
    can_id = fields.Many2one('can.register',domain="[('shts_id','=',shts_id)]",string='Can',required=True,states={'draft': [('readonly', False)]},readonly=True)
    shts_id = fields.Many2one('shts.register',string='Shts',required=True,states={'draft': [('readonly', False)]},readonly=True)
    type = fields.Selection([('winter',u'Өвөл'),
                             ('summer',u'Зун')],string='Type',states={'draft': [('readonly', False)]},readonly=True)
    gasol_type = fields.Selection([('2',u'ДТ Өвөл'),
                                   ('3',u'ДТ Зун'),
                                   ('1',u'Авто бинзен')
                                   ],string=u'Төрөл',required=True)
    state = fields.Selection([('draft',u'Draft'),
                              ('confirm',u'Confirm')],string='State',required=True,default='draft')
    