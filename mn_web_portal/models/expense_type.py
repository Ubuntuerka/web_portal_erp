# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class ExpenseType(models.Model):
    _name = 'expense.type'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company')
    name = fields.Char(string='Name')
    code = fields.Char(string='Code')
    type = fields.Selection([('loan',u'Зээлээр'),
                             ('sale',u'Борлуулалт'),
                             ('other',u'Бусад'),
                             ('cash',u'Бэлэн')],string='Type',default='loan',required=True)
    