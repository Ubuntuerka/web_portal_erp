# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from datetime import datetime
from calendar import monthrange
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

class NormalLossCompute(models.Model):
    _name = 'normal.loss.compute'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',required=True,default=lambda self: self.env['res.company']._company_default_get('normal.loss.compute'))
    start_date = fields.Date(string='Start date',required=True,default=datetime.today().replace(day=1))
    end_date = fields.Date(string='End date',required=True)
    name = fields.Char(string=u'Хэвийн хорогдлын нэр',required=True)
    reg_date = fields.Date(string='Register date',required=True,default=fields.Date.context_today)
    reg_user_id = fields.Many2one('res.users',string='Register users',required=True,default=lambda self: self.env.user)
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,domain="[('is_stock','=',False)]")
    state = fields.Selection([('draft','Draft'),
                              ('done','Done')],string='State',default='draft')
    line_ids = fields.One2many('normal.loss.compute.line','normal_id',string='Line',ondelete="cascade")
    
    
    #_sql_constraints = [
    #    ('shts_id_start_date_company_uniq', 'unique (shts_id, start_date, company_id)', u'Хэвийн хорогдлийн тооцоолол давхцаж бүртгэж болохгүй тул та бүртгэлээ шалгана уу !'),
    #]
    
    @api.model
    def create(self, vals):
        if 'company_id' in vals and 'start_date' in vals and 'shts_id' in vals:
            new_datas = self.search([('company_id','=',vals['company_id']),('start_date','=',vals['start_date']),('shts_id','=',vals['shts_id'])])
            if len(new_datas.ids)>0:
                raise UserError(u'Хэвийн хорогдлийн тооцоолол давхцаж бүртгэж болохгүй тул та бүртгэлээ шалгана уу !')
       
            if self.env.user.shts_ids:
                if vals['shts_id'] not in self.env.user.shts_ids.ids:
                        raise UserError(u'Та өөр штс сонгож үүсгэж байгаа тул зөв штс сонгоно уу!')
        normal_loss = super(NormalLossCompute, self.with_context()).create(vals)
        return normal_loss
    
    
    def write(self, vals):
        normal_loss = super(NormalLossCompute,self).write(vals)
        if 'shts_id' in vals:
            if self.env.user.shts_ids:
                if vals['shts_id'] not in self.env.user.shts_ids.ids:
                        raise UserError(u'Та өөр штс сонгож үүсгэж байгаа тул зөв штс сонгоно уу!')
        
        return normal_loss
    
    
    
    
    def action_confirm(self):
        for line in self:
            line.write({'state':'done'})
    
    def action_compute(self):
        line_obj = self.env['normal.loss.compute.line']
        income_obj = self.env['shts.income']
        shift_obj = self.env['shift.working.register']
        thh_obj = self.env['thh.data']
        product_obj = self.env['product.product']
        product_ids = product_obj.search([('prod_type','=','gasol')])
        shts_obj = self.env['shts.register']
        shts_ids = shts_obj.search([('active','=',True),('is_stock','=',False)])
        product_session_config_obj = self.env['product.session.config']
        can_obj = self.env['can.register']
        for line in self:
            start_month_date = datetime.strptime(str(line.start_date), "%Y-%m-%d")
            end_month_date = datetime.strptime(str(line.end_date), "%Y-%m-%d")
            start_month = start_month_date.month
            end_month = end_month_date.month
            start_year = start_month_date.year
            m_days=monthrange(start_year, start_month)
            for shts in self.shts_id.line_ids:
                income = 0.0
                teever_per = 0.0
                rec_per = 0.0
                sale_exp = 0.0
                send_per = 0.0
                save_norm_day = 0.0
                save_avg_balance = 0.0
                save_per = 0.0
                total_loss_kg = 0.0
                winter = [1,2,3,10,11,12]
                summer = [4,5,6,7,8,9]
                thh = 0.0
                can_id = False
                self.env.cr.execute(""" select crl.product_id, cr.id  from 
                                             can_register as cr
                                             left join can_register_line as crl on crl.can_id = cr.id
                                             where cr.shts_id = %s and crl.product_id = %s 
                                             and crl.active = true
                                             and cr.active=true group by crl.product_id, cr.id 
                                            """%(shts.shts_id.id,shts.product_id.id))
                can_ids = self._cr.dictfetchall()
                if can_ids:
                    for crl in can_ids:
                        sale_exp = 0.0
                        income = 0.0
                        thh = 0.0
                        save_kg = 0.0
                        save_count = 0.0
                        can_id = crl['id']
                        config_ids = product_session_config_obj.search([('shts_id','=',shts.shts_id.id),('start_date','<=',line.start_date),('end_date','>=',line.end_date),('product_id','=',shts.product_id.id)],limit=1)
                        if not config_ids:
                            raise UserError(_(u"Улирлын тохиргоог хийнэ үү!"))
                        if start_month in winter:
                            if config_ids:
                                thh_ids = thh_obj.search([('gasol_type','=',config_ids.gasol_type),('start_period','=',10),('end_period','=',3),('shts_id','=',shts.shts_id.id)])
                            else:
                                thh_ids = thh_obj.search([('start_period','=',10),('end_period','=',3),('shts_id','=',shts.shts_id.id),('gasol_type','=',shts.product_id.gasol_type)])
        
                        else:
                            if config_ids:
                                
                                thh_ids = thh_obj.search([('gasol_type','=',config_ids.gasol_type),('start_period','=',4),('end_period','=',9),('shts_id','=',shts.shts_id.id)])
                            else:
                                thh_ids = thh_obj.search([('start_period','=',4),('end_period','=',9),('shts_id','=',shts.shts_id.id),('gasol_type','=',shts.product_id.gasol_type)])
        
                        income_ids = income_obj.search([('state','!=','cancel'),('shts_id','=',shts.shts_id.id),('shift_date','>=',line.start_date),('shift_date','<=',line.end_date)])
                        line_ids = line_obj.search([('can_id','=',can_id),('product_id','=',shts.product_id.id),('shts_id','=',shts.shts_id.id),('normal_id','=',line.id)])
                        shift_ids = shift_obj.search([('state','!=','cancel'),('shift_date','>=',line.start_date),('shift_date','<=',line.end_date),('shts_id','=',self.shts_id.id)])
                        for shift in shift_ids:
                            for shift_line in shift.scale_ids:
                                if shts.product_id.id == shift_line.product_id.id and can_id==shift_line.can_id.id:
                                    
                                    save_kg +=round(shift_line.kg,2)
                                    save_count +=1
                                    sale_exp +=round(shift_line.total_kg,2)
                                   
                                    
                        if income_ids:
                            for inc in income_ids:
                                for in_line in inc.line_ids:
                                    if in_line.product_id.id == shts.product_id.id and can_id == in_line.can_id.id:
                                        income +=in_line.hariu_kg
                                        thh +=in_line.thh
                                        
                                        
                        if not line_ids:
                            teever_per = thh
                            rec_per = (income*thh_ids.rec_value)/1000.0
                            send_per = (sale_exp*thh_ids.deal_value)/1000.0
                            if save_kg >0.0:
                                save_avg_balance = save_kg/save_count
                            save_norm_day = (thh_ids.save_value/30)*m_days[1]
                            save_per = (save_norm_day*save_avg_balance)/1000.0
                            total_loss_kg = teever_per+rec_per+send_per+save_per
        
                            line_obj.create({
                                            'can_id':can_id,
                                            'product_id':shts.product_id.id,
                                            'shts_id':shts.shts_id.id,
                                            'normal_id':line.id,
                                            'session':thh_ids.type,
                                            'income':income,
                                            'teever_norm':thh_ids.transport_value,
                                            'teever_per':teever_per,
                                            'rec_norm':thh_ids.rec_value,
                                            'rec_per':rec_per,
                                            'expense_kg':sale_exp,
                                            'send_norm':thh_ids.deal_value,
                                            'send_per':send_per,
                                            'save_norm':thh_ids.save_value,
                                            'save_norm_day':save_norm_day,
                                            'save_avg_balance':save_avg_balance,
                                            'save_per':save_per,
                                            'total_loss_kg':total_loss_kg,
                                            })
                        else:
                            teever_per = thh
                            rec_per = (income*thh_ids.rec_value)/1000.0
                            send_per = (sale_exp*thh_ids.deal_value)/1000.0
                            if save_kg >0.0:
                                save_avg_balance = save_kg/save_count
                            save_norm_day = (thh_ids.save_value/30)*m_days[1]
                            save_per = (save_norm_day*save_avg_balance)/1000.0
                            total_loss_kg = teever_per+rec_per+send_per+save_per
                            for datas in line_ids:
                                datas.write({
                                                'can_id':can_id,
                                                'product_id':shts.product_id.id,
                                                'shts_id':shts.shts_id.id,
                                                'normal_id':line.id,
                                                'session':thh_ids.type,
                                                'income':income,
                                                'teever_norm':thh_ids.transport_value,
                                                'teever_per':teever_per,
                                                'rec_norm':thh_ids.rec_value,
                                                'rec_per':rec_per,
                                                'expense_kg':sale_exp,
                                                'send_norm':thh_ids.deal_value,
                                                'send_per':send_per,
                                                'save_norm':thh_ids.save_value,
                                                'save_norm_day':save_norm_day,
                                                'save_avg_balance':save_avg_balance,
                                                'save_per':save_per,
                                                'total_loss_kg':total_loss_kg,
                                                })
                                

    #def unlink(self):
        #for line in self:
            #if line.state in ('done'):
                #raise UserError(_("Батлагдсан тул устгах боломжгүй"))
            #line.shift_line_ids.unlink()
        #return super(NormalLossCompute, self).unlink()       
            
            
class NormalLossComputeLine(models.Model):
    _name = 'normal.loss.compute.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company')
    can_id = fields.Many2one('can.register',string='Can')
    shts_id = fields.Many2one('shts.register',string='Shts')
    normal_id = fields.Many2one('normal.loss.compute',string='Normal')
    product_id = fields.Many2one('product.product',string='Product')
    session = fields.Selection([('winter','Winter'),
                                ('summer','Summer')],string='Session')
    income = fields.Float(string='Income')
    teever_norm = fields.Float(string='Teever norm',digits=(10, 4))
    teever_per = fields.Float(string='Teever per')
    rec_norm = fields.Float(string='Receive norm',digits=(10, 4))
    rec_per = fields.Float(string='Receive per')
    expense_kg = fields.Float(string='Expense kg')
    send_norm = fields.Float(string='Send norm',digits=(10, 4))
    send_per = fields.Float(string='Send per')
    save_norm = fields.Float(string='Save norm',digits=(10, 4))
    save_norm_day = fields.Float(string='Save 30 days norm',digits=(10, 4))
    save_avg_balance = fields.Float(string='Save average balance')
    save_per = fields.Float(string='Save per')
    total_loss_kg = fields.Float(string='Total loss kg')
    
    
    