# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from _ast import In
DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

class PriceRegisterShts(models.Model):
    _name = 'price.register.shts'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'shift_date desc'
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('price.register'))
    name = fields.Char(string='Change price number',default=lambda self: self.env['ir.sequence'].next_by_code('price.register'))
    product_id = fields.Many2many('product.product',string='Product',domain="['|',('categ_id','=',category_id),('prod_type','=',type)]")#Бүтээгдэхүүн
    main_price_id = fields.Many2one('price.change.register',string='Main price')
    shts_id = fields.Many2one('shts.register','Shts')
    region_id = fields.Many2one('shts.region','Region')
    price_group_id = fields.Many2one('price.group',string=u'Үнийн грүпп')
    shift_date = fields.Date(string='Shift date',default=fields.Date.context_today)
    date = fields.Date(string='Date', default=fields.Date.context_today)
    start_date = fields.Datetime(string='Start date',default=fields.Date.context_today,required=True)
    confirm_date = fields.Datetime(string='Confirm date',readonly=True)
    reg_user_id = fields.Many2one('res.users',string='Register user',default=lambda self: self.env.user)
    is_type = fields.Boolean(string='Is type')#Төрөл сонгох эсэх
    type = fields.Selection([('gasol','Gasol'),#Шатахуун
                             ('oil','Oil'),#Тос
                             ('service','Service')],string='Type',default='gasol')#Үйлчилгээ
    line_ids = fields.One2many('price.register.shts.line','price_change_id',strin='Price change line')
    oil_line_ids = fields.One2many('price.register.shts.line','oil_change_id',string='Price oil change')
    line_gasol_ids = fields.One2many('price.gasol.register.shts','price_change_id',string='Price gasol register')
    is_download = fields.Boolean(string='Is download')
    state = fields.Selection([('draft','Draft'),#Ноорог
                              ('send','Send'),#Илгээгдсэн
                              ('confirm','Confirm'),#Батлагдсан
                              ('done','Done'),#Дууссан
                              ('return','Return'),
                              ('cancel','Cancel')],string='State',default='draft')#Буцаагдсан #Төлөв
    
    
    
    def action_return(self):
        self.write({'state':'return'})
        return True
    
    def action_done(self):
        for obj in self:
            if obj.type=='gasol':
                for lines in obj.line_ids:
                    for shts_line in obj.shts_id.line_ids:
                        if shts_line.product_id.id == lines.product_id.id:
                            shts_line.write({'price':lines.new_price})
                            
            elif obj.type =='oil':
                for olines in obj.oil_line_ids:
                    for shts_lines in obj.shts_id.oil_line_ids:
                        if shts_lines.product_id.id == olines.product_id.id:
                            shts_lines.write({'price':olines.new_price})
            
            elif obj.type =='service':
                for slines in obj.oil_line_ids:
                    for shts_lines in obj.shts_id.service_line_ids:
                        if shts_lines.product_id.id == slines.product_id.id:
                            shts_lines.write({'price':slines.new_price})
            
            
            obj.write({'state':'done'})
    
    def action_send(self):
        price_change_line_obj = self.env['price.gasol.register']
        shts_line_obj = self.env['shts.register.line']
        shift_obj = self.env['shift.working.register']
        product_obj = self.env['shift.product.line']
        for shl in self.line_gasol_ids:
            if shl.mile_target_amount == 0.0:
                raise UserError(u"Миллийн заалтын утгыг оруулна уу.")
        if self.line_gasol_ids:
            for shts in self.line_ids:
                for line in self.line_gasol_ids:
                    if shts.product_id.id == line.product_id.id:
                        price_change_line_obj.create({
                                                    'shts_id':line.shts_id.id,
                                                    'can_id':line.can_id.id,
                                                    'product_id':line.product_id.id,
                                                    'mile_target_amount':line.mile_target_amount,
                                                    'price_change_id':self.main_price_id.id,
                                                    'first_mile':line.first_mile,
                                                    'diff_mile':line.diff_mile,
                                                    'break_id':line.break_id.id,
                                                    })
     
    
                if not line.shts_id:
                    raise UserError(u"Тухайн өдрийн ээлж үүсээгүй байгаа тул ээлж үүссэн эсэхээ шалгана уу.")
                
                s_date = datetime.strptime(str(line.date), DATETIME_FORMAT)
                s_date_f = s_date.replace(hour=0, minute=0, second=1)
                s_date_l = s_date.replace(hour=23, minute=59, second=59)
                shift = shift_obj.search([('state','=','draft'),('shts_id','=',self.shts_id.id),('shift_date','>=',s_date_f),('shift_date','<=',s_date_l),('shift','=',line.shift)])
                shift_count_ids = shift_obj.search([('state','=','count'),('shift','=',line.shift),('shts_id','=',self.shts_id.id),('shift_date','>=',s_date_f),('shift_date','<=',s_date_l)])
                if shift_count_ids:
                    raise UserError(u"Тухайн өдрийн ээлж хаагдсан байгаа тул үнэ солигдохгүйг анхаарна уу.")
                shift_cofirm_ids = shift_obj.search([('state','=','confirm'),('shts_id','=',self.shts_id.id),('shift_date','>=',s_date_f),('shift_date','<=',s_date_l),('shift','=',line.shift)])
                if shift_cofirm_ids:
                    raise UserError(u"Тухайн өдрийн ээлж батлагдсан байгаа тул үнэ солигдохгүйг анхаарна уу.")
                if shift:
                    for sh in shift.shift_product_ids:
                        if sh.product_id.id == shts.product_id.id and sh.sale_price != shts.new_price:
                            sale_litr = 0.0
                            for lis in shift.mile_target_ids:
                                for tline in self.line_gasol_ids:
                                    if lis.product_id.id==tline.product_id.id and tline.break_id.name==lis.name:
                                        sale_litr += tline.mile_target_amount - lis.first_mile
                            if sh.product_id.id==shts.product_id.id:
                                sh.write({'change_price':sale_litr})
                            
                            if sale_litr == 0.0:
                                products = product_obj.search([('can_id','=',sh.can_id.id),('shift_id','=',sh.shift_id.id),('product_id','=',shts.product_id.id),('change_price','=',sale_litr),('company_id','=',shts.company_id.id)],limit=1)
                                if products.sale_litr > 0.0:
                                    if not products:
                                        product_obj.create({
                                                            'can_id':sh.can_id.id,
                                                            'product_id':sh.product_id.id,
                                                            'sale_price':shts.new_price,
                                                            'sale_litr':sale_litr,
                                                            'shift_id':sh.shift_id.id,
                                                            'total_amount':sale_litr*shts.new_price,
                                                            'change_price':sale_litr,
                                                            'is_change_price':True,
                                                            'company_id':sh.company_id.id,
                                                            })
                                    else:
                                        products.write({'sale_price':shts.new_price})
                                        
                                else:
                                    products.write({'sale_price':shts.new_price})
                            else:
                                products = product_obj.search([('can_id','=',sh.can_id.id),('shift_id','=',sh.shift_id.id),('product_id','=',shts.product_id.id),('change_price','=',sale_litr),('sale_litr','=',sale_litr),('company_id','=',shts.company_id.id)],limit=1)
                                if sale_litr> 0.0:
                                    if not products:
                                        product_obj.create({
                                                            'can_id':sh.can_id.id,
                                                            'product_id':sh.product_id.id,
                                                            'sale_price':shts.new_price,
                                                            'sale_litr':sale_litr,
                                                            'shift_id':sh.shift_id.id,
                                                            'total_amount':sale_litr*shts.new_price,
                                                            'change_price':sale_litr,
                                                            'is_change_price':True,
                                                            'company_id':sh.company_id.id,
                                                            })
                                    else:
                                        product_obj.create({
                                                            'can_id':sh.can_id.id,
                                                            'product_id':sh.product_id.id,
                                                            'sale_price':shts.new_price,
                                                            'sale_litr':sale_litr,
                                                            'shift_id':sh.shift_id.id,
                                                            'total_amount':sale_litr*shts.new_price,
                                                            'change_price':sale_litr,
                                                            'is_change_price':True,
                                                            'company_id':sh.company_id.id,
                                                            })
                                else:
                                    products.write({'sale_price':shts.new_price})
                                
                shts_line = shts_line_obj.search([('shts_id','=',self.shts_id.id)])
                for li in shts_line:
                    if li.product_id.id == shts.product_id.id and li.price !=shts.new_price:
                        li.write({
                                'price':shts.new_price,
                                })
            self.write({'state':'done',
                        'confirm_date':fields.Datetime.now(),
                        })
        elif self.oil_line_ids:
            for lins in self.oil_line_ids:
                for shts_id in self.shts_id.oil_line_ids:
                    if lins.product_id.id == shts_id.product_id.id:
                        shts_id.write({'price':lins.new_price})
            self.write({'state':'done',
                        'confirm_date':fields.Datetime.now(),
                        })
        else:
            raise UserError(u"Миллийн заалтаа оруулна уу.")
        return True
    
    
    def action_data_import(self):
        line_obj = self.env['price.change.register.line']
        shts_line_obj = self.env['shts.register.line']
        price_obj = self.env['price.gasol.register.shts']
        shift_obj = self.env['shift.working.register']
        break_obj = self.env['mile.target.register']
        product_obj = self.env['product.product']

        if self.type == 'gasol':
            for lin in self.line_ids:
                self.env.cr.execute(""" select sl.product_id, sh.company_id,crl.break_id,crl.can_id  from 
                                             shts_register_line as sl 
                                             left join shts_register as sh on sh.id = sl.shts_id
                                             left join can_register as cr on cr.shts_id = sh.id
                                             left join can_register_line as crl on crl.can_id = cr.id
                                             where sl.type = '%s' and sl.shts_id = %s and sl.product_id = %s 
                                             and crl.active = true
                                             and crl.product_id = sl.product_id and cr.active=true group by sl.product_id, sh.company_id ,crl.break_id, crl.can_id
                                            """%(self.type,self.shts_id.id,lin.product_id.id))
                datas = self._cr.dictfetchall()
                for lines in datas:
                    first_mile = 0.0
                    shift = shift_obj.search([('state','=','draft'),('shts_id','=',self.shts_id.id),('shift','=',lin.shift),('shift_date','=',str(self.shift_date))])
                    if shift:
                        break_ids = break_obj.browse(lines['break_id'])
                        for li in shift.mile_target_ids:
                            if li.product_id.id == lines['product_id'] and li.name==break_ids.name:
                                first_mile =li.first_mile
                    else:
                        raise UserError(u"Ээлж үүсээгүй байгаа тул ээлжээ үүсгэнэ үү.")
                    price_data = price_obj.search([('can_id','=',lines['can_id']),('break_id','=',lines['break_id']),('shts_id','=',self.shts_id.id),('product_id','=',lin.product_id.id),('price_change_id','=',self.id)])
                    if not price_data:
                            price_obj.create({
                                             'can_id':lines['can_id'],
                                             'company_id': lines['company_id'],
                                             'shts_id': self.shts_id.id,
                                             'product_id':lines['product_id'],
                                             'date':self.start_date,
                                             'shift':lin.shift,
                                             'break_id':lines['break_id'],
                                             'first_mile':first_mile,
                                             'price_change_id':self.id,
                                            })

        self.write({'is_download':True})
        
        return True
    
    
class PriceRegisterShtsLine(models.Model):
    _name = 'price.register.shts.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('price.register'))
    shts_id = fields.Many2one('shts.register','Shts',required=True)
    price_change_id = fields.Many2one('price.register.shts',string='Price change')
    oil_change_id = fields.Many2one('price.register.shts',string='Price change')
    product_id = fields.Many2one('product.product','Product',required=True,domain="[('type','=',type)]")
    old_price = fields.Float(string='Old Price',required=True,readonly=True)
    new_price = fields.Float(string='New Price',required=True)
    break_id = fields.Many2one('mile.target.register',string=u'Хошуу')
    qty = fields.Float(string='Quantity')
    shift = fields.Float(string=u'Ээлж')
    date = fields.Datetime(string=u'Өөрчлөлт эхлэх огноо', default=fields.Date.context_today)
    state = fields.Selection([('draft','Draft'),
                              ('send','Send'),
                              ('confirm','Confirm'),
                              ('done','Done')],string='State',default='draft')
    mile_point_ids = fields.Many2many('mile.point.shts','price_line_mile_point_new_rel','price_id','mile_id',string='Mile point')
    mile_target_amount = fields.Float(string=u'Үнэ сольсон милл')
    type = fields.Selection([('gasol',u'Шатахуун'),
                             ('oil',u'Тос'),
                             ('akk',u'Аккумулятор'),
                             ('service',u'Үйлчилгээ')],string=u'Төрөл',required=True)

    
    
    
class PriceGasolRegisterShts(models.Model):
    _name = 'price.gasol.register.shts'
    
    can_id = fields.Many2one('can.register',string=u'Сав')
    company_id = fields.Many2one('res.company',string='Company')
    product_id = fields.Many2one('product.product',string='Product')
    price_change_id = fields.Many2one('price.register.shts',string='Price change')
    shts_id = fields.Many2one('shts.register',string='Shts')
    mile_target_amount = fields.Float(string=u'Үнэ сольсон милл')
    first_mile = fields.Float(string=u'Эхний милл')
    shift = fields.Float(string=u'Ээлж')
    diff_mile = fields.Float(string=u'Зөрүү',compute="action_diff_mile")
    break_id = fields.Many2one('mile.target.register',string=u'Хошуу')
    date = fields.Datetime(string=u'Өөрчлөлт эхлэх огноо', default=fields.Date.context_today)
    state = fields.Selection([('draft','Draft'),
                              ('send','Send'),
                              ('confirm','Confirm'),
                              ('done','Done'),
                              ('cancel','Cancel'),
                              ('return','Return')],string='State',default='draft',compute="action_state")
    
    def action_diff_mile(self):
        for line in self:
            line.diff_mile = line.mile_target_amount - line.first_mile
    
    def action_state(self):
        for line in self:
            line.state = line.price_change_id.state
    
    
class MilePointShts(models.Model):
    _name = 'mile.point.shts'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('mile.point'))
    name = fields.Many2one('mile.target.register',string='Break')
    mile_point = fields.Float(string='Mile point')
    