# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'
    
    is_warehouse = fields.Boolean(string='Is warehouse',default=True)
    region_id = fields.Many2one('shts.region',string='Region')