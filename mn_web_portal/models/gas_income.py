# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class GasIncome(models.Model):
    _name = 'gas.income'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']  
    
    company_id = fields.Many2one('res.company',string=u'Компани')
    name = fields.Char(string=u'Падаан дугаар')
    wagon_number = fields.Char(string='Вагон дугаар', size=12)
    shift = fields.Integer(string=u'Ээлж',required=True)
    check = fields.Selection([('sgs', 'SGS'),('amber', 'Amber'),('mvhavt','MVHAVT'),('gtbg','GTBG')], 'Check')
    achilt_torol = fields.Selection([('gar', u'Гар ачилт'),('avtomat', u'Автомат ачилт')], u'Ачилтын төрөл',required=True,default='gar')
    from_company_code = fields.Char(string=u'Илгээх компани код')
    to_company_code = fields.Char(string=u'Хүлээн авах компани код')
    from_type = fields.Selection([('warehouse',u'Агуулах')],string='Гарах төрөл',default='warehouse')
    to_type = fields.Selection([('warehouse',u'Агуулах')],string='Хүрэх төрөл',default='warehouse')
    order_type = fields.Selection([('transfer',u'Шилжүүлэх')],string=u'Захиалгын төрөл',default='transfer')
    order_origin = fields.Char(string=u'Order origin')
    warehouse_id = fields.Many2one('shts.register',string=u'Гарсан агуулах',required=True)
    shift_id = fields.Many2one('shift.working.register',string=u'Ээлж')
    partner_id = fields.Many2one('res.partner', string='Жолооч', required=True,domain=[('drive_true','=','True')])
    drive_id = fields.Many2one('hr.employee',string=u'Жолооч',domain=[('work_type','=','drive')])
    new_drive_id = fields.Many2one('res.partner',string=u'Жолооч')
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id)
    reg_date = fields.Datetime(string=u'Илгээсэн огноо', default=fields.Date.context_today)
    send_date = fields.Date(string=u'Илгээсэн огноо', default=fields.Date.context_today)
    rec_date = fields.Datetime(string=u'Хүлээн авсан огноо')
    shift_date = fields.Date(string='')
    type = fields.Selection([('in',u'Орлого'),
                             ('ex',u'Бусад зарлага')],string=u'Төрөл')
    reg_user_id = fields.Many2one('res.users',string=u'Бүртгэсэн ажилтан',default=lambda self: self.env.user)
    line_ids = fields.One2many('gas.income.line','gas_income_id',string='Line')
    state = fields.Selection([('draft',u'Ноорог'),
                              ('send','Send'),
                              ('compute','Compute'),
                              ('receive','Received'),
                              ('cancel','Cancel')],string=u'Төлөв',default='draft',tracking=True)
    receive_user_id = fields.Many2one('hr.employee',string=u'Хүлээн авсан ШТС менежер')
    
    def action_compute(self):
        self.write({'state':'compute'})  
    
    def action_to_draft(self):
        for line in self:
            line.write({'state':'draft'})
    
    def action_rec(self):
        self.write({'state':'receive'})
        
        for line in self.line_ids:
            line.write({'state':'receive'})
    
    
    def action_send(self):
        self.write({'state':'send'})
        for line in self.line_ids:

            line.write({'state':'send',
                        })
    
    def action_return(self):
        self.write({'state':'draft'})
        for line in self.line_ids:
            line.write({'state':'draft'})
    
    def unlink(self):
        for line in self:
            if line.state in ('send','receive','cancel'):
                raise UserError(_(u"Ноорог биш тул устгах боломжгүйг анхаарна уу"))
            line.line_ids.unlink()
        return super(GasIncome, self).unlink()
    
    
class GasIncomeLine(models.Model):
    _name = 'gas.income.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']  

    company_id = fields.Many2one('res.company',string=u'Компани')
    car_id = fields.Many2one('car.register',string=u'Машин',required=True)
    propi_per = fields.Float(string=u'Пропи %')
    butan_per = fields.Float(string=u'Бутан %')
    out_can_id = fields.Many2one('can.register',string=u'Гарсан сав')
    gas_income_id = fields.Many2one('gas.income',string='Shts income')
    oil_income_id = fields.Many2one('gas.income',string='Shts income')
    product_id = fields.Many2one('product.product',string='Бүтээгдэхүүн',domain=[('prod_type','=','gasol')])
    torkh_id = fields.Many2one('bucket.register',string=u'Торх',domain="[('car_number_id','=',car_id)]")
    shift_id = fields.Many2one('shift.working.register',string=u'Ээлж')
    can_id = fields.Many2one('can.register',string=u'Сав',domain="[('is_oil_can','=',False)]")
    temp = fields.Float(string=u'Темп',digits=(10,1),default=0.0)
    self_weight = fields.Float(string='Хувийн жин',digits=(10,4),default=0.0)
    hour = fields.Float(string=u'Цаг',default=0.0)
    litr = fields.Float(string=u'Литр',default=0.0)
    kg = fields.Float(string=u'Кг',default=0.0,digits=(10,2),)
    send_qty = fields.Float(string=u'Илгээсэн тоо',default=0.0)
    rec_qty = fields.Float(string=u'Хүлээн авсан тоо',default=0.0)
    diff_qty = fields.Float(string=u'Зөрүү',default=0.0)
    torkh_size = fields.Float(string=u'Литр',default=0.0)
    density_20 = fields.Float(string=u'20 Темп хувийн жин',digits=(10,4),default=0.0)
    huurult = fields.Float(string=u'Хөөрөлт',default=0.0)
    rec_temp = fields.Float(string=u'Хүлээн авах темп',digits=(10,1),default=0.0)
    rec_self_weight = fields.Float(string='Хүлээн авах хувийн жин',digits=(10,4),default=0.0)
    rec_litr = fields.Float(string='Хүлээн авах литр',default=0.0)
    rec_kg = fields.Float(string='Хүлээн авах кг',default=0.0)
    rec_huurult = fields.Float(string='Хүлээн авах хөөрөлт',default=0.0)
    total_litr = fields.Float(string=u'Ирсэн /литр/',default=0.0)
    total_kg = fields.Float(string=u'Ирсэн /кг/',default=0.0)
    niil_hariutsah = fields.Float(string=u'Нийлүүлэгч хариуцах',default=0.0)
    p_density_check = fields.Float(string=u'Падааны чанарын зөрүү',default=0.0)
    h_density_check = fields.Float(string=u'Хэмжилтийн чанарын зөрүү',default=0.0)
    thh = fields.Float(string=u'ТХХ кг',default=0.0)
    zuruu_kg = fields.Float(string=u'Хасах кг',default=0.0)
    p_kg = fields.Float(string=u'Падаан (кг)',default=0.0)
    own_kg = fields.Float(string=u'Зөвшөөрөгдөх алдаа',default=0.0)
    p_litr = fields.Float(string=u'Харгалзах падаан литр',default=0.0)
    h_litr = fields.Float(string=u'Харгалзах хэмжилт литр',default=0.0)
    zuruu_litr = fields.Float(string=u'Зөрүү литр',default=0.0)
    hariu_kg = fields.Float(string=u'Хариу падаан (кг)',default=0.0,digits=(10,2),)
    hariu_litr = fields.Float(string=u'Хариу падаан (литр)',default=0.0,digits=(10,2),)
    irvel_zohih_kg = fields.Float(string=u'Ирвэл зохих кг',default=0.0,digits=(10,2))
    irvel_zohih_litr = fields.Float(string=u'Ирвэл зохих литр',default=0.0,digits=(10,2))
    total_diff = fields.Float(string='Нийт зөрүү',default=0.0)
    drive_diff = fields.Float(string='Жолооч зөрүү',default=0.0)
    drive_litr = fields.Float(string=u'Тээвэрлэгч хариуцах литр',default=0.0)
    balance_diff = fields.Float(string='Үлдэгдэл зөрүү',default=0.0)
    is_button_click = fields.Boolean(string=u'Тооцоолсон')
    type = fields.Selection([('winter',u'Өвөл'),
                             ('summer',u'Зун')],string=u'Төрөл')
    state = fields.Selection([('draft','Draft'),
                              ('send','Send'),
                              ('compute','Compute'),
                              ('receive','Receive')],string='Төлөв',default='draft')