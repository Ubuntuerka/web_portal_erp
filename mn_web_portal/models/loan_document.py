# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class LoanDocumentRegister(models.Model):
    _name = 'loan.document.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('loan.document.register'))
    name = fields.Char(string='Document №')
    reg_user_id = fields.Many2one('res.users',string='Regiser user',default=lambda self: self.env.user)
    date = fields.Date(string='Date', default=fields.Date.context_today)
    line_ids = fields.One2many('loan.document.register.line','loan_id',string='Loan document line')
    state = fields.Selection([('draft','Draft'),
                              ('done','Done')],string='State',default='draft')
    is_infor = fields.Boolean(string=u'ЛН Төлөв',default=False)


class LoanDocumentRegisterLine(models.Model):
    _name = 'loan.document.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    @api.model
    def _product_id(self):
        product = []
        shts_obj = self.env['shts.register']
        shts_id = shts_obj.browse(self.env.user.warehouse_id.id)
        for lined in shts_id.line_ids:
            product.append(lined.product_id.id)
        return [('id', 'in', product)]
    
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env['res.company']._company_default_get('loan.document.register'))
    phone = fields.Char(string=u'Утас',compute="action_phone")
    is_infor = fields.Boolean(string=u'ЛН Төлөв',default=False)
    infor_number = fields.Char(string='Infor number')
    product = fields.Char(string=u'Бүтээгдэхүүн',compute="action_product")
    petrol_station = fields.Char(string=u'ШТС',compute="action_shts_id")
    loan_id = fields.Many2one('loan.document.register',string='Loan document')
    partner_reg_number = fields.Char(string='Partner register number')
    name = fields.Char(string='Document №',required=True)
    desc = fields.Char(string='Description')
    partner_id = fields.Many2one('res.partner',string='Partner',required=True)
    product_id = fields.Many2one('product.product',string='Product',required=True,domain=_product_id)
    litr = fields.Float(string='Litr',required=True,compute="action_total_price")
    kg = fields.Float(string='Кг',compute="action_calc_kg")
    price = fields.Float(string='Price')
    qty = fields.Float(string=u'Тоо')
    tax_percent = fields.Float(string='Tax percent')
    total_amount = fields.Float(string='Total amount',required=True)
    car_number_id = fields.Char('Car number',required=True)
    date = fields.Date(string='Date', default=fields.Date.context_today)
    shts_id = fields.Many2one('shts.register',string='Shts')
    drive_id = fields.Char(string='Drive',required=True)
    shts_code = fields.Char(string=u'ШТС код',compute="action_shts_id")
    partner_code = fields.Char(string=u'Харилцагч код',compute="action_product")
    product_code = fields.Char(string=u'Бүтээгдэхүүний код')
    reg_date = fields.Date(string=u'Огноо')
    order_type = fields.Char(string=u'Захиалгын төрөл',default='I05')
    transaction_type = fields.Char(string=u'Гүйлгээний төрөл',default='Issue')
    work_center = fields.Many2one('expense.type',string='Work center')
    expense_type_id = fields.Many2one('expense.type',string='Expense type',required=True)
    can_id = fields.Many2one('can.register',string=u'Сав',domain="[('is_oil_can','=',False)]",required=True)
    type = fields.Selection([('loan',u'Зээлээр'),
                             ('motor',u'Моторт'),
                             ('expense',u'Зардалд'),
                             ('bonus',u'Урамшуулалд')],string='Type',default='loan',required=True)
    shift_id = fields.Many2one('shift.working.register',string='Shift register')
    
    #litr_new = fields.Float(string=u'Литр',related='litr',store=True)
    litr_kg = fields.Float(string=u'Кг',related='kg',store=True)
    itemId = fields.Char(string=u'Санхүүгийн код',compute="action_product")
    LocationId = fields.Char(string=u'ШТС код',compute="action_shts_id")
    CustomerName = fields.Char(string=u'Харилцагчийн нэр',compute="action_product")
    
    
    
    def action_calc_kg(self):
        for line in self:
            total_kg = 0.0
            for sc in line.shift_id.scale_ids:
                if sc.product_id.id==line.product_id.id and sc.can_id.id==line.can_id.id:
                    total_kg = line.litr*((sc.dhj_amount))
            line.kg = total_kg
    
    @api.onchange('partner_reg_number')
    def onchange_partner_id(self):
        for line in self:
            partner_id = self.env['res.partner'].search([('vat','=',line.partner_reg_number)])
            line.partner_id = partner_id.id
            line.partner_code = partner.code
            
    
    def action_phone(self):
        for line in self:
            line.phone = line.partner_id.phone
        
    def action_shts_id(self):
        for line in self:
            line.petrol_station = line.shift_id.shts_id.name
            line.shts_code = line.shift_id.shts_id.code
            line.LocationId = line.shift_id.shts_id.shts_code
            
    def action_product(self):
        for line in self:
            line.product = line.product_id.name
            line.partner_code = line.partner_id.vat
            line.itemId = line.product_id.fin_code
            line.CustomerName = line.partner_id.name

        
    @api.onchange('litr')
    def onchange_partner_id(self):
        for line in self:
            line.action_total_price()
        
        
    @api.onchange('product_id')
    def onchange_partner_id(self):
        shts_obj = self.env['shts.register']
        for line in self.shift_id.shts_id.line_ids:
            if self.product_id.id == line.product_id.id:
                self.price = line.price
    
    
    def action_total_price(self):
        for line in self:
            if line.total_amount >0.0 and line.price > 0.0:
                line.litr = round(line.total_amount/line.price,2)
            else:
                line.litr = 0.0

    def action_print(self):
        return self.env.ref('mn_web_portal.report_loan_document').report_action(self)

    def action_print_unegui(self):
        return self.env.ref('mn_web_portal.report_loan_document_unegui').report_action(self)

    
