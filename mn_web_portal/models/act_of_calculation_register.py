# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from datetime import timedelta, datetime
from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class ActOfCalculationRegister(models.Model):
    _name = 'act.of.calculation.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    name = fields.Char(string=u'Дугаарлалт',required=True)
    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True,copy=False)
    shts_id = fields.Many2one('shts.register', string='ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id,copy=False)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True,copy=False)
    end_date = fields.Date(string=u'Дуусах огноо', required=True,copy=False)
    line_ids = fields.One2many('act.of.calculation.register.line', 'calculation_id', string='line')
    state = fields.Selection([('draft',u'Ноорог'),
                              ('send', u'Илгээсэн'),
                              ('confirm',u'Баталсан'),
                              ('check',u'Хянасан')],string=u'Төлөв',default='draft',tracking=True)
    
    
    
    @api.model
    def create(self, vals):
        if 'company_id' in vals and 'start_date' in vals and 'shts_id' in vals:
            new_datas = self.search([('company_id','=',vals['company_id']),('start_date','=',vals['start_date']),('shts_id','=',vals['shts_id'])])
            if len(new_datas.ids)>1:
                raise UserError(u'Тооцоо бодсон акт давхцаж бүртгэж болохгүй тул та бүртгэлээ шалгана уу !')
        if 'shts_id' in vals:
            if self.env.user.shts_ids:
                if vals['shts_id'] not in self.env.user.shts_ids.ids:
                        raise UserError(u'Та өөр штс сонгож үүсгэж байгаа тул зөв штс сонгоно уу!')
        calculationRegister = super(ActOfCalculationRegister, self.with_context()).create(vals)
        return calculationRegister
    
    
    def write(self, vals):
        calculation_register = super(ActOfCalculationRegister,self).write(vals)
        if 'shts_id' in vals:
            if self.env.user.shts_ids:
                if vals['shts_id'] not in self.env.user.shts_ids.ids:
                        raise UserError(u'Та өөр штс сонгож үүсгэж байгаа тул зөв штс сонгоно уу!')
        
        return calculation_register
    
    
    def action_return(self):
        self.write({'state':'draft'})
        
    def action_confirm(self):
        self.write({'state':'confirm'})
    
    def action_send(self):
        self.write({'state':'send'})
        
    def action_check(self):
        self.write({'state':'check'})
    
    
    def action_act_calc_search(self, company_id, start_date, end_date):
         
         product_name = False
         itemId = False
         LocationId = False
         petrol_station = False
         can_code = False
         hh_litr = 0
         hh_kg = 0
         count = 0
         lines = []
         if company_id and start_date and end_date:
             line_ids = self.search([('company_id','=',company_id),('start_date','=',str(start_date)),('end_date','=',str(end_date))],limit=1000)
             if line_ids:
                 for line_id in line_ids:
                     if line_id:
                         for line in line_id.line_ids:
                             
                             count += 1
                             
                             lines.append({
                            "company_id": company_id,
                            "LocationId": line_id.shts_id.pos_number,
                            "petrol_station": line_id.shts_id.name,
                            "can_code": line.can_id.name,
                            "itemId": line.product_id.fin_code,
                            "product_name": line.product_id.name,
                            "hh_litr": round(line.hewiin_litr,2),
                            "hh_kg": round(line.hewiin_kg,2)
                })
          
         if count == 0:
             return {
                    "count":0,
                    "result":{}
                 }
         else:
             return {
                    "count":count,
                    "result":
                            lines
                        
                 }

    def action_compute(self):
        line_obj = self.env['act.of.calculation.register.line']
        product_obj = self.env['product.product']
        product_ids = product_obj.search([('prod_type','=','gasol')])
        shift_obj = self.env['shift.working.register']
        income_obj = self.env['shts.income']
        shts_income_obj = self.env['shts.income']
        other_exp_obj = self.env['shts.income']
        gas_income_obj = self.env['gas.income']
        normal_loss_obj = self.env['normal.loss.compute']
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        context = dict(self.env.context or {})
        lin_data = []
        total_kg = 0.0
        datas = []
        can_obj = self.env['can.register']
        if product_ids !=[]:
            for pro in self.shts_id.line_ids:
                can_ids = can_obj.search([('shts_id','=',self.shts_id.id)])
                for can in can_ids:
                    shift_obj = self.env['shift.working.register']
                    first_litr = 0.0
                    first_height = 0.0
                    first_self_weight = 0.0
                    first_temp = 0.0
                    first_kg = 0.0
                    income_litr = 0.0
                    income_kg = 0.0
                    income_dhj = 0.0
                    exp_litr = 0.0
                    exp_kg = 0.0
                    have_litr = 0.0
                    have_kg = 0.0
                    last_litr = 0.0
                    last_kg = 0.0
                    diff_litr = 0.0
                    diff_kg = 0.0
                    dhg_amount = 0.0
                    zarlaga_kg = 0.0
                    zarlaga_litr = 0.0
                    income_total = 0.0
                    exp_zalin = 0.0
                    last_self_weight = 0.0
                    last_temp = 0.0
                    zalin = 0.0
                    dhj = 0.0
                    
                    other_exp_kg = 0.0
                    height_count = 1
                    last_heigth = 0.0
                    normal_kg = 0.0
                    normal_litr = 0.0
                    more_litr = 0.0
                    more_kg = 0.0
                    have_litr_l = 0.0
                    have_kg_l = 0.0
                    date_p = datetime.strptime(str(self.start_date), DATE_FORMAT)
                    if self.shts_id.work_time == 2:
                        pdate = date_p-timedelta(hours=12)
                    elif self.shts_id.work_time == 1:
                        pdate = date_p-timedelta(hours=24)
                    if self.shts_id.work_time == 2:
                        shift_idss = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',self.start_date),('state','in',('draft','confirm','count'))],order="shift asc")
                    else:
                        pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('state','in',('draft','confirm','count'))],order="shift asc")
                        shift_idss = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',self.start_date),('state','in',('draft','confirm','count'))],order="shift asc")
                    
                    if shift_idss:
                        for shifti in shift_idss:
                            if self.shts_id.work_time==2:
                                if shifti.shift == 1:
                                    pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                                else:
                                    pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                                
                            elif self.shts_id.work_time==1:
                                pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                                
                            
                        if pre_shift:
                            for li in pre_shift.scale_ids:
                                if li.product_id.id == pro.product_id.id and can.id==li.can_id.id:
                                    height_count +=1
                                    first_height = li.name
                                    first_temp = float(li.temperature)
                                    
                                    first_litr += li.litr+li.can_id.balance
                                    first_kg += li.kg + li.can_id.balance*li.self_weight_value
    
    
    
                        
                    shift_ids = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('state','in',('draft','confirm','count'))],order="shift asc")
                    
                    signin_day_b = datetime.strptime(str(self.start_date), DATE_FORMAT)
                    signout_day_b = datetime.strptime(str(self.end_date), DATE_FORMAT)
                    signin_day = signin_day_b.replace(hour=0, minute=0, second=1)
                    
                    signout_day = signout_day_b.replace(hour=23, minute=59, second=59)
                    
                    if shift_ids:
                        other_exp_litr = 0.0
                        normal_ids = normal_loss_obj.search([('start_date','=',self.start_date),('end_date','=',self.end_date),('state','=','done'),('shts_id','=',self.shts_id.id)])
                        if self.shts_id.work_time == 2:
                            last_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift','=',2),('shift_date','>=',self.end_date),('shift_date','<=',self.end_date),('state','in',('draft','confirm','count'))],order="shift asc")
                        else:
                            last_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift','=',1),('shift_date','>=',self.end_date),('shift_date','<=',self.end_date),('state','in',('draft','confirm','count'))],order="shift asc")
    
                        for lines in last_shift.scale_ids:
                            if lines.product_id.id==pro.product_id.id and lines.can_id.id==can.id:
                                last_litr =lines.litr+lines.can_id.balance
                                last_kg =lines.kg+lines.can_id.balance*lines.self_weight_value
                        
                        for shift_d in shift_ids:
                            income = shts_income_obj.search([('shts_id','=',self.shts_id.id),('shift','=',shift_d.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','=','receive')])
                            oth_exp = shts_income_obj.search([('warehouse_id','=',self.shts_id.id),('shift','=',shift_d.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','=','receive')])
                            gas_income = gas_income_obj.search([('shts_id','=',self.shts_id.id),('shift','=',shift_d.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','not in',('cancel','draft'))])

                            for oth in oth_exp.line_ids:
                                if oth.product_id.id == pro.product_id.id and shift_d.shift_date==oth.shts_income_id.shift_date and oth.out_can_id.id==can.id:
                                    other_exp_litr += oth.torkh_size
                                    other_exp_kg += oth.kg
                            for li in income.line_ids:
                                if li.product_id.id == pro.product_id.id and shift_d.shift_date==li.shts_income_id.shift_date and li.can_id.id==can.id:
                                    income_litr += li.hariu_litr
                                    income_kg += li.hariu_kg
                            
                            for gli in gas_income.line_ids:
                                if gli.can_id.id == can.id and gli.product_id.id == pro.product_id.id and shift_d.shift_date==gli.gas_income_id.shift_date:
                                    income_litr += gli.hariu_litr
                                    income_kg += gli.hariu_kg
                            
                            in_num = first_litr+income_litr+last_litr
                           # if income_litr>0.0:
                           #     if in_num > 0.0:
                           #         income_dhj = round((first_kg+income_kg+last_kg)/in_num,4)
                           #     else:
                           #         income_dhj = 0.0
                           #else:
                           #     if first_litr >0.0 and last_litr >0.0:
                           #         income_dhj = round((first_kg+last_kg)/(first_litr+last_litr),4)
                           #     else:
                           #         if first_litr>0.0 and other_exp_litr>0.0:
                           #           income_dhj = round((first_kg+other_exp_kg)/(first_litr+other_exp_litr),4)
                                    
                            for exp in shift_d.mile_target_ids:
                                if exp.can_id.id==can.id and exp.product_id.id==pro.product_id.id:
                                    exp_litr += exp.expense_litr
                                    zalin +=exp.zalin
                            for shift_line in shift_d.scale_ids:
                               if shift_line.can_id.id==can.id and shift_line.product_id.id==pro.product_id.id:
                                    
                                    exp_kg += round(shift_line.total_kg,2)
                                    
                        
                        have_kg = first_kg + income_kg - exp_kg - other_exp_kg
                        have_litr = first_litr + income_litr - exp_litr - other_exp_litr
                        zarlaga_kg = exp_litr
                        #income_dhj = 
                        
                        
                        for norm in normal_ids.line_ids:
                            if norm.product_id.id == pro.product_id.id and norm.can_id.id==can.id:
                                normal_kg +=norm.total_loss_kg
                        in_num = first_litr+income_litr+last_litr
                        if exp_litr > 0.0 and exp_kg >0.0:
                            income_dhj = round((exp_kg)/exp_litr,4)
                        else:
                            income_dhj = 0.0

                        if income_dhj >0.0:
                            normal_litr = normal_kg/income_dhj
                            
                        have_kg_l = have_kg-normal_kg
                        have_litr_l = have_litr-normal_litr
                        
                        if last_litr <have_litr_l:
                            diff_litr = last_litr - have_litr_l
                        if last_kg < have_kg_l:
                            diff_kg = last_kg - have_kg_l
                        if last_litr > have_litr_l:
                            more_litr = last_litr - have_litr_l
                        if last_kg > have_kg_l:
                            more_kg = last_kg - have_kg_l
                        
                    data = {
                            'can_id':can.id,
                            'product_id':pro.product_id.id,
                            'first_litr':first_litr,
                            'first_kg':first_kg,
                            'income_litr':income_litr,
                            'income_kg':income_kg,
                            'exp_litr':exp_litr,
                            'exp_kg':exp_kg,
                            'hewiin_litr':normal_litr,
                            'hewiin_kg':normal_kg,
                            'have_litr':have_litr_l,
                            'have_kg':have_kg_l,
                            'belen_litr':last_litr,
                            'belen_kg':last_kg,
                            'iluu_litr':more_litr,
                            'iluu_kg':more_kg,
                            'dutuu_litr':diff_litr,
                            'dutuu_kg':diff_kg,
                            'dhj':income_dhj,
                            'calculation_id':self.id
                            }
                    if first_litr !=0.0:
                        line_ids = line_obj.search([('product_id','=',pro.product_id.id),('can_id','=',can.id),('calculation_id','=',self.id)])
                        if not line_ids:
                            line_obj.create(data)
                        else:
                            for lines in self.line_ids:
                                if lines.product_id.id == pro.product_id.id and lines.can_id.id == can.id and lines.calculation_id.id == self.id:
                                    u_have_kg = have_kg_l-lines.sav_tsew_kg
                                    u_have_litr = have_litr_l-lines.sav_tsew_litr
                                    u_more_litr = 0.0
                                    u_more_kg = 0.0
                                    u_diff_litr = 0.0
                                    u_diff_kg = 0.0
                                    if last_litr <u_have_litr:
                                        u_diff_litr = last_litr - u_have_litr
                                    if last_kg < u_have_kg:
                                        u_diff_kg = last_kg - u_have_kg
                                    if last_litr > u_have_litr:
                                        u_more_litr = last_litr - u_have_litr
                                    if last_kg > u_have_kg:
                                        u_more_kg = last_kg - u_have_kg
                                    lines.write({
                                                'first_litr':first_litr,
                                                'first_kg':first_kg,
                                                'income_litr':income_litr,
                                                'income_kg':income_kg,
                                                'exp_litr':exp_litr,
                                                'exp_kg':exp_kg,
                                                'hewiin_litr':normal_litr,
                                                'hewiin_kg':normal_kg,
                                                'have_litr':u_have_litr,
                                                'have_kg':u_have_kg,
                                                'belen_litr':last_litr,
                                                'belen_kg':last_kg,
                                                'iluu_litr':u_more_litr,
                                                'iluu_kg':u_more_kg,
                                                'dutuu_litr':u_diff_litr,
                                                'dutuu_kg':u_diff_kg,
                                                'report_zoruu_kg':u_more_litr+u_diff_litr,
                                                'report_zoruu_litr':u_more_kg+u_diff_kg,
                                                'dhj':income_dhj,
                                                })
                    elif first_litr ==0.0 and last_litr!=0.0:
                        line_ids = line_obj.search([('product_id','=',pro.product_id.id),('can_id','=',can.id),('calculation_id','=',self.id)])
                        if not line_ids:
                            line_obj.create(data)
                        else:
                            for lines in self.line_ids:
                                if lines.product_id.id == pro.product_id.id and lines.can_id.id == can.id and lines.calculation_id.id == self.id:
                                    u_have_kg = have_kg_l-lines.sav_tsew_kg
                                    u_have_litr = have_litr_l-lines.sav_tsew_litr
                                    u_more_litr = 0.0
                                    u_more_kg = 0.0
                                    u_diff_litr = 0.0
                                    u_diff_kg = 0.0
                                    if last_litr <u_have_litr:
                                        u_diff_litr = last_litr - u_have_litr
                                    if last_kg < u_have_kg:
                                        u_diff_kg = last_kg - u_have_kg
                                    if last_litr > u_have_litr:
                                        u_more_litr = last_litr - u_have_litr
                                    if last_kg > u_have_kg:
                                        u_more_kg = last_kg - u_have_kg
                                    
                                    lines.write({
                                                'first_litr':first_litr,
                                                'first_kg':first_kg,
                                                'income_litr':income_litr,
                                                'income_kg':income_kg,
                                                'exp_litr':exp_litr,
                                                'exp_kg':exp_kg,
                                                'hewiin_litr':normal_litr,
                                                'hewiin_kg':normal_kg,
                                                'have_litr':u_have_litr,
                                                'have_kg':u_have_kg,
                                                'belen_litr':last_litr,
                                                'belen_kg':last_kg,
                                                'iluu_litr':u_more_litr,
                                                'iluu_kg':u_more_kg,
                                                'dutuu_litr':u_diff_litr,
                                                'dutuu_kg':u_diff_kg,
                                                'dhj':income_dhj,
                                                'report_zoruu_kg':u_more_litr+u_diff_litr,
                                                'report_zoruu_litr':u_more_kg+u_diff_kg,
                                                })
                            
                            
    


class ActOfCalculationRegisterLine(models.Model):
    _name = 'act.of.calculation.register.line'

    calculation_id = fields.Many2one('act.of.calculation.register', string='calculation')
    can_id = fields.Many2one('can.register', string=u'Сав')
    product_id = fields.Many2one('product.product', string=u'Бүтээгдэхүүн')
    first_litr = fields.Float(string=u'Эх/үлд Литр', digits=(10, 2))
    first_kg = fields.Float(string=u'Эх/үлд кг', digits=(10, 2))
    income_litr = fields.Float(string=u'Орлого литр', digits=(10, 2))
    income_kg = fields.Float(string=u'Орлого кг', digits=(10, 2))
    exp_litr = fields.Float(string=u'Зарлага литр', digits=(10, 2))
    exp_kg = fields.Float(string=u'Зарлага кг', digits=(10, 2))
    hewiin_litr = fields.Float(string=u'Х/хорогдол литр', digits=(10, 2))
    hewiin_kg = fields.Float(string=u'Х/хорогдол кг', digits=(10, 2))
    have_litr = fields.Float(string=u'Б/зохих литр', digits=(10, 2))
    have_kg = fields.Float(string=u'Б/зохих кг', digits=(10, 2))
    belen_litr = fields.Float(string=u'Б/байгаа литр', digits=(10, 2))
    belen_kg = fields.Float(string=u'Б/байгаа кг', digits=(10, 2))
    iluu_litr = fields.Float(string=u'Илүү литр', digits=(10, 2))
    iluu_kg = fields.Float(string=u'Илүү кг', digits=(10, 2))
    dutuu_litr = fields.Float(string=u'Дутуу литр', digits=(10, 2))
    dutuu_kg = fields.Float(string=u'Дутуу кг', digits=(10, 2))
    dhj = fields.Float(string=u'ДХЖ', digits=(10, 4), size=5)
    sav_tsew_litr = fields.Float(string=u'СЦА литр', digits=(10, 2))
    sav_tsew_kg = fields.Float(string=u'СЦА кг', digits=(10, 2))
    report_zoruu_kg = fields.Float(string='Илүү дутуу кг')
    report_zoruu_litr = fields.Float(string='Илүү дутуу литр')
    




