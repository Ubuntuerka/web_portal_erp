# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import Warning, UserError

from odoo.exceptions import RedirectWarning, UserError, ValidationError
from odoo.osv.expression import get_unaccent_wrapper
from datetime import timedelta
from datetime import datetime
import re
from odoo.osv import expression
USER_PRIVATE_FIELDS = []
import json, requests


    
class talon_type(models.Model):
    _name = 'talon.type'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company'
                                  ,default=lambda self: self.env['res.company']._company_default_get('talon.type'))
    name = fields.Char(u'Ангилал',required=True)
    type_price = fields.Integer(u'Үнэ',required=True)
    active = fields.Boolean(u'Идэвхтэй эсэх')
    type_letter = fields.Char(u'Эхлэх үсэг', size=2,required=True)

class TalonRegister(models.Model):
    _name = 'talon.register'

    def _total_price(self):
        total = 0.0
        for view in self:
            total = view.talon_count * view.talon_price
            view.total_price = total
        
        
    def _get_default_warehouse(self):
        user = self.env['res.users'].browse(self._uid)
        return user.warehouse_id.id
    
  
    name = fields.Char(u'Баримтын дугаар',size=20,required=True)
    type_id = fields.Many2one('talon.type',u'Талоны төрөл',required=True)
    partner_id = fields.Many2one('res.partner',u'Эцсийн хэрэглэгч')
    partner_id1 = fields.Many2one('shts.register',u'Дотоод хэрэглэгч')
    buy_date = fields.Date(u'Огноо',default=fields.Date.context_today)
    state = fields.Selection([('draft', u'Ноорог'),('confirm', u'Бүртгэгдсэн')], u'Төлөв',default='draft')
    talon_count = fields.Integer(u'Тоо хэмжээ',required=True)
    start_series = fields.Char(u'Эхлэх сер',size=8,required=True)
    end_series = fields.Char(u'Дуусах сер',size=8)
    talon_price = fields.Integer(u'Үнэ')
    total_price = fields.Integer(compute=_total_price, string=u'Нийт дүн')
    warehouse_id = fields.Many2one('shts.register',u'Агуулах/ШТС',default=_get_default_warehouse)
    is_internal = fields.Boolean(u'Дотоод')
    status = fields.Selection([('income', u'Орлого'),('outgoing', u'Зарлага')], u'Статус')

class talon_view(models.Model):
    _name = 'talon.view'

    def _total_price(self):
        total = 0.0
        for view in self:
            total = view.talon_count * view.talon_price
            view.total_price = total
        
        
    def _get_default_warehouse(self):
        return self.env.user.warehouse_id.id
    
  
    name = fields.Char(u'Баримтын дугаар',size=20,required=True)
    type_id = fields.Many2one('talon.type',u'Талоны төрөл',required=True)
    partner_id = fields.Many2one('res.partner',u'Эцсийн хэрэглэгч')
    partner_id1 = fields.Many2one('shts.register',u'Дотоод хэрэглэгч')
    buy_date = fields.Date(u'Огноо',default=fields.Date.context_today)
    state = fields.Selection([('draft', u'Ноорог'),('confirm', u'Бүртгэгдсэн')], u'Төлөв',default='draft')
    talon_count = fields.Integer(u'Тоо хэмжээ',required=True)
    start_series = fields.Char(u'Эхлэх сер',size=8,required=True)
    end_series = fields.Char(u'Дуусах сер',size=8)
    talon_price = fields.Integer(u'Үнэ')
    total_price = fields.Integer(string=u'Нийт дүн',compute="_total_price")
    warehouse_id = fields.Many2one('shts.register',u'Агуулах/ШТС',default=_get_default_warehouse)
    is_internal = fields.Boolean(u'Дотоод')
    status = fields.Selection([('income', u'Орлого'),('outgoing', u'Зарлага')], u'Статус')
    is_bonus = fields.Selection([('1', u'Урамшуулалд'),('2', u'Худалдаанд')], u'Урамшуулал эсэх',default='2')

    
    
    @api.onchange('type_id','status')
    def onchange_type_id(self):
       talon_type_obj = self.env['talon.type']
       warning = 'Энэ төрөл дээр бүртгэгдсэн талон байхгүй байна'
       warning1 = "Хэрэглэгчийн статус талбар хоосон байна. Админ хэрэглэгчид хандана уу"
       if self.status == 'outgoing':
           user = self.env['res.users'].browse(self._uid)
           if user.user_status == 'cass':
              
              start_series = ''
              if self.type_id:
                  ll = self.type_id.type_letter
                  self.env.cr.execute("""
                  SELECT name 
                  FROM talon_inf 
                  where name like '%s' and state = 'print'
                  ORDER BY name asc limit (1)
                    """%(str(ll+'%')))
                  type_obj  = self._cr.dictfetchall()
                  if type_obj:
                     start_series = type_obj[0]['name']
                  else:
                      raise UserError(warning)
                      
                  self.start_series = start_series
                  self.talon_price = self.type_id.type_price
           elif user.user_status == 'gtba':
                start_series = ''
                if self.type_id:
                  ll = self.type_id.type_letter
                  self.env.cr.execute("""
                  SELECT t.name
                   FROM
                  talon_inf t
                 LEFT JOIN talon_view v on (t.view_id = v.id)
                  WHERE 
                t.state = 'gtba' and v.partner_id1 = %s and t.name like '%s'
                ORDER BY name asc limit (1)
                        
                    """%(user.warehouse_id.id,str(ll+"%")))
                  type_obj  = self._cr.dictfetchall()
                  if type_obj:
                     start_series = type_obj[0]['name']
                  else:
                      raise UserError(warning)
                  self.write({'start_series':start_series,
                              'talon_price':self.type_id.type_price})
           elif user.user_status == 'shts':
               start_series = ''
               if self.type_id:
                  ll = self.type_id.type_letter
                  self.env.cr.execute("""
                  
                  SELECT t.name
                   FROM
                  talon_inf t
                 LEFT JOIN talon_view v on (t.view_id = v.id)
                  WHERE 
                t.state = 'shts' and v.partner_id1 = %s and t.name like '%s'
                ORDER BY name asc limit (1)
                        
                    """%(user.warehouse_id.id,str(ll+"%")))
                  type_obj  = self._cr.dictfetchall()
                  if type_obj:
                     start_series = type_obj[0]['name']
                  else:
                      raise UserError(warning)
                  self.write({'start_series':start_series,
                              'talon_price':self.type_id.type_price})
           else:
               raise UserError(warning1)

       else:
           
           start_series = ''
           if self.type_id:
              ll = self.type_id.type_letter
              self.env.cr.execute("""
              
              SELECT name 
              FROM talon_inf 
              where name like '%s'
              
              ORDER BY name desc limit (1)
                    
                """%(ll))
              type_obj  = self._cr.dictfetchall()

              if type_obj:
                 start_series = type_obj[0]['name']
                 useg = start_series[0:2]
                 start = int(start_series[2:8]) + 1
                 start1 = self.add_zero(start)
                 start_series = str(useg) + str(start1)
              else:
                 start_series = str(self.type_id.type_letter) + '000001'
              self.write({'start_series':start_series,
                              'talon_price':self.type_id.type_price})
           
    

    @api.onchange('start_series','status','talon_count')
    def onchange_series_qty(self):
       
       user = self.env['res.users'].browse(self._uid)
       if self.status == 'outgoing':
           if self.start_series and self.talon_count > 0: 
               too = ''
               talon_inf_obj = self.pool.get('talon.inf')
               useg = self.start_series[0:2]
               start = int(self.start_series[2:8])
               sum = start + self.talon_count - 1
               sum1 = self.add_zero(sum)
               i = 0
               for to in range(self.talon_count):
                  start1 = self.add_zero(start + i)
                  series = str(useg) + str(start1)
                  if user.user_status == 'cass':
                    break

                  elif user.user_status == 'gtba':
                        self.env.cr.execute("""
                        SELECT t.name
                        FROM
                        talon_inf t
                        LEFT JOIN talon_view v on (t.view_id = v.id)
                        WHERE 
                        t.state = 'gtba' and v.partner_id1 = %s and t.name = '%s'
                        """%(user.warehouse_id.id,series))
                        type_obj  = self._cr.dictfetchall()
                        if type_obj:
                           i += 1
                        else:

                           raise UserError(_(u"%s эхлээд %s ширхэг талон гарах боломжгүй. Боломжит тоо %s байна") %
                                                   ( start_series,talon_count,i))
                  elif user.user_status == 'shts':
                        self.env.cr.execute("""
                          SELECT t.name
                           FROM
                            talon_inf t
                            LEFT JOIN talon_view v on (t.view_id = v.id)
                          WHERE 
                            t.state = 'shts' and v.partner_id1 = %s and t.name = '%s'
                            """%(user.warehouse_id.id,series))
                        type_obj  = self._cr.dictfetchall()
                        if type_obj:
                           i += 1
                        else:
                           raise UserError(_(u"%s эхлээд %s ширхэг талон гарах боломжгүй. Боломжит тоо %s байна") %
                                               ( self.start_series,self.talon_count,i))
                  
                  else:
                      raise UserError(_("Хэрэглэгчийн статус талбар хоосон байна. Админ хэрэглэгчид хандана уу"))   
               
               
               
               end_series = str(useg) + str(sum1)
               self.end_series = end_series

       else :
            if self.start_series and self.talon_count > 0: 
               too = ''
               talon_inf_obj = self.env['talon.inf']
               useg = self.start_series[0:2]
               start = int(self.start_series[2:8])
               sum = start + self.talon_count - 1
               sum1 = self.add_zero(sum)
               end_series = str(useg) + str(sum1)
               self.end_series = end_series
    
    def onchange_is_internal(self):
       if self.is_internal == False:
           self.partner_id1=0
       
       if self.is_internal: 
           self.partner_id = 0
      
    def add_zero(self, too):
       if len(str(too)) == 1:
           oron = '00000' + str(too)
       elif len(str(too)) == 2 :
           oron = '0000' + str(too)
       elif len(str(too)) == 3 :
           oron = '000' + str(too)
       elif len(str(too)) == 4 :
           oron = '00' + str(too)
       elif len(str(too)) == 5 :
           oron = '0' + str(too)
       else :
           oron = too
       return oron
        
    def action_to_confirm(self):
        
        obj = self.browse(self.id)
        user = self.env['res.users'].browse(self._uid)
        
        talon_inf_obj = self.env['talon.inf']
        talon_history_obj = self.env['talon.history']
        state = ''
        if obj.is_internal == False:
           state = 'buy'
        else:
            if obj.partner_id1.is_stock == True:
               state = 'gtba'
            else:
               state = 'shts'
           
        start = int(obj.start_series[2:8])
        useg = obj.start_series[0:2]
        i = 0
        for to in range(obj.talon_count):
            series = ''
            start1 = self.add_zero(start + i)
            series = str(useg) + str(start1)
     

            if user.user_status == 'cass':
                     self.env.cr.execute("""
                      SELECT t.id, t.view_id, t.doc_date,t.state
                       FROM
                        talon_inf t
                        LEFT JOIN talon_view v on (t.view_id = v.id)
                      WHERE 
                        t.state = 'print' and v.warehouse_id = %s and t.name = '%s'
                        """%(obj.warehouse_id.id,series))
                     type_obj  = self._cr.dictfetchall()
                     if type_obj:
                        i += 1
                        talon_history_obj.create({
                                'talon_id': type_obj[0]['id'],
                                'view_id': type_obj[0]['view_id'],
                                'state': type_obj[0]['state'],
                                'doc_date': type_obj[0]['doc_date'],
                                
                                } )
                        talon_inf = talon_inf_obj.browse(type_obj[0]['id'])
                        talon_inf.write({
                                'view_id': obj.id,
                                'doc_date': obj.buy_date,
                                'state': state,
                                } )
                     else:
                       raise UserError(_(u"%s эхлээд %s ширхэг талон гарах боломжгүй. Боломжит тоо %s байна") %
                                                ( obj.start_series,obj.talon_count,i))
            elif user.user_status == 'gtba':
                        self.env.cr.execute("""
                        SELECT t.id, t.view_id, t.doc_date,t.state
                        FROM
                        talon_inf t
                        LEFT JOIN talon_view v on (t.view_id = v.id)
                        WHERE 
                        t.state = 'gtba' and v.partner_id1 = %s and t.name = '%s'
                        """%(obj.warehouse_id.id,series))
                        type_obj  = self._cr.dictfetchall()
                        if type_obj:
                           i += 1
                           talon_history_obj.create({
                                'talon_id': type_obj[0]['id'],
                                'view_id': type_obj[0]['view_id'],
                                'state': type_obj[0]['state'],
                                'doc_date': type_obj[0]['doc_date'],
                                
                                } )
                           talon_inf = talon_inf_obj.browse(type_obj[0]['id'])
                           talon_inf.write({
                                'view_id': obj.id,
                                'doc_date': obj.buy_date,
                                'state': state,
                                } )
                        else:
                           raise UserError(_(u"%s эхлээд %s ширхэг талон гарах боломжгүй. Боломжит тоо %s байна") %
                                                   ( obj.start_series,obj.talon_count,i))
            elif user.user_status == 'shts':
                        self.env.cr.execute("""
                          SELECT t.id, t.view_id, t.doc_date,t.state
                           FROM
                            talon_inf t
                            LEFT JOIN talon_view v on (t.view_id = v.id)
                          WHERE 
                            t.state = 'shts' and v.partner_id1 = %s and t.name = '%s'
                            """%(obj.warehouse_id.id,series))
                        type_obj  = self._cr.dictfetchall()
                        if type_obj:
                           i += 1
                           talon_history_obj.create({
                                'talon_id': type_obj[0]['id'],
                                'view_id': type_obj[0]['view_id'],
                                'state': type_obj[0]['state'],
                                'doc_date': type_obj[0]['doc_date'],
                                
                                } )
                           talon_inf = talon_inf_obj.browse(type_obj[0]['id'])
                           talon_inf.write({
                                'view_id': obj.id,
                                'doc_date': obj.buy_date,
                                'state': state,
                                } )
                        else:
                           raise UserError(_(u"%s эхлээд %s ширхэг талон гарах боломжгүй. Боломжит тоо %s байна") %
                                               ( obj.start_series,obj.talon_count,i))
                  
            else:
                raise UserError(_("Хэрэглэгчийн статус талбар хоосон байна. Админ хэрэглэгчид хандана уу"))   
               
            
            
        self.write({'state': 'confirm'}) 
        
    def action_to_income(self):
        
        obj = self.browse(self.id)
        
        talon_inf_obj = self.env['talon.inf']
        state = 'print'
        start = int(obj.start_series[2:8])
        useg = obj.start_series[0:2]
        i = 0
        for to in range(obj.talon_count):
            series = ''
            start1 = self.add_zero(start+i)
            series = useg + str(start1)
            talon_inf_obj.create({
                                'view_id': obj.id,
                                'doc_date': obj.buy_date,
                                'name': series,
                                'type_price': obj.type_id.type_price,
                                'type_id': obj.type_id.id,
                                'state': state,
                                } )
            i += 1
            
        self.write({'state': 'confirm'}) 
    
    def unlink(self):
        for obj in self:
            talon_inf_obj = self.env['talon.inf']
            history_obj = self.env['talon.history']
            if obj.status == 'income':
               history_id =history_obj.search([('view_id','=',obj.id)])
               if history_id:
                      raise UserError(_(u"Устгах боломжгүй зарлага хийгдсэн байна. Админ хэрэглэгчид хандана уу. Зарлага тоо: %s")% len(history_id))
               else:
                    talon_id =talon_inf_obj.search( [('view_id','=',obj.id)])
                    if talon_id:
                      for tal in talon_id:
                          tal.unlink()
                    return super(talon_view, self).unlink()
               
                   
            else:
               history_id = history_obj.search([('view_id','=',obj.id)])
               if history_id:
                      raise UserError(_(u"Устгах боломжгүй БОИ бүртгэгдсэн байна. Эхлээд БОИ талон устгана уу!!"))
               else:
                    talon_id =talon_inf_obj.search([('view_id','=',obj.id)])
                    if talon_id:
                        for tal in talon_id:
                            self._cr.execute("""
                            SELECT id, view_id, state, doc_date 
                            FROM talon_history 
                            WHERE talon_id = '%s' 
                            ORDER BY id desc limit(1)
                            """%(tal.id))
                            talon_obj  = self._cr.dictfetchall()
                            if talon_obj:
                                tal.write({
                                        'doc_date': talon_obj[0]['doc_date'],
                                        'state': talon_obj[0]['state'],
                                        'view_id': talon_obj[0]['view_id'],
                                        'sale_id': False,
                                        })
                                history = history_obj.browse( talon_obj[0]['id'])
                                history.unlink()
        return super(talon_view, self).unlink()
           
class talon_info(models.Model):
    _name = 'talon.info'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company'
                                  ,default=lambda self: self.env['res.company']._company_default_get('talon.inf'))
    reg_user_id = fields.Many2one('res.users',string='Reg user',default=lambda self: self.env.user)
    name = fields.Char(u'Сер дугаар', size=8)
    type_price = fields.Integer(u'Үнэ')
    view_id = fields.Many2one('talon.view',u'Баримтын дугаар')
    state = fields.Selection([('print', u'Хэвлэгдсэн'),('gtba', u'ГТБА'),('shts', u'ШТС'),('buy', u'Худалдаанд'),('sale', u'Борлуулалт'),('delete', u'Устгагдсан')], u'Төлөв')
    doc_date = fields.Date(u'Огноо')
    history_ids = fields.One2many('talon.history','talon_id',u'Талоны түүх')
    sale_id = fields.Many2one('talon.sale',u'Борлуулалтын дугаар')
    type_id = fields.Many2one('talon.type',u'Талоны төрөл')
    
    _sql_constraints = [
        ('name_uniq', 'unique(name)', u'Сери дугаар давхцахгүй !'),
    ]
         
class talon_inf(models.Model):
    _name = 'talon.inf'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company'
                                  ,default=lambda self: self.env['res.company']._company_default_get('talon.inf'))
    reg_user_id = fields.Many2one('res.users',string='Reg user',default=lambda self: self.env.user)
    name = fields.Char(u'Сер дугаар', size=8)
    type_price = fields.Integer(u'Үнэ')
    view_id = fields.Many2one('talon.view',u'Баримтын дугаар')
    state = fields.Selection([('print', u'Хэвлэгдсэн'),('gtba', u'ГТБА'),('shts', u'ШТС'),('buy', u'Худалдаанд'),('sale', u'Борлуулалт'),('delete', u'Устгагдсан')], u'Төлөв')
    doc_date = fields.Date(u'Огноо')
    history_ids = fields.One2many('talon.history','talon_id',u'Талоны түүх')
    sale_id = fields.Many2one('talon.sale',u'Борлуулалтын дугаар')
    type_id = fields.Many2one('talon.type',u'Талоны төрөл')
    
    
    _sql_constraints = [
        ('name_uniq', 'unique(name)', u'Сери дугаар давхцахгүй !'),
    ]
               


class talon_history(models.Model):
    _name = 'talon.history'
    
    talon_id = fields.Many2one('talon.inf',u'Сер дугаар')
    view_id = fields.Many2one('talon.view',u'Баримтын дугаар')
    state = fields.Selection([('print', u'Хэвлэгдсэн'),('gtba', u'ГТБА'),('shts', u'ШТС'),('buy', u'Худалдаанд'),('sale', u'Борлуулалт'),('delete', u'Устгагдсан')], u'Төлөв')
    doc_date = fields.Date(u'Огноо')
 
class talon_sale(models.Model):
    _name = 'talon.sale'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    
            
    
    def _get_default_warehouse(self):
        return self.env.user.warehouse_id.id
    
    '''def _get_default_date(self):
        
        self._cr.execute("""
              
              select date from talon_sale where user_id = %s
              order by id desc limit(1)
                    
                """%(self._uid))
        sale_obj  = self._cr.dictfetchall()
        if sale_obj:
          return sale_obj[0]['date']'''
    
    name = fields.Char(string=u'Талоны код',required=True,size=8)
    end_code = fields.Char(string=u'Хооронд',size=8)
    type_id = fields.Many2one('talon.type',string=u'Талоны төрөл')
    user_id = fields.Many2one('res.users',string=u'Оруулсан хэрэглэгч',default=lambda self: self.env.user.id)
    date = fields.Date(string=u'Уншуулах огноо',required=True)
    date_sale = fields.Date(string='Зарагдсан огноо')
    partner_id = fields.Many2one('res.partner',string=u'Харилцагч')
    warehouse_id = fields.Many2one('shts.register',string=u'ШТС-ын нэр',default=_get_default_warehouse)
    talon_price = fields.Integer(string=u'Мөнгөн дүн')
    state = fields.Selection([('draft', u'Ноорог'),('confirm', u'Уншуулсан')], string=u'Төлөв',default='draft',readonly=True)
    qty = fields.Integer(string=u'Тоо хэмжээ')
    shift_id = fields.Many2one('shift.working.register',string='Shift')
                

    @api.onchange('qty')   
    def action_total_price(self):
        for line in self:
            price = 0.0
            if line.qty>0.0:
                price = line.qty*line.type_id.type_price
            line.talon_price = price
        
    
    @api.onchange('end_code','name')
    def onchange_action_qty(self):
        total = 1
        for sale in self:
            if sale.name and sale.end_code:
               start = int(sale.name[2:8])
               end = int(sale.end_code[2:8])
               if end < start:
                   raise UserError(_(u"Энэ талоны төгсгөл нь заавал их байх ёстой!!!")) 
               else:
                    total = end - start + total
               
            sale.qty = total

    @api.onchange('name')
    def onchange_code(self):
       if self.name:
          self._cr.execute("""
              
              SELECT t.doc_date, t.type_price,v.type_id,v.partner_id
               FROM
                talon_inf t
                LEFT JOIN talon_view v on (t.view_id = v.id)
              WHERE 
                t.state = 'buy' and t.name = '%s'
                    
                """%(self.name))
          type_obj  = self._cr.dictfetchall()
          if type_obj:
             talon_lost_obj = self.env['talon.lost']
             lost_id =talon_lost_obj.search([('name','=',self.name)])
             if lost_id:
                raise UserError(_(u"Энэ алдагдсан талон байна.")) 
             
             self.date_sale = type_obj[0]['doc_date']
             self.talon_price = type_obj[0]['type_price']
             self.type_id = type_obj[0]['type_id']
             self.partner_id = type_obj[0]['partner_id']
             if self.shift_id:
                self.date = self.shift_id.shift_date
          else:
             talon_inf_obj = self.env['talon.inf']
             talon_ids =talon_inf_obj.search([('name','=',self.name)])
             if talon_ids:
                talon_id = talon_inf_obj.browse(talon_ids.id)
                if talon_id.state == 'sale':
                   raise UserError(_(u" Энэ талон %s дээр уншигдсан байна")% talon_id.sale_id.warehouse_id.name)
                else:
                   raise UserError(_(u"Энэ талон худалдаанд гараагүй байна."))  
             else:
             
              raise UserError(_(u"Энэ талоны орлого бүртгэгдээгүй байна.")) 
    
    def add_zero(self, too):
        if len(str(too)) == 1 :
            oron = '00000' + str(too)
        elif len(str(too)) == 2 :
            oron = '0000' + str(too)
        elif len(str(too)) == 3 :
            oron = '000' + str(too)
        elif len(str(too)) == 4 :
            oron = '00' + str(too)
        elif len(str(too)) == 5 :
            oron = '0' + str(too)
        else :
            oron = too
        return oron
    
    def action_to_sale(self):
        
        obj = self.browse(self.id)
        
        talon_inf_obj = self.env['talon.inf']
        talon_history_obj = self.env['talon.history']
        
        if not  obj.warehouse_id:
            raise UserError(_(u"ШТС-ын нэр талбар хоосон байна. Талон шивэх боломжгүй")) 
        now_date = fields.Date.today()
        date_format = '%Y-%m-%d'
        #date_order = datetime.strptime(now_date, date_format) - timedelta(days=1)
        #date_order1  = date_order.date()
        if obj.date > now_date:
           raise UserError(_(u"Уншуулах огноо талбар буруу оруулсан байна. Өнөөдрийн огнооноос хойш байх ёсгүй.")) 
        #if obj.date < str(date_order1):
        #    raise UserError(_(u"Талон шивэх хоног хэтэрсэн байна. ДАГ-ын хэрэглэгчид хандана уу")) 
        
        
        if obj.name and obj.end_code:
           start = int(obj.name[2:8])
           useg = obj.name[0:2]
           end = int(obj.end_code[2:8])
           useg_end = obj.end_code[0:2]
           if useg != useg_end:
              raise UserError(_(u"Талоны эхлэх үсэгнүүд зөрүүтэй байна.")) 
           if start > end:
              raise UserError(_(u"Талоны төгсгөл нь эхлэлээс их байх ёстой."))
           count = end - start + 1
           i = 0
           for to in range(count):
               series = ''
               start1 = self.add_zero(start+i)
               series = useg + str(start1)
               #talon_id =talon_inf_obj.search(cr, uid, [('state','=','buy'),('name','=',series)])
               self._cr.execute("""
              
              SELECT t.id, t.view_id, t.doc_date
               FROM
                talon_inf t
              WHERE 
                t.state = 'buy' and t.name = '%s'
                    
                """%(series))
               type_obj  = self._cr.dictfetchall()
               if type_obj:
                 talon_lost_obj = self.env['talon.lost']
                 lost_id =talon_lost_obj.search([('name','=',series)])
                 if lost_id:
                    raise UserError(_(u"Энэ алдагдсан талон байна.")) 
                 talon_history_obj.create({
                                'talon_id': type_obj[0]['id'],
                                'view_id': type_obj[0]['view_id'],
                                'state': 'buy',
                                'doc_date': type_obj[0]['doc_date'],
                                
                                } )
                 talon_inf = talon_inf_obj.browse(type_obj[0]['id'])
                 talon_inf.write({
                                
                                'doc_date': obj.date,
                                'state': 'sale',
                                'view_id': False,
                                'sale_id': obj.id
                                } )
                 i += 1
               else:
                raise UserError(_(u"%s талон худалдаанд гараагүй эсвэл уншигдсан байна.") %
                                               (series)) 
                
        else:
         if obj.name:
           #talon_id =talon_inf_obj.search(cr, uid, [('state','=','buy'),('name','=',obj.name)])
           self._cr.execute("""
              
              SELECT t.id, t.view_id, t.doc_date
               FROM
                talon_inf t
              WHERE 
                t.state = 'buy' and t.name = '%s'
                    
                """%(obj.name))
           type_obj  = self._cr.dictfetchall()
           if type_obj:
              talon_lost_obj = self.env['talon.lost']
              lost_id =talon_lost_obj.search([('name','=',obj.name)])
              if lost_id:
                    raise UserError(_(u"Энэ алдагдсан талон байна."))

              talon_history_obj.create({
                                'talon_id': type_obj[0]['id'],
                                'view_id': type_obj[0]['view_id'],
                                'state': 'buy',
                                'doc_date': type_obj[0]['doc_date'],
                                
                                } )
              talon_inf = talon_inf_obj.browse(type_obj[0]['id'])
              talon_inf.write({
                                
                                'doc_date': obj.date,
                                'state': 'sale',
                                'view_id': False,
                                'sale_id': obj.id
                                } )
           else:
                raise UserError(_(u"%s талон худалдаанд гараагүй эсвэл уншигдсан байна.") %
                                               (obj.name)) 
               
            
            
        self.write({'state': 'confirm'}) 
        
    def unlink(self):
        for obj in self:
           talon_inf_obj = self.env['talon.inf']
           talon_history_obj = self.env['talon.history']

           talon_id =talon_inf_obj.search([('sale_id','=',obj.id),('state','=','sale')])
           if talon_id:
              for tal in talon_id:
                  self._cr.execute("""
                  
                  SELECT id, view_id, state, doc_date 
                  FROM talon_history 
                  WHERE talon_id = '%s' 
                  ORDER BY id desc limit(1)
                        
                    """%(tal.id))
                  talon_obj  = self._cr.dictfetchall()
                  if talon_obj:
                   
                    tal.write({
                                    
                                    'doc_date': talon_obj[0]['doc_date'],
                                    'state': talon_obj[0]['state'],
                                    'view_id': talon_obj[0]['view_id'],
                                    'sale_id': False,
                                    } )
                    
                    history_id = talon_history_obj.search([('id','=',talon_obj[0]['id'] )])
                    history_id.unlink()
        return super(talon_sale, self).unlink()
    
class talon_repair_request(models.Model):
    _name = 'talon.repair.request'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company'
                                  ,default=lambda self: self.env['res.company']._company_default_get('talon.request'))
    name = fields.Char(u'Дугаар')
    date = fields.Date(u'Оруулсан огноо')
    user_id = fields.Many2one('res.users',u'Оруулсан хэрэглэгч')
    warehouse_id = fields.Many2one('shts.register',u'Агуулах/ШТС')
    note = fields.Text(u'Тайлбар')
    type = fields.Selection([('change_date', u'Огноо өөрчлөх'),('change_series', u'Сер дугаар буруу оруулсан'),('delete_ser', u'Устгах')], u'Төрөл')
    state = fields.Selection([('draft', u'Ноорог'),('request', u'Хүсэлт илгээгдсэн'),('confirmed', u'Баталгаажсан')], u'Төлөв')
    ser_wrong =  fields.Char(u'Буруу оруулсан сер дугаар', size=8)
    ser_true = fields.Char(u'Зөв сер дугаар', size=8)
    ser_delete = fields.Char(u'Устгах сер дугаар', size=8)
    end_ser_delete = fields.Char(u'Хооронд', size=8)
    date_wrong = fields.Date(u'Буруу оруулсан огноо')
    date_true = fields.Date(u'Зөв огноо')
    user_confirmed = fields.Many2one('res.users',u'Батласан хэрэглэгч')
    date_confirmed = fields.Date(u'Батласан огноо')
    request_line_id = fields.One2many('request.line','talon_request_id')
    is_pull = fields.Boolean(u'Татсан эсэх')
    
class talon_request(models.Model):
    _name = 'talon.request'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company'
                                  ,default=lambda self: self.env['res.company']._company_default_get('talon.request'))
    name = fields.Char(u'Дугаар')
    date = fields.Date(u'Оруулсан огноо')
    user_id = fields.Many2one('res.users',u'Оруулсан хэрэглэгч')
    warehouse_id = fields.Many2one('shts.register',u'Агуулах/ШТС')
    note = fields.Text(u'Тайлбар')
    type = fields.Selection([('change_date', u'Огноо өөрчлөх'),('change_series', u'Сер дугаар буруу оруулсан'),('delete_ser', u'Устгах')], u'Төрөл')
    state = fields.Selection([('draft', u'Ноорог'),('request', u'Хүсэлт илгээгдсэн'),('confirmed', u'Баталгаажсан')], u'Төлөв',default='draft')
    ser_wrong =  fields.Char(u'Буруу оруулсан сер дугаар', size=8)
    ser_true = fields.Char(u'Зөв сер дугаар', size=8)
    ser_delete = fields.Char(u'Устгах сер дугаар', size=8)
    end_ser_delete = fields.Char(u'Хооронд', size=8)
    date_wrong = fields.Date(u'Буруу оруулсан огноо')
    date_true = fields.Date(u'Зөв огноо')
    user_confirmed = fields.Many2one('res.users',u'Батласан хэрэглэгч')
    date_confirmed = fields.Date(u'Батласан огноо')
    request_line_id = fields.One2many('request.line','talon_request_id')
    is_pull = fields.Boolean(u'Татсан эсэх')
                
    def _get_default_warehouse(self, cr, uid, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        return user.warehouse_id.id
    


    @api.model
    def create(self, vals):
        if context is None:
            context = {}
        if vals.get('name','/')=='/':
            vals['name'] = self.env['ir.sequence'].get('talon.request') or '/'
        context = dict(context or {}, mail_create_nolog=True)
        order =  super(talon_request, self).create(vals)
        return order
    
    def action_to_pull(self):
        obj = self
        if not obj.warehouse_id.id:
           raise UserError(_("Агуулах/ШТС талбар хоосон байна."))
        if not obj.date_wrong:
           raise UserError(_("Буруу оруулсан огноо талбар хоосон байна."))
        self._cr.execute("""
              
              SELECT t.id
               FROM
                talon_inf t
                LEFT JOIN talon_sale s on (t.sale_id = s.id)
              WHERE 
                t.state = 'sale' and s.warehouse_id = %s and t.doc_date = '%s'
                    
                """%(obj.warehouse_id.id,obj.date_wrong))
        talon_obj  = self._cr.dictfetchall()
        if talon_obj:
           for talon in talon_obj:
               line_obj = self.env['request.line']
               line_obj.create({
                                    'talon_request_id': obj.id,
                                    'talon_id': talon['id']
                                    
                                    } )
        
        self.write( {'is_pull': True}) 
    
    def action_to_request(self):
        obj = self
        talon_inf_obj = self.env['talon.inf']
        talon_history_obj = self.evn['talon.history']
        if obj.type == 'change_date':
           if obj.is_pull != True:
              raise UserError(_("Талоны мэдээлэл татагдаагүй байна. Талоны мэдээлэл татах товч дарна уу"))  
           if not obj.date_true:
                     raise UserError(_("Зөв огноо талбар хоосон байна. Оруулана уу"))
           if obj.request_line_id:
              for line_id in obj.request_line_id:
                  line_id.talon_id.write({
                                
                                'doc_date': obj.date_true,
                                } )
        elif obj.type =='change_series':
           
             talon = talon_inf_obj.search([('name','=',obj.ser_wrong),('state','=','sale')])
             talon1 = talon_inf_obj.search([('name','=',obj.ser_true),('state','=','buy')])
             if not talon:
                raise UserError(_("Буруу оруулсан сер дугаар талбарыг шалгана уу.")) 
             if not talon1:
                raise UserError(_("Зөв сер дугаар талбарыг шалгана уу."))
        else:
            talon = talon_inf_obj.search( [('name','=',obj.ser_delete),('state','=','sale')])
            if not talon:
                raise UserError(_("Устгах сер дугаар талбарыг шалгана уу.")) 
            
        self.write({'state': 'request'}) 
    
    def action_to_confirm(self):
        obj = self
        talon_inf_obj = self.env['talon.inf']
        talon_history_obj = self.env['talon.history']
        talon_sale_obj = self.env['talon.sale']
        if obj.type == 'change_date':
           if obj.is_pull != True:
              raise UserError(_("Талоны мэдээлэл татагдаагүй байна. Талоны мэдээлэл татах товч дарна уу"))  
           if obj.request_line_id:
              for line_id in obj.request_line_id:
                  if not obj.date_true:
                     raise UserError(_("Зөв огноо талбар хоосон байна. Оруулана уу"))
                  line_id.talon_id.write({
                                
                                'doc_date': obj.date_true,
                                } )
        elif obj.type == 'change_series':
           
             talon = talon_inf_obj.search([('name','=',obj.ser_wrong),('state','=','sale')])
             talon1 = talon_inf_obj.search([('name','=',obj.ser_true),('state','=','buy')])
             if not talon:
                raise UserError(_("Буруу оруулсан сер дугаар талбарыг шалгана уу.")) 
             if not talon1:
                raise UserError(_("Зөв сер дугаар талбарыг шалгана уу."))
             talon_id = talon_inf_obj.browse(cr, uid, talon)
             sale_id = talon_id.sale_id.id
             if not talon_id.sale_id.end_code:
                talon_sale_obj.write(talon_id.sale_id.id,{
                                
                                'name': obj.ser_true,
                                } )

             self._cr.execute("""
              
              SELECT id, view_id, state, doc_date 
              FROM talon_history 
              WHERE talon_id = '%s' 
              ORDER BY id desc limit(1)
                    
                """%(talon[0]))
             talon_obj  = self._cr.dictfetchall()
             if talon_obj:
               
                talon_inf_obj.write(talon[0],{
                                
                                'doc_date': talon_obj[0]['doc_date'],
                                'state': talon_obj[0]['state'],
                                'view_id': talon_obj[0]['view_id'],
                                'sale_id': False,
                                } )
                talon_history_obj.unlink( talon_obj[0]['id'],  )
             
             
             talon_id1 = talon_inf_obj.browse(talon1[0])
             talon_history_obj.create({
                                'talon_id': talon_id1.id,
                                'view_id': talon_id1.view_id.id,
                                'state': talon_id1.state,
                                'doc_date': talon_id1.doc_date,
                                
                                } )
             talon_id1.write({
                                
                                'doc_date': talon_id.doc_date,
                                'state': 'sale',
                                'view_id': False,
                                'sale_id': sale_id,
                                } )
        
        else:
             #talon delete
             if obj.ser_delete and obj.end_ser_delete:
               start = int(obj.ser_delete[2:8])
               useg = obj.ser_delete[0:2]
               end = int(obj.end_ser_delete[2:8])
               useg_end = obj.end_ser_delete[0:2]
               if useg != useg_end:
                  raise UserError(_(u"Талоны эхлэх үсэгнүүд зөрүүтэй байна.")) 
               if start > end:
                  raise UserError(_(u"Талоны төгсгөл нь эхлэлээс их байх ёстой."))
               count = end - start + 1
               i = 0
               sale_obj = self.env['talon.sale']
               for to in range(count):
                 series = ''
                 start1 = sale_obj.add_zero(start+i)
                 series = useg + str(start1)
                 str(series)
                 #talon_id =talon_inf_obj.search(cr, uid, [('state','=','buy'),('name','=',series)])
                 talon = talon_inf_obj.search( [('name','=',series),('state','=','sale')])
                 if not talon:
                    raise UserError(_("Устгах сер дугаар талбарыг шалгана уу.")) 
                 
                 self._cr.execute("""
                  
                  SELECT id, view_id, state, doc_date 
                  FROM talon_history 
                  WHERE talon_id = '%s' 
                  ORDER BY id desc limit(1)
                        
                    """%(talon[0]))
                 talon_obj  = self._cr.dictfetchall()
                 if talon_obj:
                    talon_inf_obj.write(talon[0],{
                                    
                                    'doc_date': talon_obj[0]['doc_date'],
                                    'state': talon_obj[0]['state'],
                                    'view_id': talon_obj[0]['view_id'],
                                    'sale_id': False,
                                    } )
                    talon_history_obj.unlink(talon_obj[0]['id'],  )
                 i += 1
             else:
                 
              talon = talon_inf_obj.search([('name','=',obj.ser_delete),('state','=','sale')])
             if not talon:
                raise UserError(_("Устгах сер дугаар талбарыг шалгана уу.")) 
             
             self._cr.execute("""
              
              SELECT id, view_id, state, doc_date 
              FROM talon_history 
              WHERE talon_id = '%s' 
              ORDER BY id desc limit(1)
                    
                """%(talon[0]))
             talon_obj  = self._cr.dictfetchall()
             if talon_obj:
                talon_inf_obj.write(talon[0],{
                                
                                'doc_date': talon_obj[0]['doc_date'],
                                'state': talon_obj[0]['state'],
                                'view_id': talon_obj[0]['view_id'],
                                'sale_id': False,
                                } )
                talon_history_obj.unlink( talon_obj[0]['id'],  )    
                    
        self.write( {'state': 'confirmed','user_confirmed': uid, 'date_confirmed':datetime.now().strftime("%Y-%m-%d")}) 
    
    def unlink(self):
        if self.state == 'confirmed':
           raise UserError(_("Батлагдсан төлөвтэй баримтыг устгах боломжгүй."))
        return super(talon_request, self).unlink()
    
class request_line(models.Model):
    _name = 'request.line'
    
    
    name = fields.Char(u'Талоны мэдээлэл', size=8)
    talon_request_id = fields.Many2one('talon.request',u'Хүсэлт')
    talon_id = fields.Many2one('talon.inf',u'Талоны мэдээлэл')
                
                
 

class talon_lost(models.Model):
    _name = 'talon.lost'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    company_id = fields.Many2one('res.company',string='Company'
                                  ,default=lambda self: self.env['res.company']._company_default_get('talon.lost'))
    name = fields.Char(u'Талон код',size=8)
    user_id = fields.Many2one('res.users',u'Оруулсан хэрэглэгч')
    partner_id = fields.Many2one('res.partner',u'Харилцагч')
    date = fields.Date(u'Оруулсан огноо')
    note = fields.Text(u'Тайлбар')


    

    def onchange_ser(self):
       if self.name:
          self._cr.execute("""
              
              SELECT t.doc_date, v.partner_id
               FROM
                talon_inf t
                LEFT JOIN talon_view v on (t.view_id = v.id)
              WHERE 
                t.state = 'buy' and t.name = '%s'
                    
                """%(name))
          type_obj  = self._cr.dictfetchall()
          if type_obj:
             self.write({'partner_id':type_obj[0]['partner_id']})
          else:
             raise UserError(_(u"Энэ талон худалдаанд гараагүй эсвэл уншигдсан байна.")) 
    

    
class ResCompany(models.Model):
    _inherit = 'res.company'
    
    company_code = fields.Char(string=u'Компани код')
    ugtwar = fields.Char(string=u'Угтвар')
    
class ResPartner(models.Model):
     _inherit = 'res.partner'

     code = fields.Char(string=u'Харилцагч код',copy=False)
     avlaga = fields.Float(string=u'Авлага')
     avlaga_zoruu = fields.Float(string=u'Авлага зөрүү')
     discount = fields.Float(string='Харилцагчийн хөнгөлөлт')
     warehouse_id = fields.Many2one('shts.register',u'Талон хэрэглэгч - Агуулах/ШТС')
     shts_ids = fields.Many2many('shts.register', string=u'Харах ШТС/Агуулах')
     shts_id = fields.Many2one('shts.register',u'ШТС')
     user_status = fields.Selection([('cass', u'Касс'),('gtba', u'ГТБА'),('shts', u'ШТС')], u'Хэрэглэгчийн статус')
     card_number = fields.Char(string=u'Картын дугаар')
     huis = fields.Selection([('male',u'Эрэгтэй'),
                             ('female',u'Эмэгтэй')],string=u'Хүйс')
     register_date = fields.Date(string=u'Бүртгүүлсэн огноо', default=fields.Date.context_today)
     privilege = fields.Selection([('manager',u'Эрхлэгч'),
                                   ('work',u'Түгээгч')],string=u'Эрх')
     dep_id = fields.Many2one('hr.department',string=u'Газар/нэгж')
     region_id = fields.Many2one('shts.region',string=u'Бүс')
     drive_true = fields.Boolean(string=u'Жолооч эсэх')
     tin_code = fields.Char(string=u'Тин код')


     _sql_constraints = [
        ('code_uniq', 'unique (code)', u'Харилцагчийн код давхардахгүй!')
    ]
     
     
     
    
    
     def get_tin_code(self):
        if self.vat:
            url = "https://api.ebarimt.mn/api/info/check/getTinInfo?regNo="+self.vat
            header = {
                    "Content-Type":"Application/json",                
                    }
            return_data = requests.request("get", url, headers = header)
            if return_data.status_code == 200:
                json_data = return_data.json()
                self.tin_code = json_data['data']
            else:
                raise UserError(_(u'Татвар төлөгчийн дугаар зөв оруулсан эсэхээ  шалгана уу.'))
        else:
            raise UserError(_(u'Татвар төлөгчийн дугаар зөв оруулсан эсэхээ  шалгана уу.'))
     
     
     def action_partner_search(self, vat):
         partner_name = False
         partner_id = False
         company_id = False
         count = 0
         if vat:
             partner = self.search([('vat','=',vat)],limit=1)
             if partner:
                 partner_name = partner.name
                 partner_id = partner.id
                 
                 count = 1
             else:
                vat_number = u'%s'%vat
                url="http://info.ebarimt.mn/rest/merchant/info?regno="+vat_number
                try:
                    r = requests.get(url)
                    n=r.json()
                    deletearg = n['name'].replace("\n","")
                    partner_name=deletearg
                    count = 1
                except Exception:
                    partner_name=False
         if count == 0:
             return {
                    "count":0,
                    "result":{}
                 }
         else:
             return {
                    "count":count,
                    "result":[
                            {
                            "id":partner_id,
                            "name":partner_name,
                            "company_id":company_id
                                }
                        ]
                 }
    
     def action_partner_tin_search(self, vat):
         partner_tin = False
         partner_id = False
         company_id = False
         partner_name = False
         count = 0
         countt = 0
         if vat:
             partner = self.search([('vat','=',vat)],limit=1)
             if partner.id != False:
                 if partner.tin_code!=False:
                     partner_tin = partner.tin_code
                     partner_id = partner.id
                     partner_name = partner.name
                     count = 1
                 else:
                     urls = "https://api.ebarimt.mn/api/info/check/getTinInfo?regNo="+'%s'%vat
                     header = {
                            "Content-Type":"Application/json",                
                            }
                     return_datas = requests.request("get", urls, headers = header)
                     if return_datas.status_code == 200:
                        json_datas = return_datas.json()
                        partner_tin = '%s'%json_datas['data']
                        partner_name = partner.name
                        partner_id = partner.id
                     else:
                        partner_tin = False
                     count = 1
             else:
                url = "https://api.ebarimt.mn/api/info/check/getTinInfo?regNo="+'%s'%vat
                header = {
                        "Content-Type":"Application/json",                
                        }
                return_data = requests.request("get", url, headers = header)
                if return_data.status_code == 200:
                    json_data = return_data.json()
                    partner_tin = '%s'%json_data['data']
                else:
                    partner_tin = False
                    
                vat_tin = partner_tin
                urll="https://api.ebarimt.mn/api/info/check/getInfo?tin="+'%s'%vat_tin
                try:
                    r = requests.get(urll)
                    n=r.json()
                    deletearg = n['data']['name']
                    partner_name = deletearg
                    count = 1
                except Exception:
                    partner_name=False
                                
                        
         if count != 0:
             return {
                    "count":count,
                    "result":[
                            {
                            "id":partner_id,
                            "name":partner_name,
                            "partner_tin": partner_tin
                                }
                        ]
                 } 
         else:
             return {
                    "count":0,
                    "result":{}
                 }       
     
     def _get_name(self):
        """ Utility method to allow name_get to be overrided without re-browse the partner """
        partner = self
        name = partner.name or ''

        if partner.company_name or partner.parent_id:
            if not name and partner.type in ['invoice', 'delivery', 'other']:
                name = dict(self.fields_get(['type'])['type']['selection'])[partner.type]
            if not partner.is_company:
                name = self._get_contact_name(partner, name)
        if self._context.get('show_address_only'):
            name = partner._display_address(without_company=True)
        if self._context.get('show_address'):
            name = name + "\n" + partner._display_address(without_company=True)
        name = name.replace('\n\n', '\n')
        name = name.replace('\n\n', '\n')
        if self._context.get('address_inline'):
            name = name.replace('\n', ', ')
        if self._context.get('show_email') and partner.email:
            name = "%s <%s>" % (name, partner.email)
        if self._context.get('html_format'):
            name = name.replace('\n', '<br/>')
        return name

     def name_get(self):
        res = []
        for partner in self:
            name = partner._get_name()
            res.append((partner.id, name))
        return res
     @api.model
     def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        if not self.env.su and args:
            domain_fields = {term[0] for term in args if isinstance(term, (tuple, list))}
            if domain_fields.intersection(USER_PRIVATE_FIELDS):
                raise AccessError(_('Invalid search criterion'))
        return super(ResPartner, self)._search(args, offset=offset, limit=limit, order=order, count=count,
                                          access_rights_uid=access_rights_uid)
    
     @api.model
     def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        partner_ids = []
        if operator not in expression.NEGATIVE_TERM_OPERATORS:
            if operator == 'ilike' and not (name or '').strip():
                domain = [('name', operator, name)]
            else:
                domain = [('name', operator, name)]
            partner_ids = self._search(expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid)
        if not partner_ids:
            partner_ids = self._search(expression.AND([[('name', operator, name),('vat',operator,name)], args]), limit=limit, access_rights_uid=name_get_uid)
        return models.lazy_name_get(self.browse(partner_ids).with_user(name_get_uid))
    
    
