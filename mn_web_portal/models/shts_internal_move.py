# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import date

class ShtsInternalMove(models.Model):
    _name = 'shts.internal.move'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('shts.internal.move'),required=True)
    name = fields.Char(string='Domestic number')
    portfolio = fields.Many2one('product.product',string='Portfolio')
    domestic_litr = fields.Float(string='Domestic litr')
    domestic_kg = fields.Float(string='Domestic kg')
    ratio = fields.Float(string='Ratio')
    can = fields.Many2one('can.register',string='Can')
    temperature = fields.Float(string='Temperature')
    temp_ratio = fields.Float(string='Temp ratio')
    car_number = fields.Many2one('car.register',string='Car number')
    driver = fields.Many2one('hr.employee',string='Driver')
    shts = fields.Many2one('shts.regiser',string='Shts',default=lambda self: self.env.user.warehouse_id.id)
    shts_to_make = fields.Many2one('shts.register',string='Shts to make')
    user_reg_id = fields.Many2one('res.users',string='User reg id',default=lambda self: self.env.user)
    date = fields.Date(string='Date',default=lambda self: fields.datetime.now())
    side = fields.Float(string='Side')
    state = fields.Selection([('draft','Draft'),
                              ('expense','Expense'),
                              ('done','Done')],string='State',default='draft')