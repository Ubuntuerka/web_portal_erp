# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from odoo import models, fields, api, _
from datetime import date, timedelta, datetime

DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


class CenterPaymentIncome(models.Model):
    _name = 'center.payment.income'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'card_code'

    #shift_date = fields.Datetime(string=u'Ээлжийн огноо', required=True, default=fields.Date.context_today)

    # name = fields.Char(string='Padaan №',required=True)
    # product_id = fields.Many2one('product.product',string='Бүтээгдэхүүн',required=True)
    # name = fields.Char(string='Дугаар')
    amount = fields.Float(string='Дүн', required=True)
    request_user_id = fields.Char(string='Request job user', required=True)
    car_number = fields.Char(string='Машины дугаар')
    card_number = fields.Char(string='Картын дугаар')
    card_code = fields.Char(string='Дугаар',
                            default=lambda self: self.env['ir.sequence'].next_by_code('center.payment.income'))
    card_name = fields.Char(string='Карт дээрх нэр')
    # partner = fields.Char(string='Partner')
    shts_id = fields.Many2one('shts.register', u'Хүсэлт гаргасан газар', required=True,
                              default=lambda self: self.env.user.warehouse_id.id)
    reason = fields.Selection([('pos_not', u'Посгүй'),
                               ('server', u'Сүлжээ унасан'),
                               ('gemtsen', u'Карт гэмтсэн'),
                               ('paymenting', u'Эргэн төлөлт'),
                               ('forget', u'Пин код мартсан'),
                               ('noting', u'Пос машин ажиллахгүй'),
                               ('order', u'Бусад')], string='Шалтгаан', required=True)
    # time = fields.Char(string='Time')
    desc = fields.Char(string='Тайлбар')
    #read_user_id = fields.Many2one('hr.employee', string='Уншуулсан ажилтан', required=True)
    read_user_ids = fields.Many2one('res.users', 'Бүртгэсэн ажилтан', default=lambda self: self.env.user, required=True)
    bank_name = fields.Selection([('shu_card', u'Шунхлай карт'),
                                  ('hh_bank', u'ХХ банк'),
                                  ('haan_bank', u'Хаан банк'),
                                  ('has_bank', u'Хас банк'),
                                  ('golomt_bank', u'Голомт банк'),
                                  ('tur_bank', u'Төрийн банк'),
                                  ('OU_bank', u'ОУ-ын карт'),
                                  ('capital_bank', u'Капитал банк'),
                                  ('UB_bank', u'Улаанбаатар банк'),
                                  ('order_bank', u'Бусад банк')], string='Банкны нэр', required=True)
    type = fields.Selection([('card', u'Төв карт'),
                             ('mobile', u'Мобайл банк')], string='Type', required=True, default='card')
    shift_id = fields.Many2one('shift.working.register', string='Shift register')

