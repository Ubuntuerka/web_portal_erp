# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date

class ExpenseBonusRegister(models.Model):
    _name = 'expense.bonus.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env['res.company']._company_default_get('expense.bonus.register'),required=True)
    name = fields.Char(string='Sequence number')
    reg_user_id = fields.Many2one('res.users',string='Regiser user',default=lambda self: self.env.user,required=True)
    date = fields.Date(string='Date', default=fields.Date.context_today,required=True)
    state = fields.Selection([('draft','Draft'),
                              ('done','Done')],string='State',default='draft')
    line_ids = fields.One2many('expense.bonus.register.line','expense_id',string='Expense line')
    
    
    def action_done(self):
        self.write({'state':'done'})
    
    

class ExpenseBonusRegisterLine(models.Model):
    _name = 'expense.bonus.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env['res.company']._company_default_get('expense.bonus.register.line'))
    expense_id = fields.Many2one('expense.bonus.register',string='Expense bonus')
    type = fields.Char(string='Type',required=True)
    desc = fields.Char(string='Description')
    date = fields.Date(string='Date', default=fields.Date.context_today,required=True)
    document_number = fields.Char('Document number',required=True)
    car_number_id = fields.Many2one('car.register','Car number',required=True)
    drive_id = fields.Many2one('res.users',string='Drive',required=True)
    product_id = fields.Many2one('product.product',string='Product',required=True)
    uom_id = fields.Many2one('uom.uom',string='Uom',required=True)
    qty = fields.Float(string='Quantity',required=True)
    shts_id = fields.Many2one('shts.register',string='Shts',required=True)
    shift_id = fields.Many2one('shift.working.register',string='Shift register')
    
    @api.onchange('product_id')
    def product_onchange(self):
        for line in self:
            line.uom_id = line.product_id.uom_id
            
        
        
        
    