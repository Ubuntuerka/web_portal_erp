# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date, datetime, timedelta, time
from odoo.exceptions import RedirectWarning, UserError, ValidationError
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

class FreeTimeType(models.Model):
    _name = 'free.time.type'

    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']


    name = fields.Char(string=u'Сул зосголтын төрөл', required=True)
    is_elec = fields.Boolean(string=u'Цахилгаан тасарсан эсэх')
    #time_type_ids = fields.One2many('shts.free.time.register','free_time_ids')
    
class FTSubtype(models.Model):
    _name = 'ft_subtype'

    name = fields.Char(string=u'Дэд төрөл')
    free_time_ids = fields.Many2one('free.time.type',string='Сул золсолтын төрөл')


class ShtsFreeTimeRegister(models.Model):
    _name = 'shts.free.time.register'

    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'name desc'

    state = fields.Selection([('draft', u'Ноорог'),
                              ('send', u'Илгээсэн'),
                              ('confirm', u'Хаагдсан')], string=u'Төлөв', default='draft', tracking=True)

    name = fields.Char(string='Хүсэлтийн дугаар', readonly=True,default=lambda self: self.env['ir.sequence'].next_by_code('shts.free.time.register'))
    company_id = fields.Many2one('res.company', string='Компани')
    shts_id = fields.Many2one('shts.register', string='ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id)
    free_time_ids = fields.Many2one('free.time.type',string='Сул золсолтын төрөл', required=True)
    subtype_id = fields.Many2one('ft_subtype',string='Дэд төрөл')
    start_date = fields.Datetime(string=u'Эхлэх огноо', required=True)
    end_date = fields.Datetime(string=u'Дуусах огноо')
    total_time = fields.Float(string='Сул зогсолтын  цаг')
    reg_date = fields.Datetime(string=u'Бүртгэсэн огноо', required=True, default=lambda self: fields.datetime.now(),
                               readonly=True)
    explanation = fields.Char(string=u'Тайлбар')
    electric_generator = fields.Boolean( string=u'Цахилгаан үүсгүүр залгасан эсэх')
    electric_start_date = fields.Datetime(string=u'Цахилгаан үүсгүүр залгасан огноо')
    electric_end_date = fields.Datetime(string=u'Цахилгаан үүсгүүр салгасан огноо')
    electric_time = fields.Float(string='Цахилгаан үүсгүүр ашигласан цаг')
    sales_rank = fields.Selection([('1',u'1-р зэрэглэл'),
                               ('2',u'2-р зэрэглэл'),
                               ('3',u'3-р зэрэглэл')],string=u'Борлуулалтын зэрэглэл')
    create_user_ids = fields.Many2one('res.users', 'Бүртгэсэн ажилтан', default=lambda self: self.env.user,
                                      required=True, readonly=True)
    reg_date = fields.Datetime(string=u'Бүртгэсэн огноо', required=True, default=lambda self: fields.datetime.now(),
                               readonly=True)
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн',domain="[('prod_type','=','gasol')]")
    
    @api.onchange('shts_id')
    def get_sales_rank_view(self):
        for line in self:
            line.sales_rank = line.shts_id.sales_rank
            
    @api.onchange('free_time_ids','electric_time','electric_start_date','electric_end_date', 'electric_time')
    def get_elec_time(self):
        
        for line in self:
            line.company_id = line.shts_id.company_id.id
            line.electric_generator = line.free_time_ids.is_elec
            if line.electric_start_date and line.electric_end_date:
                if line.electric_start_date < line.electric_end_date:
                    time1 = datetime.strptime(str(line.electric_start_date), DATETIME_FORMAT)
                    time2 = datetime.strptime(str(line.electric_end_date), DATETIME_FORMAT)
                    hoursDift = float((time2 - time1).seconds) / 3600
                    daysDift = float((time2 - time1).days) * 24
                    line.electric_time = daysDift + hoursDift
                # print('sssssssssssss',line.electric_time)
                else :
                    if line.electric_start_date > line.electric_end_date:
                     raise UserError(u'Цахилгаан үүсгүүр салгасан огноо нь Цахилгаан үүсгүүр залгасан огнооноос хойш байх бөгөөд талбараа шалгана уу!')
    @api.onchange('start_date','end_date','total_time')
    def get_total_time(self):
        for line in self:
            if line.start_date and line.end_date:
                if line.start_date < line.end_date:
                    d1 = datetime.strptime(str(line.start_date), DATETIME_FORMAT)
                    d2 = datetime.strptime(str(line.end_date), DATETIME_FORMAT)
                    hoursDiff = float((d2 - d1).seconds) / 3600
                    daysDiff = float((d2 - d1).days) * 24
        
                    line.total_time = daysDiff+ hoursDiff
                else :
                    if line.start_date > line.end_date:
                        raise UserError('Дуусах огноо нь эхлэх огнооноос хойш байх ёстой. \n Эхлэх огноо, Дуусах огноо талбараа шалгана уу!')
            

    def action_draft(self):

        self.write({'state': 'draft'})

    def action_send(self):
        
        self.write({'state': 'send'})

    def action_return(self):
        self.write({'state': 'send'})

    def action_confirm(self):
        
        for line in self:
            if line.end_date:
               self.write({'state': 'confirm'})
            else:
              raise UserError('Дуусах огноо талбарт утга оруулна уу.')
            
            if line.electric_start_date:
                if not line.electric_end_date:
                    raise UserError('Цахилгаан үүсгүүр салгасан огноо талбарт утга оруулна уу.')

        
  
        
        





