# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree
import logging
_logger = logging.getLogger(__name__)

DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"



class RequiredWorkplaceRegister(models.Model):
    _name = 'required.workplace.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'name desc'

    state = fields.Selection([('draft', u'Ноорог'),
                              ('send', u'Илгээсэн'),
                              ('resolved', u'Шийдвэрлэсэн'),
                              ('confirm', u'Батлагдсан'),
                              ('return', u'Буцаагдсан')], string=u'Төлөв', default='draft', tracking=True)
    name = fields.Char(string='Хүсэлтийн дугаар', readonly=True,
                       default=lambda self: self.env['ir.sequence'].next_by_code('required.workplace.register'))

    create_user_ids = fields.Many2one('res.users', 'Бүртгэсэн ажилтан', default=lambda self: self.env.user,
                                      required=True, readonly=True)
    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    reg_date = fields.Datetime(string=u'Бүртгэсэн огноо', required=True, default=lambda self: fields.datetime.now(),
                               readonly=True)
    shts_id = fields.Many2one('shts.register', string='ШТС', required=True,
                              default=lambda self: self.env.user.shts_ids)
    convert_user_id = fields.Many2one('res.users', string=u'Хуваарилсан ажилтан', readonly=True)
    shift_date = fields.Date(string=u'Ээлжийн огноо', required=True, default=fields.Date.context_today)
    convert_date = fields.Datetime(string=u'Хуваарилсан огноо', readonly=True)
    shift = fields.Selection([('1', u'1'),
                              ('2', u'2')], string=u'Ээлж', required=True)
    decide_user_id = fields.Many2one('res.users', string=u'Шийдвэрлэх ажилтан', required=True)
    resolved_user_id = fields.Many2one('res.users', string=u'Шийдвэрлэсэн', readonly=True)
    shift_name = fields.Many2one('shift.working.register', string='Ээлжийн код', readonly=True)
    decide_date = fields.Datetime(string=u'Шийдвэрлэсэн огноо', readonly=True)
    basic_name = fields.Many2one('basic.type.register', string=u'Үндсэн төрөл', required=True)
    subspecies_name = fields.Many2one('subspecies.type.register', string=u'Дэд төрөл', required=True)
    confirm_user_id = fields.Many2one('res.users', string=u'Батласан ажилтан', readonly=True)
    confirm_date = fields.Datetime(string=u'Баталсан огноо', readonly=True)
    excuse = fields.Char(string=u'Шалтгаан', required=True)
    explanation = fields.Char(string=u'Тайлбар')
    report_office = fields.Many2one('res.users', string=u'Мэдээлэл хүргэх ажилтан',
                                    write=['mn_web_portal.group_web_portal_zahialgat_ajil,mn_web_portal.group_web_portal_zahialgat_ajil_udirdlaga,mn_web_portal.group_web_portal_zahialgat_ajil_admin'])

    # basic_name = fields.Many2one('basic.type.register', string='Үндсэн төрөл')
    # sub_name = fields.Many2one('subspecies.type.register', string='Дэд төрөл')

    def write(self, vals):
        required = self.env['res.users'].has_group('mn_web_portal.group_web_portal_zahialgat_ajil')
        required_register = super(RequiredWorkplaceRegister, self).write(vals)
        if 'explanation' in vals:
            if not required:
                raise ValidationError(_('Шийдвэрлэх ажилтан засна!'))
        return required_register

    def action_draft(self):
        self.write({'state': 'draft'})
    # notification илгээж загвар
    def action_send(self):
        shift_obj = self.env['shift.working.register']
        for record in self:
            shift = shift_obj.search(
                [
                    ('company_id', '=', record.company_id.id),
                    ('shts_id', '=', record.shts_id.id),
                    ('shift_date', '=', record.shift_date),
                    ('shift', '=', record.shift),
                ],
                order='name',
                limit=1  # Нэг бичлэг буцаана
            )
            # Ээлжийн бичлэг олдвол хадгалах
            if shift:
                record.shift_name = shift.id
            # Төлөвийг илгээсэн болгон өөрчлөх
            record.write({'state': 'send'})
            # Мэдэгдэл илгээх
            if record.decide_user_id.partner_id:
                record.send_notification(
                    partner_id=record.decide_user_id.partner_id.id,
                    message=f"Танд {record.name} дугаартай захиалгат ажил ирлээ.",
                )

    # Notification илгээх функц
    def send_notification(self, partner_id, message):
        self.env['bus.bus'].sendone(
            (self._cr.dbname, 'res.partner', partner_id),
            {'type': 'simple_notification', 'title': 'Захиалгат ажил', 'message': message}
        )

        #
    #         # Schedule a follow-up action after 1 minute to notify the decision-maker
    #         follow_up_time = fields.Datetime.now() + timedelta(minutes=1)
    #         self._schedule_follow_up_notification(line, follow_up_time)
    #
    #         # Create message content
    #         message = _("Your request '%s' has been sent for processing.") % (line.name)
    #
    #         # Send notification if partner exists
    #         if line.decide_user_id and line.decide_user_id.partner_id:
    #             line.message_post(
    #                 body=message,
    #                 subject=_("Request Sent Notification"),
    #                 message_type='notification',
    #                 partner_ids=[line.decide_user_id.partner_id.id]
    #             )
    #         else:
    #             _logger.warning("Decision maker's partner_id is missing.")
    #
    #         self.write({'state': 'send'})
    #
    # def _schedule_follow_up_notification(self, line, notify_at):
    #     # Create a scheduled action (ir.cron) to send the follow-up notification
    #     cron = self.env['ir.cron'].create({
    #         'name': 'Follow-up Notification for Request',
    #         'model_id': self.env['ir.model']._get('required.workplace.register').id,
    #         'state': 'code',
    #         'code': f"model._send_reminder_notification({line.id})",
    #         'active': True,
    #         'nextcall': notify_at,  # Trigger the notification 1 minute after
    #     })
    #
    # def _send_reminder_notification(self, line_id):
    #     line = self.browse(line_id)
    #     message = _("This is a reminder for the request '%s'.") % (line.name)
    #
    #     line.message_post(
    #         body=message,
    #         subject=_("Reminder for Request Processing"),
    #         message_type='notification',
    #         partner_ids=[line.decide_user_id.partner_id.id]  # Send reminder to the decision-maker
    #     )
            # Send the initial notification

            # message = _("Your request '%s' has been sent for processing.") % (line.name)
            # line.message_post(
            #     body=message,
            #     subject=_("Request Sent Notification"),
            #     message_type='notification',
            #     partner_ids=[line.decide_user_id.partner_id.id]  # Send to the decision-maker
            # )

    # def _schedule_follow_up_notification(self, line, notify_at):
    #     # Create a scheduled action (ir.cron) to send the follow-up notification
    #     cron = self.env['ir.cron'].create({
    #         'name': 'Follow-up Notification for Request',
    #         'model_id': self.env['ir.model']._get('required.workplace.register').id,
    #         'state': 'code',
    #         'code': f"model._send_reminder_notification({line.id})",
    #         'active': True,
    #         'nextcall': notify_at,  # Trigger the notification 1 minute after
    #     })
    #
    # def _send_reminder_notification(self, line_id):
    #     line = self.browse(line_id)
    #     message = _("This is a reminder for the request '%s'.") % (line.name)
    #
    #     line.message_post(
    #         body=message,
    #         subject=_("Reminder for Request Processing"),
    #         message_type='notification',
    #         partner_ids=[line.decide_user_id.partner_id.id]  # Send reminder to the decision-maker
    #     )
            # message = _("Your request '%s' has been sent for processing.") % (line.name)
            # line.message_post(
            #     body=message,
            #     subject=_("Request Sent Notification"),
            #     message_type='notification',
            #     partner_ids=[line.decide_user_id.partner_id.id]  # Мэдэгдэл хүлээн авах хэрэглэгч
            # )
            # self.env['bus.bus'].sendone(
            #     'simple_notification',  # Суваг нэр
            #     {
            #         'title': line.name,  # Гарчиг
            #         'message': message,  # Мэдэгдэлд харуулах мессеж
            #         'partner_id': line.decide_user_id.partner_id.id,  # Хүлээн авагч
            #         'type': 'info',  # Notification төрөл: success, info, warning, danger
            #     }
            # )
            # else:
            #    raise UserError(u"Таны сонгосон ээлж үүсээгүй тул та ээлжээ шалгана уу.")

            # partner_list = [line.decide_user_id.partner_id.id]
            # mail_dict = {}

            # current_login_user = self.env.user
            # email_subject = u"Захиалгат ажил бүртгэгдлээ"
            # email_description = u" Сайн байна уу?\n %s танд Захиалгат ажил бүртгэгдлээ. Хүсэлтийн дугаар: %s \n Салбар: %s \n Үндсэн төрөл: %s \n Төлөв: Илгээсэн \n Хүндэтгэсэн, Гүүр вэб портал " % (
            # self.decide_user_id.name, self.name, self.shts_id.name, self.basic_name.name)
            #
            # if partner_list:
            #     mail_dict = {
            #         'subject': email_subject,
            #         'email_from': self.create_user_ids.email,
            #         'recipient_ids': [(6, 0, partner_list)],
            #         'body_html': email_description,
            #     }
            #     if mail_dict:
            #         mail_id = current_login_user.env['mail.mail'].create(mail_dict)
            #     if mail_id:
            #         mail_id.send()
        self.write({'state': 'send'})

    def action_resolved(self):
        self.write({'state': 'resolved', 'resolved_user_id': self.env.user.id, 'decide_date': fields.datetime.now()})

    def action_confirm(self):
        self.write({'state': 'confirm', 'confirm_user_id': self.env.user.id, 'confirm_date': fields.datetime.now()})

    def action_return(self):
        self.write({'state': 'return'})


class BasicTypeRegister(models.Model):
    _name = 'basic.type.register'

    name = fields.Char('Үндсэн нэр')


class SubspeciesTypeRegister(models.Model):
    _name = 'subspecies.type.register'

    name = fields.Char('Дэд нэр')
    basic_name = fields.Many2one('basic.type.register', string='Үндсэн нэр')

