# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date, datetime, timedelta
from odoo.exceptions import RedirectWarning, UserError, ValidationError
DATE_FORMAT = "%Y-%m-%d"


class ReportDateReport(models.Model):
    _name = 'report.date.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    company_id = fields.Many2one('res.company', 'Company', required=True,
                                 default=lambda self: self.env['res.company']._company_default_get('report.date.register'))
    report_name = fields.Char(string=u'Тайлант хугацааны нэр', required=True)
    start_date = fields.Date(string=u'Тайлан эхлэх огноо', required=True, default=fields.Date.context_today)
    end_date = fields.Date(string=u'Тайлан дуусах огноо', required=True, default=fields.Date.context_today)
    inventory_date = fields.Date(string=u'Тооллого хийх огноо')
    report_date = fields.Integer(string='Тайлангийн хоног')

    ############################# Өдөр хасах арга  #################################

    @api.onchange('start_date', 'end_date', 'report_date')
    def get_report_date(self):
        if self.start_date and self.end_date:
            d1 = datetime.strptime(str(self.start_date), DATE_FORMAT)
            d2 = datetime.strptime(str(self.end_date), DATE_FORMAT) + timedelta(hours=24)
            d3 = d2 - d1
            self.report_date = str(d3.days)

    @api.onchange('end_date')
    def get_inventory_date(self):
        if self.end_date:
            date1 = datetime.strptime(str(self.end_date), DATE_FORMAT) + timedelta(hours=24)
            self.inventory_date = date1





