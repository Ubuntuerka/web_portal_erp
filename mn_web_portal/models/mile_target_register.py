# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class MileTargetRegister(models.Model):
    _name = 'mile.target.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('mile.target.register'),required=True)
    active = fields.Boolean(string='Active',default=True)
    shts_id = fields.Many2one('shts.register',string=u'ШТС',related='can_id.shts_id')
    can_id = fields.Many2one('can.register',string='Can number',domain="[('is_oil_can','=',False)]")
    can_code = fields.Char(string=u'Савны код',compute="action_can_code")
    product_id = fields.Many2one('product.product',string='Product')
    name = fields.Char(string='Break')
    first_mile = fields.Float(string='First mile')
    end_mile = fields.Float(string='End mile',required=True)
    diff_mile = fields.Float(string='Diff mile')
    zalin = fields.Float(string='Zalin')
    expense_litr = fields.Float(string='Expense litr',compute="action_compute_expense_litr")
    expense_kg = fields.Float(string='Expense kg',compute="action_compute_expense_kg")
       
    
    def action_can_code(self):
        for line in self:
            if line.can_id:
                line.can_code = line.can_id.code
            
            
class MileTargetRegisterLine(models.Model):
    _name = 'mile.target.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env['res.company']._company_default_get('mile.target.register'))
    active = fields.Boolean(string='Active',default=True)
    shts_id = fields.Many2one('shts.register',string=u'ШТС',related='can_id.shts_id')
    can_id = fields.Many2one('can.register',string='Can number',domain="[('is_oil_can','=',False)]")
    product_id = fields.Many2one('product.product',string='Product')
    name = fields.Char(string='Break')
    first_mile = fields.Float(string='First mile',digits=(12, 2))
    end_mile = fields.Float(string='End mile')
    diff_mile = fields.Float(string='Diff mile',compute="calc_diff_mile")
    zalin = fields.Float(string='Zalin')
    expense_litr = fields.Float(string='Expense litr',compute="action_compute_expense_litr")
    expense_kg = fields.Float(string='Expense kg',compute="action_compute_expense_kg",digits=(10, 2))
    shift_id = fields.Many2one('shift.working.register',string='Shift register',digits=(10, 2))
            
    @api.onchange('end_mile','zalin')
    def onchange_end_mile(self):
        for line in self:
            if line.end_mile > 0.0:
                if round(line.end_mile,2) >= round(line.first_mile,2):
                    line.calc_diff_mile()
                    line.action_compute_expense_litr()
                    line.action_compute_expense_kg()
                else:
                    raise UserError(u"Эцсийн милл эхний миллээс бага байж болохгүйг анхаарна уу.")
            

    def calc_diff_mile(self):
        for line in self:
            diff = 0.0
            if line.end_mile>0.0:
                if line.end_mile > line.first_mile:
                        diff = line.end_mile - line.first_mile
            line.diff_mile = diff
    
    def action_compute_expense_litr(self):
        for line in self:
            expense_litr = 0.0
            if line.end_mile >0.0 and line.zalin>0.0:
                expense_litr = line.end_mile - line.first_mile-line.zalin
            elif line.end_mile >0.0 and line.zalin==0.0:
                expense_litr = line.end_mile - line.first_mile
            line.expense_litr = expense_litr
        
    def action_compute_expense_kg(self):
        for line in self:
            expense_kg = 0.0
            for shift in line.shift_id.scale_ids:
                if line.can_id.id == shift.can_id.id and line.product_id.id==shift.product_id.id:
                    expense_kg = round(line.expense_litr*shift.dhj_amount,2)
                    
            line.expense_kg = expense_kg
        
        
    
    