# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

class TtmExpenseRegister(models.Model):
    _name = 'ttm.expense.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'reg_date desc'
    
    name = fields.Char(string='Expense number')
    company_id = fields.Many2one('res.company',string='Company')
    state = fields.Selection([('draft','Draft'),
                              ('send','Send'),
                              ('confirm','Confirm'),
                              ('cancel','Cancel')],string='State',default='draft')
    reg_date = fields.Date(string='Register date')
    reg_user_id = fields.Many2one('res.users',string='Register user',default=lambda self: self.env.user)
    send_date = fields.Date(string=u'Send date', default=fields.Date.context_today)
    warehouse_id = fields.Many2one('shts.register',string='Warehouse',required=True)
    shts_id = fields.Many2one('shts.register',string='Shts',required=True,default=lambda self: self.env.user.warehouse_id)
    drive_id = fields.Many2one('hr.employee',string='Drive',domain=[('work_type','=','drive')])
    car_number_id = fields.Many2one('car.register',string=u'Машин',required=True)
    line_ids = fields.One2many('ttm.expense.register.line','parent_id',string='TTM Expense line',ondelete="cascade")
    
    def action_confirm(self):
        for line in self:
            line.write({'state':'done'})
    
    
    def action_draft(self):
        for line in self:
            if line.state == 'send':
                line.write({'state':'draft'})
            else:
                raise UserError(_(u"Батлагдсан тул ноороглох боломжгүйг анхаарна уу"))

    
    def action_send(self):
        ttm_income_obj = self.env['ttm.income.register']
        ttm_income_line_obj = self.env['ttm.income.register.line']
        for line in self:
            income_id = ttm_income_obj.create({
                                    'state':'send',
                                    'shts_id':line.shts_id.id,
                                    'company_id':line.company_id.id,
                                    'warehouse_id':line.warehouse_id.id,
                                    'send_date':line.send_date,
                                    'reg_user_id':line.reg_user_id,
                                    'date':line.reg_date,
                                    'drive_id':line.drive_id.id,
                                    'car_number_id':line.car_number_id.id,
                                    })
            for lines in line.line_ids:
                ttm_income_line_obj.create({
                                            'product_id':lines.product_id.id,
                                            'send_qty':lines.send_qty,
                                            'receive_qty':0.0,
                                            'income_id':income_id.id,
                                            'code':lines.product_id.barcode,
                                            'state':'send',
                                            })
            line.write({'state':'send'})
    
    
class TtmExpenseRegisterLine(models.Model):
    _name = 'ttm.expense.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    
    company_id = fields.Many2one('res.company',string='Company')
    product_id = fields.Many2one('product.product',string='Product',required=True)
    send_qty = fields.Float(string='Send qty',required=True)
    parent_id = fields.Many2one('ttm.expense.register',string='TTM Expense')
    
    