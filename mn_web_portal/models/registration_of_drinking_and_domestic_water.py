# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class RegistrationOfDrinkingAndDomesticWater(models.Model):
    _name = 'registration.of.drinking.and.domestic.water'

    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    shts_id = fields.Many2one('shts.register', string='Агуулах/ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id)
    date = fields.Date(string=u'Огноо', required=True)
    water_size_litr = fields.Float(string=u'Ундны усны хэмжээ литр')
    source = fields.Selection([('portable_from_the_well', u'Худгаас зөөврөөр'),
                              ('bottled_water', u'Баллонтой ус'),
                              ('center_line', u'Төвийн шугам'),
                              ('deep_well_for_own_use', u'өөрийн хэрэглээний гүний худаг '),
                              ], string='Эх үүсвэр', required=True)
    ahuin_water_size_litr = fields.Float(string=u'Ахуйн усны хэмжээ литр')
    create_user_id = fields.Many2one('res.users', string=u'Үүсгэсэн ажилтан', readonly=True,
                                     default=lambda self: self.env.user)
    # source_ = fields.Selection([('portable_from_the_well', u'Худгаас зөөврөөр'),
    #                            ('bottled_water', u'Баллонтой ус'),
    #                            ('center_line', u'Төвийн шугам'),
    #                            ('deep_well_for_own_use', u'өөрийн хэрэглээний гүний худаг '),
    #                            ], string='Эх үүсвэр', required=True)
