# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date


class ShtsPaymentRegister(models.Model):
    _name = 'shts.payment.register'
    
    company_id = fields.Many2one('res.company',string='Company',required=True
                                 ,default=lambda self: self.env['res.company']._company_default_get('shts.payment.register'))
    name = fields.Many2one('payment.type.register',string='Payment type',required=True)
    amount = fields.Float(string = 'Amount',required=True)
    shts_code = fields.Char(string=u'ШТС-ын код')
    reg_date = fields.Date(string=u'Огноо')
    type = fields.Selection([('cash',u'Бэлэн'),
                             ('bank_card',u'Банк карт'),
                             ('loan',u'Зээл'),
                             ('talon',u'Талон'),
                             ('tov_card',u'Төвд карт'),
                             ('mobile',u'Мобайл банк')],string=u'Төрөл')
    diff_amount = fields.Float(string='Diff amount')
    shift_id = fields.Many2one('shift.working.register',string='Shift register')