# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class CanRegister(models.Model):
    _name = 'can.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get('can.register'),compute="action_company")
    active = fields.Boolean(string='Active',default=True)
    is_oil_can = fields.Boolean(string='Тосны сав эсэх',default=False)
    name = fields.Char(string=u'Савны код',required=True)
    code = fields.Char(string=u'Савны дугаар',required=True)
    warehouse_code = fields.Char(string='Warehouse code')
    size = fields.Float(string='Size')
    deed_duurgelt_litr = fields.Float(string=u'Дээд дүүргэлт литр',compute="calc_deed_duurgelt")
    deed_duurgelt_huvi = fields.Float(string=u'Дээд дүүргэлт хувь')
    hii_sorolt_ondor = fields.Float(string=u'Хий сорох өндөр  сm')
    self_weight = fields.Float(string='Хувийн жин',digits=(10, 4),default='0.0000')
    height = fields.Float(string=u'Хэмжилтийн өндөр',default=103.4)
    temperature = fields.Float(string='Температур',default=13.2)
    hii_sorolt_ondor_huvi = fields.Float(string=u'Хий сорох литр')
    balance = fields.Float(string='Balance')
    description = fields.Char(string='Description')
    tablits_id = fields.One2many('tablits.register','can_id',string='Tablits')
    shts_id = fields.Many2one('shts.register',u'ШТС')
    line_ids = fields.One2many('can.register.line','can_id',string='Can line')
    atg_number = fields.Integer(string=u'ATG савны дугаар')
    
    @api.depends('name', 'code')
    def name_get(self):
        result = []
        for line in self:
            name = line.name or ''
            if line.code:
                name += " [ %s ]" % line.code
            result.append((line.id, name))
        return result
    
    def action_company(self):
        for line in self:
            line.company_id = line.shts_id.company_id.id
    
    def calc_deed_duurgelt(self):
        for line in self:
            line.deed_duurgelt_litr = line.deed_duurgelt_huvi*line.size
        
    
    
class CanRegisterLine(models.Model):
    _name = 'can.register.line'
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env['res.company']._company_default_get('can.register.line'))
    break_id = fields.Many2one('mile.target.register',string='Break',required=True)
    product_id = fields.Many2one('product.product',string='Product',domain="[('prod_type', '=', 'gasol')]",required=True)
    can_id = fields.Many2one('can.register',string='Can')
    terminal_id = fields.Many2one('shts.terminal',string=u'Түгээгүүр',required=True)
    start_date = fields.Date(string='Огноо')
    first_mile = fields.Float(string=u'Эхний милл',required=True)
    first_litr = fields.Float(string=u'Эхний литр',required=True)
    first_kg = fields.Float(string='Эхний кг',compute="can_litr_kg_compute")
    active = fields.Boolean(string='Active',default=True)
    shift = fields.Integer(string=u'Ээлж',required=True)
    
    
    def can_litr_kg_compute(self):
        for line in self:
            kg = 0.0
            if line.can_id.self_weight>0.0:
                kg = line.first_litr*(int(line.can_id.self_weight)/10000)
            line.first_kg = kg
    
    def write(self,vals):
        rslt = super(CanRegisterLine, self).write(vals)
        if 'product_id' in vals:
            raise UserError(_(u'Савны бүтээгдэхүүнийг солих боломжгүй тул та савны бүтээгдэхүүнийг шинээр үүсгэнэ үү! '))
        return rslt
    
    
    @api.model
    def create(self, vals):
        can_obj = self.env['can.register']
        if 'product_id' in vals:
            can = can_obj.browse(vals['can_id'])
            self._cr.execute("""
                            select product_id from shts_register_line 
                            where shts_id = %s and product_id = %s
                        """%(can.shts_id.id,vals['product_id']))
            data = self._cr.fetchall()
            if data==[]:
                raise UserError(_(u'Та ШТС дээр байхгүй бүтээгдэхүүн тохируулсан байгаа тул зөв бүтээгдэхүүн сонгосон эсэхээ шалгана уу!'))
        return super(CanRegisterLine, self).create(vals)
    
    
    