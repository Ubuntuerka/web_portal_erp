# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree
from dateutil.relativedelta import relativedelta
DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


class WorkClothesRegister(models.Model):
    _name = 'work.clothes.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'employee_id desc'

    company_id = fields.Many2one('res.company', string='Компани',
                                 default=lambda self: self.env['res.company']._company_default_get('shts.register'),
                                 required=True)
    employee_id = fields.Many2one('hr.employee', 'Ажилтан', required=True)
    department_id = fields.Many2one('hr.department', 'Хэлтэс')
    job_id = fields.Many2one('hr.job', u'Албан тушаал', required=True)
    rank = fields.Selection([('1', u'I'),
                             ('2', u'II'),
                             ('3', u'III')], string='Зэрэглэл')
    line_ids = fields.One2many('work.clothes.register.line', 'work_clothes_id', string='clothes line')
    create_user_id = fields.Many2one('res.users', string=u'Үүсгэсэн ажилтан', readonly=True, default=lambda self: self.env.user)

    @api.onchange('employee_id') #, 'rank'
    def onchange_employee_id(self):
        for line in self:
            line.department_id = line.employee_id.department_id.id
            line.job_id = line.employee_id.job_id.id


class WorkClothesRegisterLine(models.Model):
    _name = 'work.clothes.register.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    work_clothes_id = fields.Many2one('work.clothes.register', string='work clothes')
    season = fields.Selection([('winter', 'Өвөл'),
                               ('summer', 'Зун')], string=u'Улирал', required=True)
    clothes_type = fields.Selection([('hat', 'Малгай'),
                                     ('shirt', 'Подволк'),
                                     ('clothes', 'Хувцас'),
                                     ('shoes', 'Гутал')], string=u'Хувцасны төрөл', required=True)
    start_date = fields.Date(string='Эхлэх огноо', required=True)
    end_date = fields.Date(string='Дуусах огноо', required=True)
    size = fields.Char(string=u'Хэмжээ', required=True)
    insert_date = fields.Integer(string='Жил')






