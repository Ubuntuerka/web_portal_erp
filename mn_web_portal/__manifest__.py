# -*- coding: utf-8 -*-
##############################################################################
#
#    Cubic ERP, Enterprise and Government Management Software
#    Copyright (C) 2017 Cubic ERP S.A.C. (<http://cubicerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'WEB PORTAL',
    'version': '1.0',
    'category': 'sale',
    'sequence': 35,
    'author': 'Tengersoft Developer Team',
    'license': 'AGPL-3',
    'summary': 'Freight module add development',
    'description': """Mongolian web portal additional module""",
    'website': 'https://www.tengersoft.mn',
    'depends': [
                 'base',
                 'stock',
                 'product',
                 'hr',
                 'portal',
                 'uom',
                ],
    'data': [
            'security/security_view.xml',
            'security/talon_security_view.xml',
            'security/ir.model.access.csv',
            'views/bucket_register_view.xml',
            'views/can_register_view.xml',
            'views/car_register_view.xml',
            'views/expense_bonus_view.xml',
            'views/fuel_sale_register_view.xml',
            'views/loan_document_view.xml',
            'views/payment_type_view.xml',
            'views/price_register_view.xml',
            'wizard/oil_service_schedule_create_view.xml',
            'views/shts_register_view.xml',
            'wizard/shift_close_wizard.xml',
            'views/shift_working_register_view.xml',
            'views/tablits_register_view.xml',
            'views/talon_register_view.xml',
            'views/ttm_sale_view.xml',
            'views/ttm_income_view.xml',
            'views/huurult_register_view.xml',
            'views/distance_register_view.xml',
            'views/scaling_register_view.xml',
            'views/mile_target_register_view.xml',
            'views/product_product_view.xml',
            'views/talon_type_view.xml',
            'views/region_register_view.xml',
            'views/cash_register_view.xml',
            'views/talon_request_register_view.xml',
            'views/res_partner_view.xml',
            'views/hr_employee_inherit_view.xml',
            'views/talon_info_view.xml',
            'views/shts_internal_move.xml',
            'views/shts_income_view.xml',
            'views/price_group_view.xml',
            
            'views/product_qty_view.xml',
            'wizard/talon_report_view.xml',
            'wizard/doc_can_report_view.xml',
            'wizard/doc_payment_type_view.xml',
            'report/shts_uldegdel_view.xml',
            'report/kass_uldegdel_view.xml',
            
            'report/product_balance_view.xml',
            'views/report_date_register_view.xml',
            'views/shts_last_price_view.xml',
            'wizard/main_report_wizard_view.xml',
            'views/talon_sale_view.xml',
            'views/warehouse_inherit_view.xml',
            'views/shts_terminal_view.xml',
            'views/tablits_request_view.xml',
            'views/zalruulga.xml',
            'views/expense_type_view.xml',
            'report/report_sg_view.xml',
            'report/oil_balance_report_view.xml',
            'report/shts_shift_state_report_view.xml',
            'report/sale_retail_price_report_view.xml',
            'report/shza_centre_read_card_report_view.xml',
            'report/shza_mbank_trans_check_reg_report_view.xml',
            'report/region_retail_sale_report_view.xml',
            'report/shts_retail_sale_report_view.xml',
            'report/shts_pos_report_view.xml',
            'report/mill_break_view.xml',
            'report/center_payment_income_report_view.xml',
           # 'views/talon_shift_view.xml',
            'views/data_sync_config_register_view.xml',
            'views/sequence_view.xml',
            'views/shts_expense_register_view.xml',
            'views/talon_lost_view.xml',
            'wizard/inventory_wizard_view.xml',
            'views/inventory_view.xml',
            'views/res_company_view.xml',
            'views/thh_data_view.xml',
            'views/gas_income_view.xml',
            'report/shts_income_report_view.xml',
            'report/ttm_zarlaga_view.xml',
            'report/shts_last_price_view.xml',
            'report/change_amount_report_view.xml',
            'report/income_total_report_view.xml',
            'absract_report_view.xml',
            'wizard/service_view_report.xml',
            'wizard/doc_can_view_report.xml',
            'wizard/service_report_view.xml',
            'wizard/mile_break_report_view.xml',
            'wizard/normal_wastage_report_view.xml',
            'wizard/oil_move_report_view.xml',
            'wizard/doc_sale_report_view.xml',
            'wizard/car_move_report.xml',
            'wizard/shts_move_report.xml',
            'wizard/payment_act_report.xml',
            'wizard/shts_balance_report_wizard_view.xml',
            'wizard/sales_recapitulation_report.xml',
            'wizard/sales_recapitulation_sale_fuel_report.xml',
            #'wizard/shts_user_credit_fuel_reg.xml',
            #'wizard/sales_recapitulation_sale_retail_report.xml',
            #'wizard/retail_sale_report.xml',
            'wizard/shts_price_done_view.xml',
            'wizard/shts_gasol_sales_report_view.xml',
            'wizard/shts_loan_sales_report_view.xml',
            'wizard/shts_expense_sales_report_view.xml',
            'wizard/gs_product_balance_view.xml',
            #'wizard/credit_approval_view.xml',
            'wizard/compute_calculate_akt_view.xml',
            #'wizard/action_compute_payment_edit_view.xml',
            'views/terminal_register_view.xml',
            'views/borluulaltin_medee_view.xml',
            'views/settlement_view.xml',
            'views/product_session_config_view.xml',
            'views/shift_terminal_error_view.xml',
            'views/product_uom_inherit_view.xml',
            'views/price_register_shts_view.xml',
            'views/technical_check_info_view.xml',
            'views/device_register_view.xml',
            'views/pump_mapping_view.xml',
            'views/normal_loss_compute_view.xml',
            'views/shift_working_diff_register_view.xml',
            'views/hhn_register_view.xml',
            'views/colonization_register_view.xml',
            'views/act_of_calculation_register_view.xml',
            'views/ttm_expense_register_view.xml',
            'views/center_payment_income_view.xml',
            'wizard/income_compute_wizard_view.xml',
            'wizard/regional_average_price_report_view.xml',
            'wizard/regional_sales_revenue_type_report_view.xml',
            'wizard/iluu_dutuu_report_wizard_view.xml',
            'wizard/income_statement_report_view.xml',
            #'wizard/price_change_mill_report_view.xml',
            'wizard/main_report_wizard_not_can_view.xml',
            #'wizard/infor_data_compute_wizard_view.xml',
            'wizard/product_balance_region_report_view.xml',
            'wizard/required_return_wizard.xml',
            'views/required_workplace_register_view.xml',
            'views/basic_type_register_view.xml',
            'views/subspecies_type_register_view.xml',
            'views/basic_type_register_view.xml',
            'views/subspecies_type_register_view.xml',
            'views/nature_and_danger_register_full_view.xml',
            'views/work_clothes_register_view.xml',
            'views/eruul_mend_register_view.xml',
            'views/fire_tools_register_view.xml',
            'views/workplace_fire_review_register_view.xml',
            'views/disaster_recovery_plan_register_view.xml',
            'views/emergency_firefighting_plan_register_view.xml',
            'views/disaster_rick_assessment_register_view.xml',
            'views/general_environmental_assessment_register_view.xml',
            'views/environmental_management_plan_register_view.xml',
            'views/environmental_analysis_register_view.xml',
            'views/industrial_accident_act_register_view.xml',
            'views/supply_act_register_view.xml',
            'views/waste_registration_view.xml',
            'views/registration_of_drinking_and_domestic_water_view.xml',
            'views/bypass_page_register_view.xml',
            'views/required_page_register_view.xml',
            'views/loan_approve_request_view.xml',
            'views/location_register_view.xml',
            'views/special_work_permit_register_view.xml',
            'views/job_title_register_view.xml',
            'views/oil_center_stop_view.xml',
            'views/schedule_plan_view.xml',
            'report/report.xml',
            'report/report_loan_document.xml',
            'report/report_loan_document_unegui.xml',
            'report/ttm_toollogo_print_view.xml',
            'views/shts_free_time_register_view.xml',
            'views/employee_turnover_register_view.xml',
            'views/oil_service_schedule_view.xml',
            'views/menu_view.xml',
            
            
    ],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
