# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _


import time
import xlwt
import openerp.netsvc

class shts_expense_sales_view_report(models.TransientModel):
    _name = 'shts.expense.sales.view.report'
    
    company_id = fields.Many2many('res.company',string=u'Компани')
    partner_id = fields.Many2one('res.partner',string=u'Харилцагч',readonly=True)
    start_date = fields.Date(string=u'Эхлэх огноо',readonly=True)
    end_date = fields.Date(string=u'Дуусах огноо',readonly=True)
    shts_id = fields.Many2many('shts.register',string=u'ШТС',readonly=True)
    line_ids = fields.One2many('shts.expense.sales.view.line','parent_id',string='Line')
    
    
    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'shts.expense.sales.report' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(shts_expense_sales_view_report, self).default_get(fields)
        main_report_ids = self.env['shts.expense.sales.report'].browse(self.env.context['active_ids'])
        rec['company_id'] = main_report_ids.company_id.id
        rec['start_date'] = main_report_ids.start_date
        rec['end_date'] = main_report_ids.end_date    
        rec['partner_id'] = main_report_ids.partner_id.id  
        rec['shts_id'] = main_report_ids.shts_id.ids
        dats = self.env.context.get('line_ids')
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['line_ids'] = list
        return rec
    

class shts_expense_sales_view_line(models.TransientModel):
    _name = 'shts.expense.sales.view.line'
    
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн')
    company_id = fields.Many2one('res.company',string=u'Компани')
    name = fields.Char(string=u'Падаан №')
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    date = fields.Date(string='Огноо')
    desc = fields.Char(string=u'Тайлбар')
    drive_id = fields.Char(string=u'Жолооч')
    car_number = fields.Char(string='Машин')
    partner_id = fields.Many2one('res.partner',string=u'Харилцагч')
    litr = fields.Float(string=u'Лирт')
    kg = fields.Float(string=u'Кг')
    total = fields.Float(string='Нийт үнэ')
    parent_id = fields.Many2one('shts.expense.sales.view.report','Parent')
    

class ShtsexpenseSalesReport(models.TransientModel):
    _inherit = "abstract.report.excel"
    _name = 'shts.expense.sales.report'
    
    company_id = fields.Many2many('res.company',string=u'Компани')
    partner_id = fields.Many2one('res.partner',string=u'Харилцагч')
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    shts_id = fields.Many2many('shts.register',string=u'ШТС')
    
    
    def action_view_report(self):
        shift_obj = self.env['shift.working.register']
        context = dict(self.env.context or {})
        lin_data = []
        shts = []
        if self.shts_id:
            for sh in self.shts_id:
                shts.append(sh.id)
        
        company = []
        if self.company_id:
            for comp in self.company_id:
                company.append(comp.id)
        
        if len(shts)==1:
            if len(company)==1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',shts[0]),('company_id','=',company[0]),('state','not in',('draft','cancel'))])
            elif len(company) >1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',shts[0]),('company_id','in',company),('state','not in',('draft','cancel'))])
            else:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',shts[0]),('state','not in',('draft','cancel'))])

        elif len(shts) >1:
            if len(company) == 1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','in',shts),('company_id','=',company[0]),('state','not in',('draft','cancel'))])
            elif len(company) >1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','in',shts),('company_id','=',company),('state','not in',('draft','cancel'))])
            else:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','in',shts),('state','not in',('draft','cancel'))])

        else:
            if len(company)==1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('company_id','=',company[0]),('state','not in',('draft','cancel'))])
            elif len(company) >1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('company_id','=',company),('state','not in',('draft','cancel'))])
            else:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('state','not in',('draft','cancel'))])

        if shift_ids:
            for sv in shift_ids:
                for expense in sv.loan_document_ids:
                    if expense.expense_type_id.type == 'other':
                        if self.partner_id:
                            if expense.partner_id.id == self.partner_id.id:
                                datas = {
                                        'company_id':expense.company_id.id,
                                        'product_id':expense.product_id.id,
                                        'date':sv.shift_date,
                                        'name':expense.name,
                                        'drive_id':expense.drive_id,
                                        'desc':expense.desc,
                                        'partner_id':expense.partner_id.id,
                                        'car_number':expense.car_number_id,
                                        'shts_id':sv.shts_id.id,
                                        'litr':expense.litr,
                                        'kg':expense.kg,
                                        'total':expense.total_amount,
                                        }
                                lin_data.append(datas)
                        else:
                            datas = {
                                    'company_id':expense.company_id.id,
                                    'product_id':expense.product_id.id,
                                    'date':sv.shift_date,
                                    'name':expense.name,
                                    'drive_id':expense.drive_id,
                                    'desc':expense.desc,
                                    'partner_id':expense.partner_id.id,
                                    'car_number':expense.car_number_id,
                                    'shts_id':sv.shts_id.id,
                                    'litr':expense.litr,
                                    'kg':expense.kg,
                                    'total':expense.total_amount,
                                    }
                            lin_data.append(datas)
                
                
                for expense_ttm in sv.ttm_sale_ids:
                    if expense_ttm.expense_type_id.type == 'other':
                        if self.partner_id:
                            if expense_ttm.partner_id.id == self.partner_id.id:
                                datas = {
                                        'company_id':sv.shts_id.company_id.id,
                                        'product_id':expense_ttm.product_id.id,
                                        'date':sv.shift_date,
                                        'partner_id':expense_ttm.partner_id.id,
                                        'shts_id':sv.shts_id.id,
                                        'litr':expense_ttm.qty,
                                        'total':expense_ttm.total_price,
                                        }
                                lin_data.append(datas)
                        else:
                            datas = {
                                    'company_id':sv.shts_id.company_id.id,
                                    'product_id':expense_ttm.product_id.id,
                                    'date':sv.shift_date,
                                    'partner_id':expense_ttm.partner_id.id,
                                    'shts_id':sv.shts_id.id,
                                    'litr':expense_ttm.qty,
                                    'total':expense_ttm.total_price,
                                    }
                            lin_data.append(datas)
        context['line_ids'] = lin_data
        
        compose_form = self.env.ref('mn_web_portal.shts_expense_sales_view_report_view')
        action = {
            'name': _(u'ШТС Зардалд зарцуулсан тайлан'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'shts.expense.sales.view.report',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Зардалд зарцуулалтын мэдээ')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right1 = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'%s' % (company.name,), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write_merge(2, 2, 0, 7, u'Зардалд зарцуулалтын мэдээ', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 240'))
        sheet.write_merge(3, 3, 0, 0, u'ШТС: %s'%self.shts_id.name, ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 240'))
        sheet.write(5, 6, u'Тайлант үе: %s - %s' % (self.start_date,self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        
        
        rowx = 6
        sheet.write(rowx, 0, u'№', data_bold)
        sheet.write(rowx, 1, u'Компани', data_bold)
        sheet.write(rowx, 2, u'ШТС', data_bold)
        sheet.write(rowx, 3, u'Огноо', data_bold)
        sheet.write(rowx, 4, u'Падаан №', data_bold)
        sheet.write(rowx, 5, u'Харилцагч', data_bold)
        sheet.write(rowx, 6, u'Машин', data_bold)
        sheet.write(rowx, 7, u'Жолооч', data_bold)
        sheet.write(rowx, 8, u'Бүтээгдэхүүн', data_bold)
        sheet.write(rowx, 9, u'Литр', data_bold)
        sheet.write(rowx, 10, u'Кг', data_bold)
        sheet.write(rowx, 11, u'Нэгж үнэ', data_bold)
        sheet.write(rowx, 12, u'Нийт үнэ', data_bold)
        sheet.write(rowx, 13, u'Тайлбар', data_bold)
        shift_obj = self.env['shift.working.register']
        
        number = 0
        shts = []
        if self.shts_id:
            for sh in self.shts_id:
                shts.append(sh.id)
        
        company = []
        if self.company_id:
            for comp in self.company_id:
                company.append(comp.id)
        
        if len(shts)==1:
            if len(company)==1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',shts[0]),('company_id','=',company[0]),('state','not in',('draft','cancel'))])
            elif len(company) >1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',shts[0]),('company_id','in',company),('state','not in',('draft','cancel'))])
            else:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',shts[0]),('state','not in',('draft','cancel'))])

        elif len(shts) >1:
            if len(company) == 1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','in',shts),('company_id','=',company[0]),('state','not in',('draft','cancel'))])
            elif len(company) >1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','in',shts),('company_id','=',company),('state','not in',('draft','cancel'))])
            else:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','in',shts),('state','not in',('draft','cancel'))])

        else:
            if len(company)==1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('company_id','=',company[0]),('state','not in',('draft','cancel'))])
            elif len(company) >1:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('company_id','=',company),('state','not in',('draft','cancel'))])
            else:
                shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('state','not in',('draft','cancel'))])

        rowx = 7
        if shift_ids:
            for sv in shift_ids:
                for expense in sv.loan_document_ids:
                    if expense.expense_type_id.type == 'other':
                        if self.partner_id:
                            if expense.partner_id.id == self.partner_id.id:
                                number +=1
                                sheet.write(rowx, 0, number, data_right1)
                                sheet.write(rowx, 1, u'%s'%sv.company_id.name, data_right)
                                sheet.write(rowx, 2, u'%s'%sv.shts_id.name, data_right)
                                sheet.write(rowx, 3, u'%s'%sv.shift_date, data_right)
                                sheet.write(rowx, 4, u'%s'%expense.name, data_right)
                                sheet.write(rowx, 5, u'%s'%expense.partner_id.name, data_right)
                                sheet.write(rowx, 6, u'%s'%expense.car_number_id, data_right)
                                sheet.write(rowx, 7, u'%s'%expense.drive_id, data_right)
                                sheet.write(rowx, 8, u'%s'%expense.product_id.name, data_right)
                                sheet.write(rowx, 9, expense.litr, data_right1)
                                sheet.write(rowx, 10, expense.kg, data_right)
                                sheet.write(rowx, 11, expense.price, data_right1)
                                sheet.write(rowx, 12, expense.total_amount, data_right1)
                                if expense.desc == False:
                                   sheet.write(rowx, 13, '', data_right)
                                else:
                                   sheet.write(rowx, 13, expense.desc, data_right)
                                rowx +=1
                        else:
                            number +=1
                            sheet.write(rowx, 0, number, data_right1)
                            sheet.write(rowx, 1, u'%s'%sv.company_id.name, data_right)
                            sheet.write(rowx, 2, u'%s'%sv.shts_id.name, data_right)
                            sheet.write(rowx, 3, u'%s'%sv.shift_date, data_right)
                            sheet.write(rowx, 4, u'%s'%expense.name, data_right)
                            sheet.write(rowx, 5, u'%s'%expense.partner_id.name, data_right)
                            sheet.write(rowx, 6, u'%s'%expense.car_number_id, data_right)
                            sheet.write(rowx, 7, u'%s'%expense.drive_id, data_right)
                            sheet.write(rowx, 8, u'%s'%expense.product_id.name, data_right)
                            sheet.write(rowx, 9, round(expense.litr,2), data_right1)
                            sheet.write(rowx, 10, round(expense.kg,2), data_right)
                            sheet.write(rowx, 11, round(expense.price,2), data_right1)
                            sheet.write(rowx, 12, round(expense.total_amount,2), data_right1)
                            if expense.desc == False:
                               sheet.write(rowx, 13, '', data_right)
                            else:
                               sheet.write(rowx, 13, expense.desc, data_right)
                            rowx +=1
        
                for expense_ttm in sv.ttm_sale_ids:
                    if expense_ttm.expense_type_id.type == 'other':
                        if self.partner_id:
                            if expense_ttm.partner_id.id == self.partner_id.id:
                                number +=1
                                sheet.write(rowx, 0, number, data_right1)
                                sheet.write(rowx, 1, u'%s'%sv.company_id.name, data_right)
                                sheet.write(rowx, 2, u'%s'%sv.shts_id.name, data_right)
                                sheet.write(rowx, 3, u'%s'%sv.shift_date, data_right)
                                sheet.write(rowx, 4, u'', data_right)
                                sheet.write(rowx, 5, u'%s'%expense_ttm.partner_id.name, data_right)
                                sheet.write(rowx, 6, u'', data_right)
                                sheet.write(rowx, 7, u'', data_right)
                                sheet.write(rowx, 8, u'%s'%expense_ttm.product_id.name, data_right)
                                sheet.write(rowx, 9, expense_ttm.qty, data_right1)
                                sheet.write(rowx, 10, '', data_right)
                                sheet.write(rowx, 11, expense_ttm.price, data_right1)
                                sheet.write(rowx, 12, expense_ttm.total_price, data_right1)
                                sheet.write(rowx, 13, '', data_right)
                                rowx +=1
                        else:
                            number +=1
                            sheet.write(rowx, 0, number, data_right1)
                            sheet.write(rowx, 1, u'%s'%sv.company_id.name, data_right)
                            sheet.write(rowx, 2, u'%s'%sv.shts_id.name, data_right)
                            sheet.write(rowx, 3, u'%s'%sv.shift_date, data_right)
                            sheet.write(rowx, 4, u'', data_right)
                            sheet.write(rowx, 5, u'%s'%expense_ttm.partner_id.name, data_right)
                            sheet.write(rowx, 6, u'', data_right)
                            sheet.write(rowx, 7, u'', data_right)
                            sheet.write(rowx, 8, u'%s'%expense_ttm.product_id.name, data_right)
                            sheet.write(rowx, 9, expense_ttm.qty, data_right1)
                            sheet.write(rowx, 10, '', data_right)
                            sheet.write(rowx, 11, expense_ttm.price, data_right1)
                            sheet.write(rowx, 12, expense_ttm.total_price, data_right1)
                            sheet.write(rowx, 13, '', data_right)
                            rowx +=1


        inch = 1000
        sheet.col(0).width = 1 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 7 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.row(2).height = 400

        return {'data':book, 'directory_name':u'Үйлчилгээний тайлан',
                'attache_name':'Үйлчилгээний тайлан'}
    
    
    
    
    
    