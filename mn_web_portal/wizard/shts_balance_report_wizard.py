# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc
from datetime import date
from datetime import timedelta
from datetime import datetime
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class ShtsBalanceReportWizard(models.TransientModel):
    _name = "shts.balance.report.wizard"
    _inherit = "abstract.report.excel"
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('shts.balance.report.wizard'))
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    can_id = fields.Many2one('can.register',string=u'Сав',domain="[('shts_id','=',shts_id),('is_oil_can','=',False)]",required=True)
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    is_shts_manager = fields.Boolean(string=u'ШТС Эрхлэгч харуулах')
    is_shts_nho = fields.Boolean(string=u'ШТС НХО харуулах')
    
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'01 Тайлан')
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right_hj = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.0000_);(#,##0.0000)')

        sheet.write(0, 0, u'Компани:', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 2, u'%s'%(company.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(1, 0, u'ШТС-ын нэр:', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180')) 
        sheet.write(1, 2, u'%s'%(self.shts_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180')) 
        sheet.write(2, 0, u'Савны №:', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 2, u'%s' % (self.can_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 0, u'Савны багтаамж:', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 2, u'%s' % (self.can_id.size), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(4, 0, u'Бүтээгдэхүүн: ', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(4, 4, u'%s' % (self.can_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        
        
        
        sheet.write(0, 6, u'Тайлангийн эхлэх хугацаа:', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 8, u'%s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(1, 6, u'Тайлангийн дуусах хугацаа:', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(1, 8, u'%s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 6, u'Тайлант хоног:', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        converted_in = datetime.strptime(str(self.start_date), DATE_FORMAT)
        converted_out = datetime.strptime(str(self.end_date), DATE_FORMAT)
        converted_diff = converted_out - converted_in
        diff_day = converted_diff.days + 1
        
        sheet.write(2, 8, u'%s' % (diff_day), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))


        rowx = 6
        sheet.write_merge(rowx, rowx+1, 0, 0, u'Компани', data_center)
        sheet.write_merge(rowx, rowx+1, 1, 1, u'Бүс', data_center)
        sheet.write_merge(rowx, rowx+1, 2, 2, u'ШТС', data_center)
        sheet.write_merge(rowx, rowx+1, 3, 3, u'Савны дугаар', data_center)
        sheet.write_merge(rowx, rowx+1, 4, 4, u'Огноо', data_center)
        sheet.write_merge(rowx, rowx+1, 5, 5, u'Ээлж', data_center)
        
        
        sheet.write_merge(rowx, rowx, 6, 10, u'Эцсийн үлдэгдэл', data_center)
        sheet.write_merge(rowx+1, rowx+1, 6, 6, u'Өндөр', data_center)
        sheet.write_merge(rowx+1, rowx+1, 7, 7, u'Хувийн жин', data_right_hj)
        sheet.write_merge(rowx+1, rowx+1, 8, 8, u'Темп', data_center)
        sheet.write_merge(rowx+1, rowx+1, 9, 9, u'Литр', data_center)
        sheet.write_merge(rowx+1, rowx+1, 10, 10, u'Кг', data_center)


        
        
        shift_obj = self.env['shift.working.register']
        shts_income_obj = self.env['shts.income']
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        
        rowx +=2
        for day in days:
            date1 = day.strftime('%Y-%m-%d')
            date11 = datetime.strptime(str(date1), DATE_FORMAT)
            pdate = date11-timedelta(days=1)
            if self.shts_id.work_time == 2:
                shift_id = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',date1),('state','in',('confirm','count'))],order="shift asc")
            else:
                pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('state','in',('confirm','count'))],order="shift asc")
                shift_id = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',date1),('state','in',('confirm','count'))],order="shift asc")
            signin_day_b = datetime.strptime(str(day), DATETIME_FORMAT)
            signin_day = signin_day_b.replace(hour=0, minute=0, second=1)
            signout_day = signin_day_b.replace(hour=23, minute=59, second=59)
            total_kg = 0.0
            if shift_id:
                count = 0
                total_first_height = 0.0
                for shift in shift_id:
                    income = shts_income_obj.search([('shts_id','=',self.shts_id.id),('shift','=',shift.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','=','receive')])
                    product_id = False
                    shift_day = shift.shift
                    first_litr = 0.0
                    first_height = 0.0
                    first_self_weight = 0.0
                    first_temp = 0.0
                    first_kg = 0.0
                    income_litr = 0.0
                    income_kg = 0.0
                    income_dhj = 0.0
                    exp_litr = 0.0
                    exp_kg = 0.0
                    have_litr = 0.0
                    have_kg = 0.0
                    last_litr = 0.0
                    last_kg = 0.0
                    diff_litr = 0.0
                    diff_kg = 0.0
                    dhg_amount = 0.0
                    zarlaga_kg = 0.0
                    zarlaga_litr = 0.0
                    income_total = 0.0
                    exp_zalin = 0.0
                    last_self_weight = 0.0
                    last_temp = 0.0
                    zalin = 0.0
                    count +=1
                    for line in shift.scale_ids:
                        if line.can_id.id == self.can_id.id:
                            if shift.shift == 1:
                                pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('confirm','count'))],order="shift asc")
                            else:
                                pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',date1),('shift','=',1),('state','in',('confirm','count'))],order="shift asc")
                            if pre_shift:
                                for li in pre_shift.scale_ids:
                                    if li.can_id.id == self.can_id.id:
                                        first_height = li.name
                                        first_self_weight = li.self_weight_value
                                        first_temp = float(li.temperature)
                                        first_kg += li.kg
                                        first_litr += li.litr
                                last_temp = float(line.temperature)
                                last_litr = float(line.litr)
                                last_kg = float(line.kg)
                                last_self_weight = line.self_weight_value
                                last_heigth = line.name
                            else:
                                first_litr = self.action_calculate_litr(line.can_id.height, self.can_id)
                                first_kg = first_litr*line.can_id.self_weight
                                first_height = line.can_id.height
                                first_self_weight = line.can_id.self_weight
                                first_temp = float(line.can_id.temperature)
                                last_temp = float(line.temperature)
                                last_litr = float(line.litr)
                                last_kg = float(line.kg)
                                last_self_weight = line.self_weight_value
                                last_heigth = line.name
                            
                            for li in income.line_ids:
                                if li.can_id.id == self.can_id.id:
                                    if li.product_id.id == line.product_id.id:
                                        income_litr += li.hariu_litr
                                        income_kg += li.hariu_kg
                                
                    income_dhj = (first_kg+income_kg+last_kg)/(first_litr+income_litr+last_litr)
                    for exp in shift.mile_target_ids:
                       if self.can_id.id == exp.can_id.id:
                            exp_kg += exp.expense_litr*round(income_dhj,4)
                            exp_litr += exp.expense_litr
                            zalin +=exp.zalin
                    
                    have_kg = first_kg + income_kg - exp_kg 
                    have_litr = first_litr + income_litr - exp_litr                  
                    zarlaga_kg = exp_litr
                    diff_litr = last_litr - have_litr
                    diff_kg = last_kg - have_kg
                    
                    total_first_height = first_height/count
                    sheet.write(rowx, 0, '%s'%date1, data_center)
                    sheet.write(rowx, 1, '%s'%shift_day, data_center)
                    sheet.write(rowx, 2, round(first_height,2), data_center)
                    sheet.write(rowx, 3, round(first_self_weight,4), data_right_hj)
                    sheet.write(rowx, 4, round(first_temp,2), data_center)
                    sheet.write(rowx, 5, round(first_litr,2), data_center)
                    sheet.write(rowx, 6, round(first_kg,2), data_center)
                    sheet.write(rowx, 7, round(income_litr,2), data_center)
                    sheet.write(rowx, 8, round(income_kg,2), data_center)
                    sheet.write(rowx, 9, round(income_dhj,4), data_right_hj)
                    sheet.write(rowx, 10, round(zalin,2), data_center)
                    sheet.write(rowx, 11, round(exp_litr,2), data_center)
                    sheet.write(rowx, 12, round(exp_kg,2), data_center)
                    sheet.write(rowx, 13, round(have_litr,2), data_center)
                    sheet.write(rowx, 14, round(have_kg,2), data_center)
                    
                    sheet.write(rowx, 15, round(last_heigth,2), data_center)
                    sheet.write(rowx, 16, round(last_self_weight,4), data_right_hj)
                    sheet.write(rowx, 17, round(last_temp,2), data_center)
                    sheet.write(rowx, 18, round(last_litr,2), data_center)
                    sheet.write(rowx, 19, round(last_kg,2), data_center)
                    sheet.write(rowx, 20, round(diff_litr,2), data_center)
                    sheet.write(rowx, 21, round(diff_kg,2), data_center)
                    total_kg +=first_kg
                    if self.is_shts_manager == True and self.is_shts_nho == True:
                        manager_data = ''
                        nh_data = ''
                        for mj in shift.shift_manager_line_ids:
                            manager_data +='%s, '%mj.employee_id.name
                        for nh in shift.shift_line_ids:
                            nh_data +='%s, '%nh.employee_id.name
                        sheet.write(rowx, 22, manager_data, data_center)
                        sheet.write(rowx, 23, nh_data, data_center)
                    elif self.is_shts_manager == True and self.is_shts_nho == False:
                        manager_data = ''
                        for mj in shift.shift_manager_line_ids:
                            manager_data +='%s, '%mj.employee_id.name

                        sheet.write(rowx, 22, manager_data, data_center)
                    elif self.is_shts_manager == False and self.is_shts_nho == True:
                        nh_data = ''
                        for nh in shift.shift_line_ids:
                            nh_data +='%s, '%nh.employee_id.name
                        sheet.write(rowx, 22, nh_data, data_center)
                    rowx +=1
         
        sheet.write_merge(rowx, rowx, 0,  1, u'Нийт', data_center)
        sheet.write(rowx, 2, round(total_first_height,2), data_center)
        sheet.write(rowx, 3, 0, data_right_hj)
        sheet.write(rowx, 4, 0, data_center)
        sheet.write(rowx, 5, round(first_litr,2), data_center)
        sheet.write(rowx, 6, round(total_kg,2), data_center)
        sheet.write(rowx, 7, round(income_litr,2), data_center)
        sheet.write(rowx, 8, round(income_kg,2), data_center)
        sheet.write(rowx, 9, 0, data_right_hj)
        sheet.write(rowx, 10, round(zalin,2), data_center)
        sheet.write(rowx, 11, round(exp_litr,2), data_center)
        sheet.write(rowx, 12, round(exp_kg,2), data_center)
        sheet.write(rowx, 13, 0, data_center)
        sheet.write(rowx, 14, 0, data_center)
        
        sheet.write(rowx, 15, round(last_heigth/count,2), data_center)
        sheet.write(rowx, 16, 0, data_right_hj)
        sheet.write(rowx, 17, 0, data_center)
        sheet.write(rowx, 18, round(last_litr,2), data_center)
        sheet.write(rowx, 19, round(last_kg,2), data_center)
        sheet.write(rowx, 20, round(diff_litr,2), data_center)
        sheet.write(rowx, 21, round(diff_kg,2), data_center)
        if diff_day > 0.0:
            total_amount = total_kg/diff_day
        else:
            total_amount = total_kg/diff_day
        sheet.write(3, 6, u'Дундаж хадгалалт', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 8, total_amount, ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        

        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 2 * inch
        sheet.col(2).width = 3 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 3 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 3 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 5 * inch
        sheet.col(9).width = 3 * inch
        sheet.col(10).width = 3 * inch
        sheet.col(11).width = 3 * inch
        sheet.col(12).width = 3 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.row(0).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Тосны хөдөлгөөний тайлан',
                'attache_name':'Тосны хөдөлгөөний тайлан'}
        
    def action_calculate_litr(self,height,line):
        amount = 0.0
        ondor_too = int(height/10)*10
        buhel_ondor = int(height)
        tootsoo = height-buhel_ondor
        zoruu = int(height) - ondor_too
        too = 0
        aa = 0
        aaa = 0
        
        if zoruu == 0:
            too = ondor_too - 10
            for chart in line.tablits_id.line_ids:
               if too == chart.name:
                  check_value = 1
                  if tootsoo > 0:
                       aa = too + 10
                       aaa = chart.ten_mm
                  else: 
                      amount = chart.ten_mm
               if aa == chart.name:
                  amount = aaa + (chart.one_mm - aaa)*tootsoo
        else:
            if buhel_ondor == 10:
               too = ondor_too - 10
            else:
               
               too = ondor_too
            
            for chart in line.tablits_id.line_ids:
                if too ==chart.name:
                   check_value = 1
                   if zoruu == 1:
                      if tootsoo > 0:
                           amount = chart.one_mm + (chart.two_mm - chart.one_mm)*tootsoo
                      else: 
                          amount = chart.one_mm              
                   elif zoruu ==2:
                       if tootsoo > 0:
                           amount = chart.two_mm + (chart.three_mm - chart.two_mm)*tootsoo
                       else: 
                          amount = chart.two_mm 
                   elif zoruu ==3:
                       if tootsoo > 0:
                           amount = chart.three_mm + (chart.four_mm - chart.three_mm)*tootsoo
                       else: 
                          amount = chart.three_mm 
                   elif zoruu ==4:
                       if tootsoo > 0:
                           amount = chart.four_mm + (chart.five_mm - chart.four_mm)*tootsoo
                       else: 
                          amount = chart.four_mm   
                   elif zoruu ==5:
                       if tootsoo > 0:
                           amount = chart.five_mm + (chart.six_mm - chart.five_mm)*tootsoo
                       else: 
                          amount = chart.five_mm
                   elif zoruu ==6:
                       if tootsoo > 0:
                           amount = chart.six_mm + (chart.seven_mm - chart.six_mm)*tootsoo
                       else: 
                          amount = chart.six_mm
                   elif zoruu ==7:
                       if tootsoo > 0:
                           amount = chart.seven_mm + (chart.eight_mm - chart.seven_mm)*tootsoo
                       else: 
                          amount = chart.seven_mm
                   elif zoruu ==8:
                       if tootsoo > 0:
                           amount = chart.eight_mm + (chart.nine_mm - chart.eight_mm)*tootsoo
                       else: 
                          amount = chart.eight_mm
                   elif zoruu ==9:
                       if tootsoo > 0:
                           amount = chart.nine_mm + (chart.ten_mm - chart.nine_mm)*tootsoo
                       else: 
                          amount = chart.nine_mm   

            
            
        return amount
    
    def action_view_report(self):
        shift_obj = self.env['shift.working.register']
        shts_income_obj = self.env['shts.income']
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        context = dict(self.env.context or {})
        lin_data = []
        
        for day in days:
            date1 = day.strftime('%Y-%m-%d')
            date11 = datetime.strptime(str(date1), DATE_FORMAT)
            if self.shts_id.work_time == 2:
                pdate = date11-timedelta(hours=12)
            elif self.shts_id.work_time == 1:
                pdate = date11-timedelta(hours=24)
            
            if self.shts_id.work_time == 2:
                shift_ids = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',date1),('state','in',('confirm','count'))],order="shift asc")
            else:
                pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('state','in',('confirm','count'))],order="shift asc")
                shift_ids = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',date1),('state','in',('confirm','count'))],order="shift asc")
            
            signin_day_b = datetime.strptime(str(day), DATETIME_FORMAT)
            signin_day = signin_day_b.replace(hour=0, minute=0, second=1)
            signout_day = signin_day_b.replace(hour=23, minute=59, second=59)
            total_kg = 0.0
            if shift_ids:
                for shift_d in shift_ids:
                    income = shts_income_obj.search([('shts_id','=',self.shts_id.id),('shift','=',shift_d.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','=','receive')])
                    shift_day = shift_d.shift
                    first_litr = 0.0
                    first_height = 0.0
                    first_self_weight = 0.0
                    first_temp = 0.0
                    first_kg = 0.0
                    income_litr = 0.0
                    income_kg = 0.0
                    income_dhj = 0.0
                    exp_litr = 0.0
                    exp_kg = 0.0
                    have_litr = 0.0
                    have_kg = 0.0
                    last_litr = 0.0
                    last_kg = 0.0
                    diff_litr = 0.0
                    diff_kg = 0.0
                    dhg_amount = 0.0
                    zarlaga_kg = 0.0
                    zarlaga_litr = 0.0
                    income_total = 0.0
                    exp_zalin = 0.0
                    last_self_weight = 0.0
                    last_temp = 0.0
                    zalin = 0.0
                    height_count = 1
                    for line in shift_d.scale_ids:
                        
                        if line.can_id.id == self.can_id.id:
                            
                            if shift_d.shift == 1:
                                pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('confirm','count'))],order="shift asc")
                            else:
                                pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',date1),('shift','=',1),('state','in',('confirm','count'))],order="shift asc")
                            if pre_shift:
                                for li in pre_shift.scale_ids:
                                    if li.can_id.id == self.can_id.id:
                                        height_count +=1
                                        first_height = li.name
                                        first_self_weight = li.self_weight_value
                                        first_temp = float(li.temperature)
                                        first_kg += li.kg
                                        total_kg +=li.kg
                                        first_litr += li.litr
                                last_temp = float(line.temperature)
                                last_litr = float(line.litr)
                                last_kg = float(line.kg)
                                last_self_weight = line.self_weight_value
                                last_heigth = line.name
                            else:
                                height_count +=1
                                first_litr = self.action_calculate_litr(line.can_id.height, self.can_id)
                                first_kg = first_litr*line.can_id.self_weight
                                total_kg = first_litr*line.can_id.self_weight
                                first_height = line.can_id.height
                                first_self_weight = line.can_id.self_weight
                                first_temp = float(line.can_id.temperature)
                                last_temp = float(line.temperature)
                                last_litr = float(line.litr)
                                last_kg = float(line.kg)
                                last_self_weight = line.self_weight_value
                                last_heigth = line.name
                            
                            for li in income.line_ids:
                                if li.can_id.id == self.can_id.id:
                                    if li.product_id.id == line.product_id.id:
                                        income_litr += li.hariu_litr
                                        income_kg += li.hariu_kg
                    in_num = first_litr+income_litr+last_litr
                    if in_num > 0.0:
                        income_dhj = (first_kg+income_kg+last_kg)/in_num
                    else:
                        income_dhj = 0.0
                    for exp in shift_d.mile_target_ids:
                       if self.can_id.id == exp.can_id.id:
                            exp_kg += exp.expense_litr*round(income_dhj,4)
                            exp_litr += exp.expense_litr
                            zalin +=exp.zalin
                    
                    have_kg = first_kg + income_kg - exp_kg 
                    have_litr = first_litr + income_litr - exp_litr                  
                    zarlaga_kg = exp_litr
                    diff_litr = last_litr - have_litr
                    diff_kg = last_kg - have_kg
                    manager = []
                    nho = []
                    for mj in shift_d.shift_manager_line_ids:
                        manager.append(mj.employee_id.id)
                    for nh in shift_d.shift_line_ids:
                        nho.append(nh.employee_id.id)
                    manager_ids = [(6,0,manager)]
                    nho_ids = [(6,0,nho)]
                    show_manager = False
                    show_nho = False
                    if self.is_shts_manager == True:
                        show_manager = True
                    if self.is_shts_nho == True:
                        show_nho = True
                    datas = {
                            'date':str(date11),
                            'shift':shift_day,
                            'first_height':first_height,
                            'first_self_weight':first_self_weight,
                            'first_temp':first_temp,
                            'first_litr':first_litr,
                            'first_kg':first_kg,
                            'income_litr':income_litr,
                            'income_kg':income_kg,
                            'income_dhj':income_dhj,
                            'exp_zalin':zalin,
                            'exp_litr':exp_litr,
                            'exp_kg':exp_kg,
                            'have_litr':have_litr,
                            'have_kg':have_kg,
                            'last_heigth':last_heigth,
                            'last_self_weight':last_self_weight,
                            'last_temp':last_temp,
                            'last_litr':last_litr,
                            'last_kg':last_kg,
                            'diff_litr':diff_litr,
                            'diff_kg':diff_kg,
                            'show_manager':show_manager,
                            'show_nho':show_nho,
                            'manager_ids':manager_ids,
                            'nho_ids':nho_ids,
                            }
                    lin_data.append(datas)
        converted_in = datetime.strptime(str(self.start_date), DATE_FORMAT)
        converted_out = datetime.strptime(str(self.end_date), DATE_FORMAT)
        converted_diff = converted_out - converted_in
        diff_day = converted_diff.days + 1
        if diff_day == 0:
            diff_day = 1
        day_balance = total_kg/diff_day
        context['line_ids'] = lin_data
        context['day_balance'] = day_balance
        context['report_days'] = diff_day
        
        compose_form = self.env.ref('mn_web_portal.shts_balance_report_wizard_view')
        action = {
            'name': _(u'01 Тайлан'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'shts.balance.report.wizard.view',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action
    
    
class ShtsBalanceReportView(models.TransientModel):
    _name = "shts.balance.report.wizard.view"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,readonly=True,
                                 default=lambda self: self.env['res.company']._company_default_get('main.one.report.view'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',readonly=True)
    can_id = fields.Many2one('can.register',string=u'Сав',readonly=True)
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн',readonly=True)
    start_date = fields.Date(string=u'Эхлэх огноо',readonly=True)
    end_date = fields.Date(string=u'Дуусах огноо',readonly=True)
    can_volume = fields.Float(string=u'Савны багтаамж',readonly=True)
    day_balance = fields.Float(string=u'Дундаж хадгалалт',readonly=True)
    report_days = fields.Integer(string=u'Тайлант хоног',readonly=True)
    line_ids = fields.One2many('shts.balance.line.report.wizard.view','parent_report_id',string=u'Line')
    
    
    
    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'main.one.report' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(MainOneReportView, self).default_get(fields)
        main_report_ids = self.env['shts.balance.report.wizard'].browse(self.env.context['active_ids'])
        rec['can_id'] = main_report_ids.can_id.id
        product_id = False
        for li in main_report_ids.can_id.line_ids:
            product_id = li.product_id.id
        
        day_balance = self.env.context.get('day_balance')
        rec['product_id'] = product_id
        rec['start_date'] = main_report_ids.start_date
        rec['end_date'] = main_report_ids.end_date
        rec['can_volume'] = main_report_ids.can_id.size
        rec['shts_id'] = main_report_ids.shts_id.id
        rec['day_balance'] = day_balance
        rec['report_days'] = self.env.context.get('report_days')
        dats = self.env.context.get('line_ids')
        
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['line_ids'] = list
        return rec
    
    

    
    
    
class ShtsBalanceLineReportWizardView(models.TransientModel):
    _name = 'shts.balance.line.report.wizard.view'
    
    parent_report_id = fields.Many2one('shts.balance.report.wizard.view',string='Parent')
    date = fields.Date(string=u'Огноо')
    shift = fields.Integer(string=u'Ээлж')
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,readonly=True,
                                 default=lambda self: self.env['res.company']._company_default_get('shts.balance.line.report.wizard.view'))
    region_id = fields.Many2one('shts.region',u'Бүс',required=True)
    shts_id = fields.Many2one('shts.register',string=u'ШТС',readonly=True)
    can_id = fields.Many2one('can.register',string=u'Сав',readonly=True)
    first_height = fields.Float(string=u'Эхний үлдэгдэл өндөр',digits=(10,2))
    first_self_weight = fields.Float(string=u'Эхний үлдэгдэл ХЖ',digits=(10,4))
    first_temp = fields.Float(string=u'Эхний үлдэгдэл Темп',digits=(10,2))
    first_litr = fields.Float(string=u'Эхний үлдэгдэл Литр',digits=(10,2))
    first_kg = fields.Float(string=u'Эхний үлдэгдэл кг',digits=(10,2))
    income_litr = fields.Float(string=u'Орлого литр',digits=(10,2))
    income_kg = fields.Float(string=u'Орлого кг',digits=(10,2))
    income_dhj = fields.Float(string=u'ДХЖ',digits=(10,4),size=5)
    exp_zalin = fields.Float(string=u'Зарлага залин',digits=(10,2))
    exp_litr = fields.Float(string=u'Зарлага литр',digits=(10,2))
    exp_kg = fields.Float(string=u'Зарлага кг',digits=(10,2))
    have_litr = fields.Float(string=u'Байвал зохих литр',digits=(10,2))
    have_kg = fields.Float(string=u'Байвал зохих кг',digits=(10,2))
    last_heigth = fields.Float(string='Эцсийн үлдэгдэл өндөр',digits=(10,2))
    last_self_weight = fields.Float(string=u'Эцсийн үлдэгдэл хувийн жин',digits=(10,4),size=5)
    last_temp = fields.Float(string=u'Эцсийн үлдэгдэл темп',digits=(10,2))
    last_litr = fields.Float(string=u'Эцсийн үлдэгдэл лирт',digits=(10,2))
    last_kg = fields.Float(string=u'Эцсийн үлдэгдэл кг',digits=(10,2))
    diff_litr = fields.Float(string=u'Зөрүү литр',digits=(10,2))
    diff_kg = fields.Float(string=u'Зөрүү кг',digits=(10,2))
    manager_ids = fields.Many2many('hr.employee','shts_balance_report_manager_rel','shst_balance_id','manager_id',string='ШТС эрхлэгч')
    nho_ids = fields.Many2many('hr.employee','shts_balance_report_nho_rel','shts_balance_id','nho_id',string='ШТС НХО')
    show_nho = fields.Boolean(string='Show nho')
    show_manager = fields.Boolean(string='Show manager')
    
    
    
    
    