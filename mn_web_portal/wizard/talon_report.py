# -*- coding: utf-8 -*-

##############################################################################
from datetime import timedelta, datetime
from odoo import fields, models, _

import time
import xlwt
import openerp.netsvc

DATE_FORMAT = "%Y-%m-%d"

class TalonReport(models.TransientModel):
    _inherit = "abstract.report.excel"
    _name = "talon.report"
    _description = "Talon report"
    
    start_date = fields.Date('Эхлэх өдөр', required=True)
    end_date = fields.Date('Дуусах өдөр', required=True)
     
    
    def get_export_data(self):
       
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Тайлан БОИ талон')
        sheet.portrait = False
        
        ezxf = xlwt.easyxf
        style1 = ezxf('font: bold off; align: wrap on, vert centre, horiz center; borders: top thin, left thin, bottom thin, right thin;')
        style_table_header1 = ezxf('font: bold on; align: wrap on, vert centre, horiz center; borders: top thin, left thin, bottom thin, right thin;')
        style_table = ezxf('font: bold off; align: wrap on, vert centre, horiz center; borders: top thin, left thin, bottom thin, right thin;')
        style_table1 = ezxf('font: bold off, color red; align: wrap on, vert centre, horiz center; borders: top thin, left thin, bottom thin, right thin;')
        style_table_header = ezxf('font: bold off; align: wrap on, vert centre, horiz center; borders: top thin, left thin, bottom thin, right thin; pattern: pattern solid, fore_colour gray25;')
        sheet.write_merge(1, 1, 1, 3, u'Борлуулсан талоны мэдээлэл:', style_table_header1)
        sheet.write_merge(1, 1,  4, 13, u'Борлуулалтаар орж ирсэн талоны мэдээлэл:', style_table_header1)
        sheet.write(2, 1, u'Агуулах/ШТС', style_table_header)
        sheet.write(2, 2, u'Эцсийн хэрэглэгч', style_table_header)
        sheet.write(2, 3, u'Урамшуулал эсэх', style_table_header)
        sheet.write(2, 4, u'Огноо', style_table_header)
        sheet.write(2, 5, u'Сериал', style_table_header)
        sheet.write(2, 6, u'БО нэгж', style_table_header)
        sheet.write(2, 7, u'Компани', style_table_header)
        sheet.write(2, 8, u'Харилцагч', style_table_header)
        sheet.write(2, 9, u'Харилцагч РД', style_table_header)
        sheet.write(2, 10, u'Талоны төрөл', style_table_header)
        sheet.write(2, 11, u'Тоо хэмжээ', style_table_header)
        sheet.write(2, 12, u'Нийт дүн', style_table_header)
        sheet.write(2, 13, u'Огноо', style_table_header)
        sheet.write(2, 14, u'Зөрүү', style_table_header)
        
        #obj = self.browse(cr, uid, ids, context)
        if self.start_date < self.end_date: 
           date1 = datetime.strptime(str(self.start_date),DATE_FORMAT)
           date2 = datetime.strptime(str(self.end_date),DATE_FORMAT)
           date3 = date2 - date1
           date = int(date3.days)
           
           if date > 31:
             raise osv.except_osv(_('Анхааруулга!'),_('Хоёр огнооны зай 31-ээс бага байх ёстой !!!'))
        else:
             raise osv.except_osv(_('Анхааруулга!'),_('Дуусах огноо нь эхлэх огнооноос хойш байх ёстой !!!'))
        
        self.env.cr.execute("""Select swv.name as gazar, tv.buy_date, tv.is_bonus, rp1.name as customer1, t.name as serial, 
                              sw.name as negj, rc.name as company, rp.name as customer, rp.code, p.name as category, 
                              count(t.id)as qty, t.type_price, t.doc_date
                                      
                               from talon_inf t
                                LEFT JOIN talon_sale s on (t.sale_id = s.id)
                                LEFT JOIN talon_type p on (t.type_id = p.id)
                                LEFT JOIN shts_register sw on (sw.id = s.warehouse_id)
                                LEFT JOIN res_partner rp on (rp.id = s.partner_id)
                                LEFT JOIN talon_history th on (th.talon_id = t.id)
                                LEFT JOIN talon_view tv on (th.view_id = tv.id)
                                LEFT JOIN res_partner rp1 on (rp1.id = tv.partner_id)
                                LEFT JOIN shts_register swv on (swv.id = tv.warehouse_id)
                                LEFT JOIN res_company rc on (rc.id = sw.company_id)
                              WHERE 
                                t.state = 'sale' and th.state = 'buy' 
                                and t.doc_date >= '%s'   and t.doc_date <= '%s' 
                                group by p.name, t.type_price, t.name, sw.name, t.doc_date, s.partner_id, rc.name, rp.name, rp.code, swv.name, tv.buy_date, tv.is_bonus, rp1.name
                                order by t.doc_date asc
                         """%(self.start_date, self.end_date))
        talon = self._cr.dictfetchall()
        
        company = ''
        
        if talon:
          i = 3
          for t in talon:
              sheet.write(i, 1, u'%s' %t['gazar'], style_table)
              sheet.write(i, 2, u'%s' %t['customer1'], style_table)
              if t['is_bonus'] == '1':
                 sheet.write(i, 3, u'Урамшуулалд', style_table1)
              else:
                 sheet.write(i, 3, u'Худалдаанд', style_table)
              sheet.write(i, 4, u'%s' %t['buy_date'], style_table)
              sheet.write(i, 5, u'%s' %t['serial'], style_table)
              sheet.write(i, 6, u'%s' %t['negj'], style_table)
              sheet.write(i, 7, u'%s' %t['company'], style_table)
              sheet.write(i, 8, u'%s' %t['customer'], style_table)
              if t['code']:
                  sheet.write(i, 9, u'%s' %t['code'], style_table)
              else:
                  sheet.write(i, 9, u'', style_table)
              sheet.write(i, 10, u'%s' %t['category'], style_table)
              sheet.write(i, 11, u'%d' %t['qty'], style_table)
              sheet.write(i, 12, u'%d' %t['type_price'], style_table)
              sheet.write(i, 13, u'%s' %t['doc_date'], style_table)
              if t['customer'] == t['customer1']:
                 sheet.write(i, 14, u'', style_table)
              else:
                 sheet.write(i, 14, u'Зөрүүтэй', style_table1)
              
              i = i + 1
        
        inch = 1000
        inch1 = 900
        sheet.col(0).width = 1*inch
        sheet.col(1).width = 7*inch
        sheet.col(2).width = 8*inch
        sheet.col(3).width = 4*inch
        sheet.col(4).width = 3*inch
        sheet.col(5).width = 3*inch
        sheet.col(6).width = 5*inch
        sheet.col(7).width = 7*inch
        sheet.col(8).width = 8*inch
        sheet.col(9).width = 4*inch
        sheet.col(10).width = 3*inch
        sheet.col(12).width = 3*inch
        sheet.col(13).width = 3*inch
        sheet.col(14).width = 3*inch
        sheet.col(15).width = 3*inch
        
        
        return {'data':book}
