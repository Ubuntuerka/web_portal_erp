# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class DocPaymentTypeViewReport(models.TransientModel):
    _name = 'doc.payment.type.view.report'
    
    company_id = fields.Many2one('res.company',string='Компани')
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    start_date = fields.Date(string=u'Эхлэх огноо')
    end_date = fields.Date(string=u'Дуусах огноо')
    region_id = fields.Many2one('shts.region', string=u'ШТС бүс')
    line_ids = fields.One2many('doc.payment.type.view.line.report', 'doc_sale_view_id', string='Line')

    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'doc.payment.type.report' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(DocPaymentTypeViewReport, self).default_get(fields)
        main_report_ids = self.env['doc.payment.type.report'].browse(self.env.context['active_ids'])
        rec['company_id'] = main_report_ids.company_id.id
        rec['start_date'] = main_report_ids.start_date
        rec['end_date'] = main_report_ids.end_date
        rec['shts_id'] = main_report_ids.shts_id.id
        dats = self.env.context.get('line_ids')
        shts = []
        for li in dats:
            shts.append((0, 0, li))
        rec['line_ids'] = shts
        return rec
    
class DocPaymentTypeViewLineReport(models.TransientModel):
    _name = 'doc.payment.type.view.line.report'
    
    company_id = fields.Many2one('res.company',string=u'Компани')
    shts_id = fields.Many2one('shts.register', string=u'ШТС')
    doc_sale_view_id = fields.Many2one('doc.payment.type.view.report', string=u'Doc can view')
    gasol_sale = fields.Float(string=u'Шатахуун борлуулалт')
    shift = fields.Integer(string=u'Ээлж')
    ttm_sale = fields.Float(string=u'ТТМ-ын борлуулалт')
    service_sale = fields.Float(string=u'Авто сервисийн борлуулалт')
    date = fields.Date(string=u'Огноо')
    cash_total = fields.Float(string=u'Бэлэн')
    t_cart = fields.Float(string=u'Т Карт')
    cart = fields.Float(string=u'Карт')
    discount = fields.Float(string='Хөнгөлөлт')
    loan = fields.Float(string='Зээл')
    tuvd_card = fields.Float('Төвд карт')
    mobile = fields.Float(string=u'Шилжүүлэг')
    talon = fields.Float(string=u'Талон')
    diff = fields.Float(string=u'Зөрүү')
    total_amount = fields.Float(string=u'Нийт борлуулалт')
    
    
    
class DocPaymentTypeReport(models.TransientModel):
    """
        14 маягт саваар тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "doc.payment.type.report"
    _description = "Mile break report"
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС', default=lambda self: self.env.user.warehouse_id.id)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    region_id = fields.Many2one('shts.region', string=u'ШТС бүс')
    is_region = fields.Boolean(string=u'ШТС Бүсээр татах эсэх')
    
    def get_action_report_view(self):
        shift_obj = self.env['shift.working.register']
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        context = dict(self.env.context or {})
        lin_data = []
        shts = []
        if self.is_region == True:
            if self.region_id:
                shts_ids = self.env['shts.register'].search([('region_id', '=', self.region_id.id)])
                for sh in shts_ids:
                    shts.append(sh.id)
        else:

            if self.shts_id:
                for sh in self.shts_id:
                    shts.append(sh.id)
        for day in days:
            date1 = day.strftime('%Y-%m-%d')
            date11 = datetime.strptime(str(date1), DATE_FORMAT)
            if len(shts) == 1:
                shift_id = shift_obj.search(
                    [('shift_date', '=', date1),
                     ('state', 'in', ('confirm', 'count')), ('shts_id', '=', self.shts_id.id)],order='shift asc')
            else:
                shift_id = shift_obj.search(
                    [('shift_date', '=', date1),
                     ('state', 'in', ('confirm', 'count')), ('shts_id', 'in', tuple(shts))],order='shift asc')
            if shift_id:
                for shift in shift_id:
                    gasol_sale = 0.0
                    ttm_sale = 0.0
                    service_sale = 0.0
                    total_sale_gasol = 0.0
                    total_sale_oil = 0.0
                    total_sale_tvt = 0.0
                    total_amount = 0.0
                    cash = 0.0
                    t_card = 0.0
                    loan = 0.0
                    mobile = 0.0
                    talon = 0.0
                    tuvd_card = 0.0
                    card = 0.0
                    discount = 0.0
                    diff = 0.0
                    for gs in shift.shift_product_ids:
                        gasol_sale +=gs.total_amount
                    for ttm in shift.ttm_sale_ids:
                        ttm_sale +=ttm.total_price
                    for tvt in shift.tvt_sale_ids:
                        service_sale +=tvt.total_price
                        
                    diff = shift.total_diff_amount
                        
                    for line in shift.payment_shts_id:
                        if line.name.type == 'cash' or line.type == 'cash':
                            cash += line.amount
                        #elif line.type == 'tov_card':
                        #    t_card += line.amount
                        elif line.name.type == 'discount' or line.name.type =='discount':
                            discount += line.amount
                        elif line.name.type =='bank_card' or line.name.type == 'bank_card':
                            card += line.amount
                        elif line.name.type == 'loan':
                            loan += line.amount
                        elif line.name.type =='mobile':
                            mobile += line.amount
                        elif line.name.type == 'talon':
                            talon += line.amount
                        elif line.name.type == 'tov_card':
                            tuvd_card += line.amount
                    
                            
                    total_amount = gasol_sale+ttm_sale+service_sale
                    datas = {
                            'shts_id': shift.shts_id.id,
                            'date':date1,
                            'shift':shift.shift,
                            'gasol_sale':gasol_sale,
                            'ttm_sale':ttm_sale,
                            'service_sale':service_sale,
                            'total_amount':total_amount,
                            'cash_total':cash,
                          #  't_cart':t_card,
                            'cart':card,
                            'discount':discount,
                            'diff':diff,
                            'loan':loan,
                            'mobile':mobile,
                            'talon':talon,
                            'tuvd_card':tuvd_card,
                        }
                
                
                    lin_data.append(datas)
        context['line_ids'] = lin_data
                    
                    
        
        
        compose_form = self.env.ref('mn_web_portal.doc_payment_type_view_report_view')
        action = {
            'name': _(u'14 Тайлан төлбөрийн төрлөөр'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'doc.payment.type.view.report',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action 
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'14 Маягт төлбөрийн төрлөөр')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 4, u'ШТС: %s' % (self.shts_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 12, u'ШТС бүс: %s' % (self.region_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 0, u'Эхлэх огноо: %s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 4, u'Дуусах огноо: %s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        rowx = 5
        
        sheet.write(rowx, 0, u'ШТС', data_right)
        sheet.write(rowx, 1, u'Огноо', data_right)
        sheet.write(rowx, 2, u'Ээлж', data_right)
        sheet.write(rowx, 3, u'Шатахуун борлуулалт', data_right)
        sheet.write(rowx, 4, u'ТТМ-ын борлуулалт', data_right)
        sheet.write(rowx, 5, u'Авто сервисийн борлуулалт', data_right)
        sheet.write(rowx, 6, u'Нийт боруулалт', data_right)
        sheet.write(rowx, 7, u'Бэлэн', data_right)
        #sheet.write(rowx, 7, u'Т Карт', data_right)
        sheet.write(rowx, 8, u'Карт', data_right)
        sheet.write(rowx, 9, u'Хөнгөлөлт', data_right)
        sheet.write(rowx, 10, u'Зээл', data_right)
        #sheet.write(rowx, 12, u'Төвд карт', data_right)
        sheet.write(rowx, 11, u'Шилжүүлэг', data_right)
        sheet.write(rowx, 12, u'Талон', data_right)
        sheet.write(rowx, 13, u'Зөрүү', data_right)
        
        rowx +=1
        shift_obj = self.env['shift.working.register']
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        context = dict(self.env.context or {})
        lin_data = []
        shts = []
        if self.is_region == True:
            if self.region_id:
                shts_ids = self.env['shts.register'].search([('region_id', '=', self.region_id.id)])
                for sh in shts_ids:
                    shts.append(sh.id)
        else:

            if self.shts_id:
                for sh in self.shts_id:
                    shts.append(sh.id)
        total_gasol_sale = 0.0
        total_ttm_sale = 0.0
        total_service_sale = 0.0
        sub_total_amount = 0.0
        total_cash = 0.0
        total_card = 0.0
        total_t_card = 0.0
        total_discount = 0.0
        total_loan = 0.0
        total_tuvd_card = 0.0
        total_mobile = 0.0
        total_talon = 0.0
        total_diff = 0.0
        for day in days:
            date1 = day.strftime('%Y-%m-%d')
            date11 = datetime.strptime(str(date1), DATE_FORMAT)
            if len(shts) == 1:
                shift_id = shift_obj.search(
                    [('shift_date', '=', date1),
                     ('state', 'in', ('confirm', 'count')), ('shts_id', '=', self.shts_id.id)],order='shift asc')
            else:
                shift_id = shift_obj.search(
                    [('shift_date', '=', date1),
                     ('state', 'in', ('confirm', 'count')), ('shts_id', 'in', tuple(shts))],order='shift asc')
            if shift_id:
                for shift in shift_id:
                    gasol_sale = 0.0
                    ttm_sale = 0.0
                    service_sale = 0.0
                    total_sale_gasol = 0.0
                    total_sale_oil = 0.0
                    total_sale_tvt = 0.0
                    total_amount = 0.0
                    cash = 0.0
                    t_card = 0.0
                    loan = 0.0
                    mobile = 0.0
                    talon = 0.0
                    tuvd_card = 0.0
                    
                    diff = 0.0
                    discount = 0.0
                    card = 0.0
                    for gs in shift.shift_product_ids:
                        gasol_sale +=gs.total_amount
                    for ttm in shift.ttm_sale_ids:
                        ttm_sale +=ttm.total_price
                    for tvt in shift.tvt_sale_ids:
                        service_sale +=tvt.total_price
                        
                    diff = shift.total_diff_amount
                    for line in shift.payment_shts_id:
                        if line.name.type == 'cash' or line.type =='cash':
                            cash += line.amount
                        #elif line.type == 'tov_card':
                        #    t_card += line.amount
                        elif line.name.type == 'bank_card' or line.name.type =='bank_card':
                            card += line.amount
                        elif line.name.type == 'loan':
                            loan += line.amount
                        elif line.name.type =='mobile':
                            mobile += line.amount
                        elif line.name.type == 'talon':
                            talon += line.amount
                        elif line.name.type == 'tov_card':
                            tuvd_card += line.amount
                        elif line.name.type == 'discount' or line.name.type=='discount':
                            discount += line.amount
                    
                            
                    total_amount = gasol_sale+ttm_sale+service_sale
                    sheet.write(rowx, 0, '%s' % shift.shts_id.name, data_right)
                    sheet.write(rowx, 1, '%s'%date1, data_right)
                    sheet.write(rowx, 2, '%s'%shift.shift, data_right)
                    sheet.write(rowx, 3, round(gasol_sale,2), data_right)
                    sheet.write(rowx, 4, round(ttm_sale,2), data_right)
                    sheet.write(rowx, 5, round(service_sale,2), data_right)
                    sheet.write(rowx, 6, round(total_amount,2), data_right)
                    sheet.write(rowx, 7, round(cash,2), data_right)
                    sheet.write(rowx, 8, round(card,2), data_right)
                   # sheet.write(rowx, 8, round(t_card,2), data_right)
                    sheet.write(rowx, 9, round(discount,2), data_right)
                    sheet.write(rowx, 10, round(loan,2), data_right)
                    #sheet.write(rowx, 10,round(tuvd_card,2), data_right)
                    sheet.write(rowx, 11, round(mobile,2), data_right)
                    sheet.write(rowx, 12, round(talon,2), data_right)
                    sheet.write(rowx, 13, round(diff,2), data_right)
                    rowx +=1
                    total_gasol_sale +=gasol_sale
                    total_ttm_sale +=ttm_sale
                    total_service_sale +=service_sale
                    sub_total_amount +=total_amount
                    total_cash +=cash
                    total_card +=card
                    total_t_card +=t_card
                    total_discount +=discount
                    total_loan +=loan
                    total_tuvd_card +=tuvd_card
                    total_mobile +=mobile
                    total_talon +=talon
                    total_diff +=diff
                    
                    
                    
        sheet.write_merge(rowx, rowx, 0, 2, u'Нийт', data_right)
        sheet.write(rowx, 3, round(total_gasol_sale,2), data_right)
        sheet.write(rowx, 4, round(total_ttm_sale,2), data_right)
        sheet.write(rowx, 5, round(total_service_sale,2), data_right)
        sheet.write(rowx, 6, round(sub_total_amount,2), data_right)
        sheet.write(rowx, 7, round(total_cash,2), data_right)
        sheet.write(rowx, 8, round(total_card,2), data_right)
        #sheet.write(rowx, 8, round(total_t_card,2), data_right)
        sheet.write(rowx, 9, round(total_discount,2), data_right)
        sheet.write(rowx, 10, round(total_loan,2), data_right)
       # sheet.write(rowx, 10,round(total_tuvd_card,2), data_right)
        sheet.write(rowx, 11, round(mobile,2), data_right)
        sheet.write(rowx, 12, round(total_talon,2), data_right)
        sheet.write(rowx, 13, round(total_diff,2), data_right)
 
            
        
        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 6 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 4 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.row(4).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Миллийн заалт',
                'attache_name':'Миллийн заалт'}
