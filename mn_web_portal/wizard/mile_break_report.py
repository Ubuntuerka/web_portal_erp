# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

import time
import xlwt
import openerp.netsvc

class MileBreakReports(models.TransientModel):
    """
        Миллийн заалт тайлан
    """
    _name = "mile.break.reports"
    _description = "Mile break report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани')
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    view_ids = fields.One2many('mile.break.view.report','parent_id','Line')
    
    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'mile.break.report' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(MileBreakReports, self).default_get(fields)
        main_report_ids = self.env['mile.break.reports'].browse(self.env.context['active_ids'])        
        dats = self.env.context.get('view_ids')
        shts_id = self.env.context.get('shts_id')
        company_id = self.env.context.get('company_id')
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['view_ids'] = list
        rec['shts_id'] = shts_id
        rec['company_id'] = company_id
        return rec
    

class MileBreakViewReport(models.TransientModel):
    _name = 'mile.break.view.report'
    
    company_id = fields.Many2one('res.company',string=u'Компани',readonly=True,
                                 default=lambda self: self.env['res.company']._company_default_get('mile.break.view.report'))
    date = fields.Date(string=u'Огноо',readonly=True)
    shift = fields.Integer(string=u'Ээлж',readonly=True)
    name = fields.Char(string=u'Хошууны дугаар',readonly=True)
    first_mile = fields.Float(string=u'Эхний милл',readonly=True)
    end_mile = fields.Float(string=u'Эцсийн милл',readonly=True)
    diff_mile = fields.Float(string=u'Зөрүү милл',readonly=True)
    parent_id = fields.Many2one('mile.break.reports','Mile')
    
    
class MileBreakReport(models.TransientModel):
    """
        Миллийн заалт тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "mile.break.report"
    _description = "Mile break report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    can_id = fields.Many2one('can.register',string=u'Сав',domain="[('shts_id','=',shts_id),('is_oil_can','=',False)]",required=True)
    
   
    def action_view_report(self):
        self.env.cr.execute("""select date(sw.shift_date) as rdate, ms.first_mile, ms.end_mile, ms.diff_mile,ms.name,sw.shift
                            from mile_target_register_line as ms 
                            left join shift_working_register as sw on ms.shift_id=sw.id 
                            where date(sw.shift_date) >='%s' and date(sw.shift_date) <='%s' 
                            and sw.company_id = %s and sw.shts_id = %s and ms.can_id = %s and sw.state in ('confirm','count') order by sw.reg_date asc
                         """%(self.start_date,self.end_date,self.company_id.id,self.shts_id.id,self.can_id.id))
        mile_datas = self._cr.dictfetchall()
        context = dict(self.env.context or {})
        datas = []
        if mile_datas !=[]:
            total_diff_mile = 0.0
            for ml in mile_datas:
                data = {
                        'shift':ml['shift'],
                        'name':ml['name'],
                        'date':ml['rdate'],
                        'first_mile':ml['first_mile'],
                        'end_mile':ml['end_mile'],
                        'diff_mile':ml['end_mile']-ml['first_mile'],
                        }
                datas.append(data)
        context['view_ids'] = datas
        context['shts_id'] = self.shts_id.id
        context['company_id'] = self.company_id.id
        compose_form = self.env.ref('mn_web_portal.mile_break_reports_view_wizard')
        
        action = {
            'name': _(u'Миллийн заалт'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mile.break.reports',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Миллийн заалт')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name,), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 4, u'ШТС: %s' % (self.shts_id.name,), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write_merge(2, 2, 0, 5, u'МИЛИЙН ЗААЛТ ТАЙЛАН', ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 220'))

        rowx = 4
        sheet.write(rowx, 0, u'Огноо', data_bold)
        sheet.write(rowx, 1, u'Ээлж', data_bold)
        sheet.write(rowx, 2, u'Хошууны дугаар', data_bold)
        sheet.write(rowx, 3, u'Эхний милл', data_bold)
        sheet.write(rowx, 4, u'Эцсийн милл', data_bold)
        sheet.write(rowx, 5, u'Зөрүү', data_bold)
    
        self.env.cr.execute("""select date(sw.shift_date) as rdate, ms.first_mile, ms.end_mile, ms.diff_mile,ms.name,sw.shift
                            from mile_target_register_line as ms 
                            left join shift_working_register as sw on ms.shift_id=sw.id 
                            where date(sw.shift_date) >='%s' and date(sw.shift_date) <='%s' 
                            and sw.company_id = %s and sw.shts_id = %s and ms.can_id = %s and sw.state in ('confirm','count') order by sw.reg_date asc
                         """%(self.start_date,self.end_date,self.company_id.id,self.shts_id.id,self.can_id.id))
        mile_datas = self._cr.dictfetchall()
        rowx +=1
        if mile_datas !=[]:
            total_diff_mile = 0.0
            diff_mile = 0.0
            for ml in mile_datas:
                if ml['first_mile'] !=None and ml['end_mile']!=None:
                    diff_mile = round(ml['first_mile']-ml['end_mile'],2)
                    total_diff_mile +=ml['first_mile']-ml['end_mile']
                sheet.write(rowx, 0, '%s'%ml['rdate'], data_right)
                sheet.write(rowx, 1, '%s'%ml['shift'], data_right)
                sheet.write(rowx, 2, '%s'%ml['name'], data_right)
                sheet.write(rowx, 3, round(ml['first_mile'],2), data_right)
                sheet.write(rowx, 4, round(ml['end_mile'],2), data_right)
                sheet.write(rowx, 5, round(diff_mile,2), data_right)
                
                
                rowx +=1
            
            sheet.write_merge(rowx, rowx, 0, 4, u'Нийт', data_right)
            sheet.write_merge(rowx, rowx, 5, 5, total_diff_mile, data_right)

        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 5 * inch
        sheet.col(3).width = 4 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 4 * inch
        sheet.col(6).width = 1 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 1 * inch
        sheet.col(9).width = 3 * inch
        sheet.col(10).width = 1 * inch
        sheet.col(11).width = 3 * inch
        sheet.col(12).width = 1 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.row(2).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Миллийн заалт',
                'attache_name':'Миллийн заалт'}