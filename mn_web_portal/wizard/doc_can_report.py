# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class DocCanViewReport(models.TransientModel):
    _name = 'doc.can.view.report'
    
    company_id = fields.Many2one('res.company',string='Компани',default=lambda self: self.env['res.company']._company_default_get('doc.can.view.report'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',default=lambda self: self.env.user.warehouse_id.id)
    can_id = fields.Many2one('can.register',string=u'Сав',domain="[('shts_id','=',shts_id),('is_oil_can','=',False)]")
    start_date = fields.Date(string=u'Эхлэх огноо')
    end_date = fields.Date(string=u'Дуусах огноо')
    line_ids = fields.One2many('doc.can.view.line.report','doc_can_view_id',string='Line')
    
    
    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'doc.can.report' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(DocCanViewReport, self).default_get(fields)
        main_report_ids = self.env['doc.can.report'].browse(self.env.context['active_ids'])
        rec['can_id'] = main_report_ids.can_id.id
        rec['company_id'] = main_report_ids.company_id.id
        rec['start_date'] = main_report_ids.start_date
        rec['end_date'] = main_report_ids.end_date
        rec['shts_id'] = main_report_ids.shts_id.id
        dats = self.env.context.get('line_ids')
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['line_ids'] = list
        return rec
    
class DocCanViewLineReport(models.TransientModel):
    _name = 'doc.can.view.line.report'
    
    company_id = fields.Many2one('res.company',string='Компани')
    doc_can_view_id = fields.Many2one('doc.can.view.report',string=u'Doc can view')
    date = fields.Date(string=u'Огноо')
    shift = fields.Integer(string=u'Ээлж')
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн')
    exp_litr = fields.Float(string=u'Зарлага литр')
    exp_kg = fields.Float(string=u'Зарлага кг')
    exps_litr = fields.Float(string=u'Зардал литр')
    exps_kg = fields.Float(string=u'Зардал кг')
    bonus_litr = fields.Float(string=u'Урамшуулал литр')
    bonus_kg = fields.Float(string=u'Урамшуулал кг')
    sale_litr = fields.Float(string=u'Борлуулалт литр')
    sale_kg = fields.Float(string=u'Борлуулалт кг')
    price = fields.Float(string=u'Үнэ')
    total_amount = fields.Float(string=u'Нийт дүн')
    
class DocCanReport(models.TransientModel):
    """
        14 маягт саваар тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "doc.can.report"
    _description = "Mile break report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.shts_id.id)
    can_id = fields.Many2one('can.register',string=u'Сав',domain="[('shts_id','=',shts_id),('is_oil_can','=',False)]")
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    
    def get_action_report_view(self):
        shift_obj = self.env['shift.working.register']
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        context = dict(self.env.context or {})
        lin_data = []
        for day in days:
            date1 = day.strftime('%Y-%m-%d')
            date11 = datetime.strptime(str(date1), DATE_FORMAT)
            shift_id = shift_obj.search([('state','in',('confirm','count')),('shts_id','=',self.shts_id.id),('shift_date','=',date1)],order="shift asc")
            if shift_id:
                for shift in shift_id:
                    for line in shift.scale_ids:
                        bonus_exp_kg = 0.0
                        bonus_exp_litr = 0.0
                        other_exp_litr = 0.0
                        other_exp_kg = 0.0
                        price = 0.0
                        total = 0.0
                        sales_kg = 0.0
                        sales_litr = 0.0
                        exp_litr = 0.0
                        exp_kg = 0.0
                        total_sale_litr = 0.0
                        total_sale_kg = 0.0
                        if line.can_id.id == self.can_id.id:
                            for sh in shift.shift_product_ids:
                                if sh.product_id.id==line.product_id.id:
                                    price = sh.sale_price
                                    
                            #for ex in shift.mile_target_ids:
                            if self.can_id.id==line.can_id.id:
                                total_sale_litr +=line.total_litr
                                total_sale_kg +=line.total_kg
                            for exp in shift.loan_document_ids:
                                if line.product_id.id==exp.product_id.id and exp.expense_type_id.type in ('expense'):
                                    exp_litr +=exp.litr
                                    exp_kg +=exp.kg
                                   
                                elif line.product_id.id==exp.product_id.id and exp.expense_type_id.type in ('loan','motor'):
                                    other_exp_litr +=exp.litr
                                    other_exp_kg +=exp.kg
                                    
                                    
                                elif line.product_id.id==exp.product_id.id and exp.expense_type_id.type=='bonus':
                                    bonus_exp_litr +=exp.litr
                                    bonus_exp_kg +=exp.kg
                                    
                            sales_litr = total_sale_litr-exp_litr-other_exp_litr-bonus_exp_litr
                            total = total_sale_litr*price
                            sales_kg = total_sale_kg-exp_kg-other_exp_kg-bonus_exp_kg
                                
                            datas = {
                                    'date':date1,
                                    'shift':shift.shift,
                                    'product_id':line.product_id.id,
                                    'exp_litr':exp_litr,
                                    'exp_kg':exp_kg,
                                    'exps_litr':exp_litr,
                                    'exps_kg':exp_kg,
                                    'bonus_litr':bonus_exp_litr,
                                    'bonus_kg':bonus_exp_kg,
                                    'sale_litr':sales_litr,
                                    'sale_kg':sales_kg,
                                    'price':price,
                                    'total_amount':total,
                                }
                    
                    
                            lin_data.append(datas)
        context['line_ids'] = lin_data
                    
                    
        
        
        compose_form = self.env.ref('mn_web_portal.doc_can_view_report_view')
        action = {
            'name': _(u'14 Тайлан'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'doc.can.view.report',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action 
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'14 Маягт саваар')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 2, u'ШТС: %s' % (self.shts_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(1, 0, u'Сав: %s' % (self.can_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 0, u'Эхлэх огноо: %s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 4, u'Дуусах огноо: %s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        rowx = 4
        sheet.write_merge(rowx, rowx+1, 0, 0, u'Огноо', data_right)
        sheet.write_merge(rowx, rowx+1, 1, 1, u'Бүтээгдэхүүн', data_right)
        sheet.write_merge(rowx, rowx+1, 2, 2, u'Ээлж', data_right)
        
        sheet.write_merge(rowx, rowx, 3, 4, u'Зарлага', data_right)
        sheet.write(rowx+1, 3, u'Литр', data_right)
        sheet.write(rowx+1, 4, u'Кг', data_right)
        sheet.write_merge(rowx, rowx, 5, 6, u'Зардал', data_right)
        sheet.write(rowx+1, 5, u'Литр', data_right)
        sheet.write(rowx+1, 6, u'Кг', data_right)
        sheet.write_merge(rowx, rowx, 7, 8, u'Урамшуулал', data_right)
        sheet.write(rowx+1, 7, u'Литр', data_right)
        sheet.write(rowx+1, 8, u'Кг', data_right)
        sheet.write_merge(rowx, rowx, 9, 10, u'Борлуулалт', data_right)
        sheet.write(rowx+1, 9, u'Литр', data_right)
        sheet.write(rowx+1, 10, u'Кг', data_right)
        
        sheet.write_merge(rowx, rowx+1, 11, 11, u'Үнэ', data_right)
        sheet.write_merge(rowx, rowx+1, 12, 12, u'Нийт дүн', data_right)
        rowx +=2
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        
        shift_obj = self.env['shift.working.register']
        pro_obj = self.env['product.product']
        total_litr = 0.0
        total_kg = 0.0
        total_other_litr = 0.0
        total_other_kg = 0.0
        total_bonus_kg = 0.0
        total_bonus_litr = 0.0
        total_sum_sale_litr = 0.0
        total_sum_sale_kg = 0.0
        total_price = 0.0
        total_amount = 0.0
        for day in days:
            date1 = day.strftime('%Y-%m-%d')

            shift_id = shift_obj.search([('state','in',('confirm','count')),('shts_id','=',self.shts_id.id),('shift_date','=',date1)], order="shift asc")
            if shift_id:
                for line in shift_id:
                    for li in line.scale_ids:
                        other_exp_litr = 0.0
                        other_exp_kg = 0.0
                        bonus_exp_litr = 0.0
                        bonus_exp_kg = 0.0
                        price = 0.0
                        total = 0.0
                        sales_litr = 0.0
                        sales_kg = 0.0
                        exp_kg = 0.0
                        exp_litr = 0.0
                        litr = 0.0
                        kg = 0.0
                        total_sale_litr = 0.0
                        total_sale_kg = 0.0
                        
                        
                        if li.can_id.id == self.can_id.id:
                            sheet.write(rowx, 0, u'%s'%date1, data_right)
                            sheet.write(rowx, 1, u'%s'%li.product_id.name, data_right)
                            sheet.write(rowx, 2, u'%s'%line.shift, data_right)
                            for sh in line.shift_product_ids:
                                if sh.product_id.id==li.product_id.id:
                                    price = sh.sale_price
                                    
                            #for ex in shift.mile_target_ids:
                            if self.can_id.id==li.can_id.id:
                                total_sale_litr +=li.total_litr
                                total_sale_kg +=li.total_kg
                                
                            for exp in line.loan_document_ids:
                                if li.product_id.id==exp.product_id.id and exp.expense_type_id.type in ('expense'):
                                    exp_litr +=exp.litr
                                    exp_kg +=exp.kg
                                   
                                elif li.product_id.id==exp.product_id.id and exp.expense_type_id.type in ('loan','motor'):
                                    other_exp_litr +=exp.litr
                                    other_exp_kg +=exp.kg
                                    
                                    
                                elif li.product_id.id==exp.product_id.id and exp.expense_type_id.type=='bonus':
                                    bonus_exp_litr +=exp.litr
                                    bonus_exp_kg +=exp.kg
                                    
                            sales_litr = total_sale_litr-exp_litr-other_exp_litr-bonus_exp_litr
                            total = total_sale_litr*price
                            sales_kg = total_sale_kg-exp_kg-other_exp_kg-bonus_exp_kg
                            total_sum_sale_kg +=sales_kg
                            total_sum_sale_litr +=sales_litr
                            
                            total_amount +=total
                            
                            sheet.write(rowx, 3, round(exp_litr,2), data_right)
                            sheet.write(rowx, 4, round(exp_kg,2), data_right)
                            sheet.write(rowx, 5, round(other_exp_litr,2), data_right)
                            sheet.write(rowx, 6, round(other_exp_kg,2), data_right)
                            sheet.write(rowx, 7, round(bonus_exp_litr,2), data_right)
                            sheet.write(rowx, 8, round(bonus_exp_kg,2), data_right)
                            #sales_litr = litr-exp_litr-other_exp_litr-bonus_exp_litr
                            #sales_kg = kg-exp_kg-other_exp_kg-bonus_exp_kg
                            sheet.write(rowx, 9, round(sales_litr,2), data_right)
                            sheet.write(rowx, 10, round(sales_kg,2), data_right)
                            #total = litr*price
                            sheet.write(rowx, 11, round(price,2), data_right)
                            sheet.write(rowx, 12, round(total,2), data_right)
                            rowx +=1
        
        sheet.write_merge(rowx, rowx, 0, 2, u'Нийт', data_right)
        sheet.write(rowx, 3, round(total_litr,2), data_right)
        sheet.write(rowx, 4, round(total_kg,2), data_right)
        sheet.write(rowx, 5, round(total_other_litr,2), data_right)
        sheet.write(rowx, 6, round(total_other_kg,2), data_right)
        sheet.write(rowx, 7, round(total_bonus_litr,2), data_right)
        sheet.write(rowx, 8, round(total_bonus_kg,2), data_right)
        sheet.write(rowx, 9, round(total_sum_sale_litr,2), data_right)
        sheet.write(rowx, 10, round(total_sum_sale_kg,2), data_right)
        sheet.write(rowx, 11, round(total_price,2), data_right)
        sheet.write(rowx, 12, round(total_amount,2), data_right)
        
        

        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 2 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 5 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.row(3).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Миллийн заалт',
                'attache_name':'Миллийн заалт'}