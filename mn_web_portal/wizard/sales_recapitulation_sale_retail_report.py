# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class SalesRecapitulationSaleRetailReport(models.TransientModel):
    """
        14 маягт саваар тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "sales.recapitulation.sale.retail.report"
    _description = "Sales recapitulation sale retail report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'14 Маягт төлбөрийн төрлөөр')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        
        sheet.write(0, 3, u'Байгууллагын нэр: %s' % (company.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        #sheet.write(1, 2, u'Тайлан эхлэх огноо: %s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        #sheet.write(1, 8, u'Тайлан дуусах огноо: %s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write_merge(2, 2, 2, 10, u'ШУНХЛАЙ ТРЕЙДИНГ ХХК-НИЙ ШАТАХУУНЫ ЖИЖИГЛЭНГИЙН БОРЛУУЛАЛТЫН НЭГДСЭН ТООЦОО', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        rowx = 3

        sheet.write_merge(rowx, rowx+1, 0, 0, u'д/д', data_right)
        sheet.write_merge(rowx, rowx+1, 1, 1, u'ШТС-н нийт борлуулалт', data_right)
        sheet.write_merge(rowx, rowx, 2, 3, u'Борлуулсан шатахуун', data_right)
        sheet.write(rowx+1, 2, u'литр', data_right)
        sheet.write(rowx+1, 3, u'кг', data_right)
        sheet.write_merge(rowx, rowx+1, 4, 4, u'Залин', data_right)
        sheet.write_merge(rowx, rowx+1, 5, 5, u'Нэгжийн үнэ', data_right)
        sheet.write_merge(rowx, rowx+1, 6, 6, u'Урамшуулалд өгсөн шатахууны', data_right)
        sheet.write_merge(rowx, rowx+1, 7, 7, u'Борлуулалтын дүн', data_right)
        sheet.write_merge(rowx, rowx+1, 8, 8, u'Хий', data_right)
        sheet.write_merge(rowx, rowx+1, 9, 9, u'Нийт', data_right)
        sheet.write_merge(rowx, rowx+1, 10, 10, u'Цэвэр борлуулалт', data_right)
        sheet.write_merge(rowx, rowx+1, 11, 11, u'Борлуулалтанд ноогдох НӨАТ', data_right)
        sheet.write_merge(rowx, rowx+1, 12, 12, u'Пос-ын борлуулалт', data_right)
        sheet.write_merge(rowx, rowx+1, 13, 13, u'Зөрүү', data_right)
        sheet.write_merge(rowx, rowx, 14, 22, u'Кредитлэгдсэн дансууд', data_right)
        sheet.write(rowx+1, 14, u'Банкаар тушаасан мөнгө', data_right)
        sheet.write(rowx+1, 15, u'Замд яваа мөнгө', data_right)
        sheet.write(rowx+1, 16, u'Талоноор хийгдсэн борлуулалт', data_right)
        sheet.write(rowx+1, 17, u'Байгууллага, хувь хүнд', data_right)
        sheet.write(rowx+1, 18, u'Картын гүйлгээгээр хийгдсэн борлуулалт', data_right)
        sheet.write(rowx+1, 19, u'Картын хөнгөлөлт', data_right)
        sheet.write(rowx+1, 20, u'ХХБ посын хөнгөлөлт 3%', data_right)
        sheet.write(rowx+1, 21, u'Зардалд бичигдсэн дүн', data_right)
        sheet.write(rowx+1, 22, u'Зөрүү', data_right)
        
        
        inch = 1000
        sheet.col(0).width = 2 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 4 * inch
        sheet.col(3).width = 4 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 4 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 4 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 4 * inch
        sheet.col(13).width = 4 * inch
        sheet.col(14).width = 4 * inch
        sheet.col(15).width = 4 * inch
        sheet.col(16).width = 4 * inch
        sheet.col(17).width = 4 * inch
        sheet.col(18).width = 4 * inch
        sheet.col(19).width = 4 * inch
        sheet.col(20).width = 4 * inch
        sheet.col(21).width = 4 * inch
        sheet.col(22).width = 4 * inch


    
        sheet.row(3).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Миллийн заалт',
                'attache_name':'Миллийн заалт'}

