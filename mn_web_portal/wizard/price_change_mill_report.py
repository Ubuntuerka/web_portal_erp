# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class PriceChangeMillReport(models.TransientModel):
    """ Үнэ өөрчилсөн тайлан """
    _inherit = "abstract.report.excel"
    _name = "price.change.mill.report"
    _description = "Price change mill report"

    region_id = fields.Many2one('shts.region', 'Бүсийн нэр :', required=True)
    product_id = fields.Many2one('product.product', string=u'Шатахууны төрөл :', required=True)
    start_date = fields.Date(string=u'Эхлэх огноо :', required=True)
    end_date = fields.Date(string=u'Дуусах огноо :', required=True)
    line_ids = fields.One2many('price.change.mill.line.report', 'price_change_report_line', string='line')

    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') !='price.change.mill.report.view' or not self.env.context.get('active_ids'):
            raise UserError(_('usererror ajillahgui bn'))
        rec = super(PriceChangeMillReport, self).default_get(fields)
        price_change_ids = self.env['price.change.mill.report.view'].browse(self.env.context['active_ids'])
        rec['region_id'] = price_change_ids.region_id.id
        rec['product_id'] = price_change_ids.product_id.id
        rec['start_date'] = price_change_ids.start_date
        rec['end_date']= price_change_ids.end_date
        datas = self.env.context.get('line_ids')
        list =[]
        for da in datas:
            list.append((0,0,da))
            rec['line_ids'] = list
            return rec

class PriceChangeMillLineReport(models.TransientModel):
    _name = 'price.change.mill.line.report'

    shts_id = fields.Many2one('shts.register', 'Shts')
    region_id = fields.Many2one('shts.region', 'Region')
    product_id = fields.Many2one('product.product', 'Product', required=True, domain="[('type','=',type)]")
    old_price = fields.Float(string='Old Price', required=True, readonly=True)
    new_price = fields.Float(string='New Price', required=True)
    break_id = fields.Many2one('mile.target.register', string=u'Хошуу')
    mile_target_amount = fields.Float(string=u'Үнэ сольсон милл')
    date = fields.Datetime(string=u'Өөрчлөлт эхлэх огноо', default=fields.Date.context_today)
    price_change_report_line = fields.Many2one('price.change.mill.report', string=u'Үнэ өөрчилсөн милл')

class PriceChangeMillReportView(models.TransientModel):
    """ Үнэ өөрчилсөн тайлан """
    _inherit = "abstract.report.excel"
    _name = "price.change.mill.report.view"
    _description = "Price change mill report"

    region_id = fields.Many2one('shts.region', 'Бүсийн нэр :', required=True)
    product_id = fields.Many2one('product.product',string=u'Шатахууны төрөл :', required=True)
    start_date = fields.Date(string=u'Эхлэх огноо :', required=True)
    end_date = fields.Date(string=u'Дуусах огноо :', required=True)

    def action_view_report(self):
        '''Тайлангийн загварыг боловсруулж өгөгдлүүдийг тооцоолж байна'''

        self.env.cr.execute("""select s.id, s.region_id, p.shts_id, p.name, p.start_date, l.type, l.product_id, l.old_price, l.new_price, 
                            l.shift, g.break_id, g.mile_target_amount
                            from shts_register as s
                            left join price_register_shts as p on p.shts_id = s.id
                            left join price_register_shts_line as l on l.shts_id = p.shts_id
                            left join price_gasol_register_shts as g on g.product_id = l.product_id
                            where s.region_id = '%d' and p.shts_id = '%d' and  date(p.start_date) >='%s' and date(p.start_date) <='%s'
                            """% (self.region_id.id ,self.product_id, self.start_date, self.end_date))
        mill = self._cr.dictfetchall()
        context = dict(self.env.context or {})
        datas =[]
        if mill !=[]:
            for ml in mill:
                data ={
                    'shts_id' : ml['shts_id'],
                    'product_id' : ml['product_id'],
                    'old_price' : ml['old_price'],
                    'new_price' : ml['new_price'],
                    'break_id' : ml['break_id'],
                    'mile_target_amount' : ml['mile_target_amount'],
                    'date' : ml['date'],
                }
                datas.append(data)
        context['line_ids'] = datas
        compose_form = self.env.ref('mn_web_portal.price_change_mill_report')
        action = {
            'name' : _(u'Үнэ өөрчилсөн миллийн тайлан'),
            'type' : 'ir.actions.act_window',
            'view_mode' : 'form',
            'view_type' : 'form',
            'res_model': 'price.change.mill.report',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action

    def export_report (self):
        '''
                Тайлангийн загварыг боловсруулж өгөгдлүүдийг
                    тооцоолж байрлуулна.
                '''
