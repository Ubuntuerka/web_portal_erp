# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc


class OilMoveViewReport(models.TransientModel):
    _name = 'oil.move.view.report'
    
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    line_ids = fields.One2many('oil.move.view.line.report','oil_report_id',string=u'Line')
    
    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'oil.move.report' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(OilMoveViewReport, self).default_get(fields)
        main_report_ids = self.env['oil.move.report'].browse(self.env.context['active_ids'])        
        dats = self.env.context.get('line_ids')
        shts_id = self.env.context.get('shts_id')
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['line_ids'] = list
        rec['shts_id'] = shts_id
        rec['start_date'] = main_report_ids.start_date
        rec['end_date'] = main_report_ids.end_date
        return rec
    

class OilMoveViewLineReport(models.TransientModel):
    _name = 'oil.move.view.line.report'
    
    oil_report_id = fields.Many2one('oil.move.view.report',string='OIl')
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн',readonly=True)
    code = fields.Char(string=u'Код',readonly=True)
    uom_id = fields.Many2one('uom.uom',string=u'Хэмжих нэгж',readonly=True)
    price = fields.Float(u'Тушаал үнэ',readonly=True)
    f_qty = fields.Float(u'Эхний үлд/Тоо',readonly=True)
    f_total = fields.Float(u'Эхний үлд/Бүгд үнэ',readonly=True)
    i_qty = fields.Float(string=u'Орлого/Тоо',readonly=True)
    i_total = fields.Float(string=u'Орлого/Бүгд үнэ',readonly=True)
    e_qty = fields.Float(string=u'Зарлага/Тоо',readonly=True)
    e_total = fields.Float(string=u'Зарлага/Бүгд үнэ',readonly=True)
    oth_qty = fields.Float(string=u'Бусад Зарлага/Тоо',readonly=True)
    t_qty = fields.Float(string=u'Эцсийн үлд/Тоо',readonly=True)
    t_total = fields.Float(string=u'Эцсийн үлд/Бүгд үнэ',readonly=True)
    
    
    
    
class OilMoveReport(models.TransientModel):
    """
        Үйлчилгээний тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "oil.move.report"
    _description = "Oil move report"
    
    @api.model
    def _shts_id(self):
        shts = []
        shts_obj = self.env['shts.register']
        shts_id = shts_obj.search([('id','=',self.env.user.warehouse_id.id)])
        if shts_id:
                for line in shts_id:
                    shts.append(line.id)
        else:
            if self.env.user.shts_ids:
                for sh in self.env.user.shts_ids:
                    shts.append(sh.id)
            else:
                shts_id = shts_obj.search([('is_stock','=',False)])
                for li in shts_id:
                    shts.append(li.id)
       
        if len(shts)==1:
            return [('id', '=', shts[0])]
        else:
            return [('id', 'in', tuple(shts))]
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    shts_change = fields.Boolean(string=u'ШТС өөрчлөх эсэх')
    shts_id = fields.Many2one('shts.register',string=u'ШТС',domain=_shts_id,required=True)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    
    
    def action_view_report(self):
        context = dict(self.env.context or {})
        compose_form = self.env.ref('mn_web_portal.oil_move_view_report_view')
        product_obj = self.env['product.product']
        shts_line_obj = self.env['shts.register.line']
        product = product_obj.search([('prod_type','in',('oil','akk'))])
        context = dict(self.env.context or {})
        datas = []
        for pro in self.shts_id.oil_line_ids:
            f_qty = 0.0
            f_total = 0.0
            f_eqty = 0.0
            f_etotal = 0.0
            f_iqty = 0.0
            f_itotal = 0.0
            e_qty = 0.0
            e_total = 0.0
            oth_qty = 0.0
            i_qty = 0.0
            i_total = 0.0
            t_qty = 0.0
            t_total = 0.0
            price = 0.0
            oth_local_qty = 0.0
            self._cr.execute("""
                          SELECT sum(ipl.qty) as qty,ip.shts_id,ipl.product_id
                          FROM inventory_product as ip left join 
                          inventory_product_line as ipl on ipl.inventory_id = ip.id
                          where ip.company_id = %s and ipl.product_id = %s and ip.reg_date = '%s' and ip.shts_id = %s
                          and ip.state = 'done'
                          group by ip.shts_id,ipl.product_id
                    """%(self.company_id.id,pro.product_id.id,self.start_date,self.shts_id.id))
            f_qty_data  = self._cr.dictfetchall()
            
            self._cr.execute("""
                          SELECT sum(qty) as qty,shts_id,product_id
                          FROM product_qty
                          where company_id = %s and product_id = %s and reg_date <'%s' and shts_id = %s
                          and type = 'out'
                          group by shts_id,product_id
                    """%(self.company_id.id,pro.product_id.id,self.start_date,self.shts_id.id))
            f_exp_data  = self._cr.dictfetchall()
            
            
            self._cr.execute("""
                          SELECT sum(qty) as qty,shts_id,product_id
                          FROM product_qty
                          where company_id = %s and product_id = %s and reg_date <'%s' and shts_id = %s
                          and type = 'in'
                          group by shts_id,product_id
                    """%(self.company_id.id,pro.product_id.id,self.start_date,self.shts_id.id))
            f_inc_data  = self._cr.dictfetchall()


            
            self.env.cr.execute("""select tms.product_id, sum(tms.qty) as qty, sum(tms.price) as price, sum(tms.price*tms.qty) as total_price,sw.shift_date
                            from ttm_sale_register_line as tms 
                            left join shift_working_register as sw on tms.shift_id=sw.id
                            left join expense_type as et on tms.expense_type_id=et.id
                            where date(sw.shift_date) >='%s' and date(sw.shift_date) <='%s' 
                            and sw.company_id = %s and sw.shts_id = %s 
                            and tms.product_id = %s and et.type != 'other'
                            group by tms.product_id,sw.shift_date order by sw.shift_date asc
                         """%(self.start_date,self.end_date,self.company_id.id,self.shts_id.id,pro.product_id.id))
            exp_data = self._cr.dictfetchall()
            
            self.env.cr.execute("""select tms.product_id, sum(tms.receive_qty) as qty, sw.confirm_date
                                from ttm_income_register_line as tms 
                                left join ttm_income_register as sw on tms.income_id=sw.id 
                                where date(sw.confirm_date) >='%s' and date(sw.confirm_date) <='%s' 
                                and sw.shts_id = %s and sw.state = 'confirm' 
                                and tms.product_id = %s 
                                group by tms.product_id,sw.confirm_date order by sw.confirm_date asc
                             """%(self.start_date,self.end_date,self.shts_id.id,pro.product_id.id))
            inc_data = self._cr.dictfetchall()
            
            self.env.cr.execute("""select tms.product_id, sum(tms.qty) as qty, sum(tms.price) as price, sum(tms.price*tms.qty) as total_price,sw.shift_date
                            from ttm_sale_register_line as tms 
                            left join shift_working_register as sw on tms.shift_id=sw.id
                            left join expense_type as et on tms.expense_type_id=et.id
                            where date(sw.shift_date) >='%s' and date(sw.shift_date) <='%s' 
                            and sw.company_id = %s and sw.shts_id = %s and sw.state = 'confirm' 
                            and tms.product_id = %s and et.type = 'other'
                            group by tms.product_id,sw.shift_date order by sw.shift_date asc
                         """%(self.start_date,self.end_date,self.company_id.id,self.shts_id.id,pro.product_id.id))
            oth_data = self._cr.dictfetchall()
            
            self.env.cr.execute("""select tms.product_id, sum(tms.send_qty) as qty,sw.send_date
                                from ttm_income_register_line as tms 
                                left join ttm_income_register as sw on tms.income_id=sw.id 
                                where date(sw.send_date) >='%s' and date(sw.send_date) <='%s' 
                                and sw.name = %s and sw.state = 'confirm' 
                                and tms.product_id = %s 
                                group by tms.product_id,sw.send_date order by sw.send_date asc
                             """%(self.start_date,self.end_date,self.shts_id.id,pro.product_id.id))
            oth_local_data = self._cr.dictfetchall()
            
            if oth_local_data !=None or oth_local_data!=[]:
                for oth_local in oth_local_data:
                    oth_local_qty +=oth_local['qty']
            
            if oth_data !=None or oth_data!=[]:
                for oth in oth_data:
                    if oth['qty']!=None:
                        oth_qty +=oth['qty']
                    
            if f_qty_data !=None and f_qty_data!=[]:
                for fqty in f_qty_data:
                    if fqty['qty']!=None:
                        f_iqty +=fqty['qty']
            else:
                if f_exp_data !=None and f_exp_data!=[]:
                    for fexp in f_exp_data:
                        if fexp['qty']!=None:
                            f_eqty +=fexp['qty']
                        
                if f_inc_data !=None and f_inc_data!=[]:
                    for finc in f_inc_data:
                        if finc['qty']!=None:
                            f_iqty +=finc['qty']
                            f_itotal +=0.0

            if exp_data !=None:
                for exp in exp_data:
                    if exp['qty']!=None:
                        e_qty +=exp['qty']
                        e_total +=exp['total_price']
                    
            if inc_data !=None:
                for inc in inc_data:
                    if inc['confirm_date'] == self.start_date:
                        if inc['qty']!=None:
                            i_qty +=inc['qty']
                            f_iqty -=inc['qty']
                    else:
                        if inc['qty']!=None:
                            i_qty +=inc['qty']
                        
            f_qty = f_eqty + f_iqty
            
            shts_line_ids = shts_line_obj.search([('product_id','=',pro.product_id.id),('oil_shts_id','=',self.shts_id.id)],limit=1)
            if shts_line_ids:
                price = shts_line_ids.price
            else:
                price = 0.0
            
            if f_exp_data !=None:
                for fexp in f_exp_data:
                    if fexp['qty']!=None:
                        f_etotal +=fexp['qty']*price
                    
            i_total = i_qty*price
            f_total = f_qty*price
            t_qty = f_qty + i_qty -oth_local_qty - e_qty - oth_qty
            t_total = t_qty * price
            if e_qty > 0.0 or i_qty >0.0 or f_qty > 0.0 or f_total >0.0:
                data = {
                        'name':pro.product_id.name,
                        'product_id':pro.product_id.id,
                        'code':pro.product_id.code,
                        'uom_id':pro.product_id.uom_id.id,
                        'price':price,
                        'f_qty':f_qty,
                        'f_total':f_total,
                        'i_qty':i_qty,
                        'i_total':i_total,
                        'e_qty':e_qty,
                        'e_total':e_total,
                        'oth_qty':oth_qty+oth_local_qty,
                        't_qty':t_qty,
                        't_total':t_total,
                        }
                datas.append(data)
        context['line_ids'] = datas
        context['shts_id'] = self.shts_id.id
        
        
        action = {
            'name': _(u'Тосны хөдөлгөөний тайлан'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'oil.move.view.report',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action
        
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Тосны хөдөлгөөний тайлан')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 200'))
        
        sheet.write_merge(1, 1, 0, 13, u'ТОСНЫ ХӨДӨЛГӨӨНИЙ ТАЙЛАН', ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 220'))
        
        sheet.write(2, 10, u'Эхлэх огноо: %s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 200'))

        sheet.write(2, 12, u'Дуусах огноо: %s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 200'))

        rowx = 4
        sheet.write_merge(rowx, rowx+1, 0, 0, u'№', data_bold)
        sheet.write_merge(rowx, rowx+1, 1, 1, u'Код', data_bold)
        sheet.write_merge(rowx, rowx+1, 2, 2, u'Бүт / нэр', data_bold)
        sheet.write_merge(rowx, rowx+1, 3, 3, u'Х.Н', data_bold)
        sheet.write_merge(rowx, rowx+1, 4, 4, u'Үнэ', data_bold)
        sheet.write_merge(rowx, rowx, 5, 6, u'Эхний үлд', data_bold)
        sheet.write_merge(rowx+1, rowx+1, 5, 5, u'Тоо', data_bold)
        sheet.write_merge(rowx+1, rowx+1, 6, 6, u'Бүгд үнэ', data_bold)
        sheet.write_merge(rowx, rowx, 7, 8, u'Орлого', data_bold)
        sheet.write_merge(rowx+1, rowx+1, 7, 7, u'Тоо', data_bold)
        sheet.write_merge(rowx+1, rowx+1, 8, 8, u'Бүгд үнэ', data_bold)
        sheet.write_merge(rowx, rowx, 9, 10, u'Зарлага', data_bold)
        sheet.write_merge(rowx+1, rowx+1, 9, 9, u'Тоо', data_bold)
        sheet.write_merge(rowx+1, rowx+1, 10, 10, u'Бүгд үнэ', data_bold)
        sheet.write_merge(rowx, rowx+1, 11, 11, u'Бусад зарлага/Тоо', data_bold)
        sheet.write_merge(rowx, rowx, 12, 13, u'Эцсийн үлд', data_bold)
        sheet.write_merge(rowx+1, rowx+1, 12, 12, u'Тоо', data_bold)
        sheet.write_merge(rowx+1, rowx+1, 13, 13, u'Бүгд үнэ', data_bold)
        
        shts_line_obj = self.env['shts.register.line']
        product_obj = self.env['product.product']
        product = product_obj.search([('prod_type','in',('oil','akk'))])
        context = dict(self.env.context or {})
        datas = []
        number = 1
        rowx +=2
        total_f_qty = 0.0
        total_f_total = 0.0
        total_f_eqty = 0.0
        total_f_etotal = 0.0
        total_f_iqty = 0.0
        total_f_itotal = 0.0
        total_e_qty = 0.0
        total_e_total = 0.0
        total_oth_qty = 0.0
        total_i_qty = 0.0
        total_i_total = 0.0
        total_t_qty = 0.0
        total_t_total = 0.0
        for pro in self.shts_id.oil_line_ids:
            f_qty = 0.0
            f_total = 0.0
            f_eqty = 0.0
            f_etotal = 0.0
            f_iqty = 0.0
            f_itotal = 0.0
            e_qty = 0.0
            e_total = 0.0
            oth_qty = 0.0
            i_qty = 0.00
            i_total = 0.0
            t_qty = 0.0
            t_total = 0.0
            oth_local_qty = 0.0
            
            self._cr.execute("""
                          SELECT sum(ipl.qty) as qty,ip.shts_id,ipl.product_id
                          FROM inventory_product as ip left join 
                          inventory_product_line as ipl on ipl.inventory_id = ip.id
                          where ip.company_id = %s and ipl.product_id = %s and ip.reg_date ='%s' and ip.shts_id = %s
                          and ip.state = 'done'
                          group by ip.shts_id,ipl.product_id
                    """%(self.company_id.id,pro.product_id.id,self.start_date,self.shts_id.id))
            f_qty_data  = self._cr.dictfetchall()
            
            self._cr.execute("""
                          SELECT sum(qty) as qty,shts_id,product_id
                          FROM product_qty
                          where company_id = %s and product_id = %s and reg_date <'%s' and shts_id = %s
                          and type = 'out'
                          group by shts_id,product_id
                    """%(self.company_id.id,pro.product_id.id,self.start_date,self.shts_id.id))
            f_exp_data  = self._cr.dictfetchall()
            
            
            self._cr.execute("""
                          SELECT sum(qty) as qty,shts_id,product_id
                          FROM product_qty
                          where company_id = %s and product_id = %s and reg_date <'%s' and shts_id = %s
                          and type = 'in'
                          group by shts_id,product_id
                    """%(self.company_id.id,pro.product_id.id,self.start_date,self.shts_id.id))
            f_inc_data  = self._cr.dictfetchall()
            
            
            self.env.cr.execute("""select tms.product_id, sum(tms.qty) as qty, sum(tms.price) as price, sum(tms.price*tms.qty) as total_price,sw.shift_date
                            from ttm_sale_register_line as tms 
                            left join shift_working_register as sw on tms.shift_id=sw.id
                            left join expense_type as et on tms.expense_type_id=et.id
                            where date(sw.shift_date) >='%s' and date(sw.shift_date) <='%s' 
                            and sw.company_id = %s and sw.shts_id = %s 
                            and tms.product_id = %s and et.type != 'other'
                            group by tms.product_id,sw.shift_date order by sw.shift_date asc
                         """%(self.start_date,self.end_date,self.company_id.id,self.shts_id.id,pro.product_id.id))
            exp_data = self._cr.dictfetchall()
            self.env.cr.execute("""select tms.product_id, sum(tms.receive_qty) as qty,sw.confirm_date
                                from ttm_income_register_line as tms 
                                left join ttm_income_register as sw on tms.income_id=sw.id 
                                where date(sw.confirm_date) >='%s' and date(sw.confirm_date) <='%s' 
                                and sw.shts_id = %s and sw.state = 'confirm' 
                                and tms.product_id = %s 
                                group by tms.product_id,sw.confirm_date order by sw.confirm_date asc
                             """%(self.start_date,self.end_date,self.shts_id.id,pro.product_id.id))
            inc_data = self._cr.dictfetchall()
            
            self.env.cr.execute("""select tms.product_id, sum(tms.qty) as qty, sum(tms.price) as price, sum(tms.price*tms.qty) as total_price,sw.shift_date
                            from ttm_sale_register_line as tms 
                            left join shift_working_register as sw on tms.shift_id=sw.id
                            left join expense_type as et on tms.expense_type_id=et.id
                            where date(sw.shift_date) >='%s' and date(sw.shift_date) <='%s' 
                            and sw.company_id = %s and sw.shts_id = %s and sw.state = 'confirm' 
                            and tms.product_id = %s and et.type = 'other'
                            group by tms.product_id,sw.shift_date order by sw.shift_date asc
                         """%(self.start_date,self.end_date,self.company_id.id,self.shts_id.id,pro.product_id.id))
            oth_data = self._cr.dictfetchall()
            
            
            self.env.cr.execute("""select tms.product_id, sum(tms.receive_qty) as qty,sw.send_date
                                from ttm_income_register_line as tms 
                                left join ttm_income_register as sw on tms.income_id=sw.id 
                                where date(sw.send_date) >='%s' and date(sw.send_date) <='%s' 
                                and sw.name = %s and sw.state = 'confirm' 
                                and tms.product_id = %s 
                                group by tms.product_id,sw.send_date order by sw.send_date asc
                             """%(self.start_date,self.end_date,self.shts_id.id,pro.product_id.id))
            oth_local_data = self._cr.dictfetchall()
            
            if oth_local_data !=None or oth_local_data!=[]:
                for oth_local in oth_local_data:
                    oth_local_qty +=oth_local['qty']
            
            if oth_data !=None:
                for oth in oth_data:
                    oth_qty +=oth['qty']
                    
                    
            if f_qty_data !=None:
                for fqty in f_qty_data:
                    f_iqty +=fqty['qty']
            else:
                if f_exp_data !=None:
                    for fexp in f_exp_data:
                        f_eqty +=fexp['qty']
                        
                if f_inc_data !=None:
                    for finc in f_inc_data:
                        f_iqty +=finc['qty']
                        f_itotal +=0.0

            if exp_data !=None:
                for exp in exp_data:
                    e_qty +=exp['qty']
                    e_total +=exp['total_price']
            if inc_data !=None:
                for inc in inc_data:
                    
                    if inc['confirm_date']==self.start_date:
                        i_qty +=inc['qty']
                        f_iqty -=inc['qty']
                    else:
                        i_qty +=inc['qty']
            
            f_qty = f_eqty + f_iqty
                
            
            price = 0.0 
            shts_line_ids = shts_line_obj.search([('product_id','=',pro.product_id.id),('oil_shts_id','=',self.shts_id.id)])
            if shts_line_ids:
                for li in shts_line_ids:
                    price = li.price
            else:
                price = 0.0
            i_total = i_qty*price
            f_total = f_qty*price
            t_qty = f_qty + i_qty -oth_local_qty- e_qty - oth_qty
            t_total = t_qty * price
            if e_qty > 0.0 or i_qty >0.0 or f_qty or f_total:
                number +=1
                sheet.write(rowx,  0, '%s'%number, data_right)
                sheet.write(rowx,  1, '%s'%pro.product_id.code, data_right)
                sheet.write(rowx,  2, u'%s'%pro.product_id.name, data_right)
                sheet.write(rowx,  3, u'%s'%pro.product_id.uom_id.name, data_right)
                sheet.write(rowx,  4, round(price,2), data_right)
                sheet.write(rowx,  5, round(f_qty,2), data_right)
                sheet.write(rowx,  6, round(f_total,2), data_right)
                sheet.write(rowx,  7, round(i_qty,2), data_right)
                sheet.write(rowx,  8, round(i_total,2), data_right)
                sheet.write(rowx,  9, round(e_qty,2), data_right)
                sheet.write(rowx,  10, round(e_total,2), data_right)
                sheet.write(rowx,  11, round(oth_qty+oth_local_qty,2), data_right)
                sheet.write(rowx,  12, round(t_qty,2), data_right)
                sheet.write(rowx,  13, round(t_total,2), data_right)
                total_f_qty +=f_qty
                total_f_total +=f_total
                total_i_qty +=i_qty
                total_i_total +=i_total
                
                rowx +=1
        sheet.write_merge(rowx, rowx, 0, 4, u'Нийт', data_bold)
        sheet.write(rowx,  5, round(total_f_qty,2), data_right_bold)
        sheet.write(rowx,  6, round(total_f_total,2), data_right_bold)
        sheet.write(rowx,  7, round(total_i_qty,2), data_right_bold)
        sheet.write(rowx,  8, round(total_i_total,2), data_right_bold)
        sheet.write(rowx,  9, round(total_e_qty,2), data_right_bold)
        sheet.write(rowx,  10, round(total_e_total,2), data_right_bold)
        sheet.write(rowx,  11, round(total_oth_qty,2), data_right_bold)
        sheet.write(rowx,  12, round(total_t_qty,2), data_right_bold)
        sheet.write(rowx,  13, round(total_t_total,2), data_right_bold)
            

        inch = 1000
        sheet.col(0).width = 1 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 7 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 3 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 3 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 3 * inch
        sheet.col(9).width = 3 * inch
        sheet.col(10).width = 3 * inch
        sheet.col(11).width = 3 * inch
        sheet.col(12).width = 3 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.row(0).height = 400
        sheet.row(1).height = 400
        sheet.row(3).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Тосны хөдөлгөөний тайлан',
                'attache_name':'Тосны хөдөлгөөний тайлан'}