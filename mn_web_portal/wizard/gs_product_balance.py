# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc
from datetime import date
from datetime import timedelta
from datetime import datetime
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class GsProductBalanceForm(models.TransientModel):
    _name = "sg.product.balance.form"
    _inherit = "abstract.report.excel"

    company_id = fields.Many2one('res.company', string=u'Компани', required=True,
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register', string=u'ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id.id)
    shift_date = fields.Date(string=u'Тайлан тахах онгоо', required=True)
    shift = fields.Integer(string=u'Ээлж', required=True)

    def action_view_report(self):
        shift_obj = self.env['shift.working.register']
        shts_income_obj = self.env['shts.income']
        days = []
        date1 = datetime.strptime(str(self.shift_date), DATE_FORMAT)
        context = dict(self.env.context or {})
        lin_data = []

        for day in days:
            date1 = day.strftime('%Y-%m-%d')
            date11 = datetime.strptime(str(date1), DATE_FORMAT)
            if self.shts_id.work_time == 2:
                pdate = date11 - timedelta(hours=12)
            elif self.shts_id.work_time == 1:
                pdate = date11 - timedelta(hours=24)

            if self.shts_id.work_time == 2:
                shift_ids = shift_obj.search([('shts_id', '=', self.shts_id.id), ('shift_date', '=', date1),
                                              ('state', 'in', ('confirm', 'count'))], order="shift asc")
            else:
                pre_shift = shift_obj.search([('shts_id', '=', self.shts_id.id), ('shift_date', '=', pdate),
                                              ('state', 'in', ('confirm', 'count'))], order="shift asc")
                shift_ids = shift_obj.search([('shts_id', '=', self.shts_id.id), ('shift_date', '=', date1),
                                              ('state', 'in', ('confirm', 'count'))], order="shift asc")

            signin_day_b = datetime.strptime(str(day), DATETIME_FORMAT)
            signin_day = signin_day_b.replace(hour=0, minute=0, second=1)
            signout_day = signin_day_b.replace(hour=23, minute=59, second=59)

            if shift_ids:
                for shift_d in shift_ids:
                    income = shts_income_obj.search([('shts_id', '=', self.shts_id.id), ('shift', '=', shift_d.shift),
                                                     ('shift_date', '=', signin_day), ('state', '=', 'receive')])
                    shift_day = shift_d.shift
                    last_litr = 0.0
                    last_kg = 0.0
                    last_self_weight = 0.0
                    last_temp = 0.0
                    last_heigth = 0.0
                    pipeline_liter = 0.0
                    pipeline_kg  = 0.0


                for line in shift_d.scale_ids:
                    if line.can_id.id == self.can_id.id:
                        print('sdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddssssssssss', line.can_id)
                        if shift_d.shift == 1:
                            pre_shift = shift_obj.search(
                                [('shts_id', '=', self.shts_id.id), ('shift_date', '=', pdate), ('shift', '=', 2),
                                 ('state', 'in', ('confirm', 'count'))], order="shift asc")
                        else:
                            pre_shift = shift_obj.search(
                                [('shts_id', '=', self.shts_id.id), ('shift_date', '=', date1), ('shift', '=', 1),
                                 ('state', 'in', ('confirm', 'count'))], order="shift asc")
                            if pre_shift:
                                for li in pre_shift.scale_ids:
                                    last_temp = float(line.temperature)
                                    last_litr = float(line.litr)
                                    last_kg = float(line.kg)
                                    last_self_weight = line.self_weight_value
                                    last_heigth = line.name

                    pipeline_kg = pipeline_liter * last_self_weight
                    total_liter = last_litr + pipeline_liter
                    total_kg = last_kg + pipeline_kg

                    datas = {
                        'last_heigth': last_heigth,
                        'last_self_weight': last_self_weight,
                        'last_temp': last_temp,
                        'last_litr': last_litr,
                        'last_kg': last_kg,
                        'pipeline_kg': pipeline_kg,
                        'pipeline_liter': pipeline_liter,
                        'total_liter': total_liter,
                        'total_kg': total_kg,
                    }
                    lin_data.append(datas)
        context['fuck'] = lin_data

        compose_form = self.env.ref('mn_web_portal.sg_product_balance_wizard_view')
        action = {
            'name': _(u'СГ Бүтээгдэхүүний үлдэгдэл'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sg.product.balance.form',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action


class GsProductBalanceWizardView(models.TransientModel):
    _name = "sg.product.balance.wizard.view"

    company_id = fields.Many2one('res.company', string=u'Компани', required=True,
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register', string=u'ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id.id)
    shift_date = fields.Date(string=u'Тайлан тахах онгоо', required=True)
    shift = fields.Integer(string=u'Ээлж', required=True)
    line_ids = fields.One2many('sg.product.balance.wizard.line.view', 'balance_wizard_id', string=u'line')

    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'gs.product.balance.form' or not self.env.context.get('active_ids'):
            raise UserError(_('user error ajillahgui bn'))
        rec = super(GsProductBalanceWizardView, self).default_get(fields)
        product_balance_id = self.env['gs.product.balance.form'].browse(self.env.context['active_ids'])
        rec['company_id'] = product_balance_id.company_id.id
        rec['shts_id'] = product_balance_id.shts_id.id
        rec['shift_date'] = product_balance_id.shift_date
        rec['shift'] = product_balance_id.shift

        dats = self.env.context.get('line_ids')
        list = []
        for li in dats:
            list.append((0, 0, li))
        rec['fuck'] = list
        return rec


class SgProductBalanceWizardLineView(models.TransientModel):
    _name = 'sg.product.balance.wizard.line.view'

    balance_wizard_id = fields.Many2one('sg.product.balance.wizard.view', string='balance')
    can_id = fields.Many2one('can.register', string=u'Сав', readonly=True)
    product_id = fields.Many2one('product.product', string=u'Бүтээгдэхүүн', readonly=True)
    last_heigth = fields.Float(string='Өндөр', digits=(10, 2))
    last_self_weight = fields.Float(string=u'Хувийн жин', digits=(10, 4), size=5)
    last_temp = fields.Float(string=u'Температур', digits=(10, 2))
    last_litr = fields.Float(string=u'Лирт', digits=(10, 2))
    last_kg = fields.Float(string=u'Кг', digits=(10, 2))
    pipeline_liter = fields.Float(string='Шугам хоолойн литр')
    pipeline_kg = fields.Float(string='Шугам хоолойн кг')
    total_liter = fields.Float(string='Нийт литр')
    total_kg = fields.Float(string='Нийт кг')







