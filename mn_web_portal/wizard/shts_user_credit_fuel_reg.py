# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class ShtsUserCreditFuelRegReport(models.TransientModel):
    """
        ШТС зээлийн тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "shts.user.credit.fuel.reg.report"
    _description = "Shts user credit fuel reg report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    partner_id = fields.Many2one('res.partner',string='Partner')
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'ШТС зээлийн тайлан')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write_merge(1, 1, 0, 10, u'ШТС-аас харилцагчийн зээлээр авсан шатахууны бүртгэл', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        rowx = 3

        sheet.write(rowx, 0, u'Он', data_right)
        sheet.write(rowx, 1, u'Сар', data_right)
        sheet.write(rowx, 2, u'Өдөр', data_right)
        sheet.write(rowx, 3, u'Компани', data_right)
        sheet.write(rowx, 4, u'Харяалах бүс', data_right)
        sheet.write(rowx, 5, u'Харилцагчийн нэр', data_right)
        sheet.write(rowx, 6, u'Зээлийн зөвшөөрөл', data_right)
        sheet.write(rowx, 7, u'Машины дугаар', data_right)
        sheet.write(rowx, 8, u'Төрөл', data_right)
        sheet.write(rowx, 9, u'Шатахууны хэмжээ (₮)', data_right)
        sheet.write(rowx, 10, u'Зөвшөөрөл олгосон менежер', data_right)
        shift_obj = self.env['shift.working.register']
        shift = shift_obj.search([('state','=','confirm'),('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',self.shts_id.id)])
        for sh in shift:
            for line in sh.loan_document_ids:
                sheet.write(rowx, 0, sh.shift_date[:5], data_right)
                sheet.write(rowx, 1, sh.shift_date[6:8], data_right)
                sheet.write(rowx, 2, sh.shift_date[8:10], data_right)
                sheet.write(rowx, 3, sh.company_id.name, data_right)
                sheet.write(rowx, 4, sh.shts_id.name, data_right)
                sheet.write(rowx, 5, line.shts_id.name, data_right)
        
        
        
      
        
        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 2 * inch
        sheet.col(2).width = 2 * inch
        sheet.col(3).width = 4 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 5 * inch
        sheet.col(6).width = 5 * inch
        sheet.col(7).width = 4 * inch
        sheet.col(8).width = 3 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 5 * inch
      
        sheet.row(3).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Зээл тайлан',
                'attache_name':'Зээл тайлан'}

