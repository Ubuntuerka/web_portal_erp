# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class ShtsMoveReport(models.TransientModel):
    """
        14 маягт саваар тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "shts.move.report"
    _description = "Shts move report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    
    def get_action_report_view(self):
        
        return 
    
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'14 Маягт төлбөрийн төрлөөр')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write_merge(1, 1, 0, 10, u'Шатахууны хөдөлгөөний тайлан', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        rowx = 3

        sheet.write_merge(rowx, rowx+2, 0, 0, u'Огноо', data_right)
        sheet.write_merge(rowx, rowx+2, 1, 1, u'Нэр төрөл', data_right)
        sheet.write_merge(rowx, rowx+1, 2, 3, u'Эхний үлдэгдэл', data_right)
        sheet.write(rowx+2, 2, u'литр')
        sheet.write(rowx+2, 3, u'кг')
        sheet.write_merge(rowx, rowx+1, 4, 5, u'Орлого', data_right)
        sheet.write(rowx+2, 4, u'литр')
        sheet.write(rowx+2, 5, u'кг')
        sheet.write_merge(rowx, rowx+1, 6, 7, u'Зарлага', data_right)
        sheet.write(rowx+2, 6, u'литр')
        sheet.write(rowx+2, 7, u'кг')
        sheet.write_merge(rowx, rowx+1, 8, 9, u'СЦ-ээр гарсан бохир', data_right)
        sheet.write(rowx+2, 8, u'литр')
        sheet.write(rowx+2, 9, u'кг')
        sheet.write_merge(rowx, rowx+1, 10, 11, u'Хэвийн хорогдол', data_right)
        sheet.write(rowx+2, 10, u'литр')
        sheet.write(rowx+2, 11, u'кг')
        sheet.write_merge(rowx, rowx+1, 12, 13, u'Байвал зохих үлдэгдэл', data_right)
        sheet.write(rowx+2, 12, u'литр')
        sheet.write(rowx+2, 13, u'кг')
        sheet.write_merge(rowx, rowx+1, 14, 15, u'Бэлэн байгаа үлдэгдэл', data_right)
        sheet.write(rowx+2, 14, u'литр')
        sheet.write(rowx+2, 15, u'кг')
        sheet.write_merge(rowx, rowx, 16, 19, u'Зөрүү', data_right)
        sheet.write_merge(rowx+1, rowx+1, 16, 17, u'илүү')
        sheet.write(rowx+2, 16, u'литр')
        sheet.write(rowx+2, 17, u'кг')
        sheet.write_merge(rowx+1, rowx+1, 18, 19, u'дутуу')
        sheet.write(rowx+2, 18, u'литр')
        sheet.write(rowx+2, 19, u'кг')

      
        
        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 5 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 3 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch 
        sheet.col(16).width = 3 * inch
        sheet.col(17).width = 3 * inch
        sheet.col(18).width = 3 * inch
        sheet.col(19).width = 3 * inch        
        sheet.row(3).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Миллийн заалт',
                'attache_name':'Миллийн заалт'}

