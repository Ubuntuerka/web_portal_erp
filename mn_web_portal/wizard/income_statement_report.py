# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _
from odoo.exceptions import UserError

import time
import xlwt
import openerp.netsvc


class IncomeStatementViewReport(models.TransientModel):
    _name = 'income.statement.view.report'

    reg_user_id = fields.Many2one('res.users', string=u'Бүртгэсэн ажилтан', default=lambda self: self.env.user,
                                  required=True)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)
    product_id = fields.Many2one('product.product', string=u'Бүтээгдэхүүн')
    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    line_ids = fields.One2many('income.statement.view.report.line','income_id',string='Line')

    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') !='income.statement.report' or not self.env.context.get('active_ids'):
            raise UserError(_(u'Зөв эсэхээ шалгана уу'))
        rec = super(IncomeStatementViewReport, self).default_get(fields)
        imcome_state_id = self.env['income.statement.report'].browse(self.env.context['active_ids'])
        rec['reg_user_id'] = imcome_state_id.reg_user_id.id
        rec['company_id'] = imcome_state_id.company_id.id
        rec['product_id'] = imcome_state_id.product_id.id
        rec['start_date'] = imcome_state_id.start_date
        rec['end_date'] = imcome_state_id.end_date
        rec['reg_date'] = imcome_state_id.reg_date
        dats = self.env.context.get('line_ids')
        list =[]
        for li in dats:
            list.append((0, 0, li))
        rec['line_ids'] =list
        return rec


class IncomeStatementViewReportLine(models.TransientModel):
    _name = 'income.statement.view.report.line'

    company_id = fields.Many2one('res.company', string='Компани')
    shts_id = fields.Many2one('shts.register', string='ШТС')
    product_id = fields.Many2one('product.product', string=u'Бүтээгдэхүүн')
    income_litr = fields.Float(string=u'Илүү литр', digits=(10, 2))
    income_kg = fields.Float(string=u'Илүү кг', digits=(10, 2))
    income_id = fields.Many2one('income.statement.view.report', 'income')


class ShtsLoanSalesReport(models.TransientModel):
    _inherit = "abstract.report.excel"
    _name = 'income.statement.report'

    company_id = fields.Many2one('res.company', string='Компани')
    shts_id = fields.Many2one('shts.register', string='ШТС')
    product_id = fields.Many2one('product.product', string=u'Бүтээгдэхүүн')
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)
    

    #
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)


        ezxf = xlwt.easyxf

        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Зээлийн борлуулалт мэдээ')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'%s' % (company.name,), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write_merge(2, 2, 0, 7, u'Орлого нэгтгэлийн тайлан', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 240'))
        sheet.write_merge(3, 3, 0, 0, u'Бүтээгдэхүүн: %s'%self.product_id.name, ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 240'))
        sheet.write_merge(4, 3, 0, 0, u'Бүртгэсэн ажилтан: %s'%self.reg_user_id.name, ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 240'))
        sheet.write_merge(5, 3, 0, 0, u'Бүртгэсэн огноо: %s'%self.reg_date, ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 240'))
        sheet.write(6, 6, u'Тайлант үе: %s - %s' % (self.start_date,self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))

        rowx = 6
        sheet.write(rowx, 0, u'№', data_bold)
        sheet.write(rowx, 1, u'Компани', data_bold)
        sheet.write(rowx, 2, u'ШТС', data_bold)
        sheet.write(rowx, 3, u'Бүтээгдэхүүн', data_bold)
        sheet.write(rowx, 4, u'Илүү литр', data_bold)
        sheet.write(rowx, 5, u'Илүү кг', data_bold)

    #     shift_obj = self.env['shift.working.register']
        rowx +=1

    #     #loan_datas = self._cr.dictfetchall()
    #     context = dict(self.env.context or {})
    #     shts_obj = self.env['shts.register']
    #     company_obj = self.env['res.company']
    #     partner_obj = self.env['res.partner']
    #     product_obj = self.env['product.product']
    #     datas =[]
    #     if loan_datas !=[]:
    #         total_amount = 0.0
    #         kg = 0
    #         for ld in loan_datas:
    #
    #             shts_id = shts_obj.browse(ld['shts_id'])
    #             company_data = company_obj.browse(ld['company_id'])
    #             partner_data = partner_obj.browse(ld['partner_id'])
    #             product_data = product_obj.browse(ld['product_id'])
    #             sheet.write(rowx, 0, number, data_right)
    #             sheet.write(rowx, 1, u'%s'%company_data.name, data_right)
    #             sheet.write(rowx, 2, u'%s'%shts_id.name, data_right)
    #             sheet.write(rowx, 3, u'%s'%ld['shift_date'], data_right)
    #             sheet.write(rowx, 4, u'%s'%ld['name'], data_right)
    #             sheet.write(rowx, 5, u'%s'%(partner_data.name or ''), data_right)
    #             sheet.write(rowx, 6, u'%s'%ld['car_number_id'], data_right)
    #             sheet.write(rowx, 7, u'%s'%ld['drive_id'], data_right)
    #             sheet.write(rowx, 8, u'%s'%product_data.name, data_right)
    #             sheet.write(rowx, 9, litr, data_right)
    #             sheet.write(rowx, 10, kg, data_right)
    #             sheet.write(rowx, 11, ld['price'], data_right)
    #             sheet.write(rowx, 12, ld['total_amount'], data_right)
    #             sheet.write(rowx, 13, ld['desc'], data_right)
    #             rowx +=1

        inch = 1000
        sheet.col(0).width = 1 * inch
        sheet.col(1).width = 6 * inch
        sheet.col(2).width = 5 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 7 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 4 * inch
        sheet.col(8).width = 7 * inch
        sheet.col(9).width = 2 * inch
        sheet.col(10).width = 2 * inch
        sheet.col(11).width = 3 * inch
        sheet.col(12).width = 5 * inch
        sheet.col(13).width = 6 * inch
        sheet.row(2).height = 400

        return {'data':book, 'directory_name':u'Үйлчилгээний тайлан',
                'attache_name':'Үйлчилгээний тайлан'}
    #
    
    
    
    
    