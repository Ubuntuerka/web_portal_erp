 # -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class ComputeCalculateAktView(models.TransientModel):
    """
        Миллийн заалт тайлан
    """
    _name = "compute.calculate.akt.view"
    _description = "Mile break report"
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('compute.calculate.akt.view'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)    
    view_ids = fields.One2many('compute.calculate.akt.view.line','parent_id','Line')
    
    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'compute.calculate.akt' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(ComputeCalculateAktView, self).default_get(fields)
        main_report_ids = self.env['compute.calculate.akt.view'].browse(self.env.context['active_ids'])        
        dats = self.env.context.get('view_ids')
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['shts_id'] = self.env.context.get('shts_id')
        rec['view_ids'] = list
        rec['start_date'] = self.env.context.get('start_date')
        rec['end_date'] = self.env.context.get('end_date')
        return rec
    

class ComputeCalculateAktViewLine(models.TransientModel):
    _name = 'compute.calculate.akt.view.line'
    
    company_id = fields.Many2one('res.company',string=u'Компани',readonly=True,
                                 default=lambda self: self.env['res.company']._company_default_get('mile.break.view.report'))
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн',readonly=True)
    can_id = fields.Many2one('can.register',string=u'Сав',readonly=True)
    first_litr = fields.Float(string=u'Эхний үлдэгдэл литр',readonly=True)
    first_kg = fields.Float(string=u'Эцсийн үлдэгдэл кг',readonly=True)
    income_litr = fields.Float(string=u'Орлого литр',readonly=True)
    income_kg = fields.Float(string=u'Орлого кг',readonly=True)
    expense_litr = fields.Float(string=u'Зарлага литр',readonly=True)
    expense_kg = fields.Float(string=u'Зарлага кг',readonly=True)
    normal_litr = fields.Float(string=u'Хэвийн хорогдол литр',readonly=True)
    normal_kg = fields.Float(string=u'Хэвийн хорогдол кг',readonly=True)
    have_litr = fields.Float(string=u'Байвал зохих литр',readonly=True)
    have_kg = fields.Float(string=u'Байвал зохих кг',readonly=True)
    haved_litr = fields.Float(string=u'Бэлэн байгаа литр',readonly=True)
    haved_kg = fields.Float(string=u'Бэлэн байгаа кг',readonly=True)
    more_litr = fields.Float(string=u'Илүү литр',readonly=True)
    more_kg = fields.Float(string=u'Илүү кг',readonly=True)
    less_litr = fields.Float(string=u'Дутуу литр',readonly=True)
    less_kg = fields.Float(string=u'Дутуу кг',readonly=True)
    dhj_amount = fields.Float(string=u'ДХЖ',readonly=True,digits=(10, 4))
    parent_id = fields.Many2one('compute.calculate.akt.view','First')



class ComputeCalculateAkt(models.TransientModel):
    """
        Тооцоо бодсон акт
    """
    _inherit = "abstract.report.excel"
    _name = "compute.calculate.akt"
    _description = "Compute calculate akt report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('compute.calculate.akt'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    
    
    def action_calculate_litr(self,height,line):
        amount = 0.0
        ondor_too = int(height/10)*10
        buhel_ondor = int(height)
        tootsoo = height-buhel_ondor
        zoruu = int(height) - ondor_too
        too = 0
        aa = 0
        aaa = 0
        
        if zoruu == 0:
            too = ondor_too - 10
            for chart in line.tablits_id.line_ids:
               if too == chart.name:
                  check_value = 1
                  if tootsoo > 0:
                       aa = too + 10
                       aaa = chart.ten_mm
                  else: 
                      amount = chart.ten_mm
               if aa == chart.name:
                  amount = aaa + (chart.one_mm - aaa)*tootsoo
        else:
            if buhel_ondor == 10:
               too = ondor_too - 10
            else:
               
               too = ondor_too
            
            for chart in line.tablits_id.line_ids:
                if too ==chart.name:
                   check_value = 1
                   if zoruu == 1:
                      if tootsoo > 0:
                           amount = chart.one_mm + (chart.two_mm - chart.one_mm)*tootsoo
                      else: 
                          amount = chart.one_mm              
                   elif zoruu ==2:
                       if tootsoo > 0:
                           amount = chart.two_mm + (chart.three_mm - chart.two_mm)*tootsoo
                       else: 
                          amount = chart.two_mm 
                   elif zoruu ==3:
                       if tootsoo > 0:
                           amount = chart.three_mm + (chart.four_mm - chart.three_mm)*tootsoo
                       else: 
                          amount = chart.three_mm 
                   elif zoruu ==4:
                       if tootsoo > 0:
                           amount = chart.four_mm + (chart.five_mm - chart.four_mm)*tootsoo
                       else: 
                          amount = chart.four_mm   
                   elif zoruu ==5:
                       if tootsoo > 0:
                           amount = chart.five_mm + (chart.six_mm - chart.five_mm)*tootsoo
                       else: 
                          amount = chart.five_mm
                   elif zoruu ==6:
                       if tootsoo > 0:
                           amount = chart.six_mm + (chart.seven_mm - chart.six_mm)*tootsoo
                       else: 
                          amount = chart.six_mm
                   elif zoruu ==7:
                       if tootsoo > 0:
                           amount = chart.seven_mm + (chart.eight_mm - chart.seven_mm)*tootsoo
                       else: 
                          amount = chart.seven_mm
                   elif zoruu ==8:
                       if tootsoo > 0:
                           amount = chart.eight_mm + (chart.nine_mm - chart.eight_mm)*tootsoo
                       else: 
                          amount = chart.eight_mm
                   elif zoruu ==9:
                       if tootsoo > 0:
                           amount = chart.nine_mm + (chart.ten_mm - chart.nine_mm)*tootsoo
                       else: 
                          amount = chart.nine_mm   

            
            
        return amount
    
    
    def action_view_report(self):
        product_obj = self.env['product.product']
        product_ids = product_obj.search([('prod_type','=','gasol')])
        shift_obj = self.env['shift.working.register']
        income_obj = self.env['shts.income']
        shts_income_obj = self.env['shts.income']
        other_exp_obj = self.env['shts.income']
        normal_loss_obj = self.env['normal.loss.compute']
        gas_income_obj = self.env['gas.income']
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        context = dict(self.env.context or {})
        lin_data = []
        total_kg = 0.0
        datas = []
        can_obj = self.env['can.register']
        if product_ids !=[]:
            for pro in self.shts_id.line_ids:
                can_ids = can_obj.search([('shts_id','=',self.shts_id.id)])
                for can in can_ids:
                    shift_obj = self.env['shift.working.register']
                    first_litr = 0.0
                    first_height = 0.0
                    first_self_weight = 0.0
                    first_temp = 0.0
                    first_kg = 0.0
                    income_litr = 0.0
                    income_kg = 0.0
                    income_dhj = 0.0
                    exp_litr = 0.0
                    exp_kg = 0.0
                    have_litr = 0.0
                    have_kg = 0.0
                    last_litr = 0.0
                    last_kg = 0.0
                    diff_litr = 0.0
                    diff_kg = 0.0
                    dhg_amount = 0.0
                    zarlaga_kg = 0.0
                    zarlaga_litr = 0.0
                    income_total = 0.0
                    exp_zalin = 0.0
                    last_self_weight = 0.0
                    last_temp = 0.0
                    zalin = 0.0
                    dhj = 0.0
                    other_exp_litr = 0.0
                    other_exp_kg = 0.0
                    height_count = 1
                    last_heigth = 0.0
                    normal_kg = 0.0
                    normal_litr = 0.0
                    more_litr = 0.0
                    more_kg = 0.0
                    have_litr_l = 0.0
                    have_kg_l = 0.0
                    date_p = datetime.strptime(str(self.start_date), DATE_FORMAT)
                    if self.shts_id.work_time == 2:
                        pdate = date_p-timedelta(hours=12)
                    elif self.shts_id.work_time == 1:
                        pdate = date_p-timedelta(hours=24)
                    if self.shts_id.work_time == 2:
                        shift_idss = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',self.start_date),('state','in',('draft','confirm','count'))],order="shift asc")
                    else:
                        pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('state','in',('draft','confirm','count'))],order="shift asc")
                        shift_idss = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',self.start_date),('state','in',('draft','confirm','count'))],order="shift asc")
                    
                    if shift_idss:
                        for shifti in shift_idss:
                            if self.shts_id.work_time==2:
                                if shifti.shift == 1:
                                    pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                                else:
                                    pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                                
                            elif self.shts_id.work_time==1:
                                pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                                
                            
                        if pre_shift:
                            for li in pre_shift.scale_ids:
                                if li.product_id.id == pro.product_id.id and can.id==li.can_id.id:
                                    height_count +=1
                                    first_height = li.name
                                    first_temp = float(li.temperature)
                                    
                                    first_litr += li.litr+li.can_id.balance
                                    first_kg += li.kg + li.can_id.balance*li.self_weight_value
    
    
    
                        
                    shift_ids = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('state','in',('draft','confirm','count'))],order="shift asc")
                    
                    signin_day_b = datetime.strptime(str(self.start_date), DATE_FORMAT)
                    signout_day_b = datetime.strptime(str(self.end_date), DATE_FORMAT)
                    signin_day = signin_day_b.replace(hour=0, minute=0, second=1)
                    
                    signout_day = signout_day_b.replace(hour=23, minute=59, second=59)
                    
                    if shift_ids:
                        normal_ids = normal_loss_obj.search([('start_date','=',self.start_date),('end_date','=',self.end_date),('state','=','done'),('shts_id','=',self.shts_id.id)])
                        if self.shts_id.work_time == 2:
                            last_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift','=',2),('shift_date','>=',self.end_date),('shift_date','<=',self.end_date),('state','in',('draft','confirm','count'))],order="shift asc")
                        else:
                            last_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift','=',1),('shift_date','>=',self.end_date),('shift_date','<=',self.end_date),('state','in',('draft','confirm','count'))],order="shift asc")
    
                        for lines in last_shift.scale_ids:
                            if lines.product_id.id==pro.product_id.id and lines.can_id.id==can.id:
                                last_litr =lines.litr+lines.can_id.balance
                                last_kg =lines.kg+lines.can_id.balance*lines.self_weight_value
                        
                        for shift_d in shift_ids:
                            income = shts_income_obj.search([('shts_id','=',self.shts_id.id),('shift','=',shift_d.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','=','receive')])
                            oth_exp = shts_income_obj.search([('warehouse_id','=',self.shts_id.id),('shift','=',shift_d.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','=','receive')])
                            gas_income = gas_income_obj.search([('shts_id','=',self.shts_id.id),('shift','=',shift_d.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','not in',('cancel','draft'))])

                            for oth in oth_exp.line_ids:
                                if oth.product_id.id == pro.product_id.id and shift_d.shift_date==oth.shts_income_id.shift_date:
                                    other_exp_litr += oth.torkh_size
                                    other_exp_kg += oth.kg
                            for li in income.line_ids:
                                if li.product_id.id == pro.product_id.id and shift_d.shift_date==li.shts_income_id.shift_date and li.can_id.id==can.id:
                                    income_litr += li.hariu_litr
                                    income_kg += li.hariu_kg
                                    
                            for gli in gas_income.line_ids:
                                if gli.can_id.id == can.id and gli.product_id.id == pro.product_id.id and shift_d.shift_date==gli.gas_income_id.shift_date:
                                        income_litr += gli.hariu_litr
                                        income_kg += gli.hariu_kg
                            
                            in_num = first_litr+income_litr+last_litr
                           # if income_litr>0.0:
                           #     if in_num > 0.0:
                           #         income_dhj = round((first_kg+income_kg+last_kg)/in_num,4)
                           #     else:
                           #         income_dhj = 0.0
                           #else:
                           #     if first_litr >0.0 and last_litr >0.0:
                           #         income_dhj = round((first_kg+last_kg)/(first_litr+last_litr),4)
                           #     else:
                           #         if first_litr>0.0 and other_exp_litr>0.0:
                           #           income_dhj = round((first_kg+other_exp_kg)/(first_litr+other_exp_litr),4)
                                    
                            for exp in shift_d.mile_target_ids:
                                if exp.can_id.id==can.id and exp.product_id.id==pro.product_id.id:
                                    exp_litr += exp.expense_litr
                                    zalin +=exp.zalin
                            for shift_line in shift_d.scale_ids:
                               if shift_line.can_id.id==can.id and shift_line.product_id.id==pro.product_id.id:
                                    
                                    exp_kg += round(shift_line.total_kg,2)
                                    
                        
                        have_kg = first_kg + income_kg - exp_kg - other_exp_kg
                        have_litr = first_litr + income_litr - exp_litr - other_exp_litr           
                        zarlaga_kg = exp_litr
                        #income_dhj = 
                        
                        
                        for norm in normal_ids.line_ids:
                            if norm.product_id.id == pro.product_id.id and norm.can_id.id==can.id:
                                normal_kg +=norm.total_loss_kg
                        in_num = first_litr+income_litr+last_litr
                        if exp_litr > 0.0 and exp_kg >0.0:
                            income_dhj = round((exp_kg)/exp_litr,4)
                        else:
                            income_dhj = 0.0

                        if income_dhj >0.0:
                            normal_litr = normal_kg/income_dhj
                            
                        have_kg_l = have_kg-normal_kg
                        have_litr_l = have_litr-normal_litr
                        
                        if last_litr <have_litr_l:
                            diff_litr = last_litr - have_litr_l
                        if last_kg < have_kg_l:
                            diff_kg = last_kg - have_kg_l
                        if last_litr > have_litr_l:
                            more_litr = last_litr - have_litr_l
                        if last_kg > have_kg_l:
                            more_kg = last_kg - have_kg_l
                        
                    data = {
                            'can_id':can.id,
                            'product_id':pro.product_id.id,
                            'company_id':self.company_id.id,
                            'first_litr':first_litr,
                            'first_kg':first_kg,
                            'income_litr':income_litr,
                            'income_kg':income_kg,
                            'expense_litr':exp_litr,
                            'expense_kg':exp_kg,
                            'normal_litr':normal_litr,
                            'normal_kg':normal_kg,
                            'have_litr':have_litr_l,
                            'have_kg':have_kg_l,
                            'haved_litr':last_litr,
                            'haved_kg':last_kg,
                            'more_litr':more_litr,
                            'more_kg':more_kg,
                            'less_litr':diff_litr,
                            'less_kg':diff_kg,
                            'dhj_amount':income_dhj,
                            }
                    if first_litr !=0.0 and exp_litr!=0.0:
                        datas.append(data)
                    elif first_litr ==0.0 and exp_litr!=0.0:
                         datas.append(data)
                        
        context['view_ids'] = datas
        context['start_date'] = self.start_date
        context['end_date'] = self.end_date
        context['shts_id'] = self.shts_id.id
        compose_form = self.env.ref('mn_web_portal.compute_calculate_akt_view_wizard')
        
        action = {
            'name': _(u'Тооцоо бодсон акт'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'compute.calculate.akt.view',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Тооцоо бодсон акт')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (self.company_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 5, u'ШТС: %s' % (self.shts_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 0, u'Тайлан эхлэх огноо: %s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 5, u'Тайлан дуусах огноо: %s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 2, u'Тооцоо бодсон акт тайлан', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 220'))
        rowx = 5

        sheet.write_merge(rowx, rowx+1, 0, 0, u'Бүтээгдэхүүн', data_right)
        sheet.write_merge(rowx, rowx, 1, 2, u'Эхний үлдэгдэл', data_right)
        sheet.write(rowx+1, 1, u'литр', data_right)
        sheet.write(rowx+1, 2, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 3, 4, u'Орлого', data_right)
        sheet.write(rowx+1, 3, u'литр', data_right)
        sheet.write(rowx+1, 4, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 5, 6, u'Зарлага', data_right)
        sheet.write(rowx+1, 5, u'литр', data_right)
        sheet.write(rowx+1, 6, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 7, 8, u'Хэвийн хорогдол', data_right)
        sheet.write(rowx+1, 7, u'литр', data_right)
        sheet.write(rowx+1, 8, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 9, 10, u'Байвал зохих', data_right)
        sheet.write(rowx+1, 9, u'литр', data_right)
        sheet.write(rowx+1, 10, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 11, 12, u'Бэлэн байгаа', data_right)
        sheet.write(rowx+1, 11, u'литр', data_right)
        sheet.write(rowx+1, 12, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 13, 14, u'Илүү', data_right)
        sheet.write(rowx+1, 13, u'литр', data_right)
        sheet.write(rowx+1, 14, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 15, 16, u'Дутуу', data_right)
        sheet.write(rowx+1, 15, u'литр', data_right)
        sheet.write(rowx+1, 16, u'кг', data_right)
        sheet.write_merge(rowx,rowx+1, 17, 17, u'Дундаж хувийн жин', data_right)
        
        
        product_obj = self.env['product.product']
        product_ids = product_obj.search([('type','=','gasol')])
        shift_obj = self.env['shift.working.register']
        income_obj = self.env['shts.income']
        shts_income_obj = self.env['shts.income']
        other_exp_obj = self.env['shts.income']
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        context = dict(self.env.context or {})
        lin_data = []
        total_kg = 0.0
        rowx +=1
        
        if product_ids !=[]:
            for pro in product_ids:
                shift_obj = self.env['shift.working.register']
                first_litr = 0.0
                first_height = 0.0
                first_self_weight = 0.0
                first_temp = 0.0
                first_kg = 0.0
                income_litr = 0.0
                income_kg = 0.0
                income_dhj = 0.0
                exp_litr = 0.0
                exp_kg = 0.0
                have_litr = 0.0
                have_kg = 0.0
                last_litr = 0.0
                last_kg = 0.0
                diff_litr = 0.0
                diff_kg = 0.0
                dhg_amount = 0.0
                zarlaga_kg = 0.0
                zarlaga_litr = 0.0
                income_total = 0.0
                exp_zalin = 0.0
                last_self_weight = 0.0
                last_temp = 0.0
                zalin = 0.0
                dhj = 0.0
                other_exp_litr = 0.0
                other_exp_kg = 0.0
                height_count = 1
                last_heigth = 0.0
                normal_kg = 0.0
                normal_litr = 0.0
                more_litr = 0.0
                more_kg = 0.0
                for day in days:
                    date1 = day.strftime('%Y-%m-%d')
                    date11 = datetime.strptime(str(date1), DATE_FORMAT)
                    if self.shts_id.work_time == 2:
                        pdate = date11-timedelta(hours=12)
                    elif self.shts_id.work_time == 1:
                        pdate = date11-timedelta(hours=24)
                    
                    if self.shts_id.work_time == 2:
                        shift_ids = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',date1),('state','in',('draft','confirm','count'))],order="shift asc")
                    else:
                        pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('state','in',('draft','confirm','count'))],order="shift asc")
                        shift_ids = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',date1),('state','in',('draft','confirm','count'))],order="shift asc")
                    
                    signin_day_b = datetime.strptime(str(day), DATETIME_FORMAT)
                    signin_day = signin_day_b.replace(hour=0, minute=0, second=1)
                    signout_day = signin_day_b.replace(hour=23, minute=59, second=59)
                    
                    if shift_ids:
                        for shift_d in shift_ids:
                            income = shts_income_obj.search([('shts_id','=',self.shts_id.id),('shift','=',shift_d.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','=','receive')])
                            oth_exp = shts_income_obj.search([('warehouse_id','=',self.shts_id.id),('shift','=',shift_d.shift),('shift_date','>=',signin_day),('shift_date','<=',signout_day),('state','=','receive')])
        
                            shift_day = shift_d.shift
                            
                            for line in shift_d.scale_ids:
                                
                                if line.can_id.id == self.can_id.id:
                                    product_id = line.product_id.id
                                    if self.shts_id.work_time==2:
                                        if shift_d.shift == 1:
                                            pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                                        else:
                                            pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',date1),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                                        
                                    elif self.shts_id.work_time==1:
                                        pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',pdate),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                                        
                                    
                                    
                                    if pre_shift:
                                        for li in pre_shift.scale_ids:
                                            if li.can_id.id == self.can_id.id:
                                                height_count +=1
                                                first_height = li.name
                                                first_self_weight = li.self_weight_value
                                                first_temp = float(li.temperature)
                                                first_kg += li.kg
                                                total_kg +=li.kg
                                                first_litr += li.litr
                                        last_temp = float(line.temperature)
                                        last_litr = float(line.litr)
                                        last_kg = float(line.kg)
                                        last_self_weight = line.self_weight_value
                                        last_heigth = line.name
                                       # dhj = line.dhj_amount
                                    else:
                                        height_count +=1
                                        first_litr = self.action_calculate_litr(line.can_id.height, self.can_id)
                                        first_kg = first_litr*line.can_id.self_weight
                                        total_kg = first_litr*line.can_id.self_weight
                                        first_height = line.can_id.height
                                        first_self_weight = line.can_id.self_weight
                                        first_temp = float(line.can_id.temperature)
                                        last_temp = float(line.temperature)
                                       # dhj = line.dhj_amount
                                        last_litr = float(line.litr)
                                        last_kg = float(line.kg)
                                        last_self_weight = line.self_weight_value
                                        last_heigth = line.name
                                    
                                    for li in income.line_ids:
                                        if li.can_id.id == self.can_id.id:
                                            if li.product_id.id == line.product_id.id:
                                                income_litr += li.hariu_litr
                                                income_kg += li.hariu_kg
                                    
                                    for oth in oth_exp.line_ids:
                                        if oth.out_can_id.id == self.can_id.id:
                                            if oth.product_id.id == line.product_id.id:
                                                other_exp_litr += oth.torkh_size
                                                other_exp_kg += oth.kg
                                    
                            in_num = first_litr+income_litr+last_litr
                            if income_litr>0.0:
                                if in_num > 0.0:
                                    income_dhj = round((first_kg+income_kg+last_kg)/in_num,4)
                                else:
                                    income_dhj = 0.0
                            else:
                                if first_litr >0.0 and last_litr >0.0:
                                    income_dhj = round((first_kg+last_kg)/(first_litr+last_litr),4)
                                else:
                                    if first_litr>0.0 and other_exp_litr>0.0:
                                       income_dhj = round((first_kg+other_exp_kg)/(first_litr+other_exp_litr),4)
                            for exp in shift_d.mile_target_ids:
                               if self.can_id.id == exp.can_id.id:
                                    #exp_kg += exp.expense_kg
                                    exp_litr += exp.expense_litr
                                    zalin +=exp.zalin
                                    
                            exp_kg = round(exp_litr*income_dhj,2)
                            if exp_kg>0.0 and exp_litr>0.0:
                                last_income_dhj = round((exp_kg)/(exp_litr),4)
                            have_kg = first_kg + income_kg - exp_kg - other_exp_kg
                            have_litr = first_litr + income_litr - exp_litr - other_exp_litr           
                            zarlaga_kg = exp_litr
                            diff_litr = have_litr - last_litr
                            diff_kg = have_kg - last_kg
                            
                            
        
            sheet.write(rowx, 0, u'', data_right)
        
        

      
        
        inch = 1000
        sheet.col(0).width = 4 * inch
        sheet.col(1).width = 3 * inch
        sheet.col(2).width = 3 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 3 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 3 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 3 * inch
        sheet.col(9).width = 3 * inch
        sheet.col(10).width = 3 * inch
        sheet.col(11).width = 3 * inch
        sheet.col(12).width = 3 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.col(16).width = 3 * inch
        sheet.col(17).width = 3 * inch
        sheet.row(3).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Миллийн заалт',
                'attache_name':'Миллийн заалт'}
    
    