# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
import statistics
from odoo import models, fields, api, _

import xlwt
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class RegionalAveragePriceReportApi(models.TransientModel):
    _name = 'regional.average.price.report.api'
    _description = 'Regional average price report.api'

    region_id = fields.Many2one('shts.region', u'Бүсийн нэр', required=True)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)
    line_ids = fields.One2many('regional.average.price.report.tree', 'average_report_ids', 'line')

    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'regional.average.price.report' or not self.env.context.get(
                'active_ids'):
            raise UserError(_('UserError con not'))
        rec = super(RegionalAveragePriceReportApi, self).default_get(fields)
        average_ids = self.env['regional.average.price.report'].browse(self.env.context['active_ids'])
        rec['region_id'] = average_ids.region_id.id
        rec['start_date'] = average_ids.start_date
        rec['end_date'] = average_ids.end_date
        dats = self.env.context.get('line_ids')
        list = []
        for li in dats:
            list.append((0, 0, li))
        rec['line_ids'] = list
        return rec


class RegionalAveragePriceReportTree(models.TransientModel):
    _name = 'regional.average.price.report.tree'
    _description = 'Regional average price report.tree'

    region_id = fields.Many2one('shts.region', u'Бүсийн нэр', required=True)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)
    product_id = fields.Many2one('product.product', string=u'Бүтээгдэхүүн')
    sale_price = fields.Float(string=u'Нэгж үнэ')
    sale_litr = fields.Float(string=u'Зарсан литр')
    total_amount = fields.Float(string=u'Нийт дүн')
    revenue_price = fields.Float(string=u'Дундаж үнэ')
    average_report_ids = fields.Many2one('regional.average.price.report.api', 'average')


class RegionalAveragePriceReport(models.TransientModel):
    _name = 'regional.average.price.report'
    _description = 'Regional average price report'

    region_id = fields.Many2one('shts.region', u'Бүсийн нэр', required=True)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)

    def get_action_report_view(self, iterable=None):
        """Тайлангийн загварыг тооцоолж байна"""
        shift_obj = self.env['shift.working.register']
        shts_obj = self.env['shts.register']
        shts_line_obj = self.env['shts.register.line']
        product_obj = self.env['product.product']
        product_ids = product_obj.search([('prod_type','=','gasol')])
        shts_ids = shts_obj.search([('region_id', '=', self.region_id.id)], order='name asc')
        shts_data = []

        for sh in shts_ids:
            shts_data.append(sh.id)
        context = dict(self.env.context or {})
        lin_data = []
        if product_ids:

                for pro in product_ids:
                    print("loggggg")
                    count = 0.0
                    sale_price = 0.0
                    sale_litr = 0.0
                    total_amount = 0.0
                    total_pro_price = 0.0
                    total_pro_litr = 0.0
                    total_pro_total = 0.0
                    all_sale_litr = 0.0
                    all_total_amount = 0.0
                    revenue_price = 0.0
                    sum_total_litr = 0.0

                    if len(shts_data) > 1:
                        print("logggdddddd")
                        shift_ids = shift_obj.search(
                            [('state', 'in', ('confirm', 'count')), ('shts_id', 'in', tuple(shts_data)),
                             ('shift_date', '>=', self.start_date), ('shift_date', '<=', self.end_date)], order='shift asc')
                    else:
                        shift_ids = shift_obj.search(
                            [('state', 'in', ('confirm', 'count')), ('shts_id', '=', shts_data[0]),
                             ('shift_date', '>=', self.start_date), ('shift_date', '<=', self.end_date)], order='shift asc')

                    for shift in shift_ids:
                        print('logggggggqqqqqqq', shift)
                        for line in shift.shift_product_ids:

                            if line.product_id.id == pro.id:
                                count += 1
                                pro_name = line.product_id.name
                                total_pro_price += line.sale_price
                                total_pro_litr += line.sale_litr
                                total_pro_total += line.total_amount
                    if total_pro_price>1:
                        revenue_price = total_pro_price / count
                    datas = {
                        'product_id': pro.id,
                        'sale_litr': total_pro_litr,
                        'total_amount': total_pro_total,
                        'revenue_price': revenue_price,
                    }
                    lin_data.append(datas)
        context['line_ids'] = lin_data

        compose_form = self.env.ref('mn_web_portal.regional_average_price_report_api_view')
        action = {
                'name': _(u'Бүсийн дундаж үнэ'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'regional.average.price.report.api',
                'views': [(compose_form.id, 'form')],
                'view_id': compose_form.id,
                'target': 'new',
                'context': context,
        }
        return action

    def export_data(self):
        """Тайлангийн загварыг тооцоолж байна. Та түр хүлээнэ үү"""

        region = self.env['shts.region'].browse(self.region_id.id)

        ezxf = xlwt.easyxf

        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Бүсийн дундаж үнийн тайлан :)')
        sheet.portrait = False
        data_style = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_bold = ezxf(
            'font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_right = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_left = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf(
            'font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_center = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Бүсийн нэр: %s' % (region.name,),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 2, 0, 0, u'Эхлэх огноо: %s' % (self.start_date,),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 180'))
        sheet.write(3, 2, 0, 13, u'Дуусах огноо: %s' % (self.end_date,),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 180'))
        sheet.write_merge(2, 2, 0, 14, u'Бүсийн дундаж үнийн тайлан',
                          ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 220'))

        rowx = 5
        sheet.write(rowx, 0, u'Шатахууны төрөл', data_bold)
        sheet.write(rowx, 1, u'Мөнгөн дүн', data_bold)
        sheet.write(rowx, 2, u'Зарсан литр', data_bold)
        sheet.write(rowx, 3, u'Дундаж үнэ', data_bold)


        rowx += 1
        """Тайлангийн загварыг тооцоолж байна"""
        shift_obj = self.env['shift.working.register']
        shts_obj = self.env['shts.register']
        shts_line_obj = self.env['shts.register.line']
        product_obj = self.env['product.product']
        product_ids = product_obj.search([('prod_type','=','gasol')])
        shts_ids = shts_obj.search([('region_id', '=', self.region_id.id)], order='name asc')
        shts_data = []

        for sh in shts_ids:
            shts_data.append(sh.id)
        context = dict(self.env.context or {})
        lin_data = []
        if product_ids:

                for pro in product_ids:
                    print("loggggg")
                    count = 0.0
                    sale_price = 0.0
                    sale_litr = 0.0
                    total_amount = 0.0
                    total_pro_price = 0.0
                    total_pro_litr = 0.0
                    total_pro_total = 0.0
                    all_sale_litr = 0.0
                    all_total_amount = 0.0
                    revenue_price = 0.0
                    sum_total_litr = 0.0

                    if len(shts_data) > 1:
                        print("logggdddddd")
                        shift_ids = shift_obj.search(
                            [('state', 'in', ('confirm', 'count')), ('shts_id', 'in', tuple(shts_data)),
                             ('shift_date', '>=', self.start_date), ('shift_date', '<=', self.end_date)], order='shift asc')
                    else:
                        shift_ids = shift_obj.search(
                            [('state', 'in', ('confirm', 'count')), ('shts_id', '=', shts_data[0]),
                             ('shift_date', '>=', self.start_date), ('shift_date', '<=', self.end_date)], order='shift asc')

                    for shift in shift_ids:
                        print('logggggggqqqqqqq', shift)
                        for line in shift.shift_product_ids:

                            if line.product_id.id == pro.id:
                                count += 1
                                pro_name = line.product_id.name
                                total_pro_price += line.sale_price
                                total_pro_litr += line.sale_litr
                                total_pro_total += line.total_amount
                    if total_pro_price>1:
                        revenue_price = total_pro_price / count

                sheet.write(rowx, 0, round(pro_name, 2), data_right)
                sheet.write(rowx, 1, round(total_pro_total, 2), data_right)
                sheet.write(rowx, 2, round(total_pro_litr, 2), data_right)
                sheet.write(rowx, 3, round(revenue_price, 2), data_right)
                rowx += 1

        inch = 1000
        sheet.col(0).width = 2 * inch
        sheet.col(1).width = 7 * inch
        sheet.col(2).width = 4 * inch
        sheet.col(3).width = 4 * inch

        sheet.row(0).height = 350
        sheet.row(8).height = 700

        return {'data': book, 'directory_name': u'Бүсийн дундаж үнийн тайлан',
                'attache_name': 'Бүсийн дундаж үнийн тайлан'}




