# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _


import time
import xlwt
import openerp.netsvc

class service_view_report(models.TransientModel):
    _name = 'service.view.report'
    
    company_id = fields.Many2one('res.company',string=u'Компани',readonly=True,
                                 default=lambda self: self.env['res.company']._company_default_get('service.view.report'))
    start_date = fields.Date(string=u'Эхлэх огноо',readonly=True)
    end_date = fields.Date(string=u'Дуусах огноо',readonly=True)
    shts_id = fields.Many2one('shts.register',string=u'ШТС',readonly=True)
    line_ids = fields.One2many('service.view.line','parent_id',string='Line')
    
    
    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'service.report' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(service_view_report, self).default_get(fields)
        main_report_ids = self.env['service.report'].browse(self.env.context['active_ids'])
        rec['company_id'] = main_report_ids.company_id.id
        rec['start_date'] = main_report_ids.start_date
        rec['end_date'] = main_report_ids.end_date       
        rec['shts_id'] = main_report_ids.shts_id.id 
        dats = self.env.context.get('line_ids')
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['line_ids'] = list
        return rec
    

class service_view_line(models.TransientModel):
    _name = 'service.view.line'
    
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн')
    code = fields.Char(string=u'Код')
    uom_id = fields.Many2one('uom.uom',string=u'Хэмжих нэгж')
    qty = fields.Float(string=u'Тоо')
    price = fields.Float(string=u'Үнэ')
    total = fields.Float(string='Нийт')
    parent_id = fields.Many2one('service.view.report','Parent')
    
    
    

class service_report(models.TransientModel):
    """
        Үйлчилгээний тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "service.report"
    _description = "Service report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,
                                 default=lambda self: self.env['res.company']._company_default_get('service.report'))
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    
    def action_view_report(self):
        self.env.cr.execute("""select p.id, p.default_code, pt.uom_id, avg(s.price) as price, sum(s.qty) as qty
                            from ttm_sale_register_line_service as s 
                            left join shift_working_register as sw on s.shift_id=sw.id 
                            left join product_product as p on p.id = s.product_id
                            left join product_template as pt on pt.id=p.product_tmpl_id
                            where date(sw.reg_date) >='%s' and date(sw.reg_date) <='%s' 
                            and sw.company_id = %s and sw.shts_id = %s and sw.state != 'draft'
                            group by p.id, p.default_code, pt.uom_id
                         """%(self.start_date,self.end_date,self.company_id.id,self.shts_id.id))
        service = self._cr.dictfetchall()
        product_obj = self.env['product.product']
        uom_obj = self.env['uom.uom']
        context = dict(self.env.context or {})
        rowx = 7
        lin_data = []
        if service != []:
            for sv in service:
                total = sv['price']*sv['qty']
                datas = {
                        'product_id':sv['id'],
                        'code':sv['default_code'],
                        'uom_id':sv['uom_id'],
                        'price':sv['price'],
                        'qty':sv['qty'],
                        'total':total,
                        }
                lin_data.append(datas)
        context['line_ids'] = lin_data
        
        compose_form = self.env.ref('mn_web_portal.service_view_report_wizard_view')
        action = {
            'name': _(u'Үйлчилгээний Тайлан'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'service.view.report',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action
    
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Үйлчилгээний тайлан')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name,), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write_merge(2, 2, 0, 6, u'Үйлчилгээний тайлан', ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 240'))
        sheet.write(4, 1, u'Эхлэх огноо: %s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(4, 4, u'Дуусах огноо: %s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        
        
        rowx = 6
        sheet.write(rowx, 0, u'№', data_bold)
        sheet.write(rowx, 1, u'Код', data_bold)
        sheet.write(rowx, 2, u'Бүт / нэр', data_bold)
        sheet.write(rowx, 3, u'Х.Н', data_bold)
        sheet.write(rowx, 4, u'Үнэ', data_bold)
        sheet.write(rowx, 5, u'Тоо', data_bold)
        sheet.write(rowx, 6, u'Нийт', data_bold)
        

        
        number = 0
        self.env.cr.execute("""select p.id, p.default_code, pt.uom_id, avg(s.price) as price, sum(s.qty) as qty
                            from ttm_sale_register_line_service as s 
                            left join shift_working_register as sw on s.shift_id=sw.id 
                            left join product_product as p on p.id = s.product_id
                            left join product_template as pt on pt.id=p.product_tmpl_id
                            where date(sw.reg_date) >='%s' and date(sw.reg_date) <='%s' 
                            and sw.company_id = %s and sw.shts_id = %s and sw.state != 'draft'
                            group by p.id, p.default_code, pt.uom_id
                         """%(self.start_date,self.end_date,self.company_id.id,self.shts_id.id))
        service = self._cr.dictfetchall()
        product_obj = self.env['product.product']
        uom_obj = self.env['uom.uom']
        rowx = 7
        if service != []:
            for sv in service:
                number +=1
                sheet.write(rowx, 0, number, data_right)
                sheet.write(rowx, 1, u'%s'%sv['default_code'], data_right)
                sheet.write(rowx, 2, u'%s'%sv['name'], data_right)
                uom = uom_obj.browse(sv['uom_id'])
                sheet.write(rowx, 3, u'%s'%uom.name, data_right)
                sheet.write(rowx, 4, round(sv['price'],2), data_right)
                sheet.write(rowx, 5, round(sv['qty'],2), data_right)
                total = 0.0
                if sv['price'] !=None and sv['qty']:
                    total = sv['price']*sv['qty']
                sheet.write(rowx, 6, round(total,2), data_right)
                rowx +=1
        
        


        inch = 1000
        sheet.col(0).width = 2 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 7 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.row(2).height = 400

        return {'data':book, 'directory_name':u'Үйлчилгээний тайлан',
                'attache_name':'Үйлчилгээний тайлан'}
    
    