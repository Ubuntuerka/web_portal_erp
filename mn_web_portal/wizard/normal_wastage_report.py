# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import fields, models, _

import time
import xlwt
import openerp.netsvc

class NormalWastageReportView(models.TransientModel):
    _name = 'normal.wastage.report.view'
    
    company_id = fields.Many2one('res.company',string=u'Компани')
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    start_date = fields.Date(string=u'Эхлэх огноо')
    end_date = fields.Date(string=u'Дуусах огноо')
    line_ids = fields.One2many('normal.wastage.report.view.line','parent_id','Line')

class NormalWastageReportViewLine(models.TransientModel):
    _name = 'normal.wastage.report.view.line'
    
    parent_id = fields.Many2one('normal.wastage.report.view','Parent')
    company_id = fields.Many2one('res.company',string=u'Компани')
    product_id = fields.Many2one('product.product',string=u'Бүтээгдэхүүн')
    season = fields.Selection([('winter',u'Өвөл'),
                               ('summer',u'Зун')],string=u'Улирал')
    income_kg = fields.Float(string=u'Орлого кг')
    transport_norm = fields.Float(string=u'Тээвэрлэлтийн норм')
    transport_per = fields.Float(string=u'Тээвэрлэлтийн гүйцэтгэл')
    rec_norm = fields.Float(string=u'Хүлээн авалтын норм')
    rec_per = fields.Float(string=u'Хүлээн авалт гүйцэтгэл')
    exp_norm = fields.Float(string=u'Зарлагын норм')
    exp_per = fields.Float(string=u'Зарлагын гүйцэтгэл')
    deal_norm = fields.Float(string=u'Түгээлт норм')
    deal_per = fields.Float(string=u'Түгээлт гүйцэтгэл')
    save_norm = fields.Float(string=u'Хадгалалтын норм')
    save_day = fields.Float(string=u'Хадгалалтын норм, хоног')
    save_med = fields.Float(string=u'Хадгалалтын дундаж үлдэгдэл')
    save_per = fields.Float(string=u'Хадгалалтын гүйцэтгэл')
    total = fields.Float(string=u'Хорогдлын нийт хэмжээ')
    
    

class NormalWastageReport(models.TransientModel):
    _name = 'normal.wastage.report'
    
    company_id = fields.Many2one('res.company',string=u'Компани')
    start_date = fields.Date(string=u'Эхлэх огноо')
    end_date = fields.Date(string=u'Дуусах огноо')
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    
    
    
    
    
    