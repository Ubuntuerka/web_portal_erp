# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc
from datetime import date
from datetime import timedelta
from datetime import datetime
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class IluuDutuuReportExport(models.TransientModel):
    _name = 'iluu.dutuu.report.export'
    _inherit = "abstract.report.excel"

    reg_user_id = fields.Many2one('res.users', string=u'Бүртгэсэн ажилтан', default=lambda self: self.env.user,
                                  required=True)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)
    reg_date = fields.Datetime(string=u'Нээсэн огноо', required=True)
    product_id = fields.Many2one('product.product', string=u'Бүтээгдэхүүн')
    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    def action_view_report(self):
        shift_obj = self.env['shift.working.register']
        shts_income_obj = self.env['shts.income']
        days = []

        context = dict(self.env.context or {})
        lin_data = []
        datas = {
            'company_id': self.company_id,
            'product_id': self.product_id,
            'shts_id': shts_income_obj.shts_id,
            # 'iluu_litr':iluu_litr,
            # 'iluu_kg': self.iluu_kg,
        }
        lin_data.append(datas)
        context['fuck'] = lin_data

        compose_form = self.env.ref('mn_web_portal.iluu_dutuu_report_wizard')
        action = {
            'name': _(u'Илүү дутуу тайлан'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'iluu.dutuu.report.export',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action

    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
                    тооцоолж байрлуулна.
                '''

        company = self.env['res.company'].browse(self.company_id.id)

        ezxf = xlwt.easyxf
        styles = self.get_easyxf_styles()



class IluuDutuuReportWizard(models.TransientModel):
    _name = 'iluu.dutuu.report.wizard'
    _inherit = "abstract.report.excel"

    reg_user_id = fields.Many2one('res.users', string=u'Бүртгэсэн ажилтан', default=lambda self: self.env.user,
                                  required=True)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)
    reg_date = fields.Datetime(string=u'Нээсэн огноо', required=True)
    product_id = fields.Many2one('product.product', string=u'Бүтээгдэхүүн')
    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('shts.income'),
                                 required=True)
    line_ids = fields.One2many('iluu.dutuu.report.wizard.line','iluu_dutuu_id', string=u'line')



    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') !='iluu.dutuu.report.export' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super().default_get()
        iluu_dutuu_ids = self.env['iluu.dutuu.report.export'].browse(self.env.context['active_ids'])
        rec['company_id'] = iluu_dutuu_ids.company_id.id
        rec['reg_user_id'] = iluu_dutuu_ids.reg_user_id.id
        rec['product_id'] = iluu_dutuu_ids.product_id.id
        rec['start_date'] = iluu_dutuu_ids.start_date
        rec['end_date'] = iluu_dutuu_ids.end_date
        rec['reg_date'] = iluu_dutuu_ids.reg_date
        datas = self.env.context.get('line_ids')
        list = []
        for da in datas:
            list.append((0, 0, da))
            rec['line_ids'] = list
            return rec


class IluuDutuuReportLine(models.TransientModel):
    _name = 'iluu.dutuu.report.wizard.line'

    iluu_dutuu_id = fields.Many2one('iluu.dutuu.report.wizard', string='iluu')
    company_id = fields.Many2one('res.company', string='Компани')
    shts_id = fields.Many2one('shts.register', string='ШТС')
    product_id = fields.Many2one('product.product', string=u'Бүтээгдэхүүн')
    iluu_litr = fields.Float(string=u'Илүү литр', digits=(10, 2))
    iluu_kg = fields.Float(string=u'Илүү кг', digits=(10, 2))
