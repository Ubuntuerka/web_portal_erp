# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import date
from datetime import timedelta
from datetime import datetime
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class OilServiceScheduleCreate(models.TransientModel):
    """
        Тосны төвийн хуваарь үүсгэх
    """
    _name = "oil.service.schedule.create"
    _description = "Oil service schedule create"
    
    company_id = fields.Many2one('res.company', string='Компани',default=lambda self: self.env['res.company']._company_default_get('oil.service.schedule.create'))
    plan_id = fields.Many2one('schedule.plan', string='Хуваарь')
    start_date = fields.Date(string='Эхлэх огноо')
    end_date = fields.Date(string='Дуусах огноо')
    stop_id = fields.Many2one('oil.center.stop', string='Зогсоол')
    shts_id = fields.Many2one('shts.register', string='ШТС')
    working_hours = fields.Float(string='Ажиллах цаг')
    
    
    def action_create_plan(self):
        create_plan_obj = self.env['oil.service.schedule']
        days = []
        context = self._context
        shts_obj = self.env['shts.register']
        shts_id = self.shts_id
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        
        
        
            
        for day in days:
            now_time = 0
            start_time = 0
            end_time = 0
            count = 0
            for pl in self.plan_id.line_ids:
                week = day.weekday()+1
                if int(week) == int(pl.days):
                    start_time = pl.start_time
                    end_time = pl.end_time
                    now_time = pl.start_time
                    
                    count = (end_time - start_time)/(pl.plan_id.duration/60.0)
            
            
            for li in range(0,int(count)):
                
                start_time += self.plan_id.duration/60.0
                plan_ids = create_plan_obj.search([('state','=','free'),('plan_date','=',day),('start_time','=',now_time),('end_time','=',start_time),('stop_id','=',self.stop_id.id),('shts_id','=',shts_id.id)])
                
                time_in_hours = timedelta(hours=now_time)
                now_hours, remainder_st = divmod(time_in_hours.seconds, 3600)
                now_minutes, _ = divmod(remainder_st, 60)
                
                time_in_hours_st = timedelta(hours=start_time)
                start_hours, remainder = divmod(time_in_hours_st.seconds, 3600)
                start_minutes, _ = divmod(remainder, 60)
                
                if not plan_ids:
                    create_plan_obj.create({
                                            'company_id':self.company_id.id,
                                            "name":f"{now_hours}:{now_minutes:02d} - {start_hours}:{start_minutes:02d}",
                                            'shts_id':shts_id.id,
                                            'state':'free',
                                            'shts_code':shts_id.shts_code,
                                            'plan_date':day,
                                            'stop_id':self.stop_id.id,
                                            'start_time':now_time,
                                            'end_time':start_time,
                                            'duration':self.plan_id.duration
                                            })
            
                now_time +=self.plan_id.duration/60.0
                
        
        
        return 
        
    
    
    
    
    
    