# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc
from datetime import date
from datetime import timedelta
from datetime import datetime
from odoo import models, fields, api, _
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError



DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class ProductBalanceRegionReport(models.TransientModel):
    _inherit = "abstract.report.excel"
    _name = "product.balance.region.report"
    _description = "Product balance report"
    
    company_id = fields.Many2one('res.company', string=u'Компани', required=True,
                                default=lambda self: self.env['res.company']._company_default_get(
                                    'product.balance.report'))
    region_id = fields.Many2one('shts.region', u'Бүс')
    shts_id = fields.Many2one('shts.register', u'ШТС')
    is_region = fields.Boolean(string=u'Бүсээр татах')
    date = fields.Date(string=u'Огноо', required=True)
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Шатахууны үлдэгдлийн тайлан')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bolddata_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right_hj = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.0000_);(#,##0.0000)')

        sheet.write(0, 0, u'Компани:', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 1, u'%s'%(company.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(1, 0, u'ШТС:', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(1, 1, u'%s'%(self.shts_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 6, u'Бүс:', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 7, u'%s' % (self.region_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(1, 6, u'Огноо:', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(1, 7, u'%s' % (self.date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 4, u'Шатахууны үлдэгдлийн тайлан', ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 220'))
       
        rowx = 5
        
        sheet.write_merge(rowx, rowx+1, 0, 0, u'ШТС', data_bold)
        sheet.write_merge(rowx, rowx+1, 1, 1, u'Савны дугаар', data_bold)
        sheet.write_merge(rowx, rowx+1, 2, 2, u'Шатахууны төрөл', data_bold)
        sheet.write_merge(rowx, rowx+1, 3, 3, u'Савны багтаамж', data_bold)
        sheet.write_merge(rowx, rowx+1, 4, 4, u'Гарахгүй үлдэгдэл Л', data_bold)
        sheet.write_merge(rowx, rowx+1, 5, 5, u'Эхний үлдэгдэл кг', data_bold)
        sheet.write_merge(rowx, rowx+1, 6, 6, u'Орлого кг', data_bold)
        sheet.write_merge(rowx, rowx+1, 7, 7, u'Савны бусад зарлага кг', data_bold)
        
        sheet.write_merge(rowx, rowx, 8, 12, u'Борлуулалт', data_bold)
        sheet.write(rowx+1, 8, u'Хүнд', data_bold)
        sheet.write(rowx+1, 9, u'ДХЖ', data_bold)
        sheet.write(rowx+1, 10, u'Литр', data_bold)
        sheet.write(rowx+1, 11, u'1л үнэ', data_bold)
        sheet.write(rowx+1, 12, u'Мөнгөн дүн', data_bold)
        
        sheet.write_merge(rowx, rowx+1, 13, 13, u'Байвал зохих үлдэгдэл кг', data_bold)
        sheet.write_merge(rowx, rowx+1, 14, 14, u'Эцсийн үдэгдэл кг', data_bold)
        sheet.write_merge(rowx, rowx+1, 15, 15, u'Эцсийн үдэгдэл литр', data_bold)
        sheet.write_merge(rowx, rowx+1, 16, 16, u'Зарцуулах үлдэгдэл кг', data_bold)
        sheet.write_merge(rowx, rowx+1, 17, 17, u'Захиалсан орлого Л', data_bold)
        sheet.write_merge(rowx, rowx+1, 18, 18, u'Дүүргэлтийн хувь', data_bold)
        sheet.write_merge(rowx, rowx+1, 19, 19, u'Савны дүүргэлт хувиар', data_bold)
        sheet.write_merge(rowx, rowx+1, 20, 20, u'Захиалах', data_bold)
        sheet.write_merge(rowx, rowx+1, 21, 21, u'Шугам хоолойн литр', data_bold)
        sheet.write_merge(rowx, rowx+1, 22, 22, u'Нийт литр', data_bold)
        sheet.write_merge(rowx, rowx+1, 23, 23, u'Нийт кг', data_bold)
        
        shift_obj = self.env['shift.working.register']
        shts_income_obj = self.env['shts.income']
        shts_obj = self.env['shts.register']
        gas_income_obj = self.env['gas.income']
        days = []
        idate = datetime.strptime(str(self.date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
            
        rowx +=2
        total_kg = 0.0
        
        
        if self.is_region == True:
            if self.region_id:
                shts_ids = shts_obj.search([('region_id','=',self.region_id.id)])
        
        
        if self.company_id and self.region_id and date:
            
            for shts in shts_ids:
                if shts.work_time == 2:
                    shift_id = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shts.id),('shift','=',2),('shift_date','=',self.date),('state','in',('draft','confirm','count'))],order="shift asc")
                elif shts.work_time == 1:
                    shift_id = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shts.id),('shift','=',1),('shift_date','=',self.date),('state','in',('draft','confirm','count'))],order="shift asc")
                else:
                    shift_id = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shts.id),('shift_date','=',self.date),('state','in',('draft','confirm','count'))],order="shift asc")     
                if shift_id :
                    count = 0
                    total_hii_sorolt_ondor_huvi = 0.0
                    total_hii_sorolt_ondor_huvi =0.0
                    total_first_kg = 0.0
                    total_income_kg =0.0
                    total_other_exp_kg = 0.0
                    total_exp_litr= 0.0
                    total_exp_kg = 0.0
                    amount = 0.0
                    total_have_kg = 0.0
                    total_last_kg = 0.0
                    total_last_litr = 0.0
                    total_use_kg = 0.0
                    total_send_income_litr = 0.0
                    total_balance = 0.0
                    total_litr = 0.0
                    total_kg = 0.0
                    
                    for shift in shift_id:
                        pro_type = ''
                        product_id = 0.0
                        shts_id = 0.0
                        can_size = 0.0
                        can_code = 0.0
                        hii_sorolt_ondor_huvi = 0.0
                        first_kg = 0.0
                        other_exp_kg = 0.0
                        dhj_amount = 0.000
                        have_kg = 0.0
                        last_kg = 0.0
                        last_litr = 0.0
                        use_kg = 0.0
                        send_income_litr = 0.0
                        duurgelt_huwi = 0.0
                        can_duutgelt_huwi = 0
                        order = 0.0
                        balance = 0.0
                        total_all_kg = 0.0
                        total_all_litr = 0.0
                        count +=1
                        shts_id = shift.shts_id.name
                        for scale in shift.scale_ids:
                            if scale.product_id.prod_type=='lpg':
                               pro_type = 'lpg'
                            elif scale.product_id.prod_type=='gasol':
                                pro_type = 'gasol'
                            if pro_type == 'gasol':
                                income = shts_income_obj.search([('shts_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','not in',('cancel','draft','send'))])
                                income1 = shts_income_obj.search([('shts_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','=',('send'))])
                            elif pro_type == 'lpg':
                                income = gas_income_obj.search([('shts_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','not in',('cancel','draft','send'))])
                                income1 = gas_income_obj.search([('shts_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','=',('send'))])
                            if pro_type == 'gasol':
                                oth_exp = shts_income_obj.search([('warehouse_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','not in',('cancel','draft','send'))])
                            elif pro_type == 'lpg':
                                oth_exp = gas_income_obj.search([('warehouse_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','not in',('cancel','draft','send'))])
                        product_id = False
                        for line in shift.scale_ids:
                            product_id = line.product_id.name
                            if line.can_id.size == 0:
                                raise UserError(_(u" %s савны нийт багтаамж 0 байна. Оруулна уу!!!")% line.can_id.name)
                            can_size = line.can_id.size
                            dhj_amount = line.dhj_amount
                            can_code = line.can_id.code
                            exp_kg = 0.0
                            exp_litr = 0.0
                            sale_price = 0.0
                            total_amount = 0.0
                            hii_sorolt_ondor_huvi = line.can_id.hii_sorolt_ondor_huvi * line.self_weight_value
                            for tolbor in shift.shift_product_ids:
                                if tolbor.can_id.id == line.can_id.id and tolbor.product_id.id == line.product_id.id:
                                   exp_kg += tolbor.total_kg   
                                   exp_litr += tolbor.sale_litr      
                                   total_amount += tolbor.total_amount
                                   if exp_litr == 0:
                                       sale_price = tolbor.sale_price
                                   else:
                                       sale_price = total_amount/ exp_litr
                                    
                            if shift.shts_id.work_time==2:
                                if shift.shift == 1:
                                    pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',self.date),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                                else:
                                    pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',self.date),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                                
                            elif shift.shts_id.work_time==1:
                                pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',self.date),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                            
                            
                            income_kg = 0.0
                            oth_exp_kg = 0.0
                            for inc in income:
                            
                                
                                for li in inc.line_ids:
                                    
                                    if li.can_id.id == line.can_id.id:
                                        if li.product_id.id == line.product_id.id:
                                            #income_litr += li.hariu_litr
                                            income_kg += li.hariu_kg
                                 
                                for oth in oth_exp.line_ids:
                                    if oth.out_can_id.id == line.can_id.id:
                                        if oth.product_id.id == line.product_id.id:
                                            #oth_exp_litr += oth.torkh_size
                                            oth_exp_kg += oth.kg
                            for inco in income1:
                                for lin in inco.line_ids:
                                    if lin.product_id.id == line.product_id.id:
                                        send_income_litr = lin.torkh_size
                                    
                                            
                                
                                            
                            pdate = self.date-timedelta(hours=24)       
                               
                            if shift.shts_id.work_time==2:
                                
                                if shift_id.shift == 1:
                                    
                                    pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                                else:
                                    pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',self.date),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                                    
                            elif shift.shts_id.work_time==1:
                                pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',pdate),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                            
                               
                            if pre_shift:
                                for pre in pre_shift:
                                    for pre_id in pre.scale_ids:
                                         if pre_id.can_id.id == line.can_id.id and pre_id.product_id.id == line.product_id.id:
                                             first_kg = pre_id.kg
                                             balance = line.can_id.balance
                                             duurgelt_huwi = line.can_id.deed_duurgelt_huvi
                                             
                                             
                            
                            have_kg = first_kg + income_kg - other_exp_kg - exp_kg
                            last_kg = line.kg
                            last_litr = line.litr
                            use_kg = last_litr- hii_sorolt_ondor_huvi
                            total_all_litr =  balance + last_litr
                            total_all_kg = total_all_litr * line.self_weight_value
                            can_duutgelt_huwi = last_litr / can_size *100
                            
        
        
                           
                            sheet.write(rowx, 0, '%s'%shts_id, data_center)
                            sheet.write(rowx, 1, '%s'%can_code, data_center)
                            sheet.write(rowx, 2, '%s'%product_id, data_center)
                            sheet.write(rowx, 3, '%s'%can_size, data_center)
                            sheet.write(rowx, 4, round(hii_sorolt_ondor_huvi,2), data_center)
                            sheet.write(rowx, 5, round(first_kg,4), data_center)
                            sheet.write(rowx, 6, round(income_kg,2), data_center)
                            sheet.write(rowx, 7, round(other_exp_kg,2), data_center)
                            sheet.write(rowx, 8, round(exp_kg,2), data_center)
                            sheet.write(rowx, 9, '%s'%round(dhj_amount,4), data_right_hj)
                            sheet.write(rowx, 10, round(exp_litr,2), data_center)
                            sheet.write(rowx, 11, round(sale_price,0), data_center)
                            sheet.write(rowx, 12, round(total_amount,2), data_center)
                            sheet.write(rowx, 13, round(have_kg,2), data_center)
                            sheet.write(rowx, 14, round(last_kg,2), data_center)
                            sheet.write(rowx, 15, round(last_litr,2), data_center)
                            sheet.write(rowx, 16, round(use_kg,2), data_center)
                            sheet.write(rowx, 17, round(send_income_litr,2), data_center)
                            sheet.write(rowx, 18, round(duurgelt_huwi,2), data_center)
                            sheet.write(rowx, 19, '%s'%round(can_duutgelt_huwi,0), data_center)
                            sheet.write(rowx, 20, round(order,2), data_center)
                            sheet.write(rowx, 21, round(balance,2), data_center)
                            sheet.write(rowx, 22, round(total_all_litr,2), data_center)
                            sheet.write(rowx, 23, round(total_all_kg,2), data_center)
                            total_hii_sorolt_ondor_huvi += hii_sorolt_ondor_huvi
                            total_first_kg += first_kg
                            total_income_kg += income_kg
                            total_other_exp_kg += other_exp_kg
                            total_exp_litr += exp_litr
                            amount += total_amount
                            total_have_kg += have_kg
                            total_last_kg += last_kg
                            total_last_litr += last_litr
                            total_use_kg += use_kg
                            total_send_income_litr += send_income_litr
                            total_balance += balance
                            total_litr +=total_all_litr
                            total_kg += total_all_kg
                            
                        
                            rowx +=1
        
        else:
            if self.shts_id  and date:
                if self.shts_id.work_time == 2:
                
                    shift_id = shift_obj.search([('shts_id','=',self.shts_id.id),('shift','=',2),('shift_date','=',self.date),('state','in',('draft','confirm','count'))],order="shift asc")
                    
                elif self.shts_id.work_time == 1:
                
               
                    shift_id = shift_obj.search([('shts_id','=',self.shts_id.id),('shift','=',1),('shift_date','=',self.date),('state','in',('draft','confirm','count'))],order="shift asc")
                else:
                
                    shift_id = shift_obj.search([('shts_id','=',self.shts_id.id),('shift_date','=',self.date),('state','in',('draft','confirm','count'))],order="shift asc")
      
                if shift_id :
                    count = 0
                    
                    total_hii_sorolt_ondor_huvi =0.0
                    total_first_kg = 0.0
                    total_income_kg =0.0
                    total_other_exp_kg = 0.0
                    total_exp_litr= 0.0
                    total_exp_kg = 0.0
                    amount = 0.0
                    total_have_kg = 0.0
                    total_last_kg = 0.0
                    total_last_litr = 0.0
                    total_use_kg = 0.0
                    total_send_income_litr = 0.0
                    total_balance = 0.0
                    total_litr = 0.0
                    total_kg = 0.0
                    for shift in shift_id:
                        pro_type = ''
                        can_code = 0.0
                        product_id = 0.0
                        shts_id = 0.0
                        can_size = 0.0
                        hii_sorolt_ondor_huvi = 0.0
                        first_kg = 0.0
                        other_exp_kg = 0.0
                        dhj_amount = 0.000
                        have_kg = 0.0
                        last_kg = 0.0
                        last_litr = 0.0
                        use_kg = 0.0
                        send_income_litr = 0.0
                        duurgelt_huwi = 0.0
                        can_duutgelt_huwi = 0
                        order = 0.0
                        balance = 0.0
                        total_all_kg = 0.0
                        total_all_litr = 0.0
                        count +=1
                        shts_id = shift.shts_id.name
                        for scale in shift.scale_ids:
                            if scale.product_id.prod_type=='lpg':
                               pro_type = 'lpg'
                            elif scale.product_id.prod_type=='gasol':
                                pro_type = 'gasol'
                            if pro_type == 'gasol':
                                income = shts_income_obj.search([('shts_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','not in',('cancel','draft','send'))])
                                income1 = shts_income_obj.search([('shts_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','=',('send'))])
                            elif pro_type == 'lpg':
                                income = gas_income_obj.search([('shts_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','not in',('cancel','draft','send'))])
                                income1 = gas_income_obj.search([('shts_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','=',('send'))])
                            if pro_type == 'gasol':
                                oth_exp = shts_income_obj.search([('warehouse_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','not in',('cancel','draft','send'))])
                            elif pro_type == 'lpg':
                                oth_exp = gas_income_obj.search([('warehouse_id','=',shift.shts_id.id),('shift','=',shift.shift),('shift_date','=',self.date),('state','not in',('cancel','draft','send'))])
                        product_id = False
                        for line in shift.scale_ids:
                            product_id = line.product_id.name
                            can_size = line.can_id.size
                            dhj_amount = line.dhj_amount
                            can_code = line.can_id.code
                            exp_kg = 0.0
                            exp_litr = 0.0
                            sale_price = 0.0
                            total_amount = 0.0
                            hii_sorolt_ondor_huvi = line.can_id.hii_sorolt_ondor_huvi * line.self_weight_value
                            for tolbor in shift.shift_product_ids:
                                if tolbor.can_id.id == line.can_id.id and tolbor.product_id.id == line.product_id.id:
                                   exp_kg += tolbor.total_kg   
                                   exp_litr += tolbor.sale_litr      
                                   total_amount += tolbor.total_amount
                                   if exp_litr == 0:
                                       sale_price = tolbor.sale_price
                                   else:
                                       sale_price = total_amount/ exp_litr
                                    
                            if shift.shts_id.work_time==2:
                                if shift.shift == 1:
                                    pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',self.date),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                                else:
                                    pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',self.date),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                                
                            elif shift.shts_id.work_time==1:
                                pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',self.date),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                            
                            
                            income_kg = 0.0
                            oth_exp_kg = 0.0
                            for inc in income:
                            
                                
                                for li in inc.line_ids:
                                    
                                    if li.can_id.id == line.can_id.id:
                                        if li.product_id.id == line.product_id.id:
                                            #income_litr += li.hariu_litr
                                            income_kg += li.hariu_kg
                                 
                                for oth in oth_exp.line_ids:
                                    if oth.out_can_id.id == line.can_id.id:
                                        if oth.product_id.id == line.product_id.id:
                                            #oth_exp_litr += oth.torkh_size
                                            oth_exp_kg += oth.kg
                            for inco in income1:
                                for lin in inco.line_ids:
                                    if lin.product_id.id == line.product_id.id:
                                        send_income_litr = lin.torkh_size
                                    
                                            
                                
                                            
                            pdate = self.date-timedelta(hours=24)       
                               
                            if shift.shts_id.work_time==2:
                                
                                if shift_id.shift == 1:
                                    
                                    pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',pdate),('shift','=',2),('state','in',('draft','confirm','count'))],order="shift asc")
                                else:
                                    pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',self.date),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                                    
                            elif shift.shts_id.work_time==1:
                                pre_shift = shift_obj.search([('company_id','=',self.company_id.id),('shts_id','=',shift.shts_id.id),('shift_date','=',pdate),('shift','=',1),('state','in',('draft','confirm','count'))],order="shift asc")
                            
                               
                            if pre_shift:
                                for pre in pre_shift:
                                    for pre_id in pre.scale_ids:
                                         if pre_id.can_id.id == line.can_id.id and pre_id.product_id.id == line.product_id.id:
                                             first_kg = pre_id.kg
                                             balance = line.can_id.balance
                                             duurgelt_huwi = line.can_id.deed_duurgelt_huvi
                                             
                                             
                            
                            have_kg = first_kg + income_kg - other_exp_kg - exp_kg
                            last_kg = line.kg
                            last_litr = line.litr
                            use_kg = last_litr- hii_sorolt_ondor_huvi
                            total_all_litr =  balance + last_litr
                            total_all_kg = total_all_litr * line.self_weight_value
                            can_duutgelt_huwi = last_litr / can_size *100
        
        
                            
                            sheet.write(rowx, 0, '%s'%shts_id, data_center)
                            sheet.write(rowx, 1, '%s'%can_code, data_center)
                            sheet.write(rowx, 2, '%s'%product_id, data_center)
                            sheet.write(rowx, 3, '%s'%can_size, data_center)
                            sheet.write(rowx, 4, round(hii_sorolt_ondor_huvi,2), data_center)
                            sheet.write(rowx, 5, round(first_kg,4), data_center)
                            sheet.write(rowx, 6, round(income_kg,2), data_center)
                            sheet.write(rowx, 7, round(other_exp_kg,2), data_center)
                            sheet.write(rowx, 8, round(exp_kg,2), data_center)
                            sheet.write(rowx, 9, '%s'%round(dhj_amount,4), data_right_hj)
                            sheet.write(rowx, 10, round(exp_litr,2), data_center)
                            sheet.write(rowx, 11, round(sale_price,0), data_center)
                            sheet.write(rowx, 12, round(total_amount,2), data_center)
                            sheet.write(rowx, 13, round(have_kg,2), data_center)
                            sheet.write(rowx, 14, round(last_kg,2), data_center)
                            sheet.write(rowx, 15, round(last_litr,2), data_center)
                            sheet.write(rowx, 16, round(use_kg,2), data_center)
                            sheet.write(rowx, 17, round(send_income_litr,2), data_center)
                            sheet.write(rowx, 18, round(duurgelt_huwi,2), data_center)
                            sheet.write(rowx, 19, '%s'%round(can_duutgelt_huwi,0), data_center)
                            sheet.write(rowx, 20, round(order,2), data_center)
                            sheet.write(rowx, 21, round(balance,2), data_center)
                            sheet.write(rowx, 22, round(total_all_litr,2), data_center)
                            sheet.write(rowx, 23, round(total_all_kg,2), data_center)
                            
                            total_hii_sorolt_ondor_huvi += hii_sorolt_ondor_huvi
                            total_first_kg += first_kg
                            total_income_kg += income_kg
                            total_other_exp_kg += other_exp_kg
                            total_exp_litr += exp_litr
                            total_exp_kg += exp_kg
                            amount += total_amount
                            total_have_kg += have_kg
                            total_last_kg += last_kg
                            total_last_litr += last_litr
                            total_use_kg += use_kg
                            total_send_income_litr += send_income_litr
                            total_balance += balance
                            total_litr +=total_all_litr
                            total_kg += total_all_kg
                        
                            rowx +=1
                    
        sheet.write_merge(rowx, rowx, 0,  3, u'Нийт', data_center)
        sheet.write(rowx, 4, round(total_hii_sorolt_ondor_huvi,2), data_center)
        sheet.write(rowx, 5, round(total_first_kg,4), data_center) 
        sheet.write(rowx, 6, round(total_income_kg,2), data_center) 
        sheet.write(rowx, 7, round(total_other_exp_kg,2), data_center)  
        sheet.write(rowx, 8, round(total_exp_kg,2), data_center)
        sheet.write(rowx, 9, '', data_center)
        sheet.write(rowx, 10, round(total_exp_litr,2), data_center)
        sheet.write(rowx, 11,'', data_center)
        sheet.write(rowx, 12, round(amount,2), data_center)
        sheet.write(rowx, 13, round(total_have_kg,2), data_center)
        sheet.write(rowx, 14, round(total_last_kg,2), data_center)
        sheet.write(rowx, 15, round(total_last_litr,2), data_center)
        sheet.write(rowx, 16, round(total_use_kg,2), data_center)
        sheet.write(rowx, 17, round(total_send_income_litr,2), data_center)
        sheet.write(rowx, 18, '', data_center)
        sheet.write(rowx, 19, '', data_center)
        sheet.write(rowx, 20, '', data_center)
        sheet.write(rowx, 21, round(total_balance,2), data_center)
        sheet.write(rowx, 22, round(total_litr,2), data_center)
        sheet.write(rowx, 23, round(total_kg,2), data_center)
        
        
                   
                      
        
        inch = 1000
        sheet.col(0).width = 7 * inch
        sheet.col(1).width = 6 * inch
        sheet.col(2).width = 4 * inch
        sheet.col(3).width = 4 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 4 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 4 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 3 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 4 * inch
        sheet.col(13).width = 4 * inch
        sheet.col(14).width = 4 * inch
        sheet.col(15).width = 4 * inch
        sheet.col(16).width = 4 * inch
        sheet.col(17).width = 4 * inch
        sheet.col(18).width = 4 * inch
        sheet.col(19).width = 4 * inch
        sheet.col(20).width = 4 * inch
        sheet.col(21).width = 4 * inch
        sheet.col(22).width = 4 * inch
        sheet.col(23).width = 4 * inch
        sheet.row(6).height = 400
        sheet.row(7).height = 300

        return {'data':book, 'directory_name':u'Шатахууны үлдэгдлийн тайлан',
                'attache_name':'Шатахууны үлдэгдлийн тайлан'}
        

class ProductBalanceRegionReportApi(models.TransientModel):
    _name = "product.balance.region.report.api"
    
    company_id = fields.Many2one('res.company', string=u'Компани',
                                default=lambda self: self.env['res.company']._company_default_get(
                                    'product.balance.report'))
    region_id = fields.Many2one('shts.region', u'Бүс')
    
    shts_id = fields.Many2one('shts.register', u'ШТС')
    is_region = fields.Boolean(string=u'ШТС Бүсээр татах эсэх')
    date = fields.Date(string=u'Огноо')
    
   
    
    @api.model
    def default_get(self, fields):
        if self.env.content.get('active_model') !='product.balance.report' or not self.env.content.get('active_ids'):
            raise UserError(_('This can only'))
        rec = super(ProductBalanceRegionReportApi, self).default_get(fields)
        product_balance_ids = self.env['product.balance.report'].browse(self.env.content['active_ids'])
        rec['company_id'] = product_balance_ids.company_id.id
        rec['region_id'] = product_balance_ids.region_id.id
        rec['shts_id'] = product_balance_ids.shts_id.id
        rec['date'] = product_balance_ids.date
        dats = self.env.content.get('line_ids')
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['line_ids'] = list
        return rec
    
    
    
    