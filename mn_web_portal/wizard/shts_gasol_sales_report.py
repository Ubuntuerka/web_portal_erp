# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _


import time
import xlwt
import openerp.netsvc

class shts_gasol_sales_view_report(models.TransientModel):
    _name = 'shts.gasol.sales.view.report'
    
    company_id = fields.Many2many('res.company',string=u'Компани')
    start_date = fields.Date(string=u'Эхлэх огноо',readonly=True)
    end_date = fields.Date(string=u'Дуусах огноо',readonly=True)
    shts_id = fields.Many2many('shts.register',string=u'ШТС',readonly=True)
    line_ids = fields.One2many('shts.gasol.sales.view.line','parent_id',string='Line')
    
    
    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'shts.gasol.sales.report' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(shts_gasol_sales_view_report, self).default_get(fields)
        main_report_ids = self.env['shts.gasol.sales.report'].browse(self.env.context['active_ids'])
        rec['company_id'] = main_report_ids.company_id.ids
        rec['start_date'] = main_report_ids.start_date
        rec['end_date'] = main_report_ids.end_date
        rec['shts_id'] = main_report_ids.shts_id.ids
        dats = self.env.context.get('line_ids')
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['line_ids'] = list
        return rec
    

class shts_gasol_sales_view_line(models.TransientModel):
    _name = 'shts.gasol.sales.view.line'
    
    product_id = fields.Many2one('product.product',string=u'Шатахуун')
    shift = fields.Integer(string=u'Ээлж')
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    kg = fields.Float(string=u'Кг')
    litr = fields.Float(string=u'Литр')
    qty = fields.Float(string=u'Тоо')
    total_amount = fields.Float(string='Нийт үнэ')
    parent_id = fields.Many2one('shts.gasol.sales.view.report','Parent')

class ShtsGasolSalesReport(models.TransientModel):
    _inherit = "abstract.report.excel"
    _name = 'shts.gasol.sales.report'
    
    company_id = fields.Many2many('res.company',string=u'Компани')
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    shts_id = fields.Many2many('shts.register',string=u'ШТС')
    region_id = fields.Many2one('shts.region',string=u'ШТС бүс')
    is_region = fields.Boolean(string=u'ШТС Бүсээр татах эсэх')
    
    def action_view_report(self):
        shift_obj = self.env['shift.working.register']
        product_obj = self.env['product.product']
        context = dict(self.env.context or {})
        lin_data = []
        shts = []
        if self.is_region == True:
            if self.region_id:
                shts_ids = self.env['shts.register'].search([('region_id','=',self.region_id.id)])
                for sh in shts_ids:
                    shts.append(sh.id)
        else:
                
            if self.shts_id:
                for sh in self.shts_id:
                    shts.append(sh.id)
            
        if len(shts)==1:
            shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',shts[0]),('company_id','=',self.company_id.id),('state','!=','cancel')])
            self._cr.execute("""select spl.product_id,swr.shts_id  from shift_working_register as swr
                    left join shift_product_line as spl on spl.shift_id = swr.id
                    where swr.shift_date >= '%s' and swr.shift_date <='%s'
                    and swr.state != 'cancel' and swr.company_id = %s
                    and swr.shts_id = %s
                    group by spl.product_id,swr.shts_id """%(self.start_date,self.end_date,self.company_id.id,shts[0]))
            products = self._cr.dictfetchall()
        elif len(shts)>1:
            shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','in',shts),('company_id','=',self.company_id.id),('state','!=','cancel')])
            self._cr.execute("""select spl.product_id,swr.shts_id from shift_working_register as swr
                    left join shift_product_line as spl on spl.shift_id = swr.id
                    where swr.shift_date >= '%s' and swr.shift_date <='%s'
                    and swr.state != 'cancel' and swr.company_id = %s
                    and swr.shts_id in %s
                    group by spl.product_id,swr.shts_id """%(self.start_date,self.end_date,self.company_id.id,tuple(shts)))
            products = self._cr.dictfetchall()
        else:
            shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('company_id','=',self.company_id.id),('state','!=','cancel')])
            self._cr.execute("""select spl.product_id,swr.shts_id from shift_working_register as swr
                    left join shift_product_line as spl on spl.shift_id = swr.id
                    where swr.shift_date >= '%s' and swr.shift_date <='%s'
                    and swr.state != 'cancel' and swr.company_id = %s
                    group by spl.product_id,swr.shts_id """%(self.start_date,self.end_date,self.company_id.id))
            products = self._cr.dictfetchall()
        
        
        if shift_ids:
            if products !=None:
                for pro in products:
                    product = product_obj.browse(pro['product_id'])
                    total_litr = 0.0
                    total_kg = 0.0
                    total = 0.0
                    for sv in shift_ids:
                        for line in sv.shift_product_ids:
                            if product.id == line.product_id.id and pro['shts_id']==sv.shts_id.id:
                                total_litr += round(line.sale_litr,2)
                                total_kg +=round(line.total_kg,2)
                                total +=line.total_amount
                    datas = {
                            'shts_id':pro['shts_id'],
                            'product_id':product.id,
                            'litr':total_litr,
                            'kg':total_kg,
                            'total_amount':total,
                            }
                    lin_data.append(datas)
        context['line_ids'] = lin_data
        
        compose_form = self.env.ref('mn_web_portal.shts_gasol_sales_view_report_view')
        action = {
            'name': _(u'ШТС Шатахуун борлуулалт тайлан'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'shts.gasol.sales.view.report',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Шатахуун борлуулалт тайлан')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name,), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write_merge(2, 2, 0, 4, u'Шатахуун борлуулалт тайлан', ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 240'))
        if len(self.shts_id)>1:
            shts_name = ''
            for shts in self.shts_id:
                shts_name = shts_name + ' ' + shts.name
                sheet.write_merge(3, 3, 0, 4, u'ШТС: %s' % (shts_name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        else:
            
                sheet.write_merge(3, 3, 0, 4, u'ШТС: %s' % (self.shts_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 240'))
        sheet.write(4, 0, u'Эхлэх огноо: %s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(4, 3, u'Дуусах огноо: %s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        
        
        rowx = 6
        sheet.write(rowx, 0, u'№', data_bold)
        sheet.write(rowx, 1, u'ШТС', data_bold)
        sheet.write(rowx, 2, u'Шатахуун', data_bold)
        sheet.write(rowx, 3, u'Литр', data_bold)
        sheet.write(rowx, 4, u'Кг', data_bold)
        sheet.write(rowx, 5, u'Нийт үнэ', data_bold)
        shift_obj = self.env['shift.working.register']
        product_obj = self.env['product.product']
        shts_obj = self.env['shts.register']
        number = 0
        shts = []
        if self.is_region == True:
            if self.region_id:
                shts_ids = self.env['shts.register'].search([('region_id','=',self.region_id.id)])
                for sh in shts_ids:
                    shts.append(sh.id)
        else:
                
            if self.shts_id:
                for sh in self.shts_id:
                    shts.append(sh.id)
                    
        if len(shts)==1:
            shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','=',shts[0]),('company_id','=',self.company_id.id),('state','not in',('draft','cancel'))])
            self._cr.execute("""select spl.product_id,swr.shts_id  from shift_working_register as swr
                    left join shift_product_line as spl on spl.shift_id = swr.id
                    where swr.shift_date >= '%s' and swr.shift_date <='%s'
                    and swr.state not in ('draft','cancel') and swr.company_id = %s
                    and swr.shts_id = %s
                    group by spl.product_id,swr.shts_id """%(self.start_date,self.end_date,self.company_id.id,shts[0]))
            products = self._cr.dictfetchall()
            
        elif len(shts)>1:
            shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('shts_id','in',shts),('company_id','=',self.company_id.id),('state','not in',('draft','cancel'))])
            self._cr.execute("""select spl.product_id ,swr.shts_id from shift_working_register as swr
                    left join shift_product_line as spl on spl.shift_id = swr.id
                    where swr.shift_date >= '%s' and swr.shift_date <='%s'
                    and swr.state not in ('draft','cancel') and swr.company_id = %s
                    and swr.shts_id in %s
                    group by spl.product_id,swr.shts_id """%(self.start_date,self.end_date,self.company_id.id,tuple(shts)))
            products = self._cr.dictfetchall()
        else:
            shift_ids = shift_obj.search([('shift_date','>=',self.start_date),('shift_date','<=',self.end_date),('company_id','=',self.company_id.id),('state','not in',('draft','cancel'))])
            self._cr.execute("""select spl.product_id,swr.shts_id  from shift_working_register as swr
                    left join shift_product_line as spl on spl.shift_id = swr.id
                    where swr.shift_date >= '%s' and swr.shift_date <='%s'
                    and swr.state not in ('draft','cancel') and swr.company_id = %s
                    group by spl.product_id,swr.shts_id """%(self.start_date,self.end_date,self.company_id.id))
            products = self._cr.dictfetchall()
        rowx = 7
        if shift_ids != []:
            if products !=None:
                for pro in products:
                    product = product_obj.browse(pro['product_id'])
                    if product:
                        pro_name = product.name
                    else:
                        pro_name = ''
                    shts_data = shts_obj.browse(pro['shts_id'])
                    total_litr = 0.0
                    total_kg = 0.0
                    total = 0.0
                    for sv in shift_ids:
                        for line in sv.shift_product_ids:
                            if product.id == line.product_id.id and shts_data.id == sv.shts_id.id:
                                total_litr +=round(line.sale_litr,2)
                                total_kg +=round(line.total_kg,2)
                                total +=line.total_amount
                    number +=1
                    sheet.write(rowx, 0, number, data_right)
                    sheet.write(rowx, 1, u'%s'%shts_data.name, data_right)
                    sheet.write(rowx, 2, u'%s'%(pro_name), data_right)
                    sheet.write(rowx, 3, round(total_litr,2), data_right)
                    sheet.write(rowx, 4, round(total_kg,2), data_right)
                    sheet.write(rowx, 5, round(total,2), data_right)
                    rowx +=1
        
        


        inch = 1000
        sheet.col(0).width = 2 * inch
        sheet.col(1).width = 6 * inch
        sheet.col(2).width = 4 * inch
        sheet.col(3).width = 4 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.row(2).height = 400

        return {'data':book, 'directory_name':u'Үйлчилгээний тайлан',
                'attache_name':'Үйлчилгээний тайлан'}
    
    
    
    
    
    