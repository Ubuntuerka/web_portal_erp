# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class ActionComputePaymentEdit(models.TransientModel):
    _name = 'action.compute.payment.edit'
    
    def action_update(self):
        shift_obj = self.env['shift.working.register']
        active_id = self._context.get('active_ids')
        shift_id = shift_obj.search([('id','=',active_id)])
        for shift in shift_id:
            shift.action_new_compute()
    
    def action_compute(self):
        shift_obj = self.env['shift.working.register']
        active_id = self._context.get('active_ids')
        shift_id = shift_obj.search([('id','=',active_id)])
        for shift in shift_id:
            shift.action_compute_shift()
            
    
    def action_infor_compute(self):
        shift_obj = self.env['shift.working.register']
        active_id = self._context.get('active_ids')
        shift_id = shift_obj.search([('id','=',active_id)])
        for shift in shift_id:
            shift.action_cash_sales_info()
            
    
    