# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class SalesRecapitulationReport(models.TransientModel):
    """
        14 маягт саваар тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "sales.recapitulation.report"
    _description = "Sales recapitulation report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'14 Маягт төлбөрийн төрлөөр')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        
        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 220'))
        sheet.write(0, 5, u'ШТС: %s' % (self.shts_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 220'))    
        sheet.write_merge(2, 2, 0, 28, u'Борлуулалтын нэгтгэл шатахуун', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 220'))
        rowx = 4

        sheet.write_merge(rowx, rowx+1, 0, 0, u'Д/д', data_left)
        sheet.write(rowx, 1, u'А80 бензин', data_right)
        sheet.write(rowx+1, 1, u'ШТС-ууд', data_right)
        sheet.write_merge(rowx, rowx, 2, 3, u'Орлого Эхний үлдэгдэл', data_right)
        sheet.write(rowx+1, 2, u'литр', data_right)
        sheet.write(rowx+1, 3, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 4, 5, u'Орлого БМ-ын хөдөлгөөн', data_right)
        sheet.write(rowx+1, 4, u'литр', data_right)
        sheet.write(rowx+1, 5, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 6, 7, u'Зарлага Борлуулалт', data_right)
        sheet.write(rowx+1, 6, u'литр', data_right)
        sheet.write(rowx+1, 7, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 8, 9, u'Зарлага БМ хөдөлгөөн', data_right)
        sheet.write(rowx+1, 8, u'литр', data_right)
        sheet.write(rowx+1, 9, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 10, 11, u'Зарлага Зардалд', data_right)
        sheet.write(rowx+1, 10, u'литр', data_right)
        sheet.write(rowx+1, 11, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 12, 13, u'Хэвийн хорогдол', data_right)
        sheet.write(rowx+1, 12, u'литр', data_right)
        sheet.write(rowx+1, 13, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 14, 15, u'Эцсийн үлдэгдэл', data_right)
        sheet.write(rowx+1, 14, u'литр', data_right)
        sheet.write(rowx+1, 15, u'кг', data_right)
        sheet.write_merge(rowx, rowx, 16, 19, u'Эхний милл', data_right)
        sheet.write(rowx+1, 16, u'милл', data_right)
        sheet.write(rowx+1, 17, u'милл', data_right)
        sheet.write(rowx+1, 18, u'милл', data_right)
        sheet.write(rowx+1, 19, u'милл', data_right)
        sheet.write_merge(rowx, rowx, 20, 23, u'Эцсийн милл', data_right)
        sheet.write(rowx+1, 20, u'милл', data_right)
        sheet.write(rowx+1, 21, u'милл', data_right)
        sheet.write(rowx+1, 22, u'милл', data_right)
        sheet.write(rowx+1, 23, u'милл', data_right)
        sheet.write_merge(rowx, rowx+1, 24, 24, u'Борлуулалт', data_right)
        sheet.write_merge(rowx, rowx+1, 25, 25, u'Залин', data_right)
        sheet.write_merge(rowx, rowx+1, 26, 26, u'Залин', data_right)
        sheet.write_merge(rowx, rowx+1, 27, 27, u'Зөрүү', data_right)
      
        
        inch = 1000
        sheet.col(0).width = 1 * inch
        sheet.col(1).width = 3 * inch
        sheet.col(2).width = 3 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 3 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 3 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 3 * inch
        sheet.col(9).width = 3 * inch
        sheet.col(10).width = 3 * inch
        sheet.col(11).width = 3 * inch
        sheet.col(12).width = 3 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch 
        sheet.col(16).width = 3 * inch
        sheet.col(17).width = 3 * inch
        sheet.col(18).width = 3 * inch
        sheet.col(19).width = 3 * inch    
        sheet.col(20).width = 3 * inch
        sheet.col(21).width = 3 * inch
        sheet.col(22).width = 3 * inch
        sheet.col(23).width = 3 * inch
        sheet.col(24).width = 3 * inch
        sheet.col(25).width = 3 * inch 
        sheet.col(26).width = 3 * inch
        sheet.col(27).width = 3 * inch
    
        sheet.row(3).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Миллийн заалт',
                'attache_name':'Миллийн заалт'}

