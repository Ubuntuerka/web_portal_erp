# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class PriceChangeViewReport(models.TransientModel):
    _name = 'price.change.view.report'
    
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    line_ids = fields.One2many('price.change.view.line.report','price_report_id',string=u'Line')
    
    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'price.change.report' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(PriceChangeViewReport, self).default_get(fields)
        main_report_ids = self.env['price.change.report'].browse(self.env.context['active_ids'])        
        dats = self.env.context.get('line_ids')
        shts_id = self.env.context.get('shts_id')
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['line_ids'] = list
        rec['shts_id'] = shts_id
        return rec
    

class PriceChangeViewLineReport(models.TransientModel):
    _name = 'price.change.view.line.report'
    
    price_report_id = fields.Many2one('price.change.view.report',string='Car')
    car_id = fields.Many2one('car.register',string=u'Тээврийн хэрэгсэл',readonly=True)
    send_location =fields.Many2one('shts.register',string=u'Гарсан байршил',readonly=True)
    rec_location = fields.Many2one('shts.register',string=u'Хүрэх байршил',readonly=True)
    range_km = fields.Float(string=u'Зай км',readonly=True)
    send_date = fields.Date(string=u'Гарсан огноо',readonly=True)
    rec_date = fields.Date(string=u'Очсон огноо',readonly=True)
    rec_time = fields.Date(string=u'Очсон хугацаа',readonly=True)


class PriceChangeReport(models.TransientModel):
    """
        14 маягт саваар тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "price.change.report"
    _description = "Price change report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    
    
    
