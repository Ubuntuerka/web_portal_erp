# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class DocSaleViewReport(models.TransientModel):
    _name = 'doc.sale.view.report'
    
    company_id = fields.Many2one('res.company',string='Компани')
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    start_date = fields.Date(string=u'Эхлэх огноо')
    end_date = fields.Date(string=u'Дуусах огноо')
    line_ids = fields.One2many('doc.sale.view.line.report','doc_sale_view_id',string='Line')
    
    
    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'doc.sale.report' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(DocSaleViewReport, self).default_get(fields)
        main_report_ids = self.env['doc.sale.report'].browse(self.env.context['active_ids'])
        rec['company_id'] = main_report_ids.company_id.id
        rec['start_date'] = main_report_ids.start_date
        rec['end_date'] = main_report_ids.end_date
        rec['shts_id'] = main_report_ids.shts_id.id
        dats = self.env.context.get('line_ids')
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['line_ids'] = list
        return rec
    
class DocSaleViewLineReport(models.TransientModel):
    _name = 'doc.sale.view.line.report'
    
    company_id = fields.Many2one('res.company',string='Компани')
    doc_sale_view_id = fields.Many2one('doc.sale.view.report',string=u'Doc can view')
    date = fields.Date(string=u'Огноо')
    gasol_sale = fields.Float(string=u'Борлуулалт орлого')
    oil_sale = fields.Float(string=u'Тос')
    tvt_sale = fields.Float(string=u'ТҮТ')
    total_amount = fields.Float(string=u'Нийт дүн')
    
class DocSaleReport(models.TransientModel):
    """
        14 маягт саваар тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "doc.sale.report"
    _description = "Mile break report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    
    def get_action_report_view(self):
        shift_obj = self.env['shift.working.register']
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        context = dict(self.env.context or {})
        lin_data = []
        for day in days:
            date1 = day.strftime('%Y-%m-%d')
            date11 = datetime.strptime(str(date1), DATE_FORMAT)
            shift = shift_obj.search([('state','in',('confirm','count')),('shts_id','=',self.shts_id.id),('shift_date','=',date1)])
            if shift:
                total_sale_gasol = 0.0
                total_sale_oil = 0.0
                total_sale_tvt = 0.0
                total_amount = 0.0
                for li in shift.mile_target_ids:
                    for lis in self.shts_id.line_ids:
                        if lis.product_id.id==li.product_id.id:
                            total_sale_gasol +=li.expense_litr*lis.price
                for tvt_line in shift.ttm_sale_ids:
                    total_sale_oil +=tvt_line.total_price
                for oilline in shift.tvt_sale_ids:
                    total_sale_tvt +=oilline.total_price

                total_amount = total_sale_gasol+total_sale_tvt+total_sale_oil
                datas = {
                        'date':date1,
                        'total_amount':total_amount,
                        'tvt_sale':total_sale_tvt,
                        'oil_sale':total_sale_oil,
                        'gasol_sale':total_sale_gasol
                    }
            
            
                lin_data.append(datas)
        context['line_ids'] = lin_data
                    
                    
        
        
        compose_form = self.env.ref('mn_web_portal.doc_sale_view_line_report_view')
        action = {
            'name': _(u'14 Тайлан'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'doc.sale.view.report',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action 
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'14 маягт борлуулалтаар тайлан')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 0, u'ШТС : %s' % (self.shts_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(1, 0, u'14 маягт борлуулалтаар тайлан', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 0, u'Эхлэх огноо: %s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(4, 4, u'Дуусах огноо: %s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        
        rowx = 5
        
        sheet.write_merge(rowx, rowx+1, 0, 0, u'Огноо', data_right)
        sheet.write_merge(rowx, rowx+1, 1, 1, u'Борлуулалтын орлого', data_right)
        sheet.write_merge(rowx, rowx+1, 2, 2, u'Тос', data_right)
        sheet.write_merge(rowx, rowx+1, 3, 3, u'ТҮТ', data_right)
        sheet.write_merge(rowx, rowx+1, 4, 4, u'Нийт дүн', data_right)
        
        rowx +=2
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        
        shift_obj = self.env['shift.working.register']
        pro_obj = self.env['product.product']
        for day in days:
            date1 = day.strftime('%Y-%m-%d')
            shift = shift_obj.search([('state','in',('confirm','count')),('shts_id','=',self.shts_id.id),('shift_date','=',date1)])
            if shift:
                total_sale_gasol = 0.0
                total_sale_oil = 0.0
                total_sale_tvt = 0.0
                total_amount = 0.0
                for li in shift.mile_target_ids:
                    for lis in self.shts_id.line_ids:
                        if lis.product_id.id==li.product_id.id:
                            total_sale_gasol +=li.expense_litr*lis.price
                for tvt_line in shift.ttm_sale_ids:
                    total_sale_oil +=tvt_line.total_price
                for oilline in shift.tvt_sale_ids:
                    total_sale_tvt +=oilline.total_price
                total_amount = total_sale_gasol+total_sale_tvt+total_sale_oil
                
                sheet.write(rowx,  0, u'%s'%date1, data_right)
                sheet.write(rowx,  1, total_sale_gasol, data_right)
                sheet.write(rowx,  2, total_sale_oil, data_right)
                sheet.write(rowx,  3, total_sale_tvt, data_right)
                sheet.write(rowx,  4, total_amount, data_right)
                rowx +=1
                
            
            
        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 5 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 1 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.row(3).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Миллийн заалт',
                'attache_name':'Миллийн заалт'}