# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class CarMoveViewReport(models.TransientModel):
    _name = 'car.move.view.report'
    
    shts_id = fields.Many2one('shts.register',string=u'ШТС')
    line_ids = fields.One2many('car.move.view.line.report','car_report_id',string=u'Line')
    
    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') != 'car.move.report' or not self.env.context.get('active_ids'):
            raise UserError(_('This can only be used on journal items'))
        rec = super(CarMoveViewReport, self).default_get(fields)
        main_report_ids = self.env['car.move.report'].browse(self.env.context['active_ids'])        
        dats = self.env.context.get('line_ids')
        shts_id = self.env.context.get('shts_id')
        list = []
        for li in dats:
            list.append((0,0,li))
        rec['line_ids'] = list
        rec['shts_id'] = shts_id
        return rec
    

class CarMoveViewLineReport(models.TransientModel):
    _name = 'car.move.view.line.report'
    
    car_report_id = fields.Many2one('car.move.view.report',string='Car')
    car_id = fields.Many2one('car.register',string=u'Тээврийн хэрэгсэл',readonly=True)
    send_location =fields.Many2one('shts.register',string=u'Гарсан байршил',readonly=True)
    rec_location = fields.Many2one('shts.register',string=u'Хүрэх байршил',readonly=True)
    range_km = fields.Float(string=u'Зай км',readonly=True)
    send_date = fields.Date(string=u'Гарсан огноо',readonly=True)
    rec_date = fields.Date(string=u'Очсон огноо',readonly=True)
    rec_time = fields.Date(string=u'Очсон хугацаа',readonly=True)


class CarMoveReport(models.TransientModel):
    """
        14 маягт саваар тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "car.move.report"
    _description = "Car move report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    
    
    
    def action_view_report(self):
        context = dict(self.env.context or {})
        compose_form = self.env.ref('mn_web_portal.car_move_view_report_view')
        product_obj = self.env['product.product']
        shts_line_obj = self.env['shts.register.line']
        product = product_obj.search([('prod_type','=','oil')])
        context = dict(self.env.context or {})
        self.env.cr.execute("""select date(sw.shift_date) as rdate, ms.first_mile, ms.end_mile, ms.diff_mile,ms.name,sw.shift
                            from mile_target_register_line as ms 
                            left join shift_working_register as sw on ms.shift_id=sw.id 
                            where date(sw.shift_date) >='%s' and date(sw.shift_date) <='%s' 
                            and sw.company_id = %s and sw.shts_id = %s and ms.can_id = %s and sw.state in ('confirm','count') order by sw.reg_date asc
                         """%(self.start_date,self.end_date,self.company_id.id,self.shts_id.id,self.can_id.id))
        car_move_data = self._cr.dictfetchall()
        datas = []
        for pro in product:
                data = {
                        'name':pro.name,
                        }
                datas.append(data)
        context['line_ids'] = datas
        context['shts_id'] = self.shts_id.id
        
        action = {
            'name': _(u'ШЗА тээврийн машины хөдөлгөөний тайлан'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'car.move.view.report',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action
    
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'14 Маягт төлбөрийн төрлөөр')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))

        sheet.write(2, 0, u'Тайлан эхлэх огноо: %s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 4, u'Тайлан дуусах огноо: %s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 2, u'Тээврийн машины хөдөлгөөний тайлан', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        rowx = 4

        sheet.write(rowx, 0, u'№', data_right)
        sheet.write(rowx, 1, u'Тээврийн хэрэгсэл', data_right)
        sheet.write(rowx, 2, u'Гарсан байршил', data_right)
        sheet.write(rowx, 3, u'Хүрэх байршил', data_right)
        sheet.write(rowx, 4, u'Зай км', data_right)
        sheet.write(rowx, 5, u'Гарсан огноо', data_right)
        sheet.write(rowx, 6, u'Очсон огноо', data_right)
        sheet.write(rowx, 7, u'Очсон хугацаа', data_right)
        
        
        self.env.cr.execute("""select date(sw.shift_date) as rdate, ms.first_mile, ms.end_mile, ms.diff_mile,ms.name,sw.shift
                            from mile_target_register_line as ms 
                            left join shift_working_register as sw on ms.shift_id=sw.id 
                            where date(sw.shift_date) >='%s' and date(sw.shift_date) <='%s' 
                            and sw.company_id = %s and sw.shts_id = %s and ms.can_id = %s and sw.state in ('confirm','count') order by sw.reg_date asc
                         """%(self.start_date,self.end_date,self.company_id.id,self.shts_id.id,self.can_id.id))
        car_move_data = self._cr.dictfetchall()

      
        
        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 5 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 1 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.row(3).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Миллийн заалт',
                'attache_name':'Миллийн заалт'}

