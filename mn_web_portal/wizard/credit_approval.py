# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import xlwt
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class CreditApprovalReportApi(models.TransientModel):
    _name = "credit.approval.api"
    _description = "Credit approval report"

    company_id = fields.Many2one('res.company', string=u'Компани')
    shts_id = fields.Many2one('shts.register', string=u'ШТС')
    partner_id = fields.Many2one('res.partner', string='Харилцагч', required=True)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)
    line_ids = fields.One2many('credit.approval.tree', 'credit_id', 'line')

    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') !='credit.approval' or not self.env.context.get('active_ids'):
            raise UserError(_('UserError can not'))
        rec = super(CreditApprovalReportApi, self).default_get(fields)
        approval_ids = self.env['credit.approval'].browse(self.env.context['active_ids'])
        rec['company_id'] = approval_ids.company_id.id
        rec['shts_id'] = approval_ids.shts_id.id
        rec['partner_id'] = approval_ids.partner_id.id
        rec['start_date'] = approval_ids.start_date
        rec['end_date'] = approval_ids.end_date
        dats = self.env.context.get('line_ids')
        list = []
        for li in dats:
            list.append((0, 0, li))
        rec['line_ids'] = list
        return rec


class CreditApprovalReportTree(models.TransientModel):
    _name = 'credit.approval.tree'

    company_id = fields.Many2one('res.company', string=u'Компани', readonly=True,
                                default=lambda self: self.env['res.company']._company_default_get(
                                    'mile.break.view.report'))
    shift_date = fields.Date(string=u'Ээлжийн огноо', required=True)
    shts_reg_user_id = fields.Many2one('hr.employee',string=u'Бүртгэсэн ажилтан',required=True)
    shts_id = fields.Many2one('shts.register', u'ШТС', required=True,
                              default=lambda self: self.env.user.warehouse_id.id)
    region_id = fields.Many2one('shts.region', u'Бүс', required=True)
    shift = fields.Integer(string=u'Ээлж')
    name = fields.Char(string='Падааны дугаар', required=True)
    product_id = fields.Many2one('product.product', string='Бүтээгдэхүүн', required=True)
    partner_id = fields.Many2one('res.partner', string='Харилцагч', required=True)
    car_number_id = fields.Char('Машины дугаар', required=True)
    total_amount = fields.Float(string='Нийт үнэ', required=True)
    price = fields.Float(string='Үнэ')
    credit_id = fields.Many2one('credit.approval.api', 'Credit')



class CreditApproval (models.TransientModel):
    """Зээлийн зөвшөөрсөн тайлан"""

    _inherit = "abstract.report.excel"
    _name = "credit.approval"
    _description = "Credit Approval Report"

    company_id = fields.Many2one('res.company', string=u'Компани', required=True,
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register', string=u'ШТС',
                              default=lambda self: self.env.user.warehouse_id.id)
    partner_id = fields.Many2one('res.partner', string='Харилцагч')
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)

    def get_action_report_view(self):
        if self.shts_id and self.partner_id:


            self.env.cr.execute("""select s.id, s.shts_id, s.company_id, s.shift, s.region_id, s.shts_reg_user_id, s.shift_date,
                            l.name, l.partner_id, l.car_number_id, l.product_id, l.price,  l.total_amount
                            from shift_working_register as s
                            left join loan_document_register_line as l on l.shift_id = s.id
                            where date(s.shift_date) >='%s' and date(s.shift_date) <='%s' 
                            and s.company_id = %d and s.shts_id = %d and l.partner_id = %d and l.type = 'loan'
                            """ % (self.start_date, self.end_date, self.company_id.id, self.shts_id.id,self.partner_id.id))
        elif self.shts_id:

            self.env.cr.execute("""select s.id, s.shts_id, s.company_id, s.shift, s.region_id, s.shts_reg_user_id, s.shift_date,
                            l.name, l.partner_id, l.car_number_id, l.product_id, l.price,  l.total_amount
                            from shift_working_register as s
                            left join loan_document_register_line as l on l.shift_id = s.id
                            where date(s.shift_date) >='%s' and date(s.shift_date) <='%s' 
                            and s.company_id = %d and s.shts_id = %d and l.type = 'loan'
                            """ % (self.start_date, self.end_date, self.company_id.id, self.shts_id.id))
        else:
            if self.partner_id:
               self.env.cr.execute("""select s.id, s.shts_id, s.company_id, s.shift, s.region_id, s.shts_reg_user_id, s.shift_date,
                                        l.name, l.partner_id, l.car_number_id, l.product_id, l.price,  l.total_amount
                                        from shift_working_register as s
                                        left join loan_document_register_line as l on l.shift_id = s.id
                                        where date(s.shift_date) >='%s' and date(s.shift_date) <='%s' 
                                        and s.company_id = %d and l.partner_id = %d and l.type = 'loan'
                                       """ % (self.start_date, self.end_date, self.company_id.id, self.partner_id.id))
            else:
                raise UserError(_(u'ШТС эсвэл Харилцагчийн нэр заавал оруулна уу!'))
        loan_datas = self._cr.dictfetchall()
        context = dict(self.env.context or {})
        datas =[]
        if loan_datas !=[]:
            total_amount = 0.0
            for ld in loan_datas:

                data = {
                    'shift_date': ld['shift_date'],
                    'shift': ld['shift'],
                    'region_id': ld['region_id'],
                    'shts_id': ld['shts_id'],
                    'name': ld['name'],
                    'partner_id': ld['partner_id'],
                    'car_number_id': ld['car_number_id'],
                    'product_id': ld['product_id'],
                    'price': ld['price'],
                    'shts_reg_user_id': ld['shts_reg_user_id'],
                    'total_amount': ld['total_amount'],
                }
                datas.append(data)
                #print('fffffff', datas)
        context['line_ids'] = datas
        context['company_id'] = self.company_id.id
        context['shts_id'] = self.shts_id.id
        compose_form = self.env.ref('mn_web_portal.credit_approval_api_view')
        print('ggggg',compose_form)

        action = {
            'name': _(u'Зээлийн зөвшөөрөл'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'credit.approval.api',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action

    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)


        ezxf = xlwt.easyxf

        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Миллийн заалт')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name,), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 4, u'ШТС: %s' % (self.shts_id.name,), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write_merge(2, 2, 0, 5, u'Зээлээр зөвшөөрсөн тайлан', ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 220'))

        rowx = 4
        sheet.write(rowx, 0, u'Огноо', data_bold)
        sheet.write(rowx, 1, u'Ээлж', data_bold)
        sheet.write(rowx, 2, u'Харьяалах бүс', data_bold)
        sheet.write(rowx, 3, u'ШТС', data_bold)
        sheet.write(rowx, 4, u'Падааны дугаар', data_bold)
        sheet.write(rowx, 5, u'Харилцагчийн нэр', data_bold)
        sheet.write(rowx, 6, u'Машины дугаар', data_bold)
        sheet.write(rowx, 7, u'Бүтээгдэхүүний нэр', data_bold)
        sheet.write(rowx, 8, u'Нэгж үнэ', data_bold)
        sheet.write(rowx, 9, u'Нийт үнэ', data_bold)
        sheet.write(rowx, 10, u'Зөвшөөрөл өгсөн менежер', data_bold)

        if self.shts_id and self.partner_id:

            self.env.cr.execute("""select s.id, s.shts_id, s.company_id, s.shift, s.region_id, s.shts_reg_user_id, s.shift_date,
                                   l.name, l.partner_id, l.car_number_id, l.product_id, l.price,  l.total_amount
                                   from shift_working_register as s
                                   left join loan_document_register_line as l on l.shift_id = s.id
                                   where date(s.shift_date) >='%s' and date(s.shift_date) <='%s' 
                                   and s.company_id = %d and s.shts_id = %d and l.partner_id = %d and l.type = 'loan'
                                   """ % (
                self.start_date, self.end_date, self.company_id.id, self.shts_id.id, self.partner_id.id))
        elif self.shts_id:

            self.env.cr.execute("""select s.id, s.shts_id, s.company_id, s.shift, s.region_id, s.shts_reg_user_id, s.shift_date,
                                   l.name, l.partner_id, l.car_number_id, l.product_id, l.price,  l.total_amount
                                   from shift_working_register as s
                                   left join loan_document_register_line as l on l.shift_id = s.id
                                   where date(s.shift_date) >='%s' and date(s.shift_date) <='%s' 
                                   and s.company_id = %d and s.shts_id = %d and l.type = 'loan'
                                   """ % (self.start_date, self.end_date, self.company_id.id, self.shts_id.id))
        else:
            if self.partner_id:
                self.env.cr.execute("""select s.id, s.shts_id, s.company_id, s.shift, s.region_id, s.shts_reg_user_id, s.shift_date,
                                               l.name, l.partner_id, l.car_number_id, l.product_id, l.price,  l.total_amount
                                               from shift_working_register as s
                                               left join loan_document_register_line as l on l.shift_id = s.id
                                               where date(s.shift_date) >='%s' and date(s.shift_date) <='%s' 
                                               and s.company_id = %d and l.partner_id = %d and l.type = 'loan'
                                              """ % (
                self.start_date, self.end_date, self.company_id.id, self.partner_id.id))
            else:
                raise UserError(_(u'ШТС эсвэл Харилцагчийн нэр заавал оруулна уу!'))
        loan_datas = self._cr.dictfetchall()
        rowx +=1
        if loan_datas != []:
            total_amount = 0.0
            for ld in loan_datas:
                sheet.write(rowx, 0, '%s' % ld['shift_date'], data_right)
                sheet.write(rowx, 1, '%s' % ld['shift'], data_right)
                sheet.write(rowx, 2, '%s' % ld['region_id'], data_right)
                sheet.write(rowx, 3, '%s' % ld['shts_id'], data_right)
                sheet.write(rowx, 4, '%s' % ld['name'], data_right)
                sheet.write(rowx, 5, '%s' % ld['partner_id'], data_right)
                sheet.write(rowx, 6, '%s' % ld['car_number_id'], data_right)
                sheet.write(rowx, 7, '%s' % ld['product_id'], data_right)
                sheet.write(rowx, 8, '%s' % ld['price'], data_right)
                sheet.write(rowx, 9, '%s' % ld['total_amount'], data_right)
                sheet.write(rowx, 10, '%s' % ld['shts_reg_user_id'], data_right)
                rowx += 1


                rowx +=1

            sheet.write_merge(rowx, rowx, 0, 4, u'Нийт', data_right)
            #sheet.write_merge(rowx, rowx, 5, 5, total_diff_mile, data_right)

        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 5 * inch
        sheet.col(3).width = 4 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 4 * inch
        sheet.col(6).width = 1 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 1 * inch
        sheet.col(9).width = 3 * inch
        sheet.col(10).width = 1 * inch
        sheet.col(11).width = 3 * inch
        sheet.col(12).width = 1 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.row(2).height = 350
        sheet.row(8).height = 700

        return {'data': book, 'directory_name': u'Зээлээр зөвшөөрсөн тайлан',
                'attache_name': 'Зээлээр зөвшөөрсөн тайлан'}