# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import fields, models, _

import time
import xlwt
import openerp.netsvc
from datetime import date
from datetime import timedelta
from datetime import datetime
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class ProductInfoReport(models.TransientModel):
    """
        01 тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "product.info.report"
    _description = "Product info report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True)
    can_id = fields.Many2one('can.register',string=u'Сав',required=True)
    
    
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        styles = self.get_easyxf_styles()
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'01 Тайлан')
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 5, u'Компани нэр: %s' % (company.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 220'))
        sheet.write(0, 9, u'ШТС: %s' % (self.shts_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 220'))
        
        sheet.write(2, 0, u'Савны дугаар: %s' % (self.can_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))

        sheet.write(2, 6, u'Бүтээгдэхүүний нэр: %s' % (self.can_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        
        sheet.write(2, 12, u'Савны багтаамж: %s' % (self.can_id.size), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        
        sheet.write(4, 0, u'Тайлан эхлэх огноо: %s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(4, 6, u'Тайлан дуусах огноо: %s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(4, 12, u'Өдрийн дундаж үлдэгдэл: ', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))

        rowx = 6
        sheet.write_merge(rowx, rowx+1, 0, 1, u'Огноо', data_center)
        
        sheet.write_merge(rowx, rowx, 2, 6, u'Эхний үлдэгдэл', data_center)
        sheet.write_merge(rowx+1, rowx+1, 2, 2, u'Өндөр', data_center)
        sheet.write_merge(rowx+1, rowx+1, 3, 3, u'ХЖ', data_center)
        sheet.write_merge(rowx+1, rowx+1, 4, 4, u'Темп', data_center)
        sheet.write_merge(rowx+1, rowx+1, 5, 5, u'Литр', data_center)
        sheet.write_merge(rowx+1, rowx+1, 6, 6, u'Кг', data_center)
        
        sheet.write_merge(rowx, rowx, 7, 10, u'Орлого', data_center)
        sheet.write_merge(rowx+1, rowx+1, 7, 7, u'Литр', data_center)
        sheet.write_merge(rowx+1, rowx+1, 8, 8, u'Кг', data_center)
        sheet.write_merge(rowx+1, rowx+1, 9, 9, u'ДХЖ', data_center)
        sheet.write_merge(rowx+1, rowx+1, 10, 10, u'Нийт гүйлт', data_center)
        
        sheet.write_merge(rowx, rowx, 11, 13, u'Зарлага', data_center)
        sheet.write_merge(rowx+1, rowx+1, 11, 11, u'Залин', data_center)
        sheet.write_merge(rowx+1, rowx+1, 12, 12, u'Литр', data_center)
        sheet.write_merge(rowx+1, rowx+1, 13, 13, u'Кг', data_center)
        
        sheet.write_merge(rowx, rowx, 14, 15, u'Байвал зохих', data_center)
        sheet.write_merge(rowx+1, rowx+1, 14, 14, u'Литр', data_center)
        sheet.write_merge(rowx+1, rowx+1, 15, 15, u'Кг', data_center)
        
        sheet.write_merge(rowx, rowx, 16, 18, u'Эцсийн үлдэгдэл', data_center)
        sheet.write_merge(rowx+1, rowx+1, 16, 16, u'Өндөр', data_center)
        sheet.write_merge(rowx+1, rowx+1, 17, 17, u'Хувийн жин', data_center)
        sheet.write_merge(rowx+1, rowx+1, 18, 18, u'Темп', data_center)
        sheet.write_merge(rowx+1, rowx+1, 19, 19, u'Литр', data_center)
        sheet.write_merge(rowx+1, rowx+1, 20, 20, u'Кг', data_center)
        
        sheet.write_merge(rowx, rowx, 21, 22, u'Зөрүү', data_center)
        sheet.write_merge(rowx+1, rowx+1, 21, 21, u'Литр', data_center)
        sheet.write_merge(rowx+1, rowx+1, 22, 22, u'Кг', data_center)
        
        shift_obj = self.env['shift.working.register']
        shts_income_obj = self.env['shts.income']
        days = []
        idate = datetime.strptime(str(self.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(self.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
        rowx +=1
        for day in days:
            date1 = day.strftime('%Y-%m-%d')
            date11 = datetime.strptime(str(date1), DATE_FORMAT)
            
            pdate = date11-timedelta(days=1)
            pre_shift = shift_obj.search([('shts_id','=',self.shts_id.id),('reg_date','=',pdate),('state','in',('confirm','count'))])
            shift = shift_obj.search([('shts_id','=',self.shts_id.id),('reg_date','=',date1),('state','in',('confirm','count'))])
            income = shts_income_obj.search([('shts_id','=',self.shts_id.id),('reg_date','=',date1),('state','=','receive')])
            if pre_shift:
                for line in shift.scale_ids:
                    first_litr = 0.0
                    first_kg = 0.0
                    income_litr = 0.0
                    income_kg = 0.0
                    exp_litr = 0.0
                    exp_kg = 0.0
                    have_litr = 0.0
                    have_kg = 0.0
                    last_litr = 0.0
                    last_kg = 0.0
                    diff_litr = 0.0
                    diff_kg = 0.0
                    dhg_amount = 0.0
                    zarlaga_kg = 0.0
                    for pline in pre_shift.scale_ids:
                        if pline.product_id.id == line.product_id.id:
                            sheet.write_merge(rowx, rowx, 0, 1, '%s'%date1, data_center)
                            sheet.write(rowx, 2, '%s'%pline.name, data_center)
                            sheet.write(rowx, 3, '%s'%pline.self_weight, data_center)
                            sheet.write(rowx, 4, '%s'%pline.temperature, data_center)
                            sheet.write(rowx, 5, '%s'%pline.litr, data_center)
                            sheet.write(rowx, 6, '%s'%pline.kg, data_center)
                    
                    for li in income.line_ids:
                        if li.product_id.id == line.product_id.id:
                            income_litr = li.litr
                            income_kg = li.kg

                            sheet.write(rowx, 7, li.litr, data_center)
                            sheet.write(rowx, 8, li.kg, data_center)
                            sheet.write(rowx, 9, li.kg, data_center)
                            sheet.write(rowx, 10, li.kg, data_center)
                    for exp in shift.mile_target_ids:
                        if exp.product_id.id == line.product_id.id:
                            exp_kg = exp.kg
                            exp_litr = exp.litr
                            sheet.write(rowx, 11,  exp.zalin, data_center)
                            sheet.write(rowx, 12, exp.litr, data_center)
                            sheet.write(rowx, 13, exp.kg, data_center)
                            
                    have_kg = line.kg + income_kg - exp_kg 
                    have_litr = line.litr + income_litr - exp_litr
                    
                    
                    sheet.write(rowx, 14, have_litr, data_center)
                    sheet.write(rowx, 15, have_kg, data_center)
                    
                    zarlaga_kg = zarlaga_litr * dhg_amount
                    sheet.write(rowx, 16, line.name, data_center)
                    sheet.write(rowx, 17, line.self_weight, data_center)
                    sheet.write(rowx, 18, line.temperature, data_center)
                    sheet.write(rowx, 19, line.litr, data_center)
                    sheet.write(rowx, 20, line.kg, data_center)
                    diff_litr = have_litr - last_litr
                    diff_kg = have_kg - last_kg
                    sheet.write(rowx, 21, diff_litr, data_center)
                    sheet.write(rowx, 22, diff_kg, data_center)
                    rowx +=1
        
        

        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 2 * inch
        sheet.col(2).width = 3 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 3 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 3 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 5 * inch
        sheet.col(9).width = 3 * inch
        sheet.col(10).width = 3 * inch
        sheet.col(11).width = 3 * inch
        sheet.col(12).width = 3 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.row(0).height = 350
        sheet.row(8).height = 700

        return {'data':book, 'directory_name':u'Тосны хөдөлгөөний тайлан',
                'attache_name':'Тосны хөдөлгөөний тайлан'}