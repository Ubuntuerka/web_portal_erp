# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import xlwt
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class RegionalSalesRevenueTypeReportApi(models.TransientModel):
    _name = 'regional.sales.revenue.type.report.api'
    _description = 'Regional sales revenue type report api'

    region_id = fields.Many2one('shts.region', u'Бүсийн нэр', required=True)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)
    line_ids = fields.One2many('regional.sales.revenue.type.report.tree', 'regional_report_ids', 'line')

    @api.model
    def default_get(self, fields):
        if self.env.context.get('active_model') !='regional.sales.revenue.type.report' or not self.env.context.get('active_ids'):
            raise UserError(_('UserError con not'))
        rec = super(RegionalSalesRevenueTypeReportApi, self).default_get(fields)
        revenue_ids = self.env['regional.sales.revenue.type.report'].browse(self.env.context['active_ids'])
        rec['region_id'] = revenue_ids.region_id.id
        rec['start_date'] = revenue_ids.start_date
        rec['end_date'] = revenue_ids.end_date
        dats = self.env.context.get('line_ids')
        list =[]
        for li in dats:
            list.append((0, 0, li))
        rec['line_ids'] = list
        return rec


class RegionalSalesRevenueTypeReportTree(models.TransientModel):
    _name = 'regional.sales.revenue.type.report.tree'
    _description = 'Regional sales revenue type report tree'

    region_id = fields.Many2one('shts.region', u'Бүсийн нэр', required=True)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)
    shift_date = fields.Date(string=u'Ээлжийн огноо', required=True)
    shts_id = fields.Many2one('shts.register', u'ШТС')
    gasol_sale = fields.Float(string=u'Шатахуун борлуулалт')
    shift = fields.Integer(string=u'Ээлж')
    ttm_sale = fields.Float(string=u'ТТМ-ын борлуулалт')
    service_sale = fields.Float(string=u'Авто сервисийн борлуулалт')
    date = fields.Date(string=u'Огноо')
    cash = fields.Float(string=u'Бэлэн')
    t_card = fields.Float(string=u'Т Карт')
    card = fields.Float(string=u'Карт')
    bank_card = fields.Float(string=u'bank_card')
    discount = fields.Float(string='Хөнгөлөлт')
    loan = fields.Float(string='Зээл')
    tuvd_card = fields.Float('Төвд карт')
    mobile = fields.Float(string=u'Шилжүүлэг')
    talon = fields.Float(string=u'Талон')
    diff = fields.Float(string=u'Зөрүү')
    total_amount = fields.Float(string=u'Нийт борлуулалт')
    total_pay = fields.Float(string=u'Нийт')
    regional_report_ids = fields.Many2one('regional.sales.revenue.type.report.api', 'regional')


class RegionalSalesRevenueTypeReport(models.TransientModel):
    _inherit = "abstract.report.excel"
    _name = 'regional.sales.revenue.type.report'
    _description = 'Regional sales revenue type report'

    region_id = fields.Many2one('shts.region', u'Бүсийн нэр', required=True)
    start_date = fields.Date(string=u'Эхлэх огноо', required=True)
    end_date = fields.Date(string=u'Дуусах огноо', required=True)

    def get_action_report_view(self):
        """Тайлангийн загварыг тооцоолж байна"""
        shift_obj = self.env['shift.working.register']
        shts_obj = self.env['shts.register']
        shts_ids = shts_obj.search([('region_id', '=', self.region_id.id)], order='name asc')
        context = dict(self.env.context or {})
        lin_data = []
        if shts_ids:
            for shts in shts_ids:
                gasol_sale = 0.0
                ttm_sale = 0.0
                service_sale = 0.0
                all_gasol_sale = 0.0
                all_ttm_sale = 0.0
                all_service_sale = 0.0
                total_amount = 0.0
                total_pay = 0.0

                all_cash = 0.0
                all_bank_card = 0.0
                all_mobile = 0.0
                all_talon = 0.0
                all_loan = 0.0
                all_t_card = 0.0
                all_tuvd_card = 0.0
                all_discount = 0.0
                all_card = 0.0
                all_diff = 0.0

                cash =0.0
                bank_card = 0.0
                mobile = 0.0
                talon = 0.0
                loan = 0.0
                t_card = 0.0
                tuvd_card = 0.0
                discount = 0.0
                card = 0.0
                diff =0.0

                shift_ids = shift_obj.search([('state', 'in', ('confirm', 'count')), ('shts_id', '=', shts.id),
                                              ('shift_date', '>=', self.start_date), ('shift_date', '<=', self.end_date)])
                for shift in shift_ids:
                    for gs in shift.shift_product_ids:
                        gasol_sale += gs.total_amount
                    for ttm in shift.ttm_sale_ids:
                        ttm_sale += ttm.total_price
                    for tvt in shift.tvt_sale_ids:
                        service_sale += tvt.total_price

                    for line in shift.payment_shts_id:
                        if line.name.type == 'cash' or line.type == 'cash':
                            cash += line.amount
                        elif line.type == 'tov_card':
                            t_card += line.amount
                        elif line.type == 'discount' or line.name.type == 'discount':
                            discount += line.amount
                        elif line.type == 'bank_card' or line.name.type == 'bank_card':
                            card += line.amount
                        elif line.type == 'loan':
                            loan += line.amount
                        elif line.type == 'mobile':
                            mobile += line.amount
                        elif line.type == 'talon':
                            talon += line.amount
                        elif line.type == 'tuvd_card':
                            tuvd_card += line.amount

                    all_gasol_sale += gasol_sale
                    all_ttm_sale += ttm_sale
                    all_service_sale += service_sale
                    all_cash += cash
                    all_bank_card += bank_card
                    all_mobile += mobile
                    all_talon += talon
                    all_loan += loan
                    all_t_card += t_card
                    all_tuvd_card += tuvd_card
                    all_discount += discount
                    all_card += card
                    all_diff += diff

                total_amount = service_sale + ttm_sale + gasol_sale
                total_pay = talon + cash + bank_card + mobile + loan + t_card + tuvd_card + discount + card
                all_diff = total_pay - total_amount

                datas = {

                    'shts_id': shts.id,
                    'total_amount': total_amount,
                    'total_pay': total_pay,
                    'cash': cash,
                    'bank_card': bank_card,
                    'mobile': mobile,
                    'talon': talon,
                    'loan': loan,
                    't_card': t_card,
                    'tuvd_card': tuvd_card,
                    'discount': discount,
                    'card': card,
                    'diff': all_diff,
                }
                lin_data.append(datas)
        context['line_ids'] = lin_data

        compose_form = self.env.ref('mn_web_portal.regional_sales_revenue_type_report_api_view')
        action = {
            'name': _(u'Бүсийн төлбөрийн төрлөөр'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'regional.sales.revenue.type.report.api',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': context,
        }
        return action

    def export_data(self):
        """Тайлангийн загварыг тооцоолж байна. Та түр хүлээнэ үү"""

        region = self.env['shts.region'].browse(self.region_id.id)

        ezxf = xlwt.easyxf

        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'Бүсийн борлуулалтын тайлан :)')
        sheet.portrait = False
        data_style = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_bold = ezxf(
            'font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_right = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_left = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf(
            'font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_center = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Бүсийн нэр: %s' % (region.name,),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 2, 0, 0, u'Эхлэх огноо: %s' % (self.start_date,),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 180'))
        sheet.write(3, 2, 0, 13, u'Дуусах огноо: %s' % (self.end_date,),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 180'))
        sheet.write_merge(2, 2, 0, 14, u'Бүсийн борлуулалтын тайлан',
                          ezxf('font:bold on;align:wrap off,vert centre,horiz center;font: height 220'))

        rowx = 5
        sheet.write(rowx, 0, u'Ээлж', data_bold)
        sheet.write(rowx, 1, u'ШТС-ийн нэр', data_bold)
        sheet.write(rowx, 2, u'Шатахууны борлуулалт', data_bold)
        sheet.write(rowx, 3, u'ТТМ борлуулалт', data_bold)
        sheet.write(rowx, 4, u'ТТМ борлуулалт', data_bold)
        sheet.write(rowx, 5, u'Нийт дүн', data_bold)
        sheet.write(rowx, 6, u'Шилжүүлэг', data_bold)
        sheet.write(rowx, 7, u'Бэлэн', data_bold)
        sheet.write(rowx, 8, u'Т-карт', data_bold)
        sheet.write(rowx, 9, u'Зээл', data_bold)
        sheet.write(rowx, 10, u'Толан', data_bold)
        sheet.write(rowx, 11, u'Төвт карт', data_bold)
        sheet.write(rowx, 12, u'Карт', data_bold)
        sheet.write(rowx, 13, u'Хөнгөлөлт', data_bold)
        sheet.write(rowx, 14, u'Банк карт', data_bold)
        sheet.write(rowx, 15, u'Зөрүү', data_bold)

        rowx += 1
        """Тайлангийн загварыг тооцоолж байна"""
        shift_obj = self.env['shift.working.register']
        shts_obj = self.env['shts.register']
        shts_ids = shts_obj.search([('region_id', '=', self.region_id.id)], order='name asc')
        context = dict(self.env.context or {})
        lin_data = []
        if shts_ids:
            for shts in shts_ids:
                gasol_sale = 0.0
                ttm_sale = 0.0
                service_sale = 0.0
                all_gasol_sale = 0.0
                all_ttm_sale = 0.0
                all_service_sale = 0.0
                total_amount = 0.0
                total_pay = 0.0

                all_cash = 0.0
                all_bank_card = 0.0
                all_mobile = 0.0
                all_talon = 0.0
                all_loan = 0.0
                all_t_card = 0.0
                all_tuvd_card = 0.0
                all_discount = 0.0
                all_card = 0.0
                all_diff = 0.0

                cash = 0.0
                bank_card = 0.0
                mobile = 0.0
                talon = 0.0
                loan = 0.0
                t_card = 0.0
                tuvd_card = 0.0
                discount = 0.0
                card = 0.0
                diff = 0.0

                shift_ids = shift_obj.search([('state', 'in', ('confirm', 'count')), ('shts_id', '=', shts.id),
                                              ('shift_date', '>=', self.start_date),
                                              ('shift_date', '<=', self.end_date)])
                for shift in shift_ids:
                    for gs in shift.shift_product_ids:
                        gasol_sale += gs.total_amount
                    for ttm in shift.ttm_sale_ids:
                        ttm_sale += ttm.total_price
                    for tvt in shift.tvt_sale_ids:
                        service_sale += tvt.total_price

                    for line in shift.payment_shts_id:
                        if line.name.type == 'cash' or line.type == 'cash':
                            cash += line.amount
                        elif line.type == 'tov_card':
                            t_card += line.amount
                        elif line.type == 'discount' or line.name.type == 'discount':
                            discount += line.amount
                        elif line.type == 'bank_card' or line.name.type == 'bank_card':
                            card += line.amount
                        elif line.type == 'loan':
                            loan += line.amount
                        elif line.type == 'mobile':
                            mobile += line.amount
                        elif line.type == 'talon':
                            talon += line.amount
                        elif line.type == 'tuvd_card':
                            tuvd_card += line.amount

                    all_gasol_sale += gasol_sale
                    all_ttm_sale += ttm_sale
                    all_service_sale += service_sale
                    all_cash += cash
                    all_bank_card += bank_card
                    all_mobile += mobile
                    all_talon += talon
                    all_loan += loan
                    all_t_card += t_card
                    all_tuvd_card += tuvd_card
                    all_discount += discount
                    all_card += card
                    all_diff += diff

                total_amount = service_sale + ttm_sale + gasol_sale
                total_pay = talon + cash + bank_card + mobile + loan + t_card + tuvd_card + discount + card
                all_diff = total_pay - total_amount

            sheet.write(rowx, 0, '%s' % shift.shift, data_right)
            sheet.write(rowx, 1, '%s' % shift.shts_id.name, data_right)
            sheet.write(rowx, 2, round(gasol_sale, 2), data_right)
            sheet.write(rowx, 3, round(ttm_sale, 2), data_right)
            sheet.write(rowx, 4, round(service_sale, 2), data_right)
            sheet.write(rowx, 5, round(total_amount, 2), data_right)
            sheet.write(rowx, 6, round(cash, 2), data_right)
            sheet.write(rowx, 7, round(card, 2), data_right)
            sheet.write(rowx, 8, round(t_card, 2), data_right)
            sheet.write(rowx, 9, round(discount, 2), data_right)
            sheet.write(rowx, 10, round(loan, 2), data_right)
            sheet.write(rowx, 11, round(tuvd_card, 2), data_right)
            sheet.write(rowx, 12, round(mobile, 2), data_right)
            sheet.write(rowx, 13, round(talon, 2), data_right)
            sheet.write(rowx, 14, round(bank_card, 2), data_right)
            sheet.write(rowx, 15, round(diff, 2), data_right)
            rowx += 1

        inch = 1000
        sheet.col(0).width = 2 * inch
        sheet.col(1).width = 7 * inch
        sheet.col(2).width = 4 * inch
        sheet.col(3).width = 4 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 4 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 4 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 4 * inch
        sheet.col(13).width = 4 * inch
        sheet.col(14).width = 4 * inch
        sheet.col(15).width = 4 * inch
        sheet.row(0).height = 350
        sheet.row(8).height = 700

        return {'data': book, 'directory_name': u'Бүсийн борлуулалтын орлогын тайлан',
                'attache_name': 'Бүсийн борлуулалтын орлогын тайлан'}
