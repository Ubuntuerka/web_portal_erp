# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"



class RetailSaleReport(models.TransientModel):
    """
        14 маягт саваар тайлан
    """
    _inherit = "abstract.report.excel"
    _name = "retail.sale.report"
    _description = "Retail sale report"
    
    
    company_id = fields.Many2one('res.company',string=u'Компани',required=True,default=lambda self: self.env['res.company']._company_default_get('account.asset.detail.report'))
    shts_id = fields.Many2one('shts.register',string=u'ШТС',required=True,default=lambda self: self.env.user.warehouse_id.id)
    start_date = fields.Date(string=u'Эхлэх огноо',required=True)
    end_date = fields.Date(string=u'Дуусах огноо',required=True)
    
    
    def get_export_data(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
            тооцоолж байрлуулна.
        '''

        company = self.env['res.company'].browse(self.company_id.id)

        
        ezxf = xlwt.easyxf
        
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet(u'14 Маягт төлбөрийн төрлөөр')
        sheet.portrait = False
        data_style = ezxf('font: bold off; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold = ezxf('font: bold on; align: wrap on, vert centre, horiz centre; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_right = ezxf('font: bold off; align: wrap on, vert centre, horiz right; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_left = ezxf('font: bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_bold_left = ezxf('font: bold on; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')
        data_center = ezxf('font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;','#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 4, u'ШТС: %s' % (self.shts_id.name), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))

        sheet.write(2, 0, u'Эхлэх огноо: %s' % (self.start_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 4, u'Дуусах огноо: %s' % (self.end_date), ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 2, u'Бүтээгдэхүүний төрлөөр борлуулалтыг харвал', ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        rowx = 4

        sheet.write(rowx, 0, u'№', data_right)
        sheet.write(rowx, 1, u'Шатахууны нэр', data_right)
        sheet.write(rowx, 2, u'Бүсийн бүх ШТС-уудын 1-р 7 хоногийн Хоногийн дундаж борлуулалт, кг 7 хоног', data_right)
        sheet.write(rowx, 3, u'Бүсийн бүх ШТС-уудын 2-р 7 хоногийн Хоногийн дундаж борлуулалт, кг 7 хоног', data_right)
        sheet.write(rowx, 4, u'Бүсийн бүх ШТС-уудын 3-р 7 хоногийн Хоногийн дундаж борлуулалт, кг 7 хоног', data_right)
        sheet.write(rowx, 5, u'Бүсийн бүх ШТС-уудын 4-р 7 хоногийн Хоногийн дундаж борлуулалт, кг 7 хоног', data_right)
        sheet.write(rowx, 6, u'Бүсийн бүх ШТС-уудын 5-р 7 хоногийн Хоногийн дундаж борлуулалт, кг 7 хоног', data_right)


      
        
        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 8 * inch
        sheet.col(3).width = 8 * inch
        sheet.col(4).width = 8 * inch
        sheet.col(5).width = 8 * inch
        sheet.col(6).width = 8 * inch

        sheet.row(4).height = 400

        return {'data':book, 'directory_name':u'Миллийн заалт',
                'attache_name':'Миллийн заалт'}

