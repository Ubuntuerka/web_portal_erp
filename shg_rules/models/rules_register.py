# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree


class JuramType(models.Model):
    _name = 'juram.type'

    name = fields.Char('Name', required=True)


class JuramArea(models.Model):
    _name = 'juram.area'

    name = fields.Char('Name', required=True)


class ResUsers(models.Model):
    _inherit = 'res.users'

    use_area_ids = fields.Many2many('juram.area', string='Use area')


class Juram(models.Model):
    _name = 'juram'

    name = fields.Char('Name', required=True)
    number = fields.Char('Number', required=True)
    dd = fields.Char('№', copy=False, default=lambda self: self.env['ir.sequence'].next_by_code('juram'))
    department_id = fields.Many2one('hr.department', 'Department', required=True)
    job_id = fields.Many2one('hr.job', 'Job', required=True)

    # use_department_ids = fields.Many2one('hr.department', 'juram_id', 'Use department')
    use_area_ids = fields.Many2many('juram.area', 'juram_area_rel', 'juram_id', 'juram_area_id', 'Use area')
    version = fields.Char('Version', size=8, required=True)
    date = fields.Date('Approve date', required=True, default=fields.Date.today)
    start_date = fields.Date('Start Date')
    is_renew = fields.Boolean('Renew')
    dugaar = fields.Char('Dugaar', size=5)
    is_english = fields.Boolean('Is english')
    type_id = fields.Many2one('juram.type', 'Type', required=True)
    note = fields.Text('Note')
    # day = fields.Char('Day')
    # month = fields.Char('Month')
    # year = fields.Char('Year')
    #
    # @api.onchange('date')
    # def onchange_date(self):
    #     for rec in self:
    #         if rec.date:
    #             rec.year = rec.date.year
    #             rec.month = rec.date.month
    #             rec.day = rec.date.day

