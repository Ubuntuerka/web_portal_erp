# -*- coding: utf-8 -*-
##############################################################################
#
#
#
#
#
##############################################################################

{
    "name": "Rules module",
    "version": "1.0",
    "author": "Uuganbayar",
    "category": "document",
    "description": """This module rules management module.""",
    "depends": [
        'hr',
    ],
    'update_xml': [
        'security/rules_security_view.xml',
        'views/rules_sequence_view.xml',
        'views/rules_register_view.xml',
        'views/rules_area_register_view.xml',
        'views/rules_type_register_view.xml',
        'security/ir.model.access.csv',
        'views/menu_view.xml',
    ],
    'test': [],
    'installable': True,
    'auto_install': False,
}