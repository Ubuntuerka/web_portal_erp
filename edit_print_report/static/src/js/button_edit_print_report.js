odoo.define('edit_print_report.button_edit_print_report', function (require) {
    "use strict";

    var FormRenderer = require('web.FormRenderer');
    var Dialog = require('web.Dialog');

    var WidgetEditPrintReport = require('edit_print_report.widget_sign_print_report');
    var ajax = require('web.ajax');    
    
    var core = require('web.core');
    var QWeb = core.qweb;

    var _t = core._t;
    var _lt = core._lt;
    var QWeb = core.qweb;

    ajax.loadXML('/edit_print_report/static/src/xml/button_edit_print_report.xml', QWeb);

    FormRenderer.include({
        init(parent, state, params) {
            this._super(...arguments);
        },
        willStart: function () {
            var self = this;
            var def = this._rpc({
                model: 'ir.actions.report',
                method: 'search_read',
                args: [[['model', '=', self.state.model]], ['id', 'name', 'report_name']],
            }).then(function (result) {
                self.edit_report_data = result;
            });
            return Promise.all([this._super.apply(this, arguments), def]);
        },
        _renderView: function () {
            var self = this;
            var def = this._super.apply(this, arguments);
            def.then(function () {
                if (self.mode === 'readonly' && self.edit_report_data.length > 0) {
                    self._renderShareButton();
                }
            });
            return def;
        },
        _renderShareButton: function () {
            if (this.state.viewType === "form" && this.edit_report_data.length > 0) {
                var $signbutton = $('<div>');
                $signbutton.addClass("edit_print_report");
                $signbutton.append($('<button>').addClass("btn btn-primary").append($('<i class="fa-file-signatire"/>')));
                $signbutton.on('click', _.bind(this._clickSignButton, this));
                this.$el.find('.o_form_sheet').append($signbutton);
            }
        },
        _clickSignButton: async function (ev) {
            ev.stopPropagation();
            ev.preventDefault();
            var self = this;
            var data = self.edit_report_data;
            if (data.length > 0) {
                self._openSingPrintDialog(data);
            }
        },
        _openSingPrintDialog: function (data) {
            var self = this;
            self.editDialog = new Dialog(this, {
                size: 'medium',
                title: _t("Edit Print Report"),
                buttons: [
                    {
                        text: _t('Edit'), classes: 'btn-primary', close: false, click: function (ev) {
                            ev.preventDefault();
                            ev.stopPropagation();

                            var reportSelect = self.editDialog.$el.find('select[id="report"]');
                            var report_name = reportSelect.find(':selected');

                            if (report_name){                                                                
                                if (report_name.length === 0) {
                                    reportSelect.addClass('highlight_alert');
                                }else{
                                    reportSelect.removeClass('highlight_alert');
                                }

                                if (report_name) {
                                    self._getReportData(report_name.attr('report_name'));
                                }
                            }
                            else{
                                return self.displayNotification({ message: _t("Please select report or upload file.") });
                            }
                            
                        }
                    },
                    {
                        text: _t('Discard'), close: true
                    }
                ],
                $content: $(QWeb.render('edit_print_report.editDialog', {
                    reports: data,
                })),
            });
            self.editDialog.opened().then(function () {
                var checkboxs = self.editDialog.$('input[type="checkbox"]');
                self.editDialog.$('input[type="checkbox"]').click(function (ev) {
                    ev.stopPropagation();
                    for (var i = 0; i < checkboxs.length; i++) {
                        checkboxs[i].checked = false;
                    }
                    var checkboxValue = ev.target.value;
                    if (checkboxValue === 'select_report') {
                        self.editDialog.$('#select_report')[0].style.display = '';
                        self.editDialog.$('#upload_file')[0].style.display = 'none';
                    }
                    if (checkboxValue === 'upload_file') {
                        self.editDialog.$('#upload_file')[0].style.display = '';
                        self.editDialog.$('#select_report')[0].style.display = 'none';
                    }
                    ev.target.checked = true
                });

                self.editDialog.$('.pdf_add_new').click(function (ev) {
                    ev.preventDefault();
                    ev.stopPropagation();
                    const $newPdfInput = $('<input/>', {
                        type: 'file',
                        name: 'files[]',
                        accept: 'application/pdf',
                    });
                    $newPdfInput.on('change', async e => {
                        const files = $newPdfInput[0].files;
                        $newPdfInput.remove();
                        for (const file of files) {
                            if(file){
                                ev.currentTarget.nextElementSibling.innerHTML = file.name;
                                self.uploaded_file = file;
                            }                          
                        }
                    });
                    $newPdfInput.click();
                });
            });
            self.editDialog.open();
        },
        base64ToBlob: function (base64, type = "application/octet-stream") {
            var self = this;
            const binStr = atob(base64);
            const len = binStr.length;
            const arr = new Uint8Array(len);
            for (let i = 0; i < len; i++) {
                arr[i] = binStr.charCodeAt(i);
            }
            return new Blob([arr], { type: type });
        },
        _getReportData: async function (report_name) {
            var self = this;
            var defs = [];

            var active_model = self.state.model;
            var active_id = self.state.res_id;

            self._editPrintReport = new WidgetEditPrintReport(self, {
                'name': self.state.data.name,
                'active_id': active_id,
                'active_model': active_model,
                'report_name': report_name,
            });
            self._editPrintReport.appendTo(self.el).then(function () {
                self.editDialog.close();
            });
        },
    });

});
