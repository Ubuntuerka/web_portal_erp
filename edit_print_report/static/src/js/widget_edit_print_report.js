odoo.define('edit_print_report.widget_sign_print_report', function (require) {
    'use strict';

    var core = require('web.core');
    var QWeb = core.qweb;

    var Widget = require('web.Widget');
    var config = require('web.config');
    var Dialog = require('web.Dialog');
    var session = require('web.session');
    var framework = require('web.framework');
    var ajax = require('web.ajax');    

    var _t = core._t;
    var _lt = core._lt;

    ajax.loadXML('/edit_print_report/static/src/xml/widget_edit_print_report.xml', QWeb);
    
    var WidgetEditPrintReport = Widget.extend({
        template: "WidgetEditPrintReport",
        xmlDependencies: [
            '/edit_print_report/static/src/xml/widget_edit_print_report.xml',
        ],

        custom_events: Object.assign({}, Widget.prototype.custom_events, {
            
        }),

        init: function (parent, params) {
            this._super(parent);
            this.parent = parent;
            this.params = params;
            this.active_id = params.active_id || false;
            this.active_model = params.active_model || false;
            this.report_name = params.report_name || false;
            this.data = params.data || false;
            this.name = params.name || 'report.pdf';

            if (!this.active_id || !this.report_name) {
                return;
            }
        },
        start: function () {
            var self = this;

            this.dialog = this.$el;
            this.dialog.modal('show');

            this.dialog.find('.edit_report_close').on('click', this._onClickClose.bind(this));
            this.dialog.find('.edit_print_report').on('click', this._onClickPrint.bind(this, this.data))

            this.report_url = `/report/html/${this.report_name}/${this.active_id}`;
            this.$iframe = this.$el.find('iframe')[0];
            this.$iframe.addEventListener("load", function () {
                self.waitForLoadHTML();
            });
            this.$iframe.addEventListener("scroll", function(e) {
                
            })
            this.$iframe.setAttribute("src", self.report_url);
            this.$text_area = this.$el.find('textarea')[0];
        },
        waitForLoadHTML: async function () {
            var self = this;
            if (!$(this.$iframe).length || !$(this.$iframe).contents().length || $(this.$iframe).contents().find('#errorMessage').is(":visible")) {
                return Dialog.alert(this, _t("To edit report, you'll need a supported PDF format!"));
            }

            if (this.$iframe) {
                var contentDocument = this.$iframe.contentDocument;
                contentDocument.body.contentEditable = true;
            }

            const iframeDocument = $(this.$iframe).contents();
            const iframeHead = iframeDocument.find('head');

            if (iframeHead) {
                await this.afterLoadHTML();
            } else {
                var self = this;
                setTimeout(function () {
                    self.waitForLoadHTML();
                }, 10);
            }
        },
        afterLoadHTML: function () {
            var self = this;
            var defs = [];

            const iframeDocument = $(this.$iframe).contents();
            this.setElement(iframeDocument.find('html'));

            defs.push(this._rpc({
                route: '/edit_print_report/render_assets_editor_iframe',
                params: {
                    args: [{
                        debug: config.isDebug()
                    }]
                }
            }).then(function (html) {
                $.each($(html), function (key, value) {
                    self.$('head')[0].appendChild(value);                    
                });
            }));

            Promise.all(defs).then(function () {
                self._loadEditorJS();
            });
        },
        _loadEditorJS: function () {
            var self = this;  
            if (self.$iframe && self.$iframe.contentWindow){
                self.editor = self.$iframe.contentWindow.inLine('#wrapwrap')
                //self.editor.set({theme:'dark'}); //set theme
            }else{

            }
        },    
        _onClickClose(ev) {
            ev.stopPropagation();
            var self = this;
            self.destroy();
        },

        destroy: function () {
            var self = this;
            if (self.dialog) {
                if (self.editor){
                    self.editor.destroy();
                }                
                self.dialog.modal('hide');
                self.dialog.remove();
            }
            self._super.apply(this, arguments);
        },
        _onClickPrint: function(){
            var self = this;
            const iframeDocument = $(this.$iframe).contents();
            const iframeHTML = iframeDocument.find('html');
            const html = '<!DOCTYPE html><html>' + iframeHTML.html() + '</html>'
            framework.blockUI();
            session.get_file({
                url: '/edit_print_report/print_pdf_content',
                data: { html: html},
                complete: framework.unblockUI,
                error: (error) => this.call('crash_manager', 'rpc_error', error),
            });
        }
    });
    return WidgetEditPrintReport;
});
