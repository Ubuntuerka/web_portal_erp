Print Report Editor | Print Report Quick Editor | Report Quick Editor | Report Editor
=========================================================================
- This module allows users to edit change the selected print report, and formatting options include 
bold, italic, underline, strike, align, unordered list, ordered list, font colour, backdrop colour, and hyper link.

Installation
============
- Copy edit_print_report module to addons folder
- Install the module normally like other modules.
