# -*- coding: utf-8 -*-
#################################################################################
# Author      : CFIS (<https://www.cfis.store/>)
# Copyright(c): 2017-Present CFIS.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.cfis.store/>
#################################################################################

{
    "name": "Print Report Editor | Print Report Quick Editor | Report Quick Editor | Report Editor",
    "summary": """
        This module allows users to quickly edit the selected print report, and formatting options include 
        bold, italic, underline, strike, align, unordered list, ordered list, font colour, backdrop colour, and hyper link.
        """,
    "version": "13.0.1",
    "description": """
        This module allows users to quickly edit the selected print report, and formatting options include 
        bold, italic, underline, strike, align, unordered list, ordered list, font colour, backdrop colour, and hyper link.
        """,    
    "author": "CFIS",
    "maintainer": "CFIS",
    "license" :  "Other proprietary",
    "website": "https://www.cfis.store",
    "images": ["images/edit_print_report.png"],
    "category": "Extra Tools",
    "depends": [
        "base",
    ],
    "data": [
        "views/assets.xml",
    ],
    "qweb": [
        "/edit_print_report/static/src/xml/*.xml"
    ],
    "installable": True,
    "application": True,
    "price"                 :  80,
    "currency"              :  "EUR",
    "pre_init_hook"         :  "pre_init_check",
}
