import base64
import json
import werkzeug

from odoo import fields, http, SUPERUSER_ID, tools, _
from odoo.http import request
from odoo.exceptions import ValidationError, UserError

import uuid
import random

from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, _, tools

from odoo.addons.web.controllers.main import _serialize_exception
from odoo.tools import html_escape
from odoo.tools import html2plaintext, html_escape

import base64
import io

class EditPrintReport(http.Controller):
    
    @http.route("/edit_print_report/render_assets_editor_iframe", type="json", auth="user")
    def render_assets_editor_iframe(self, **kw):
        context = {'debug': kw.get('debug')} if 'debug' in kw else {}
        return request.env['ir.ui.view'].sudo().render_template('edit_print_report.assets_editor_iframe', context)
    
    @http.route("/edit_print_report/print_pdf_content", type="http", auth="user")
    def print_pdf_content(self, html=None, **kw):
        try:
            if html:
                report = request.env['ir.actions.report']
                context = {}
                context['debug'] = False
                bodies, html_ids, header, footer, specific_paperformat_args = report.sudo().with_context(context)._prepare_html(html)
                pdf_content = report._run_wkhtmltopdf(
                    bodies,
                    header=header,
                    footer=footer,
                    landscape=context.get('landscape'),
                    specific_paperformat_args=specific_paperformat_args,
                    set_viewport_size=context.get('set_viewport_size'),
                )
                pdfhttpheaders = [
                    ('Content-Type', 'application/pdf'), 
                    ('Content-Disposition', 'attachment; filename=' + 'download' + '.pdf;'),
                    ('Content-Length', len(pdf_content))]
                return request.make_response(pdf_content, headers=pdfhttpheaders)
        except Exception as e:
            se = _serialize_exception(e)
            error = {
                'code': 200,
                'message': 'Odoo Server Error',
                'data': se
            }
            return request.make_response(html_escape(json.dumps(error)))