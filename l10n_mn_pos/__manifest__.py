# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'MN Point of Sale',
    'version': '1.0.1',
    'category': 'Point Of Sale',
    'sequence': 20,
    'website': 'https://www.tengersoft.co',
    'description': """    
                Mongolian pos vat posapi connect module
    """,
    'depends': [
                'point_of_sale',
                'stock',
                'l10n_mn_product_category',
                'account',
                'base',
                ],
    'data': [
       # 'data/district_code_data.xml',
       'data/vat_pos_send_service.xml',
        'security/ir.model.access.csv',
        
        'views/pos_sequence_view.xml',
        'views/vat_config_view.xml',
        'views/pos_config_inherit.xml',
        'views/account_journal_inherit_view.xml',
        'views/point_of_sale_inherit.xml',
        'views/menu_view.xml',
        'views/pos_receipt_view.xml',
        'views/report_view.xml',
    ],
    
    'installable': True,
    'application': True,
    'qweb': ['static/src/xml/pos.xml'],
    'website': 'www.tengersoft.mn',
}