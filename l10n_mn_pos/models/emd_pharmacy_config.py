# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import timedelta
from functools import partial

import psycopg2
import pytz

from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
from odoo.http import request
import odoo.addons.decimal_precision as dp
import json,requests

_logger = logging.getLogger(__name__)

class EmdPharmacyConfig(models.Model):
    _name = 'emd.pharmacy.config'
    
    name = fields.Char(string='ЭМД тохиргоо')
    company_id = fields.Many2one('res.company',u'Компани',default=lambda self: self.env.user.company_id,required=True)
    reg_user_id = fields.Many2one('res.users',u'Бүртгэсэн ажилтан',default=lambda self: self.env.user.id,required=True)
    username = fields.Char(u'Нэвтрэх нэр',required=True)
    password = fields.Char(u'Нууц үг',required=True)
    url = fields.Char(u'Хаяг',required=True)
    check_data_see_url = fields.Char(u'Хөнгөлөлттэй эмийн жагсаалт')
    check_url = fields.Char(u'Жор шалгах',required=True)
    batch_url = fields.Char(u'Багцаар илгээх',required=True)
    return_url = fields.Char(u'Буцаалт хийх',required=True)
    send_url = fields.Char(u'Жор илгээх',required=True)
    data = fields.Char(u'Угтвар',required=True)
    reg_data = fields.Date(u'Бүртгэсэн огноо', default=fields.Date.context_today,required=True)
    logg = fields.Text(u'Алдааны мэдээлэл')
    sale_line_ids = fields.One2many('emd.pharmacy.sale','config_ids','Med line')
    product_ids = fields.One2many('emd.pharmacy.data.product','parent_id','Product line')
    
    
    def medicine_download_data(self):
        obj = self.browse( self.ids)
        medicine_data_list_obj = self.env['emd.pharmacy.data.product']
        header  = {
                 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'en-US,en;q=0.5',
                 'Authorization':'Basic VV9mZl05Qmp5WlhMbUcmZHcmOlo3JHtFenlyRDRheUN9RkxkJg==',
                'Connection': 'keep-alive',
                'Host': 'ws.emd.gov.mn',
                 'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0'
                }
        url = obj.url
        access_token = ''
        param = {
                'grant_type':obj.data,
                'username':obj.username,
                'password':obj.password
            }
        
        request = requests.post(url,  params=param,headers=header,verify=False)
        result = json.loads(request.content)
        access_token = result['access_token']
        date_headers = {
                            'Content-Type': 'application/json',
                            'Connection': 'keep-alive',
                            'Host': 'ws.emd.gov.mn',
                            'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0'
                            }
        url_data = {
                    'field':'null',
                    'value':'null',
                    'type':'null',
                    'order':'null',
                    'dir':'null',
            }
        param_data = {
                'page':1,
                'size':1000,
                'access_token':access_token,
                }
        
        data_request = requests.post(obj.check_data_see_url, params=param_data, data=json.dumps(url_data), headers=date_headers,verify=False)
        if data_request.status_code == 200:
            data_result = json.loads(data_request.content)
            count = 0.0           
            for jdata in data_result['data']:
                count +=1
                med_data_line_id = medicine_data_list_obj.search([('dataId','=',jdata['id']),('id','=',self.id)])
                if med_data_line_id.id == False:
                    if jdata['tbltType'] == None:
                        jdata['tbltType'] = 0
                    medicine_data_list_obj.create({
                                                 'parent_id':obj.id,
                                                 'tbltType':str(jdata['tbltType']),
                                                 'dataId':jdata['id'],
                                                 'tbltNameMon':jdata['tbltNameMon'],
                                                 'tbltNameInter':jdata['tbltNameInter'],
                                                 'tbltSizeUnit':jdata['tbltSizeUnit'],
                                                 'tbltSizeMixture':jdata['tbltSizeMixture'],
                                                 'tbltManufacture':jdata['tbltManufacture'],
                                                 'tbltCountryId':jdata['tbltCountryId'],
                                                 'tbltBarCode':jdata['tbltBarCode'],
                                                 'tbltLifeDate':jdata['tbltLifeDate'],
                                                 'tbltIsDiscount':str(jdata['tbltIsDiscount']),
                                                 'status':str(jdata['status']),
                                                 'discount_per':jdata['tbltDiscountPerc'],
                                                 'tbltPackingCnt':jdata['tbltPackingCnt'],
                                                 'max_price':jdata['tbltMaxPrice'],
                                                 'discount_price':jdata['tbltDiscountAmt'],
                                                 'discounted_price':jdata['tbltUnitDisAmt'],
                                                 'tbltGroup':str(jdata['tbltGroup']),
                                                 'packGroup':str(jdata['packGroup']),
                                            })
                else:
                    med_data_line_id.write({'packGroup':str(jdata['packGroup'])})
        
        return True
    
    
class EmdPharmacyConfigLine(models.Model):
    _name = 'emd.pharmacy.config.line'
    
    state = fields.Selection([('not_send',u'Илгээгдээгүй'),
                                    ('done',u'Илгээгдсэн'),
                                  ('return',u'Буцаагдсан')],u'Төлөв',default='not_send')
    config_id = fields.Many2one('emd.pharmacy.config','Config',readonly=True)
    pos_rno = fields.Char('Pos Rno')
    tbltId = fields.Char('TbltId')
    med_data = fields.Text('Med data')
    access_token = fields.Char('Access token')
    receiptId = fields.Integer(u'Жор тодорхойлох дугаар')
    pos_id = fields.Many2one('pos.order','Борлуулалтын дугаар')
    date = fields.Datetime('Date',readonly=True)
    name = fields.Char('First name',readonly=True)
    last_name = fields.Char('Last name',readonly=True)
    register_number = fields.Char('Register number',readonly=True)
    emd_number = fields.Char('Health number',readonly=True)
    phone_number = fields.Char(u'Утасны дугаар',readonly=True)
    ndsh_amount = fields.Float(u'НДШ дүн')
    ndd_number = fields.Char('Social insurance number',readonly=True)
    address = fields.Char('Home address',readonly=True)
    prescription = fields.Char('Prescription number',readonly=True)
    doctor_number = fields.Char('Doctor',readonly=True)
    eb = fields.Char('Eb',readonly=True)
    medicine_name = fields.Char('Medicine name',readonly=True)
    total_price = fields.Float(string='Нийт үнэ',readonly=True)
    company_id = fields.Many2one('res.company','Company',readonly=True)
    created_user = fields.Many2one('res.users','Created user',readonly=True)
    origin = fields.Char('Origin',readonly=True)

    
    
    
    