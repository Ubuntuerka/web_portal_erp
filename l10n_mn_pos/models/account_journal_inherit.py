# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import timedelta
from functools import partial

import psycopg2
import pytz

from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
from odoo.http import request
import odoo.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class AccountJournal(models.Model):
    _inherit = 'account.journal'
    
    is_pos_terminal = fields.Boolean(string='Is pos terminal')
    bank_id = fields.Char('Bank id')
    bank_pos_terminal_id = fields.Char(string='Bank pos terminal id')
    approve_code = fields.Char(string='Approve code')
    
    