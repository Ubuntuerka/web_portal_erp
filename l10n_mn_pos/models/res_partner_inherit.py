# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import timedelta
from functools import partial

import psycopg2
import pytz

from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
from odoo.http import request
import odoo.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class PartnerType(models.Model):
    _name = 'partner.type'
    
    company_id = fields.Many2one('res.company',string='Company')
    name = fields.Char(string='Name')
    

class VacineRegister(models.Model):
    _name = 'vacine.register'

    company_id = fields.Many2one('res.company',string='Company')
    name = fields.Char(string='Vacine name')
    
    
class Partner(models.Model):
    _inherit = 'res.partner'
    
    first_vacine_date = fields.Date(string='First vacine date')
    two_vacine_date = fields.Date(string='Two vacine date')
    vacine_id = fields.Many2one('vacine.register',string='Vacine')
    lastname = fields.Char(string='Last name')
    age = fields.Integer(string='Age')
    sex = fields.Selection([('male','Male'),
                            ('female','Female')],string='Sex')
    registernumber = fields.Char(string='Register number')
    partner_type_id = fields.Many2one('partner.type',string='Partner type')
    vat = fields.Char(string='Tax ID', index=True, help="The Tax Identification Number. Complete it if the contact is subjected to government taxes. Used in some legal statements.")

    
    _sql_constraints = [
        ('unique_vat', 'unique(vat)', 'This vat is already assigned to another contact. Please make sure you assign a unique vat to this contact.'),
        ('unique_registernumber', 'unique(registernumber)', 'This registernumber is already assigned to another contact. Please make sure you assign a unique registernumber to this contact.'),
    ]
    
    
    @api.model
    def create_from_ui(self, partner):
        """ create or modify a partner from the point of sale ui.
            partner contains the partner's fields. """
        # image is a dataurl, get the data after the comma
        if partner.get('image_1920'):
            partner['image_1920'] = partner['image_1920'].split(',')[1]
        partner_id = partner.pop('id', False)
        if partner_id:  # Modifying existing partner
            if partner['registernumber']!=False:
                if len(str(partner['registernumber']))==10:
                    number = partner['registernumber'][2:]
                    useg = partner['registernumber'][:2]
                    numbers = ["1","2","3","4","5","6","7","8","9","0"]
                    try:
                        if type(int(number))!=int:
                            raise UserError(u"Сүүлийн 8 орон тоо биш байгаа тул шалгана уу.")
                        elif useg[0] in numbers and useg[1] in numbers and useg[2] in numbers:
                            raise UserError(u"Эхний 2 орон үсэг биш байгаа тул шалгана уу.")
                        else:
                            part = self.search([('registernumber','=',str(partner['registernumber']))])
                            #if not part:
                            self.browse(partner_id).write(partner)
                            #else:
                            #    raise UserError(u"Регистрийн дугаартай харилцагч бүртгэгдсэн байна.")
                                
                    except ValueError:
                        raise UserError(u"Таны оруулсан регистрийн дугаар зөв байгаа эсэхийг шалгана уу.")
                else:
                    raise UserError(u"10 тэмдэгтээс их болон бага байж болохгүй.")
            else:
                raise UserError(u"Таны оруулсан регистрийн дугаар зөв байгаа эсэхийг шалгана уу.")
                    
            
        else:
            partner['lang'] = self.env.user.lang
            if partner.get('registernumber'):
                if len(str( partner.get('registernumber')))==10:
                    number = partner.get('registernumber')[2:]
                    useg = partner.get('registernumber')[:2]
                    numbers = ["1","2","3","4","5","6","7","8","9","0"]
                    try:
                        if type(int(number))!=int:
                            raise UserError(u"Сүүлийн 8 орон тоо биш байгаа тул шалгана уу.")
                        elif useg[0] in numbers and useg[1] in numbers:
                            raise UserError(u"Эхний 2 орон үсэг биш байгаа тул шалгана уу.")
                        else:
                            part = self.search([('registernumber','=',str(partner.get('registernumber')))])
                            if not part:
                                partner_id = self.create(partner).id
                            else:
                                raise UserError(u"Регистрийн дугаартай харилцагч бүртгэгдсэн байна.")
                    except ValueError:
                        raise UserError(u"Таны оруулсан регистрийн дугаар зөв байгаа эсэхийг шалгана уу.")
                else:
                    raise UserError(u"10 тэмдэгтээс их болон бага байж болохгүй.")
            else:
                raise UserError(u"Таны оруулсан регистрийн дугаар зөв байгаа эсэхийг шалгана уу.")
            
        return partner_id
    
    
    
    
    
    
    
    
    
    
    
    
    