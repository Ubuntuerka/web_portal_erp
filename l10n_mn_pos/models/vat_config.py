# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import timedelta
from functools import partial
import json, requests
import psycopg2
import pytz
#import vatbridge
from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
from odoo.http import request
import odoo.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class VatInformation(models.Model):
    _name = 'vat.information'
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env.user.company_id)
    name = fields.Char(string='Vat config name')
    state = fields.Selection([('send_data','Send data'),
                              ],string='State')
    container_id = fields.Char(string='Container ID')
    container_status = fields.Selection([('running','Running'),
                                         ('stop','Stop')],string='Container status')
    reg_user_id = fields.Many2one('res.users','Reg users',default=lambda self: self.env.user.id)
    date = fields.Date(string='Register date')
    update_date = fields.Date(string='Update date')
    desc = fields.Text(string='Description')
    get_info_url = fields.Char('Get info url')
    get_check_url = fields.Char('Get check url')
    get_send_url = fields.Char('Get send url')
    put_data_url = fields.Char('Put data url')
    put_return_url = fields.Char('Put return url')
    vat_data_line_id = fields.One2many('vat.data','vat_id',string='Vat data',domain=[('is_active','=',False)])
    
    def _auto_send_vat(self):
        vat_ids = self.search([])
        for lines in vat_ids: 
            lines.action_sendData()
    
    def action_checkAPI(self):
        data = requests.get(self.get_check_url)
        ndata = data.json()
        return self.write({'desc':ndata})    
    
    
    
    def action_sendData(self):
        vat_data_obj = self.env['vat.data']
        vat_ids = self.search([('reg_user_id','=',self._uid)])
        obj = []
        if vat_ids:
            for vat in vat_ids:
                dat = self.browse(vat.id)
                obj.append(dat)
                
        else:
            dat = self.browse(self.id)
            obj.append(dat)
        for lines in obj:
            data = requests.get(lines.get_send_url)
            if data.status_code == 200:
                new_data = data.json()
                if new_data['success'] == True:
                    if lines.vat_data_line_id !=False:
                        for line in lines.vat_data_line_id:
                            if line.state == 'not_send':
                                line.write({
                                            'state':'send',
                                            'is_active':True,
                                            
                                            })
                    lines.write({'desc':new_data})
                else:
                    raise UserError(_(u'%s'%new_data))
            else:
                raise UserError(_(u'Алдаа гарсан тул тохиргоогоо шалгана уу'))
    
    
    def action_getinfo(self):
        datainfo = requests.get(self.get_info_url)
        new_data_info = datainfo.json()
        if new_data_info['extraInfo']:
            if int(new_data_info['extraInfo']['countBill']) >= 19990:
                raise Warning(_(u'Баримтын дугаар дуусч байгаа тул илгээх товч дээр дарна уу'))
            elif int(new_data_info['extraInfo']['countLottery']) <= 100:
                raise Warning(_(u'Баримтын дугаар дуусч байгаа тул илгээх товч дээр дарна уу'))
        return self.write({'desc':new_data_info})    
    
    
class VatData(models.Model):
    _name = 'vat.data'
    _order = 'reg_date desc'
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env.user.company_id)
    pos_order_name = fields.Char(string='Pos order name')
    is_active = fields.Boolean(string='Active')
    state = fields.Selection([('not_send','Not send'),
                              ('send','Send'),
                              ('refund','Refund')],u'Төлөв')
    vat_id = fields.Many2one('vat.information',string='Vat info')
    registerNo = fields.Char(string='Register number')
    billId = fields.Char(string='Bill Id')
    reg_date = fields.Datetime(string='Date')
    internalCode = fields.Char(string='Internal code')
    lotteryWarningMsg = fields.Char(string='lotteryWarningMsg')
    errorCode = fields.Char(string='Адааны мэдээлэл')
    message = fields.Char(string='message')
    amount = fields.Float(string='Amount')
    vat = fields.Float(string='Vat')
    cashAmount = fields.Float(string='Cash amount')
    nonCashAmount = fields.Float(string='None cash amount')
    cityTax = fields.Float(string='City tax')
    districtCode = fields.Float(string='District code')
    posNo = fields.Char(string='Pos number')
    billIdSuffix = fields.Char(string='Bill id suffix')
    returnBillId = fields.Integer(string='Return Bill Id')
    taxType = fields.Integer(string='Tax Type')
    invoiceId = fields.Integer(string='Invoice Id')
    reportMonth = fields.Char(string='Өмнөх сар')
    branchNo = fields.Integer(string='Branch number')
    billType = fields.Char(string='Bill type')
    customerNo = fields.Char(string='Customer number')
    line_id = fields.One2many('vat.data.line','vat_data_id',string='Vat data line')
    no_lottery_note = fields.Text(string='Сугалааны дугаар хэвлээгүй тэмдэглэл')
    no_lottery = fields.Boolean(string='Сугалааны дугаар хэвлээгүй эсэх')
    info = fields.Text(string='Send info')
    
    
    def action_refund(self):
        obj = self.browse(self.ids)
        bill_id = '%s'%obj.billId
        bill_date = '%s'%obj.reg_date
        return_data = {
                        'returnBillId':"%s"%bill_id,
                        'date':"%s"%bill_date,
                        }
        header = {
                'Content-type':'application/json; application/x-www-form-urlencoded',
            }
        s_data = json.dumps(return_data)
        l_data = {
                'param':'%s'%s_data,
            }
        data = requests.post(url=obj.vat_id.put_return_url,data=l_data)
        rec_data = data.json()
        if rec_data != None:
            if rec_data['success'] == True:
                self.write({
                                    'info':rec_data,
                                    'state':'refund',
                                    })
            else:
                raise UserError(_(u'%s'%rec_data['message']))
        else:
            raise UserError(_(u'Та татвар луу мэдээ илгээгүй байгаа тул буцаах боломжгүй'))
        
        return True
    
    
class VatDataLine(models.Model):
    _name = 'vat.data.line'
    
    company_id = fields.Many2one('res.company',string='Company',default=lambda self: self.env.user.company_id)
    vat_data_id = fields.Many2one('vat.data',string='Vat data')
    code = fields.Char(string='Local code')
    pro_name = fields.Char(string='Product name')
    unit = fields.Char(string='Measure unit')
    qty = fields.Float(string='Quantity')
    unit_price = fields.Float(string='Unit price')
    total_amount = fields.Float(string='Total amount')
    vat_amount = fields.Float(string='Vat amount')
    city_tax = fields.Float(string='City Tax')
    barcode = fields.Char(string='Barcode')
    
    
    