# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import timedelta
from functools import partial
from collections import defaultdict
import psycopg2
import pytz
import json, requests
from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
from odoo.http import request
import odoo.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)


class PosSession(models.Model):
    _inherit = 'pos.session'
    
    
    def _accumulate_amounts(self, data):
        # Accumulate the amounts for each accounting lines group
        # Each dict maps `key` -> `amounts`, where `key` is the group key.
        # E.g. `combine_receivables` is derived from pos.payment records
        # in the self.order_ids with group key of the `payment_method_id`
        # field of the pos.payment record.
        amounts = lambda: {'amount': 0.0, 'amount_converted': 0.0}
        tax_amounts = lambda: {'amount': 0.0, 'amount_converted': 0.0, 'base_amount': 0.0}
        split_receivables = defaultdict(amounts)
        split_receivables_cash = defaultdict(amounts)
        combine_receivables = defaultdict(amounts)
        combine_receivables_cash = defaultdict(amounts)
        invoice_receivables = defaultdict(amounts)
        sales = defaultdict(amounts)
        taxes = defaultdict(tax_amounts)
        stock_expense = defaultdict(amounts)
        stock_output = defaultdict(amounts)
        # Track the receivable lines of the invoiced orders' account moves for reconciliation
        # These receivable lines are reconciled to the corresponding invoice receivable lines
        # of this session's move_id.
        order_account_move_receivable_lines = defaultdict(lambda: self.env['account.move.line'])
        rounded_globally = self.company_id.tax_calculation_rounding_method == 'round_globally'
        for order in self.order_ids:
            # Combine pos receivable lines
            # Separate cash payments for cash reconciliation later.
            for payment in order.payment_ids:
                amount, date = payment.amount, payment.payment_date
                if payment.payment_method_id.split_transactions:
                    if payment.payment_method_id.is_cash_count:
                        split_receivables_cash[payment] = self._update_amounts(split_receivables_cash[payment], {'amount': amount}, date)
                    else:
                        split_receivables[payment] = self._update_amounts(split_receivables[payment], {'amount': amount}, date)
                else:
                    key = payment.payment_method_id
                    if payment.payment_method_id.is_cash_count:
                        combine_receivables_cash[key] = self._update_amounts(combine_receivables_cash[key], {'amount': amount}, date)
                    else:
                        combine_receivables[key] = self._update_amounts(combine_receivables[key], {'amount': amount}, date)

            if order.is_invoiced:
                # Combine invoice receivable lines
                key = order.partner_id.property_account_receivable_id.id
                invoice_receivables[key] = self._update_amounts(invoice_receivables[key], {'amount': order._get_amount_receivable()}, order.date_order)
                # side loop to gather receivable lines by account for reconciliation
                for move_line in order.account_move.line_ids.filtered(lambda aml: aml.account_id.internal_type == 'receivable'):
                    order_account_move_receivable_lines[move_line.account_id.id] |= move_line
            else:
                order_taxes = defaultdict(tax_amounts)
                for order_line in order.lines:
                    line = self._prepare_line(order_line)
                    # Combine sales/refund lines
                    sale_key = (
                        # account
                        line['income_account_id'],
                        # sign
                        -1 if line['amount'] < 0 else 1,
                        # for taxes
                        tuple((tax['id'], tax['account_id'], tax['tax_repartition_line_id']) for tax in line['taxes']),
                    )
                    sales[sale_key] = self._update_amounts(sales[sale_key], {'amount': line['amount']}, line['date_order'])
                    # Combine tax lines
                    for tax in line['taxes']:
                        tax_key = (tax['account_id'], tax['tax_repartition_line_id'], tax['id'], tuple(tax['tag_ids']))
                        order_taxes[tax_key] = self._update_amounts(
                            order_taxes[tax_key],
                            {'amount': tax['amount'], 'base_amount': tax['base']},
                            tax['date_order'],
                            round=not rounded_globally
                        )
                for tax_key, amounts in order_taxes.items():
                    if rounded_globally:
                        amounts = self._round_amounts(amounts)
                    for amount_key, amount in amounts.items():
                        taxes[tax_key][amount_key] += amount

                if self.company_id.anglo_saxon_accounting:
                    # Combine stock lines
                    stock_moves = self.env['stock.move'].search([
                        ('picking_id', '=', order.picking_id.id),
                        ('company_id.anglo_saxon_accounting', '=', True),
                        ('product_id.categ_id.property_valuation', '=', 'real_time')
                    ])
                    for move in stock_moves:
                        exp_key = move.product_id.property_account_expense_id or move.product_id.categ_id.property_account_expense_categ_id
                        out_key = move.product_id.categ_id.property_stock_account_output_categ_id
                        amount = -sum(move.stock_valuation_layer_ids.mapped('value'))
                        #stock_expense[exp_key] = self._update_amounts(stock_expense[exp_key], {'amount': amount}, move.picking_id.date)
                        #stock_output[out_key] = self._update_amounts(stock_output[out_key], {'amount': amount}, move.picking_id.date)

                # Increasing current partner's customer_rank
                order.partner_id._increase_rank('customer_rank')

        MoveLine = self.env['account.move.line'].with_context(check_move_validity=False)

        data.update({
            'taxes':                               taxes,
            'sales':                               sales,
            'stock_expense':                       stock_expense,
            'split_receivables':                   split_receivables,
            'combine_receivables':                 combine_receivables,
            'split_receivables_cash':              split_receivables_cash,
            'combine_receivables_cash':            combine_receivables_cash,
            'invoice_receivables':                 invoice_receivables,
            'stock_output':                        stock_output,
            'order_account_move_receivable_lines': order_account_move_receivable_lines,
            'MoveLine':                            MoveLine
        })
        return data
    
    

class PosConfig(models.Model):
    _inherit = 'pos.config'
    
    district_code = fields.Many2one('res.country.state',string=u'Дүүргийн код',required=True)
    is_vat_sale = fields.Boolean(string=u'НӨАТ баримт гаргах эсэх')
    
    
class pos_order(models.Model):
    _inherit = 'pos.order'
    
    ebarimt = fields.Char(string='Ebarimt')
    qrcode = fields.Char(string='Qrcode')
    cttd = fields.Char(string='CTTD')
    
    
    @api.model
    def _order_fields(self, ui_order):
        process_line = partial(self.env['pos.order.line']._order_line_fields, session_id=ui_order['pos_session_id'])
        return {
            'user_id':      ui_order['user_id'] or False,
            'session_id':   ui_order['pos_session_id'],
            'lines':        [process_line(l) for l in ui_order['lines']] if ui_order['lines'] else False,
            'pos_reference': ui_order['name'],
            'sequence_number': ui_order['sequence_number'],
            'partner_id':   ui_order['partner_id'] or False,
            'date_order':   ui_order['creation_date'].replace('T', ' ')[:19],
            'fiscal_position_id': ui_order['fiscal_position_id'],
            'pricelist_id': ui_order['pricelist_id'],
            'amount_paid':  ui_order['amount_paid'],
            'amount_total':  ui_order['amount_total'],
            'amount_tax':  ui_order['amount_tax'],
            'amount_return':  ui_order['amount_return'],
            'company_id': self.env['pos.session'].browse(ui_order['pos_session_id']).company_id.id,
            'to_invoice': ui_order['to_invoice'] if "to_invoice" in ui_order else False,
        }
    
    @api.model
    def get_company_name(self,number):
        user_obj = self.env['res.users']
        names = u'%s'%number
        name = ''
        url="http://info.ebarimt.mn/rest/merchant/info?regno="+names
        try:
            r = requests.get(url)
            n=r.json()
            deletearg = n['name'].replace("\n","")
            name=deletearg
        except Exception:
            r=' '
            name='' 
        return name
    
    
    @api.model
    def action_put(self, data):
        if not data:
            return False
        product_obj=self.env['product.product']
        journal_obj=self.env['account.journal']
        payment_obj = self.env['pos.payment.method']
        data_obj=self.env['vat.data']
        session_obj=self.env['pos.session']
        vat_obj = self.env['vat.data']
        vat_line_obj = self.env['vat.data.line']
        vat_info_obj = self.env['vat.information']
        session = session_obj.browse(data['pos_session_id'])
        vat_info_id = vat_info_obj.search([('company_id','=',session.config_id.company_id.id)])
        order_name = data['name']
        
        vat_data_ids = vat_obj.search([('pos_order_name','=',order_name)])
        print("aaaaaaaaaaaaa",data)
        order_seq = self.env['ir.sequence'].get('pos.order.seq')
        suff=order_seq
        if len(suff)== 5:
            suff = "0"+suff
        suff=suff[-6:]
        name=''
        number = ''
        type = '1'
        if data["type"]=='company':
            type='3'
            number=data['partner_vat']
            ur="http://info.ebarimt.mn/rest/merchant/info?regno="+number+""
            try:
                r = requests.get(ur)
                n=r.json()
                name=n['name']
            except Exception:
                r=' '
                name=''            
        elif data["type"]=='self':
            type='1'
            number=''
        
        bank_amount=0.00
        cash_amount=0.00
        dcode=24
        taxtype=1
        bankid="05"
        not_lottery=False
        not_lottery_note=''
        payment = payment_obj.browse(data['payment_id'])
        if payment.cash_journal_id.type=='bank':
            bank_amount=data["amount_total"]
        else:
            cash_amount=data["amount_total"]
        
        if session.config_id and session.config_id.district_code:
            dcode=session.config_id.district_code.code
        r = u"{ \"districtCode\": \""+str(dcode)+"\",\
                          \"posNo\": \"0001\",\
                          \"taxType\":\""+str(taxtype)+"\",\
                          \"customerNo\": \""+number+"\",\
                          \"customerName\": \""+name+"\",\
                          \"billType\":\""+str(type)+"\",\
                          \"billIdSuffix\":\""+str(suff)+"\",\
                          \"stocks\":["
        total_vat=0.00
        total_ubtax=0.00
        barcode=""
        vat=1
        for l in data["lines"]:
            product=product_obj.browse(l[2]['product_id'])
            vat_tax=False
            city_tax=False
            if product.barcode:# and not barcode
                barcode=product.barcode

            for tax in product.taxes_id:
                if tax and tax.amount==0.01000:
                    city_tax=True
                if l[2]['discount'] >0.0:
                    price = l[2]["price_unit"]/100.0*l[2]['discount']
                    vat=((l[2]["price_unit"]-price)*l[2]["qty"])/1.1*0.10
                    ubtax=0.00
                    total_vat+=vat
                    price_unit=l[2]["price_unit"]-price
                else:
                    vat=((l[2]["price_unit"])*l[2]["qty"])/1.1*0.10
                    ubtax=0.00
                    total_vat+=vat
                    price_unit=l[2]["price_unit"]
            else:
                vat=0.0
                ubtax=0.00
                total_vat+=vat
                price_unit=l[2]["price_unit"]
            qty=l[2]["qty"]
            if city_tax:
                if l[2]['discount'] >0.0:
                    price = l[2]["price_unit"]/100.0*l[2]['discount']
                    ubtax=(l[2]["price_unit"]-price)*l[2]["qty"]*0.01
                    total_ubtax+=ubtax
                else:
                    ubtax=l[2]["price_unit"]*l[2]["qty"]*0.01
                    total_ubtax+=ubtax
            pname=product.name.replace('"',' ')
            if "{ \"code\"" in r :
                r+=u",{ \"code\": \"321\",\
                              \"name\": \""+pname+"\",\
                              \"measureUnit\": \"ш\",\
                              \"qty\": \"%0.2f\""%round(qty,2)+",\
                              \"unitPrice\": \"%0.2f\""%round(price_unit,2)+",\
                              \"totalAmount\": \"%0.2f\""%round(price_unit*qty,2)+",\
                              \"cityTax\": \"%0.2f\""%ubtax+",\
                              \"vat\": \"%0.2f\""%vat+",\
                              \"barCode\":\""+barcode+"\" }"
            else:
                r+=u"{ \"code\": \"321\",\
                              \"name\": \""+pname+"\",\
                              \"measureUnit\": \"ш\",\
                              \"qty\": \"%0.2f\""%round(qty,2)+",\
                              \"unitPrice\": \"%0.2f\""%round(price_unit,2)+",\
                              \"totalAmount\": \"%0.2f\""%round(price_unit*qty,2)+",\
                              \"cityTax\": \"%0.2f\""%ubtax+",\
                              \"vat\": \"%0.2f\""%vat+",\
                              \"barCode\":\""+barcode+"\" }"
        for s in data["statement_ids"]:
            journal=journal_obj.browse(payment.cash_journal_id.id)
            if journal.type=='bank' and journal.bank_id:
                bankid=journal.bank_id
        r+=u"],\
              \"amount\": \"%0.2f\""%round(data["amount_total"],2)+",\
              \"vat\": \"%0.2f\""%total_vat+",\
              \"cashAmount\": \"%0.2f\""%round(cash_amount,2)+",\
              \"nonCashAmount\": \"%0.2f\""%round(bank_amount,2)+",\
              \"cityTax\": \"%0.2f\""%total_ubtax+",\
              \"bankTransactions\":\
              [{ \"rrn\": \"123456789123\",\
              \"bankId\": \""+bankid+"\",\
              \"acquiringBankId\": \"05\",\
              \"terminalId\": \"aa123456\",\
              \"approvalCode\": \"asdasd\",\
                      \"amount\": \"0.00\"}]}"
        

        y=r.encode('utf-8')
        header = {
                'Content-type':'application/json; application/x-www-form-urlencoded',
            }
        l_data = {
                "param":r,
            }
        s_data = json.loads(r)
        print("aaaaaaaaa",vat_info_id)
        datas=requests.post(url=vat_info_id.put_data_url,data=l_data)
        new_data = datas.json()
        print("aaaaaaaaaaaa",new_data)
        no_lottery = False
        no_lottery_note = ''
        if new_data['success'] == True:                               
            if data["type"]=='self':
                if not new_data['lottery'] or new_data['lottery']=='':
                    no_lottery=True
                    #no_lottery_note=vatbridge.getInformation()
                    #raise osv.except_osv(_(u'Анхааруулга!'), _(u'Та сугалааны мэдээгээ илгээнэ үү сугалаа хэвлэх боломжгүй.'))
            vat_id = vat_obj.create({
                                    'pos_order_name':order_name,
                                    'vat_id':vat_info_id.id,
                                    'reg_date':new_data['date'],
                                    'registerNo':new_data['registerNo'],
                                    'districtCode':new_data['districtCode'],
                                    'posNo':new_data['posNo'],
                                    'customerNo':new_data['customerNo'],
                                    'billType':new_data['billType'],
                                    'billIdSuffix':new_data['billIdSuffix'],
                                    'amount':new_data['amount'],
                                    'vat':new_data['vat'],
                                    'cashAmount':new_data['cashAmount'],
                                    'customerNo':new_data['customerNo'],
                                    'cityTax':new_data['cityTax'],
                                    'nonCashAmount':new_data['nonCashAmount'],
                                    'billId':new_data['billId'],                                    #'internalCode':new_data['internalCode'],
                                    'no_lottery':no_lottery,
                                    'no_lottery_note':no_lottery_note,
                                    'billIdSuffix':new_data['billIdSuffix'],
                                    'state':'not_send',
                                    })
            for line in new_data['stocks']:
                vat_line_obj.create({
                                                'vat_data_id':vat_id.id,
                                                'code':line['code'],
                                                'pro_name':line['name'],
                                                'unit':line['measureUnit'],
                                                'qty':line['qty'],
                                                'unit_price':line['unitPrice'],
                                                'total_amount':line['totalAmount'],
                                                'city_tax':line['cityTax'],
                                                'vat_amount':line['vat'],
                                                'barcode':line['barCode']
                                                })
        else:
            raise UserError( _(u'Баримт алдаа байгаа тул шалгана уу! %s, %s'%(new_data['errorCode'],new_data['message'])))
        print("aaaaaaaa",new_data)
        return new_data
    
    @api.model
    def create_from_ui(self, orders, draft=False):
        """ Create and update Orders from the frontend PoS application.

        Create new orders and update orders that are in draft status. If an order already exists with a status
        diferent from 'draft'it will be discareded, otherwise it will be saved to the database. If saved with
        'draft' status the order can be overwritten later by this function.

        :param orders: dictionary with the orders to be created.
        :type orders: dict.
        :param draft: Indicate if the orders are ment to be finalised or temporarily saved.
        :type draft: bool.
        :Returns: list -- list of db-ids for the created and updated orders.
        """
        order_ids = []
        for order in orders:
            existing_order = False
            if 'server_id' in order['data']:
                existing_order = self.env['pos.order'].search(['|', ('id', '=', order['data']['server_id']), ('pos_reference', '=', order['data']['name'])], limit=1)
            if (existing_order and existing_order.state == 'draft') or not existing_order:
                order_ids.append(self._process_order(order, draft, existing_order))

        return self.env['pos.order'].search_read(domain = [('id', 'in', order_ids)], fields = ['id', 'pos_reference'])
    
    def refund(self):
        """Create a copy of order  for refund order"""
        refund_orders = self.env['pos.order']
        vat_data_obj = self.env['vat.data']
        for order in self:
            # When a refund is performed, we are creating it in a session having the same config as the original
            # order. It can be the same session, or if it has been closed the new one that has been opened.
            
            current_session = order.session_id.config_id.current_session_id
            if not current_session:
                raise UserError(_('To return product(s), you need to open a session in the POS %s') % order.session_id.config_id.display_name)
            else:
                vat = vat_data_obj.search([('pos_order_name','=',order.pos_reference)])
                if vat.id !=False:
                    vat.action_refund()
            refund_order = order.copy({
                'name': order.name + _(' REFUND'),
                'session_id': current_session.id,
                'date_order': fields.Datetime.now(),
                'pos_reference': order.pos_reference,
                'lines': False,
                'amount_tax': -order.amount_tax,
                'amount_total': -order.amount_total,
                'amount_paid': 0,
            })
            for line in order.lines:
                PosOrderLineLot = self.env['pos.pack.operation.lot']
                for pack_lot in line.pack_lot_ids:
                    PosOrderLineLot += pack_lot.copy()
                line.copy({
                    'name': line.name + _(' REFUND'),
                    'qty': -line.qty,
                    'order_id': refund_order.id,
                    'price_subtotal': -line.price_subtotal,
                    'price_subtotal_incl': -line.price_subtotal_incl,
                    'pack_lot_ids': PosOrderLineLot,
                    })
            refund_orders |= refund_order

        return {
            'name': _('Return Products'),
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id': refund_orders.ids[0],
            'view_id': False,
            'context': self.env.context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }
    
    
    def create_picking(self):
        """Create a picking for each order and validate it."""
        Picking = self.env['stock.picking']
        # If no email is set on the user, the picking creation and validation will fail be cause of
        # the 'Unable to log message, please configure the sender's email address.' error.
        # We disable the tracking in this case.
        if not self.env.user.partner_id.email:
            Picking = Picking.with_context(tracking_disable=True)
        Move = self.env['stock.move']
        StockWarehouse = self.env['stock.warehouse']
        for order in self:
            if not order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu']):
                continue
            address = order.partner_id.address_get(['delivery']) or {}
            picking_type = order.picking_type_id
            return_pick_type = order.picking_type_id.return_picking_type_id or order.picking_type_id
            order_picking = Picking
            return_picking = Picking
            moves = Move
            location_id = picking_type.default_location_src_id.id
            if order.partner_id:
                destination_id = order.partner_id.property_stock_customer.id
            else:
                if (not picking_type) or (not picking_type.default_location_dest_id):
                    customerloc, supplierloc = StockWarehouse._get_partner_locations()
                    destination_id = customerloc.id
                else:
                    destination_id = picking_type.default_location_dest_id.id

            if picking_type:
                message = _("This transfer has been created from the point of sale session: <a href=# data-oe-model=pos.order data-oe-id=%d>%s</a>") % (order.id, order.name)
                picking_vals = {
                    'origin': order.name,
                    'partner_id': address.get('delivery', False),
                    'user_id': False,
                    'date_done': order.date_order,
                    'picking_type_id': picking_type.id,
                    'company_id': order.company_id.id,
                    'move_type': 'direct',
                    'note': order.note or "",
                    'location_id': location_id,
                    'location_dest_id': destination_id,
                }
                pos_qty = any([x.qty > 0 for x in order.lines if x.product_id.type in ['product', 'consu']])
                if pos_qty:
                    order_picking = Picking.create(picking_vals.copy())
                    if self.env.user.partner_id.email:
                        order_picking.message_post(body=message)
                    else:
                        order_picking.sudo().message_post(body=message)
                neg_qty = any([x.qty < 0 for x in order.lines if x.product_id.type in ['product', 'consu']])
                if neg_qty:
                    return_vals = picking_vals.copy()
                    return_vals.update({
                        'location_id': destination_id,
                        'location_dest_id': return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id,
                        'picking_type_id': return_pick_type.id
                    })
                    return_picking = Picking.create(return_vals)
                    if self.env.user.partner_id.email:
                        return_picking.message_post(body=message)
                    else:
                        return_picking.message_post(body=message)

            for line in order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu'] and not float_is_zero(l.qty, precision_rounding=l.product_id.uom_id.rounding)):
                moves |= Move.create({
                    'name': line.name,
                    'product_uom': line.product_id.uom_id.id,
                    'picking_id': order_picking.id if line.qty >= 0 else return_picking.id,
                    'picking_type_id': picking_type.id if line.qty >= 0 else return_pick_type.id,
                    'product_id': line.product_id.id,
                    'price_unit':line.product_id.standard_price,
                    'product_uom_qty': abs(line.qty),
                    'state': 'draft',
                    'location_id': location_id if line.qty >= 0 else destination_id,
                    'location_dest_id': destination_id if line.qty >= 0 else return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id,
                })

            # prefer associating the regular order picking, not the return
            order.write({'picking_id': order_picking.id or return_picking.id})

            if return_picking:
                order._force_picking_done(return_picking)
            if order_picking:
                order._force_picking_done(order_picking)

            # when the pos.config has no picking_type_id set only the moves will be created
            if moves and not return_picking and not order_picking:
                moves._action_assign()
                moves.filtered(lambda m: m.product_id.tracking == 'none')._action_done()

        return True
