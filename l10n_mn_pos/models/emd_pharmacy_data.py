# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import timedelta
from functools import partial

import psycopg2
import pytz

from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
from odoo.http import request
import odoo.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

    
class EmdPharmacyDataProduct(models.Model):
    _name = 'emd.pharmacy.data.product'
    
    company_id = fields.Many2one('res.company','Company')
    product_ids = fields.Many2many('product.product','product_medicine_regsiter_rel','product_id','discount_id', 'Product', required=False)
    tbltSizeUnit = fields.Integer(u'Эмийн тоо')
    tbltSize = fields.Integer(u'Эмийн тун')
    dataId = fields.Integer(u'Бичлэг хянах дугаар')
    max_price = fields.Float('Maximum price')
    packGroup = fields.Char(u'Эмийн бүлэглэлт')
    tbltIsDiscount = fields.Boolean(u'Хөнгөлөлттэй эсэх')
    tbltLifeDate = fields.Date(u'Хүчинтэй хугацаа')
    tbltBarCode = fields.Char(u'Зураасан код')
    tbltManufacture = fields.Char(u'Эмийг үйлдвэрлэгч')
    tbltSizeMixture = fields.Char(u'Эмийн хэмжээ')
    tbltNameMon = fields.Char(u'Эмийн монгол нэршил')
    tbltNameInter = fields.Char(u'Эмийн ОУ-ын нэршил')
    tbltNameSales = fields.Char(u'Эмийн худалдааны нэршил')
    tbltCountryId = fields.Integer(u'Үйлдвэрлэгч улс')
    tbltPackingCnt = fields.Char(u'Савлалтын тоо')
    discount_per = fields.Float(u'Хөнгөлөлт хувь')
    discount_price = fields.Float('Discount price')
    discounted_price = fields.Float('Хөнгөлөгдсөн дүн')
    parent_id = fields.Many2one('emd.pharmacy.config','Parent')
    tbltType = fields.Selection([('0',u'Хүүхэд'),
                                ('1',u'Насанд хүрсэн хүн'),
                                ('2',u'Шахмал'),
                                 ('3',u'Капсул'),
                                 ('4',u'Лаа'),
                                 ('5',u'Крем'),
                                 ('6',u'Сироп'),
                                 ('7',u'Гель'),
                                 ('8',u'Тосон түрхлэг'),
                                 ('9',u'Цацлага'),
                                 ('10',u'энгийн эм хүүхэд'),
                                 ('11',u'энгийн эм том хүн'),
                                 ('12',u'Уусмал'),
                                 ('13',u'Ретард шахмал'),
                                 ('14',u'Бүрхүүлтэй шахмал'),
                                 ('15',u'Зажилдаг шахмал'),
                                 ('16',u'Хөвмөл'),
                                 ('17',u'Уусдаг шахмал'),
                                 ('18',u'Хандмал'),
                                 ('19',u'Аэрозоль'),
                                 ('20',u'B хүүхэд'),
                                 ('21',u'B том хүн'),
                                 ('30',u'C хүүхэд'),
                                 ('31',u'C том хүн'),
                                 ],u'Төрөл')
    tbltIsDiscount = fields.Selection([('1',u'Тийм'),
                                       ('2',u'Үгүй')],u'Хөнгөлөлттэй эсэх')
    status = fields.Selection([('1',u'Идэвхитэй'),
                               ('0',u'Идэвхигүй'),
                               ('2',u'Шинэ эм'),
                               ('3',u'Шинэ')],u'Төлөв')
    tbltRegCode = fields.Char('tbltRegCode')
    tbltGroup = fields.Selection([('1',u'Эм'),
                                  ('2',u'Эмчилгээ'),
                                  ('3',u'Шинжилгээ')
                                  ],string=u'Бүлэг')
    tbltGroupName = fields.Char('tbltGroup name')
                
    
class EmdPharmacySale(models.Model):
    _name = "emd.pharmacy.sale"

    
    state = fields.Selection([('not_send',u'Илгээгдээгүй'),
                               ('done',u'Илгээгдсэн'),
                               ('return',u'Буцаагдсан')],u'Төлөв')
    config_ids =  fields.Many2one('emd.pharmacy.config','Config')
    pos_rno =  fields.Char('Pos Rno')
    tbltId =  fields.Char('TbltId')
    med_data =  fields.Text('Med data')
    access_token =  fields.Char('Access token')
    receiptId =  fields.Integer(u'Жор тодорхойлох дугаар')
    pos_id =  fields.Many2one('pos.order','Борлуулалтын дугаар')
    date =  fields.Datetime('Date',readonly=True)
    name =  fields.Char('First name',readonly=True)
    last_name =  fields.Char('Last name',readonly=True)
    register_number =  fields.Char('Register number',readonly=True)
    emd_number =  fields.Char('Health number',readonly=True)
    phone_number =  fields.Char(u'Утасны дугаар',readonly=True)
    ndsh_amount =  fields.Float(u'НДШ дүн')
    ndd_number =  fields.Char('Social insurance number',readonly=True)
    address =  fields.Char('Home address',readonly=True)
    prescription =  fields.Char('Prescription number',readonly=True)
    doctor_number =  fields.Char('Doctor',readonly=True),
    eb =  fields.Char('Eb',readonly=True)
    medicine_name =  fields.Char('Medicine name',readonly=True)
    total_price =  fields.Float(string='Нийт үнэ',readonly=True)
    company_id =  fields.Many2one('res.company','Company',readonly=True)
    created_user =  fields.Many2one('res.users','Created user',readonly=True)
    origin =  fields.Char('Origin',readonly=True)
    line_ids =  fields.One2many('emd.pharmacy.sale.product','parent_id','Discount product list')
               
                
        
class EmdPharmacySaleProduct(models.Model):
    _name = 'emd.pharmacy.sale.product'
    
    company_id = fields.Many2one('res.company','Company')
    product =  fields.Many2one('product.product','Product')
    qty =  fields.Float('Quantity')
    price =  fields.Float('Unit Price')
    total =  fields.Float('Total')
    discount =  fields.Float('Discount')
    packGroup = fields.Char(u'Эмийн бүлэглэлт')
    date =  fields.Datetime('Date')
    parent_id = fields.Many2one('discount.medicine.list','Parent')
    tbltId = fields.Char('TbltId')
    detailId = fields.Char('DetailId')
    tbltSize = fields.Integer(u'Эмийн тун')
    tbltType = fields.Selection([('0',u'Хүүхэд'),
                                 ('1',u'Насанд хүрсэн хүн'),
                                 ('2',u'Шахмал'),
                                 ('3',u'Капсул'),
                                 ('4',u'Лаа'),
                                 ('5',u'Крем'),
                                 ('6',u'Сироп'),
                                 ('7',u'Гель'),
                                 ('8',u'Тосон түрхлэг'),
                                 ('9',u'Цацлага'),
                                 ('10',u'энгийн эм хүүхэд'),
                                 ('11',u'энгийн эм том хүн'),
                                 ('12',u'Уусмал'),
                                 ('13',u'Ретард шахмал'),
                                 ('14',u'Бүрхүүлтэй шахмал'),
                                 ('15',u'Зажилдаг шахмал'),
                                 ('16',u'Хөвмөл'),
                                 ('17',u'Уусдаг шахмал'),
                                 ('18',u'Хандмал'),
                                 ('19',u'Аэрозоль'),
                                 ('20',u'B хүүхэд'),
                                 ('21',u'B том хүн'),
                                 ('30',u'C хүүхэд'),
                                 ('31',u'C том хүн'),
                                 ],u'Төрөл')
    tbltIsDiscount = fields.Selection([('1',u'Тийм'),
                                       ('2',u'Үгүй')],u'Хөнгөлөлттэй эсэх')
    status = fields.Selection([('1',u'Идэвхитэй'),
                               ('0',u'Идэвхигүй'),
                               ('2',u'Шинэ эм'),
                               ('3',u'Шинэ')],u'Төлөв')
    tbltGroup = fields.Selection([('1',u'Эм'),
                                  ('2',u'Эмчилгээ'),
                                  ('3',u'Шинжилгээ')
                                  ],u'Бүлэг')
                