odoo.define('l10n_mn_pos.models_inherit', function (require) {
"use strict";

var ajax = require('web.ajax');
var BarcodeParser = require('barcodes.BarcodeParser');
var BarcodeReader = require('point_of_sale.BarcodeReader');
var PosPopups = require('point_of_sale.popups');
var PosDB = require('point_of_sale.DB');
var devices = require('point_of_sale.devices');
var concurrency = require('web.concurrency');
var config = require('web.config');
var core = require('web.core');
var field_utils = require('web.field_utils');

var rpc = require('web.rpc');
var session = require('web.session');
var time = require('web.time');
var utils = require('web.utils');
var pos_model = require('point_of_sale.models');
var models = pos_model.PosModel.prototype.models;
var PosModelSuper = pos_model.PosModel;
var OrderSuper = pos_model.Order;
var QWeb = core.qweb;
var _t = core._t;
var Mutex = concurrency.Mutex;
var round_di = utils.round_decimals;
var round_pr = utils.round_precision;
var gui = require('point_of_sale.gui');
var exports = {};


var PackLotLinePopupWidget = PosPopups.extend({
    template: 'PackLotLinePopupWidget',
    events: _.extend({}, PosPopups.prototype.events, {
        'click .remove-lot': 'remove_lot',
        'keydown': 'add_lot',
        'blur .packlot-line-input': 'lose_input_focus'
    }),

    show: function(options){
    	this._super(options);
        
        this.focus();
    	var lot_data = this.options.order.pos.lot_datas;
    	var product = this.options.order_line.product.id;
    	var lot_names = [];
    	var select = document.getElementById('lot_data_name');
    	
    	for (var i = 0; i < lot_data.length; i++) {     		
    		if (lot_data[i].product_id[0]==product && lot_data[i].product_qty > 0.0){
    			var opt = document.createElement('option');
    		    opt.value = lot_data[i].name;
    		    opt.innerHTML = lot_data[i].name+" тоо "+lot_data[i].product_qty;
    		    select.appendChild(opt);
    		}
    		
    	}
    	
        
    },

    click_confirm: function(ev){
    	var lot = document.getElementById("lot_data_name").value;
        var pack_lot_lines = this.options.pack_lot_lines;    
        var cid = pack_lot_lines.models[0].cid;
        var lot_name = lot;
        var pack_line = pack_lot_lines.get({cid:cid});
        pack_line.set_lot_name(lot);
        var lot_model = pack_lot_lines.get({cid:cid});
        lot_model.set_lot_name(lot_name);  
        pack_lot_lines.remove_empty_model();
        pack_lot_lines.set_quantity_by_lot();
        this.options.order.save_to_db();
        this.options.order_line.trigger('change', this.options.order_line);
        this.gui.close_popup();
    },

    add_lot: function(ev) {
        if (ev.keyCode === $.ui.keyCode.ENTER && this.options.order_line.product.tracking == 'serial'){
            var pack_lot_lines = this.options.pack_lot_lines,
                $input = $(ev.target),
                cid = $input.attr('cid'),
                lot_name = $input.val();

            var lot_model = pack_lot_lines.get({cid: cid});
            lot_model.set_lot_name(lot_name);  // First set current model then add new one
            if(!pack_lot_lines.get_empty_model()){
                var new_lot_model = lot_model.add();
                this.focus_model = new_lot_model;
            }
            pack_lot_lines.set_quantity_by_lot();
            this.renderElement();
            this.focus();
        }
    },

    remove_lot: function(ev){
        var pack_lot_lines = this.options.pack_lot_lines,
            $input = $(ev.target).prev(),
            cid = $input.attr('cid');
        var lot_model = pack_lot_lines.get({cid: cid});
        lot_model.remove();
        pack_lot_lines.set_quantity_by_lot();
        this.renderElement();
    },

    lose_input_focus: function(ev){
        var $input = $(ev.target),
            cid = $input.attr('cid');
        var lot_model = this.options.pack_lot_lines.get({cid: cid});
        lot_model.set_lot_name($input.val());
    },

    focus: function(){
        this.$("input[autofocus]").focus();
        this.focus_model = false;   // after focus clear focus_model on widget
    }
});
gui.define_popup({name:'packlotline', widget:PackLotLinePopupWidget});





models.push(
		{
            model:  'stock.production.lot',
            label: 'load_lot',
            fields: ['name','product_id','product_qty'],
            loaded: function(self,lot_data){
                self.lot_datas = lot_data;
            },
        }

        );
        


pos_model.PosModel = pos_model.PosModel.extend({
    initialize: function(session, attributes) {
        PosModelSuper.prototype.initialize.call(this, session, attributes)
        var  self = this;
        this.partner_vat = null;
        this.tax_type = 'self';
        this.note = '';
        this.lot_datas = [];
        this.ebarimt = '';
        this.qrcode = '';
        this.cttd = '';

    },
    
    
    push_order: function (order, opts) {
        opts = opts || {};
        var self = this;

        if (order) {
            this.db.add_order(order.export_as_JSON());
        }
        return new Promise(function (resolve, reject) {
            self.flush_mutex.exec(function () {
                var flushed = self._flush_orders(self.db.get_orders(), opts);
				
                flushed.then(resolve, reject);

                return flushed;
            });
        });
    },
    
    import_orders: function(str) {
        var json = JSON.parse(str);
        var report = {
            // Number of paid orders that were imported
            paid: 0,
            // Number of unpaid orders that were imported
            unpaid: 0,
            // Orders that were not imported because they already exist (uid conflict)
            unpaid_skipped_existing: 0,
            // Orders that were not imported because they belong to another session
            unpaid_skipped_session:  0,
            // The list of session ids to which skipped orders belong.
            unpaid_skipped_sessions: [],
        };

        if (json.paid_orders) {
            for (var i = 0; i < json.paid_orders.length; i++) {
                this.db.add_order(json.paid_orders[i].data);
            }
            report.paid = json.paid_orders.length;
            //this.push_order();
        }

        if (json.unpaid_orders) {

            var orders  = [];
            var existing = this.get_order_list();
            var existing_uids = {};
            var skipped_sessions = {};

            for (var i = 0; i < existing.length; i++) {
                existing_uids[existing[i].uid] = true;
            }

            for (var i = 0; i < json.unpaid_orders.length; i++) {
                var order = json.unpaid_orders[i];
                if (order.pos_session_id !== this.pos_session.id) {
                    report.unpaid_skipped_session += 1;
                    skipped_sessions[order.pos_session_id] = true;
                } else if (existing_uids[order.uid]) {
                    report.unpaid_skipped_existing += 1;
                } else {
                    orders.push(new exports.Order({},{
                        pos: this,
                        json: order,
                    }));
                }
            }

            orders = orders.sort(function(a,b){
                return a.sequence_number - b.sequence_number;
            });

            if (orders.length) {
                report.unpaid = orders.length;
                this.get('orders').add(orders);
            }

            report.unpaid_skipped_sessions = _.keys(skipped_sessions);
        }

        return report;
    },
    
});



exports.Packlotline = Backbone.Model.extend({
    defaults: {
        lot_name: null
    },
    initialize: function(attributes, options){
        this.order_line = options.order_line;
        if (options.json) {
            this.init_from_JSON(options.json);
            
            return;
        }
    },
   

    init_from_JSON: function(json) {
        this.order_line = json.order_line;
        this.set_lot_name(json.lot_name);
    },

    set_lot_name: function(name){
        this.set({lot_name : _.str.trim(name) || null});
    },

    get_lot_name: function(){
        return this.get('lot_name');
    },

    export_as_JSON: function(){
        return {
            lot_name: this.get_lot_name(),
        };
    },

    add: function(){
        var order_line = this.order_line,
            index = this.collection.indexOf(this);
        var new_lot_model = new exports.Packlotline({}, {'order_line': this.order_line});
        this.collection.add(new_lot_model, {at: index + 1});
        return new_lot_model;
    },

    remove: function(){
        this.collection.remove(this);
    }
});


var PacklotlineCollection = Backbone.Collection.extend({
    model: exports.Packlotline,
    initialize: function(models, options) {
        this.order_line = options.order_line;
    },

    get_empty_model: function(){
        return this.findWhere({'lot_name': null});
    },

    remove_empty_model: function(){
        this.remove(this.where({'lot_name': null}));
    },

    get_valid_lots: function(){
        return this.filter(function(model){
            return model.get('lot_name');
        });
    },

    set_quantity_by_lot: function() {
        if (this.order_line.product.tracking == 'serial') {
            var valid_lots_quantity = this.get_valid_lots().length;
            if (this.order_line.quantity < 0){
                valid_lots_quantity = -valid_lots_quantity;
            }
            this.order_line.set_quantity(valid_lots_quantity);
        }
    }
});

pos_model.Order = pos_model.Order.extend({
    initialize: function(attributes,options){
    	this.partner_vat = null;
        return OrderSuper.prototype.initialize.call(this, attributes,options);;
    },

    
    myFunction:function() {
        var person = prompt("Татвар төлөгч иргэний 8 оронтой тоог оруулана уу!", "");
        if (person != null) {
        	customer_number = person;
        	return person;
        }else{
        	return true;
        }
    },
    set_note: function (note) {
        this.note = note;
        this.trigger('change', this);
    },
    get_note: function (note) {
        return this.note;
    },   
    
    
     export_as_vat_JSON: function(){
    	var json = OrderSuper.prototype.export_as_JSON.call(this,arguments);
    	var orderLines, paymentLines;
        orderLines = [];
    	var vat_type = this.pos.tax_type;
    	var partner_vat = '';
    	var partner = '';
    	var payment_id = null;
        this.orderlines.each(_.bind( function(item) {
            return orderLines.push([0, 0, item.export_as_JSON()]);
        }, this));
        paymentLines = [];
        this.paymentlines.each(_.bind( function(item) {
        	payment_id = item.payment_method['id'];
            return paymentLines.push([0, 0, item.export_as_JSON()]);
        }, this));
        json.type = vat_type;
        json.note = this.pos.note;
        json.partner_vat = this.pos.partner_vat;
        json.partner_id = this.get_client() ? this.get_client().id : false;
        json.creation_date =  this.validation_date || this.creation_date; // todo: rename creation_date in master
        json.fiscal_position_id = this.fiscal_position ? this.fiscal_position.id : false;
        json.pricelist_id = this.pricelist ? this.pricelist.id : false;
        json.sequence_number = this.sequence_number;
        json.payment_id = payment_id;
        json.ebarimt = this.pos.ebarimt;
        json.cttd = this.pos.cttd;
        json.qrcode = this.pos.qrcode;
    	return json;
    	
    },
    
    export_as_JSON: function(){
    	var json = OrderSuper.prototype.export_as_JSON.call(this,arguments);
    	var orderLines, paymentLines;
        orderLines = [];
    	var partner_vat = '';
    	var partner = '';
    	var payment_id = null;
        this.orderlines.each(_.bind( function(item) {
            return orderLines.push([0, 0, item.export_as_JSON()]);
        }, this));
        paymentLines = [];
        this.paymentlines.each(_.bind( function(item) {
        	payment_id = item.payment_method['id'];
            return paymentLines.push([0, 0, item.export_as_JSON()]);
        }, this));
        json.type = this.pos.tax_type;
        json.note = this.pos.note;
        json.partner_vat = this.pos.partner_vat;
        json.partner_id = this.get_client() ? this.get_client().id : false;
        json.creation_date =  this.validation_date || this.creation_date; // todo: rename creation_date in master
        json.fiscal_position_id = this.fiscal_position ? this.fiscal_position.id : false;
        json.pricelist_id = this.pricelist ? this.pricelist.id : false;
        json.sequence_number = this.sequence_number;
        json.payment_id = payment_id;
        json.ebarimt = this.pos.ebarimt;
        json.cttd = this.pos.cttd;
        json.qrcode = this.pos.qrcode;
    	return json;
    	
    },
});


});