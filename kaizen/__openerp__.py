# -*- coding: utf-8 -*-
##############################################################################
#
#
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name" : "Kaizen",
    "version" : "1.0",
    "author": "Uuganbayar",
    "category": "document",
    "description": """This module document management.""",
    "depends" : [
                 'hr_contract',
                 ],
    'update_xml': [
        'views/kaizen_view.xml',
        'views/kaizen_type_view.xml',
        'views/sequence_view.xml',
        'report/kaizen_report_view.xml',
        'security/kaizen_security_view.xml',
        'views/product_expense_view.xml',
        'views/inventort_income_register_view.xml',
        'security/ir.model.access.csv',
        'views/menu_view.xml',
    ],
    'test': [],
    'installable': True,
    'auto_install': False,
}
