# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo import tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class kaizen_total(models.Model):
    _name = "kaizen_total"
    _auto = False
    _order = 'score desc'

    type_id = fields.Many2one('kaizen_type', 'Ангилал нэр', required=True)
    date = fields.Date('Огноо', required=True)
    score = fields.Integer('Оноо')

    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'kaizen_total')
        self.env.cr.execute("""
        create or replace view kaizen_total as (
        select id, score,type_id,date from kaizen
        where is_kaizen = 'kaizen'
        group by id, type_id, date  order by id desc
        )""")


class kaizen_department(models.Model):
    _name = "kaizen_department"
    _auto = False
    _order = 'score desc'

    department_id = fields.Many2one('hr.department', 'Department',readonly=True)
    date = fields.Date('Огноо', required=True)
    score = fields.Integer('Score',readonly=True)

    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'kaizen_department')
        self.env.cr.execute("""
       create or replace view kaizen_department as (
           select id, sum(score) as score, department_id, date from kaizen
               where score > 0
               group by id, department_id, date
       )""")


class kaizen_user(models.Model):
    _name = "kaizen_user"
    _auto = False
    _order = 'score desc'

    user_id = fields.Many2one('res.users', 'User', readonly=True)
    date = fields.Date('Огноо', required=True)
    score = fields.Integer('Score', readonly=True)
    job_id = fields.Many2one('hr.job', 'Job', readonly=True)

    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'kaizen_user')
        self.env.cr.execute("""
            create or replace view kaizen_user as (
                select  id, date, sum(score) as score,user_id,job_id from kaizen
                    where score > 0
                    group by id, user_id, date, job_id order by score desc    
            )""")
