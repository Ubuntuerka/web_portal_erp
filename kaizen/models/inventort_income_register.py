# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning


class InventortIncomeRegister(models.Model):
    _name = "inventort.income.register"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'name desc'

    company_id = fields.Many2one('res.company', string=u'Компани', default=lambda self: self.env['res.company']._company_default_get('inventort.income.register'),
                                 required=True)
    name = fields.Char(string=u'Нэхэмжлэхийн дугаар')
    reg_user_id = fields.Many2one('res.users', string=u'Бүртгэсэн ажилтан', default=lambda self: self.env.user,
                                      required=True, readonly=True)

    send_date = fields.Date(string=u'Илгээсэн огноо', required=True)
    shts_id = fields.Many2one('shts.register', string=u'ШТС', required=True)
    line_id = fields.One2many('inventort.income.register.line', 'inventort_id', string='Line')
    state = fields.Selection([('draft', u'Ноорог'),
                              ('send', u'Илгээсэн'),
                              ('return', u'Буцаагдсан'),
                              ('confirm', u'Хүлээн авсан')], string=u'Төлөв', default='draft')

    def action_send(self):
        self.write({'state': 'send'})

    def action_return(self):
        for line in self:
            if line.state!='confirm':
                self.write({'state':'return'})
            else:
                raise UserError(u"Батлагдсан тул буцаах боломжгүйг анхаарна уу!")

    def action_draft(self):
        qty_obj = self.env['product.qty']
        for line in self:
            if line.state !='confirm':
                self.write({'state':'draft'})
            else:
                qty_ids = qty_obj.search([('inventort_id','=',line.id)])
                for qt in qty_ids:
                    qt.unlink()
                self.write({'state': 'send'})

    def action_confirm(self):
        qty_obj = self.env['product.qty']

        for income_reg in self:
            for income in income_reg.line_id:
                qty_obj.create({
                    'company_id': income_reg.company_id.id,
                    'shts_id': income_reg.shts_id.id,
                    'reg_date': income_reg.send_date,
                    'product_id': income.product_id.id,
                    'qty': income.receive_qty
                })
            income_reg.write({'state': 'confirm'})

class InventortIncomeRegisterLine(models.Model):
    _name = "inventort.income.register.line"

    inventort_id = fields.Many2one('inventort.income.register', string='Product expense')
    product_id = fields.Many2one('product.product', string='Бүтээгдэхүүн', required=True)
    uom_id = fields.Many2one('uom.uom', string=u'Хэмжих нэгж', required=True)
    send_qty = fields.Float(string=u'Илгээсэн тоо')
    receive_qty = fields.Float(string=u'Хүлээн авсан тоо', required=True)
    diff = fields.Float(string=u'Зөрүү',  store=True) #, compute="_diff_qty"

    # @api.depends('send_qty', 'receive_qty')
    # def _diff_qty(self):
    #     for line in self:
    #         diff = 0.0
    #         if line.send_qty > 0.0 and line.receive_qty >= 0.0:
    #             diff = line.send_qty - line.receive_qty
    #         line.diff = diff

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id


    # @api.depends('send_qty', 'receive_qty')
    # def _diff_qty(self):
    #     for line in self:
    #         diff = 0.0
    #         if line.send_qty > 0.0 and line.receive_qty >= 0.0:
    #             diff = line.send_qty - line.receive_qty
    #         line.diff = diff
