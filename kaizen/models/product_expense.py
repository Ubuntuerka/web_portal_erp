 # -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree
import socket


class ProductExpense(models.Model):

    _name = "product.expense"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    # _description = "Expense"
    # _order = "date desc, id desc"
    # _check_company_auto = True

    name = fields.Char('Тайлбар ') #, readonly=True, required=True, states={'draft': [('readonly', False)], 'reported': [('readonly', False)], 'refused': [('readonly', False)]}
    date = fields.Date( string="Огноо", required=True, default=lambda self: fields.datetime.now()) # readonly=True, states={'draft': [('readonly', False)], 'reported': [('readonly', False)], 'refused': [('readonly', False)]}, default=fields.Date.context_today,
    user_id = fields.Many2one('res.users', 'Ажилтан', readonly=True, default=lambda self: self.env.user, required=True)
    department_id = fields.Many2one('hr.department', string='Хэлтэс ', default=lambda self: self.env.user.dep_id)
    line_ids = fields.One2many('product.expense.line', 'product_expense', string='Product expense')
    attachment_number = fields.Integer('Number of Attachments', compute='_compute_attachment_number')
    shts_id = fields.Many2one('shts.register', string='Гарах агуулах', required=True, domain=[('is_stock', '=', True)])
    company_id = fields.Many2one('res.company', string='Компани'
                                 , default=lambda self: self.env['res.company']._company_default_get('product.expense'),
                                 required=True)
    state = fields.Selection([('draft', u'Ноорог'),
                              ('send', u'Илгээсэн'),
                              ('return', u'Буцаагдсан'),
                              ('confirm', u'Хүлээн авсан')], string=u'Төлөв', default='draft')


    def action_send(self):
        self.write({'state': 'send'})

    def action_return(self):
        self.write({'state': 'draft'})

    def action_confirm(self):
        qty_obj = self.env['product.qty']

        for product_expense in self:
            for expense in product_expense.line_ids:
                # Search for the qty_data records
                qty_data = qty_obj.search([
                    ('shts_id', '=', product_expense.shts_id.id),
                    ('product_id', '=', expense.product_id.id)
                ])

                if not qty_data:
                    raise UserError(_(u"Таны сонгосон бараа үлдэгдэлгүй байгаа тул шалгана уу!"))
                total_qty = sum(qty_data.mapped('qty'))
                if expense.quantity <= total_qty:
                    qty_obj.create({
                        'company_id': product_expense.company_id.id,
                        'shts_id': product_expense.shts_id.id,
                        'reg_date': product_expense.date,
                        'product_id': expense.product_id.id,
                        'qty': expense.quantity * (-1),
                    })
                else:
                    raise UserError(_(u" барааны үлдэгдэл хүрэхгүй байгаа тул шалгана уу!"))
            product_expense.write({'state': 'confirm'})


class ProductExpenseLine(models.Model):
    _name = "product.expense.line"

    product_expense = fields.Many2one('product.expense', string='Product expense')
    product_id = fields.Many2one('product.product', string='Product', domain="[('can_be_expensed', '=', True)]",)
    unit_amount = fields.Float("Unit Price") #, readonly=True, required=True
    quantity = fields.Float("Quantity", default=1)
    total_amount = fields.Float("Total amount", compute='_compute_total_amount', store=True)

    @api.onchange('product_id')
    def onchange_product(self):
        if self.product_id:
            self.unit_amount = self.product_id.lst_price

    @api.depends('unit_amount', 'quantity')
    def _compute_total_amount(self):
        for record in self:
            record.total_amount = record.unit_amount * record.quantity


# class ProductExpenseAccept(models.Model):
#     _name = "product.expense.accept"
#
#     date = fields.Date(string="Огноо", required=True, default=lambda self: fields.datetime.now())
#     user_id = fields.Many2one('res.users', 'Ажилтан', readonly=True, default=lambda self: self.env.user, required=True)
#     department_id = fields.Many2one('hr.department', string='Хэлтэс ', default=lambda self: self.env.user.dep_id)
#     line_ids = fields.One2many('product.expense.accept.line', 'product_expense', string='Product expense accept')
#     attachment_number = fields.Integer('Number of Attachments', compute='_compute_attachment_number')
#     shts_id = fields.Many2one('shts.register', string='Гарах агуулах', required=True, domain=[('is_stock', '=', True)])
#     company_id = fields.Many2one('res.company', string='Компани', default=lambda self: self.env['res.company']._company_default_get('product.expense.accept'), required=True)
#
#
# class ProductExpenseAcceptLine(models.Model):
#     _name = "product.expense.accept.line"
#
#     product_expense_accept = fields.Many2one('product.expense.accept', string='Product expense accept')
#     product_id = fields.Many2one('product.product', string='Product', domain="[('can_be_expensed', '=', True)]",)
#     unit_amount = fields.Float("Unit Price") #, readonly=True, required=True
#     quantity = fields.Float("Quantity", default=1)
#     total_amount = fields.Float("Total amount", compute='_compute_total_amount', store=True)





