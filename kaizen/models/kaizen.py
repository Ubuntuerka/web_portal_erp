# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree

class KaizenType(models.Model):
    _name = 'kaizen_type'
    name = fields.Char('Name',required=True)

class Kaizen(models.Model):
    _name = 'kaizen'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'code desc'

    name = fields.Char('Дэвшүүлж буй санал',required=True)
    code = fields.Char(u'Дугаарлалт',required=True, default=lambda self: self.env['ir.sequence'].next_by_code('kaizen'))
    type_id = fields.Many2one('kaizen_type','Ангилал нэр',required=True)
    date = fields.Date('Огноо',required=True,  default=lambda self: fields.datetime.now())
    department_id = fields.Many2one('hr.department', 'Газар/Нэгж',required=True)
    job_id = fields.Many2one('hr.job', 'Албан тушаал',required=True)
    user_id = fields.Many2one('res.users','Ажилтан', readonly=True, default=lambda self: self.env.user, required=True)
    note1 = fields.Text('Одоогийн нөхцөл байдал:',required=True)
    note2 = fields.Text('Санал хэрэгжсэнээр:',required=True)
    note3 = fields.Text('Хэрэгжүүлэх төлөвлөгөө',required=True)
    is_kaizen = fields.Selection([('draft', 'Дүгнэгдээгүй'),('duplicate','Давхардсан санал'),('kaizen', 'Кайзен'),('notkaizen', 'Кайзен бус')], string='Төлөв', default='draft')
    imp_by = fields.Selection([('self', 'Өөрөө'),('with', 'Чанарын дугуйлантай хамтран'),('quality', 'Чанарын дугуйлан'),('department', 'Холбогдох газар нэгж')], 'Саналыг .... хэрэгжүүлнэ:',required=True)
    score = fields.Integer('Оноо')
    is_quality_circle = fields.Boolean('Чанарын дугуйлан ажиллаж байгаа эсэх')
    note = fields.Text('Саналын хариу')
    report = fields.Binary('Тайлан оруулах')
    department_id1 = fields.Many2one('hr.department', 'Саналын хариу өгөх Газар/Нэгж')
    user_id1 = fields.Many2one('res.users','Саналын хариу өгөх удирдлага',)

    @api.onchange('user_id')
    def onchange_user_id(self):
        if self.user_id:

            employee = self.env['hr.employee'].search([('user_id', '=', self.user_id.id)], limit=1)
            if employee:
                self.department_id = employee.department_id.id
                self.job_id = employee.job_id.id
    



