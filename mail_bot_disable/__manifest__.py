# -*- coding: utf-8 -*-
{
    "name": "Disable Odoobot",
    "version": "1.0.2",
    "author": "Trình Gia Lạc",
    "website": "https://trinhgialac.com",
    "summary": "Disable receive chatbot from OdooBot",
    "description": """
    
        Disable receive chatbot from OdooBot

    """,
    "category": "mail",
    "depends": ["mail_bot"],
    "data": [],
    'images': ['static/description/odoobot.png'],
    "qweb": [],
    "post_init_hook": "_delete_odoobot_channel",
    "installable": True,
}
