# -*- coding: utf-8 -*-
from . import models


from odoo import api, SUPERUSER_ID

#----------------------------------------------------------
# Hooks
#----------------------------------------------------------

def _delete_odoobot_channel(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    channels = env['mail.channel'].search([('name', '=', 'OdooBot'), ('channel_type', '=', 'chat')])
    if channels:
        channels.unlink()
