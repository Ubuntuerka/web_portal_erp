# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError
import datetime
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class ShiftRegisterEmployeeWizard(models.TransientModel):
    _inherit = 'abstract.report.excel'
    _name = 'shift.register.employee.wizard'

    @api.model
    def _shts_id(self):
        shts = []
        shts_obj = self.env['shts.register']
        shts_id = shts_obj.search([('id', '=', self.env.user.warehouse_id.id)])
        if shts_id:
            for line in shts_id:
                shts.append(line.id)
        else:
            for sh in self.env.user.shts_ids:
                shts.append(sh.id)

        if len(shts) == 1:
            return [('id', '=', shts[0])]
        else:
            return [('id', 'in', tuple(shts))]

    company_id = fields.Many2one('res.company', string='Компани',
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'shift.register.employee.wizard'), required=True)
    shts_id = fields.Many2one('shts.register', string='ШТС', domain=_shts_id)
    shts_uud_id = fields.Many2many('shts.register', string='ШТС', required=True)
    start_date = fields.Date(string='Эхлэх огноо', required=True)
    end_date = fields.Date(string='Дуусах огноо', required=True)

    def get_export_data(self):
        company = self.env['res.company'].browse(self.company_id.id)
        context = self._context
        ezxf = xlwt.easyxf
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('Ээлжийн бүртгэлийн ажилтны тайлан')
        now = datetime.datetime.today()
        new_datetime = now + timedelta(hours=8)
        formatted_datetime = new_datetime.strftime('%Y-%m-%d %H:%M:%S')

        header = ezxf('font: name Times New Roman, bold on, height 180; align: wrap on, vert centre, horiz center; '
                      'borders: top thin, left thin, bottom thin, right thin, top_colour gray50,'
                      'bottom_colour gray50,left_colour gray50,right_colour gray50; pattern: pattern solid, fore_color pale_blue')
        data_center = ezxf(
            'font: name Times New Roman, bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')

        sheet.write(0, 13,  formatted_datetime, ezxf('font: name Times New Roman, bold on, height 200; align: horiz left'))
        # sheet.write_merge(2, 2, 0, 13, u'Ээлжийн бүртгэлийн нэгтгэл тайлан',
        #                   ezxf(
        #                       'font: name Times New Roman, bold on, height 200; align: wrap on, vert centre, horiz center'))
        sheet.write(4, 0, u'Байгууллагын нэр: %s' % (company.name),
                    ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))
        # sheet.write(5, 0, u'ШТС: %s' % (self.shts_uud_id.name),
        #             ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))
        sheet.write(4, 10, u'Эхлэх огноо: %s' % (self.start_date),
                    ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))
        sheet.write(5, 10, u'Дуусах огноо: %s' % (self.end_date),
                    ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))

        rowx = 7
        sheet.write(rowx, 0, u'№', header)
        sheet.write(rowx, 1, u'ШТС', header)
        sheet.write(rowx, 2, u'Ажилтны код', header)
        sheet.write(rowx, 3, u'Албан тушаал', header)
        sheet.write(rowx, 4, u'Овог нэр', header)
        sheet.write(rowx, 5, u'Нийт ажилласан цаг', header)
        sheet.write(rowx, 6, u'Цалин тооцох цаг', header)
        sheet.write(rowx, 7, u'Энгийн илүү цаг', header)
        sheet.write(rowx, 8, u'Баярын илүү цаг', header)
        sheet.write(rowx, 9, u'Цаг /Өдөр/', header)
        sheet.write(rowx, 10, u'Шөнийн цаг', header)
        sheet.write(rowx, 11, u'Шатахуун', header)
        sheet.write(rowx, 12, u'Хий', header)
        sheet.write(rowx, 13, u'ТТМ', header)
        sheet.write(rowx, 14, u'Акк', header)
        rowx = 7
        shts = []
        if self.shts_uud_id:
            shts = self.shts_uud_id.ids

        if  len(shts)== 1 and self.company_id and  self.start_date and self.end_date:
            self.env.cr.execute("""select c.name as company_name, r.name as shts_name,  s.state, e.name as employee_name,
            e.last_name, e.employee_code, j.name as job_name,d.name as department_name, sum(l.time) as udur_time,
            sum(l.n_time) as shono_tsag, sum(l.fuel_sale) as gasol, sum(l.gas) as hii, sum(l.qty) as ttm,
            sum(l.acc_qty) as akk
                from shift_working_register_line as l
                left join shift_working_register as s on s.id = l.shift_manager_id
                left join shts_register as r on r.id = s.shts_id
                left join hr_employee as e on e.id = l.employee_id
                left join hr_department as d on d.id = l.department_id
                left join hr_job as j on j.id = l.job_id
                left join res_company as c on c.id = s.company_id
                where s.company_id ='%d' and s.shts_id ='%d' and s.shift_date >='%s' and s.shift_date <='%s'
                group by c.name, r.name, s.state, e.name, e.last_name, e.employee_code, j.name, d.name """
                                % (self.company_id.id, shts[0], self.start_date, self.end_date))
            shift_working_register = self._cr.dictfetchall()
        elif len(shts) > 1 and self.company_id and  self.start_date and self.end_date:
            self.env.cr.execute("""select c.name as company_name, r.name as shts_name,  s.state, e.name as employee_name,
            e.last_name, e.employee_code, j.name as job_name,d.name as department_name, sum(l.time) as udur_time,
            sum(l.n_time) as shono_tsag, sum(l.fuel_sale) as gasol, sum(l.gas) as hii, sum(l.qty) as ttm,
            sum(l.acc_qty) as akk
                from shift_working_register_line as l
                left join shift_working_register as s on s.id = l.shift_manager_id
                left join shts_register as r on r.id = s.shts_id
                left join hr_employee as e on e.id = l.employee_id
                left join hr_department as d on d.id = l.department_id
                left join hr_job as j on j.id = l.job_id
                left join res_company as c on c.id = s.company_id
                where s.company_id ='%d' and s.shts_id in %s and s.shift_date >='%s' and s.shift_date <='%s'
                group by c.name, r.name, s.state, e.name, e.last_name, e.employee_code, j.name, d.name """
                                % (self.company_id.id, tuple(shts), self.start_date, self.end_date))
            shift_working_register = self._cr.dictfetchall()

        rowx += 1
        if shift_working_register != []:

            number = 1
            str_number = str(number)
            total_time = 0
            total_n_time = 0
            total_fuel_sale = 0
            total_gas = 0
            total_qty = 0
            total_acc_qty = 0
            for shift in shift_working_register:
                total_time += shift['udur_time']
                total_n_time += shift['shono_tsag']
                total_fuel_sale += shift['gasol']
                total_gas += shift['hii']
                total_qty += shift['ttm']
                total_acc_qty += shift['akk']
                name = shift['employee_name']
                last_name = shift['last_name'][0]
                employee_code = shift['employee_code']
                employee = f"{name}.{last_name}"
                all_time = shift['udur_time'] + shift['shono_tsag']

                sheet.write(rowx, 0, str_number, data_center)
                sheet.write(rowx, 1, '%s' % shift['shts_name'], data_center)
                sheet.write(rowx, 2, '%s' % employee_code, data_center)
                sheet.write(rowx, 3, '%s' % shift['job_name'], data_center)
                sheet.write(rowx, 4, '%s' % employee, data_center)
                sheet.write(rowx, 5, '%s' % all_time, data_center)
                sheet.write(rowx, 6, '', data_center)
                sheet.write(rowx, 7, '', data_center)
                sheet.write(rowx, 8, '', data_center)
                sheet.write(rowx, 9, '%s' % shift['udur_time'], data_center)
                sheet.write(rowx, 10, '%s' % shift['shono_tsag'], data_center)
                sheet.write(rowx, 11, '%s' % round(shift['gasol'], 2), data_center)
                sheet.write(rowx, 12, '%s' % shift['hii'], data_center)
                sheet.write(rowx, 13, '%s' % shift['ttm'], data_center)
                sheet.write(rowx, 14, '%s' % shift['akk'], data_center)
                rowx += 1
                number += 1
                str_number = str(number)


        rowx += 4
        sheet.write(rowx, 0, u'№', header)
        sheet.write(rowx, 1, u'ШТС', header)
        sheet.write(rowx, 2, u'Ажилтны код', header)
        sheet.write(rowx, 3, u'Албан тушаал', header)
        sheet.write(rowx, 4, u'Овог нэр', header)
        sheet.write(rowx, 5, u'Нийт ажилласан цаг', header)
        sheet.write(rowx, 6, u'Цалин тооцох цаг', header)
        sheet.write(rowx, 7, u'Энгийн илүү цаг', header)
        sheet.write(rowx, 8, u'Баярын илүү цаг', header)
        sheet.write(rowx, 9, u'Цаг /Өдөр/', header)
        sheet.write(rowx, 10, u'Шөнийн цаг', header)
        sheet.write(rowx, 11, u'Шатахуун', header)
        sheet.write(rowx, 12, u'Хий', header)
        sheet.write(rowx, 13, u'ТТМ', header)
        sheet.write(rowx, 14, u'Акк', header)
        if self.shts_uud_id:
            shts = self.shts_uud_id.ids

        if len(shts) == 1 and self.company_id and  self.start_date and self.end_date:
            self.env.cr.execute("""select c.name as company_name, r.name as shts_name,  s.state, e.name as employee_name, 
            e.last_name, e.employee_code, j.name as job_name,d.name as department_name, sum(l.time) as udur_time, 
            sum(l.n_time) as shono_tsag, sum(l.fuel_sale) as gasol, sum(l.gas) as hii, sum(l.qty) as ttm, 
            sum(l.acc_qty) as akk  
                from shift_working_register_line as l
                left join shift_working_register as s on s.id = l.shift_id
                left join shts_register as r on r.id = s.shts_id
                left join hr_employee as e on e.id = l.employee_id
                left join hr_department as d on d.id = l.department_id
                left join hr_job as j on j.id = l.job_id
                left join res_company as c on c.id = s.company_id
                where s.company_id ='%d' and s.shts_id = '%d' and s.shift_date >='%s' and s.shift_date <='%s'
                group by c.name, r.name, s.state, e.name, e.last_name, e.employee_code, j.name, d.name """
                                % (self.company_id.id, shts[0], self.start_date, self.end_date))
            shift_working_register = self._cr.dictfetchall()
        elif len(shts) > 1 and self.company_id and self.start_date and self.end_date:
            self.env.cr.execute("""select c.name as company_name, r.name as shts_name,  s.state, e.name as employee_name, 
            e.last_name, e.employee_code, j.name as job_name,d.name as department_name, sum(l.time) as udur_time, 
            sum(l.n_time) as shono_tsag, sum(l.fuel_sale) as gasol, sum(l.gas) as hii, sum(l.qty) as ttm, 
            sum(l.acc_qty) as akk  
                from shift_working_register_line as l
                left join shift_working_register as s on s.id = l.shift_id
                left join shts_register as r on r.id = s.shts_id
                left join hr_employee as e on e.id = l.employee_id
                left join hr_department as d on d.id = l.department_id
                left join hr_job as j on j.id = l.job_id
                left join res_company as c on c.id = s.company_id
                where s.company_id ='%d' and s.shts_id in %s and s.shift_date >='%s' and s.shift_date <='%s'
                group by c.name, r.name, s.state, e.name, e.last_name, e.employee_code, j.name, d.name """
                                % (self.company_id.id, tuple(shts), self.start_date, self.end_date))
            shift_working_register = self._cr.dictfetchall()

        rowx += 1
        if shift_working_register != []:

            number = 1
            str_number = str(number)
            total_time = 0
            total_n_time = 0
            total_fuel_sale = 0
            total_gas = 0
            total_qty = 0
            total_acc_qty = 0
            all_tsag = 0
            for shift in shift_working_register:
                total_time += shift['udur_time']
                total_n_time += shift['shono_tsag']
                total_fuel_sale += shift['gasol']
                total_gas += shift['hii']
                total_qty += shift['ttm']
                total_acc_qty += shift['akk']
                name = shift['employee_name']
                last_name = shift['last_name'][0]
                employee_code = shift['employee_code']
                employee = f"{name}.{last_name}"
                all_time = shift['udur_time'] + shift['shono_tsag']
                all_tsag += all_time

                sheet.write(rowx, 0, str_number, data_center)
                sheet.write(rowx, 1, '%s' % shift['shts_name'], data_center)
                sheet.write(rowx, 2, '%s' % employee_code, data_center)
                sheet.write(rowx, 3, '%s' % shift['job_name'], data_center)
                sheet.write(rowx, 4, '%s' % employee, data_center)
                sheet.write(rowx, 5, '%s' % all_time, data_center)
                sheet.write(rowx, 6, '', data_center)
                sheet.write(rowx, 7, '', data_center)
                sheet.write(rowx, 8, '', data_center)
                sheet.write(rowx, 9, '%s' % shift['udur_time'], data_center)
                sheet.write(rowx, 10, '%s' % shift['shono_tsag'], data_center)
                sheet.write(rowx, 11, '%s' % round(shift['gasol'], 2), data_center)
                sheet.write(rowx, 12, '%s' % shift['hii'], data_center)
                sheet.write(rowx, 13, '%s' % shift['ttm'], data_center)
                sheet.write(rowx, 14, '%s' % shift['akk'], data_center)
                rowx += 1
                number += 1
                str_number = str(number)

            sheet.write_merge(rowx, rowx, 0, 4, u'Нийт дүн', header)
            sheet.write(rowx, 5, all_tsag, header)
            sheet.write(rowx, 6, u'', header)
            sheet.write(rowx, 7, u'', header)
            sheet.write(rowx, 8, u'', header)
            sheet.write(rowx, 9, total_time, header)
            sheet.write(rowx, 10, total_n_time, header)
            sheet.write(rowx, 11, total_fuel_sale, header)
            sheet.write(rowx, 12, total_gas, header)
            sheet.write(rowx, 13, total_qty, header)
            sheet.write(rowx, 14, total_acc_qty, header)

        inch = 1000
        sheet.col(0).width = 1 * inch
        sheet.col(1).width = 5 * inch
        sheet.col(2).width = 3 * inch
        sheet.col(3).width = 6 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 5 * inch
        sheet.col(6).width = 2 * inch
        sheet.col(7).width = 2 * inch
        sheet.col(8).width = 2 * inch
        sheet.col(9).width = 3 * inch
        sheet.col(10).width = 3 * inch
        sheet.col(11).width = 3 * inch
        sheet.col(12).width = 3 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch

        return {'data': book, 'directory_name': u'Ажилтны цагийн бүртгэлийн тайлан',
                'attache_name': u'Ажилтны цагийн бүртгэлийн тайлан'}