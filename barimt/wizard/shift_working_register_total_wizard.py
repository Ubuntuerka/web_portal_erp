# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError
import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class ShiftWorkingRegisterTotalWizard(models.TransientModel):
    _inherit = 'abstract.report.excel'
    _name = 'shift.working.register.total.wizard'
    
    
    @api.model
    def _shts_id(self):
        shts = []
        shts_obj = self.env['shts.register']
        shts_id = shts_obj.search([('id','=',self.env.user.warehouse_id.id)])
        if shts_id:
            for line in shts_id:
                shts.append(line.id)
        else:
            for sh in self.env.user.shts_ids:
                shts.append(sh.id)
            
        if len(shts)==1:
            return [('id', '=', shts[0])]
        else:
            return [('id', 'in', tuple(shts))]

    company_id = fields.Many2one('res.company', string='Компани',
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'shift.working.register.total.wizard'))
    shts_id = fields.Many2one('shts.register', string='ШТС',domain=_shts_id)
    is_employee = fields.Boolean(string=u'Эрхлэгч татах')
    # employee_id = fields.Many2one('hr.employee', string='Ажилтан')
    start_date = fields.Date(string='Эхлэх огноо', required=True)
    end_date = fields.Date(string='Дуусах огноо', required=True)

    def get_export_data(self):
        company = self.env['res.company'].browse(self.company_id.id)
        context = self._context
        ezxf = xlwt.easyxf
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('Ээлжийн ажилсан эрхлэгч')
        header = ezxf('font: name Times New Roman, bold on, height 180; align: wrap on, vert centre, horiz center; '
                      'borders: top thin, left thin, bottom thin, right thin, top_colour gray50,'
                      'bottom_colour gray50,left_colour gray50,right_colour gray50; pattern: pattern solid, fore_color pale_blue')
        footer1 = ezxf('font: name Times New Roman, bold on, height 200')
        footer3 = ezxf('font: name Times New Roman, bold off;align:wrap off,vert centre,horiz right;font: height 200')
        italic = ezxf(
            'font: name Times New Roman, italic on, underline off, bold off;align:wrap off,vert centre,horiz right;font: height 160')
        underline = ezxf(
            'font: name Times New Roman, italic off, underline on, bold off;align:wrap off,vert centre,horiz right;font: height 200')
        data_center_left = ezxf(
            'font: name Times New Roman, bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_center = ezxf(
            'font: name Times New Roman, bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        format_with_separator = ezxf(
            'font: name Times New Roman, bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00')
        sheet.write_merge(2, 2, 0, 13, u'Ээлжийн бүртгэлийн нэгтгэл тайлан',
                          ezxf(
                              'font: name Times New Roman, bold on, height 200; align: wrap on, vert centre, horiz center'))
        sheet.write(4, 0, u'Байгууллагын нэр: %s' % (company.name),
                    ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))
        sheet.write(5, 0, u'ШТС: %s' % (self.shts_id.name),
                    ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))
        sheet.write(4, 10, u'Эхлэх огноо: %s' % (self.start_date),
                    ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))
        sheet.write(5, 10, u'Дуусах огноо: %s' % (self.end_date),
                    ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))

        rowx = 7
        sheet.write(rowx, 0, u'№', header)
        sheet.write(rowx, 1, u'ШТС', header)
        sheet.write(rowx, 2, u'Огноо', header)
        sheet.write(rowx, 3, u'Ээлж', header)
        sheet.write(rowx, 4, u'Төлөв', header)
        sheet.write(rowx, 5, u'Ажилтны нэр, код', header)
        sheet.write(rowx, 6, u'Албан тушаал', header)
        sheet.write(rowx, 7, u'Хэлтэс', header)
        sheet.write(rowx, 8, u'Цаг /Өдөр/', header)
        sheet.write(rowx, 9, u'Цаг /Шөнө/', header)
        sheet.write(rowx, 10, u'Бор шат л', header)
        sheet.write(rowx, 11, u'Бор тос л', header)
        sheet.write(rowx, 12, u'Бор акк ш', header)
        sheet.write(rowx, 13, u'Бор хийн түлш л', header)
        #
        if self.company_id and self.shts_id and self.start_date and self.end_date and self.is_employee==True :
            self.env.cr.execute("""select c.name as company_name, r.name as shts_name, s.shift_date, s.shift, s.state, 
                e.name as employee_name, e.last_name, e.employee_code, j.name as job_name,
                d.name as department_name, l.time, l.n_time, l.fuel_sale, l.gas, l.qty, l.acc_qty  
                from shift_working_register_line as l
                left join shift_working_register as s on s.id = l.shift_manager_id
                left join shts_register as r on r.id = s.shts_id
                left join hr_employee as e on e.id = l.employee_id
                left join hr_department as d on d.id = l.department_id
                left join hr_job as j on j.id = l.job_id
                left join res_company as c on c.id = s.company_id
                where s.company_id ='%d' and s.shts_id ='%d' and s.shift_date >='%s' and s.shift_date <='%s'
                order by shift_date asc, shift asc
                """
                                %(self.company_id.id, self.shts_id.id,self.start_date,self.end_date))
            shift_working_register = self._cr.dictfetchall()
        # and self.is_employee == False
        elif self.company_id and self.shts_id and self.start_date and self.end_date and self.is_employee==False :
            self.env.cr.execute("""select c.name as company_name, r.name as shts_name, s.shift_date, s.shift, s.state, 
                e.name as employee_name, e.last_name, e.employee_code, j.name as job_name,
                d.name as department_name, l.time, l.n_time, l.fuel_sale, l.gas, l.qty, l.acc_qty  
                from shift_working_register_line as l
                left join shift_working_register as s on s.id = l.shift_id
                left join shts_register as r on r.id = s.shts_id
                left join hr_employee as e on e.id = l.employee_id
                left join hr_department as d on d.id = l.department_id
                left join hr_job as j on j.id = l.job_id
                left join res_company as c on c.id = s.company_id
                where s.company_id ='%d' and s.shts_id ='%d' and s.shift_date >='%s' and s.shift_date <='%s'
                order by shift_date asc, shift asc
                """
                                %(self.company_id.id, self.shts_id.id,self.start_date,self.end_date))
            shift_working_register = self._cr.dictfetchall()
        elif self.company_id and self.start_date and self.end_date and self.is_employee==True :
            self.env.cr.execute("""select c.name as company_name, r.name as shts_name, s.shift_date, s.shift, s.state, 
                e.name as employee_name, e.last_name, e.employee_code, j.name as job_name,
                d.name as department_name, l.time, l.n_time, l.fuel_sale, l.gas, l.qty, l.acc_qty  
                from shift_working_register_line as l
                left join shift_working_register as s on s.id = l.shift_manager_id
                left join shts_register as r on r.id = s.shts_id
                left join hr_employee as e on e.id = l.employee_id
                left join hr_department as d on d.id = l.department_id
                left join hr_job as j on j.id = l.job_id
                left join res_company as c on c.id = s.company_id
                where s.company_id ='%d' and  s.shift_date >='%s' and s.shift_date <='%s'
                order by shift_date asc, shift asc
                """
                                %(self.company_id.id, self.start_date,self.end_date))
            shift_working_register = self._cr.dictfetchall()
        elif self.company_id and self.start_date and self.end_date and self.is_employee==False :
            self.env.cr.execute("""select c.name as company_name, r.name as shts_name, s.shift_date, s.shift, s.state, 
                e.name as employee_name, e.last_name, e.employee_code, j.name as job_name,
                d.name as department_name, l.time, l.n_time, l.fuel_sale, l.gas, l.qty, l.acc_qty  
                from shift_working_register_line as l
                left join shift_working_register as s on s.id = l.shift_id
                left join shts_register as r on r.id = s.shts_id
                left join hr_employee as e on e.id = l.employee_id
                left join hr_department as d on d.id = l.department_id
                left join hr_job as j on j.id = l.job_id
                left join res_company as c on c.id = s.company_id
                where s.company_id ='%d' and s.shift_date >='%s' and s.shift_date <='%s'
                order by shift_date asc, shift asc
                """
                                %(self.company_id.id, self.start_date,self.end_date))
            shift_working_register = self._cr.dictfetchall()

        elif self.start_date and self.end_date and self.is_employee==True:
            raise UserError('Компани болон ШТС сонголт хоосон байна бөгөлнө үү')

        elif self.start_date and self.end_date and self.is_employee==False:
            raise UserError('Компани болон ШТС сонголт хоосон байна бөгөлнө үү')

        elif self.shts_id and self.start_date and self.end_date and self.is_employee==True:
            raise UserError('Компани сонголт хоосон байна бөгөлнө үү')

        elif self.shts_id and self.start_date and self.end_date and self.is_employee==False:
            raise UserError('Компани сонголт хоосон байна бөгөлнө үү')

        elif self.start_date and self.end_date:
            raise UserError('Компани болон ШТС сонголт хоосон байна бөгөлнө үү')

        rowx += 1
        if shift_working_register !=[]:

            number = 1
            str_number = str(number)
            total_time = 0
            total_n_time = 0
            total_fuel_sale = 0
            total_gas = 0
            total_qty = 0
            total_acc_qty = 0
            state_labels = {
                'draft': u'Ноорог',
                'count': u'Ээлж хаагдсан',
                'confirm': u'Батлагдсан',
                'cancel': u'Буцаагдсан'}
            for shift in shift_working_register:
                formatted_shift_date = datetime.strptime(str(shift['shift_date']), DATE_FORMAT)
                formatted_shift_date_str = formatted_shift_date.strftime(DATE_FORMAT)
                state = shift['state']
                state_label = state_labels.get(state, state)
                total_time += shift['time']
                total_n_time += shift['n_time']
                total_fuel_sale += shift['fuel_sale']
                total_gas += shift['gas']
                total_qty += shift['qty']
                total_acc_qty += shift['acc_qty']
                name = shift['employee_name']
                last_name = shift['last_name'][0]
                employee_code = shift['employee_code']

                employee = f"{name}.{last_name}/{employee_code}"

                sheet.write(rowx, 0, str_number, data_center)
                sheet.write(rowx, 1, '%s' % shift['shts_name'], data_center)
                sheet.write(rowx, 2, '%s' % formatted_shift_date_str, data_center)
                sheet.write(rowx, 3, '%s' % shift['shift'], data_center)
                sheet.write(rowx, 4, '%s' % state_label, data_center)
                sheet.write(rowx, 5, '%s' % employee, data_center)
                sheet.write(rowx, 6, '%s' % shift['job_name'], data_center)
                sheet.write(rowx, 7, '%s' % shift['department_name'], data_center)
                sheet.write(rowx, 8, '%s' % shift['time'], data_center)
                sheet.write(rowx, 9, '%s' % shift['n_time'], data_center)
                sheet.write(rowx, 10, '%s' % shift['fuel_sale'], data_center)
                sheet.write(rowx, 11, '%s' % shift['qty'], data_center)
                sheet.write(rowx, 12, '%s' % shift['acc_qty'], data_center)
                sheet.write(rowx, 13, '%s' % shift['gas'], data_center)
                rowx += 1
                number += 1
                str_number = str(number)

            sheet.write_merge(rowx, rowx, 0, 7, u'Нийт дүн', header)
            sheet.write(rowx, 8, total_time, header)
            sheet.write(rowx, 9, total_n_time, header)
            sheet.write(rowx, 10, total_fuel_sale, header)
            sheet.write(rowx, 11, total_qty, header)
            sheet.write(rowx, 12, total_acc_qty, header)
            sheet.write(rowx, 13, total_gas, header)



        inch = 1000
        sheet.col(0).width = 1 * inch
        sheet.col(1).width = 7 * inch
        sheet.col(2).width = 4 * inch
        sheet.col(3).width = 2 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 6 * inch
        sheet.col(6).width = 7 * inch
        sheet.col(7).width = 4 * inch
        sheet.col(8).width = 3 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 4 * inch
        sheet.col(13).width = 4 * inch

        return {'data': book, 'directory_name': u'Ажилтны цагийн бүртгэлийн тайлан',
                'attache_name': u'Ажилтны цагийн бүртгэлийн тайлан'}