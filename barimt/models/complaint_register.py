# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree
class ComplaintRegister(models.Model):
    _name = 'complaint.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'reg_date desc'

    reg_date = fields.Datetime(string=u'Бүртгэсэн огноо', required=True,
                               default=lambda self: fields.datetime.now())
    state = fields.Selection([('burtgesen', u'Бүртгэсэн'),
                              ('shiljuulsen', u'Шилжүүлсэн'),
                              ('haasan', u'Хаасан')], string=u'Төлөв', default='burtgesen', tracking=True)
    chiglel = fields.Selection([('sanal', u'Санал'),
                             ('tatarhal', 'Талархал'),
                             ('gomdol', u'Гомдол')], u'Чиглэл',default='gomdol',required=True)
    type = fields.Selection([('buteegdehuun',u'Бүтээгдэхүүнтэй холбоотой'),
                             ('uilchilgee',u'Үйл ажиллагаа, стандарттай холбоотой'),
                             ('person',u'Ажилтантай холбоотой')], u'Төрөл')
    category_type = fields.Many2one('complaint.type.category', u'Ангилал')
    heglegch_phone = fields.Char(u'Хэрэглэгчийн утасны дугаар')
    delgerengui_utga = fields.Text(u'Дэлгэрэнгүй утга',required=True)
    department_id = fields.Many2one('hr.department',u'Шилжүүлсэн газар')
    shilj_user_id = fields.Many2one('res.users', u'Шилжүүлсэн ажилтан')
    shiljuulsen_date = fields.Datetime(string=u'Шилжүүлсэн огноо')
    user_id = fields.Many2one('res.users', u'Бүртгэсэн ажилтан', default=lambda self: self.env.user,
                                      required=True)
    shts_id = fields.Many2one('shts.register', string=u'Газар/Нэгж', help=u'Гомдол, талархал ирсэн газар')
    hugatsaa = fields.Boolean(u'Хугацаандаа шийдвэрлэсэн эсэх')
    gomdol_shiidwerlesen = fields.Text(u'Санал, гомдлыг хэрхэн шийдвэрлэсэн')
    suwag = fields.Many2one('complain.suwag',u'Гомдол ирсэн суваг')


    def action_shiljuuleh(self):
        if self.shiljuulsen_date  and self.department_id:
            self.write({'state': 'shiljuulsen'})
        else:
            raise UserError(_('Шилжүүлсэн огноо болон газар оруулсан эсэхийг шалгана уу!!'))

    def action_haah(self):
        self.write({'state': 'haasan'})


class ComplaintTypeCategory(models.Model):
    _name = 'complaint.type.category'

    name = fields.Char(u'Ангилал')
    type = fields.Selection([('buteegdehuun',u'Бүтээгдэхүүнтэй холбоотой'),
                             ('uilchilgee',u'Үйл ажиллагаа, стандарттай холбоотой'),
                             ('person',u'Ажилтантай холбоотой')], u'Төрөл')

class ComplaintSuwag(models.Model):
    _name = 'complain.suwag'

    name = fields.Char(u'Суваг')


class ComplaintTypeCategory(models.Model):
    _name = 'complaint.type.category'

    name = fields.Char(u'Ангилал')
    type = fields.Selection([('buteegdehuun',u'Бүтээгдэхүүнтэй холбоотой'),
                             ('uilchilgee',u'Үйл ажиллагаа, стандарттай холбоотой'),
                             ('person',u'Ажилтантай холбоотой')], u'Төрөл')

class ComplaintSuwag(models.Model):
    _name = 'complain.suwag'

    name = fields.Char(u'Суваг')
    
class ScalingRegister(models.Model):
    _inherit = 'scaling.register'

    def action_gas_balance_search(self, shift_ids):
         shift_id = False
         shift_date = False
         product_name = False
         itemId = False
         LocationId = False
         petrol_station = False
         can_code = False
         litr = 0
         kg = 0
         count = 0
         lines = []
         if shift_ids:
             line_ids = self.search([('shift_id','in',(shift_ids))],limit=1000)
             if line_ids:
                 for line_id in line_ids:
                     
                     count += 1
                     
                     lines.append({
                    "shift_id": line_id.shift_id.id,
                    'shift_date': str(line_id.shift_id.shift_date),
                    "company_id": line_id.company_id.id,
                    "LocationId": line_id.shift_id.shts_id.pos_number,
                    "petrol_station": line_id.shift_id.shts_id.name,
                    "can_code": line_id.can_id.name,
                    "itemId": line_id.product_id.fin_code,
                    "product_name": line_id.product_id.name,
                    "litr": round(line_id.litr,2),
                    "kg": round(line_id.kg,2),
                    "pipeline_volume": line_id.can_id.balance,
                    "pipeline_kg": round(line_id.can_id.balance * line_id.self_weight_value,2)
                })
          
         if count == 0:
             return {
                    "count":0,
                    "result":{}
                 }
         else:
             return {
                    "count":count,
                    "result":
                            lines
                        
                 }
             
class InventoryProduct(models.Model):
    _inherit = 'inventory.product'
    
    def action_oil_balance_search(self, inv_date,company_id):
        
         inventory_date = False
         product_name = False
         itemId = False
         LocationId = False
         petrol_station = False
         c2 = 0
         count = 0
         lines = []
         if inv_date and company_id:
             line_ids = self.search([('inventory_date','=',inv_date),('company_id','=',company_id),('state','=','done')],limit=200)
             if line_ids:
                 for lineid in line_ids:
                     for line_id in lineid.line_ids:
                         count += 1
                         
                         lines.append({
                            
                            'inventory_date': str(lineid.inventory_date),
                            "company_id": lineid.company_id.id,
                            "LocationId": line_id.shts_id.pos_number,
                            "petrol_station": line_id.shts_id.name,
                            "itemId": line_id.product_id.fin_code,
                            "product_name": line_id.product_id.name,
                            "c2": line_id.qty
                })
          
         if count == 0:
             return {
                    "count":0,
                    "result":{}
                 }
         else:
             return {
                    "count":count,
                    "result":
                            lines
                        
                 }
    

