# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-today OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime, date

import dateutil.parser
from lxml import etree
import time

from openerp import SUPERUSER_ID
from openerp import tools
from odoo import fields, api, models, tools, _
from openerp.tools.translate import _

class tushaal(models.Model):
    
    _name = 'tushaal'
    _description = "tushaal"
    _order = 'date desc'

    name = fields.Char(u'Тушаалын нэр',required=True)
    date = fields.Date('Огноо', required=True, default=lambda self: fields.datetime.now())
    dugaar = fields.Char(u'Дугаар')
    note = fields.Text(u'Тайлбар')
    blank_number = fields.Char(u'Бланк №:')
    blank_person = fields.Many2one('res.users', u'Бланк авсан хүн')
    gazar = fields.Many2one('hr.department',u'Хэлтэс')
    year = fields.Char(u'Жил')
    company_id = fields.Many2one('res.company', string=u'Компани', required=True)
    type = fields.Selection([('office',u'Оффис'),
                             ('shts_gtba',u'ШТС/ГТБА'),
                             ('shts',u'ШТС'),
                             ('gtba',u'ГТБА')], u'Төрөл')
    category = fields.Selection([('price', u'Үнэ, бусад'),
                                 ('hod_hariltsaa', u'Хөдөлмөрийн харилцаа')], u'Тушаалын ангилал')
    # _sql_constraints = [
    #    ('dugaar', 'unique (dugaar)', u'Тушаалын дугаар давхацсан байна. Өөр дугаар өгнө үү.')]

    @api.onchange('date')
    def onchange_date(self):
        if self.date:
            year = self.date.year
            self.year = (str(year))

    @api.onchange('company_id', 'category', 'date')
    def onchange_cat(self):
        if self.company_id and self.category and self.date:
            tushaal_number = self.search([('company_id', '=', self.company_id.id), ('year', '=', self.date.year),('category','=',self.category)],
                                          order='id desc', limit=1).dugaar
            if tushaal_number:
                too1 = int(tushaal_number[3:7]) + 1
                too = self._add_zero(too1)
                if self.category == 'price':
                    dugaar = '%s' % self.company_id.ugtwar + 'A/' + str(too)
                elif self.category == 'hod_hariltsaa':
                    dugaar = '%s' % self.company_id.ugtwar + 'B/' + str(too)
                else:
                    dugaar = ''
            else:
                if self.category == 'price':
                    dugaar = '%sA/0001' % self.company_id.ugtwar  # Эхний дугаар
                elif self.category == 'hod_hariltsaa':
                    dugaar = '%sB/0001' % self.company_id.ugtwar  # Эхний дугаар
                else:
                    dugaar = ''

            self.dugaar = dugaar  # Засварлах боломжтой

    def _add_zero(self, too):
        if too < 10:
            dugaar = "000" + str(too)
        elif too >= 10 and 100 > too:
            dugaar = "00" + str(too)
        elif too >= 100 and 1000 > too:
            dugaar = "0" + str(too)
        else:
            dugaar = too
        return dugaar


class yvsan_bichig(models.Model):
    _name = 'yvsanbichig'
    _order = 'date desc'

    name = fields.Char(u'Бичгийн утга',required=True)
    date = fields.Date(u'Огноо', required=True, default=lambda self: fields.datetime.now())
    baiguullaga = fields.Char(u'Бичиг очих байгууллагын нэр')
    dugaar = fields.Char(u'Дугаар')
    blank_number = fields.Char(u'Бланк №:')
    blank_person = fields.Many2one('res.users',u'Бланк авсан хүн')
    gazar = fields.Many2one('hr.department','Хэлтэс')
    company_id = fields.Many2one('res.company', string=u'Компани', required=True)
    year = fields.Char(u'Жил')

    # _sql_constraints = [
    #     ('dugaar', 'unique (dugaar)', u'Дугаар давхацсан байна. Өөр дугаар өгнө үү.')]

    @api.onchange('date')
    def onchange_date(self):
        if self.date:
            year = self.date.year
            self.year = (str(year))

    @api.onchange('company_id', 'date')
    def onchange_cat(self):
        if self.company_id and self.date:
            previous_number = self.search([('company_id', '=', self.company_id.id),('year','=', self.date.year)], order='id desc', limit=1).dugaar
            if previous_number:
                too1 = int(previous_number[2:6]) + 1
                too = self._add_zero(too1)
                dugaar = '%s/' % self.company_id.ugtwar + str(too)
            else:
                dugaar = '%s/0001' % self.company_id.ugtwar

            self.dugaar = dugaar  # Засварлах боломжтой

    def _add_zero(self, too):
        if too < 10:
            dugaar = "000" + str(too)
        elif too >= 10 and 100 > too:
            dugaar = "00" + str(too)
        elif too >= 100 and 1000 > too:
            dugaar = "0" + str(too)
        else:
            dugaar = too
        return dugaar


class irsen_bichig(models.Model):
    _name = 'irsenbichig'
    _order = 'date desc'

    name = fields.Char(u'Бичгийн утга',required=True)
    date = fields.Date(u'Огноо',required=True, default=lambda self: fields.datetime.now())
    bichig_date = fields.Date(u'Шийдвэрлэх огноо')
    irsen_baiguullaga = fields.Char(u'Бичиг ирсэн байгууллагын нэр')
    baiguullaga = fields.Many2one('hr.department',u'Газар/Алба')
    dugaar = fields.Char(u'Дугаар',required=True)
    user_id = fields.Many2one('res.users', u'Хариуцах эзэн', required=True)

    company_id = fields.Many2one('res.company', string=u'Компани', required=True)
    is_fix = fields.Boolean(u'Шийдвэрлэгдсэн эсэх')

                
                



class geree_type(models.Model):
    _name = 'geree_type'

    name = fields.Char('Name')
                


class geree(models.Model):
    _name = 'geree'
    _order = 'date desc'

    name = fields.Char(u'Гэрээний дугаар',required=True)
    date = fields.Date(u'Огноо',required=True, default=lambda self: fields.datetime.now())
    contract_name = fields.Char(u'Гэрээний нэр',required=True)
    partner_id = fields.Many2one('res.partner',u'Гэрээлэгч тал',required=True)
    user_id = fields.Many2one('res.users', u'Хариуцсан ажилтан')
    department_id = fields.Many2one('hr.department',u'Газар/алба')
    company_id = fields.Many2one('res.company', string=u'Компани', required=True)
    geree_type_id = fields.Many2one('geree_type',u'Нэр')
    year = fields.Char(u'Жил')
    # _sql_constraints = [
    #    ('name', 'unique (name)', u'Гэрээний дугаар давхацсан байна. Өөр дугаар өгнө үү.')]

    @api.onchange('date')
    def onchange_date(self):
        if self.date:
            year = self.date.year
            self.year = (str(year))

    @api.onchange('geree_type_id')
    def onchange_geree_type(self):
        if self.geree_type_id:
            self.contract_name = self.geree_type_id.name



    @api.onchange('company_id', 'date')
    def onchange_geree(self):
        if self.company_id and self.date:
            geree_number = self.search([('company_id', '=', self.company_id.id), ('year', '=', self.date.year)],
                                          order='id desc', limit=1).name
            if geree_number:
                too1 = int(geree_number[2:6]) + 1
                too = self._add_zero(too1)
                dugaar = '%s/' % self.company_id.ugtwar + str(too)
            else:
                dugaar = '%s/0001' % self.company_id.ugtwar

            self.name = dugaar  # Засварлах боломжтой

    def _add_zero(self, too):
        if too < 10:
            dugaar = "000" + str(too)
        elif too >= 10 and 100 > too:
            dugaar = "00" + str(too)
        elif too >= 100 and 1000 > too:
            dugaar = "0" + str(too)
        else:
            dugaar = too
        return dugaar
    

    

class huvez_shiidver(models.Model):
    _name = 'huvez_shiidver'
    _order = 'date desc'
    
    name = fields.Char(u'Шийдвэрийн нэр')
    date = fields.Date(u'Огноо',required=True, default=lambda self: fields.datetime.now())
    note = fields.Char(u'Тайлбар')
    dugaar = fields.Char(u'Дугаар',required=True)
    file = fields.Binary(u'Файл')
    year = fields.Char(u'Жил')
    company_id = fields.Many2one('res.company', string=u'Компани', required=True)

    @api.onchange('date')
    def onchange_date(self):
        if self.date:
            year = self.date.year
            self.year = (str(year))

    @api.onchange('company_id', 'date')
    def onchange_geree(self):
        if self.company_id and self.date:
            huvez_shiidver_number = self.search([('company_id', '=', self.company_id.id), ('year', '=', self.date.year)],
                                       order='id desc', limit=1).dugaar
            if huvez_shiidver_number:
                too1 = int(huvez_shiidver_number[2:6]) + 1
                too = self._add_zero(too1)
                dugaar = '%s/' % self.company_id.ugtwar + str(too)
            else:
                dugaar = '%s/0001' % self.company_id.ugtwar

            self.dugaar = dugaar  # Засварлах боломжтой

    def _add_zero(self, too):
        if too < 10:
            dugaar = "000" + str(too)
        elif too >= 10 and 100 > too:
            dugaar = "00" + str(too)
        elif too >= 100 and 1000 > too:
            dugaar = "0" + str(too)
        else:
            dugaar = too
        return dugaar
                

                
class huvez_togtool(models.Model):
    _name = 'huvez_togtool'
    _order = 'date desc'
    
    name = fields.Char(u'Тогтоолын нэр')
    date = fields.Date(u'Огноо',required=True, default=lambda self: fields.datetime.now())
    note = fields.Char(u'Тайлбар')
    dugaar = fields.Char(u'Дугаар',required=True)
    file = fields.Binary(u'Файл')
    year = fields.Char(u'Жил')
    company_id = fields.Many2one('res.company', string=u'Компани', required=True)

    @api.onchange('date')
    def onchange_date(self):
        if self.date:
            year = self.date.year
            self.year = (str(year))

    @api.onchange('company_id', 'date')
    def onchange_geree(self):
        if self.company_id and self.date:
            huvez_togtool_number = self.search(
                [('company_id', '=', self.company_id.id), ('year', '=', self.date.year)],
                order='id desc', limit=1).dugaar
            if huvez_togtool_number:
                too1 = int(huvez_togtool_number[2:6]) + 1
                too = self._add_zero(too1)
                dugaar = '%s/' % self.company_id.ugtwar + str(too)
            else:
                dugaar = '%s/0001' % self.company_id.ugtwar

            self.dugaar = dugaar  # Засварлах боломжтой

    def _add_zero(self, too):
        if too < 10:
            dugaar = "000" + str(too)
        elif too >= 10 and 100 > too:
            dugaar = "00" + str(too)
        elif too >= 100 and 1000 > too:
            dugaar = "0" + str(too)
        else:
            dugaar = too
        return dugaar
