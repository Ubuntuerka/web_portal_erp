# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree


class MileChangeRegister(models.Model):

    _name = 'mile.change.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'name desc'

    @api.model
    def _shts_id(self):
        shts = []
        shts_obj = self.env['shts.register']
        shts_id = shts_obj.search([('id', '=', self.env.user.warehouse_id.id)])
        if shts_id:
            for line in shts_id:
                shts.append(line.id)
        else:
            for sh in self.env.user.shts_ids:
                shts.append(sh.id)

        if len(shts) == 1:
            return [('id', '=', shts[0])]
        else:
            return [('id', 'in', tuple(shts))]

    name = fields.Char('Хүсэлтийн дугаар',readonly=True, default=lambda self: self.env['ir.sequence'].next_by_code('mile.change.register'))
    company_id = fields.Many2one('res.company', 'Компани', default=lambda self: self.env.user.company_id, required=True)
    shts_id = fields.Many2one('shts.register', 'ШТС', domain=_shts_id)
    shift_date = fields.Date(string=u'Ээлжийн огноо', required=True, default=fields.Date.context_today)
    shift = fields.Selection([('1', u'1'),
                              ('2', u'2')], string=u'Ээлж', required=True)
    reg_user_id = fields.Many2one('res.users', string='Бүртгэсэн ажилтан', readonly=True,
                                  default=lambda self: self.env.user, required=True)
    confirm_user_id = fields.Many2one('res.users', string='Шийдвэрлэх ажилтан', required=True)
    state = fields.Selection([('draft', 'Ноорог'),  # Ноорог
                              ('send', 'Илгээгдсэн'),
                              ('return', 'Буцаагдсан'),
                              ('done', 'Батлагдсан')], string='Төлөв', default='draft')
    can_id = fields.Many2one('can.register', string='Сав', required=True)
    first_mile = fields.Float(string='Эхний милл', readonly=True)
    change_mill = fields.Float(string='Солих милл', required=True)
    tailbar = fields.Char('Тайлбар')
    target = fields.Char(string='id')


    def action_data_import(self):
        shift_obj = self.env['shift.working.register']
        mile_target_obj = self.env['mile.target.register.line']
        for rec in self:
            shift = shift_obj.search([
                ('state', '=', 'draft'),
                ('company_id', '=', rec.company_id.id),
                ('shts_id', '=', rec.shts_id.id),
                ('shift_date', '=', rec.shift_date),
                ('shift', '=', rec.shift)
            ], limit=1)
            if shift:
                mile_change = mile_target_obj.search([
                    ('can_id', '=', rec.can_id.id),
                    ('shift_id', '=', shift.id)

                ], limit=1)
                if mile_change:
                    rec.first_mile = mile_change.first_mile
                    rec.target = mile_change.id
            else:
                raise UserError(u"Ээлж үүсээгүй эсвэл батлагдсан байна шалгана уу.")
        return rec

    def action_send(self):
        for record in self:
            if record.change_mill > 0.00:
                record.state = 'send'
                # confirm_user_info = record.confirm_user_id

                # Check if chat channel exists
                # channel_name = "Mile Change Request - {}".format(record.name)
                # existing_channel = self.env['mail.channel'].search([('name', '=', channel_name)])
                #
                # if not existing_channel:
                #     # Create new chat channel
                #     existing_channel = self.env['mail.channel'].create({
                #         'channel_partner_ids': [(4, confirm_user_info.partner_id.id)],
                #         'public': 'private',
                #         'channel_type': 'chat',
                #         'email_send': False,
                #         'name': channel_name,
                #     })
                #
                # # Send chat message
                # message_body = "Hello {}, a mile change request has been sent.".format(confirm_user_info.name)
                # existing_channel.message_post(body=message_body, message_type="comment")

            else:
                raise UserError(u"Милл солих талбар хоосон байна")

    def action_done(self):
        mile_target_obj = self.env['mile.target.register.line']
        for obj in self:
            obj.write({'state': 'done'})
            mile = mile_target_obj.search([('id', '=', obj.target), ('can_id', '=', obj.can_id.id)])
            mile.write({'first_mile': obj.change_mill})
            reg_user_info = obj.reg_user_id

            message = "Сайн байна уу?, Таньд милл солих хүсэлт батлагдлаа.".format(
                reg_user_info.name)

            channel_name = format(reg_user_info.name)
            channel = self.env['mail.channel'].create({
                'channel_partner_ids': [(4, reg_user_info.partner_id.id)],
                'public': 'private',
                'channel_type': 'chat',
                'email_send': False,
                'name': channel_name,
            })

            if channel:
                channel.message_post(body=message, message_type="comment")

    def action_return(self):
        self.write({'state': 'draft'})







