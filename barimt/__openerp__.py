# -*- coding: utf-8 -*-
##############################################################################
#
#    
#
#    
#
##############################################################################

{
    "name": "Barimt module",
    "version": "1.0",
    "author": "Uuganbayar",
    "category": "document",
    "description": """This module document management.""",
    "depends" : [
                 'hr_contract',
                 ],
    'update_xml': [
        "security/barimt_security_view.xml",
        "security/ir.model.access.csv",
        "views/barimt_view.xml",
        "views/complaint_register_view.xml",
        "views/mile_change_register_view.xml",
        "views/sequence_view.xml",
        "wizard/shift_working_register_total_wizard_view.xml",
        "wizard/shift_register_employee_wizard_view.xml",
        "views/menu_view.xml",
                   ],
    'test': [],
    'installable': True,
    'auto_install': False, 
}



