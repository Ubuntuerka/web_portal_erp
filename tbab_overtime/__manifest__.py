# -*- coding: utf-8 -*-
##############################################################################
#
#    TENGERSOFT, Enterprise Management Solution    
#    Copyright (C) 2016-2021 TENGERSOFT Co.,ltd (<http://www.tengersoft.mn>). All Rights Reserved
#
#    Address :
#    Email : tengersoft@gmail.com
#    Phone : 976 + 75153060
#
##############################################################################

{
    "name" : "HR Overtime",
    "version" : "1.0",
    "author" : "Tengersoft Developer Team",
    "category" : "HR",
    "description": """
       HR overtime - Илүү цагийн менежмент.
    """,
    "depends" : [
        "base",
        'hr',
        'hr_attendance',
    ],
    "update_xml" : [
                    'views/tbab_overtime_view.xml',
                    'security/tbab_overtime_security.xml',
                    'security/ir.model.access.csv',
                   ],
    "auto_install": False,
    "installable": True,
    "application": True,
}
