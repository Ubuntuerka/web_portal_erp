# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import da

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

_logger = logging.getLogger(__name__)


class tbab_overtime(models.Model):
    _name = 'tbab.overtime'
    _description = 'Overtime'
    _inherit = ['mail.thread']
    
    def _employee_get(self):        
        emp_id = self._context.get('default_employee_id', False)
        if emp_id:
            return emp_id
        ids = self.pool.get('hr.employee').search([('user_id', '=', self._uid)])
        if ids:
            return ids[0]
        return False
    
    name = fields.Text('Илүү цагаар ажиллах шалтгаан', required=True)
    employee_id = fields.Many2one('hr.employee', 'Ажилтан', track_visibility='onchange', default = lambda self: self.env.user.employee_ids.id)
    company_id = fields.Many2one('res.company', 'Компани')
    department_id = fields.Many2one('hr.department', 'Харьяалагдах нэгж')
    job_id = fields.Many2one('hr.job', 'Албан тушаал')
    date_start = fields.Datetime('Эхлэх огноо', select=True, copy=False,required=True)
    date_end = fields.Datetime('Дуусах огноо', select=True, copy=False,required=True)
    hours = fields.Float('Илүү ажилласан цаг')
    manager_id = fields.Many2one('hr.employee', 'Зөвшөөрөл өгсөн удирдлагын нэр')
    
    in_time = fields.Float('Ирсэн цаг')
    out_time = fields.Float('Явсан цаг')
    
    state = fields.Selection([('requested', 'Хүсэлт'),
                              ('approved', 'Батлагдсан'),
                              ('cancelled', 'Цуцлагдсан')
                              ], 'Төлөв', track_visibility='onchange', default='requested')
        
    is_no_limit = fields.Boolean('Ажлын цагийн хязгаар үйлчлэхгүй')
    
    def action_confirm(self):
        obj_employee = self.env['hr.employee']
        emp_id = obj_employee.search([('user_id', '=', self.env.user.id)],limit=1)
        emp = obj_employee.browse(emp_id)
        
        self.write({'state': 'approved',
                    'manager_id':emp.id})
        return True
    
    def action_cancel(self):
        obj_employee = self.env['hr.employee']
        emp_id = obj_employee.search([('user_id', '=', self.env.user.id)],limit=1)
        emp = obj_employee.browse(emp_id)
        
        self.write({'state': 'cancelled',
                    'manager_id':emp.id})
        return True
    
    def action_draft(self):
        self.write({'state': 'requested',
                    'manager_id':False})
        return True
    
    @api.onchange('employee_id')
    def onchange_employee_id(self):
        result = {'value': {'department_id': False, 'job_id': False}}
        if self.employee_id:
            employee = self.env['hr.employee'].browse(self.employee_id.id)
            result['value'] = { 'company_id': employee.company_id.id,
                                'department_id': employee.department_id.id,
                                'job_id': employee.job_id.id}
        return result
    
    # _order = 'sequence'
    @api.onchange('date_start','date_end')
    def onchange_date_start(self):
        DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        TIME_FORMAT = "%H:%M:%S"
        diff_hours = 0
        
        # date_end has to be greater than date_start
        if (self.date_start and self.date_end) and (self.date_start > self.date_end):
            raise ValidationError(_('Эхлэх огноо нь Дуусах огнооноос хойно байж болохгүй.'))
        result = {'value': {}}
        # Compute and update the hours of day - Amraltiin odriin ilvv tsag oloh
        if (self.date_start and self.date_end) and (self.date_start <= self.date_end):
            timedelta = datetime.strptime(str(self.date_end), DATETIME_FORMAT) - datetime.strptime(str(self.date_start), DATETIME_FORMAT)
            print ('FFFFFFF', timedelta)
            # Odor damnasan ognoo songowol aldaa zaana
            if timedelta.days > 0:
                raise ValidationError(_('Та өдөр болгоноор илүү цагийг тус тусад нь оруулна уу!'))
            # Ognoonuud neg odort hamaarch bwal hiih vildlvvd
            else:
                # Herew amraltiin odor ilvv tsag ajillasniig hvsch bga bol shuud ilvv tsagiin zorvvg olj tsag talbart ogno
                if self.is_no_limit == True:
                    diff_hours = float(timedelta.seconds) / 3600
                    result['value']['hours'] = diff_hours
                # Herew 'Ajliin odor'-t ajillasan ilvv tsag hvsch bwal hiih vildlvvd
                else:
                    date_start_date = self.date_start.date()  # ehleh ognoonii zowhon ognoog salgaj awah
                    date_end_date = self.date_end.date()  # duusah ognoonii zowhon ognoog salgaj awah
                    cal_morning_hour_from = 0.0
                    cal_morning_hour_to = 0.0
                    cal_afternoon_hour_from = 0.0
                    cal_afternoon_hour_to = 0.0
                    for line_cal in self.employee_id.resource_calendar_id.attendance_ids:
                        if line_cal.dayofweek == str(self.date_start.weekday()) and line_cal.day_period == 'morning':
                            cal_morning_hour_from = line_cal.hour_from
                            cal_morning_hour_to = line_cal.hour_to
                        elif line_cal.dayofweek == str(self.date_start.weekday()) and line_cal.day_period == 'afternoon':
                            cal_afternoon_hour_from = line_cal.hour_from
                            cal_afternoon_hour_to = line_cal.hour_to
                    
                    only_hour_morn = int(cal_morning_hour_from)
                    minutes = cal_morning_hour_from*60
                    hours, minutes = divmod(minutes, 60)
                    only_minut_morn = int(minutes)
                    ehleh_tsag = datetime.strptime(str(self.date_start), DATETIME_FORMAT).replace(hour=only_hour_morn, minute=only_minut_morn,second=0)
                    
                    only_hour_aft = int(cal_afternoon_hour_to)
                    minutes2 = cal_afternoon_hour_to*60
                    hours2, minutes2 = divmod(minutes2, 60)
                    only_minut_aft = int(minutes2)
                    duusah_tsag = datetime.strptime(str(self.date_start), DATETIME_FORMAT).replace(hour=only_hour_aft,minute=only_minut_aft,second=0)
                    
                    
                    #ehleh_tsag = str(date_end_date) + str(' 09:00:00')  # ajliin ehleh tsag
                    #duusah_tsag = str(date_end_date) + str(' 18:00:00')  # ajliin duusah tsag
                    
                    tttt = datetime.strptime(str(self.date_start), DATETIME_FORMAT) + relativedelta.relativedelta(hours=8)  # tsagiin zorvv arilgalt
                    if tttt <= ehleh_tsag:
                        niit_odriin_tsag = datetime.strptime(str(ehleh_tsag), DATETIME_FORMAT) - datetime.strptime(str(tttt), DATETIME_FORMAT)
                        # niit_odriin_tsag = datetime.strptime(niit_odriin_tsag_str, DATETIME_FORMAT)
                        datetime_bolgoh = str(date_end_date) + ' ' + str(niit_odriin_tsag)
                        # tsagiin zorvv olood ilvv tsag
                        ehlel_ilvv = datetime.strptime(str(datetime_bolgoh), DATETIME_FORMAT)
                        
                    else:
                        ehlel_ilvv_tsag = '00:00:00'
                        ehlel_ilvv = str(date_start_date) + ' ' + str(ehlel_ilvv_tsag)
                    pppp = datetime.strptime(str(self.date_end), DATETIME_FORMAT) + relativedelta.relativedelta(hours=8)
                    if pppp >= duusah_tsag:
                        duusah_ilvv_tsag = datetime.strptime(str(pppp), DATETIME_FORMAT) - datetime.strptime(str(duusah_tsag), DATETIME_FORMAT)
                        duusah_ilvv = str(date_end_date) + ' ' + str(duusah_ilvv_tsag)
                    else:
                        duusah_ilvv_tsag = '00:00:00'
                        duusah_ilvv = str(date_end_date) + ' ' + str(duusah_ilvv_tsag)
                    duusah_ilvv_dt = datetime.strptime(duusah_ilvv, DATETIME_FORMAT)
                    diff_hours = float(str(ehlel_ilvv)[11:13]) + float(str(ehlel_ilvv)[14:16]) / 60.0 + float(str(duusah_ilvv_dt)[11:13]) + float(str(duusah_ilvv_dt)[14:16]) / 60.0
                    result['value']['hours'] = diff_hours
                    
                    #print ('WWWWWWWW', ehleh_tsag, tttt, duusah_tsag, pppp)
        else:
            result['value']['hours'] = 0

        return result
    
#===============================================================================
#     @api.onchange('date_start','date_end')
#     def onchange_date_end(self):
#         DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
#         TIME_FORMAT = "%H:%M:%S"
#         diff_hours = 0
#         
#         # date_end has to be greater than date_start
#         if (date_start and date_end) and (date_start > date_end):
#             raise osv.except_osv(_('Анхааруулга!'), _('Эхлэх огноо нь Дуусах огнооноос хойно байж болохгүй.'))
#         result = {'value': {}}
#         # Compute and update the hours of day - Amraltiin odriin ilvv tsag oloh
#         if (date_start and date_end) and (date_start <= date_end):
#             timedelta = datetime.strptime(date_end, DATETIME_FORMAT) - datetime.strptime(date_start, DATETIME_FORMAT)
#             # Odor damnasan ognoo songowol aldaa zaana
#             if timedelta.days > 0:
#                 raise osv.except_osv(_('Анхааруулга!'), _('Та өдөр болгоноор илүү цагийг тус тусад нь оруулна уу!'))
#             # Ognoonuud neg odort hamaarch bwal hiih vildlvvd
#             else:
#                 # Herew amraltiin odor ilvv tsag ajillasniig hvsch bga bol shuud ilvv tsagiin zorvvg olj tsag talbart ogno
#                 if datetime.strptime(date_end, DATETIME_FORMAT).strftime("%A") == 'Saturday' or datetime.strptime(date_end, DATETIME_FORMAT).strftime("%A") == 'Sunday':
#                     diff_hours = float(timedelta.seconds) / 3600
#                     result['value']['hours'] = diff_hours
#                 # Herew 'Ajliin odor'-t ajillasan ilvv tsag hvsch bwal hiih vildlvvd
#                 else:
#                     date_start_date = date_start[0:10]  # ehleh ognoonii zowhon ognoog salgaj awah
#                     date_end_date = date_end[0:10]  # duusah ognoonii zowhon ognoog salgaj awah
#                     ehleh_tsag = str(date_end_date) + str(' 09:00:00')  # ajliin ehleh tsag
#                     duusah_tsag = str(date_end_date) + str(' 18:00:00')  # ajliin duusah tsag
#                     tttt = datetime.strptime(date_start, DATETIME_FORMAT) + relativedelta(hours=8)  # tsagiin zorvv arilgalt
#                     if str(tttt) <= ehleh_tsag:
#                         niit_odriin_tsag = datetime.strptime(ehleh_tsag, DATETIME_FORMAT) - datetime.strptime(str(tttt), DATETIME_FORMAT)
#                         datetime_bolgoh = str(date_end_date) + ' ' + str(niit_odriin_tsag)
#                         # tsagiin zorvv olood ilvv tsag
#                         ehlel_ilvv = datetime.strptime(str(datetime_bolgoh), DATETIME_FORMAT)
#                     else:
#                         ehlel_ilvv_tsag = '00:00:00'
#                         ehlel_ilvv = str(date_start_date) + ' ' + str(ehlel_ilvv_tsag)
#                     pppp = datetime.strptime(date_end, DATETIME_FORMAT) + relativedelta(hours=8)
#                     if str(pppp) >= duusah_tsag:
#                         duusah_ilvv_tsag = datetime.strptime(str(pppp), DATETIME_FORMAT) - datetime.strptime(duusah_tsag, DATETIME_FORMAT)
#                         duusah_ilvv = str(date_end_date) + ' ' + str(duusah_ilvv_tsag)
#                     else:
#                         duusah_ilvv_tsag = '00:00:00'
#                         duusah_ilvv = str(date_end_date) + ' ' + str(duusah_ilvv_tsag)
#                     duusah_ilvv_dt = datetime.strptime(duusah_ilvv, DATETIME_FORMAT)
#                     diff_hours = float(str(ehlel_ilvv)[11:13]) + float(str(ehlel_ilvv)[14:16]) / 60.0 + float(str(duusah_ilvv_dt)[11:13]) + float(str(duusah_ilvv_dt)[14:16]) / 60.0
#                     result['value']['hours'] = diff_hours
#         else:
#             result['value']['hours'] = 0
# 
#         return result
#===============================================================================
    @api.model
    def create(self, vals):
        # Onchange hiih ognoonuudaa todorhoiloh
        if 'date_start' in vals:
            date_start = vals['date_start']
        if 'date_end' in vals:
            date_end = vals['date_end']
        
        DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        TIME_FORMAT = "%H:%M:%S"
        diff_hours = 0
        
        # date_end has to be greater than date_start
        if (self.date_start and self.date_end) and (self.date_start > self.date_end):
            raise osv.except_osv(_('Эхлэх огноо нь Дуусах огнооноос хойно байж болохгүй.'))
        result = {'value': {}}
        # Compute and update the hours of day - Amraltiin odriin ilvv tsag oloh
        if (self.date_start and self.date_end) and (self.date_start <= self.date_end):
            timedelta = datetime.strptime(str(self.date_end), DATETIME_FORMAT) - datetime.strptime(str(self.date_start), DATETIME_FORMAT)
            # Odor damnasan ognoo songowol aldaa zaana
            if timedelta.days > 0:
                raise ValidationError(_('Та өдөр болгоноор илүү цагийг тус тусад нь оруулна уу!'))
            # Ognoonuud neg odort hamaarch bwal hiih vildlvvd
            else:
                # Herew amraltiin odor ilvv tsag ajillasniig hvsch bga bol shuud ilvv tsagiin zorvvg olj tsag talbart ogno
                if datetime.strptime(str(self.date_end), DATETIME_FORMAT).strftime("%A") == 'Saturday' or datetime.strptime(str(self.date_end), DATETIME_FORMAT).strftime("%A") == 'Sunday':
                    diff_hours = float(timedelta.seconds) / 3600
                    # result['value']['hours'] = diff_hours
                # Herew 'Ajliin odor'-t ajillasan ilvv tsag hvsch bwal hiih vildlvvd
                else:
                    date_start_date = self.date_start[0:10]  # ehleh ognoonii zowhon ognoog salgaj awah
                    date_end_date = self.date_end[0:10]  # duusah ognoonii zowhon ognoog salgaj awah
                    
                    
                    cal_morning_hour_from = 0.0
                    cal_morning_hour_to = 0.0
                    cal_afternoon_hour_from = 0.0
                    cal_afternoon_hour_to = 0.0
                    for line_cal in self.employee_id.resource_calendar_id.attendance_ids:
                        if line_cal.dayofweek == str(self.date_start.weekday()) and line_cal.day_period == 'morning':
                            cal_morning_hour_from = line_cal.hour_from
                            cal_morning_hour_to = line_cal.hour_to
                        elif line_cal.dayofweek == str(self.date_start.weekday()) and line_cal.day_period == 'afternoon':
                            cal_afternoon_hour_from = line_cal.hour_from
                            cal_afternoon_hour_to = line_cal.hour_to
                    
                    only_hour_morn = int(cal_morning_hour_from)
                    minutes = cal_morning_hour_from*60
                    hours, minutes = divmod(minutes, 60)
                    only_minut_morn = int(minutes)
                    ehleh_tsag = datetime.strptime(str(self.date_start), DATETIME_FORMAT).replace(hour=only_hour_morn, minute=only_minut_morn,second=0)
                    
                    only_hour_aft = int(cal_afternoon_hour_to)
                    minutes2 = cal_afternoon_hour_to*60
                    hours2, minutes2 = divmod(minutes2, 60)
                    only_minut_aft = int(minutes2)
                    duusah_tsag = datetime.strptime(str(self.date_start), DATETIME_FORMAT).replace(hour=only_hour_aft,minute=only_minut_aft,second=0)
                    
                    
                    #ehleh_tsag = str(date_end_date) + str(' 09:00:00')  # ajliin ehleh tsag
                    #duusah_tsag = str(date_end_date) + str(' 18:00:00')  # ajliin duusah tsag
                    tttt = datetime.strptime(str(self.date_start), DATETIME_FORMAT) + relativedelta.relativedelta(hours=8)  # tsagiin zorvv arilgalt
                    if str(tttt) <= ehleh_tsag:
                        niit_odriin_tsag = datetime.strptime(ehleh_tsag, DATETIME_FORMAT) - datetime.strptime(str(tttt), DATETIME_FORMAT)
                        datetime_bolgoh = str(date_end_date) + ' ' + str(niit_odriin_tsag)
                        # tsagiin zorvv olood ilvv tsag
                        ehlel_ilvv = datetime.strptime(str(datetime_bolgoh), DATETIME_FORMAT)
                    else:
                        ehlel_ilvv_tsag = '00:00:00'
                        ehlel_ilvv = str(date_start_date) + ' ' + str(ehlel_ilvv_tsag)
                    pppp = datetime.strptime(str(self.date_end), DATETIME_FORMAT) + relativedelta.relativedelta(hours=8)
                    if str(pppp) >= duusah_tsag:
                        duusah_ilvv_tsag = datetime.strptime(str(pppp), DATETIME_FORMAT) - datetime.strptime(duusah_tsag, DATETIME_FORMAT)
                        duusah_ilvv = str(date_end_date) + ' ' + str(duusah_ilvv_tsag)
                    else:
                        duusah_ilvv_tsag = '00:00:00'
                        duusah_ilvv = str(date_end_date) + ' ' + str(duusah_ilvv_tsag)
                    duusah_ilvv_dt = datetime.strptime(duusah_ilvv, DATETIME_FORMAT)
                    diff_hours = float(str(ehlel_ilvv)[11:13]) + float(str(ehlel_ilvv)[14:16]) / 60.0 + float(str(duusah_ilvv_dt)[11:13]) + float(str(duusah_ilvv_dt)[14:16]) / 60.0
                    # result['value']['hours'] = diff_hours
        else:
            diff_hours = 0
        
        overtime_id = super(tbab_overtime, self).create(vals)
        return overtime_id
    
    def write(self, vals):
        obj = self
        # Onchange hiih ognoonuudaa todorhoiloh
        if 'date_start' in vals:
            date_start = vals['date_start']
        else:
            date_start = self.date_start
        if 'date_end' in vals:
            date_end = vals['date_end']
        else:
            date_end = self.date_end
        
        DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        TIME_FORMAT = "%H:%M:%S"
        diff_hours = 0
        
        # date_end has to be greater than date_start
        if (self.date_start and self.date_end) and (self.date_start > self.date_end):
            raise ValidationError(_('Эхлэх огноо нь Дуусах огнооноос хойно байж болохгүй.'))
        result = {'value': {}}
        # Compute and update the hours of day - Amraltiin odriin ilvv tsag oloh
        if (self.date_start and self.date_end) and (self.date_start <= self.date_end):
            timedelta = datetime.strptime(str(self.date_end), DATETIME_FORMAT) - datetime.strptime(str(self.date_start), DATETIME_FORMAT)
            # Odor damnasan ognoo songowol aldaa zaana
            if timedelta.days > 0:
                raise ValidationError(_('Та өдөр болгоноор илүү цагийг тус тусад нь оруулна уу!'))
            # Ognoonuud neg odort hamaarch bwal hiih vildlvvd
            else:
                # Herew amraltiin odor ilvv tsag ajillasniig hvsch bga bol shuud ilvv tsagiin zorvvg olj tsag talbart ogno
                if datetime.strptime(str(self.date_end), DATETIME_FORMAT).strftime("%A") == 'Saturday' or datetime.strptime(str(self.date_end), DATETIME_FORMAT).strftime("%A") == 'Sunday':
                    diff_hours = float(timedelta.seconds) / 3600
                    # result['value']['hours'] = diff_hours
                # Herew 'Ajliin odor'-t ajillasan ilvv tsag hvsch bwal hiih vildlvvd
                else:
                    date_start_date = str(self.date_start)[0:10]  # ehleh ognoonii zowhon ognoog salgaj awah
                    date_end_date = str(self.date_end)[0:10]  # duusah ognoonii zowhon ognoog salgaj awah
                    ehleh_tsag = str(date_end_date) + str(' 09:00:00')  # ajliin ehleh tsag
                    duusah_tsag = str(date_end_date) + str(' 18:00:00')  # ajliin duusah tsag
                    tttt = datetime.strptime(str(self.date_start), DATETIME_FORMAT) + relativedelta.relativedelta(hours=8)  # tsagiin zorvv arilgalt
                    if str(tttt) <= ehleh_tsag:
                        niit_odriin_tsag = datetime.strptime(ehleh_tsag, DATETIME_FORMAT) - datetime.strptime(str(tttt), DATETIME_FORMAT)
                        datetime_bolgoh = str(date_end_date) + ' ' + str(niit_odriin_tsag)
                        # tsagiin zorvv olood ilvv tsag
                        ehlel_ilvv = datetime.strptime(str(datetime_bolgoh), DATETIME_FORMAT)
                    else:
                        ehlel_ilvv_tsag = '00:00:00'
                        ehlel_ilvv = str(date_start_date) + ' ' + str(ehlel_ilvv_tsag)
                    pppp = datetime.strptime(str(self.date_end), DATETIME_FORMAT) + relativedelta.relativedelta(hours=8)
                    if str(pppp) >= duusah_tsag:
                        duusah_ilvv_tsag = datetime.strptime(str(pppp), DATETIME_FORMAT) - datetime.strptime(duusah_tsag, DATETIME_FORMAT)
                        duusah_ilvv = str(date_end_date) + ' ' + str(duusah_ilvv_tsag)
                    else:
                        duusah_ilvv_tsag = '00:00:00'
                        duusah_ilvv = str(date_end_date) + ' ' + str(duusah_ilvv_tsag)
                    duusah_ilvv_dt = datetime.strptime(duusah_ilvv, DATETIME_FORMAT)
                    diff_hours = float(str(ehlel_ilvv)[11:13]) + float(str(ehlel_ilvv)[14:16]) / 60.0 + float(str(duusah_ilvv_dt)[11:13]) + float(str(duusah_ilvv_dt)[14:16]) / 60.0
                    # result['value']['hours'] = diff_hours
        else:
            diff_hours = 0
            
        return super(tbab_overtime, self).write(vals)
    
     
class hr_department(models.Model):
    _inherit = 'hr.department'
    
    sub_manager_id = fields.Many2one('hr.employee', 'Sub manager')
    sub_manager_ids = fields.Many2many('hr.employee', 'department_hr_rel', 'sub_manager_ids', 'sub_manager_id', 'Sub managers')
