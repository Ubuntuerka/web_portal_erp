# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import tools
from odoo.tools.sql import drop_view_if_exists
from odoo import api, fields, models


    
class internal_training_report(models.Model):
    _name = 'internal.training.report'
    _description = 'Internal training report' #Сургалтын төлөвлөгөөний Шинжилгээ тайлан
    _auto = False
    
    name = fields.Char('Training subject', required=True, track_visibility='onchange')#Сургалтын сэдэв
    code = fields.Char('Code', track_visibility='onchange')#Сургалтын код
    training_category = fields.Selection([('internal', 'Internal'), ('external', 'External')], 'Training category', track_visibility='onchange')#Ангилал
    type = fields.Selection([('annual_planned', 'Annual planned'), ('company_unplanned', 'Company unplanned'), ('staff_order', 'Staff order')], 'Type', track_visibility='onchange')#Төрөл
    training_type = fields.Many2one('training.type', 'Sub type', track_visibility='onchange')#Дэд төрөл
    inst_name = fields.Many2one('res.partner', 'Training organization', track_visibility='onchange')#Сургалт зохион байгуулах газрын нэр
    trainer_name = fields.Many2one('res.partner', 'Trainer name', track_visibility='onchange')#Сургалт хийх хүн
    employee_number = fields.Integer('Employee number')#Хамрагдах ажилтнуудын тоо
    inst_date = fields.Date('Start date', track_visibility='onchange')#Эхлэх огноо
    end_date = fields.Date('End date', track_visibility='onchange')#Дуусах огноо
    ended_date = fields.Date('Ended date', track_visibility='onchange')#Дууссан огноо
    inst_budget = fields.Float('Training budget', track_visibility='onchange')#Анхны төсөв
    authority_sign = fields.Many2one('res.users', 'Authority sign', readonly=True, default=lambda self: self.env.user)#Баталгаажуулагчийн гарын үсэг
    respondent_id = fields.Many2one('hr.employee', 'Respondent')#Хариуцсан ажилтан
    training_execution_id = fields.Many2one('training.execution', 'Training execution')#Сургалтын төлөвлөгөөний биелэлт
    #employee_line_ids = fields.Many2many('hr.employee', 'hr_inter_tr_id', 'inter_tr_hr_rel', 'internal_training_id', 'EmpInterTr', track_visibility='onchange')#Хамрагдах ажилчдийн хэсэг
    state = fields.Selection([
        ('draft', 'Draft'),#Ноорог
        ('cancel', 'Cancel'),#Цуцлагдсан
        ('approve', 'Approve'),#Батлагдсан
        ], 'State', track_visibility='onchange', default='draft')#Төлөв
    company_id = fields.Many2one('res.company', 'Company', required=True, track_visibility='onchange', default=lambda self: self.env.company)#Харъяалагдах компани
    
    line_name = fields.Char('Emp name')#Сургалтанд хамрагдсан ажилтан
    
    inter_id = fields.Many2one('internal.training', 'Training #', readonly=True)
    
    

#===============================================================================
#     def _select(self):
#         return super(internal_training_report, self)._select() + ", internal_training as it line.name for in line in it.employee_line_ids as line_name"
# 
#     def _group_by(self):
#         return super(AccountInvoiceReport, self)._group_by() + ", line.name"
#===============================================================================
     
 
    #===========================================================================
    # def init(self):
    #     drop_view_if_exists(self.env.cr, 'internal_training_report')
    #     self.env.cr.execute("""
    #         create or replace view internal_training_report as (
    #             SELECT
    #                 min(l.id) as id,
    #                 l.name as line_name,
    #                  
    #                 s.name as name,
    #                 s.code as code,
    #                 s.training_category as training_category,
    #                 s.type as type,
    #                 s.training_type as training_type,
    #                  
    #                 rp.name as inst_name,
    #                 partner.name as trainer_name,
    #                 s.employee_number as employee_number,
    #                 s.inst_date as inst_date,
    #                 s.end_date as end_date,
    #                 s.ended_date as ended_date,
    #                 s.inst_budget as inst_budget,
    #                 ru.authority_sign as authority_sign,
    #                 he.respondent_id as respondent_id,
    #                 s.state as state,
    #                 rc.company_id as company_id,
    #                 s.id as inter_id
    #                  
    #             FROM
    #                 hr_employee l
    #                 join internal_training s
    #                 left    join training_type tt on (tt.id in s.training_type)
    #                 left    join res_partner rp on s.inst_name = rp.id
    #                 left    join res_partner partner on s.trainer_name = partner.id
    #                 left    join res_users ru on s.authority_sign = ru.id
    #                 left    join hr_employee he on s.respondent_id = he.id
    #                 left    join res_company rc on s.company_id = rc.id
    #                  
    #             WHERE
    #                 l.id in s.employee_line_ids
    #             GROUP BY
    #                 l.name,
    #                  
    #                 s.name,
    #                 s.code,
    #                 s.training_category,
    #                 s.type,
    #                 s.training_type,
    #                  
    #                 rp.name,
    #                 partner.name,
    #                 s.inst_date,
    #                 s.end_date,
    #                 s.ended_date,
    #                 ru.authority_sign,
    #                 he.respondent_id,
    #                 s.state,
    #                 rc.company_id
    #                 s.id
    #         )""")
    #===========================================================================
#===============================================================================
#     
#     def _query(self, with_clause='', fields={}, groupby='', from_clause=''):
#         with_ = ("WITH %s" % with_clause) if with_clause else ""
# 
#         select_ = """
#             min(l.id) as id,
#             l.name as line_name,
#             
#             s.name as name,
#             s.code as code,
#             s.training_category as training_category,
#             s.type as type,
#             s.training_type as training_type,
#             
#             rp.name as inst_name,
#             partner.name as trainer_name,
#             s.employee_number as employee_number,
#             s.inst_date as inst_date,
#             s.end_date as end_date,
#             s.ended_date as ended_date,
#             s.inst_budget as inst_budget,
#             ru.authority_sign as authority_sign,
#             he.respondent_id as respondent_id,
#             s.state as state,
#             rc.company_id as company_id,
#             s.id as inter_id
#         """
# 
#         for field in fields.values():
#             select_ += field
# 
#         from_ = """
#                 hr_employee l
#                     join internal_training s on (l.id in s.employee_line_ids)
#                 left    join training_type tt on (tt.id in s.training_type)
#                 left    join res_partner rp on s.inst_name = rp.id
#                 left    join res_partner partner on s.trainer_name = partner.id
#                 left    join res_users ru on s.authority_sign = ru.id
#                 left    join hr_employee he on s.respondent_id = he.id
#                 left    join res_company rc on s.company_id = rc.id
#                 %s
#         """ % from_clause
# 
#         groupby_ = """
#             l.name,
#             
#             s.name,
#             s.code,
#             s.training_category,
#             s.type,
#             s.training_type,
#             
#             rp.name,
#             partner.name,
#             s.inst_date,
#             s.end_date,
#             s.ended_date,
#             ru.authority_sign,
#             he.respondent_id,
#             s.state,
#             rc.company_id
#             s.id %s
#         """ % (groupby)
# 
#         return '%s (SELECT %s FROM %s WHERE l.name IS NOT NULL GROUP BY %s)' % (with_, select_, from_, groupby_)
# 
#     def init(self):
#         self._table = internal_training_report
#         #tools.drop_view_if_exists(self.env.cr, self._table)
#         self.env.cr.execute("""CREATE or REPLACE VIEW %s as (%s)""" % (self._table, self._query()))
# 
# class InternalTrainingReportProforma(models.AbstractModel):
#     _name = 'internal.training.report_saleproforma'
#     _description = 'InternalTraining Report'
# 
#     def _get_report_values(self, docids, data=None):
#         docs = self.env['internal.training'].browse(docids)
#         return {
#             'doc_ids': docs.ids,
#             'doc_model': 'internal.training',
#             'docs': docs,
#             'proforma': True
#         }
#===============================================================================
