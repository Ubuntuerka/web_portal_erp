# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import da

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

_logger = logging.getLogger(__name__)

        
class training_type(models.Model):
    _name = 'training.type'
    _description = 'Training type' #Сургалтын төрөл
    
    name = fields.Char('Training type')#Сургалтын төрөл
    training_code = fields.Char('Training code')#Код
    level = fields.Selection([('basic', 'Basic'), ('medium', 'Medium'), ('best', 'Best')], 'Level')#Түвшин
    hr_employee_id = fields.Many2one('hr.employee', 'Hr employee')
    
class internal_training(models.Model):
    _name = 'internal.training'
    _inherit = ['mail.thread']
    _description = 'Internal training' #Сургалтын төлөвлөгөө
    
    name = fields.Char('Training subject', required=True, track_visibility='onchange')#Сургалтын сэдэв
    code = fields.Char('Code', track_visibility='onchange')#Сургалтын код
    training_category = fields.Selection([('internal', 'Internal'), ('external', 'External')], 'Training category', track_visibility='onchange')#Ангилал
    type = fields.Selection([('annual_planned', 'Annual planned'), ('company_unplanned', 'Company unplanned'), ('staff_order', 'Staff order')], 'Type', track_visibility='onchange')#Төрөл
    training_type = fields.Many2one('training.type', 'Sub type', track_visibility='onchange')#Дэд төрөл
    inst_name = fields.Many2one('res.partner', 'Training organization', track_visibility='onchange')#Сургалт зохион байгуулах газрын нэр
    trainer_name = fields.Many2one('res.partner', 'Trainer name', track_visibility='onchange')#Сургалт хийх хүн
    employee_number = fields.Integer('Employee number')#Хамрагдах ажилтнуудын тоо
    inst_date = fields.Date('Start date', track_visibility='onchange')#Эхлэх огноо
    end_date = fields.Date('End date', track_visibility='onchange')#Дуусах огноо
    ended_date = fields.Date('Ended date', track_visibility='onchange')#Дууссан огноо
    inst_budget = fields.Float('Training budget', track_visibility='onchange')#Анхны төсөв
    authority_sign = fields.Many2one('res.users', 'Authority sign', readonly=True, default=lambda self: self.env.user)#Баталгаажуулагчийн гарын үсэг
    respondent_id = fields.Many2one('hr.employee', 'Respondent')#Хариуцсан ажилтан
    training_execution_id = fields.Many2one('training.execution', 'Training execution')#Сургалтын төлөвлөгөөний биелэлт
    employee_line_ids = fields.Many2many('hr.employee', 'hr_inter_tr_id', 'inter_tr_hr_rel', 'internal_training_id', 'EmpInterTr', track_visibility='onchange')#Хамрагдах ажилчдийн хэсэг
    state = fields.Selection([
        ('draft', 'Draft'),#Ноорог
        ('cancel', 'Cancel'),#Цуцлагдсан
        ('approve', 'Approve'),#Батлагдсан
        ], 'State', track_visibility='onchange', default='draft')#Төлөв
    company_id = fields.Many2one('res.company', 'Company', required=True, track_visibility='onchange', default=lambda self: self.env.company)#Харъяалагдах компани

    

    def action_to_draft(self):
        self.write({'state': 'draft'})
        return True

    def action_to_cancel(self):
        order_obj = self.env['training.order'].search([('plan_id','=',self.id)])
        if order_obj:
            for order_line in order_obj:
                order_line.write({'state': 'cancel'})
        self.write({'state': 'cancel'})
        return True
    
    def action_to_approve(self):
        # mail yawuulah - send mail (ustgaj bolohgvi)
    
#===============================================================================
#         obj = self.browse(cr, uid, ids)[0]
#         empl_obj = self.pool.get('hr.employee')
# 
#         template_obj = self.pool.get('email.template')
#         group_model_id = self.pool.get('ir.model').search(cr, uid, [('model', '=', 'internal.training')])[0]
#         body_html = '''Та дотоод сургалтанд хамрагдлаа'''
#         if obj.employee_line_ids:
#             for eee in obj.employee_line_ids:
#                 empl = empl_obj.browse(cr, uid, eee.id)
#                 print 'geeeeeeh', empl.user_id.login
# 
#                 template_data = {
#                     'model_id': group_model_id,
#                     'name': 'Сургалтанд хамрагдсан мэдэгдэл!',
#                     'subject' : obj.name,
#                     'body_html': body_html,
#                     #'email_from' : '${object.user_id.email}',
#                     'type' : 'email',
#                     'email_from' : 'torbatj79@gmail.com',
#                     'email_to' : empl.user_id.login,
#                 }
# 
#                 template_id = template_obj.create(cr, uid, template_data, context=context)
#                 template_obj.send_mail(cr, uid, template_id, ids[0], force_send=True, context=context)
#         
#         #zurwas vvsgeh - create message
#         for record in self.browse(cr, uid, ids, context=context):
#             if record.employee_line_ids:
#                 for lili in record.employee_line_ids:
#                      self.message_subscribe_users(cr, uid, [record.id], user_ids=[lili.user_id.id], context=context)
#         return self.write(cr, uid, ids, {'state': 'approve'})
#===============================================================================
        order_obj = self.env['training.order'].search([('plan_id','=',self.id)])
        if order_obj:
            for order_line in order_obj:
                order_line.write({'state': 'approved'})
        self.write({'state': 'approve',
                    'ended_date': date.today(),
                    })
        return True
    
    def action_invoice(self):
        
        return True   
    
    #===========================================================================
    # def unlink(self, cr, uid, ids, context=None):
    #     obj = self.browse(cr, uid, ids)[0]
    #     line_obj = self.pool.get('employee.line')
    #     for line in obj.employee_line_id:
    #         line_obj.unlink(cr, uid, line.id)
    #     return super(internal_training, self).unlink(cr, uid, ids, context=context)
    #===========================================================================
    
    @api.model
    def create(self,vals):
        j = 0
        if 'employee_line_ids' in vals:
            for line in vals['employee_line_ids']:
                for myyy in line[2]:
                    if myyy:
                        j = j + 1
        vals['employee_number'] = j
        my_id = super(internal_training, self).create(vals)
        obj = self.browse(my_id)
        return my_id
    
    #@api.model
    def write(self,vals):
        j = 0
        sum1 = 0
        sum2 = 0
        if 'employee_line_ids' in vals:
            for line in vals['employee_line_ids']:
                for myyy in line[2]:
                    if myyy:
                        j = j + 1
            sum1 = j
        else:
            for line in self.employee_line_ids:
                if line:
                    j = j + 1
            sum2 = j
        vals['employee_number'] = sum1 + sum2
        my_id = super(internal_training, self).write(vals) 
        return my_id

    


        
class training_execution(models.Model):
    _name = 'training.execution'
    _inherit = ['mail.thread']
    _description = 'Training execution' #Сургалтын төлөвлөгөөний биелэлт
    
    name = fields.Many2one('res.partner', 'Training organization', required=True, track_visibility='onchange')#Сургалт зохион байгуулах газрын нэр
    code = fields.Char('Code', track_visibility='onchange')#Сургалтын код
    training_category = fields.Selection([('internal', 'Internal'), ('external', 'External')], 'Training category', track_visibility='onchange')#Ангилал
    type = fields.Selection([('annual_planned', 'Annual planned'), ('company_unplanned', 'Company unplanned'), ('staff_order', 'Staff order')], 'Type', track_visibility='onchange')#Төрөл
    training_type = fields.Many2one('training.type', 'Sub type', track_visibility='onchange')#Дэд төрөл
    respondent_id = fields.Many2one('hr.employee', 'Respondent')#Хариуцсан ажилтан
    #training_type = fields.Char('Training type')#Дэд төрөл
    employee_number = fields.Integer('Employee number')#Хамрагдах ажилтнуудын тоо
    involved_number = fields.Integer('Involved number')#Хамрагдсан ажилтнуудын тоо
    inst_date = fields.Date('Start date', track_visibility='onchange')#Эхлэх огноо
    end_date = fields.Date('End date', track_visibility='onchange')#Дуусах огноо
    trainer_name = fields.Many2one('res.partner', 'Trainer name', track_visibility='onchange')#Сургалт хийсэн хүн
    ended_date = fields.Date('Ended date', track_visibility='onchange')#Дууссан огноо
    #is_plan = fields.Selection([('no', 'No'), ('yes', 'Yes')], 'Is plan', track_visibility='onchange')#Төлөвлөгөөт эсэх
    inst_budget = fields.Float('Training budget', track_visibility='onchange')#Анхны төсөв
    base_expense = fields.Float('Base expense', track_visibility='onchange')#Бодит гарсан зардал
    other_expense = fields.Float('Other expense', track_visibility='onchange')#Нэмэлт зардал
    present_expense = fields.Float('Present expense', track_visibility='onchange')#Нийт зардал
    variation_cost = fields.Float('Variation cost', track_visibility='onchange')#Төсвийн зөрүү
    internal_training_id = fields.Many2one('internal.training', 'Training subject', required=True, track_visibility='onchange')#Сургалтын сэдэв
    invoice_id = fields.Many2one('account.invoice', 'Invoice')#Үүссэн нэхэмжлэх
    company_id = fields.Many2one('res.company', 'Company', track_visibility='onchange')#Харъяалагдах компани
    execution_line_id = fields.Many2many('hr.employee', 'hr_exec_id', 'exec_line_hr_rel', 'training_execution_id', 'EmpExecLine', track_visibility='onchange')#Хамрагдагсан ажилчдийн хэсэг
    state = fields.Selection([
        ('draft', 'Draft'),#Ноорог
        ('invoiced', 'Invoiced'),#Нэхэмжилсэн
        ('done', 'Done'),#Баталгаажсан
        ('cancel', 'Cancel'),#Цуцлагдсан
        ], 'State', track_visibility='onchange', default='draft')#Төлөв


    def action_to_cancel(self):
        inter_obj = self.env['internal.training']
        
        self.internal_training_id.action_to_cancel()
            
        order_obj = self.env['training.order'].search([('plan_id','=',self.internal_training_id.id)])
        if order_obj:
            for order_line in order_obj:
                order_line.write({'state': 'done'})
                
        self.write({'state': 'cancel'})
        return True

    def action_to_done(self):
        inter_obj = self.env['internal.training']
        if self.ended_date == False:
            raise ValidationError(_("Та дууссан огноо талбарыг оруулна уу!!!"))
        else:
            self.internal_training_id.write({'ended_date': self.ended_date})
            
        order_obj = self.env['training.order'].search([('plan_id','=',self.internal_training_id.id)])
        if order_obj:
            for order_line in order_obj:
                order_line.write({'state': 'done'})
                
        self.write({'state': 'done'})
        return True
    
    @api.model
    def create(self,vals):
        j = 0
        if 'execution_line_id' in vals:
            for line in vals['execution_line_id']:
                for myyy in line[2]:
                    if myyy:
                        j = j + 1
            vals['involved_number'] = j
        employees_id = super(training_execution, self).create(vals)

        return employees_id
    
    def write(self,vals):
        j = 0
        if 'execution_line_id' in vals:
            for line in vals['execution_line_id']:
                for myyy in line[2]:
                    if myyy:
                        j = j + 1
            vals['involved_number'] = j
        else:
            for line in self.execution_line_id:
                if line:
                    j = j + 1
            vals['involved_number'] = j
        my_id = super(training_execution, self).write(vals)
        return my_id
    
    @api.onchange('internal_training_id')
    def onchange_name(self):
        res = {}
        if self.internal_training_id:
            training_obj = self.env['internal.training']
            training = training_obj.browse(self.internal_training_id.id)
            res['type'] = training.type
            res['training_category'] = training.training_category
            res['training_type'] = training.training_type.id
            res['code'] = training.code
            res['respondent_id'] = training.respondent_id.id
            res['name'] = training.inst_name
            res['company_id'] = training.company_id.id
            res['employee_number'] = training.employee_number
            res['inst_date'] = training.inst_date
            res['end_date'] = training.end_date
            res['inst_budget'] = training.inst_budget
            res['trainer_name'] = training.trainer_name.id
            res['execution_line_id'] = training.employee_line_ids
            
            return {'value':res}
        
    @api.onchange('other_expense','base_expense')
    def onchange_sum(self):
       if self.other_expense >= 0 and self.base_expense >= 0:
           sum = self.base_expense + self.other_expense
           return {'value': {'present_expense': sum, }}
    
    @api.onchange('inst_budget','present_expense')
    def onchange_odd(self):
       if self.inst_budget >= 0 and self.present_expense >= 0:
           odd = self.inst_budget - self.present_expense
           return {'value': {'variation_cost': odd}}
       
    def action_invoice(self):
        invoice = self.env['account.invoice']
        journal_obj = self.env['account.journal']
       
        currency_id = self.company_id.currency_id.id
        journal_ids = journal_obj.search([('type', '=', 'purchase'), ('company_id', '=', self.company_id.id or False)], limit=1)
        invoice_lines = []
             
        invoice_line = []
        invoice_line.append((0, 0, {
                                        'name': self.internal_training_id.name,
                                        'account_id': self.name.property_account_payable.id,
                                        # 'unit_type': obj.unit_type,
                                        'price_unit': self.present_expense,
                                        # 'quantity': obj.continuity_time,
                                        # 'price_subtotal': obj.total_price,
                                        
        }))
            
        sch_invoice = invoice.create({
                                    'account_id': self.name.property_account_payable.id,
                                    'type': 'in_invoice',
                                    'partner_id': self.name.id,
                                    # 'currency_id': currency_id,
                                    # 'journal_id': len(journal_ids) and journal_ids[0] or False,
                                    # 'date_invoice': obj.executed_time,
                                    # 'fiscal_position': fpos and fpos.id,
                                    'company_id': self.name.property_account_payable.company_id.id,
                                    'invoice_line': invoice_line,
                                    # 'date_due': obj.executed_time,
                                    # 'comment': obj.description,
                                     })
        self.write({'state': 'invoiced',
                                  'invoice_id': sch_invoice})
        return True   
    
    def unlink(self):
        line_obj = self.env['execution.line']
        for line in self.execution_line_id:
            line_obj.unlink(line.id)
        return super(training_execution, self).unlink()
    
#===============================================================================
#     
# class execution_line(osv.osv):
#     _name = 'execution.line'
#     _columns = {
#                'name':fields.char('Last name'),
#                'department_name':fields.many2one('hr.department', 'Department'),
#                'position_name':fields.many2one('hr.job', 'Position'),
#                'training_execution_id':fields.many2one('training.execution', 'Training execution'),
#                'hr_employee_id':fields.many2one('hr.employee', 'Hr employee'),
#                }
#     
#     def onchange_execution_line(self, cr, uid, ids, empl_id, context=None):
#         res = {}
#         if empl_id:
#             empl_obj = self.pool.get('hr.employee')
#             empl = empl_obj.browse(cr, uid, empl_id)[0]
#             res['name'] = u'%s' % empl.last_name
#             res['department_name'] = empl.department_id.id
#             res['position_name'] = empl.job_id.id
#             return {'value':res}
# 
#                
#===============================================================================





