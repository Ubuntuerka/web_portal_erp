from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree

DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

class EmployeeWorkTimeChangeRegister(models.Model):
    _name = 'employee.work.time.change.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _order = 'name desc'

    state = fields.Selection([('draft', u'Ноорог'),
                              ('confirm', u'Батлагдсан'),
                              ('return', u'Буцаах'),
                              ], string='Төлөв', default='draft', tracking=True)
    company_id = fields.Many2one('res.company', 'Компани', required=True)
    department_id = fields.Many2one('hr.department', 'Хэлтэс')
    employee_id = fields.Many2one('hr.employee', 'Ажилтны нэр',required=True)
    resource_calendar_id = fields.Many2one('resource.calendar', 'Ажлын цаг')
    start_date = fields.Date(string=u'Эхлэх огноо')
    new_work_time = fields.Many2one('resource.calendar', 'Шинэ ажлын цаг', required=True)
    rec_user_id = fields.Many2one('res.users', 'Бүртгэсэн ажилтан', default=lambda self: self.env.user, required=True,
                                  readonly=True)
    reg_date = fields.Datetime(string=u'Бүртгэсэн огноо', required=True, default=lambda self: fields.datetime.now(), readonly=True)
    name = fields.Char(string='Хүсэлтийн дугаар',readonly = True, default = lambda self: self.env['ir.sequence'].next_by_code('employee.work.time.change.register'))

    # @api.onchange('department_id')
    # def onchange_department(self):
    #     if self.department_id:
    #         self.company_id = self.department_id.company_id

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        for line in self:
            line.resource_calendar_id = line.employee_id.resource_calendar_id

    def action_done(self):
        for record in self:
            if record.new_work_time != record.employee_id.resource_calendar_id:
                record.employee_id.write({'resource_calendar_id': record.new_work_time.id})
            record.write({'state': 'confirm'})

    def action_return(self):
        self.write({'state': 'draft'})