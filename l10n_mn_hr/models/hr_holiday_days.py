# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError, UserError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import da
from odoo.osv import expression
from pytz import timezone, UTC

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


_logger = logging.getLogger(__name__)



class hr_holiday_days(models.Model):
    _name = 'hr.holiday.days'
    _description = 'HR holiday days'        #Улсад ажилласан жилийн нэмэгдэл
    
    name = fields.Char('Name')#Нэр
    min_year = fields.Integer('Min year', required=True)#Улсад ажилласан жилийн доод хязгаар
    max_year = fields.Integer('Max year', required=True)#Улсад ажилласан жилийн дээд хязгаар
    get_days = fields.Integer('Get days', required=True)#Нэмэх боломжтой хоног
    base_days = fields.Integer('Base days', required=True)#Амрах үндсэн хоног
 
    
class HrLeaveType(models.Model):
    _inherit = "hr.leave.type"
    #_order = "position"
    #_description = 'Information on a employee'
    more_finger_time = fields.Many2one('hr.leave.type',string = 'Хуруу даралт')
    new_type = fields.Selection([
                        ('legal_leave', 'Legal leaves'),#Ээлжийн амралт
                        ('sick', 'Sick'),#Өвчний чөлөө
                        ('out_work', 'Out work'),#Гадуур ажил
                        ('payroll_half', 'Payroll half'),#Цалинтай чөлөө (50%)
                        ('payroll', 'Payroll'),#Цалинтай чөлөө
                        ('unpayroll', 'Unpayroll'),#Цалингүй чөлөө
                        ('public_holiday', 'Public holiday'),#Тэмдэглэлт баярын өдөр
                        ('no_finger','No finger'),
                        ('more_time','More time'),# Илүү цаг
                        ('work','Ажлын цаг'),
                        ('assignment','Томилолт'),
                        ('training','Сургалт'),
                        ('family_day','Гэр бүлийн өдөр'),
                        ('patch_day','Нөхөн амралт'),
                        ('sul_zogsolt','Сул зогсолт'),
                        ('online','Гэрээс ажилласан'),
                        ('more_times2', u'Шөнийн илүү цаг'),#Шөнийн илүү цаг
                    ], string='New type', required=True)#Төрөл

class HolidaysAllocation(models.Model):
    _inherit = "hr.leave.allocation"
    
    
    @api.model
    def create(self, values):
        """ Override to avoid automatic logging of creation """
        if values.get('allocation_type', 'regular') == 'accrual':
            values['date_from'] = fields.Datetime.now()
        employee_id = values.get('employee_id', False)
        date_start = fields.Datetime.now()
        req_day = values.get('number_of_days_display')
        emp_id2 = self.env['hr.employee'].search([('id','=',employee_id)])
        holiday_day_range = self.env['hr.holiday.days'].search([('min_year','<=',emp_id2.seniority),('max_year','>=',emp_id2.seniority)])
        if values.get('holiday_status_id'):
            hs_id = values.get('holiday_status_id')
        else:
            hs_id = self.holiday_status_id.id
        holiday_status_id = self.env['hr.leave.type'].search([('id','=',hs_id)])
        if holiday_status_id.new_type == 'legal_leave':
            legal_type_id = self.env['hr.leave.type'].search([('new_type','=','legal_leave')])
        year_start = datetime.strptime(str(date_start), DATETIME_FORMAT).replace(month=1,day=1)
        year_end = datetime.strptime(str(date_start), DATETIME_FORMAT).replace(month=12,day=31)
        #Ene jild amarsan eeljiin amraltuudiig shvvj awah
        this_year_approved_legal_leave = self.env['hr.leave'].search([('holiday_status_id','=',legal_type_id.id),
                                                                       ('holiday_type','=','employee'),
                                                                       ('employee_id','=',employee_id),
                                                                       ('state','=','validate'),
                                                                       ('request_date_from','>=',year_start.date()),
                                                                       ('request_date_to','<=',year_end.date())
                                                                       ])
        #Ene jild amarsan eeljiin amraltiin honoguudiig oloh
        this_year_took_day_sum = 0
        if this_year_approved_legal_leave:
            for find_sum in this_year_approved_legal_leave:
                this_year_took_day_sum = this_year_took_day_sum + find_sum.number_of_days_display
        
        if holiday_day_range:
            total_available = holiday_day_range.base_days + holiday_day_range.get_days
            remaining_available = total_available - this_year_took_day_sum
            if remaining_available < req_day:
                raise ValidationError(_("Амрах боломжтой ээлжийн амралтын хоног хүрэлцэхгүй байна. Энэ жилд амрах боломжтой нийт хоног '%s'. Энэ жилд амарсан нийт хоног '%s'. Энэ жилд амрах боломжтой нийт үлдсэн хоног '%s'. Хүсэлт гаргаж буй хоног '%s' !") %(total_available,this_year_took_day_sum,remaining_available,req_day))
        else:
            raise ValidationError(_("Ажилласан жилээс хамаарч нэмэлтээр олгодог Ээлжийн амралтын нэмэлт хоногийн тохиргоонд %s жил ажилласан гэсэн тохиргоо алга!!! Та Менежертээ хандан энэхүү тохиргоог нэмүүлсний дараагаар хадгалах боломжтой !") %emp_id2.seniority)

        if not values.get('department_id'):
            values.update({'department_id': self.env['hr.employee'].browse(employee_id).department_id.id})
        holiday = super(HolidaysAllocation, self.with_context(mail_create_nosubscribe=True)).create(values)
        holiday.add_follower(employee_id)
        if holiday.validation_type == 'hr':
            holiday.message_subscribe(partner_ids=(holiday.employee_id.parent_id.user_id.partner_id | holiday.employee_id.leave_manager_id.partner_id).ids)
        if not self._context.get('import_file'):
            holiday.activity_update()
        return holiday



class HolidaysRequest(models.Model):
    _inherit = "hr.leave"
    
    
    company_id = fields.Many2one('res.company', string='Company of Employee', states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
    more_finger_time = fields.Many2one('hr.leave.type',string = 'Хуруу даралт')
    request_from = fields.Char(string=u'Орсон',compute="calculate_more_time",default='')
    request_to = fields.Char(string=u'Гарсан',compute="calculate_more_time",default='')
    
    
    def calculate_more_time(self):
        
        
        for line in self:
            att_obj = self.env['hr.leave']
            if line.holiday_status_id.new_type == 'more_time':
                request_from = line.date_from.replace(hour=0, minute=0, second=1) 
                request_to = line.date_from.replace(hour=23, minute=59, second=59) 
                work_time = att_obj.search([('employee_id','=',line.employee_id.id),('date_from','>=',request_from),('date_to','<=',request_to),('holiday_status_id','=',36)])
                if work_time:
                    line.request_from = datetime.strptime(str(work_time.date_from), DATETIME_FORMAT) + timedelta(hours=8)
                    line.request_to = datetime.strptime(str(work_time.date_to), DATETIME_FORMAT) + timedelta(hours=8)
                else:
                    line.request_from = ''
                    line.request_to = ''
                    
            else:
                 line.request_from = ''
                 line.request_to = ''
    
    @api.constrains('date_from', 'date_to', 'state', 'employee_id','holiday_status_id')
    def _check_date(self):
        domains = [[
            ('holiday_status_id', '!=', 36),
            ('date_from', '<', holiday.date_to),
            ('date_to', '>', holiday.date_from),
            ('employee_id', '=', holiday.employee_id.id),
            ('id', '!=', holiday.id),
        
        ] for holiday in self.filtered('employee_id')]
        domain = expression.AND([
            [('state', 'not in', ['cancel', 'refuse']),('holiday_status_id', '!=', 36)],
            
            expression.OR(domains)
        ])
        
        #if self.search_count(domain):
           # raise ValidationError(_('You can not set 2 times off that overlaps on the same day for the same employeew.'))
    
    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        self._sync_employee_details()
        if self.employee_id.user_id != self.env.user and self._origin.employee_id != self.employee_id:
            self.holiday_status_id = False
        if self.employee_id.company_id:
            self.company_id = self.employee_id.company_id.id
        else:
            self.company_id = False
    
    @api.model_create_multi
    def create(self, vals_list):
        """ Override to avoid automatic logging of creation """
        if not self._context.get('leave_fast_create'):
            leave_types = self.env['hr.leave.type'].browse([values.get('holiday_status_id') for values in vals_list if values.get('holiday_status_id')])
            mapped_validation_type = {leave_type.id: leave_type.validation_type for leave_type in leave_types}

            for values in vals_list:
                employee_id = values.get('employee_id', False)
                leave_type_id = values.get('holiday_status_id')
                # Handle automatic department_id
                if not values.get('department_id'):
                    values.update({'department_id': self.env['hr.employee'].browse(employee_id).department_id.id})

                # Handle no_validation
                if mapped_validation_type[leave_type_id] == 'no_validation':
                    values.update({'state': 'confirm'})

                # Handle double validation
                if mapped_validation_type[leave_type_id] == 'both':
                    self._check_double_validation_rules(employee_id, values.get('state', False))

                # Handle legal_leave tb
                if values.get('holiday_status_id'):
                    holiday_status_id = self.env['hr.leave.type'].search([('id','=',values.get('holiday_status_id'))])
                    print ('JJJJJJJJ00000', values.get('holiday_status_id'), values.get('employee_id'), values.get('request_date_from'), values.get('request_date_to'), holiday_status_id.new_type, values.get('number_of_days'))
                    #Eeljiin amralt bwal
                    if holiday_status_id.new_type == 'legal_leave':
                        legal_type_id = self.env['hr.leave.type'].search([('new_type','=','legal_leave')])
                        year_start = datetime.strptime(str(values.get('request_date_from')), DATE_FORMAT).replace(month=1,day=1)
                        year_end = datetime.strptime(str(values.get('request_date_from')), DATE_FORMAT).replace(month=12,day=31)
                        req_day = values.get('number_of_days')
                        print ('QQQQQQQQ', year_start.date(), year_end.date() )
                        #Ene jild amarsan eeljiin amraltuudiig shvvj awah
                        this_year_approved_legal_leave = self.env['hr.leave'].search([('holiday_status_id','=',legal_type_id.id),
                                                                                       ('holiday_type','=','employee'),
                                                                                       ('employee_id','=',values.get('employee_id')),
                                                                                       ('state','=','validate'),
                                                                                       ('request_date_from','>=',year_start.date()),
                                                                                       ('request_date_to','<=',year_end.date())
                                                                                       ])
                        #Ene jild amarsan eeljiin amraltiin honoguudiig oloh
                        this_year_took_day_sum = 0
                        if this_year_approved_legal_leave:
                            for find_sum in this_year_approved_legal_leave:
                                this_year_took_day_sum = this_year_took_day_sum + find_sum.number_of_days
                        
                        #Tuhain ajiltnii odoo awah bolomjtoi eeljiin amraltiin honogiig oloh
                        emp_id = self.env['hr.employee'].search([('id','=',values.get('employee_id'))])
                        if emp_id:
                            emp_id.working_year_other2()
                            emp_id.get_seniority2()
                        
        holidays = super(HolidaysRequest, self.with_context(mail_create_nosubscribe=True)).create(vals_list)

        for holiday in holidays:
            if self._context.get('import_file'):
                holiday._onchange_leave_dates()
            if not self._context.get('leave_fast_create'):
                # FIXME remove these, as they should not be needed
                if employee_id:
                    holiday.with_user(SUPERUSER_ID)._sync_employee_details()
                if 'number_of_days' not in values and ('date_from' in values or 'date_to' in values):
                    holiday.with_user(SUPERUSER_ID)._onchange_leave_dates()

                # Everything that is done here must be done using sudo because we might
                # have different create and write rights
                # eg : holidays_user can create a leave request with validation_type = 'manager' for someone else
                # but they can only write on it if they are leave_manager_id
                holiday_sudo = holiday.sudo()
                holiday_sudo.add_follower(employee_id)
                if holiday.validation_type == 'manager':
                    holiday_sudo.message_subscribe(partner_ids=holiday.employee_id.leave_manager_id.partner_id.ids)
                if holiday.holiday_status_id.validation_type == 'no_validation':
                    # Automatic validation should be done in sudo, because user might not have the rights to do it by himself
                    holiday_sudo.action_validate()
                    holiday_sudo.message_subscribe(partner_ids=[holiday_sudo._get_responsible_for_approval().partner_id.id])
                    holiday_sudo.message_post(body=_("The time off has been automatically approved"), subtype="mt_comment") # Message from OdooBot (sudo)
                elif not self._context.get('import_file'):
                    holiday_sudo.activity_update()
        return holidays
    
    def write(self, values):
        is_officer = self.env.user.has_group('hr_holidays.group_hr_holidays_user')
        #is_reviewer = self.env.user.has_group('hr_holidays.group_hr_holidays_responsible')

        if not is_officer and values.keys() - {'message_main_attachment_id'}:
            if any(hol.date_from.date() < fields.Date.today() and hol.employee_id.leave_manager_id != self.env.user for hol in self):
                raise UserError(_('You must have manager rights to modify/validate a time off that already begun'))
            

        
        # Handle legal_leave tb
        if values.get('number_of_days'):
            req_day = values.get('number_of_days')
            if values.get('holiday_status_id'):
                hs_id = values.get('holiday_status_id')
            else:
                hs_id = self.holiday_status_id.id
            holiday_status_id = self.env['hr.leave.type'].search([('id','=',hs_id)])
            #Eeljiin amralt bwal
            if holiday_status_id.new_type == 'legal_leave':
                legal_type_id = self.env['hr.leave.type'].search([('new_type','=','legal_leave')])
                if values.get('request_date_from'):
                    date_start = values.get('request_date_from')
                else:
                    date_start = self.request_date_from
                if values.get('request_date_to'):
                    date_end = values.get('request_date_to')
                else:
                    date_end = self.request_date_to
                if values.get('employee_id'):
                    empl_id = values.get('employee_id')
                else:
                    empl_id = self.employee_id.id
                    
                year_start = datetime.strptime(str(date_start), DATE_FORMAT).replace(month=1,day=1)
                year_end = datetime.strptime(str(date_start), DATE_FORMAT).replace(month=12,day=31)
                #Ene jild amarsan eeljiin amraltuudiig shvvj awah
                this_year_approved_legal_leave = self.env['hr.leave'].search([('holiday_status_id','=',legal_type_id.id),
                                                                               ('holiday_type','=','employee'),
                                                                               ('employee_id','=',empl_id),
                                                                               ('state','=','validate'),
                                                                               ('request_date_from','>=',year_start.date()),
                                                                               ('request_date_to','<=',year_end.date())
                                                                               ])
                #Ene jild amarsan eeljiin amraltiin honoguudiig oloh
                this_year_took_day_sum = 0
                if this_year_approved_legal_leave:
                    for find_sum in this_year_approved_legal_leave:
                        this_year_took_day_sum = this_year_took_day_sum + find_sum.number_of_days
                
                #Tuhain ajiltnii odoo awah bolomjtoi eeljiin amraltiin honogiig oloh
                emp_id = self.env['hr.employee'].search([('id','=',empl_id)])
                if emp_id:
                    emp_id.working_year_other2()
                    emp_id.get_seniority2()
                emp_id2 = self.env['hr.employee'].search([('id','=',empl_id)])
                holiday_day_range = self.env['hr.holiday.days'].search([('min_year','<=',emp_id2.seniority),('max_year','>=',emp_id2.seniority)])
                
                #if holiday_day_range:
                #    total_available = holiday_day_range.base_days + holiday_day_range.get_days
                #    remaining_available = total_available - this_year_took_day_sum
                #    if remaining_available < req_day:
                #        raise ValidationError(_("Амрах боломжтой ээлжийн амралтын хоног хүрэлцэхгүй байна. Энэ жилд амрах боломжтой нийт хоног '%s'. Энэ жилд амарсан нийт хоног '%s'. Энэ жилд амрах боломжтой нийт үлдсэн хоног '%s'. Хүсэлт гаргаж буй хоног '%s' !") %(total_available,this_year_took_day_sum,remaining_available,req_day))
                #else:
                #    raise ValidationError(_("Ажилласан жилээс хамаарч нэмэлтээр олгодог Ээлжийн амралтын нэмэлт хоногийн тохиргоонд %s жил ажилласан гэсэн тохиргоо алга!!! Та Менежертээ хандан энэхүү тохиргоог нэмүүлсний дараагаар хадгалах боломжтой !") %emp_id2.seniority)

        employee_id = values.get('employee_id', False)
        if not self.env.context.get('leave_fast_create'):
            if values.get('state'):
                self._check_approval_update(values['state'])
                if any(holiday.validation_type == 'both' for holiday in self):
                    if values.get('employee_id'):
                        employees = self.env['hr.employee'].browse(values.get('employee_id'))
                    else:
                        employees = self.mapped('employee_id')
                    self._check_double_validation_rules(employees, values['state'])
            if 'date_from' in values:
                values['request_date_from'] = values['date_from']
            if 'date_to' in values:
                values['request_date_to'] = values['date_to']
        result = super(HolidaysRequest, self).write(values)
        if not self.env.context.get('leave_fast_create'):
            for holiday in self:
                if employee_id:
                    holiday.add_follower(employee_id)
                    self._sync_employee_details()
                if 'number_of_days' not in values and ('date_from' in values or 'date_to' in values):
                    holiday._onchange_leave_dates()
        return result
    
    
