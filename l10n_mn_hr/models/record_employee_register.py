 # -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from datetime import timedelta
from datetime import datetime
from lxml import etree
# import socket
# from . import tls_socket


class RecordEmployeeRegister(models.Model):
    _name = 'record.employee.register'

    name = fields.Char('Register number')
    company_id = fields.Many2one('res.company', string='Компани')
    employee_id = fields.Many2one('hr.employee', 'Ажилтан', copy=False)
    user_id = fields.Many2one('res.users', 'Ажилтан', default=lambda self: self.env.user, required=True, readonly=True)
    department_id = fields.Many2one('hr.department', 'Хэлтэс')
    job_title = fields.Many2one('job.task', 'Ажлын байрны нэр')
    job_id = fields.Many2one('hr.job', string=u'Албан тушаал')
    line_ids = fields.One2many('record.employee.register.line', 'register_id', string='register line')
    state = fields.Selection([('draft', 'Ноорог'),  # Ноорог
                              ('send', 'Илгээсэн'),  # Илгээх
                              ('confirm', 'Батлагдсан'),  # Батлах
                              ], 'Төлөв', default='draft')  # Төлөв

    def action_send(self):
        task_obj = self.env['job.task'].search([('id', '=', self.job_title.id)])
        for task in task_obj:
            for line in task.line_ids:
                self.env['record.employee.register.line'].create({
                    'register_id': self.id,
                    'work_task': line.work_task.id,
                    'support_employee': line.support_employee,
                    'one_week': line.one_week,
                    'two_week': line.two_week,
                    'three_week': line.three_week,
                    'four_week': line.four_week,
                    'five_week': line.five_week,
                    'six_week': line.six_week,
                })
        self.write({'state': 'send'})
        return True


    # def action_data(self):
    #     # existing_lines = self.line_ids
    #     # if existing_lines:
    #     #     raise UserError("Мэдээлэл аль хэдийн татагдсан байна.")
    #
    #     task_obj = self.env['job.task'].search([('id', '=', self.job_title.id)])
    #     for task in task_obj:
    #         for line in task.line_ids:
    #             self.env['record.employee.register.line'].create({
    #                 'register_id': self.id,
    #                 'work_task': line.work_task.id,
    #                 'support_employee': line.support_employee,
    #                 'one_week': line.one_week,
    #                 'two_week': line.two_week,
    #                 'three_week': line.three_week,
    #                 'four_week': line.four_week,
    #                 'five_week': line.five_week,
    #                 'six_week': line.six_week,
    #             })
    #     return True

    def action_return(self):
        self.write({'state': 'send'})

    # def action_check(self):
    #     self.write({'state': 'check'})

    def action_confirm(self):
        self.write({'state': 'confirm'})
    
    @api.onchange('employee_id')
    def onchange_employee_id(self):
        for line in self:
            line.department_id = line.employee_id.department_id.id
            line.job_id = line.employee_id.job_id.id
            line.company_id = line.employee_id.company_id.id

    class RecordEmployeeRegisterLine(models.Model):
        _name = 'record.employee.register.line'

        register_id = fields.Many2one('record.employee.register', string='line_ids')
        work_task = fields.Many2one('record.employee.list', string='Хийгдэх ажлууд')
        support_employee = fields.Char('Дэмжлэг үүлзүүлэгч ажилтан')
        one_week = fields.Integer('I-р 7 хоног')
        two_week = fields.Integer('II-р 7 хоног')
        three_week = fields.Integer('III-р 7 хоног')
        four_week = fields.Integer('IV-р 7 хоног')
        five_week = fields.Integer('V-р 7 хоног')
        six_week = fields.Integer('VI-р 7 хоног')

        # @api.constrains('work_task')
        # def _check_unique_work_task(self):
        #     for record in self:
        #         # Check if the work_task is already assigned to another line in the same register
        #         existing_record = self.env['record.employee.register.line'].search([
        #             ('work_task', '=', record.work_task.id)
        #         ], limit=1)
        #         if existing_record and existing_record != record:
        #             raise ValidationError("This work task is already assigned to another line in this register.")

    # class RecordEmployeeListJob(models.Model):
    #     _name = 'record.employee.list.job'
    #
    #     work_task = fields.Many2one('record.employee.list', string='Хийгдэх ажлууд')
    #     support_employee = fields.Char('Дэмжлэг үүлзүүлэгч ажилтан')


