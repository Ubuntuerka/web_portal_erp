# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import da

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

_logger = logging.getLogger(__name__)

class supply_bonus(models.Model):
    _name = 'supply.bonus'
    _inherit = ['mail.thread']
    _description = 'Supply bonus'       #Хөнгөлөлт, хангамж, тэтгэмж
    
    name = fields.Char('Last name')#Овог
    company_name = fields.Many2one('res.company', 'Company')#Харъяалагдах компани
    department_name = fields.Many2one('hr.department', 'Department')#Алба, хэлтэс
    position_name = fields.Many2one('hr.job', 'Position')#Албан тушаал
    type = fields.Selection([('discount', 'Discount'),#Хөнгөлөлт
                             ('supply', 'Supply'),#Хангамж
                             ('allowance', 'Allowance'),#Тэтгэмж
                             ('bonus', 'Bonus'),#Урамшуулал
                             ('payback', 'Payback')],#Нөхөн олговор
                              'Type', track_visibility='onchange')#Төрөл
    amount = fields.Float('Amount', required=True, track_visibility='onchange')#Олгосон мөнгөн дүн
    giving_date = fields.Date('Giving date', required=True, track_visibility='onchange')#Олгосон огноо
    description = fields.Text('Description', required=True)#Утга
    decree_id = fields.Many2one('hr.decree', 'Decree', required=True, track_visibility='onchange')#Тушаал
    hr_employee_id = fields.Many2one('hr.employee', 'First name', required=True, track_visibility='onchange')#Нэр
    supply_invoice = fields.Many2one('res.partner', 'Partner', required=True, track_visibility='onchange')#Нэхэмжлэгч
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.company)#Компани
    invoice_id = fields.Many2one('account.invoice', 'Invoice')#Нэхэмжлэл
    state = fields.Selection([('draft', 'Draft'),#
                             ('approved', 'Approved'),#
                             ('cancel', 'Cancel'),#
                             ],#Нөхөн олговор
                              'State', track_visibility='onchange', default='draft')#Төрөл
   
    def action_to_draft(self):
        self.write({'state': 'draft'})
        return True
    def action_to_approved(self):
        self.write({'state': 'approved'})
        return True
    def action_to_cancel(self):
        self.write({'state': 'cancel'})
        return True
    
    @api.onchange('hr_employee_id')
    def onchange_supply_bonus(self):
        res = {}
        if self.hr_employee_id:
            empl_obj = self.env['hr.employee']
            empl = empl_obj.browse(self.hr_employee_id.id)
            res['name'] = self.hr_employee_id.last_name
            res['company_name'] = self.hr_employee_id.company_id.id
            res['department_name'] = self.hr_employee_id.department_id.id
            res['position_name'] = self.hr_employee_id.job_id.id
            return {'value':res}
        
