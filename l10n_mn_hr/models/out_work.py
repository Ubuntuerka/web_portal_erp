# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import da

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

_logger = logging.getLogger(__name__)

class out_work(models.Model):
    _name = 'out.work'
    _inherit = ['mail.thread']
    #_inherit = ['portal.mixin', 'mail.alias.mixin', 'mail.thread', 'rating.parent.mixin']
    _description = 'Out work' # Томилолтын төлөвлөгөө
    _is_mail_activity = True
    
    name = fields.Char('Purpose', required=True, track_visibility='onchange')#Ажлын зорилго
    site_name = fields.Char('Site name')#Ажилласан газрын нэр
    approval_date = fields.Date('Approval date', track_visibility='onchange')#Баталсан огноо
    out_time = fields.Date('Out time', track_visibility='onchange')#Явах огноо
    in_time = fields.Date('In time', track_visibility='onchange')#Ирэх огноо
    guideline = fields.Char('Guideline', required=True, track_visibility='onchange')#Удирдамж
    day = fields.Integer('Day', track_visibility='onchange')#Хоногийн тоо
    country = fields.Many2one('res.country', 'Country', track_visibility='onchange')#Ямар улс
    city = fields.Many2one('res.country.state', 'City', track_visibility='onchange')#Хот
    authority_sign = fields.Many2one('res.partner', 'Authority sign', track_visibility='onchange')#
    budget = fields.Float('Budget', track_visibility='onchange')#Анхны төсөв
    is_plan = fields.Selection([('no', 'No'), ('yes', 'Yes')], 'Is plan', track_visibility='onchange')#Төлөвлөгөөт эсэх
    #work_line_id = fields.One2many('work.line', 'out_work_id', 'Work line', track_visibility='onchange')#Хамрагдагсдын хэсэг
    work_line_ids = fields.Many2many('hr.employee', 'hr_out_work_id', 'out_work_hr_rel', 'out_work_id', 'EmpOutWork', track_visibility='onchange')#Хамрагдах ажилчдийн хэсэг
    
    decree_id = fields.Many2one('hr.decree', 'Decree', track_visibility='onchange')#Тушаал
    state = fields.Selection([
        ('draft', 'Draft'),#Ноорог
        ('cancel', 'Cancel'),#Цуцлагдсан
        ('approve', 'Approve'),#Батлагдсан
        ], 'State', track_visibility='onchange', default='draft')#Төлөв
    company_id = fields.Many2one('res.company', 'Company', required=True, track_visibility='onchange', default=lambda self: self.env.company)#Харъяалагдах компани

    def action_work_draft(self):
        self.write({'state': 'draft'})
        return True

    def action_work_cancel(self):
        self.write({'state': 'cancel'})
        return True
    
    def action_work_approve(self):
        self.write({'state': 'approve'})
        
        msg = _("Batlagdsan medeelel %s ") % (self.state,)
        self.message_post(body=msg)
        #self._get_creation_message()
        
        #=======================================================================
        # MailThread = self.env['mail.thread']
        # values = {
        #     'parent_id': False,
        #     'model': self._name if self else False,
        #     'res_id': self.id if self else False,
        #     'message_type': 'user_notification',
        #     'subject': 'LLLLLL',
        #     'body': 'KKKKK',
        #     'author_id': False,
        #     'email_from': 'aaaa@aaa.com',
        #     'partner_ids': False,
        #     'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
        #     'record_name': False,
        #     #'reply_to': MailThread._notify_get_reply_to(default=email_from, records=None)[False],
        #     #'message_id': tools.generate_tracking_message_id('message-notify'),
        # }
        # #values.update(msg_kwargs)
        # new_message = MailThread._message_create(values)
        # MailThread._notify_thread(new_message, values, **notif_kwargs)
        #=======================================================================
        
    
    @api.onchange('in_time','out_time')
    def onchange_in_time(self):
        delta_day = 0.0
        day = '%Y-%m-%d'
        if self.in_time != False :
            if self.out_time == False:
                raise ValidationError(_('Error!'), _("You must fill the out_time"))
            else:
                a = datetime.strptime(str(self.out_time), day)
                b = datetime.strptime(str(self.in_time), day)
                timedelta = b - a
                delta_day = timedelta.days
                return {'value': {'day': delta_day, }}
        
    #===========================================================================
    # def unlink(self):
    #     line_obj = self.env['work.line']
    #     for line in self.work_line_id:
    #         line_obj.unlink(line.id)
    #     return super(out_work, self).unlink(self)
    #===========================================================================


    def message_subscribe(self, partner_ids=None, channel_ids=None, subtype_ids=None):
        """ Subscribe to all existing active tasks when subscribing to a project """
        res = super(out_work, self).message_subscribe(partner_ids=partner_ids, channel_ids=channel_ids, subtype_ids=subtype_ids)
        project_subtypes = self.env['mail.message.subtype'].browse(subtype_ids) if subtype_ids else None
        task_subtypes = project_subtypes.mapped('parent_id').ids if project_subtypes else None
        if not subtype_ids or task_subtypes:
            self.mapped('tasks').message_subscribe(
                partner_ids=partner_ids, channel_ids=channel_ids, subtype_ids=task_subtypes)
        return res

    def message_unsubscribe(self, partner_ids=None, channel_ids=None):
        """ Unsubscribe from all tasks when unsubscribing from a project """
        self.mapped('tasks').message_unsubscribe(partner_ids=partner_ids, channel_ids=channel_ids)
        return super(out_work, self).message_unsubscribe(partner_ids=partner_ids, channel_ids=channel_ids)
    
#===============================================================================
# class work_line(models.Model):
#     _name = 'work.line'
#     _description= 'Out work line'   #Томилолтын төлөвлөгөөний мөр
#     
#     name = fields.Char('Last name')#Нэр
#     department_name = fields.Many2one('hr.department', 'Department')#Алба хэлтэс
#     position_name = fields.Many2one('hr.job', 'Position')#Албан тушаал
#     out_work_id = fields.Many2one('out.work', 'Out work')#Томилолтын төлөвлөгөө
#     hr_employee_id = fields.Many2one('hr.employee', 'Hr employee')#Ажилтан
#     
#     @api.onchange('hr_employee_id')
#     def onchange_out_work(self):
#         res = {}
#         if self.hr_employee_id:
#             res['name'] = self.hr_employee_id.last_name
#             res['department_name'] = self.hr_employee_id.department_id.id
#             res['position_name'] = self.hr_employee_id.job_id.id
#             return {'value':res}
#===============================================================================

    
    @api.model
    def create(self, vals):
        
        is_headmans = self.env.user.has_group('l10n_mn_hr.group_department_headmans')
        is_headman = self.env.user.has_group('l10n_mn_hr.group_hr_headman')
        is_manager = self.env.user.has_group('hr.group_hr_manager')
        
        if not is_headmans:
            if not is_headman:
                if not is_manager:
                    raise ValidationError(_("Танд томилолт бүртгэх эрх байхгүй байна!"))
        
        out_id = super(out_work, self).create(vals)
        
        return out_id
    
    def write(self, values):
        
        is_headmans = self.env.user.has_group('l10n_mn_hr.group_department_headmans')
        is_headman = self.env.user.has_group('l10n_mn_hr.group_hr_headman')
        is_manager = self.env.user.has_group('hr.group_hr_manager')
        
        if not is_headmans:
            if not is_headman:
                if not is_manager:
                    if self.authority_sign != self.env.user.partner_id:
                        raise ValidationError(_("Танд томилолт засах эрх байхгүй байна!"))
                
        result = super(out_work, self).write(values)
        return result

class out_execution(models.Model):
    _name = 'out.execution'
    _inherit = ['mail.thread']
    _description = 'Out execution'  # Томилолтын төлөвлөгөөний биелэлт
    
    #===========================================================================
    # #@api.onchange('out_execution_line_ids')
    # def _count_parts(self):
    #      
    #     self.count_participants = len(self.out_execution_line_ids)
    #     #self.write({'count_participants': len(self.out_execution_line_ids)})
    # 
    #===========================================================================
    out_work_id = fields.Many2one('out.work', 'Purpose', required=True, track_visibility='onchange')#Ажлын зорилго
    name = fields.Char('Guideline', required=True)#Удирдамж
    site_name = fields.Char('Site name')#Ажилласан газрын нэр
    approval_date = fields.Date('Approval date', track_visibility='onchange')#Баталсан огноо
    out_time = fields.Date('Out time', track_visibility='onchange')#Явсан огноо
    in_time = fields.Date('In time', track_visibility='onchange')#Ирсэн огноо
    timeout = fields.Integer('Timeout', track_visibility='onchange')#Хугацааны хэмнэлт, хэтрэлт
    is_plan = fields.Selection([('no', 'No'), ('yes', 'Yes')], 'Is plan', track_visibility='onchange')#Төлөвлөгөөт эсэх
    country = fields.Many2one('res.country', 'Country')#Ямар улс
    city = fields.Many2one('res.country.state', 'City')#Хот
    authority_sign = fields.Many2one('res.partner', 'Authority sign', required=True)#Хариуцагч
    budget = fields.Float('Budget', track_visibility='onchange')#Анхны төсөв
    core_cost = fields.Float('Core cost', track_visibility='onchange')#Үндсэн зардал
    other_cost = fields.Float('Other cost', track_visibility='onchange')#Бусад зардал
    real_cost = fields.Float('Real cost', track_visibility='onchange')#Нийт зардал
    variation_cost = fields.Float('Variation cost', track_visibility='onchange')#Төсвийн зөрүү
    invoice_id = fields.Many2one('account.invoice', 'Invoice')#Нэхэмжлэх
    company_id = fields.Many2one('res.company', 'Company', track_visibility='onchange')#Харъяалагдах компани
    planned_day = fields.Integer('Planned day')#Төлөвлөсөн хоног
    count_participants = fields.Integer(track_visibility='onchange', string='Count participants', )#Хамрагдагсдын тоо
    #out_execution_line_id = fields.One2many('out.execution.line', 'out_execution_id', 'Out execution', track_visibility='onchange')#Хамрагдагсдын хэсэг
    out_execution_line_ids = fields.Many2many('hr.employee', 'hr_out_execution_id', 'out_execution_hr_rel', 'out_execution_id', 'EmpOutExecution', track_visibility='onchange')#Хамрагдах ажилчдийн хэсэг
    decree_id = fields.Many2one('hr.decree', 'Decree', track_visibility='onchange')#Тушаал
    state = fields.Selection([
        ('draft', 'Draft'),#Ноорог
        ('invoiced', 'Invoiced'),#Нэхэмжилсэн
        ('exec_done', 'Execution done'),#Дууссан
        ], 'State', track_visibility='onchange', default='draft')#Төлөв
    
    @api.model
    def create(self, vals):
        
        is_headmans = self.env.user.has_group('l10n_mn_hr.group_department_headmans')
        is_headman = self.env.user.has_group('l10n_mn_hr.group_hr_headman')
        is_manager = self.env.user.has_group('hr.group_hr_manager')
        
        if not is_headmans:
            if not is_headman:
                if not is_manager:
                    raise ValidationError(_("Танд томилолт бүртгэх эрх байхгүй байна!"))
                
        if 'out_execution_line_id' in vals:
            vals['count_participants'] = len(vals['out_execution_line_id'])
        out_execution_id = super(out_execution, self).create(vals)
        return out_execution_id

    def write(self, vals):
        
        is_headmans = self.env.user.has_group('l10n_mn_hr.group_department_headmans')
        is_headman = self.env.user.has_group('l10n_mn_hr.group_hr_headman')
        is_manager = self.env.user.has_group('hr.group_hr_manager')
        
        if not is_headmans:
            if not is_headman:
                if not is_manager:
                    if self.authority_sign != self.env.user.partner_id:
                        raise ValidationError(_("Танд томилолт засах эрх байхгүй байна!"))
                    
        if 'out_execution_line_id' in vals:
            vals['count_participants'] = len(vals['out_execution_line_id'])
        return super(out_execution, self).write(vals)
    
    def recompute_counts(self):
        self.write({'count_participants': len(self.out_execution_line_ids)})
        return True
    
    @api.onchange('out_work_id')
    def onchange_work(self):
        res = {}
        list = []
        if self.out_work_id:
            work = self.out_work_id
            res['name'] = work.guideline
            res['site_name'] = work.site_name
            res['is_plan'] = work.is_plan
            res['approval_date'] = work.approval_date
            res['planned_day'] = work.day
            res['country'] = work.country
            res['city'] = work.city
            res['authority_sign'] = work.authority_sign
            res['budget'] = work.budget
            res['company_id'] = work.company_id
            res['decree_id'] = work.decree_id
            if work.work_line_ids:
                res['out_execution_line_ids'] = work.work_line_ids
            else:
                res['out_execution_line_ids'] = False
            return {'value':res}
    
    @api.onchange('core_cost','other_cost')
    def onchange_cost(self):
       if self.core_cost >= 0 and self.other_cost >= 0:
           sum = self.core_cost + self.other_cost
           return {'value': {'real_cost': sum}}
    
    @api.onchange('budget','real_cost')
    def onchange_variation(self):
       if self.budget >= 0 and self.real_cost >= 0:
           sum = self.budget - self.real_cost
           return {'value': {'variation_cost': sum}}
    
    @api.onchange('out_time','in_time','planned_day')
    def onchange_timeout(self):
        delta = 0.0
        day = '%Y-%m-%d'
        if self.in_time != False :
            if self.out_time == False:
                raise ValidationError(_('Error!'), _("You must fill the out_time"))
            else:
                a = datetime.strptime(str(self.out_time), day)
                b = datetime.strptime(str(self.in_time), day)
                timedelta = b - a
                delta = self.planned_day - timedelta.days
                return {'value': {'timeout': delta}}
    
    def action_draft(self):
        self.write({'state': 'draft'})
        return True   
    
    def action_done(self):
        self.write({'state': 'exec_done'})
        return True   
    
    def action_create_invoice(self):
        invoice = self.env['account.invoice']
        journal_obj = self.env['account.journal']
       
        currency_id = obj.company_id.currency_id.id
        journal_ids = journal_obj.search([('type', '=', 'purchase'), ('company_id', '=', obj.company_id.id or False)], limit=1)
        invoice_lines = []
             
        invoice_line = []
        invoice_line.append(({
                                'name': obj.out_work_id.name,
                                'account_id': obj.authority_sign.property_account_payable.id,
                                'price_unit': obj.real_cost,
        }))
            
        sch_invoice = invoice.create({
                                    'account_id': obj.authority_sign.property_account_payable.id,
                                    'type': 'in_invoice',
                                    'partner_id': obj.authority_sign.id,
                                    'currency_id': currency_id,
                                    'journal_id': len(journal_ids) and journal_ids[0] or False,
                                    'company_id': obj.company_id.id,
                                    'invoice_line': invoice_line,
                                     })
        self.write({'state': 'invoiced',
                    'invoice_id': sch_invoice})
        return True   
        
    #===========================================================================
    # def unlink(self):
    #     line_obj = self.env['out.execution.line']
    #     for line in self.out_execution_line_id:
    #         line_obj.unlink(line.id)
    #     return super(out_execution, self).unlink(self)
    #===========================================================================

        
#===============================================================================
# class out_execution_line(models.Model):
#     _name = 'out.execution.line'
#     _description = 'Out execution line' # Томилолтын төлөвлөгөөний биелэлтийн мөр
#     
#     name = fields.Char('Last name')#Овог
#     department_name = fields.Many2one('hr.department', 'Department')#Алба, хэлтэс
#     position_name = fields.Many2one('hr.job', 'Position')#Албан тушаал
#     out_execution_id = fields.Many2one('out.execution', 'Out execution')#Хамрагдагсдын хэсэг
#     hr_employee_id = fields.Many2one('hr.employee', 'Hr employee')#Нэр
#     
#     @api.model
#     def create(self, vals):
#         out_execution_line_id = super(out_execution_line, self).create(vals)
#         return out_execution_line_id
#     
#     @api.onchange('hr_employee_id')
#     def onchange_out_execution(self):
#         res = {}
#         if self.hr_employee_id:
#             empl = self.hr_employee_id
#             res['name'] = empl.last_name
#             res['department_name'] = empl.department_id.id
#             res['position_name'] = empl.job_id.id
#             return {'value':res}
#===============================================================================

        
