import base64
import logging

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY
from odoo import api, fields, models, SUPERUSER_ID, tools

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import af

from odoo.modules.module import get_module_resource
from odoo.tools.translate import _
from calendar import month, day_abbr, day_name
from dateutil import rrule
import math
from operator import countOf






class MedicalExamination(models.Model):
    _name = 'medical.examination'
    _description = 'Medical examination'
    _inherit = ['mail.thread']

   
    name = fields.Char('Last name')
    identification_id = fields.Char(string='Register number')
    age = fields.Integer(string='Age')
    gender = fields.Selection([('male','Male'),
                            ('female','Female')],string='Gender')
    company_name = fields.Many2one('res.company', 'Company')
    department_name = fields.Many2one('hr.department', 'Department')
    position_name = fields.Many2one('hr.job', 'Position')
    examination_date = fields.Date('Examination date', track_visibility='onchange')
    end_date = fields.Date('End date', track_visibility='onchange')
    #medical_examination_id = fields.Many2one('medical.examination.type', 'Medical examination type')
    medical_examination_id = fields.Many2many(
        'medical.examination.type', 'medical_examination_rel',
        'medical_id', 'category_id',
        string='Tags')
    description = fields.Text('Description', track_visibility='onchange')
    hr_employee_id = fields.Many2one('hr.employee', 'First name', track_visibility='onchange')
    #===========================================================================
    # weight = fields.Char('Weight')
    # high = fields.Char('High')
    # blood_pressure_left= fields.Char('Blood pressure left')medical_id')
    # pulse = fields.Char('Pulse')
    #===========================================================================
    state = fields.Selection([
                            ('draft', 'Draft'),
                            ('cancel', 'Cancel'),
                            ('approve', 'Approve'),
                            ], string='State',
                             track_visibility='onchange',  default = 'draft')
    


    def action_to_draft(self):
        self.write({'state': 'draft'})
        return True

    def action_to_cancel(self):
        self.write({'state': 'cancel'})
        return True
    
    
    def action_to_approve(self):
        self.write({'state': 'approve'})
       
        return True
    

    @api.onchange('examination_date')
    def onchange_date(self):
        res = {}
        startDate = self.examination_date
        if self.examination_date:   
            enddate = date(startDate.year + 1, startDate.month, startDate.day)
            res['end_date'] = enddate
            return {'value':res}
        
    @api.onchange('hr_employee_id')
    def onchange_movement(self):
        res = {}
        #if empl_id:
         #   empl_obj = self.env['hr.employee']
          #  empl = empl_obj.browse(self.empl_id)[0]
        if self.hr_employee_id:   
            res['name'] = self.hr_employee_id.last_name
            res['company_name'] = self.hr_employee_id.company_id.id
            res['department_name'] = self.hr_employee_id.department_id.id
            res['position_name'] = self.hr_employee_id.job_id.id
            res['identification_id'] = self.hr_employee_id.identification_id
            res['age'] = self.hr_employee_id.employee_age
            res['gender'] = self.hr_employee_id.gender
            return {'value':res}    
        
        
        
        
class medical_examination_type(models.Model):
    _name = 'medical.examination.type'
    _description = 'Medical examination type'
    
    name = fields.Char(string="Tag Name", required=True)
    color = fields.Integer(string='Color Index')
    medical_ids = fields.Many2many('medical.examination', 'medical_examination_rel', 'category_id', 'medical_id', string='medical category')
    
    _sql_constraints = [
        ('name_uniq', 'unique (name)', "Tag name already exists !"),
    ]
