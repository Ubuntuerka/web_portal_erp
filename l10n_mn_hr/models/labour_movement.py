import base64
import logging

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY
from odoo import api, fields, models, SUPERUSER_ID, tools

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import af

from odoo.modules.module import get_module_resource
from odoo.tools.translate import _
from calendar import month, day_abbr, day_name
from dateutil import rrule
import math
from operator import countOf


class labour_movement(models.Model):
    _name = 'labour.movement'
    _inherit = ['mail.thread']

    name = fields.Char('Last name')
    company_name = fields.Many2one('res.company', 'Company')
    department_name = fields.Many2one('hr.department', 'Department')
    position_name = fields.Many2one('hr.job', 'Position')
    to_company = fields.Many2one('res.company', 'To company', track_visibility='onchange')
    to_department = fields.Many2one('hr.department', 'To department', track_visibility='onchange')
    to_position = fields.Many2one('hr.job', 'To position', track_visibility='onchange')
    decree_id = fields.Many2one('hr.decree', 'Decree', track_visibility='onchange')
    # 'possesed_asset':fields.char('Possesed asset'),
    date = fields.Date('Date', track_visibility='onchange')
    shift_reason = fields.Text('Shift reason', track_visibility='onchange')
    hr_employee_id = fields.Many2one('hr.employee', 'First name', track_visibility='onchange')
    state = fields.Selection([
                            ('draft', 'Draft'),
                            ('cancel', 'Cancel'),
                            ('approve', 'Approve'),
                            ], string='State',
                             track_visibility='onchange',  default = 'draft')
    movement_type = fields.Selection([
                                    ('vertical_displacement', 'Vertical displacement'),
                                    ('horizontal_displacement', 'Horizontal displacement'),
                                    ('reduction_displacement', 'Reduction displacement'),
                                    ], string='Movement type',
                                     track_visibility='onchange')  # Шилжилт хөдөлгөөний төрөл
    
    
    
    
    #===========================================================================
    # def _labour_movement(self, field_name):
    #     res = {}
    #     task_bom = self.env['labour.movement']
    #     for boms in ids:
    #         nb = task_bom.search_count([('hr_employee_id', '=', boms)])
    #         res[boms] = nb
    #     return res
    #===========================================================================
    
    #===========================================================================
    # def action_labour_movement(self):
    #     boms = self._get_movements()
    #     result = self._get_act_window_dict('l10n_mn_hr.action_labour_movement')
    #     result['domain'] = "[('id','in',[" + ','.join(map(boms)) + "])]"
    #     return result
    #===========================================================================
    

    def action_to_draft(self):
        self.write({'state': 'draft'})
        return True

    def action_to_cancel(self):
        self.write({'state': 'cancel'})
        return True
    
    #===========================================================================
    # def action_to_approve(self):
    #     self.write({'state': 'approve'})
    #     emp_obj = self.env['hr.employee']
    #     obj = self.browse()
    #     asset_obj = self.env['account.asset.asset']
    #     partner_obj = self.env['res.partner']
    #     
    #     if obj.hr_employee_id.user_id.partner_id.id != []:
    #         print('dddddddd',obj.hr_employee_id.user_id.partner_id.id)
    #         asset_id = asset_obj.search([('owner_id', '=', obj.hr_employee_id.user_id.partner_id.id)])
    #         if asset_id != []:
    #             #===============================================================
    #             # raise osv.except_osv(_('Error!'), _("You must an accumulation"))
    #             #===============================================================
    #             raise UserError(_("You must an accumulation"))
    #         else:
    #             emp_obj.write(obj.hr_employee_id.id, {'company_id':obj.to_company.id,
    #                                                             'department_id':obj.to_department.id,
    #                                                             'job_id':obj.to_position.id,
    #                                                             'job_grade_id':obj.to_position.job_grade_id.id,
    #                                                             'job_code':obj.to_position.job_code})
    #     return True
    #===========================================================================
    
    def action_to_approve(self):
        self.write({'state': 'approve'})
        emp_obj = self.env['hr.employee']
        obj = self.browse()
        #asset_obj = self.env['account.asset.asset']
        partner_obj = self.env['res.partner']
        shts_obj = self.env['shts.register']
        #emp_rel = self.env['shts_register_employee_rel']
        emp_id = self.env['hr.employee'].search([('id','=',self.hr_employee_id.id)])
        to_shts_id = shts_obj.search([('pos_number','=',self.to_department.code)])
        shts_id = shts_obj.search([('pos_number','=',self.department_name.code)])
        
        print('dddddddddddddddd',shts_id.employee_ids,len(shts_id))
        #emp_br = self.env['hr.employee'].browse(self.hr_employee_id.id)
        if len(shts_id) > 1:
            raise ValidationError(_('ШТС бүртгэл Посын дугаар болон хэлтэс дээрх код талбар давхацсан эсэхийг шалгана уу!!!'))  
        if shts_id:
           #rel_id = emp_rel.search([('employee_id','=',self.hr_employee_id.id),('shts_id','=',shts_id.id)])
           
           #shts_id.write({'employee_ids': [( 4, self.hr_employee_id.id)]})
           
           emp_id.write({'company_id':self.to_company.id,
                                                        'department_id':self.to_department.id,
                                                        'job_id':self.to_position.id,
                                                        'job_title':self.to_position.name,
                                                        #'job_date':self.date,
                                                        'parent_id':self.to_department.manager_id.id,
                                                        'main_shts_id':to_shts_id.id,
                                                        'shts_id': to_shts_id,
                                                        #'job_grade_id':obj.to_position.job_grade_id.id,
                                                        #'job_code':obj.to_position.job_code
                                                        })
        else:
            raise ValidationError(_('ШТС бүртгэл Посын дугаар болон хэлтэс дээрх код талбар адил байгаа эсэхийг шалгана уу!!!'))  
        #=======================================================================
        # if obj.hr_employee_id.user_id.partner_id.id != []:
        #     print('dddddddd',obj.hr_employee_id.user_id.partner_id.id)
        #     asset_id = asset_obj.search([('owner_id', '=', obj.hr_employee_id.user_id.partner_id.id)])
        #     if asset_id != False:
        #         #===============================================================
        #         # raise osv.except_osv(_('Error!'), _("You must an accumulation"))
        #         #===============================================================
        #         raise UserError(_("You must an accumulation"))
        #     else:
        #         emp_obj.write(obj.hr_employee_id.id, {'company_id':obj.to_company.id,
        #                                                 'department_id':obj.to_department.id,
        #                                                 'job_id':obj.to_position.id,
        #                                                 'job_grade_id':obj.to_position.job_grade_id.id,
        #                                                 'job_code':obj.to_position.job_code})
        #=======================================================================
        return True
    @api.onchange('hr_employee_id')
    def onchange_movement(self):
        res = {}
        #if empl_id:
         #   empl_obj = self.env['hr.employee']
          #  empl = empl_obj.browse(self.empl_id)[0]
        if self.hr_employee_id:   
            res['name'] = self.hr_employee_id.last_name
            res['company_name'] = self.hr_employee_id.company_id.id
            res['department_name'] = self.hr_employee_id.department_id.id
            res['position_name'] = self.hr_employee_id.job_id.id
            return {'value':res}

        
    # def change_movement(self, cr, uid, ids, *args):
        # obj = self.browse(cr, uid, ids)
        # asset_obj = self.env('account.asset.asset')
        # asset_id = asset_obj.search(cr, uid, [('owner_id','=',obj.hr_employee_id.id)])
        
        # if asset_id:
            # raise osv.except_osv(_('Error!'),_("You must an accumulation"))

        # else:
            # self.write(cr, uid, ids,{'active':False})
        # return True
        
    