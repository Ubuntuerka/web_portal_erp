# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import da

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

_logger = logging.getLogger(__name__)


class award_list(models.Model):
    _name = 'award.list'
    _inherit = ['mail.thread']
    _description = 'Award list'     #Шагналын бүртгэл
    
    name = fields.Char('Award name', required=True, track_visibility='onchange')#Шагналын нэр
    level = fields.Selection([('government', 'Government'),#Засгийн газрын
                              ('international', 'International'),#Олон улсын
                              ('organization', 'Organization'),#Байгууллагын
                              ('nonpolitical', 'Nonpolitical')],#Төрийн бус
                               'Level')#Шагналын ангилал
    award_type = fields.Many2one('award.type', 'Award type')#Шагналын төрөл
    award_code = fields.Char('Award code')#Шагналын  код
    partner_id = fields.Many2one('res.partner', 'Invoicer', track_visibility='onchange')#Нэхэмжлэгч
    award_last_name = fields.Char('Last name')#Овог
    award_department = fields.Many2one('hr.department', 'Department',)#Алба, хэлтэс
    award_position = fields.Many2one('hr.job', 'Position',)#Албан тушаал
    received_date = fields.Date('Received date', track_visibility='onchange')#Огноо
    is_amount = fields.Boolean('Is amount')#Мөнгөн шагнал
    amount = fields.Float('Amount', track_visibility='onchange')#Олгосон мөнгөн дүн
    definition = fields.Text('Definition')#Шагналын тодорхойлолт
    company_id = fields.Many2one('res.company', 'Company')#Харъяалагдах компани
    hr_employee_id = fields.Many2one('hr.employee', 'Hr employee', track_visibility='onchange')#Нэр
    invoice_id = fields.Many2one('account.move', 'Invoice', track_visibility='onchange')#Нэхэмжлэл
    decree_id = fields.Many2one('hr.decree', 'Decree', track_visibility='onchange')#Тушаал
    state = fields.Selection([
        ('draft', 'Draft'),#Ноорог
        ('invoiced', 'Invoiced'),#Нэхэмжилсэн
        ('award_approved', 'Award approved'),#Олгогдсон
        ], 'State', track_visibility='onchange', default='draft')#Төлөв
    
    @api.onchange('award_type')
    def onchange_type(self):
        res = {}
        if self.award_type:
            res['award_code'] = self.award_type.award_code
            return {'value':res}
        
    @api.onchange('hr_employee_id')
    def onchange_award(self):
        res = {}
        if self.hr_employee_id:
            empl_obj = self.env['hr.employee']
            empl = empl_obj.browse(self.hr_employee_id.id)
            res['award_last_name'] = u'%s' % self.hr_employee_id.last_name
            res['company_id'] = self.hr_employee_id.company_id.id
            res['award_department'] = self.hr_employee_id.department_id.id
            res['award_position'] = self.hr_employee_id.job_id.id
            return {'value':res}
        
    def action_award_invoice(self):
        invoice = self.env['account.move']
        invoice_line2 = self.env['account.move.line']
        journal_obj = self.env['account.journal']
        # par_obj = self.pool.get('fleet.vehicle')
       
        currency_id = self.company_id.currency_id.id
        journal_ids = journal_obj.search([('type', '=', 'purchase'), ('company_id', '=', self.company_id.id or False)], limit=1)
        print ('DDDDDDDDD', journal_ids)
        invoice_lines = []
             
        invoice_line = []
        invoice_line.append(({
                                'name': self.name + ' - ' + self.hr_employee_id.name,
                                'account_id': self.hr_employee_id.user_id.partner_id.property_account_payable_id.id,
                                # 'unit_type': obj.unit_type,
                                'price_unit': self.amount,
                                        
        }))
            
        sch_invoice_2 = invoice_line2.create({
                                            'name': self.name + ' - ' + self.hr_employee_id.name,
                                            'account_id': self.hr_employee_id.user_id.partner_id.property_account_payable_id.id,
                                            # 'unit_type': obj.unit_type,
                                            'price_unit': self.amount,
                                            #'move_id': sch_invoice.id,
                                     })
            
        sch_invoice = invoice.create({
                                    #'account_id': self.partner_id.property_account_payable_id.id,
                                    'type': 'in_invoice',
                                    'partner_id': self.hr_employee_id.user_id.partner_id.id,
                                    'currency_id': currency_id,
                                    'journal_id': journal_ids.id,
                                    'company_id': self.company_id.id,
                                    'invoice_line_ids': sch_invoice_2,
                                    # 'date_due': obj.executed_time,
                                     })
        self.write({'state': 'invoiced',
                    'invoice_id': sch_invoice})
        return True 
        
    def action_award_approved(self):
        self.write({'state': 'award_approved'})
        return True  

        
class award_type(models.Model):
    _name = 'award.type'
    _description= 'Award type'  #Шагналын төрөл
    
    name = fields.Char('Award name')#Шагналын нэр
    award_code = fields.Char('Award code')#Шагналын код
    level = fields.Selection([('government', 'Government'),#Засгийн газрын
                              ('international', 'International'),#Олон улсын
                              ('organization', 'Organization'),#Байгууллагын
                              ('nonpolitical', 'Nonpolitical')],#Төрийн бус
                               'Level')#Шагналын ангилал
    hr_employee_id = fields.Many2one('hr.employee', 'Hr employee')#Ажилтан
    

