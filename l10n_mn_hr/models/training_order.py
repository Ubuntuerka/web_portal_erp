# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import da

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

_logger = logging.getLogger(__name__)


        
class training_order(models.Model):
    _name = 'training.order'
    _description = 'Training order' #Сургалтын захиалга
    
    name = fields.Char('Name', track_visibility='onchange')#Хүсч буй сургалтын нэр
    #type = fields.Selection([('annual_planned', 'Annual planned'), ('company_unplanned', 'Company unplanned'), ('staff_order', 'Staff order')], 'Type', track_visibility='onchange')#Төрөл
    training_type = fields.Many2one('training.type', 'Sub type', track_visibility='onchange')#Дэд төрөл
    hr_employee_id = fields.Many2one('hr.employee', 'Hr employee', default=lambda self: self.env['hr.employee'].search([('user_id', '=', self.env.uid)]).id)#Ажилтан
    dep_id = fields.Many2one('hr.department', 'Department', default=lambda self: self.env['hr.employee'].search([('user_id', '=', self.env.uid)]).department_id.id)#Хэлтэс
    company_id = fields.Many2one('res.company', 'Company', required=True, track_visibility='onchange', default=lambda self: self.env.company)#Харъяалагдах компани
    state = fields.Selection([
        ('draft', 'Draft'),#Хүсэлт
        ('integrated', 'Integrated'),#Нэгтгэгдсэн
        ('approved', 'Approved'),#Захиалга батлагдсан
        ('done', 'Done'),#Сургалт хийгдсэн
        ('cancel', 'Cancel'),#Цуцлагдсан
        ], 'State', track_visibility='onchange', default='draft')#Төлөв
    plan_id = fields.Many2one('internal.training', 'Internal training')#Сургалтын төлөвлөгөө
   
   
class Survey(models.Model):

    _inherit = 'survey.survey'

    title = fields.Char(u'Асуулгын гарчиг', required=True) 
