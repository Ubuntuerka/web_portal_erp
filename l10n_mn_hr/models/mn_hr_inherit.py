# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY
from odoo import api, fields, models, SUPERUSER_ID, tools
from odoo.osv import expression
from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import af

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

_logger = logging.getLogger(__name__)


class HrEeljiinHuwaari(models.Model):
    _name = 'eeljiin.huwaari'
    
    name = fields.Char(string=u'Нэр')
    company_id = fields.Many2one('res.company',string='Company')
    department_id = fields.Many2one('hr.department',string=u'Газар/Нэгж') #Хэлтэс
    work_day1 = fields.Float(string=u"Эхлэх цаг")#Өглөө эхлэх
    work_day2 = fields.Float(string=u"Дуусах цаг")#Орой дуусах
    work_day3 = fields.Float(string=u"Эхлэх цаг")#Орой эхлэх
    work_day4 = fields.Float(string=u"Дуусах цаг")#Өглөө дуусах
    line_ids = fields.One2many('eeline','ee_huwaari_id','line')
    
class HrEeLine(models.Model):
    _name = 'eeline'
    
    department_id = fields.Many2one('hr.department',string=u'Газар/Нэгж') #Хэлтэс 
    emp_id = fields.Many2many('hr.employee', string=u'Өглөө')
    emp_id1 = fields.Many2many('hr.employee', 'eeline_emp_rel','employee_id','eeline_id',string=u'Орой')
    date1 = fields.Date(string=u'Огноо')
    ee_huwaari_id = fields.Many2one('eeljiin.huwaari',string=u'Хуваарь') #Хэлтэс 


    
class HrCelebrationDay(models.Model):
    _name = 'celebration.day'
    
    name = fields.Char(string=u'Нэр')
    start_date = fields.Date(string=u'Огноо')

class inshurance_type(models.Model):
    _name = 'inshurance.type'
    
    company_id = fields.Many2one('res.company',string=u'Компани')
    name = fields.Char(string=u'Даатгуулагчийн төрөл')
    code = fields.Char(string=u'Код')

class HrJob(models.Model):
    _inherit = 'hr.job'
    
    category_id = fields.Many2one('hr.job.category',string='Hr job category',required=True)
    job_category = fields.Selection([('1', '1'), 
                                        ('2', '2'),
                                        ('3', '3'), 
                                        ('4', '4'),
                                        ('5', '5'),
                                        ('6', '6'), 
                                        ('7', '7')], 
                                        string=u'Ажилтны төрөл',required=True)
    
class HrJobCategory(models.Model):
    _name = 'hr.job.category'
    
    company_id = fields.Many2one('res.company',string='Company')
    name = fields.Char(string='Hr job category name')
    code = fields.Char(string='Hr job category code')
    percent =  fields.Char(string='ҮО-ын хувь')

class State(models.Model):
    _inherit = 'res.country.state'
    
    name = fields.Char(string=u'Нэр')
    code = fields.Char(string=u'Код')

class SumDuureg(models.Model):
    _name = 'sum.duureg'
    
    name = fields.Char(string=u'Нэр')
    code = fields.Char(string=u'Код')
    aimag_hot_id = fields.Many2one('res.country.state',string=u'Аймаг/Хот')
    
class Hrkhoroo(models.Model):
    _name = 'khoroo'
    
    name = fields.Char(string=u'Нэр')
    code = fields.Char(string=u'Код')
    sum_duureg_id = fields.Many2one('sum.duureg',string=u'Сум/Дүүрэг')
    
class EmpType(models.Model):
    _name ='emp.type'
    _inherit = ['mail.thread']
    
    company_id = fields.Many2one('res.company',string='Company')
    name = fields.Char(string='Name')
    



class HrEmployeePrivate(models.Model):
    _inherit = "hr.employee"
    
    last_name = fields.Char(string='Last name')
    identification_id = fields.Char(string='Identification No', groups="hr.group_hr_user", tracking=True,required=True,size=10)
    #job_date = fields.Date(string=u'Тухайн ажилд орсон огноо')
    emp_type = fields.Many2one('emp.type',string=u'Статус',required=True)
    resource_calendar_id1 = fields.Many2one('resource.calendar', string=u'Хуучин ажлын цаг')
    surname = fields.Char(string='Surname')
    ndd_type_id = fields.Many2one('inshurance.type',string='Insurance type')
    ndd_number = fields.Char(string='Ndd number')
    bank_number = fields.Char(string=u'Цалингийн дансны дугаар')
    tin_number = fields.Char(string=u'ТТДугаар',required=True,size=12)
    bank_id = fields.Many2one('res.bank',string=u'Банк')
    is_solder = fields.Boolean(string=u'Цэрэгт явсан эсэх')
    solder_number = fields.Char(string=u'Цэргийн үнэмлэхний дугаар')
    register_number = fields.Char(string='Register number')
    education_id = fields.One2many('education','hr_employee_id',string='Education')
    credit_hours_id = fields.One2many('credit.hours','hr_employee_id',string='Credit hours')
    training_course_id = fields.One2many('training.course','hr_employee_id',string='Training course')
    hr_language_id = fields.One2many('hr.language','hr_employee_id',string='Hr Language')
    computer_skill_id = fields.One2many('computer.skill','hr_employee_id',string='Computer skill')
    urlag_skill_id = fields.One2many('urlag.skill','hr_employee_id',string='Urlag skill')
    awards_id = fields.One2many('awards.skill','hr_employee_id',string='awards')
    employment_id = fields.One2many('employment','hr_employee_id',string='Employment')
    employment_description = fields.Text('Employment description')
    #other_programm = fields.Text('Other programm')
    certificate = fields.Selection([
        ('bbusdund', 'Бүрэн бус дунд'),
        ('burendund', 'Бүрэн дунд'),
        ('tusgaidund', 'Тусгай дунд'),
        ('mergeshih', 'Мэргэших сургалт'),
        ('bachelor', 'Баклавр'),
        ('master', 'Мастер'),
        ('other', 'Доктор'),
    ], 'Certificate Level', default='bachelor', groups="hr.group_hr_user", tracking=True)
    live_people_id = fields.One2many('live.people','hr_employee_id',string='Live')
    hr_relationship_id = fields.One2many('hr.relationship','hr_employee_id',string='Hr relationship')
    blood_type = fields.Selection([('1', '1'), 
                                        ('2', '2'),
                                        ('3', '3'), 
                                        ('4', '4')], 
                                        string='Blood type')
    #===========================================================================
    # other_use_skill = fields.Selection([('bad', 'Bad'), 
    #                                     ('middle', 'Middle'),
    #                                     ('good', 'Good'), 
    #                                     ('best', 'Best')], string='Other use skill')
    #===========================================================================
    
    employee_code = fields.Char('Employee code')#Ажилтны код
    employee_code_old = fields.Char(u'Ажилтны код /Хуучин/')#Ажилтны код хуучин
    employee_age = fields.Integer('Age')
    
    family = fields.Integer(string='Number of Family', groups="hr.group_hr_user", tracking=True)
    children = fields.Integer(string='Number of Children', groups="hr.group_hr_user", tracking=True)
    
    accession_date = fields.Date('Accession date', required=True,
                              read=['base.group_user'],
                              write=['base.group_hr_user'])#Ажилд орсон огноо
    working_year = fields.Integer('Working year')#Байгууллагад ажиллаж буй жил
    working_year_month = fields.Integer('Working year month')#Байгууллагад ажиллаж буй сар
    working_year_other1 = fields.Integer('Working year other')#Бусад байгууллагад ажиллаж буй жил
    working_year_other1_month = fields.Integer('Working year other1 month')#Бусад байгууллагад ажиллаж буй сар
    # 'seniority': fields.function(_get_seniority,type = 'integer',string='Seniority'),
    seniority = fields.Integer(string='Seniority')#Улсад ажилласан жил
    seniority_month = fields.Integer(string='Seniority month')#Улсад ажилласан сар
    
    #ndd=fields.Char('NDD')#НДД дугаар
    #emdd=fields.Char('EMDD')#ЭМДД дугаар
    driver_type_id=fields.Many2many('driver.type', 'emp_driver_type_hr_rel', 'driver_type_id', 'hr_employee_id', 'Driver type')#Жолооны ангилал
    driver_date=fields.Date('Driver date')#Жолооч болсон огноо
    
    #dependence_address = fields.Char('Dependence address')
    dependence_address = fields.Many2one('res.country.state',string=u'Аймаг/Хот')
    dependence_address1 = fields.Many2one('res.country.state',string=u'Аймаг/Хот')
    khoroo_id = fields.Many2one('khoroo',string=u'Баг/Хороо')
    khoroo_id1 = fields.Many2one('khoroo',string=u'Баг/Хороо')
    khoroo_id2 = fields.Char(string=u'Баг/Хороо')
    khoroo_id3 = fields.Char(string=u'Баг/Хороо')
    sum_duureg_id = fields.Many2one('sum.duureg',string=u'Сум/Дүүрэг')
    sum_duureg_id1 = fields.Many2one('sum.duureg',string=u'Сум/Дүүрэг')
    desc_id = fields.Char(string=u'Хашаа/Байрны дугаар, тоот')
    desc_id1 = fields.Char(string=u'Хашаа/Байрны дугаар, тоот')
    home_phone=fields.Char('Home phone')#Гэрийн утас
    map_home_work=fields.Char(string =u'W3W',help="What3words хаяг оруулна")
    map_home_work1=fields.Char(string =u'W3W',help="What3words хаяг оруулна")
    #several_name = fields.Char('Several name')
    #several_phone = fields.Char('Several phone')
    
    # Нэмэлт - Check info
    is_temporary_staff = fields.Boolean(string='Is temporary staff', tracking=True)#Түр ажилтан эсэх
    is_provided_work_id = fields.Boolean(string='Is provided work id', tracking=True)#Ажлын үнэмлэх олгосон эсэх
    is_provided_business_card = fields.Boolean(string='Is provided business card', tracking=True)#Нэрийн хуудас олгосон эсэх
    is_completed_required_materials = fields.Boolean(string='Is completed required materials', tracking=True)#Бүрдүүлэх материал бүрэн эсэх
    is_provided_required_materials = fields.Boolean(string='Is provided required materials')#Бүрдүүлэх материал олгосон эсэх
    is_submitted_required_materials = fields.Boolean(string='Is submitted required materials')#Бүрдүүлэх материал бүрдүүлсэн эсэх
    is_provided_required_materials_iltod = fields.Boolean(string='Is provided required materials iltod')
    is_submitted_required_materials_project = fields.Boolean(string='Is submitted required materials project')
    
    
    is_meet_internal_labor_regulations = fields.Boolean(string='Is meet internal labor regulations', tracking=True)#Хөдөлмөрийн дотоод журамтай танилцсан эсэх
    is_meet_internal_bonus_regulations = fields.Boolean(string='Is meet internal bonus regulations', tracking=True)#Урамшуулал бонус олгох журамтай танилцсан эсэх
    is_meet_use_computer_tools = fields.Boolean(string='Is meet use computer tools', tracking=True)#Компьютерийн хэрэгсэл хэрэглэх журамтай танилцсан эсэх
    is_meet_job_description = fields.Boolean(string='Is meet job description', tracking=True)#Ажлын байрны тодорхойлолттой танилцсан эсэх
    is_completed_orientation_training = fields.Boolean(string='Is completed orientation training', tracking=True)#Баримжаа олгох сургалт авсан эсэх
    is_meet_job_description_connection = fields.Boolean(string='Is meet job description connection', tracking=True)
    is_completed_orientation_training_dadlaga = fields.Boolean(string='Is completed orientation training dadlaga', tracking=True)
    
    country_id = fields.Many2one('res.country', 'Nationality (Country)', groups="hr.group_hr_user", tracking=True)
    country_of_birth = fields.Many2one('res.country', string="Country of Birth", groups="hr.group_hr_user", tracking=True)
    place_of_birth = fields.Char('Place of Birth', groups="hr.group_hr_user", tracking=True)
    birthday = fields.Date('Date of Birth', groups="hr.group_hr_user", tracking=True)
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')], groups="hr.group_hr_user", default="male", tracking=True)
    identification_id = fields.Char(string='Identification No', groups="hr.group_hr_user", tracking=True)
    passport_id = fields.Char('Passport No', groups="hr.group_hr_user", tracking=True)
    marital = fields.Selection([
        ('single', 'Single'),
        ('married', 'Married'),
        ('cohabitant', 'Legal Cohabitant'),
        ('widower', 'Widower'),
        ('divorced', 'Divorced')], string='Marital Status', groups="hr.group_hr_user", default='single', tracking=True)
    phone = fields.Char(related='address_home_id.phone', related_sudo=False, string="Гар утас", groups="hr.group_hr_user")
    private_email1 = fields.Char(string="Хувийн имэйл", groups="hr.group_hr_user")
    emergency_contact = fields.Char("Emergency Contact", groups="hr.group_hr_user", tracking=True)
    emergency_phone = fields.Char("Emergency Phone", groups="hr.group_hr_user", tracking=True)
    km_home_work = fields.Integer(string="Km Home-Work", groups="hr.group_hr_user", tracking=True)
    bank_account_id = fields.Many2one(
        'res.partner.bank', 'Bank Account Number',
        domain="[('partner_id', '=', address_home_id), '|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        groups="hr.group_hr_user",
        tracking=True,
        help='Employee bank salary account')
    
    
    

    special_need_employee = fields.Boolean(string='Special need employee', tracking=True)#Тусгай хэрэгцээт ажилтны мэдээлэл
    special_renewal_date = fields.Date(string='Special renewal date')#Тусгай хэрэгцээт ажилтны сунгалт хийх огноо
    special_line_id = fields.One2many('special.need.line', 'hr_employee_id', string='Line')#Сунгалтын түүх
    emp_status = fields.Selection([('choloo', u'Урт хугацааны чөлөө'), 
                                        ('jir_amralt', u'Жирэмсний чөлөө'),
                                        ('huuhed_asrah', u'Хүүхэд асрах чөлөө'),
                                        ('hugj_berhsheel', u'Хөгжлийн бэрхшээлтэй'),
                                        ('emp_active', u'Идэвхтэй'),
                                        ('uvchin', u'Өвчний бүртгэл')], 
                                        string=u'Ажилтны төлөв', default="emp_active", required=True)
    hugj_berhsheel_disc =fields.Char(string=u'Гэрчилгээний мэдээлэл')
    hugj_huwi =fields.Char(string=u'Хувь')
    choloo_start = fields.Date(string=u'Эхлэх огноо')
    choloo_end = fields.Date(string=u'Дуусах огноо')
    is_black_list = fields.Boolean(string=u'Хар жагсаалт орсон эсэх')
    job_end_date = fields.Date(string=u'Ажлаас гарсан огноо')
    departure_reason = fields.Selection([
        ('fired', u'Байгууллагын санаачлагаар'),
        ('resigned', u'Өөрийн хүсэлтээр'),
        ('retired', u'Гэрээний хэрэгжилтийг дүгнэсэн протокол/байгууллагын санаачилгаар'),
        ('retired1', u'Гэрээний хэрэгжилтийг дүгнэсэн протокол/ажилтны санаачилгаар')
    ], string="Departure Reason", groups="hr.group_hr_user", copy=False, tracking=True)

    
    _sql_constraints = [
        ('identification_id_uniq', 'unique(identification_id)', u'Энэ регистрын дугаар дээр ажилтан үүссэн байна!'),
        ('tin_number_uniq', 'unique(tin_number)', u'Энэ ТТДугаар дугаар дээр ажилтан үүссэн байна. Шалгана уу!'),
        ('employee_code', 'unique(employee_code)', u'Энэ ажилтны дугаартай ажилтан үүссэн байна. Шалгана уу!'),
    ]
    

    
    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('code', '=ilike', name.split(' ')[0] + '%'), ('name', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&', '!'] + domain[1:]
        employee_ids = self._search(expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid)
        return models.lazy_name_get(self.browse(employee_ids).with_user(name_get_uid))
    
    
    @api.depends('name', 'code')
    def name_get(self):
        result = []
        for line in self:
            name = line.name or ''
            if line.employee_code and line.last_name:
                name += ".%s [ %s ]" % (line.last_name[:1],line.employee_code)
            elif line.employee_code and not line.last_name:
                name += " [ %s ]" % line.employee_code
            result.append((line.id, name))
        return result
    
    
    
    def show_in_resume(self):
        hr_resume_line = self.env['hr.resume.line']
        hr_resume_line_type = self.env['hr.resume.line.type']
        for record in self.employment_id:
            if record.resume_line_id:
                record.resume_line_id.write({
                                    'name':record.name,
                                    'line_type_id':1,
                                    'display_type':'classic',
                                    'description':record.org_position,
                                    'date_start':record.org_start,
                                    'date_end':record.org_finish,
                                    })
            else:
                created_id = hr_resume_line.create({
                                    'name':record.name,
                                    'line_type_id':1,
                                    'display_type':'classic',
                                    'description':record.org_position,
                                    'date_start':record.org_start,
                                    'date_end':record.org_finish,
                                    'employee_id':self.id,
                                    })
                record.resume_line_id = created_id.id
        
        return True
    
    def compute_all_wyo(self):
        emp_obj = self.env['hr.employee']
        emp_ids = emp_obj.search([('active', '=', True)])
        for pr_emps in emp_ids:
            pr_emps.working_year_other2()
        return True
    
    def working_year_other2(self):
        res = {}
        line_month = 0.0
        line_year = 0.0
        working_year_others = 0.0
        wyo = 0
        wyo_other = 0
        for record in self:
            if record.employment_id:
                for emp in record.employment_id:
                    if emp.is_ndd == True:
                        a = int(str(emp.org_finish)[0:4])
                        b = int(str(emp.org_start)[0:4])
                        c = int(str(emp.org_finish)[5:7])
                        d = int(str(emp.org_start)[5:7])
                        if a == b and c > d:
                            line_month = c - d + 1
                        elif a > b and c > d:
                            line_year = (a - b) * 12
                            line_month = line_year + (c - d) + 1
                        elif a > b and c < d:
                            line_year = (a - b - 1) * 12
                            line_month = line_year + (c + 12 - d) + 1
                        elif a == b and c == d:
                            line_month = 1
                        elif a > b and c == d:
                            line_month = (a - b) * 12 + 1
                        else:
                            line_month = 0
                    else:
                        line_month = 0.0
                    
                    working_year_others += line_month
        if int(working_year_others / 12) > 0:
            wyo = int(working_year_others / 12)
            wyo_other = working_year_others - int(working_year_others / 12) * 12
        elif int(working_year_others / 12) == 0:
            wyo = 0
            wyo_other = working_year_others
        self.write({'working_year_other1': wyo,
                    'working_year_other1_month': wyo_other})
        return True
    
    
    def compute_all_gs(self):
        emp_obj = self.env['hr.employee']
        emp_ids = emp_obj.search([('active', '=', True)])
        for pr_emps in emp_ids:
            pr_emps.get_seniority2()
        return True
    
    def get_seniority2(self):
        obj = self.browse()
        line_month = 0.0
        line_year = 0.0
        employment_month = 0
        our_month = 0
        ndd_month = 0.0
        
        sy = 0
        sy_other = 0
        
        employment_month = self.working_year_other1 * 12 + self.working_year_other1_month
        our_month = self.working_year * 12 + self.working_year_month
        ndd_month = employment_month + our_month
            
        if int(ndd_month / 12) > 0:
            sy = int(ndd_month / 12)
            sy_other = ndd_month - int(ndd_month / 12) * 12
        elif int(ndd_month / 12) == 0:
            sy = 0
            sy_other = ndd_month
        self.write({'seniority': sy,
                    'seniority_month': sy_other})
        return True
    
    def shg_working_year(self):
        if self.accession_date:
                date_from = datetime.strptime(str(self.accession_date), DATE_FORMAT)
                date_to = datetime.today()
                diff = relativedelta.relativedelta(date_to, date_from)
                
        self.write({'working_year': diff.years,
                    'working_year_month': diff.month})
    
    
   
    @api.model
    def create(self, vals):
        if vals.get('user_id'):
            user = self.env['res.users'].browse(vals['user_id'])
            vals.update(self._sync_user(user))
            vals['name'] = vals.get('name', user.name)
            
    # Getting 'Employee code'
        #Kompaniin kod hamaarahgvi 8n orontoi ajiltnii kod bolgowol doorh kodiig ashiglana
        i = ''
        create_year = 0
        create_year = str(vals.get('accession_date'))[2:4]
        create_month = str(vals.get('accession_date'))[5:7]
        create_day = str(vals.get('accession_date'))[8:10]
        cc= create_year + create_month + create_day
        self.env.cr.execute("""select employee_code from hr_employee 
                    where employee_code like '%s'
                    order by employee_code desc limit 1""" %(cc+'%'))
        employee_code_info = self.env.cr.dictfetchall()
        if employee_code_info:
            for employee in employee_code_info:
                empii = employee['employee_code']
                i = int(empii[6:8]) + 1
        else:
            i = 1
          
      
        code = u'%s' % create_year + u'%s' % create_month + u'%s' % create_day + u'%s' % str(i).zfill(2)
        vals['employee_code'] = code
        #Kompaniin kod hamaarsan 10n orontoi ajiltnii kod bolgowol doorh kodiig ashiglana
        #=======================================================================
        # i = ''
        # create_year = 0
        # new_create_sub = ''
        # create_year = vals['accession_date'][0:4]
        # comp_obj = self.env['res.company']
        # comp_id = comp_obj.search([('id', '=', vals['company_id'])])
        # if comp_id.code:
        #     if len(comp_id.code) == 3:
        #         new_create_sub = str(create_year) + str(comp_id.code)
        #         self.env.cr.execute("""select employee_code from hr_employee 
        #                     where substr(employee_code,1,7) = '%s'
        #                     order by employee_code desc limit 1""" % new_create_sub)
        #         employee_code_info = self.env.cr.dictfetchall()
        #         if employee_code_info:
        #             for employee in employee_code_info:
        #                 empii = employee['employee_code']
        #                 if empii == None:
        #                     i = 1
        #                 else:
        #                     i = int(empii[8:10]) + 1
        #         else:
        #             i = 1
        #     else:
        #         raise ValidationError(_('Компанийн код 3-н оронтой тоо байх ёстой!!!'))
        # else:
        #     raise ValidationError(_('Компанийн код байхгүй байна. Компанийн код 3-н оронтой тоо байх ёстой!!!'))
        #  
        # 
        # code = u'%s' % new_create_sub + u'%s' % str(i).zfill(3)
        # vals['employee_code'] = code
        #=======================================================================
        employee_id = super(HrEmployeePrivate, self).create(vals)
        
        #print ('AAAAAA4444', employee_code_info222)
        
        
        return employee_id

    def write(self, vals):
        if 'address_home_id' in vals:
            account_id = vals.get('bank_account_id') or self.bank_account_id.id
            if account_id:
                self.env['res.partner.bank'].browse(account_id).partner_id = vals['address_home_id']
        if vals.get('user_id'):
            vals.update(self._sync_user(self.env['res.users'].browse(vals['user_id'])))
        
        if vals.get('active'):
            self.user_id.write({'active':vals.get('active')})
            
        ##### for TS production by tb - start from here #####
        if_Baseuser = self.env['res.users'].has_group('base.group_user')
        if_HRmanager = self.env['res.users'].has_group('hr.group_hr_manager')
        
        if 'accession_date' in vals:
            date_from = datetime.strptime(str(vals['accession_date']), DATE_FORMAT)
            date_to = datetime.today()
            diff = relativedelta.relativedelta(date_to, date_from)
            vals['working_year'] = diff.years
            vals['working_year_month'] = diff.months
            
        if 'is_provided_work_id' in vals:
            #Oorchlogdohoos omnoh utga - self.is_provided_work_id
            #Oorchilson baigaa utga - vals['is_provided_work_id']
            if vals['is_provided_work_id'] == False:
                if not if_HRmanager:
                    raise ValidationError(_('Хүний нөөцийн менежер сонголтыг буцааж болно. Та менежертээ хандана уу!!!'))
        
        if 'is_provided_business_card' in vals:
            if vals['is_provided_business_card'] == False:
                if not if_HRmanager:
                    raise ValidationError(_('Хүний нөөцийн менежер сонголтыг буцааж болно. Та менежертээ хандана уу!!!'))
           
        if 'is_completed_required_materials' in vals:
            if vals['is_completed_required_materials'] == False:
                if not if_HRmanager:
                    raise ValidationError(_('Хүний нөөцийн менежер сонголтыг буцааж болно. Та менежертээ хандана уу!!!'))     
        
        if 'is_meet_internal_labor_regulations' in vals:
            if vals['is_meet_internal_labor_regulations'] == False:
                if not if_HRmanager:
                    raise ValidationError(_('Хүний нөөцийн менежер сонголтыг буцааж болно. Та менежертээ хандана уу!!!'))   
                
        if 'is_meet_internal_bonus_regulations' in vals:
            if vals['is_meet_internal_bonus_regulations'] == False:
                if not if_HRmanager:
                    raise ValidationError(_('Хүний нөөцийн менежер сонголтыг буцааж болно. Та менежертээ хандана уу!!!'))        
                
        if 'is_meet_use_computer_tools' in vals:
            if vals['is_meet_use_computer_tools'] == False:
                if not if_HRmanager:
                    raise ValidationError(_('Хүний нөөцийн менежер сонголтыг буцааж болно. Та менежертээ хандана уу!!!'))        
                
        if 'is_meet_job_description' in vals:
            if vals['is_meet_job_description'] == False:
                if not if_HRmanager:
                    raise ValidationError(_('Хүний нөөцийн менежер сонголтыг буцааж болно. Та менежертээ хандана уу!!!'))             
        
        if 'is_completed_orientation_training' in vals:
            if vals['is_completed_orientation_training'] == False:
                if not if_HRmanager:
                    raise ValidationError(_('Хүний нөөцийн менежер сонголтыг буцааж болно. Та менежертээ хандана уу!!!'))             
        
        if 'department_id' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'job_id' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'parent_id' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'user_id' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'accession_date' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'pin' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'address_id' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'leave_manager_id' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'expense_manager_id' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'resource_calendar_id' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        if 'employee_code_old' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'employee_code' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'main_shts_id' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'shts_id' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'company_id' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        if 'emp_type' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        if 'emp_status' in vals:
            if not if_HRmanager:
                raise ValidationError(_('Хүний нөөцийн менежер засна. Та менежертээ хандана уу!!!'))
        
        ##### for TS production by tb - end to here #####
            
        res = super(HrEmployeePrivate, self).write(vals)
        if vals.get('department_id') or vals.get('user_id'):
            department_id = vals['department_id'] if vals.get('department_id') else self[:1].department_id.id
            # When added to a department or changing user, subscribe to the channels auto-subscribed by department
            self.env['mail.channel'].sudo().search([
                ('subscription_department_ids', 'in', department_id)
            ])._subscribe_users()
        return res
    
    def compute_all_aa(self):
        emp_obj = self.env['hr.employee']
        emp_ids = emp_obj.search([('active', '=', True)])
        for pr_emps in emp_ids:
            pr_emps.autochange_age()
        return True
    
    def autochange_age(self):
        res = {}
        now_age = 0
        year = 0
        month = 0
        day = 0
        year1 = 0
        month1 = 0
        day1 = 0
        diff_year = 0
        diff_month = 0
        diff_day = 0
        if  self.birthday:
            date_from = datetime.strptime(str(self.birthday), DATE_FORMAT)
            date_to = datetime.today()
            diff = relativedelta.relativedelta(date_to, date_from)
            now_age = diff.years
        else:
            pass
        self.write({'employee_age': now_age})
        return True
    
   

    #habea-d zoriulsan user deerh alban tushaal tseneglelt onchamge_job_id
    @api.onchange('job_id')
    def onchange_job_id(self):
        if self.user_id:
            self.user_id.function = self.job_id.name

#===============================================================================
# class ResumeLine(models.Model):
#     _inherit = 'hr.resume.line'
# 
#     employment_id = fields.One2many('employment', 'resume_line_id', 'Employment')
#===============================================================================

class HrEmployeePrivatePublic(models.Model):
    _inherit = "hr.employee.public"
    
    last_name = fields.Char(string='Last name')
    identification_id = fields.Char(string='Identification No', groups="hr.group_hr_user", tracking=True,required=True,size=10)
    #job_date = fields.Date(string=u'Тухайн ажилд орсон огноо')
    emp_type = fields.Many2one('emp.type',string=u'Ажлын байрны төрөл',required=True)
    resource_calendar_id1 = fields.Many2one('resource.calendar', string=u'Хуучин ажлын цаг')
    expense_manager_id = fields.Char(string='Expense manager')
    surname = fields.Char(string='Surname')
    ndd_type_id = fields.Many2one('inshurance.type',string='Insurance type')
    ndd_number = fields.Char(string='Ndd number')
    employment_description = fields.Text('Employment description')
    bank_number = fields.Char(string=u'Цалингийн дансны дугаар')
    tin_number = fields.Char(string=u'ТТДугаар',size=12)
    bank_id = fields.Many2one('res.bank',string=u'Банк')
    is_solder = fields.Boolean(string=u'Цэрэгт явсан эсэх')
    solder_number = fields.Char(string=u'Цэргийн үнэмлэхний дугаар')
    register_number = fields.Char(string='Register number')
    education_id = fields.One2many('education','hr_employee_id',string='Education')
    credit_hours_id = fields.One2many('credit.hours','hr_employee_id',string='Credit hours')
    training_course_id = fields.One2many('training.course','hr_employee_id',string='Training course')
    hr_language_id = fields.One2many('hr.language','hr_employee_id',string='Hr Language')
    computer_skill_id = fields.One2many('computer.skill','hr_employee_id',string='Computer skill')
    employment_id = fields.One2many('employment','hr_employee_id',string='Employment')
    dependence_address1 = fields.Many2one('res.country.state',string=u'Аймаг/Хот')
    khoroo_id1 = fields.Many2one('khoroo',string=u'Баг/Хороо')
    khoroo_id3 = fields.Char(string=u'Баг/Хороо')
    #other_programm = fields.Text('Other programm')
    live_people_id = fields.One2many('live.people','hr_employee_id',string='Live')
    hugj_berhsheel_disc = fields.Char(string=u'Гэрчилгээний мэдээлэл')
    hugj_huwi = fields.Char(string=u'Хувь')
    hr_relationship_id = fields.One2many('hr.relationship','hr_employee_id',string='Hr relationship')
    blood_type = fields.Selection([('1', '1'), 
                                        ('2', '2'),
                                        ('3', '3'), 
                                        ('4', '4')], 
                                        string='Blood type')
    #===========================================================================
    # other_use_skill = fields.Selection([('bad', 'Bad'), 
    #                                     ('middle', 'Middle'),
    #                                     ('good', 'Good'), 
    #                                     ('best', 'Best')], string='Other use skill')
    #===========================================================================
    
    employee_code = fields.Char('Employee code')#Ажилтны код
    employee_code_old = fields.Char('Employee code')#Ажилтны код
    employee_age = fields.Integer('Age')
    
    family = fields.Integer(string='Number of Family', groups="hr.group_hr_user", tracking=True)
    children = fields.Integer(string='Number of Children', groups="hr.group_hr_user", tracking=True)
    
    accession_date = fields.Date(string=u'Шунхлай компанид орсон огноо', required=True,
                              read=['base.group_user'],
                              write=['base.group_hr_user'])#Ажилд орсон огноо
    working_year = fields.Integer('Working year')#Байгууллагад ажиллаж буй жил
    working_year_month = fields.Integer('Working year month')#Байгууллагад ажиллаж буй сар
    working_year_other1 = fields.Integer('Working year other')#Бусад байгууллагад ажиллаж буй жил
    working_year_other1_month = fields.Integer('Working year other1 month')#Бусад байгууллагад ажиллаж буй сар
    # 'seniority': fields.function(_get_seniority,type = 'integer',string='Seniority'),
    seniority = fields.Integer(string='Seniority')#Улсад ажилласан жил
    seniority_month = fields.Integer(string='Seniority month')#Улсад ажилласан сар
    
    #ndd=fields.Char('NDD')#НДД дугаар
    #emdd=fields.Char('EMDD')#ЭМДД дугаар
    driver_type_id=fields.Many2many('driver.type', 'emp_driver_type_hr_rel', 'driver_type_id', 'hr_employee_id', 'Driver type')#Жолооны ангилал
    driver_date=fields.Date('Driver date')#Жолооч болсон огноо
    
    dependence_address = fields.Many2one('res.country.state',string=u'Аймаг/Хот')
    khoroo_id = fields.Many2one('khoroo',string=u'Баг/Хороо')
    khoroo_id2 = fields.Char(string=u'Баг/Хороо')
    sum_duureg_id = fields.Many2one('sum.duureg',string=u'Сум/Дүүрэг')
    sum_duureg_id1 = fields.Many2one('sum.duureg',string=u'Сум/Дүүрэг')
    desc_id = fields.Char(string=u'Хашаа/Байрны дугаар, тоот')
    desc_id1 = fields.Char(string=u'Хашаа/Байрны дугаар, тоот')
    home_phone=fields.Char('Home phone')#Гэрийн утас
    map_home_work=fields.Text(string =u'W3W')
    map_home_work1=fields.Char(string =u'W3W',help="What3words хаяг оруулна")
    #several_name = fields.Char('Several name')
    #several_phone = fields.Char('Several phone')
    
    # Нэмэлт - Check info
    is_temporary_staff = fields.Boolean(string='Is temporary staff', tracking=True)#Түр ажилтан эсэх
    is_provided_work_id = fields.Boolean(string='Is provided work id', tracking=True)#Ажлын үнэмлэх олгосон эсэх
    is_provided_business_card = fields.Boolean(string='Is provided business card', tracking=True)#Нэрийн хуудас олгосон эсэх
    is_completed_required_materials = fields.Boolean(string='Is completed required materials', tracking=True)#Бүрдүүлэх материал бүрэн эсэх
    is_provided_required_materials = fields.Boolean(string='Is provided required materials')#Бүрдүүлэх материал олгосон эсэх
    is_submitted_required_materials = fields.Boolean(string='Is submitted required materials')#Бүрдүүлэх материал бүрдүүлсэн эсэх
    is_provided_required_materials_iltod = fields.Boolean(string='Is provided required materials iltod')
    is_submitted_required_materials_project = fields.Boolean(string='Is submitted required materials project')
    
    
    is_meet_internal_labor_regulations = fields.Boolean(string='Is meet internal labor regulations', tracking=True)#Хөдөлмөрийн дотоод журамтай танилцсан эсэх
    is_meet_internal_bonus_regulations = fields.Boolean(string='Is meet internal bonus regulations', tracking=True)#Урамшуулал бонус олгох журамтай танилцсан эсэх
    is_meet_use_computer_tools = fields.Boolean(string='Is meet use computer tools', tracking=True)#Компьютерийн хэрэгсэл хэрэглэх журамтай танилцсан эсэх
    is_meet_job_description = fields.Boolean(string='Is meet job description', tracking=True)#Ажлын байрны тодорхойлолттой танилцсан эсэх
    is_completed_orientation_training = fields.Boolean(string='Is completed orientation training', tracking=True)#Баримжаа олгох сургалт авсан эсэх
    is_meet_job_description_connection = fields.Boolean(string='Is meet job description connection', tracking=True)
    is_completed_orientation_training_dadlaga = fields.Boolean(string='Is completed orientation training dadlaga', tracking=True)
    
    special_need_employee = fields.Boolean(string='Special need employee', tracking=True)#Тусгай хэрэгцээт ажилтны мэдээлэл
    special_renewal_date = fields.Date(string='Special renewal date')#Тусгай хэрэгцээт ажилтны сунгалт хийх огноо
    special_line_id = fields.One2many('special.need.line', 'hr_employee_id', string='Line')#Сунгалтын түүх
    emp_status = fields.Selection([('choloo', u'Урт хугацааны чөлөө'), 
                                        ('jir_amralt', u'Жирэмсний чөлөө'),
                                        ('huuhed_asrah', u'Хүүхэд асрах чөлөө'),
                                        ('hugj_berhsheel', u'Хөгжлийн бэрхшээлтэй'),
                                        ('uvchin', u'Өвчний бүртгэл')], 
                                        string=u'Ажилтны төлөв',required=True)
    choloo_start = fields.Date(string=u'Эхлэх огноо')
    choloo_end = fields.Date(string=u'Дуусах огноо')
    is_black_list = fields.Boolean(string=u'Хар жагсаалт орсон эсэх')
    job_end_date = fields.Date(string=u'Ажлаас гарсан огноо')
    departure_reason = fields.Selection([
        ('fired', u'Байгууллагын санаачлагаар'),
        ('resigned', u'Өөрийн хүсэлтээр'),
        ('retired', u'Гэрээний хэрэгжилтийг дүгнэсэн протокол/байгууллагын санаачилгаар'),
        ('retired1', u'Гэрээний хэрэгжилтийг дүгнэсэн протокол/ажилтны санаачилгаар')
    ], string="Departure Reason", groups="hr.group_hr_user", copy=False, tracking=True)
    
class special_need_line(models.Model):
    _name = 'special.need.line'
    
    date_start = fields.Date(string='Date start')
    date_end = fields.Date(string='Date end')
    hr_employee_id = fields.Many2one('hr.employee', string='Employee')
    
    

        
class driver_type(models.Model):
    _name = 'driver.type'       #Жолооны ангилал
    
    
    name = fields.Char(string='Driver type', size=1)#Нэр
    #hr_employee_id = fields.Many2one('hr.employee',string='Hr employee')
    #hr_applicant_id = fields.Many2one('hr.applicant',string='Hr applicant')#Ажилтан
               
    

class hr_education_degree(models.Model):
    _name = 'hr.education.degree'       #Боловсролын зэрэг

    name = fields.Char(string=u'Name')#Боловсролын зэрэг
    
class Education(models.Model):
    _name = 'education'     #Боловсрол
    
    graduate_school = fields.Char(string='Graduate school courses', size=256)
    where = fields.Char(string='Where', size=256)
    starting_year = fields.Date(string='Starting year')
    ending_year = fields.Date(string='Ending year')
    qualifications = fields.Char(string='Qualifications', size=64)
    occupation = fields.Char(string='Occupation', size=64)
    edu_rate = fields.Float(string='Edu rate', size=3)
    edu_percent = fields.Float(string='Percent', size=3)
    edu_payment_type = fields.Selection([('exhibition', 'Exhibition'),('credit', 'Credit'),('self', 'Self')], string='Payment type')
    type_id = fields.Many2one('hr.education.degree', string='Education degree')
    hr_employee_id = fields.Many2one('hr.employee',string='Hr employee')
    #hr_applicant_id = fields.Many2one('hr.applicant',string='Hr applicant')
    
    
    
class training_course(models.Model):
    _name = 'training.course'

    where = fields.Char(string='Where')
    tarining_rate = fields.Char(string=u'Зэрэг')
    time = fields.Char(string='Time')
    training_profession = fields.Char(string='Training profession')
    hr_employee_id = fields.Many2one('hr.employee',string='Hr employee')
    
    
class language_type(models.Model):
    _name = 'language.type'
     
    name = fields.Char(string='Language name')
     
class hr_language(models.Model):
    _name = 'hr.language'
     
    name = fields.Char(string='Hr Language')
    lang_speak = fields.Selection([('bad', 'Муу'),('middle', 'Дунд'),('good', 'Сайн')], string='Speak')
    lang_listen = fields.Selection([('bad', 'Муу'), ('middle', 'Дунд'),('good', 'Сайн')], string='Listen')
    lang_translation = fields.Selection([('bad', 'Муу'), ('middle', 'Дунд'), ('good', 'Сайн')], string='Translation')
    lang_write = fields.Selection([('bad', 'Муу'), ('middle', 'Дунд'),('good', 'Сайн')], string=u'Бичих чалвар')
    lang_time = fields.Char(string=u'Суралцсан хугацаа')
    exam_name = fields.Char(string=u'Шалгалтын нэр')
    exam_rate = fields.Char(string=u'Шалгалтын оноо')
    lang_reason = fields.Char(string=u'Тайлбар')
    hr_employee_id = fields.Many2one('hr.employee',string='Hr employee')
    #hr_applicant_id =fields.Many2one('hr.applicant',string='Hr applicant')

    
    
class employment(models.Model):
    _name = 'employment'

    name = fields.Char(string='Organization')
    org_start = fields.Date(string='Start date')
    org_finish = fields.Date(string='End date')
    salary_size = fields.Float(string='Salary size')
    org_position = fields.Char(string='Position')
    work_schedule = fields.Char(string='Work schedule')
    org_reason = fields.Char('Reason')
    supervisor_name = fields.Char('Supervisor name')
    supervisor_phone = fields.Char('Supervisor phone')
    hr_employee_id = fields.Many2one('hr.employee','Hr employee')
    resume_line_id = fields.Many2one('hr.resume.line', 'Created Resume line')
    hr_applicant_id = fields.Many2one('hr.applicant','Hr applicant')
    
    is_ndd = fields.Boolean('Is ndd')#НДШ төлсөн эсэх

class hr_programm(models.Model):
    _name = 'hr.programm'
 
    name = fields.Char(string='Programm name')
 
class computer_skill(models.Model):
    _name = 'computer.skill'
 
    name = fields.Char(string='Programm name')
    computer_time = fields.Char(string=u'Хичээллэсэн хугацаа')
    use_skill = fields.Selection([('bad', 'Bad'),('middle', 'Дунд'),('good', 'Good'),('best', 'Best')], string='Use skill')
    skill_reason = fields.Char(string=u'Тайлбар')
    hr_employee_id = fields.Many2one('hr.employee',string='Hr employee')
    #hr_applicant_id = fields.Many2one('hr.applicant',string='Hr applicant')


class awards_skill(models.Model):
    _name = 'awards.skill'
 
    awards_name = fields.Char(string=u'Шагналын нэр')
    awards_year = fields.Char(string=u'Он')
    org_name = fields.Char(string=u'Шагнал олгосон байгууллагын нэр')
    hr_employee_id = fields.Many2one('hr.employee',string='Hr employee')


class urlag_skill(models.Model):
    _name = 'urlag.skill'
 
    name = fields.Char(string=u'Урлаг/Спортын төрөл')
    urlag_time = fields.Char(string=u'Хичээллэсэн хугацаа')
    urlag_name = fields.Char(string=u'Зэрэг, цол')
    urlag_reason = fields.Char(string=u'Тайлбар')
    hr_employee_id = fields.Many2one('hr.employee',string='Hr employee')


class hr_relationship(models.Model):
    _name = 'hr.relationship'
    
    who = fields.Char(string='Who')
    last_name = fields.Char(string='Last name')
    first_name = fields.Char(string='First name')
    age = fields.Integer(string='Age', size=3)
    gender = fields.Selection([('male', 'Male'), ('female', 'Female')], string='Gender')
    work_organization = fields.Char(string='Work organization')
    position = fields.Char(string='Position')
    phone = fields.Char(string='Phone', size=10)
    salar = fields.Float(string='Salary')
    hr_employee_id = fields.Many2one('hr.employee',string='Hr employee')
    #hr_applicant_id = fields.Many2one('hr.applicant',string='Hr applicant')



class live_people(models.Model):
    _name = 'live.people'
    
    who = fields.Selection([('Father', 'Эцэг'),
                            ('Mother', 'Эх'),
                            ('Grandfather', 'Өвөө'),
                            ('Grandmother', 'Эмээ'),
                            ('husband', 'Нөхөр'),
                            ('wife', 'Эхнэр'),
                            ('child', 'Хүүхэд'),
                            ('brother', 'Ах'),
                            ('sister', 'Эгч')],
                            string='Who')
    last_name = fields.Char(string='Last name')
    first_name = fields.Char(string='First name')
    is_child = fields.Boolean('Is your child')
    identification_id = fields.Char(string='Identification No', groups="hr.group_hr_user", tracking=True)
    live_birth = fields.Date('Live birth')
    age = fields.Integer(string='Age', size=3)
    gender = fields.Selection([('male', 'Male'), ('female', 'Female')], string='Gender')
    work_organization = fields.Char(string='Work organization')
    position = fields.Char(string='Position')
    #where = fields.Char(string=u'Хаана')
    phone = fields.Char(string='Phone', size=10)
    #salary = fields.Float(string='Salary')
    explanation = fields.Char('Explanation')
    hr_employee_id = fields.Many2one('hr.employee',string='Hr employee')
    #hr_applicant_id = fields.Many2one('hr.applicant',string='Hr applicant')
    
    
    def compute_all_child(self):
        for child in self:
            child.autochanged_age()
        return True
    
    def autochanged_age(self):
        res = {}
        now_age = 0
        year = 0
        month = 0
        day = 0
        year1 = 0
        month1 = 0
        day1 = 0
        diff_year = 0
        diff_month = 0
        diff_day = 0
        if  self.live_birth:
            date_from = datetime.strptime(str(self.live_birth), DATE_FORMAT)
            date_to = datetime.today()
            diff = relativedelta.relativedelta(date_to, date_from)
            now_age = diff.years
        else:
            pass
        self.write({'age': now_age})
        return True
    
class hr_award(models.Model):
    _name = 'hr.award'
    
    
    award_name = fields.Char(string='Award name')
    award_year = fields.Char(string='Award year',size=4)
    award_where = fields.Char(string='Where do awarded during')
    hr_employee_id = fields.Many2one('hr.employee',string='Hr employee')
    
    


class Department(models.Model):
    _inherit = "hr.department"
    
    code = fields.Char(string='Code')
    food_money = fields.Many2one('food.money.register',string=u'Хоолны мөнгө',required=True)
    
    
    
    
class Credit_hours(models.Model):
    _name = 'credit.hours'
    _description = 'Credit hours' # Кредит цаг
    
    hr_employee_id = fields.Many2one('hr.employee',string='Employee') #Ажилтан
    last_name = fields.Char(string='Last name') #Овог
    company_id = fields.Many2one('res.company',string='Company') #Компани
    department_id = fields.Many2one('hr.department',string='Нэгж') #Хэлтэс
    job_id = fields.Many2one('hr.job',string='Job') #Албан тушаал
    profession = fields.Char(string='Profession', size=256) #Боловсрол мэргэжил, мэргэшил
    
    date = fields.Date(string='Date') #Огноо
    topic = fields.Char(string='Topic', size=256) #Сэдэв
    result = fields.Float(string='Result') # Шалгалтын дүн
    measures_taken = fields.Char(string='Measures taken', size=256) #Хаана
    hours_credit = fields.Float(string='Credit', size=256) #Кредит цаг
    
    
    @api.onchange('hr_employee_id')
    def onchange_employee_id(self):
        res = {}
        list = []
        if self.hr_employee_id:
            employee_obj = self.env['hr.employee']
            emp_id = employee_obj.search([('id', '=', self.hr_employee_id.id)])
            if emp_id:
                res['last_name'] = emp_id.last_name
                res['company_id'] = emp_id.company_id.id
                res['department_id'] = emp_id.department_id.id
                res['job_id'] = emp_id.job_id.id
        else:
            res['last_name'] = False
            res['company_id'] = False
            res['department_id'] = False
            res['job_id'] = False
            
        return {'value':res}


        
class app_employment(models.Model):
    _name = 'app_employment'

    name = fields.Char(string='Байгууллагын нэр')
    org_start = fields.Date(string='Ажилд орсон огноо')
    org_finish = fields.Date(string='Ажлаас гарсан огноо')
    salary_size = fields.Float(string='Цалингийн хэмжээ')
    org_position = fields.Char(string='Албан тушаал')
    work_schedule = fields.Char(string='Work schedule')
    org_reason = fields.Char('Ажлаас гарсан шалтгаан')
    supervisor_name = fields.Char('Удирдах албан тушаалтны нэр')
    supervisor_phone = fields.Char('Удирдах албан тушаалтны дугаар')
    applicant_id = fields.Many2one('hr.applicant','Hr employee')
    resume_line_id = fields.Many2one('hr.resume.line', 'Created Resume line')
    #hr_applicant_id = fields.Many2one('hr.applicant','Hr applicant')
    
    is_ndd = fields.Boolean('НДШ төлсөн эсэх')#НДШ төлсөн эсэх


class app_hr_relationship(models.Model):
    _name = 'app_hr_relationship'
    
    who = fields.Char(string='Таны юу болох?')
    last_name = fields.Char(string='Овог')
    first_name = fields.Char(string='Нэр')
    age = fields.Integer(string='Нас', size=3)
    gender = fields.Selection([('male', 'Male'), ('female', 'Female')], string='Хүйс')
    work_organization = fields.Char(string='Work organization')
    position = fields.Char(string='Албан тушаал')
    phone = fields.Char(string='Утасны дугаар', size=10)
    salar = fields.Float(string='Salary')
    applicant_id = fields.Many2one('hr.applicant',string='Hr employee')
    #hr_applicant_id = fields.Many2one('hr.applicant',string='Hr applicant')



class app_live_people(models.Model):
    _name = 'app_live_people'
    
    who = fields.Selection([('Father', 'Эцэг'),
                            ('Mother', 'Эх'),
                            ('Grandfather', 'Өвөө'),
                            ('Grandmother', 'Эмээ'),
                            ('husband', 'Нөхөр'),
                            ('wife', 'Эхнэр'),
                            ('child', 'Хүүхэд'),
                            ('brother', 'Ах'),
                            ('sister', 'Эгч')],
                            string='Таны юу болох?')
    last_name = fields.Char(string='Овог')
    first_name = fields.Char(string='Нэр')
    is_child = fields.Boolean('Төрсөн хүүхэд эсэх?')
    identification_id = fields.Char(string='Регистрийн дугаар', groups="hr.group_hr_user", tracking=True)
    live_birth = fields.Date('Төрсөн огноо')
    age = fields.Integer(string='Нас', size=3)
    gender = fields.Selection([('male', 'Male'), ('female', 'Female')], string='Хүйс')
    work_organization = fields.Char(string='Ажлын газар')
    position = fields.Char(string='Албан тушаал')
    #where = fields.Char(string=u'Хаана')
    phone = fields.Char(string='Утасны дугаар', size=10)
    #salary = fields.Float(string='Salary')
    explanation = fields.Char('Тайлбар')
    applicant_id = fields.Many2one('hr.applicant',string='Hr employee')               

    
class app_Education(models.Model):
    _name = 'app_education'     #Боловсрол
    
    graduate_school = fields.Char(string='Төгссөн сургууль, дамжаа', size=256)
    where = fields.Char(string='Хаана', size=256)
    starting_year = fields.Date(string='Эхэлсэн огноо')
    ending_year = fields.Date(string='Дууссан огноо')
    qualifications = fields.Char(string='qualifications', size=64)
    occupation = fields.Char(string='Ажил мэргэжил', size=64)
    edu_rate = fields.Float(string='Голч', size=3)
    edu_percent = fields.Float(string='edu percent', size=3)
    edu_payment_type = fields.Selection([('exhibition', 'Exhibition'),('credit', 'Credit'),('self', 'Self')], string='Payment type')
    type_id = fields.Many2one('hr.education.degree', string='Боловсролын зэрэг')
    applicant_id = fields.Many2one('hr.applicant',string='Hr employee')
    #hr_applicant_id = fields.Many2one('hr.applicant',string='Hr applicant')
    
    
    
class app_training_course(models.Model):
    _name = 'app_training_course'

    where = fields.Char(string='Хаана')
    when = fields.Char(string='Хэзээ')
    time = fields.Char(string='Хугацаа')
    training_profession = fields.Char(string='Ямар чиглэлээр')
    applicant_id = fields.Many2one('hr.applicant',string='Hr employee')

class Applicant(models.Model):
    _inherit = "hr.applicant"
    
    accession_date = fields.Date('Ажилд орсон огноо')#Ажилд орсон огноо
    education_ids = fields.One2many('app_education','applicant_id',string=u'Боловсрол')
    training_course_ids = fields.One2many('app_training_course','applicant_id',string=u'Мэргэжил дээшлүүлэх сургалтын мэдээлэл')
    employment_ids = fields.One2many('app_employment','applicant_id',string='Employment')
    live_people_ids = fields.One2many('app_live_people','applicant_id',string=u'Гэр бүлийн мэдээлэл')
    hr_relationship_ids = fields.One2many('app_hr_relationship','applicant_id',string=u'Ураг төрлийн мэдээлэл')
    last_name = fields.Char(string='Овог')
    identification_id = fields.Char(string=u'Регистрийн дугаар', groups="hr.group_hr_user", tracking=True,required=True,size=10)
    tin_number = fields.Char(string=u'ТТДугаар',required=True,size=12)
    emp_type = fields.Many2one('emp.type',string=u'Статус',required=True)
    surname = fields.Char(string='Surname')
    ndd_type_id = fields.Many2one('inshurance.type',string='Даатгуулагчийн төрөл')
    is_solder = fields.Boolean(string=u'Цэрэгт явсан эсэх')
    blood_type = fields.Selection([('1', '1'), 
                                        ('2', '2'),
                                        ('3', '3'), 
                                        ('4', '4')], 
                                        string='Цусны бүлэг')
    employee_age = fields.Integer('Нас')
    
    family = fields.Integer(string='Ам бүлийн тоо', groups="hr.group_hr_user", tracking=True)
    children = fields.Integer(string='Хүүхдийн тоо', groups="hr.group_hr_user", tracking=True)
    
    driver_type_id=fields.Many2many('driver.type', 'emp_driver_type_hr_rel', 'driver_type_id', 'applicant_id', 'Жолооны ангилал')#Жолооны ангилал
    
    dependence_address = fields.Many2one('res.country.state',string=u'Аймаг/Хот')
    khoroo_id = fields.Many2one('khoroo',string=u'Баг/Хороо')
    khoroo_id2 = fields.Char(string=u'Баг/Хороо')
    sum_duureg_id = fields.Many2one('sum.duureg',string=u'Сум/Дүүрэг')
    desc_id = fields.Char(string=u'Хашаа/Байрны дугаар, тоот')
    desc_id1 = fields.Char(string=u'Хашаа/Байрны дугаар, тоот')
    employment_description = fields.Text('Employment description')
    home_phone=fields.Char('Гэрийн утас')#Гэрийн утас
    map_home_work=fields.Char(string =u'W3W',help="What3words хаяг оруулна")
    map_home_work1=fields.Char(string =u'W3W',help="What3words хаяг оруулна")
    
    
    def create_employee_from_applicant(self):
        """ Create an hr.employee from the hr.applicants """
        employee = False
        for applicant in self:
            contact_name = False
            if applicant.partner_id:
                address_id = applicant.partner_id.address_get(['contact'])['contact']
                contact_name = applicant.partner_id.display_name
            else:
                if not applicant.partner_name:
                    raise ValidationError(_('You must define a Contact Name for this applicant.'))
                new_partner_id = self.env['res.partner'].create({
                    'is_company': False,
                    'type': 'private',
                    'name': applicant.partner_name,
                    'email': applicant.email_from,
                    'phone': applicant.partner_phone,
                    'mobile': applicant.partner_mobile
                })
                address_id = new_partner_id.address_get(['contact'])['contact']
            if applicant.partner_name or contact_name:
                employee = self.env['hr.employee'].create({
                    'name': applicant.partner_name or contact_name,
                    'job_id': applicant.job_id.id or False,
                    'job_title': applicant.job_id.name,
                    'address_home_id': address_id,
                    'department_id': applicant.department_id.id or False,
                    'address_id': applicant.company_id and applicant.company_id.partner_id
                            and applicant.company_id.partner_id.id or False,
                    'work_email': applicant.department_id and applicant.department_id.company_id
                            and applicant.department_id.company_id.email or False,
                    'work_phone': applicant.department_id and applicant.department_id.company_id
                            and applicant.department_id.company_id.phone or False,
                    'accession_date': applicant.accession_date,})
                applicant.write({'emp_id': employee.id})
                if applicant.job_id:
                    applicant.job_id.write({'no_of_hired_employee': applicant.job_id.no_of_hired_employee + 1})
                    applicant.job_id.message_post(
                        body=_('New Employee %s Hired') % applicant.partner_name if applicant.partner_name else applicant.name,
                        subtype="hr_recruitment.mt_job_applicant_hired")
                applicant.message_post_with_view(
                    'hr_recruitment.applicant_hired_template',
                    values={'applicant': applicant},
                    subtype_id=self.env.ref("hr_recruitment.mt_applicant_hired").id)

        employee_action = self.env.ref('hr.open_view_employee_list')
        dict_act_window = employee_action.read([])[0]
        dict_act_window['context'] = {'form_view_initial_mode': 'edit'}
        dict_act_window['res_id'] = employee.id
        return dict_act_window
    
    

#===============================================================================
# class HolidaysRequest(models.Model):
#     _inherit = "hr.leave"
#     
#     company_id = fields.Many2one('res.company', string='Company of Employee', states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
# 
#     @api.onchange('employee_id')
#     def _onchange_employee_id(self):
#         self._sync_employee_details()
#         if self.employee_id.user_id != self.env.user and self._origin.employee_id != self.employee_id:
#             self.holiday_status_id = False
#         if self.employee_id.company_id:
#             self.company_id = self.employee_id.company_id.id
#         else:
#             self.company_id = False
#===============================================================================
    
#===============================================================================
#     @api.model_create_multi
#     def create(self, vals_list):
#         """ Override to avoid automatic logging of creation """
#         if not self._context.get('leave_fast_create'):
#             leave_types = self.env['hr.leave.type'].browse([values.get('holiday_status_id') for values in vals_list if values.get('holiday_status_id')])
#             mapped_validation_type = {leave_type.id: leave_type.validation_type for leave_type in leave_types}
# 
#             for values in vals_list:
#                 employee_id = values.get('employee_id', False)
#                 leave_type_id = values.get('holiday_status_id')
#                 # Handle automatic department_id
#                 if not values.get('department_id'):
#                     values.update({'department_id': self.env['hr.employee'].browse(employee_id).department_id.id})
# 
#                 # Handle no_validation
#                 if mapped_validation_type[leave_type_id] == 'no_validation':
#                     values.update({'state': 'confirm'})
# 
#                 # Handle double validation
#                 if mapped_validation_type[leave_type_id] == 'both':
#                     self._check_double_validation_rules(employee_id, values.get('state', False))
# 
#         holidays = super(HolidaysRequest, self.with_context(mail_create_nosubscribe=True)).create(vals_list)
# 
#         for holiday in holidays:
#             if self._context.get('import_file'):
#                 holiday._onchange_leave_dates()
#             if not self._context.get('leave_fast_create'):
#                 # FIXME remove these, as they should not be needed
#                 if employee_id:
#                     holiday.with_user(SUPERUSER_ID)._sync_employee_details()
#                 if 'number_of_days' not in values and ('date_from' in values or 'date_to' in values):
#                     holiday.with_user(SUPERUSER_ID)._onchange_leave_dates()
# 
#                 # Everything that is done here must be done using sudo because we might
#                 # have different create and write rights
#                 # eg : holidays_user can create a leave request with validation_type = 'manager' for someone else
#                 # but they can only write on it if they are leave_manager_id
#                 holiday_sudo = holiday.sudo()
#                 holiday_sudo.add_follower(employee_id)
#                 if holiday.validation_type == 'manager':
#                     holiday_sudo.message_subscribe(partner_ids=holiday.employee_id.leave_manager_id.partner_id.ids)
#                 if holiday.holiday_status_id.validation_type == 'no_validation':
#                     # Automatic validation should be done in sudo, because user might not have the rights to do it by himself
#                     holiday_sudo.action_validate()
#                     holiday_sudo.message_subscribe(partner_ids=[holiday_sudo._get_responsible_for_approval().partner_id.id])
#                     holiday_sudo.message_post(body=_("The time off has been automatically approved"), subtype="mt_comment") # Message from OdooBot (sudo)
#                 elif not self._context.get('import_file'):
#                     holiday_sudo.activity_update()
#                     
#             #edited by Tengersoft - start
#             
#             if 'holiday_status_id' in vals:
#                 hr_leave_type_obj = self.env['hr.leave.type'].search([('id','=',vals['holiday_status_id'])])
#                 if hr_leave_type_obj and hr_leave_type_obj.new_type == 'legal_leave':
#                     if vals['holiday_type'] == 'employee':
#                         emp_id = self.env['hr.employee'].search([('id','=',vals['employee_id'])])
#                         if emp_id.working_year == 0 and emp_id.working_year_month < 11:
#                             raise ValidationError(_('Your worked year of the organization is not enough. It must be 11 month and more!'))
#                         else:
#                             year_start = datetime.strptime(str(vals['request_date_from']), '%Y-%m-%d').replace(month=1,day=1)
#                             year_end = datetime.strptime(str(vals['request_date_from']), '%Y-%m-%d').replace(month=12,day=31)
#                             print('OOOOOOOOOOOOOOO', year_start, year_end)
#                             #awsan bga eeljiin amraltiin honog
#                             done_legal = 0.0
#                             #niit awah bolomjtoi eeljiin amraltiin honog
#                             available_legal = 0.0
#                             #vldej bui awah bolomjtoi honog
#                             remaining_legal = 0.0
#                             #ajiltnii tuhain ulsad ajillasan zowhon jil
#                             sen_year = 0
#                             #ajiltnii tuhain ulsad ajillasan zowhon sar
#                             sen_month = 0
#                             #ajiltnii tuhain ulsad ajillasan jil
#                             total_wy = 0
#                             #awsan bga eeljiin amraltiig haij oloh
#                             done_legal_search = self.env['hr.leave'].search([
#                                                                             ('holiday_status_id','=',vals['holiday_status_id']),
#                                                                             ('state','=','validate'),
#                                                                             ('holiday_type','=','employee'),
#                                                                             ('employee_id','=',vals['employee_id']),
#                                                                             ('request_date_from','>=',year_start),
#                                                                             ('request_date_to','<=',year_end)
#                                                                             ])
#                             print('111111', done_legal_search)
#                             if done_legal_search:
#                                 for line1 in done_legal_search:
#                                     done_legal = done_legal + line1.number_of_days
#                             
#                             
#                             sen_year = emp_id.seniority
#                             sen_month = emp_id.seniority_month
#                             if sen_month <= 0:
#                                 total_wy = sen_year
#                             else:
#                                 total_wy = sen_year + 1
#                             
#                             hr_holiday_days_id = self.env['hr.holiday.days'].search([('min_year','<=',total_wy),('max_year','>=',total_wy)])
#                             print ('GGGGGGG', hr_holiday_days_id, hr_holiday_days_id.base_days, hr_holiday_days_id.get_days)
#                             available_legal = hr_holiday_days_id.base_days + hr_holiday_days_id.get_days
#                             remaining_legal = available_legal - done_legal
#                         
#                         print ('BBBBBBB', vals['number_of_days'], remaining_legal)
#                         if vals['number_of_days'] > remaining_legal:
#                             raise ValidationError(_('Remaining legal leave is %s, Requesting legal leave is %s. It must be Requesting <= Remaining! You need to change value of Requesting legal leave, then try again!'%(remaining_legal, vals['number_of_days'])))
#                         
#             #edited by Tengersoft - end
#                     
#         return holidays
#     
#     def write(self, values):
#         is_officer = self.env.user.has_group('hr_holidays.group_hr_holidays_user')
# 
#         if not is_officer:
#             if any(hol.date_from.date() < fields.Date.today() for hol in self):
#                 raise UserError(_('You must have manager rights to modify/validate a time off that already begun'))
# 
#         employee_id = values.get('employee_id', False)
#         if not self.env.context.get('leave_fast_create'):
#             if values.get('state'):
#                 self._check_approval_update(values['state'])
#                 if any(holiday.validation_type == 'both' for holiday in self):
#                     if values.get('employee_id'):
#                         employees = self.env['hr.employee'].browse(values.get('employee_id'))
#                     else:
#                         employees = self.mapped('employee_id')
#                     self._check_double_validation_rules(employees, values['state'])
#             if 'date_from' in values:
#                 values['request_date_from'] = values['date_from']
#             if 'date_to' in values:
#                 values['request_date_to'] = values['date_to']
#         result = super(HolidaysRequest, self).write(values)
#         if not self.env.context.get('leave_fast_create'):
#             for holiday in self:
#                 if employee_id:
#                     holiday.add_follower(employee_id)
#                     self._sync_employee_details()
#                 if 'number_of_days' not in values and ('date_from' in values or 'date_to' in values):
#                     holiday._onchange_leave_dates()
#         return result
#===============================================================================
    
    