# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import da

class record_employee_list(models.Model):
    _name = 'record.employee.list'

    name = fields.Char('Хийгдэх ажлууд', required=True)
    department_id = fields.Many2one('hr.department', 'Хэлтэс')
    support_id = fields.Many2many('hr.job', string=u'Албан тушаал')
