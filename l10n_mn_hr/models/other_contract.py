# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import da

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

_logger = logging.getLogger(__name__)



class other_contract(models.Model):
    _name = 'other.contract'

    name=fields.Char('Contract name', required=True)
    contract_number=fields.Char('Contract number', default=lambda self: self.env['ir.sequence'].next_by_code('other.contract'))
    contract_type=fields.Selection([('nuuts_hadgalah',u'НУУЦ ХАДГАЛАХ ГЭРЭЭ'),
                                        ('togtwortoi_ajillah',u'ТОГТВОРТОЙ АЖИЛЛАХ НӨХЦӨЛ БҮХИЙ ГЭРЭЭ'),
                                        ('NHO',u'ХӨДӨЛМӨРИЙН ГЭРЭЭ-НХО'),
                                        ('erhlegch',u'ХӨДӨЛМӨРИЙН ГЭРЭЭ-Эрхлэгч'),
                                        ('office_gtba',u'ХӨДӨЛМӨРИЙН ГЭРЭЭ-Оффис,ГТБА'),
                                        ('SHTS',u'ХӨЛСӨӨР АЖИЛЛАХ ГЭРЭЭ'),
                                        ('ed_hurungu',u'ЭД ХӨРӨНГИЙН БҮРЭН ХАРИУЦЛАГЫН ГЭРЭЭ')], 'Contract type', required=True)
    agent_name=fields.Many2one('hr.employee', 'Agent name', required=True)
    last_name=fields.Char('Last name')
    hr_employee_id=fields.Many2one('hr.employee', 'First name', required=True)
    position=fields.Char('Position')
    company_id = fields.Many2one('res.company', 'Company')
    department_name=fields.Char('Department')
    date_start=fields.Datetime('Date start')
    date_end=fields.Datetime('Date end')
    #definition=fields.Text('Definition', required=True)
    notes=fields.Text('Notes')
    state=fields.Selection([
        ('draft', 'Draft'),
        ('cancel', 'Cancel'),
        ('approve', 'Approve'),
        ], 'State', default='draft')
    
    def action_print(self):
        if self.contract_type == 'nuuts_hadgalah':
            return self.env.ref('l10n_mn_hr.report_geree_nuuts_hadgalah_data_data').report_action(self)
        
        if self.contract_type == 'togtwortoi_ajillah':
            return self.env.ref('l10n_mn_hr.report_geree_togtwortoi_ajillah_datas_new').report_action(self)

        if self.contract_type == 'NHO':
            return self.env.ref('l10n_mn_hr.report_geree_NHO_data_new').report_action(self)

        if self.contract_type == 'erhlegch':
            return self.env.ref('l10n_mn_hr.report_geree_erhlegch_data_new').report_action(self)

        if self.contract_type == 'office_gtba':
            return self.env.ref('l10n_mn_hr.report_geree_office_gtba_data_new').report_action(self)

        if self.contract_type == 'SHTS':
            return self.env.ref('l10n_mn_hr.report_geree_SHTS_data_new').report_action(self)
        
        if self.contract_type == 'ed_hurungu':
            return self.env.ref('l10n_mn_hr.report_geree_ed_hurungu_data_new').report_action(self)

    def action_other_draft(self):
        self.write({'state': 'draft'})
        return True

    def action_other_cancel(self):
        obj = self.browse()
        emp_obj = self.env['hr.employee']
        state = ''
        i = 0
        cr.execute("""select * from other_contract as o
                        where o.hr_employee_id = %s
                            and o.state = 'approve'""" % obj.hr_employee_id.id)
        have_other = cr.dictfetchall()
        if have_other:
            for quant in have_other:
                i = i + 1
        cr.execute("""select * from hr_contract as h
                        where h.employee_id = %s
                            and h.state = 'approve'""" % obj.hr_employee_id.id)
        have_contract = cr.dictfetchall()
        if have_contract:
            state = 'employee'
        else:
            if i > 1:
                state = 'others'
            else:
                state = 'noncontract'
        emp_obj.write(obj.hr_employee_id.id, {'forum':state})
        self.write({'state': 'cancel'})
        return True

    
    def action_other_approve(self):
        #NEEEEEEEEEEH
        contract_type = ''
        if self.contract_type == 'secrets_contract':
            contract_type = 'НХ'
        elif self.contract_type == 'property_responsibility_contract':
            contract_type = 'ЭХ'
        elif self.contract_type == 'study_abroad':
            contract_type = 'ГС'
         
        i = ''
        create_year = 0
        new_create_sub = ''
        this_year = datetime.now().year
        create_year = this_year
        comp_obj = self.env['res.company']
        comp_id = comp_obj.search([('id', '=', self.company_id.id)])
        if comp_id.code:
            if len(comp_id.code) == 3:
                new_create_sub = str(contract_type) + str(create_year) + str(comp_id.code)
                self.env.cr.execute("""select contract_number from other_contract 
                            where substr(contract_number,1,9) = '%s'
                            order by contract_number desc limit 1""" % new_create_sub)
                employee_code_info = self.env.cr.dictfetchall()
                if employee_code_info:
                    for employee in employee_code_info:
                        empii = employee['contract_number']
                        if empii == None:
                            i = 1
                        else:
                            i = int(empii[10:12]) + 1
                else:
                    i = 1
            else:
                raise ValidationError(_('Компанийн код 3-н оронтой тоо байх ёстой!!!'))
        else:
            raise ValidationError(_('Компанийн код байхгүй байна. Компанийн код 3-н оронтой тоо байх ёстой!!!'))
           
        code = u'%s' % new_create_sub + u'%s' % str(i).zfill(3)
        #NEEEEEEEEEEEH - togsgol
        
        
        
        #vals['contract_number'] = code
        
        #=======================================================================
        # emp_obj = self.env['hr.employee']
        # cr.execute("""select company_code
        #             from 
        #                 public.res_company as c,
        #                 public.hr_employee as e,
        #                 public.resource_resource as r
        #             where
        #                 e.id = %s and
        #                 r.id = e.resource_id AND
        #                 c.id = r.company_id""" % obj.hr_employee_id.id)
        # conts = cr.dictfetchall()
        # val1 = 0
        # val = 0
        # if conts:
        #     for cont in conts:
        #         val1 = str(cont['company_code'])
        #         if len(val1) != 4:
        #             raise osv.except_osv(_('Анхааруулга'), _("Гэрээлэгч ажилтны компанийн код буруу байна. Компанийн код 4-н оронтой байх ёстой!!! Та системийн администраторт хандана уу."))
        #         else:
        #             val2 = ''
        #             if obj.contract_type == 'secrets_contract':
        #                 val2 = 30
        #             elif obj.contract_type == 'property_responsibility_contract':
        #                 val2 = 40
        #             elif obj.contract_type == 'study_abroad':
        #                 val2 = 50
        #             else:
        #                 raise osv.except_osv(_('Error!'), _("You must select the contract_type"))
        #             
        #             vall = str(val1) + str(val2)
        #             cr.execute("select contract_number from other_contract where contract_number like '" + vall + "%' order by contract_number desc limit 1")
        #             cont_code = cr.dictfetchall()
        #             if cont_code:
        #                 cont_code = cont_code[0]
        #                 valar = cont_code['contract_number']
        #                 val = int(valar[6:])
        #             
        # else:
        #     raise ValidationError(_('Анхааруулга!'), _("Ажилтны компани талбар хоосон байна!!!"))
        # 
        # val = val + 1
        # contract_number1 = u'%s' % cont['company_code'] + u'%s' % val2 + u'%s' % str(val).zfill(3)
        #=======================================================================
        
        # busad geree batlahad ajiltnii hr_employee medeelliin tolow solih
        #=======================================================================
        # state = ''
        # cr.execute("""select * from hr_contract as h
        #                 where h.employee_id = %s
        #                     and h.state = 'approve'""" % obj.hr_employee_id.id)
        # have_contract = cr.dictfetchall()
        # if have_contract:
        #     state = 'employee'
        # else:
        #     state = 'others'
        # emp_obj.write(obj.hr_employee_id.id, {'forum':state})
        #=======================================================================
        
        # busad geree buyu oor bdre tolow shiljvvlj, contract_number bodoh
        #NEEEEEEEH
        self.write({'state': 'approve',
                    'contract_number' : code})
        return True
    
    @api.onchange('hr_employee_id')
    def onchange_other_contract(self):
        res = {}
        if self.hr_employee_id:
            empl_obj = self.env['hr.employee']
            empl = empl_obj.browse(self.hr_employee_id.id)
            res['last_name'] = empl.last_name
            res['company_id'] = empl.company_id.id
            res['department_name'] = empl.department_id.name
            res['position'] = empl.job_id.name
            return {'value':res}
        
        
        