# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging

#===============================================================================
# from openerp import SUPERUSER_ID
# from openerp import tools, api
# from openerp.modules.module import get_module_resource
# from openerp.tools.translate import _
# from datetime import date, timedelta, datetime
# import time
# from calendar import month, day_abbr, day_name
# from dateutil import rrule
# import math
# from operator import countOf
# from openerp import models
#===============================================================================

import base64
import logging

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY
from odoo import api, fields, models, SUPERUSER_ID, tools

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta


class routing_slip(models.Model):
    _name = 'routing.slip'
    _order = 'employee_id desc'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    department_id = fields.Many2one('hr.department', u'Нэгж', required=True)
    employee_id = fields.Many2one('hr.employee', 'Employee', copy=False)
    company_id = fields.Many2one('res.company','Company')
    job_id = fields.Many2one('hr.job', 'Job')
    accession_date = fields.Date('Accession date')
    left_date = fields.Date('Left date')
    last_name = fields.Char('Last name')
    identification_id = fields.Char(string='Ригестрийн дугаар')
    employee_code = fields.Char('Employee code')  # Ажилтны код
    out_reason_id = fields.Many2one('out.reason', 'Out reason')
    departure_reason = fields.Selection([
        ('fired', u'Байгууллагын санаачилгаар'),
        ('resigned', u'Өөрийн хүсэлтээр'),
        ('retired', u'Гэрээний хэрэгжилтийг дүгнэсэн протокол/байгууллагын санаачилгаар'),
        ('retired1', u'Гэрээний хэрэгжилтийг дүгнэсэн протокол/ажилтны санаачилгаар')
    ], string="Чөлөөлөх шалтгаан", default="fired")
    routing_date = fields.Date('Routing date', default = datetime.today())
    slip_line_id = fields.One2many('routing.slip.line', 'routing_slip_id', 'Line')
    state = fields.Selection([('draft', 'Ноорог'),  # Ноорог
                              ('send', 'Илгээх'),  # Илгээх
                              # ('routing_print', 'Тойрох хэвлэж өгсөн'),  # Тойрох хэвлэж өгсөн
                              ('routing_ended', 'Тойрох зуруулж дууссан'),  # Тойрох зуруулж дууссан
                              ('leaved_job', 'Ажлаас гарсан'),  # Ажлаас гарсан

                              ], 'State', default='draft')  # Төлөв

    sec_deposit = fields.Selection([('olgono','Олгоно'),('olgohgui','Олгохгүй')], 'Барьцаа мөнгө', default='olgono', required=True)

    shts_tootsoo_eseh = fields.Boolean('Ажилтан ажлаа бүрэн хүлээлгэн өгсөн эсэх, Хөдөлмөр хамгааллын хувцас, хэрэгслийг хүлээн авах, Багаж хэрэгсэл, бусад')
    shts_tootsoo_text = fields.Char(compute='_compute_tootsoo_texts', string='Тооцоотой эсэх')
    shts_note = fields.Text('Тэмдэглэл')
    shts_date = fields.Date('Огноо')
    shts_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')
    shuud_tootsoo_eseh = fields.Boolean('LN, Гүүр системийн эрхийг IT инженер, хяналтын Нягтлан бодогчоор хаалгах')
    shuud_note = fields.Text('Тэмдэглэл')
    shuud_date = fields.Date('Огноо')
    shuud_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')

    shatahuun_tootsoo_eseh = fields.Boolean('Шатахууны дутагдлын тооцоог хийх')
    shatahuun_tootsoo_text = fields.Char(compute='_compute_tootsoo_texts', string='Тооцоотой эсэх')
    shatahuun_note = fields.Text('Тэмдэглэл')
    shatahuun_date = fields.Date('Огноо')
    shatahuun_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')

    hodolmor_tootsoo_eseh = fields.Boolean('Хөдөлмөр хамгааллын хувцас, хэрэгслийн нормын гүйцэтгэлийг хянах, үнэлэх ')
    hodolmor_note = fields.Text('Тэмдэглэл')
    hodolmor_date = fields.Date('Огноо')
    hodolmor_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')
    hodolmor_tootsoo_text = fields.Char(compute='_compute_tootsoo_texts', string='Тооцоотой эсэх')

    surgalt_tootsoo_eseh = fields.Boolean('Сургалтын төлбөрийн тооцоотой эсэх ')
    surgalt_note = fields.Text('Тэмдэглэл')
    surgalt_date = fields.Date('Огноо')
    surgalt_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')
    surgalt_tootsoo_text = fields.Char(compute='_compute_tootsoo_texts', string='Тооцоотой эсэх')
    company_tootsoo_eseh = fields.Boolean('Компанийн Facebook группээс хасах ')
    company_note = fields.Text('Тэмдэглэл')
    company_date = fields.Date('Огноо')
    company_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')
    company_tootsoo_text = fields.Char(compute='_compute_tootsoo_texts', string='Тооцоотой эсэх')
    yriltslaga_tootsoo_eseh = fields.Boolean('Ажилтантай ажлаас гарах ярилцлага хийх ')
    yriltslaga_note = fields.Text('Тэмдэглэл')
    yriltslaga_date = fields.Date('Огноо')
    yriltslaga_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')
    yriltslaga_tootsoo_text = fields.Char(compute='_compute_tootsoo_texts', string='Тооцоотой эсэх')
    medeelel_tootsoo_eseh = fields.Boolean('ХН-ийн Green ERP програмаас  мэдээллийг хасах ')
    medeelel_note = fields.Text('Тэмдэглэл')
    medeelel_date = fields.Date('Огноо')
    medeelel_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')
    medeelel_tootsoo_text = fields.Char(compute='_compute_tootsoo_texts', string='Тооцоотой эсэх')
    hogshil_tootsoo_eseh = fields.Boolean('Ажилтантай үндсэн хөрөнгө, эд хогшлыг хүлээлцэх ')
    hogshil_note = fields.Text('Тэмдэглэл')
    hogshil_date = fields.Date('Огноо')
    hogshil_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')
    hogjil_tootsoo_text = fields.Char(compute='_compute_tootsoo_texts', string='Тооцоотой эсэх')
    or_tootsoo_eseh = fields.Boolean('Ажилтны өр, зээл, авлага, ээлжийн амралт, цалинтай холбоотой тооцоог хийх ')
    or_note = fields.Text('Тэмдэглэл')
    or_date = fields.Date('Огноо')
    or_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')
    or_tootsoo_text = fields.Char(compute='_compute_tootsoo_texts', string='Тооцоотой эсэх')
    ndd_tootsoo_eseh = fields.Boolean('НДД-ийн  нийгмийн даатгал төлөлтийн бичилт хийх, баталгаажуулж хүлээлгэн өгөх ')
    ndd_note = fields.Text('Тэмдэглэл')
    ndd_date = fields.Date('Огноо')
    ndd_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')
    ndd_tootsoo_text = fields.Char(compute='_compute_tootsoo_texts', string='Тооцоотой эсэх')
    toiroh_tootsoo_eseh = fields.Boolean('Тойрох хуудас хүлээн авах, ажлаас чөлөөлсөн тушаалыг хүлээлгэн өгөх')
    toiroh_note = fields.Text('Тэмдэглэл')
    toiroh_date = fields.Date('Огноо')
    toiroh_user_id = fields.Many2one('res.users', string='Шалгасан ажилтан')
    toiroh_tootsoo_text = fields.Char(compute='_compute_tootsoo_texts', string='Тооцоотой эсэх')
    black_list = fields.Boolean('Хар жагсаалт орсон эсэх')

    @api.depends('shts_tootsoo_eseh', 'shatahuun_tootsoo_eseh', 'hodolmor_tootsoo_eseh',
                 'shuud_tootsoo_eseh', 'surgalt_tootsoo_eseh', 'company_tootsoo_eseh',
                 'yriltslaga_tootsoo_eseh', 'medeelel_tootsoo_eseh', 'hogshil_tootsoo_eseh',
                 'or_tootsoo_eseh', 'ndd_tootsoo_eseh', 'toiroh_tootsoo_eseh')
    def _compute_tootsoo_texts(self):
        fields_mapping = {
            'shts_tootsoo_eseh': 'shts_tootsoo_text',
            'shatahuun_tootsoo_eseh': 'shatahuun_tootsoo_text',
            'hodolmor_tootsoo_eseh': 'hodolmor_tootsoo_text',
            'shuud_tootsoo_eseh': 'shuud_tootsoo_text',
            'surgalt_tootsoo_eseh': 'surgalt_tootsoo_text',
            'company_tootsoo_eseh': 'company_tootsoo_text',
            'yriltslaga_tootsoo_eseh': 'yriltslaga_tootsoo_text',
            'medeelel_tootsoo_eseh': 'medeelel_tootsoo_text',
            'hogshil_tootsoo_eseh': 'hogjil_tootsoo_text',
            'or_tootsoo_eseh': 'or_tootsoo_text',
            'ndd_tootsoo_eseh': 'ndd_tootsoo_text',
            'toiroh_tootsoo_eseh': 'toiroh_tootsoo_text',
        }

        for record in self:
            for bool_field, text_field in fields_mapping.items():
                setattr(record, text_field, 'Тооцоотой' if not getattr(record, bool_field) else 'Тооцоогүй')

    def action_send(self):
        self.write({'state': 'send'})

    def action_return(self):
        self.write({'state': 'draft'})

    @api.onchange('shts_tootsoo_eseh', 'shts_note')
    def onchange_shts_tootsoo_eseh(self):
        if self.shts_tootsoo_eseh:
            # Хэрэв shts_tootsoo_eseh = True бол
            self.shts_date = fields.Date.today()
            self.shts_user_id = self.env.user.id
        else:
            # Хэрэв shts_tootsoo_eseh = False бол
            if self.shts_note:
                # Хэрэв тэмдэглэл бичигдсэн байвал
                self.shts_date = fields.Date.today()
                self.shts_user_id = self.env.user.id
            else:
                # Хэрэв тэмдэглэл бичигдээгүй байвал
                self.shts_date = False
                self.shts_user_id = False

    @api.onchange('shuud_tootsoo_eseh', 'shuud_note')
    def onchange_shuud_tootsoo_eseh(self):
        if self.shuud_tootsoo_eseh:
            self.shuud_date = fields.Date.today()
            self.shuud_user_id = self.env.user.id
        else:
            if self.shuud_note:
                self.shuud_date = fields.Date.today()
                self.shuud_user_id = self.env.user.id
            else:
                self.shuud_date = False
                self.shuud_user_id = False

    @api.onchange('shatahuun_tootsoo_eseh','shatahuun_note')
    def onchange_shatahuun_tootsoo_eseh(self):
        if self.shatahuun_tootsoo_eseh:
            self.shatahuun_date = fields.Date.today()
            self.shatahuun_user_id = self.env.user.id
        else:
            if self.shatahuun_note:
                self.shatahuun_date = fields.Date.today()
                self.shatahuun_user_id = self.env.user.id
            else:
                self.shatahuun_date = False
                self.shatahuun_user_id = False

    @api.onchange('hodolmor_tootsoo_eseh', 'hodolmor_note')
    def onchange_hodolmor_tootsoo_eseh(self):
        if self.hodolmor_tootsoo_eseh:
            self.hodolmor_date = fields.Date.today()
            self.hodolmor_user_id = self.env.user.id
        else:
            if self.hodolmor_note:
                self.hodolmor_date = fields.Date.today()
                self.hodolmor_user_id = self.env.user.id
            else:
                self.hodolmor_date = False
                self.hodolmor_user_id = False

    @api.onchange('surgalt_tootsoo_eseh','surgalt_note')
    def onchange_surgalt_tootsoo_eseh(self):
        if self.surgalt_tootsoo_eseh:
            self.surgalt_date = fields.Date.today()
            self.surgalt_user_id = self.env.user.id
        else:
            if self.surgalt_note:
                self.surgalt_date = fields.Date.today()
                self.surgalt_user_id = self.env.user.id
            else:
                self.surgalt_date = False
                self.surgalt_user_id = False

    @api.onchange('company_tootsoo_eseh','company_note')
    def onchange_company_tootsoo_eseh(self):
        if self.company_tootsoo_eseh:
            self.company_date = fields.Date.today()
            self.company_user_id = self.env.user.id
        else:
            if self.company_note:
                self.company_date = fields.Date.today()
                self.company_user_id = self.env.user.id
            else:
                self.company_date = False
                self.company_user_id = False

    @api.onchange('yriltslaga_tootsoo_eseh', 'yriltslaga_note')
    def onchange_yriltslaga_tootsoo_eseh(self):
        if self.yriltslaga_tootsoo_eseh:
            self.yriltslaga_date = fields.Date.today()
            self.yriltslaga_user_id = self.env.user.id
        else:
            if self.yriltslaga_note:
                self.yriltslaga_date = fields.Date.today()
                self.yriltslaga_user_id = self.env.user.id
            else:
                self.yriltslaga_date = False
                self.yriltslaga_user_id = False

    @api.onchange('medeelel_tootsoo_eseh','medeelel_note')
    def onchange_medeelel_tootsoo_eseh(self):
        if self.medeelel_tootsoo_eseh:
            self.medeelel_date = fields.Date.today()
            self.medeelel_user_id = self.env.user.id
        else:
            if self.medeelel_note:
                self.medeelel_date = fields.Date.today()
                self.medeelel_user_id = self.env.user.id
            else:
                self.shatahuun_date = False
                self.medeelel_user_id = False

    @api.onchange('hogshil_tootsoo_eseh','hogshil_note')
    def onchange_hogshil_tootsoo_eseh(self):
        if self.hogshil_tootsoo_eseh:
            self.hogshil_date = fields.Date.today()
            self.hogshil_user_id = self.env.user.id
        else:
            if self.hogshil_note:
                self.hogshil_date = fields.Date.today()
                self.hogshil_user_id = self.env.user.id
            else:
                self.hogshil_date = False
                self.hogshil_user_id = False

    @api.onchange('or_tootsoo_eseh','or_note')
    def onchange_or_tootsoo_eseh(self):
        if self.or_tootsoo_eseh:
            self.or_date = fields.Date.today()
            self.or_user_id = self.env.user.id
        else:
            if self.or_note:
                self.or_date = fields.Date.today()
                self.or_user_id = self.env.user.id
            else:
                self.or_date = False
                self.or_user_id = False

    @api.onchange('ndd_tootsoo_eseh','ndd_note')
    def onchange_ndd_tootsoo_eseh(self):
        if self.ndd_tootsoo_eseh:
            self.ndd_date = fields.Date.today()
            self.ndd_user_id = self.env.user.id
        else:
            if self.ndd_note:
                self.ndd_date = fields.Date.today()
                self.ndd_user_id = self.env.user.id
            else:
                self.ndd_date = False
                self.ndd_user_id = False

    @api.onchange('toiroh_tootsoo_eseh','toiroh_note')
    def onchange_toiroh_tootsoo_eseh(self):
        if self.toiroh_tootsoo_eseh:
            self.toiroh_date = fields.Date.today()
            self.toiroh_user_id = self.env.user.id
        else:
            if self.toiroh_note:
                self.toiroh_date = fields.Date.today()
                self.toiroh_user_id = self.env.user.id
            else:
                self.toiroh_date = False
                self.toiroh_user_id = False



    _sql_constraints = [
        ('employee_id', 'unique(employee_id,out_reason_id)', 'Field employee_id and out_reason_id must be unique.'),
        ]
    
    def to_ended(self):

        if (self.shts_user_id and
                # self.shuud_user_id and
                self.shatahuun_user_id and
                self.hodolmor_user_id and
                self.surgalt_user_id and
                self.company_user_id and
                self.yriltslaga_user_id and
                self.hogshil_user_id and
                self.or_user_id and
                self.ndd_user_id and
                self.toiroh_user_id):

            self.write({'state': 'routing_ended'})
        else:
            raise ValidationError("Бүх нөхцөл биелэгдээгүй байна.")

    def to_leaved(self):
        for contract in self:
            contracts = self.env['hr.contract'].search([
                ('employee_id', '=', contract.employee_id.id),
                ('state', 'in', ['draft', 'open', 'close'])
            ])
            for rec in contracts:
                rec.write({'state': 'cancel'})
        employee = self.env['hr.employee'].search([('id', '=', self.employee_id.id)])
        if employee:
            employee.write({
                'job_end_date': self.left_date,
                'is_black_list': self.black_list,
                'departure_reason': self.departure_reason,
                'active': False,

            })

            # Ажилтны user_id-г авах
            user_id = employee.user_id
            if user_id:
                user = self.env['res.users'].search([('id', '=', user_id.id)])
                if user:
                    user.write({
                        'active': False,
                    })
        self.write({'state': 'leaved_job'})
        return True
    
    @api.onchange('employee_id')
    def onchange_employee_id(self):
        res = {}
        list = []
        if self.employee_id:
            employee_obj = self.env['hr.employee']
            emp_id = employee_obj.search([('id', '=', self.employee_id.id)])
            if emp_id:
                res['last_name'] = emp_id.last_name
                res['company_id'] = emp_id.company_id.id
                res['job_id'] = emp_id.job_id.id
                res['accession_date'] = emp_id.accession_date
                res['identification_id'] = emp_id.identification_id
                res['employee_code'] = emp_id.employee_code
                res['department_id'] = emp_id.department_id.id
            if emp_id.job_id:
                rm_obj = self.env['routing.master']
                rm_id = rm_obj.search([('job_id', '=', emp_id.job_id.id)])
                rsl_obj = self.env['routing.slip.line']
                if rm_id:
                    for line in rm_id.master_line_id:
                        line_values = {
                            'code': line.code,
                            'company_id': line.company_id.id,
                            'job_id': line.job_id.id,
                            'employee_id': line.employee_id.id,
                            'date': line.date,
                            'note': line.note,
                            'signature': line.signature,
                            'category_ids': line.category_ids,
                            }
                        list.append((0,0,line_values))
                        res['slip_line_id'] = list
                else:
                    res['slip_line_id'] = False
        else:
            res['last_name'] = False
            res['company_id'] = False
            res['job_id'] = False
            res['accession_date'] = False
            res['slip_line_id'] = False
        # self.update({'slip_line_id': list })
        return {'value':res}
        

        
    #===========================================================================
    # @api.multi
    #===========================================================================
    def refresh_page(self):
        return { 'type': 'ir.actions.client', 'tag': 'reload'}
    
    
    def unlink(self):
        line_obj = self.env['routing.slip.line']
        for line in self.slip_line_id:
            line.unlink()
        return super(routing_slip, self).unlink()


class routing_slip_line(models.Model):
    _name = 'routing.slip.line'
    _description = 'Routing slip line'
    
    routing_slip_id = fields.Many2one('routing.slip', 'Routing slip')
    code = fields.Integer('Code')
    company_id = fields.Many2one('res.company', 'Company')
    job_id = fields.Many2one('hr.job', 'Job')
    employee_id = fields.Many2one('hr.employee', 'Employee')
    date = fields.Date('Date')
    category_ids = fields.Many2many(
        'routing.category', 'routing_category_rel',
        'rout_id', 'category_id', groups="hr.group_hr_manager",
        string='Tags')
    note = fields.Char('Note')
    signature = fields.Char('Signature')
    tootsoo_eseh = fields.Boolean('Тооцоотой эсэх')
      
    #===========================================================================
    # _sql_constraints = [
    #     ('job_id', 'unique("job_id")', 'Line field job_id must be unique.'),
    #     ]
    #===========================================================================
    
    @api.onchange('job_id')
    def onchange_job_id(self):
        obj = self.browse()
        res = {}
        if self.job_id:
            job_obj = self.env['hr.job']
            job_id = job_obj.search([('id', '=', self.job_id.id)])
            if job_id:
                res['company_id'] = job_id.company_id.id
                res['code'] = job_id.job_code
                # print 'nnnnnnnnnn', job_id
                employee_obj = self.env['hr.employee']
                emp_id = employee_obj.search([('job_id', '=', job_id.id)])
                emp = employee_obj.browse(emp_id)
                if len(emp_id) == 1:
                    res['employee_id'] = emp.id
                    
                # emp = employee_obj.browse(cr, uid, emp_id)
            
        return {'value':res}


class routing_master(models.Model):
    _name = 'routing.master'
    _description = 'Routing master'

    job_id = fields.Many2one('hr.job', 'Job', copy=False)
    master_line_id = fields.One2many('routing.master.line', 'routing_master_id', 'Line', copy=False)

    _sql_constraints = [
        ('job_id', 'unique("job_id")', 'Field job_id must be unique.'),
        ]
    
    #===========================================================================
    # def create(self, vals):
    #     res = {}
    #     
    #     routing_id = super(routing_master, self).create(vals)
    #     return routing_id
    #===========================================================================
    
    def unlink(self, vals):
        obj = self.browse()[0]
        line_obj = self.env['routing.master.line']
        for line in obj.master_line_id:
            line_obj.unlink(line.id)
        return super(routing_master, self).unlink(vals)


class routing_master_line(models.Model):
    _name = 'routing.master.line'
    _description = 'Routing master line'

    routing_master_id = fields.Many2one('routing.master', 'Routing master')
    code = fields.Integer('Code')
    company_id = fields.Many2one('res.company', 'Company')
    job_id = fields.Many2one('hr.job', 'Job', required=True)
    employee_id = fields.Many2one('hr.employee', 'Employee')
    date = fields.Date('Date')
    category_ids = fields.Many2many(
        'routing.category', 'master_category_rel',
        'master_id', 'category_id', groups="hr.group_hr_manager",
        string='Tags')
    note = fields.Char('Note')
    signature = fields.Char('Signature')
    #===========================================================================
    # _sql_constraints = [
    #     ('job_id', 'unique("job_id")', 'Line field job_id must be unique.'),
    #     ]
    #===========================================================================
    
    def create(self, vals):
        res = {}
        
        master_id = super(routing_master_line, self).create(vals)
        return master_id
    
    @api.onchange('job_id')
    def onchange_job_id(self):
        obj = self.browse()
        res = {}
        if self.job_id:
            job_obj = self.env['hr.job']
            job_id = job_obj.search([('id', '=', self.job_id.id)])
            if job_id:
                res['company_id'] = job_id.company_id.id
                res['code'] = job_id.job_code
                # print 'nnnnnnnnnn', job_id
                employee_obj = self.env['hr.employee']
                emp_id = employee_obj.search([('job_id', '=', job_id.id)])
                emp = employee_obj.browse(emp_id)
                if len(emp_id) == 1:
                    res['employee_id'] = emp.id
            
        return {'value':res}
   

    
class Job(models.Model):
    _inherit = 'hr.job'
    
    job_code = fields.Char('Job code')

    
class out_reason(models.Model):
    _name = 'out.reason'
    
    name = fields.Char('Left reason')
    left_type = fields.Selection([('company_decision', 'Company decision'), ('own_decision', 'Own decision')], 'Left type')
    left_reason_code = fields.Integer('Left reason code')
    hr_employee_id = fields.Many2one('hr.employee', 'Hr employee')


class routing_category(models.Model):
    _name = "routing.category"
    _description = "Routing Category"

    name = fields.Char(string="Tag Name", required=True)
    color = fields.Integer(string='Color Index')
    rounting_ids = fields.Many2many('routing.slip.line', 'routing_category_rel', 'category_id', 'rout_id', string='Routing category')
    master_ids = fields.Many2many('routing.master.line', 'master_category_rel', 'category_id', 'master_id', string='Master category')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "Tag name already exists !"),
    ]
