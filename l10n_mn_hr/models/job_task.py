# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging

#===============================================================================
# from openerp import SUPERUSER_ID
# from openerp import tools, api
# from openerp.modules.module import get_module_resource
# from openerp.tools.translate import _
# from datetime import date, timedelta, datetime
# import time
# from calendar import month, day_abbr, day_name
# from dateutil import rrule
# import math
# from operator import countOf
# from openerp import models
#===============================================================================

import base64
import logging

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY
from odoo import api, fields, models, SUPERUSER_ID, tools

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta


class JobTask(models.Model):
    _name = 'job.task'

    name = fields.Char('Албан тушаал')
    job_list = fields.Char('Албан тушаал')
    register_number = fields.Char('Дугаарлалт')
    user_id = fields.Many2one('res.users', 'Ажилтан', default=lambda self: self.env.user, required=True, readonly=True)
    department_id = fields.Many2one('hr.department', 'Хэлтэс')
    line_ids = fields.One2many('job.task.line', 'task_id', string='line')


class JobTaskLine(models.Model):
    _name = 'job.task.line'

    task_id = fields.Many2one('job.task', string='line_ids')
    work_task = fields.Many2one('record.employee.list', string='Хийгдэх ажлууд')
    support_employee = fields.Char('Дэмжлэг үүлзүүлэгч ажилтан')
    one_week = fields.Integer('I-р 7 хоног')
    two_week = fields.Integer('II-р 7 хоног')
    three_week = fields.Integer('III-р 7 хоног')
    four_week = fields.Integer('IV-р 7 хоног')
    five_week = fields.Integer('V-р 7 хоног')
    six_week = fields.Integer('VI-р 7 хоног')



