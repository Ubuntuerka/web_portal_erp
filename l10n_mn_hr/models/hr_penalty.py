# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import da

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

_logger = logging.getLogger(__name__)


    
class hr_penalty(models.Model):
    _name = 'hr.penalty'
    _inherit = ['mail.thread']
    _description = 'HR penalty' #Сахилга шийтгэл
    
    name = fields.Char('Last name', track_visibility='onchange')#Овог
    penalty_type = fields.Selection([
        ('penalty', 'Penalty'),#Сахилгын шийтгэл
        ('impropriety', 'Impropriety'),#Зөрчил
        ], 'Penalty type', track_visibility='onchange')#Төрөл
    penalty_name = fields.Many2one('hr.penalty.type', 'Penalty name', track_visibility='onchange')#Гаргасан зөрчил эсвэл Холбогдох хуулийн заалт
    company_id = fields.Many2one('res.company', 'Company')#Компани
    department_name = fields.Many2one('hr.department', 'Department')#Алба, хэлтэс
    position_name = fields.Many2one('hr.job', 'Position')#Албан тушаал
    date = fields.Date('Date', required=True, track_visibility='onchange')#Сахилгын шийтгэл оноосон огноо
    
    decree_id = fields.Many2one('hr.decree', 'Decree')#Тушаал
    date_end = fields.Date('Date end', track_visibility='onchange')#Сахилгын шийтгэлгүйд тооцогдох хугацаа
    is_employee_accepted = fields.Boolean('Is employee accepted', track_visibility='onchange')#Ажилтан хүлээн зөвшөөрсөн эсэх
    nonaccepted_note = fields.Text('Nonaccepted note')#Зөвшөөрөөгүй тайлбар
    need_deliver_finance = fields.Boolean('Need deliver finance', track_visibility='onchange')#Санхүүд мэдээлэл илгээх
    
    
    penalty_select = fields.Many2one('hr.imposing.penalty', 'Penalty select', required=True, track_visibility='onchange')#Ногдуулсан шийтгэл
    #deg_department = fields.Many2one('hr.department', 'Deg department', track_visibility='onchange')#Бууруулах алба, хэлтэс
    #deg_position = fields.Many2one('hr.job', 'Deg position', track_visibility='onchange')#Бууруулах албан тушаал
    #deg_wage = fields.Float('Deg wage', track_visibility='onchange')#Цалингийн хэмжээ
    note = fields.Text('Note')#Гаргасан зөрчлийн агуулга
    #is_degredation = fields.Boolean('Is degredation', track_visibility='onchange')#Албан тушаал бууруулах эсэх
    #is_salary_deduction = fields.Boolean('Is salary deduction', track_visibility='onchange')#Цалингийн зэрэг бууруулах эсэх
    is_withhold_salary = fields.Boolean('Is withhold salary', track_visibility='onchange')#Цалингаас суутгах эсэх
    percent = fields.Float('Percent', track_visibility='onchange')#Цалингаас суутгах хувь
    amount = fields.Float('Amount', track_visibility='onchange')#Цалингаас суутгах дүн
    
    #===========================================================================
    # penalty_type = fields.Selection([
    #     ('position_degredation', 'Is degredation'),
    #     ('wage_degredation', 'Wage degredation'),
    #     ('loss_of_responsibility', 'Loss of responsibility'),
    #     ('interruption_of_work', 'Interruption of work'),
    #     ('discard_labor_contract', 'Discard labor contract'),
    #     ], 'Penalty type', track_visibility='onchange')#Сахилгын төрөл
    #===========================================================================
    state = fields.Selection([
        ('nonapprove', 'Nonapprove'),
        ('approve', 'Approve'),
        ], 'State', track_visibility='onchange', default='nonapprove')#Төлөв
    
    department_id = fields.Many2one('hr.department', 'Department')#Хэлтэс
    hr_employee_id = fields.Many2one('hr.employee', 'First name', required=True, track_visibility='onchange')#Ажилтан
    hr_contract_id = fields.Many2one('hr.contract', 'Hr contract')#Хүний нөөцийн гэрээ

    def action_to_nonapprove(self):
        self.write({'state': 'nonapprove'})
        return True
    
    def action_to_approve(self):
        self.write({'state': 'approve'})
        
        emp_obj = self.env['hr.employee']
        contract_obj = self.env['hr.contract']
        job_obj = self.env['hr.job']
        asset_obj = self.env['account.asset.asset']
        partner_obj = self.env['res.partner']
        #print ('SSSSSSSSSS',self.hr_employee_id.user_id.id)
        
        #Horongo salgah hesgiig daraa n hiiheer orhiw - start
        #=======================================================================
        # partner_id = partner_obj.search([('user_id', '=', self.hr_employee_id.user_id.id)])
        # if partner_id:
        #     asset_id = asset_obj.search([('owner_id', '=', partner_id.id)])
        #     if asset_id:
        #         raise ValidationError(_('Error!'), _("You must an accumulation"))
        #=======================================================================
        #Horongo salgah hesgiig daraa n hiiheer orhiw - end
        
        #=======================================================================
        # if self.hr_employee_id.contract_id:
        #     if self.is_degredation == True:
        #         self.hr_employee_id.contract_id.write({'wage':self.deg_wage,
        #                                                 'job_id':self.deg_position.id, }),
        #         self.hr_employee_id.write({'department_id':self.deg_department.id,
        #                                     'job_id':self.deg_position.id,
        #                                     'job_grade_id':self.deg_position.job_grade_id.id,
        #                                     'job_code':self.deg_position.job_code, })
        #     
        # else:
        #     raise ValidationError(_('Error!'), _("Тухайн ажилтан хөдөлмөрийн гэрээ байгуулаагүй байна! Хөдөлмөрийн гэрээ нь хүчин төгөлдөр(батлагдсан) ажилтанд сахилга шийтгэлийг оноох боломжтой"))
        #         
        #=======================================================================
        return True

    #===========================================================================
    # def write(self, cr, uid, ids,vals,context=None):
    #     print "assssssssssssssss",vals.get('amount')
    #     if vals.get('percent'):
    #         print "logg"
    #         vals.update({'amount':vals['amount']})
    #     return super(hr_penalty,self).write(cr, uid, ids,vals, context=context)
    # 
    #===========================================================================
    
    @api.onchange('hr_employee_id','percent')
    def onchange_calculate(self):
        emp_obj = self.env['hr.employee']
        res = {}
        sum = 0
        contract_wage = 0
        if self.hr_employee_id and self.percent >= 0:
            if self.hr_employee_id.contract_id and self.hr_employee_id.contract_id.state == 'open':
                contract_wage = self.hr_employee_id.contract_id.wage
            else:
                contract_wage = 0
            sum = (self.percent * contract_wage) / 100
        res['amount'] = sum
        return {'value':res}
        
    @api.onchange('hr_employee_id')
    def onchange_penalty(self):
        res = {}
        if self.hr_employee_id:
            empl_obj = self.env['hr.employee']
            empl = empl_obj.browse(self.hr_employee_id.id)
            res['name'] = self.hr_employee_id.last_name
            res['company_id'] = self.hr_employee_id.company_id.id
            res['department_name'] = self.hr_employee_id.department_id.id
            res['position_name'] = self.hr_employee_id.job_id.id
            return {'value':res}

        
class hr_penalty_type(models.Model):
    _name = 'hr.penalty.type'
    _description = 'HR penalty type' #Гаргасан зөрчил

    name = fields.Char('Penalty name')#Гаргасан зөрчил
    penalty_code = fields.Char('Penalty code')#Гаргасан зөрчлийн код
    hr_employee_id = fields.Many2one('hr.employee', 'First name')#Ажилтан


class hr_imposing_penalty(models.Model):
    _name = 'hr.imposing.penalty'
    _description = 'HR imposing penalty'        #Ногдуулсан шийтгэл
    
    name = fields.Char('Imposing penalty')#Ногдуулах шийтгэл
    hr_employee_id = fields.Many2one('hr.employee', 'First name')#Ажилтан

