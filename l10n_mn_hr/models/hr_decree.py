import base64
import logging

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY
from odoo import api, fields, models, SUPERUSER_ID, tools

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import af

from odoo.modules.module import get_module_resource
from odoo.tools.translate import _
from calendar import month, day_abbr, day_name
from dateutil import rrule
import math
from operator import countOf

class HrEmployeePublic(models.Model):
    _inherit = "hr.employee.public"

    decree_id = fields.Many2one('hr.decree',string='Decree')#Тушаал


class HrEmployeePrivate(models.Model):
    _inherit = "hr.employee"

    decree_id = fields.Many2one('hr.decree',string='Decree')#Тушаал

class HrContract(models.Model):
    _inherit = "hr.contract"

    contract_number=fields.Char('Contract number', copy=False, default=lambda self: self.env['ir.sequence'].next_by_code('hr.contract'))
    decree_id = fields.Many2one('hr.decree',string='Decree')#Тушаал
    
''' @api.model
    def create(self, vals):
             
    # Getting 'Employee code'
        #Kompaniin kod hamaarsan 10n orontoi ajiltnii kod bolgowol doorh kodiig ashiglana
        i = ''
        create_year = 0
        new_create_sub = ''
        this_year = datetime.now().year
        create_year = this_year
        comp_obj = self.env['res.company']
        comp_id = comp_obj.search([('id', '=', vals['company_id'])])
        #------------------------------------------------------ if comp_id.code:
            #---------------------------------------- if len(comp_id.code) == 3:
                #-- new_create_sub = 'ХГ' + str(create_year) + str(comp_id.code)
                # self.env.cr.execute("""select contract_number from hr_contract
                            #---------- where substr(contract_number,1,9) = '%s'
                            #order by contract_number desc limit 1""" % new_create_sub)
                #--------------- employee_code_info = self.env.cr.dictfetchall()
                #---------------------------------------- if employee_code_info:
                    #----------------------- for employee in employee_code_info:
                        #------------------- empii = employee['contract_number']
                        #------------------------------------- if empii == None:
                            #--------------------------------------------- i = 1
                        #------------------------------------------------- else:
                            #------------------------- i = int(empii[10:12]) + 1
                #--------------------------------------------------------- else:
                    #----------------------------------------------------- i = 1
            #------------------------------------------------------------- else:
                # raise ValidationError(_('Компанийн код 3-н оронтой тоо байх ёстой!!!'))
        #----------------------------------------------------------------- else:
            # raise ValidationError(_('Компанийн код байхгүй байна. Компанийн код 3-н оронтой тоо байх ёстой!!!'))
#------------------------------------------------------------------------------ 
        code = u'%s' % new_create_sub + u'%s' % str(i).zfill(3)
        vals['contract_number'] = code
        employee_id = super(HrContract, self).create(vals)
        return employee_id '''

class OtherContract(models.Model):
    _inherit = "other.contract"

    decree_id = fields.Many2one('hr.decree',string='Decree')#Тушаал
    
class hr_pro(models.Model):
    _name = 'hr.pro'
    _inherit = ['mail.thread']
    
    last_name = fields.Char(string=u'Овог')
    employee_name = fields.Many2one('hr.employee',string=u'Нэр')
    pro_number = fields.Char(string=u'ГХДП дугаар', copy=False, default=lambda self: self.env['ir.sequence'].next_by_code('hr.pro'))#Дугаар
    date = fields.Date(string=u'Бүртгэсэн огноо', track_visibility='onchange',required=True,)#Бүртгэсэн огноо
    end_date = fields.Date(u'Дуусах огноо')
    pro_type = fields.Selection([('ajiltnii_sanaachlagaar',u'ГХДП Ажилтны санаачилгаар'),
                                ('baiguullagiin',u'ГХДП Байгууллагын санаачилгаар')],string=u'ГХДП-н төрөл')
    
    employee_id = fields.Many2one('hr.employee', string=u'Ажилтан', track_visibility='onchange')#Ажилтан
    company_id = fields.Many2one('res.company', string=u'Компани')#Компани
    department_id = fields.Many2one('hr.department', string=u'Нэгж')#Хэлтэс
    position_id = fields.Many2one('hr.job', string=u'Албан тушаал')#Албан тушаал
    state = fields.Selection([
                            ('draft', 'Draft'),#Ноорог
                            ('cancelled', 'Cancelled'),#Цуцлагдсан
                            ('approved', 'Approved'),#Батлагдсан
                            ], string='State',
                             track_visibility='onchange',  default = 'draft')
    hr_manager_name = fields.Char('Хүний нөөцийн менежер')
    note = fields.Char(string=u'Заалт')
    director = fields.Char(string=u'Захирал/ГТБА дарга', readonly=True)
    hr_contract = fields.Many2one('hr.contract', string=u'Үндсэн гэрээ', readonly=True)
    baritsaa = fields.Selection([
                            ('sent','шилжүүлнэ'),
                            ('not_sent','шилжүүлэхгүй'),
                            ], string='Барьцаа')
    
    @api.onchange('employee_id')
    def onchange_employee_id(self):
        res = {}
        list = []
        if self.employee_id:
            employee_obj = self.env['hr.employee']
            empl_id = employee_obj.search([('id', '=', self.employee_id.id)])
            if empl_id:
                res['last_name'] = empl_id.last_name
                res['company_id'] = empl_id.company_id.id
                res['department_id'] = empl_id.department_id.id
                res['position_id'] = empl_id.job_id.id
        else:
            res['last_name'] = False
            res['company_id'] = False
            res['department_id'] = False
            res['position_id'] = False
            
        return {'value':res}

    def approved(self):
        
        self.write({'state': 'approved'})
        return True
    
    def draft(self):
        
        self.write({'state': 'draft'})
        return True
    
    def cancelled(self):
        
        self.write({'state': 'cancelled'})
        return True
class hr_decree_type(models.Model):
    _name = 'hr.decree.type'
    _inherit = ['mail.thread']
    
    name = fields.Char(u'Тушаалын төрөл')
    company_id = fields.Many2one('res.company', u'Компани')#Компани
    

class hr_decree(models.Model):
    _name = 'hr.decree'
    _inherit = ['mail.thread']
    
    to_company = fields.Many2one('res.company', string=u'Компани')
    to_department = fields.Many2one('hr.department', string=u'Хэлтэс')
    to_position = fields.Many2one('hr.job', string=u'Албан тушаал')
    to_employee = fields.Many2one('hr.employee', string=u'Ажилтан')
    last_name = fields.Char(string=u'Овог')
    employee_name = fields.Many2one('hr.employee',string=u'Нэр')
    name = fields.Char('Тушаалын нэр')#Товч утга , default=lambda self: self.env['ir.sequence'].next_by_code('hr.decree')
    #decree_number = fields.Char('Decree number')#Дугаар
    insurance_type_id = fields.Many2one('insurance.type', u'Даатгуулагчын төрөл')
    date = fields.Date(string=u'Бүртгэсэн огноо', track_visibility='onchange',required=True,)#Бүртгэсэн огноо
    start_date = fields.Date(u'Эхлэх огноо')
    end_date = fields.Date(u'Дуусах огноо')
    decree_type_sel = fields.Selection([('ajil_orlon',u'Ажил албан тушаал түр орлон/хавсрах гүйцэтгэх ГТБА/ТО'),
                                        ('ajil_hawsran_shts',u'Ажил албан тушаал түр орлон/хавсрах гүйцэтгэх ШТС'),
                                        ('ajild_avah',u'Туршилтын хугацаагаар ажилд авах тухай'),
                                        ('ajild_avah1',u'Ажилд авах/гэрээгээр/'),
                                        ('devshuulen_ajiluulah_tuhai',u'Дэвшүүлэн болон шилжүүлэн ажиллуулах тухай'),
                                        ('jinhlen_ajiluulah',u'Жинхлэн ажиллуулах тухай'),
                                        ('sahilgiin_shiitgel_sanuulah',u'Хаалттай сануулах'),
                                        ('sahilgiin_shiitgel_tolbor_noogduulah',u'Цалингиас хувьдах'),
                                        ('tetgemj',u'Тэтгэмж олгох тухай'),
                                        ('hod_geree_duusah',u'Хөдөлмөрийн сахилгын шийтгэл ногдуулах тухай-цуцлах'),
                                        ('hod_geree_tsutslah_ajil_olgogch',u'Ажил олгогчийн санаачилгаар хөдөлмөр эрхлэлтийн харилцааг цуцлах тухай '),
                                        ('hod_geree_ajiltan',u'Ажилтны санаачилгаар хөдөлмөр эрхлэлтийн харилцааг цуцлах тухай'),
                                        ('tsalintai_choloo',u'Цалинтай чөлөө олгох тухай'),
                                        ('jiremsenii_bolon',u'Жирэмсний болон амаржсаны амралт олгох тухай'),
                                        ('hodolmoriin_hariltsaa_duusgawar_bolgoh',u'Хөдөлмөр эрхлэлтийн харилцаа  дуусгавар болох тухай'),
                                        ('choloo_olgoh',u'Цалингүй чөлөө олгох тухай'),
                                        ('shagnal',u'Шагнан урамшуулах тухай'),
                                        ('shiljuulen_ajiluulah',u'Шилжүүлэн ажиллуулах тухай'),
                                        ('eeljiin_amralt_olgoh',u'Ээлжийн амралт олгох тухай'),
                                        ('alban_tushaal_buuruulah',u'Албан тушаал бууруулах'),
                                        ('sahilga_shiitgel_neelttei',u'Нээлттэй сануулах'),
                                        ('ajil_hawsran',u'Ажил албан тушаал түр хавсран гүйцэтгүүлэх тухай')],string=u'Тушаалын дотоод төрөл')
    decree_type = fields.Selection([
                            ('public', 'Public'),#Нийтэд гаргах тушаал
                            ('staff', 'Staff'),#Ажилчдад гаргах тушаал
                            ], string='Decree type',#Тушаалын төрөл
                             track_visibility='onchange')
    hr_employee_id = fields.Many2one('hr.employee', 'Employee', track_visibility='onchange')#Ажилтан
    company_id = fields.Many2one('res.company', 'Company')#Компани
    department_id = fields.Many2one('hr.department', 'Department')#Хэлтэс
    position_id = fields.Many2one('hr.job', 'Position')#Албан тушаал
    is_read_decree = fields.Boolean('Is read the Decree')#Тушаалтай танилцсан эсэх
    is_employee = fields.Boolean(string=u'Түр эзгүй ажлын байр эсэх')
    test_period = fields.Integer(string=u'Test period')
    #user_ids=fields.Many2many('res.users', 'emp_decree_user_rel', 'user_ids', 'hr_decree_id', 'Users')#Ажилчид
    employee_ids=fields.Many2many('hr.employee', 'emp_decree_hr_rel', 'employee_ids', 'decree_id', 'Employees')#Ажилчид
    state = fields.Selection([
                            ('draft', 'Draft'),#Ноорог
                            ('cancelled', 'Cancelled'),#Цуцлагдсан
                            ('approved', 'Approved'),#Батлагдсан
                            ], string='State',
                             track_visibility='onchange',  default = 'draft')
    note = fields.Text('Note')#Тайлбар
    director_id = fields.Many2one('hr.employee',string=u'Гүйцэтгэх захирал', readonly=True)
    zereg_id = fields.Many2one('hr.zereg.register',string=u'Зэрэг', readonly=True)
    dev_id = fields.Many2one('hr.dev.register',string=u'Дэв', readonly=True)
    matrits = fields.Many2one('zereg.dev.matrits',string=u'Матриц')
    add_amount = fields.Integer(string=u'Нэмэгдэлийн дүн')
    amount = fields.Integer(string=u'Бусад нэмэгдэл')
    #iin = fields.Selection([('iin', 'ийн'),('in', 'ын'),], string=u'1 ')
    
    #dt = fields.Selection([('d', 'д'),('t', 'т'),], string=u'2 ')
    
    #td = fields.Selection([('d', 'д'),('t', 'т'),], string=u'13')
    
    #heltes = fields.Selection([('iin', 'ийн'),('in', 'ын'),], string=u' 3')
    
    #aar = fields.Selection([('aar', 'аар'),('eer', 'ээр'),('oor', 'оор'),('uur', 'өөр'),], string=u'14')
    
    #job = fields.Selection([('aar', 'аар'),('eer', 'ээр'),('oor', 'оор'),('uur', 'өөр'),], string=u' 4')
    
    #names = fields.Selection([('iig', 'ийг'),('ig', 'ыг'),('g', 'г'),], string=u'5 ')
    
    
    #haanegj = fields.Selection([('iin', 'ийн'),('in', 'ын'),], string=u'6 ')
    
    #haajob = fields.Selection([('iin', 'ийн'),('in', 'ын'),], string=u' 7')
    
    text1 = fields.Text('Шагналын нэр')
    
    text2 = fields.Text('Шалтгаан')
    
    #turshilt = fields.Selection([('neg', 'Нэг'),('hoyr', 'Хоёр'),('guraw', 'Гурван'),], string=u'8 ')
    
    #days = fields.Selection([('nii', 'ний'),('ni', 'ны'),], string=u'9 ')
    
    #shiljsenheltes = fields.Selection([('iin', 'ийн'),('in', 'ын'),], string=u'10 ')
    
    #shiljsenjob = fields.Selection([('aar', 'аар'),('eer', 'ээр'),('oor', 'оор'),('uur', 'өөр'),], string=u'11 ')
    
    #shagnal = fields.Text('Шагналын нэр')#Тайлбар
    
    huwi = fields.Selection([
                            ('20', '20'),
                            ('25', '25'),
                            ('30', '30'),
                            ('40', '40'),
                            ('80', '80'),
                            ('100', '100'),
                            ], string=u'Хувь')
    
    #shagnaldun = fields.Text('Дүн')#Тайлбар
    
    usgeer = fields.Text('Мөнгөн дүн')
    
    #sar = fields.Text('Үргэлжлэх сар')#Тайлбар
    
    #odor = fields.Text('Үргэлжлэх өдөр')#Тайлбар
    
    #usgeer1 = fields.Text('Ээлжийн амралт үсгээр')#Тайлбар
    
    #towchlol = fields.Selection([('iin', 'ийн'),('in', 'ын'),], string=u'12 ')
    
  
    def action_print(self):
        if self.decree_type_sel == 'ajil_orlon':
            return self.env.ref('l10n_mn_hr.report_hr_orlon').report_action(self)
        
        if self.decree_type_sel == 'ajil_hawsran':
            return self.env.ref('l10n_mn_hr.report_hr_hawsran_datas').report_action(self)

        if self.decree_type_sel == 'ajild_avah':
            return self.env.ref('l10n_mn_hr.report_hr_decree').report_action(self)

        if self.decree_type_sel == 'sahilga_shiitgel_neelttei':
            return self.env.ref('l10n_mn_hr.report_hr_sahilga_shiitgel_neelttei').report_action(self)

        if self.decree_type_sel == 'alban_tushaal_buuruulah':
            return self.env.ref('l10n_mn_hr.report_hr_alban_tushaal_buuruulah').report_action(self)

        if self.decree_type_sel == 'hodolmoriin_hariltsaa_duusgawar_bolgoh':
            return self.env.ref('l10n_mn_hr.report_hr_hodolmoriin_hariltsaa_duusgawar_bolgoh').report_action(self)

        if self.decree_type_sel == 'devshuulen_ajiluulah_tuhai':
            return self.env.ref('l10n_mn_hr.report_hr_dewshuuleh').report_action(self)        
        
        if self.decree_type_sel == 'jinhlen_ajiluulah':
            return self.env.ref('l10n_mn_hr.report_hr_jinhleh').report_action(self)

        if self.decree_type_sel == 'jiremsenii_bolon':
            return self.env.ref('l10n_mn_hr.report_hr_jiremsnii_choloo').report_action(self)

        if self.decree_type_sel == 'sahilgiin_shiitgel_sanuulah':
            return self.env.ref('l10n_mn_hr.report_hr_sahilga_noogduulah').report_action(self)

        if self.decree_type_sel == 'sahilgiin_shiitgel_tolbor_noogduulah':
            return self.env.ref('l10n_mn_hr.report_hr_sahilga_noogduulah_tolbor').report_action(self)

        if self.decree_type_sel == 'tetgemj':
            return self.env.ref('l10n_mn_hr.report_hr_tetgemj').report_action(self)

        if self.decree_type_sel == 'hod_geree_duusah':
            return self.env.ref('l10n_mn_hr.report_hr_hodolmoriin_duusgawar_bolgoh').report_action(self)

        if self.decree_type_sel == 'hod_geree_tsutslah_ajil_olgogch':
            return self.env.ref('l10n_mn_hr.report_hr_ajil_olgogch_sanaachlasan').report_action(self)

        if self.decree_type_sel == 'hod_geree_ajiltan':
            return self.env.ref('l10n_mn_hr.report_hr_ajiltan_sanaachlasan').report_action(self)
        
        if self.decree_type_sel == 'tsalintai_choloo':
            return self.env.ref('l10n_mn_hr.report_hr_tsalintai_choloo').report_action(self)

        if self.decree_type_sel == 'choloo_olgoh':
            return self.env.ref('l10n_mn_hr.report_hr_choloo_olgoh').report_action(self)

        if self.decree_type_sel == 'shagnal':
            return self.env.ref('l10n_mn_hr.report_hr_shagnal').report_action(self)

        if self.decree_type_sel == 'shiljuulen_ajiluulah':
            return self.env.ref('l10n_mn_hr.report_hr_shiljuuleh').report_action(self)

        if self.decree_type_sel == 'eeljiin_amralt_olgoh':
            return self.env.ref('l10n_mn_hr.report_hr_eeljiin_amralt').report_action(self)   
    
    
    def action_to_draft(self):
        self.write({'state': 'draft'})
        return True

    def action_to_cancel(self):
        self.write({'state': 'cancelled'})
        return True
    
    def action_to_approve(self):
        self.write({'state': 'approved'})
        if self.decree_type_sel == 'shiljuulen_ajiluulah':
            emp_id = self.env['hr.employee'].search([('id','=',self.hr_employee_id.id)])
               
            emp_id.write({'company_id':self.to_company.id,
                                                            'department_id':self.to_department.id,
                                                            'job_id':self.to_position.id,
                                                            })
        return True
    
    @api.onchange('decree_type')
    def onchange_decree_type(self):
        res = {}
        if self.decree_type == 'public':
            res['hr_employee_id'] = False
            res['company_id'] = False
            res['department_id'] = False
            res['position_id'] = False
        elif self.decree_type == 'staff':
            #res['employee_ids'] = False
            res['employee_ids'] = False
        return {'value':res}
    
    @api.onchange('hr_employee_id')
    def onchange_employee_id(self):
        res = {}
        list = []
        if self.hr_employee_id:
            employee_obj = self.env['hr.employee']
            emp_id = employee_obj.search([('id', '=', self.hr_employee_id.id)])
            if emp_id:
                res['last_name'] = emp_id.last_name
                res['company_id'] = emp_id.company_id.id
                res['department_id'] = emp_id.department_id.id
                res['position_id'] = emp_id.job_id.id
        else:
            res['last_name'] = False
            res['company_id'] = False
            res['department_id'] = False
            res['position_id'] = False
            
        return {'value':res}
    
    def write(self, vals):
        
        if self.decree_type == 'staff':
            if 'is_read_decree' in vals:
                print ('DDDDDDDDD', vals['is_read_decree'], self.hr_employee_id.user_id.id, self.env.user.id)
                if self.hr_employee_id.user_id.id != self.env.user.id:
                    raise ValidationError(_("Тухайн ажилтан зөвхөн өөрөө тушаалтай танилцсанаа тэмдэглэнэ !"))
                
        ifuser_hrmanager = self.env.user.has_group('hr.group_hr_manager')
        if ifuser_hrmanager == False:
            if 'name' in vals:
                raise ValidationError(_("Зөвхөн хүний нөөцийн менежер эрхтэй хүн засах эрхтэй ! Та зөвхөн өөрийнхөө << Тушаалтай танилцсан эсэх >> - ийг л засч болно. Та хэрэгсэхгй дарж байгаад Засах дарж ороод << Тушаалтай танилцсан эсэх >> талбарыг засна уу !!!"))
            
            #if 'decree_number' in vals:
                #raise ValidationError(_("Зөвхөн хүний нөөцийн менежер эрхтэй хүн засах эрхтэй ! Та зөвхөн өөрийнхөө << Тушаалтай танилцсан эсэх >> - ийг л засч болно. Та хэрэгсэхгй дарж байгаад Засах дарж ороод << Тушаалтай танилцсан эсэх >> талбарыг засна уу !!!"))
            
            if 'decree_type' in vals:
                raise ValidationError(_("Зөвхөн хүний нөөцийн менежер эрхтэй хүн засах эрхтэй ! Та зөвхөн өөрийнхөө << Тушаалтай танилцсан эсэх >> - ийг л засч болно. Та хэрэгсэхгй дарж байгаад Засах дарж ороод << Тушаалтай танилцсан эсэх >> талбарыг засна уу !!!"))
            
            if 'date' in vals:
                raise ValidationError(_("Зөвхөн хүний нөөцийн менежер эрхтэй хүн засах эрхтэй ! Та зөвхөн өөрийнхөө << Тушаалтай танилцсан эсэх >> - ийг л засч болно. Та хэрэгсэхгй дарж байгаад Засах дарж ороод << Тушаалтай танилцсан эсэх >> талбарыг засна уу !!!"))
            
            if 'user_ids' in vals:
                raise ValidationError(_("Зөвхөн хүний нөөцийн менежер эрхтэй хүн засах эрхтэй ! Та зөвхөн өөрийнхөө << Тушаалтай танилцсан эсэх >> - ийг л засч болно. Та хэрэгсэхгй дарж байгаад Засах дарж ороод << Тушаалтай танилцсан эсэх >> талбарыг засна уу !!!"))
            
            if 'note' in vals:
                raise ValidationError(_("Зөвхөн хүний нөөцийн менежер эрхтэй хүн засах эрхтэй ! Та зөвхөн өөрийнхөө << Тушаалтай танилцсан эсэх >> - ийг л засч болно. Та хэрэгсэхгй дарж байгаад Засах дарж ороод << Тушаалтай танилцсан эсэх >> талбарыг засна уу !!!"))
            
            if 'hr_employee_id' in vals:
                raise ValidationError(_("Зөвхөн хүний нөөцийн менежер эрхтэй хүн засах эрхтэй ! Та зөвхөн өөрийнхөө << Тушаалтай танилцсан эсэх >> - ийг л засч болно. Та хэрэгсэхгй дарж байгаад Засах дарж ороод << Тушаалтай танилцсан эсэх >> талбарыг засна уу !!!"))
            
            if 'company_id' in vals:
                raise ValidationError(_("Зөвхөн хүний нөөцийн менежер эрхтэй хүн засах эрхтэй ! Та зөвхөн өөрийнхөө << Тушаалтай танилцсан эсэх >> - ийг л засч болно. Та хэрэгсэхгй дарж байгаад Засах дарж ороод << Тушаалтай танилцсан эсэх >> талбарыг засна уу !!!"))
            
            if 'department_id' in vals:
                raise ValidationError(_("Зөвхөн хүний нөөцийн менежер эрхтэй хүн засах эрхтэй ! Та зөвхөн өөрийнхөө << Тушаалтай танилцсан эсэх >> - ийг л засч болно. Та хэрэгсэхгй дарж байгаад Засах дарж ороод << Тушаалтай танилцсан эсэх >> талбарыг засна уу !!!"))
            
            if 'position_id' in vals:
                raise ValidationError(_("Зөвхөн хүний нөөцийн менежер эрхтэй хүн засах эрхтэй ! Та зөвхөн өөрийнхөө << Тушаалтай танилцсан эсэх >> - ийг л засч болно. Та хэрэгсэхгй дарж байгаад Засах дарж ороод << Тушаалтай танилцсан эсэх >> талбарыг засна уу !!!"))
            
            if 'state' in vals:
                raise ValidationError(_("Зөвхөн хүний нөөцийн менежер эрхтэй хүн засах эрхтэй ! Та зөвхөн өөрийнхөө << Тушаалтай танилцсан эсэх >> - ийг л засч болно. Та хэрэгсэхгй дарж байгаад Засах дарж ороод << Тушаалтай танилцсан эсэх >> талбарыг засна уу !!!"))
            
            
        res = super(hr_decree, self).write(vals)
        return res
    
    
    
    