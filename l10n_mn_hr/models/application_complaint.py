# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
from odoo.modules.module import get_module_resource
from reportlab.graphics.shapes import skewY

from datetime import date, datetime, time
from datetime import timedelta
from dateutil import relativedelta
from docutils.languages import da

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

_logger = logging.getLogger(__name__)


        
class application_complaint(models.Model):
    _name = 'application.complaint'
    _inherit = ['mail.thread']
    _description = 'Application complaint'  # Өргөдөл гомдол
    
    #Eronhii angilal
    complaint_category = fields.Selection([
        ('internal_staff', 'Internal staff'),#Дотоод ажилчдийн өргөдөл
        ('customers', 'Customers'),#Үйлчлүүлэгчдийн өргөдөл
        ], 'Complaint category', track_visibility='onchange', default='internal_staff')#Ангилал
    #Dotood ajiltnii orgodol bol
    hr_employee_id = fields.Many2one('hr.employee', 'First name', track_visibility='onchange')#Ажилтан
    name = fields.Char('Last name')#Овог
    company_name = fields.Many2one('res.company', 'Company')#Харъяалагдах компани
    department_name = fields.Many2one('hr.department', 'Department')#Алба, хэлтэс
    position_name = fields.Many2one('hr.job', 'Position')#Албан тушаал
    #Gadnii hvnii orgodol bol
    partner_type = fields.Selection([
        ('person', 'Person'),#Хувь хүн
        ('company', 'Company'),#Компани
        ], 'Partner type', track_visibility='onchange')#Харилцагчийн төрөл
    partner_company = fields.Many2one('res.partner', 'Partner company', track_visibility='onchange')#Өргөдөл гаргагч байгууллага
    partner_id = fields.Many2one('res.partner', 'Partner', track_visibility='onchange')#Өргөдөл гаргагч этгээдийн нэр
    partner_lastname = fields.Char('Partner lastname', track_visibility='onchange')#Өргөдөл гаргагч этгээдийн овог
    partner_register = fields.Char('Partner register', track_visibility='onchange')#Өргөдөл гаргагч этгээдийн регистрийн дугаар
    accepted_employee_id = fields.Many2one('hr.employee', 'Accepted employee', track_visibility='onchange')#Хүлээн авсан ажилтан
    #Hyanaltiin heseg
    manager_id = fields.Many2one('hr.employee', 'Manager', track_visibility='onchange')#Шууд удирдлага
    manager_decision = fields.Text('Manager decision', track_visibility='onchange')#Шууд удирдлагын цохолт
    director_id = fields.Many2one('hr.employee', 'Director', track_visibility='onchange')#Дээд удирдлага
    director_decision = fields.Text('Director decision', track_visibility='onchange')#Дээд удирдлагын цохолт
    
    #Niitleg zvils
    comp_date = fields.Date('Complaint date', track_visibility='onchange')#Өргөдөл гаргасан огноо
    deliver_date = fields.Date('Deliver date', track_visibility='onchange')#Танилцуулсан огноо
    to_invoice = fields.Boolean('To invoice', track_visibility='onchange')#Нэхэмжлэл илгээх
    decision = fields.Text('Decision', track_visibility='onchange')#Шийдвэрлэсэн байдал
    approved_date = fields.Date('Approved date', track_visibility='onchange')#Шийдвэрлэсэн огноо
    complaint_comment = fields.Text('Complaint comment', track_visibility='onchange')#Өргөдлийн утга
    complaint_type = fields.Many2one('complaint.type', 'Complaint type', track_visibility='onchange')#Өргөдлийн төрөл
    received_source = fields.Many2one('complaint.source', 'Received source', track_visibility='onchange')#Өргөдлийг хүлээн авсан хэлбэр
   # 'mill_amount':fields.float('Mill amount'),
    #supply_amount = fields.Float('Supply amount', track_visibility='onchange')#Хөнгөлөлт эдэлж мөнгө авах бол бөглөнө үү
    state = fields.Selection([
        ('created_comp', 'Created complaint'),#Ноорог
        ('delivered_comp', 'Delivered complaint'),#Хүргэгдсэн
        ('cancelled_comp', 'Cancelled complaint'),#Цуцлагдсан
        ('approved_comp', 'Approved complaint'),#Батлагдсан
        ], 'State', track_visibility='onchange', default='created_comp')#Төлөв

    
    def action_delivery(self):
        deliver_date = datetime.today()
        self.write({'deliver_date': deliver_date,
                    'state': 'delivered_comp'
                    })
        return True

    def action_draft(self):
        self.write({'state': 'created_comp'})
        return True
        
    def action_cancel(self):
        if not self.decision:
            raise ValidationError(_('You need to fill the field called the Decision ! First click Edit button !'))
        self.write({'state': 'cancelled_comp'})
        return True
    
    def action_approve(self):
        #supply_amount = self.supply_amount
        supply_draft = self.env['supply.bonus']
        if not self.manager_decision:
            raise ValidationError(_('You need to fill the field called <<Manager decision>> ! First click Edit button !'))
        if not self.director_decision:
            raise ValidationError(_('You need to fill the field called <<Director decision>> ! First click Edit button !'))
        if not self.decision:
            raise ValidationError(_('You need to fill the field called <<Decision>> ! First click Edit button !'))
        
        if_director = self.env['res.users'].has_group('l10n_mn_hr.group_position_director')
        if_HRmanager = self.env['res.users'].has_group('hr.group_hr_manager')
        
        if not if_HRmanager:
            if if_director:
                if self.director_id.user_id != self.env.user:
                    raise ValidationError(_('''Энэхүү өргөдөлд сонгогдсон Дээд удирдлага нь та биш тул батлах боломжгүй. Дээд удирдлага -талбарт сонгогдсон байгаа Удирдлага нэвтэрч батална. Эсвэл дээд удирдлагын зөвшөөрлөөр Хүний нөөцийн менежер батална !'''))
            else:
                raise ValidationError(_('''Энэхүү өргөдлийг батлахад таны эрх хүрэхгүй байна. Дээд удирдлага батална !'''))
        
        approved_date = datetime.today()
        self.write({'approved_date': approved_date,
                    'state': 'approved_comp'})
        return True
    
    @api.onchange('hr_employee_id')
    def onchange_application(self):
        res = {}
        if self.hr_employee_id:
            res['name'] = self.hr_employee_id.last_name
            res['company_name'] = self.hr_employee_id.company_id.id
            res['department_name'] = self.hr_employee_id.department_id.id
            res['position_name'] = self.hr_employee_id.job_id.id
            return {'value':res}
        #=======================================================================
        # sch_supply_draft = supply_draft.create({
        #                     'hr_employee_id': self.hr_employee_id.id,
        #                     'name': self.name,
        #                     'company_name': self.company_name.id,
        #                     'department_name': self.department_name.id,
        #                     'position_name': self.position_name.id,
        #                     'amount': self.supply_amount,
        #                     'giving_date': self.date,
        #                     'supply_invoice':self.partner_id.id,
        #                     'description':self.complaint_type.name,
        #                     # 'journal_id': len(journal_ids) and journal_ids[0] or False,
        #                      })
        #=======================================================================
    
class complaint_type(models.Model):
    _name = 'complaint.type'
    _description = 'Complaint type' # Өргөдлийн төрөл
    
    name = fields.Char('Complaint type')#Өргөдлийн төрөл
    hr_employee_id = fields.Many2one('hr.employee', 'First name')#Ажилтан
    
class complaint_source(models.Model):
    _name = 'complaint.source'
    _description = 'Complaint source' # Өргөдлийн хүлээн авсан хэлбэр
    
    name = fields.Char('Complaint type')#Өргөдлийн хүлээн авсан хэлбэр


        
