# -*- coding: utf-8 -*-
##############################################################################
#
#    Cubic ERP, Enterprise and Government Management Software
#    Copyright (C) 2017 Cubic ERP S.A.C. (<http://cubicerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Mongolian HR',
    'version': '1.0',
    'category': 'hr',
    'sequence': 35,
    'author': 'Tengersoft Developer Team',
    'license': 'AGPL-3',
    'summary': 'Human resource additional development',
    'description': """Mongolian hr additional module""",
    'website': 'https://www.tengersoft.co',
    'depends': [
                 'base',
                 'hr',
                 'hr_payroll_community',
                 'hr_attendance',
                 'hr_expense',
                 'hr_holidays',
                 'hr_contract',
                 'hr_recruitment',
                 'l10n_mn_payroll',
                 'account'
                 #'hr_recruitment',
                ],
    'data': [
            'views/report_template.xml',
            'views/mn_hr_inherit_view.xml',
            'views/internal_training.xml',
            'views/hr_penalty.xml',
            'views/application_complaint.xml',
            'views/award_list.xml',
            'views/out_work.xml',
            'views/supply_bonus.xml',
            'views/routing_slip_view.xml', 
            'views/hr_holiday_days.xml',
            'views/labour_movement.xml', 
            'views/training_order.xml',
            'views/other_contract.xml',
            'views/medical_examination_view.xml',
            'views/hr_decree.xml',
            'views/common_procedure.xml',
            'wizard/training_order_integrate_view.xml',
            'report/internal_training_report.xml',
            'report/routing_slip_report.xml',
            'report/hr_decree_view.xml',
            'report/hr_ajil_olgogch_sanaachilsan.xml',
            'report/hr_ajiltan_sanaachlasan.xml',
            'report/hr_ajlaas_tutgelzuuleh.xml',
            'report/hr_choloo_olgoh.xml',
            'report/hr_dewshuuleh.xml',
            'report/hr_eeljiin_amralt_batlah.xml',
            'report/hr_eeljiin_amralt.xml',
            'report/hr_hawsran_office.xml',
            'report/hr_hawsran_SHTS.xml',
            'report/hr_hodolmoriin_duusgawar_bolgoh.xml',
            'report/hr_jinhleh.xml',
            'report/hr_jiremsenii_choloo.xml',
            'report/hr_orlon.xml',
            'report/hr_hawsran_SHTS.xml',
            'report/hr_sahilga_noogduulah_tolbor.xml',
            'report/hr_sahilga_noogduulah.xml',
            'report/hr_sahilga_shiitgel_neelttei.xml',
            'report/hr_alban_tushaal_buuruulah.xml',
            'report/hr_shagnal.xml',
            'report/hr_shiljuuleh.xml',
            'report/hr_tetgemj.xml',
            'report/hr_tsalind_oorchlolt_ oruulah.xml',
            'report/hr_tsalintai_choloo.xml',
            'report/hr_hodolmoriin_hariltsaa_duusgawar_bolgoh.xml',
            'report/geree_nuuts_hadgalah.xml',
            'report/geree_togtwortoi_ajillah.xml',
            'report/geree_NHO.xml',
            'report/geree_erhlegch.xml',
            'report/geree_office_gtba.xml',
            'report/geree_SHTS.xml',
            'report/geree_ed_hurungu.xml',
            'report/protokol.xml',
            'views/report_view.xml',
            'wizard/medic_exam_parts.xml',
            'wizard/hr_departure_wizard_views.xml',
            'views/hr_job_category_view.xml',
            'views/hr_job_inherit_view.xml',
            'views/inshurance_type_view.xml',
            'views/hr_decree_type_view.xml',
            'views/emp_type_view.xml',
            'views/res_country_state_view.xml',
            'views/khoroo_view.xml',
            'views/hr_protokol.xml',
            'views/sum_duureg_view.xml',
            'views/hr_sequence.xml',
            'views/celebration_day_view.xml',
            'views/employee_work_time_change_register_view.xml',
            'views/record_employee_adaptive_functions_view.xml',
            'views/record_employee_register_view.xml',
            'views/job_task_view.xml',
            #'views/eeljiin_huwaari.xml',
            #'views/hr_eeljiin_amralt.xml',
            'views/menu_view.xml',
            'views/cron.xml',
            'security/security.xml',
            'security/ir.model.access.csv',
            
    ],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
