from odoo import models, fields, api, _
from odoo.tools.float_utils import float_round


class HrDepartureWizard(models.TransientModel):
    _inherit = 'hr.departure.wizard'

    departure_reason = fields.Selection([
        ('fired', u'Байгууллагын санаачлагаар'),
        ('resigned', u'Өөрийн хүсэлтээр'),
        ('retired', u'Гэрээний хэрэгжилтийг дүгнэсэн протокол/байгууллагын санаачилгаар'),
        ('retired1', u'Гэрээний хэрэгжилтийг дүгнэсэн протокол/ажилтны санаачилгаар')
    ], string="Departure Reason", default="fired")
    job_end_date = fields.Date(string=u'Ажлаас гарсан огноо')
    is_black_list = fields.Boolean(string=u'Хар жагсаалт орсон эсэх')

    def action_register_departure(self):
        employee = self.employee_id
        employee.departure_reason = self.departure_reason
        employee.departure_description = self.departure_description
        employee.job_end_date = self.job_end_date
        employee.is_black_list = self.is_black_list