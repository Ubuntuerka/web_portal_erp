from odoo import models, fields, api, _
from odoo.tools.float_utils import float_round


class MedicExamParts(models.TransientModel):
    _name = 'medic.exam.parts'
    _description = 'Medic Exam Parts'
    
    description = fields.Text('Description')
    employee_ids = fields.Many2many(
        'hr.employee', 'mep_rel',
        'mep_id', 'mep_emp_id',
        string='Employees')
    examination_date = fields.Date('Examination date')
    end_date = fields.Date('End date')
    medical_examination_id = fields.Many2many(
        'medical.examination.type', 'me_med_rel',
        'me_med_id', 'me_cat_id',
        string='Tags')
            
    def create_registrations(self):
        exam_obj = self.env['medical.examination']
        for line in self.employee_ids:
            created_exam = exam_obj.create({
                                        'description':self.description,
                                        'hr_employee_id':line.id,
                                        
                                        'name':line.last_name,
                                        'identification_id':line.identification_id,
                                        'age':line.employee_age,
                                        'gender':line.gender,
                                        'company_name':line.company_id.id,
                                        'department_name':line.department_id.id,
                                        'position_name':line.job_id.id,
                                        
                                        'examination_date':self.examination_date,
                                        'end_date':self.end_date,
                                        'medical_examination_id':self.medical_examination_id
                        })
            