from odoo import models, fields, api, _
from odoo.tools.float_utils import float_round


class TrainingOrderIntegrate(models.TransientModel):
    _name = 'training.order.integrate'
    _description = 'Training order integrate'
    
    name = fields.Char('Training subject', required=True, track_visibility='onchange')#Сургалтын сэдэв
    inst_date = fields.Date('Start date', track_visibility='onchange')#Эхлэх огноо
    end_date = fields.Date('End date', track_visibility='onchange')#Дуусах огноо
    inst_budget = fields.Float('Training budget', track_visibility='onchange')#Анхны төсөв
            
    def integrate_order(self):
        context = dict(self._context or {})
        order_obj = self.env['training.order']
        tr_obj = self.env['internal.training']
        active_ids = context.get('active_ids', []) or []
        if active_ids:
            training_type = False
            emp_ids = []
            employee_line_ids = []
            for line in order_obj.browse(active_ids):
                training_type = line.training_type.id
                if line.hr_employee_id.id not in emp_ids and line.state == 'draft':
                    employee_line_ids.append(line.hr_employee_id.id)
                    emp_ids.append(line.hr_employee_id.id)
                
            created_tr = tr_obj.create({
                                        'name':self.name,
                                        'inst_date':self.inst_date,
                                        'end_date':self.end_date,
                                        'inst_budget':self.inst_budget,
                                        'type':self.inst_budget,
                                        
                                        'type':'staff_order',
                                        'training_type':training_type,
                                        'employee_line_ids':[[6,0,employee_line_ids]]
                        })
            if created_tr:
                for line2 in order_obj.browse(active_ids):
                    line2.write({'plan_id':created_tr.id,
                                 'state':'integrated'})
                
                
                
            