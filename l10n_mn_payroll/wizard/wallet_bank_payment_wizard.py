# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class WalletBankPaymentWizard(models.TransientModel):
    _inherit = 'abstract.report.excel'
    _name = 'wallet.bank.payment.wizard'

    company_id = fields.Many2one('res.company', string='Конпани', required=True,
                                 default=lambda self: self.env['res.company']._company_default_get('wallet.bank.payment.wizard'))
    bank_id = fields.Many2one('res.bank', string=u'Банк', required=True)
    start_date = fields.Date(string='Эхлэх огноо', required=True)
    end_date = fields.Date(string='Дуусах огноо', required=True)

    def get_export_data(self):
        company = self.env['res.company'].browse(self.company_id.id)
        context = self._context
        ezxf = xlwt.easyxf
        styledict = self.get_easyxf_styles()
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('АЖИЛТНУУДЫН ЖАГСААЛТ')
        header = ezxf('font: bold on; align: wrap off, vert centre, horiz centre;font: height 220')
        header1 = ezxf('font: bold off; align: wrap on, vert centre, horiz left;font: height 200')
        print_dateL = ezxf('font: bold off; align: wrap off, vert centre, horiz left;font: height 200')
        print_dateR = ezxf('font: bold off; align: wrap off, vert centre, horiz right;font: height 200')
        footer1 =ezxf('font:bold off;align:wrap off,vert centre,horiz left;font: height 180')
        footer2 =ezxf('font:bold off;align:wrap off,vert centre,horiz center;font: height 180')
        footer3 =ezxf('font:bold off;align:wrap off,vert centre,horiz right;font: height 180')
        italic =ezxf('font:italic on, underline off, bold off;align:wrap off,vert centre,horiz right;font: height 180')
        underline =ezxf('font:italic off, underline on, bold off;align:wrap off,vert centre,horiz right;font: height 180')
        line = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz left;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        line_data = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz right;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        line_center = ezxf(
            'font: bold on; align: wrap on, vert centre, horiz center;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        nonline = ezxf('font: bold off; align: wrap off, vert centre, horiz left;font: height 200;')
        data_center = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        sheet.write(2, 2, u'%s-НЫ ДАНСААР ЦАЛИН ОЛГОХ АЖИЛТНУУДЫН ЖАГСААЛТ ' % (self.bank_id.name),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(4, 0, u'Байгууллагын нэр: %s' % (company.name),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(5, 0, u'Тайлан эхлэх огноо: %s' % (self.start_date),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(5, 5, u'Тайлан дуусах огноо: %s' % (self.end_date),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(6, 0, u'Цалингийн түр дансны дугаар: .................',
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        rowx = 8

        col= 3
        num = 1
        sheet.write(rowx, 0, u'Д/Д', styledict['heading_xf'])
        sheet.write(rowx, 1, u'Хэлтэс', styledict['heading_xf'])
        sheet.write(rowx, 2, u'Ажилтны код', styledict['heading_xf'])
        sheet.write(rowx, 3, u'Овог нэр', styledict['heading_xf'])
        sheet.write(rowx, 4, u'Ажилтны нэр', styledict['heading_xf'])
        sheet.write(rowx, 5, u'Дансны дугаар', styledict['heading_xf'])
        sheet.write(rowx, 6, u'Урьдчилгаа цалин', styledict['heading_xf'])

        self.env.cr.execute("""select d.name as department_id, e.employee_code, e.last_name, e.name, e.bank_number, h.prepayment_payroll, p.create_user
                            from hr_payroll_prepayment_line as h
                            left join hr_payroll_prepayment as p on h.prepayment_id = p.id 
                            left join hr_employee as e on e.id = h.name 
                            left join hr_department as d on d.id = e.department_id
                            where p.state= 'confirm' and e.company_id='%d' and e.bank_id= '%d' and p.start_date = '%s' and p.end_date = '%s'
                            """%(self.company_id.id, self.bank_id.id, self.start_date, self.end_date))
        wallet_bank = self._cr.dictfetchall()


        rowx += 1
        if wallet_bank:
            total_payroll = 0
            for number, rb in enumerate(wallet_bank, start=1):
                crate_user_id = self.env['hr.employee'].search([('user_id', '=', rb['create_user'])], limit=1)
                number = str(number)
                total_payroll += rb.get('prepayment_payroll', 0)
                sheet.write(rowx, 0, number, data_center)
                sheet.write(rowx, 1, '%s' % rb['department_id'], data_center)
                sheet.write(rowx, 2, '%s' % rb['employee_code'], data_center)
                sheet.write(rowx, 3, '%s' % rb['last_name'], data_center)
                sheet.write(rowx, 4, '%s' % rb['name'], data_center)
                sheet.write(rowx, 5, '%s' % rb['bank_number'], data_center)
                sheet.write(rowx, 6, '%s' % rb['prepayment_payroll'], data_center)
                rowx += 1
            sheet.write_merge(rowx, rowx, 6, 6, '%s' % total_payroll, data_center)
            rowx += 1
            footer_text = "Гүйцэтгэх Захирал"
            footer_text1 = "                        "
            footer_text2 = "Ж.Мөнхнаран"
            footer_text3 = "Санхүү Хариуцсан Захирал"
            footer_text5 = "С.Болортуяа"
            footer_text6 ="(гарын үсэг)"
            footer_text7 ="Жагсаалт хүргүүлсэн"
            first_letter_last_name = crate_user_id.last_name[0] if crate_user_id.last_name else ''
            footer_text_user = f"{first_letter_last_name}. {crate_user_id.name}" if crate_user_id.name else ''
            sheet.write(rowx, 0, footer_text7, footer1)
            rowx += 1
            sheet.write(rowx,4, footer_text, footer3)
            sheet.write(rowx,5, footer_text1, underline)
            sheet.write(rowx,6, footer_text2, underline)
            rowx += 1
            sheet.write(rowx, 5, footer_text6, italic)
            rowx += 1
            sheet.write(rowx, 4, footer_text3, footer3)
            sheet.write(rowx, 5, footer_text1, underline)
            sheet.write(rowx, 6, footer_text5, underline)
            rowx += 1
            sheet.write(rowx, 5, footer_text6, italic)
            rowx += 1
            sheet.write(rowx, 4, crate_user_id.job_id.name, footer3)
            sheet.write(rowx, 5, footer_text1, underline)
            sheet.write(rowx, 6, footer_text_user, underline)
            rowx += 1
            sheet.write(rowx, 5, footer_text6, italic)

        inch = 1000
        sheet.col(0).width = 2 * inch
        sheet.col(1).width = 6 * inch
        sheet.col(2).width = 4 * inch
        sheet.col(3).width = 4 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 3 * inch
        sheet.row(2).height = 400

        return {'data': book, 'directory_name': u'Ажилтны цагийн бүртгэлийн тайлан',
                'attache_name': u'Ажилтны цагийн бүртгэлийн тайлан'}
