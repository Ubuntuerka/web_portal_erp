# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class SocialSecurityReportWizard(models.TransientModel):
    _inherit = 'abstract.report.excel'
    _name = 'social.security.report.wizard'

    company_id = fields.Many2one('res.company', string='Компани', required=True,
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'social.security.report.wizard'))
    department_id = fields.Many2one('hr.department', string='Хэлтэс')
    date = fields.Date(string='Огноо')
    start_date = fields.Date(string='Эхлэх огноо', required=True)
    end_date = fields.Date(string='Дуусах огноо', required=True)

    def get_export_data(self):
        company = self.env['res.company'].browse(self.company_id.id)
        context = self._context
        ezxf = xlwt.easyxf
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('Нийгмийн даатгал тайлан')
        header = ezxf('font: name Times New Roman, bold on, height 180; align: wrap on, vert centre, horiz center; '
                      'borders: top thin, left thin, bottom thin, right thin, top_colour gray50,'
                      'bottom_colour gray50,left_colour gray50,right_colour gray50; pattern: pattern solid, fore_color pale_blue')
        footer1 = ezxf('font: name Times New Roman, bold on, height 200')
        footer3 = ezxf('font: name Times New Roman, bold off;align:wrap off,vert centre,horiz right;font: height 200')
        italic = ezxf(
            'font: name Times New Roman, italic on, underline off, bold off;align:wrap off,vert centre,horiz right;font: height 160')
        underline = ezxf(
            'font: name Times New Roman, italic off, underline on, bold off;align:wrap off,vert centre,horiz right;font: height 200')
        data_center_left = ezxf(
            'font: name Times New Roman, bold off; align: wrap on, vert centre, horiz left; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        data_center = ezxf(
            'font: name Times New Roman, bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        format_with_separator = ezxf(
            'font: name Times New Roman, bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00')
        sheet.write_merge(2, 2, 0, 20, u'Нийгмийн даатгалын тайлан',
                          ezxf(
                              'font: name Times New Roman, bold on, height 200; align: wrap on, vert centre, horiz center'))
        sheet.write(4, 0, u'Байгууллагын нэр: %s' % (company.name),
                    ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))
        sheet.write(6, 0, u'Хэлтэс: %s' % (self.department_id.name),
                    ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))
        sheet.write(5, 0, u'Огноо: %s' % (self.start_date),
                    ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))
        sheet.write(5, 6, u'Огноо: %s' % (self.end_date),
                    ezxf('font: name Times New Roman, bold off, height 200; align: horiz left'))

        rowx = 6
        sheet.write_merge(7, 8, 0, 0, u'Д/Д', header)
        sheet.write_merge(7, 8, 1, 1, u'Хэлтэс', header)
        sheet.write_merge(7, 8, 2, 2, u'Ажилчдын  овог нэр', header)
        sheet.write_merge(7, 8, 3, 3, u'Хөдөлмөрийн гэрээгээр ажиллагсадын  нэрс', header)
        sheet.write_merge(7, 8, 4, 4, u'ТТДугаар', header)
        sheet.write_merge(7, 8, 5, 5, u'Регистрийн дугаар', header)
        sheet.write_merge(7, 8, 6, 6, u'Ажил мэргэжлийн код', header)
        sheet.write_merge(7, 8, 7, 7, u'Даатгуулагчийн төрөл', header)
        sheet.write_merge(7, 8, 8, 8, u'Хөдөлмөрийн хөлс', header)
        sheet.write_merge(7, 7, 9, 10, u'Тэтгэвэрийн', header)
        sheet.write_merge(8, 8, 9, 9, u'Байг.', header)
        sheet.write_merge(8, 8, 10, 10, u'Даат.', header)
        sheet.write_merge(7, 7, 11, 12, u'Тэтгэмжийн', header)
        sheet.write_merge(8, 8, 11, 11, u'Байг.', header)
        sheet.write_merge(8, 8, 12, 12, u'Даат.', header)
        sheet.write_merge(7, 7, 13, 14, u'Ажилгүйдлийн', header)
        sheet.write_merge(8, 8, 13, 13, u'Байг.', header)
        sheet.write_merge(8, 8, 14, 14, u'Даат.', header)
        sheet.write_merge(7, 7, 15, 16, u'Эрүүл мэндийн', header)
        sheet.write_merge(8, 8, 15, 15, u'Байг.', header)
        sheet.write_merge(8, 8, 16, 16, u'Даат.', header)
        sheet.write_merge(7, 8, 17, 17, u'ҮОМШӨ', header)
        sheet.write_merge(7, 7, 18, 19, u'Нийт дүн', header)
        sheet.write_merge(8, 8, 18, 18, u'Байг.', header)
        sheet.write_merge(8, 8, 19, 19, u'Даат.', header)

        hr_payslip_obj = self.env['hr.payslip']
        hr_payslip_line_obj = self.env['hr.payslip.line']



        if self.company_id and self.department_id :
            # Бүх нөхцөлүүдийг хангаж байгаа тохиолдолд
            payslips = hr_payslip_obj.search([
                ('company_id', '=', self.company_id.id),
                ('employee_id.department_id', '=', self.department_id.id),
                ('date_from', '=', self.start_date),
                ('date_to', '=', self.end_date),
                ('state', '=', 'done')
            ])
        elif self.company_id:
            # Гаргах нөхцөлүүдийг хангаж байгаа тохиолдолд
            payslips = hr_payslip_obj.search([
                ('company_id', '=', self.company_id.id),
                ('date_from', '=', self.start_date),
                ('date_to', '=', self.end_date),
                ('state', '=', 'done')
            ])
        else:
            # Тусгай нөхцөлүүдийг хангах эсвэл тодорхойлсон нөхцөлүүд байхгүй тохиолдолд
            payslips = hr_payslip_obj.search([
                ('company_id', '=', self.company_id.id),
                ('state', '=', 'done')
            ])

        employee_totals = {}
        rowx = 9
        number = 1.00

        for payslip in payslips:
            payslip_lines = payslip.line_ids.filtered(lambda line: line.code in ['T', 'AM'])
            total_rs = sum(line.amount for line in payslip_lines if line.code == 'T')
            total_am = sum(line.amount for line in payslip_lines if line.code == 'AM')
            hodolmor = total_rs - total_am

            percent = payslip.employee_id.job_id.category_id.percent

            t_ajil = 0
            t_daatgal = 0
            tet_ajil = 0
            tet_daatgal = 0
            aj_ajil = 0
            aj_daatgal = 0
            er_ajil = 0
            er_daatgal = 0
            uon = 0

            
            insurance_code = payslip.employee_id.contract_id.insurance_type_id.code
            if insurance_code == '01001':
                t_ajil = hodolmor * 0.085
                t_daatgal = hodolmor * 0.085
                tet_ajil = hodolmor * 0.01
                tet_daatgal = hodolmor * 0.008
                aj_ajil = hodolmor * 0.005
                aj_daatgal = hodolmor * 0.002
                er_ajil = hodolmor * 0.02
                er_daatgal = hodolmor * 0.02
                uon = hodolmor * float(percent) / 100
            elif insurance_code == '40001':
                t_ajil = hodolmor * 0.085
                t_daatgal = hodolmor * 0.085
                tet_ajil = hodolmor * 0
                tet_daatgal = hodolmor * 0
                aj_ajil = hodolmor * 0
                aj_daatgal = hodolmor * 0
                er_ajil = hodolmor * 0.02
                er_daatgal = hodolmor * 0.02
                uon = (hodolmor * float(percent) / 100) * 0
            elif insurance_code == '70001':
                t_ajil = hodolmor * 0.085
                t_daatgal = hodolmor * 0.085
                tet_ajil = hodolmor * 0.01
                tet_daatgal = hodolmor * 0.008
                aj_ajil = hodolmor * 0
                aj_daatgal = hodolmor * 0
                er_ajil = hodolmor * 0
                er_daatgal = hodolmor * 0
                uon = hodolmor * float(percent) / 100
            elif insurance_code == '22001':
                t_ajil = hodolmor * 0.085
                t_daatgal = hodolmor * 0.085
                tet_ajil = hodolmor * 0.01
                tet_daatgal = hodolmor * 0.008
                aj_ajil = hodolmor * 0
                aj_daatgal = hodolmor * 0
                er_ajil = hodolmor * 0
                er_daatgal = hodolmor * 0
                uon = hodolmor * float(percent) / 100
            elif insurance_code == '22011':
                t_ajil = hodolmor * 0
                t_daatgal = hodolmor * 0
                tet_ajil = hodolmor * 0.01
                tet_daatgal = hodolmor * 0.008
                aj_ajil = hodolmor * 0
                aj_daatgal = hodolmor * 0
                er_ajil = hodolmor * 0
                er_daatgal = hodolmor * 0
                uon = hodolmor * float(percent) / 100
            elif insurance_code == '06002' or insurance_code == '17002' or insurance_code == '21002':
                t_ajil = hodolmor * 0.085
                t_daatgal = hodolmor * 0
                tet_ajil = hodolmor * 0.01
                tet_daatgal = hodolmor * 0
                aj_ajil = hodolmor * 0.005
                aj_daatgal = hodolmor * 0
                er_ajil = hodolmor * 0.02
                er_daatgal = hodolmor * 0
                uon = hodolmor * float(percent) / 100
                
            niit_baigaa = t_ajil + tet_ajil + aj_ajil + er_ajil+ uon
            niit_daatgal = t_daatgal + tet_daatgal + aj_daatgal + er_daatgal
            # Update employee totals
            employee_id = payslip.employee_id.id
            employee_totals.setdefault(employee_id, 0)
            employee_totals[employee_id] += hodolmor

            sheet.write(rowx, 0, number, data_center)
            sheet.write(rowx, 1, payslip.employee_id.department_id.name, data_center)
            sheet.write(rowx, 2, payslip.employee_id.last_name, data_center)
            sheet.write(rowx, 3, payslip.employee_id.name, data_center)
            sheet.write(rowx, 4, payslip.employee_id.tin_number, data_center)
            sheet.write(rowx, 5, payslip.employee_id.identification_id, data_center)
            sheet.write(rowx, 6, payslip.employee_id.job_id.job_code, data_center)
            sheet.write(rowx, 7, payslip.employee_id.contract_id.insurance_type_id.code, data_center)
            sheet.write(rowx, 8, hodolmor, data_center)
            sheet.write(rowx, 9, t_ajil, data_center)
            sheet.write(rowx, 10, t_daatgal, data_center)
            sheet.write(rowx, 11, tet_ajil, data_center)
            sheet.write(rowx, 12, tet_daatgal, data_center)
            sheet.write(rowx, 13, aj_ajil, data_center)
            sheet.write(rowx, 14, aj_daatgal, data_center)
            sheet.write(rowx, 15, er_ajil, data_center)
            sheet.write(rowx, 16, er_daatgal, data_center)
            sheet.write(rowx, 17, uon, data_center)
            sheet.write(rowx, 18, niit_baigaa, data_center)
            sheet.write(rowx, 19, niit_daatgal, data_center)
            rowx += 1
            number += 1

        inch = 1000
        sheet.col(0).width = 1 * inch
        sheet.col(1).width = 7 * inch
        sheet.col(2).width = 4 * inch
        sheet.col(3).width = 5 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 4 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 2 * inch
        sheet.col(11).width = 2 * inch
        sheet.col(12).width = 2 * inch
        sheet.col(13).width = 2 * inch
        sheet.col(14).width = 2 * inch
        sheet.col(15).width = 2 * inch
        sheet.col(16).width = 2 * inch
        sheet.col(17).width = 2 * inch
        sheet.col(18).width = 3 * inch
        sheet.col(19).width = 2 * inch
        sheet.col(20).width = 2 * inch

        return {'data': book, 'directory_name': u'Ажилтны цагийн бүртгэлийн тайлан',
                'attache_name': u'Ажилтны цагийн бүртгэлийн тайлан'}