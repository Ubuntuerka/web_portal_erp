# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class emp_report(models.TransientModel):
    _inherit = 'abstract.report.excel'
    _name = 'emp_report'

    company_id = fields.Many2one('res.company', string='Компани', required=True,default=lambda self: self.env['res.company']._company_default_get('year.of.employed.reference.wizard'))
    department_id = fields.Many2one('hr.department', string='Хэлтэс', required=True)
    employee_id = fields.Many2one('hr.employee', string='Ажилтан')
    start_date = fields.Date(string='Эхлэх огноо', required=True, default=fields.Date.context_today)
    #end_date = fields.Date(string='Дуусах огноо', required=True, default=fields.Date.context_today)
    is_employee = fields.Boolean(string=u'Хэлтэс/Ажилтан')

    def get_export_data(self):
        company = self.env['res.company'].browse(self.company_id.id)
        emp = self.env['hr.employee'].browse(self.employee_id.id)

        context = self._context
        ezxf = xlwt.easyxf
        styledict = self.get_easyxf_styles()
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('Ажилтны анкет')
        header = ezxf('font: bold on; align: wrap off, vert centre, horiz centre;font: height 220')
        header1 = ezxf('font: bold off; align: wrap on, vert centre, horiz left;font: height 200')
        print_dateL = ezxf('font: bold off; align: wrap off, vert centre, horiz left;font: height 200')
        print_dateR = ezxf('font: bold on; align: wrap on, vert centre, horiz right;font: height 200')
        line = ezxf(
            'font: bold on; align: wrap on, vert centre, horiz left;font: height 200; ')
        border = ezxf(
            'font: bold on; align: wrap on, vert centre, horiz left;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        line2 = ezxf('font: bold on; align: wrap off, vert centre, horiz left;font: height 200; ')
        style1 = ezxf('font: bold off; align: wrap on, vert centre, horiz center;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        line_data = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz right;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        line_center = ezxf(
            'font: bold on; align: wrap on, vert centre, horiz center;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        nonline = ezxf('font: bold off; align: wrap off, vert centre, horiz left;font: height 200;')
        data_center = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')

        
            
        sheet.write_merge(3, 3, 2, 6, u'АЖИЛТНЫ АНКЕТ', ezxf('font: bold on, height 280, name Times New Roman;align:wrap off,vert centre, horiz centre;'))    
        sheet.write_merge(5, 11, 8, 8, u'Сүүлийн 3 сарын дотор авхуулсан 3:4 хэмжээтэй зураг /Photo 3*4/', ezxf('font: bold off, name Times New Roman;borders: top thin, left thin, bottom thin, right thin;align:wrap on,horizontal centre, vert top;'))    
        
        sheet.write_merge(6, 6, 2, 6, u'НЭГ. ҮНДСЭН МЭДЭЭЛЭЛ:', ezxf('font: bold on, height 200, name Times New Roman;align:wrap off,vert centre, horiz left;'))
        sheet.write_merge(8, 8, 0, 0, u'Ургийн овог:', line)
        if emp.surname == False:
            sheet.write_merge(8, 8, 1, 1, u'', line)
        else:
            sheet.write_merge(8, 8, 1, 1, u'%s' %emp.surname, line)
            
        sheet.write_merge(8, 8, 2, 2, u'Овог:', print_dateR)
        if emp.last_name == False:
            sheet.write_merge(8, 8, 3, 3, u'', line2)
        else:
            sheet.write_merge(8, 8, 3, 3, u'%s' %emp.last_name, line2)
        sheet.write_merge(8, 8, 4, 4, u'Нэр:', print_dateR)
        if emp.name == False:
            sheet.write_merge(8, 8, 5, 5, u'', line2)
        else:
            sheet.write_merge(8, 8, 5, 5, u'%s' %emp.name, line2)
        sheet.write_merge(10, 10, 0, 0, u'Компани:', line)
        sheet.write_merge(10, 10, 1, 1, u'%s' %emp.company_id.name, line)
        sheet.write_merge(10, 10, 2, 2, u'Нэгж:', print_dateR)
        sheet.write_merge(10, 10, 3, 5, u'%s' %emp.department_id.name, line2)
        sheet.write_merge(12, 12, 0, 0, u'Албан Тушаал:', line2)
        sheet.write_merge(12, 12, 1, 3, u'%s' %emp.job_id.name, line2)
        sheet.write_merge(12, 12, 4, 4, u'Ажлын Утас:', line)
        if emp.work_phone == False:
            sheet.write_merge(12, 12, 5, 5, u'', line)
        else:
            sheet.write_merge(12, 12, 5, 5, u'%s' %emp.work_phone, line)
        sheet.write_merge(12, 12, 6, 6, u'Ажлын Имэйл:', line)
        if emp.work_email == False:
            sheet.write_merge(12, 12, 7, 8, u'', line)
        else:
            sheet.write_merge(12, 12, 7, 8, u'%s' %emp.work_email, line)
        sheet.write_merge(14, 14, 0, 1, u'Төрсөн огноо:', line)
        sheet.write_merge(14, 14, 2, 3, u'%s' %emp.birthday, line)
        sheet.write_merge(14, 14, 4, 4, u'Нас:', print_dateR)
        if emp.employee_age == False:
            sheet.write_merge(14, 14, 5, 5, u'', line)
        else:
            sheet.write_merge(14, 14, 5, 5, u'%s' %emp.employee_age, line)
        sheet.write_merge(14, 14, 6, 6, u'Хүйс:', print_dateR)
        if emp.gender == 'male':
            sheet.write_merge(14, 14, 7, 7, u'Эрэгтэй', line)
        else:
            sheet.write_merge(14, 14, 7, 7, u'Эмэгтэй', line)
        sheet.write_merge(16, 16, 0, 0, u'Төрсөн улс:', line)
        sheet.write_merge(16, 16, 1, 1, u'%s' %emp.country_of_birth.name, line)
        sheet.write_merge(16, 16, 2, 3, u'Төрсөн газар:', print_dateR)
        sheet.write_merge(16, 16, 4, 5, u'%s' %emp.place_of_birth, line)
        sheet.write_merge(18, 18, 0, 1, u'Регистрын дугаар:', line)
        sheet.write_merge(18, 18, 2, 3, u'%s' %emp.identification_id, line)
        sheet.write_merge(20, 20, 0, 1, u'Оршин суугаа хаяг:', line)
        hayg = emp.dependence_address.name+', '+emp.sum_duureg_id.name+', '+emp.khoroo_id.name+', '+emp.desc_id
        sheet.write_merge(21, 21, 0, 0, u'%s' %hayg, line2)
        '''sheet.write_merge(21, 21, 1, 1, u'%s' %emp.sum_duureg_id.name, line2)
        sheet.write_merge(21, 21, 2, 2, u'%s' %emp.khoroo_id.name, line2)
        sheet.write_merge(21, 21, 3, 4, u'%s' %emp.desc_id, line2)'''
        sheet.write_merge(23, 23, 0, 1, u'Гэрийн утас:', line)
        if emp.home_phone == False:
            sheet.write_merge(23, 23, 2, 2, u'', line)
        else:
            sheet.write_merge(23, 23, 2, 2, u'%s' %emp.home_phone, line)
        sheet.write_merge(23, 23, 3, 4, u'Хувийн утас:', print_dateR)
        if emp.phone == False:
            sheet.write_merge(23, 23, 5, 5, u'', line)
        else:
            sheet.write_merge(23, 23, 5, 5, u'%s' %emp.phone, line)
        sheet.write_merge(25, 25, 0, 1, u'Жолооны ангилал:', line)
        if emp.driver_type_id == 'A':
            sheet.write_merge(25, 25, 2, 2, u'A', line)
        elif emp.driver_type_id.name == 'B':
            sheet.write_merge(25, 25, 2, 2, u'B', line)
        elif emp.driver_type_id.name == 'C':
            sheet.write_merge(25, 25, 2, 2, u'C', line)
        elif emp.driver_type_id.name == 'D':
            sheet.write_merge(25, 25, 2, 2, u'D', line)
        elif emp.driver_type_id.name == 'E':
            sheet.write_merge(25, 25, 2, 2, u'E', line)
        else:
            sheet.write_merge(25, 25, 2, 2, u'', line)
            
        sheet.write_merge(27, 27, 0, 1, u'Гэр бүлтэй эсэх:', line)
        if emp.marital == 'married':
            sheet.write_merge(27, 27, 2, 2, u'Тийм', line)
        else:
            sheet.write_merge(27, 27, 3, 3, u'Үгүй' , line)    
        
        rowx = 5
        col = 3
        num = 1
        
        sheet.write_merge(29, 29, 1, 7, u'ХОЁР. БОЛОВСРОЛ:', ezxf('font: bold on, height 200, name Times New Roman;align:wrap off,vert centre, horiz left;'))
            
        sheet.write_merge(30, 30, 0, 0, u'Хаана:', line_center)
        sheet.write_merge(30, 30, 1, 1, u'Төгссөн сургууль, дамжаа:', line_center)
        sheet.write_merge(30, 30, 2, 2, u'Элссэн огноо:', line_center)
        sheet.write_merge(30, 30, 3, 4, u'Төгссөн огноо:', line_center)
        sheet.write_merge(30, 30, 5, 6, u'Ажил мэргэжил:', line_center)
        sheet.write_merge(30, 30, 7, 7, u'Боловсролын зэрэг:', line_center)
        sheet.write_merge(30, 30, 8, 8, u'Голч:', line_center)
        
        i = 31
        ab = i + 1
        self._cr.execute("""select d.where, d.graduate_school, d.starting_year, d.ending_year, 
                    d.occupation, c.name, d.edu_rate from education as d 
                    left join hr_employee as e on d.hr_employee_id = e.id 
                    left join hr_education_degree as c on d.type_id = c.id 
                    where e.id = '%s' order by d.type_id """%(self.employee_id.id))
        edu_info  = self._cr.dictfetchall()
        
        for line2 in edu_info:
            if line2['where'] == None:
                sheet.write_merge(i, i, 0, 0, u'', style1)
            else:
                sheet.write_merge(i, i, 0, 0, u'%s' %line2['where'], style1)
            if line2['graduate_school'] == None:
                sheet.write_merge(i, i, 1, 1, u'', style1)
            else:
                sheet.write_merge(i, i, 1, 1, u'%s' %line2['graduate_school'], style1)
            if line2['starting_year'] == None:
                sheet.write_merge(i, i, 2, 2, u'', style1)
            else:
                sheet.write_merge(i, i, 2, 2, u'%s' %line2['starting_year'], style1)
            if line2['ending_year'] == None:
                sheet.write_merge(i, i, 3, 4, u'', style1)
            else:
                sheet.write_merge(i, i, 3, 4, u'%s' %line2['ending_year'], style1)
            if line2['occupation'] == None:
                sheet.write_merge(i, i, 5, 6, u'', style1)
            else:
                sheet.write_merge(i, i, 5, 6, u'%s' %line2['occupation'], style1)  
            if line2['name'] == None:
                sheet.write_merge(i, i, 7, 7, u'', style1)
            else:
                sheet.write_merge(i, i, 7, 7, u'%s' %line2['name'], style1)  
            if line2['edu_rate'] == None:
                sheet.write_merge(i, i, 8, 8, u'', style1)
            else:
                sheet.write_merge(i, i, 8, 8, u'%s' %line2['edu_rate'], style1)
            ab = ab + 1
            i +=1
        
        em = ab + 1
        ar = em + 1
        ep = ar + 1
        
        sheet.write_merge(em, em, 1, 7, u'ГУРАВ. АЖИЛ ЭРХЛЭЛТ:', ezxf('font: bold on, height 200, name Times New Roman;align:wrap off,vert centre, horiz left;'))
            
        sheet.write_merge(ar, ar, 0, 0, u'Байгууллагын нэр:', line_center)
        sheet.write_merge(ar, ar, 1, 1, u'Ажилд орсон огноо:', line_center)
        sheet.write_merge(ar, ar, 2, 2, u'Ажлаас гарсан огноо:', line_center)
        sheet.write_merge(ar, ar, 3, 3, u'НДШ төлсөн эсэх:', line_center)
        sheet.write_merge(ar, ar, 4, 4, u'Цалингийн хэмжээ:', line_center)
        sheet.write_merge(ar, ar, 5, 5, u'Албан тушаал:', line_center)
        sheet.write_merge(ar, ar, 6, 6, u'Ажлаас гарсан шалтгаан:', line_center)
        sheet.write_merge(ar, ar, 7, 7, u'Удирдах албан тушаалтны нэр:', line_center)
        sheet.write_merge(ar, ar, 8, 8, u'Удирдах албан тушаалтны утас:', line_center)
        
        self._cr.execute("""select em.name, em.org_start, em.org_finish, em.is_ndd, 
                    em.salary_size, em.org_position, em.org_reason, em.supervisor_name, em.supervisor_phone from employment as em 
                    left join hr_employee as e on em.hr_employee_id = e.id 
                    where e.id = '%s' """%(self.employee_id.id))
        em_info  = self._cr.dictfetchall()
        
        j = ar + 1
        
        for line3 in em_info:
            if line3['name'] == None:
                sheet.write_merge(j, j, 0, 0, u'', style1)
            else:
                sheet.write_merge(j, j, 0, 0, u'%s' %line3['name'], style1)
            if line3['org_start'] == None:
                sheet.write_merge(j, j, 1, 1, u'', style1)
            else:
                sheet.write_merge(j, j, 1, 1, u'%s' %line3['org_start'], style1)
            if line3['org_finish'] == None:
                sheet.write_merge(j, j, 2, 2, u'', style1)
            else:
                sheet.write_merge(j, j, 2, 2, u'%s' %line3['org_finish'], style1)
            if line3['is_ndd'] == True:
                sheet.write_merge(j, j, 3, 3, u'Тийм', style1)
            else:
                sheet.write_merge(j, j, 3, 3, u'Үгүй', style1)
            if line3['salary_size'] == None:
                sheet.write_merge(j, j, 4, 4, u'', style1)
            else:
                sheet.write_merge(j, j, 4, 4, u'%s' %line3['salary_size'], style1)
            if line3['org_position'] == None:
                sheet.write_merge(j, j, 5, 5, u'', style1)
            else:
                sheet.write_merge(j, j, 5, 5, u'%s' %line3['org_position'], style1)  
            if line3['org_reason'] == None:
                sheet.write_merge(j, j, 6, 6, u'', style1)
            else:
                sheet.write_merge(j, j, 6, 6, u'%s' %line3['org_reason'], style1)
            if line3['supervisor_name'] == None:
                sheet.write_merge(j, j, 7, 7, u'', style1)
            else:
                sheet.write_merge(j, j, 7, 7, u'%s' %line3['supervisor_name'], style1)
            if line3['supervisor_phone'] == None:
                sheet.write_merge(j, j, 8, 8, u'', style1)
            else:
                sheet.write_merge(j, j, 8, 8, u'%s' %line3['supervisor_phone'], style1)
            j = j + 1
        
        aw = j + 1
        q = aw + 1
        aa = q + 1
        
        sheet.write_merge(aw, aw, 1, 7, u'ДӨРӨВ. ГАВЬЯА ШАГНАЛ:', ezxf('font: bold on, height 200, name Times New Roman;align:wrap off,vert centre, horiz left;'))
            
        sheet.write_merge(q, q, 0, 2, u'Шагналын нэр:', line_center)
        sheet.write_merge(q, q, 3, 5, u'Он:', line_center)
        sheet.write_merge(q, q, 6, 8, u'Шагнал олгосон байгууллагын нэр:', line_center)
        
        self._cr.execute("""select aw.awards_name, aw.awards_year, aw.org_name from awards_skill as aw 
                    left join hr_employee as e on aw.hr_employee_id = e.id 
                    where e.id ='%s' """%(self.employee_id.id))
        aw_info  = self._cr.dictfetchall()
        
        p = q + 1
        
        for line4 in aw_info:
            if line4['awards_name'] == None:
                sheet.write_merge(p, p, 0, 2, u'', style1)
            else:
                sheet.write_merge(p, p, 0, 2, u'%s' %line4['awards_name'], style1)
            if line4['awards_year'] == None:
                sheet.write_merge(p, p, 3, 5, u'', style1)
            else:
                sheet.write_merge(p, p, 3, 5, u'%s' %line4['awards_year'], style1)
            if line4['org_name'] == None:
                sheet.write_merge(p, p, 6, 8, u'', style1)
            else:
                sheet.write_merge(p, p, 6, 8, u'%s' %line4['org_name'], style1)
            p = p + 1
        
        sk = p + 1
        u = sk + 1
        rr = u + 1
        
        sheet.write_merge(sk, sk, 1, 7, u'ТАВ. ГАДААД ХЭЛНИЙ МЭДЛЭГ:', ezxf('font: bold on, height 200, name Times New Roman;align:wrap off,vert centre, horiz left;'))
            
        sheet.write_merge(u, u, 0, 0, u'Хэлний нэр:', line_center)
        sheet.write_merge(u, u, 1, 1, u'Ярих чадвар:', line_center)
        sheet.write_merge(u, u, 2, 2, u'Сонсох чадвар:', line_center)
        sheet.write_merge(u, u, 3, 3, u'Унших чадвар:', line_center)
        sheet.write_merge(u, u, 4, 4, u'Бичих чадвар:', line_center)
        sheet.write_merge(u, u, 5, 5, u'Суралцсан хугацаа:', line_center)
        sheet.write_merge(u, u, 6, 6, u'Шалгалтын нэр:', line_center)
        sheet.write_merge(u, u, 7, 7, u'Шалгалтын оноо:', line_center)
        sheet.write_merge(u, u, 8, 8, u'Тайлбар:', line_center)
        
        self._cr.execute("""select lan.name, lan.lang_speak, lan.lang_listen, lan.lang_translation, 
                    lan.lang_write, lan.lang_time, lan.exam_name, lan.exam_rate, lan.lang_reason from hr_language as lan 
                    left join hr_employee as e on lan.hr_employee_id = e.id 
                    where e.id = '%s' """%(self.employee_id.id))
        lan_info  = self._cr.dictfetchall()
        
        dd = u + 1 
        
        for line5 in lan_info:
            if line5['name'] == None:
                sheet.write_merge(dd, dd, 0, 0, u'', style1)
            else:
                sheet.write_merge(dd, dd, 0, 0, u'%s' %line5['name'], style1)
            if line5['lang_speak'] == 'bad':
                sheet.write_merge(dd, dd, 1, 1, u'Муу', style1)
            elif line5['lang_speak'] == 'middle':
                sheet.write_merge(dd, dd, 1, 1, u'Дунд', style1)
            elif line5['lang_speak'] == 'good':
                sheet.write_merge(dd, dd, 1, 1, u'Сайн', style1)
            else:
                sheet.write_merge(dd, dd, 1, 1, u'', style1)
            if line5['lang_listen'] == 'bad':
                sheet.write_merge(dd, dd, 2, 2, u'Муу', style1)
            elif line5['lang_listen'] == 'middle':
                sheet.write_merge(dd, dd, 2, 2, u'Дунд', style1)
            elif line5['lang_listen'] == 'good':
                sheet.write_merge(dd, dd, 2, 2, u'Сайн', style1)
            else:
                sheet.write_merge(dd, dd, 2, 2, u'', style1)
            if line5['lang_translation'] == 'bad':
                sheet.write_merge(dd, dd, 3, 3, u'Муу', style1)
            elif line5['lang_translation'] == 'middle':
                sheet.write_merge(dd, dd, 3, 3, u'Дунд', style1)
            elif line5['lang_translation'] == 'good':
                sheet.write_merge(dd, dd, 3, 3, u'Сайн', style1)
            else:
                sheet.write_merge(dd, dd, 3, 3, u'', style1)
            if line5['lang_write'] == 'bad':
                sheet.write_merge(dd, dd, 4, 4, u'Муу', style1)
            elif line5['lang_write'] == 'middle':
                sheet.write_merge(dd, dd, 4, 4, u'Дунд', style1)
            elif line5['lang_write'] == 'good':
                sheet.write_merge(dd, dd, 4, 4, u'Сайн', style1)
            else:
                sheet.write_merge(dd, dd, 4, 4, u'', style1)
            if line5['lang_time'] == None:
                sheet.write_merge(dd, dd, 5, 5, u'', style1)
            else:
                sheet.write_merge(dd, dd, 5, 5, u'%s' %line5['lang_time'], style1)  
            if line5['exam_name'] == None:
                sheet.write_merge(dd, dd, 6, 6, u'', style1)
            else:
                sheet.write_merge(dd, dd, 6, 6, u'%s' %line5['exam_name'], style1)
            if line5['exam_rate'] == None:
                sheet.write_merge(dd, dd, 7, 7, u'', style1)
            else:
                sheet.write_merge(dd, dd, 7, 7, u'%s' %line5['exam_rate'], style1)
            if line5['lang_reason'] == None:
                sheet.write_merge(dd, dd, 8, 8, u'', style1)
            else:
                sheet.write_merge(dd, dd, 8, 8, u'%s' %line5['lang_reason'], style1)
            dd = dd + 1
        
        pr = dd + 1
        cm = pr + 1
        tt = cm + 1
        
        sheet.write_merge(pr, pr, 1, 7, u'ЗУРГАА. КОМПЬЮТЕР ДЭЭР АЖИЛЛАХ УР ЧАДВАР:', ezxf('font: bold on, height 200, name Times New Roman;align:wrap off,vert centre, horiz left;'))
            
        sheet.write_merge(cm, cm, 0, 2, u'Програмын нэр:', line_center)
        sheet.write_merge(cm, cm, 3, 4, u'Хичээллэсэн хугацаа:', line_center)
        sheet.write_merge(cm, cm, 5, 6, u'Ажиллах чадвар:', line_center)
        sheet.write_merge(cm, cm, 7, 8, u'Тайлбар:', line_center)
        
        self._cr.execute("""select cm.name, cm.computer_time, cm.use_skill, cm.skill_reason from computer_skill as cm 
                    left join hr_employee as e on cm.hr_employee_id = e.id 
                    where e.id = '%s' """%(self.employee_id.id))
        cm_info  = self._cr.dictfetchall()
        
        gg = cm + 1 
        
        for line6 in cm_info:
            if line6['name'] == None:
                sheet.write_merge(gg, gg, 0, 2, u'', style1)
            else:
                sheet.write_merge(gg, gg, 0, 2, u'%s' %line6['name'], style1)
            if line6['computer_time'] == None:
                sheet.write_merge(gg, gg, 3, 4, u'', style1)
            else:
                sheet.write_merge(gg, gg, 3, 4, u'%s' %line6['computer_time'], style1)
            if line6['use_skill'] == 'bad':
                sheet.write_merge(gg, gg, 5, 6, u'Муу', style1)
            elif line6['use_skill'] == 'middle':
                sheet.write_merge(gg, gg, 5, 6, u'Дунд', style1)
            elif line6['use_skill'] == 'good':
                sheet.write_merge(gg, gg, 5, 6, u'Сайн', style1)
            elif line6['use_skill'] == 'best':
                sheet.write_merge(gg, gg, 5, 6, u'Мэргэжлийн', style1)
            else:
                sheet.write_merge(gg, gg, 5, 6, u'', style1)
            if line6['skill_reason'] == None:
                sheet.write_merge(gg, gg, 7, 8, u'', style1)
            else:
                sheet.write_merge(gg, gg, 7, 8, u'%s' %line6['skill_reason'], style1)
            gg = gg + 1
        
        ii = gg + 1
        ur = ii + 1
        yy = ur + 1
        
        sheet.write_merge(ii, ii, 1, 7, u'ДОЛОО. УРЛАГ, СПОРТЫН АВЪЯАС:', ezxf('font: bold on, height 200, name Times New Roman;align:wrap off,vert centre, horiz left;'))
            
        sheet.write_merge(ur, ur, 0, 2, u'Урлаг спортын төрөл:', line_center)
        sheet.write_merge(ur, ur, 3, 4, u'Хичээлэсэн хугацаа:', line_center)
        sheet.write_merge(ur, ur, 5, 6, u'Зэрэг, цол:', line_center)
        sheet.write_merge(ur, ur, 7, 8, u'Тайлбар:', line_center)
        
        self._cr.execute("""select ur.name, ur.urlag_time, ur.urlag_name, ur.urlag_reason from urlag_skill as ur 
                    left join hr_employee as e on ur.hr_employee_id = e.id 
                    where e.id = '%s' """%(self.employee_id.id))
        ur_info  = self._cr.dictfetchall()
        
        op = ur + 1 
        
        for line7 in ur_info:
            if line7['name'] == None:
                sheet.write_merge(op, op, 0, 2, u'', style1)
            else:
                sheet.write_merge(op, op, 0, 2, u'%s' %line7['name'], style1)
            if line7['urlag_time'] == None:
                sheet.write_merge(op, op, 3, 4, u'', style1)
            else:
                sheet.write_merge(op, op, 3, 4, u'%s' %line7['urlag_time'], style1)
            if line7['urlag_name'] == None:
                sheet.write_merge(op, op, 5, 6, u'', style1)
            else:
                sheet.write_merge(op, op, 5, 6, u'%s' %line7['urlag_name'], style1)
            if line7['urlag_reason'] == None:
                sheet.write_merge(op, op, 7, 8, u'', style1)
            else:
                sheet.write_merge(op, op, 7, 8, u'%s' %line7['urlag_reason'], style1)
            op = op + 1
            
        hu = op + 1
        gr = hu + 1
        gh = gr + 1 
        
        sheet.write_merge(hu, hu, 1, 7, u'НАЙМ. ГЭР БҮЛИЙН БАЙДАЛ:', ezxf('font: bold on, height 200, name Times New Roman;align:wrap off,vert centre, horiz left;'))
            
        sheet.write_merge(gr, gr, 0, 0, u'Таны хэн болох:', line_center)
        sheet.write_merge(gr, gr, 1, 1, u'Овог:', line_center)
        sheet.write_merge(gr, gr, 2, 2, u'Нэр:', line_center)
        sheet.write_merge(gr, gr, 3, 3, u'Төрсөн огноо:', line_center)
        sheet.write_merge(gr, gr, 4, 4, u'Нас:', line_center)
        sheet.write_merge(gr, gr, 5, 5, u'Хүйс:', line_center)
        sheet.write_merge(gr, gr, 6, 6, u'Ажилладаг байгууллага:', line_center)
        sheet.write_merge(gr, gr, 7, 7, u'Албан тушаал:', line_center)
        sheet.write_merge(gr, gr, 8, 8, u'Холбоо барих утас:', line_center)
        
        self._cr.execute("""select l.who, l.last_name, l.first_name, l.identification_id, l.live_birth, l.age, l.gender, l.is_child,
                    l.work_organization, l.position, l.phone from live_people as l 
                    left join hr_employee as e on l.hr_employee_id = e.id 
                    where e.id = %s order by l.is_child"""%(self.employee_id.id))
        live_info  = self._cr.dictfetchall()

        o = gr + 1
        
        for line0 in live_info:
            if line0['is_child'] == False:
                if line0['who'] == None:
                    sheet.write_merge(o, o, 0, 0, u'', style1)
                else:
                    sheet.write_merge(o, o, 0, 0, u'%s' %line0['who'], style1)
                if line0['last_name'] == None:
                    sheet.write_merge(o, o, 1, 1, u'', style1)
                else:
                    sheet.write_merge(o, o, 1, 1, u'%s' %line0['last_name'], style1)
                if line0['first_name'] == None:
                    sheet.write_merge(o, o, 2, 2, u'', style1)
                else:
                    sheet.write_merge(o, o, 2, 2, u'%s' %line0['first_name'], style1)
                if line0['live_birth'] == None:
                    sheet.write_merge(o, o, 3, 3, u'', style1)
                else:
                    sheet.write_merge(o, o, 3, 3, u'%s' %line0['live_birth'], style1)
                if line0['age'] == None:
                    sheet.write_merge(o, o, 4, 4, u'', style1)
                else:
                    sheet.write_merge(o, o, 4, 4, u'%s' %line0['age'], style1)
                if line0['gender'] == 'male':
                    sheet.write_merge(o, o, 5, 5, u'Эрэгтэй', style1)
                else:
                    sheet.write_merge(o, o, 5, 5, u'Эмэгтэй', style1)
                if line0['work_organization'] == None:
                    sheet.write_merge(o, o, 6, 6, u'', style1)
                else:
                    sheet.write_merge(o, o, 6, 6, u'%s' %line0['work_organization'], style1)
                if line0['position'] == None:
                    sheet.write_merge(o, o, 7, 7, u'', style1)
                else:
                    sheet.write_merge(o, o, 7, 7, u'%s' %line0['position'], style1)
                if line0['phone'] == None:
                    sheet.write_merge(o, o, 8, 8, u'', style1)
                else:
                    sheet.write_merge(o, o, 8, 8, u'%s' %line0['phone'], style1)
                o = o + 1
        n = o + 1
        sheet.write_merge(n, n, 0, 2, u'Хүүхдийн овог', line_center)
        sheet.write_merge(n, n, 3, 4, u'Хүүхдийн нэр', line_center)
        sheet.write_merge(n, n, 5, 6, u'Төрсөн огноо', line_center)
        sheet.write_merge(n, n, 7, 8, u'Сургууль, цэцэрлэг', line_center)
            
        zaaz = n + 1
        for line1 in live_info:
                
            if line1['is_child'] == True:
                if line1['last_name'] == None:
                    sheet.write_merge(zaaz, zaaz, 0, 2, u'', style1)
                else:
                    sheet.write_merge(zaaz, zaaz, 0, 2, u'%s' %line1['last_name'], style1)
                if line1['first_name'] == None:
                    sheet.write_merge(zaaz, zaaz, 3, 4, u'', style1)
                else:
                    sheet.write_merge(zaaz, zaaz, 3, 4, u'%s' %line1['first_name'], style1)
                if line1['live_birth'] == None:
                    sheet.write_merge(zaaz, zaaz, 5, 6, u'', style1)
                else:
                    sheet.write_merge(zaaz, zaaz, 5, 6, u'%s' %line1['live_birth'], style1)
                if line1['work_organization'] == None:
                    sheet.write_merge(zaaz, zaaz, 7, 8, u'', style1)
                else:
                    sheet.write_merge(zaaz, zaaz, 7, 8, u'%s' %line1['work_organization'], style1)
                zaaz = zaaz + 1
        
        w = zaaz + 2
        x = w + 2
        y = x + 2
        sheet.write_merge(w, w, 1, 8, u'Гарын үсэг:',ezxf('font: bold on, height 200, name Times New Roman;align:wrap on,vert centre, horiz left;'))
        sheet.write_merge(x, x, 1, 8, u'Огноо:',ezxf('font: bold on, height 200, name Times New Roman;align:wrap on,vert centre, horiz left;'))
        sheet.write_merge(y, y, 0, 8, u'ТАНД АМЖИЛТ ХҮСЬЕ:',ezxf('font: bold on, height 220, name Times New Roman;align:wrap off,vert centre, horiz center;'))
        
        inch = 2000
        inch1 = 5000
        sheet.col(0).width = 1*inch
        sheet.col(1).width = 1*inch
        sheet.col(2).width = 1*inch
        sheet.col(3).width = 1*inch
        sheet.col(4).width = 1*inch
        sheet.col(5).width = 1*inch
        sheet.col(6).width = 1*inch
        sheet.col(7).width = 1*inch
        sheet.col(8).width = 1*inch
        sheet.col(9).width = 1*inch
        sheet.col(10).width = 1*inch
        sheet.col(11).width = 1*inch
        
        
        
        self._cr.execute("""select e.last_name, e.name as first_name, e.employee_code, e.employee_age, y.name as emp_type, e.accession_date, c.name as company_id,  h.name as department_id, 
                            j. name as job_id, d.name as rank_code, e.working_year, e.working_year_other1, t.name, t.org_start, t.org_finish, t.is_ndd,t.salary_size, 
                            t.org_position, t.org_reason, t.supervisor_name, t.supervisor_phone
                            from hr_employee as e 
                            left join employment as t on t.hr_employee_id = e.id
                            left join hr_decree as d on d.hr_employee_id = e.id
                            left join emp_type as y on y.id = e.emp_type
                            left join res_company as c on c.id = e.company_id
                            left join hr_department as h on h.id = e.department_id
                            left join hr_job as j on j.id = e.job_id
                            where e.company_id =%d and e.department_id =%d and date(e.accession_date) >='%s'
                            """ % (self.company_id.id, self.department_id.id, self.start_date))
        employed_reference = self._cr.dictfetchall()

        rowx += 1
        if employed_reference != []:
            number = 0

            for emp in employed_reference:
                number += 1
                # if emp

                sheet.write(rowx, 0, number, data_center)
                sheet.write(rowx, 1, '%s' % emp['last_name'], data_center)
                sheet.write(rowx, 2, '%s' % emp['first_name'], data_center)
                sheet.write(rowx, 3, '%s' % emp['employee_code'], data_center)
                sheet.write(rowx, 4, '%s' % emp['employee_age'], data_center)
                sheet.write(rowx, 5, '%s' % emp['emp_type'], data_center)
                sheet.write(rowx, 6, '%s' % emp['accession_date'], data_center)
                sheet.write(rowx, 7, '%s' % emp['company_id'], data_center)
                sheet.write(rowx, 8, '%s' % emp['department_id'], data_center)
                sheet.write(rowx, 9, '%s' % emp['job_id'], data_center)
                sheet.write(rowx, 10, '%s' % emp['rank_code'], data_center)
                sheet.write(rowx, 11, '%s' % emp['working_year'], data_center)
                sheet.write(rowx, 12, '%s' % emp['working_year_other1'], data_center)
                sheet.write(rowx, 13, '%s' % emp['name'], data_center)
                sheet.write(rowx, 14, '%s' % emp['org_start'], data_center)
                sheet.write(rowx, 15, '%s' % emp['org_finish'], data_center)
                sheet.write(rowx, 16, '%s' % emp['is_ndd'], data_center)
                sheet.write(rowx, 17, '%s' % emp['salary_size'], data_center)
                sheet.write(rowx, 18, '%s' % emp['org_position'], data_center)
                sheet.write(rowx, 19, '%s' % emp['org_reason'], data_center)
                sheet.write(rowx, 20, '%s' % emp['supervisor_name'], data_center)
                sheet.write(rowx, 21, '%s' % emp['supervisor_phone'], data_center)


                rowx += 1
        inch = 1000
        sheet.col(0).width = 3 * inch
        sheet.col(1).width = 3 * inch
        sheet.col(2).width = 2 * inch
        sheet.col(3).width = 2 * inch
        sheet.col(4).width = 2 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 3 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 3 * inch
        sheet.col(9).width = 3 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 3 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.col(16).width = 3 * inch
        sheet.col(17).width = 3 * inch
        sheet.col(18).width = 3 * inch
        sheet.col(19).width = 3 * inch
        sheet.row(2).height = 400

        return {'data': book, 'directory_name': u'Ажилтны анкет',
                'attache_name': u'Ажилтны анкет'}