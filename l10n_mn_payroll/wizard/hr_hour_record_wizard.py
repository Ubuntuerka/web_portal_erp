# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class HrHourRecordWizard(models.TransientModel):
    _inherit = 'abstract.report.excel'
    _name = 'hr.hour.record.wizard'

    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env['res.company']._company_default_get('hr.hour.record'))
    department_id = fields.Many2one('hr.department', string='Department', required=True)
    start_date = fields.Date(string='Start date', required=True, default=fields.Date.context_today)
    end_date = fields.Date(string='End date', required=True, default=fields.Date.context_today)

    def get_export_data(self):
        company = self.env['res.company'].browse(self.company_id.id)

        context = self._context
        ezxf = xlwt.easyxf
        styledict = self.get_easyxf_styles()
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('Ажилтны цагийн бүртгэлийн тайлан')
        header = ezxf('font: bold on; align: wrap off, vert centre, horiz centre;font: height 220')
        header1 = ezxf('font: bold off; align: wrap on, vert centre, horiz left;font: height 200')
        print_dateL = ezxf('font: bold off; align: wrap off, vert centre, horiz left;font: height 200')
        print_dateR = ezxf('font: bold off; align: wrap off, vert centre, horiz right;font: height 200')
        line = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz left;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        line_data = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz right;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        line_center = ezxf(
            'font: bold on; align: wrap on, vert centre, horiz center;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        nonline = ezxf('font: bold off; align: wrap off, vert centre, horiz left;font: height 200;')
        data_center = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')
        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 15, u'Байгууллагын нэр: %s' % (self.department_id.name),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 0, u'Тайлан эхлэх огноо: %s' % (self.start_date),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 15, u'Тайлан дуусах огноо: %s' % (self.end_date),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 6, u'Ажилтны цагийн бүртгэлийн тайлан',
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))



        rowx = 5
        col = 3
        num = 1
        sheet.write(rowx, 0, u'№', styledict['heading_xf'])
        sheet.write(rowx, 1, u'Албан тушаал', styledict['heading_xf'])
        sheet.write(rowx, 2, u'Ажилтны нэр', styledict['heading_xf'])
        sheet.write(rowx, 3, u'Ажиллавал зохих хоног', styledict['heading_xf'])
        sheet.write(rowx, 4, u'Ажиллавал зохих цаг', styledict['heading_xf'])
        sheet.write(rowx, 5, u'Ажилласан хоног', styledict['heading_xf'])
        sheet.write(rowx, 6, u'Ажилласан цаг', styledict['heading_xf'])
        sheet.write(rowx, 7, u'Цалинд тооцоолсон цаг', styledict['heading_xf'])
        sheet.write(rowx, 8, u'Тасалсан', styledict['heading_xf'])
        sheet.write(rowx, 9, u'Өвчтэй цаг', styledict['heading_xf'])
        sheet.write(rowx, 10, u'Амралттай', styledict['heading_xf'])
        sheet.write(rowx, 11, u'Хоцорсон цаг /өглөө/', styledict['heading_xf'])
        sheet.write(rowx, 12, u'Чөлөөтэй цаг /цалинтай/', styledict['heading_xf'])
        sheet.write(rowx, 13, u'Чөлөөтэй цаг /цалингүй/', styledict['heading_xf'])
        sheet.write(rowx, 14, u'Илүү цаг', styledict['heading_xf'])
        sheet.write(rowx, 15, u'Албан ёсны амралтын өдөр ажилласан цаг', styledict['heading_xf'])
        # sheet.write(rowx, 15, u'Хуруу бүртгүүлээгүй', styledict['heading_xf'])
        sheet.write(rowx, 16, u'Томилолт', styledict['heading_xf'])
        sheet.write(rowx, 17, u'Сургалт', styledict['heading_xf'])
        # sheet.write(rowx, 17, u'Гадуур ажилласан цаг', styledict['heading_xf'])
        # sheet.write(rowx, 18, u'Сургалт', styledict['heading_xf'])
        # sheet.write(rowx, 19, u'Гэр бүлийн өдөр', styledict['heading_xf'])
        # sheet.write(rowx, 20, u'Нөхөж амралт', styledict['heading_xf'])
        # sheet.write(rowx, 21, u'Сул зогсолт', styledict['heading_xf'])
        # sheet.write(rowx, 22, u'Гэрээс ажилласан', styledict['heading_xf'])
        # sheet.write(rowx, 15, u'Нийт тооцоолсон цаг', styledict['heading_xf'])

        self.env.cr.execute("""select h.company_id, h.department as department_id, h.start_date, h.end_date,
                            j.name as position_id, e.name as employee_id, l.due_day, l.due_time, l.work_day, l.work_time,
                            l.sick_time, l.shift_holiday, l.miss_time_morning, l.night_time, l.free_time1, l.total_time,
                            l.free_time2, l.more_time, l.no_finger,l.tasalsan, l.more_time1, l.assignment, l.out_work, l.more_times2, l.training,
                            l.family_day, l.patch_day, l.sul_zogsolt, l.online, l.training
                            from hr_hour_record as h
                            left join hr_hour_record_line as l on l.parentd_id = h.id
                            left join hr_job as j on j.id = l.position
							left join hr_employee as e on e.id = l.employee_id
                            where h.state='confirmed' and h.company_id ='%d' and h.department ='%d' and h.start_date >='%s' and h.end_date <='%s'
                            """%(self.company_id.id, self.department_id.id, self.start_date, self.end_date))
        hr_hour_records = self._cr.dictfetchall()

        rowx += 1
        if hr_hour_records !=[]:
            number = 0

            for hr in hr_hour_records:
                number += 1

                sheet.write(rowx, 0, number, data_center)
                sheet.write(rowx, 1, '%s' % hr['position_id'], data_center)
                sheet.write(rowx, 2, '%s' % hr['employee_id'], data_center)
                sheet.write(rowx, 3, '%s' % (hr['due_day'] or 0.0), data_center)
                sheet.write(rowx, 4, '%s' % (hr['due_time'] or 0.0), data_center)
                sheet.write(rowx, 5, '%s' % (hr['work_day'] or 0.0), data_center)
                sheet.write(rowx, 6, '%s' % (hr['work_time'] or 0.0), data_center)
                sheet.write(rowx, 7, '%s' % (hr['total_time'] or 0.0), data_center)
                sheet.write(rowx, 8, '%s' % (hr['tasalsan'] or 0.0), data_center)
                sheet.write(rowx, 9, '%s' % (hr['sick_time'] or 0.0), data_center)
                sheet.write(rowx, 10, '%s' % (hr['shift_holiday'] or 0.0), data_center)
                sheet.write(rowx, 11, '%s' % (hr['miss_time_morning'] or 0.0), data_center)
                sheet.write(rowx, 12, '%s' % (hr['free_time1'] or 0.0), data_center)
                sheet.write(rowx, 13, '%s' % (hr['free_time2'] or 0.0), data_center)
                sheet.write(rowx, 14, '%s' % (hr['more_time'] or 0.0), data_center)
                sheet.write(rowx, 15, '%s' % (hr['more_time1'] or 0.0), data_center)
                sheet.write(rowx, 16, '%s' % (hr['assignment'] or 0.0), data_center)
                sheet.write(rowx, 17, '%s' % (hr['training'] or 0.0), data_center)
                # sheet.write(rowx, 15, '%s' % hr['no_finger'], data_center)
                # sheet.write(rowx, 17, '%s' % hr['out_work'], data_center)
                # sheet.write(rowx, 18, '%s' % hr['training'], data_center)
                # sheet.write(rowx, 19, '%s' % hr['family_day'], data_center)
                # sheet.write(rowx, 20, '%s' % hr['patch_day'], data_center)
                # sheet.write(rowx, 21, '%s' % hr['sul_zogsolt'], data_center)
                # sheet.write(rowx, 22, '%s' % hr['online'], data_center)


                rowx += 1

        inch = 1000
        sheet.col(0).width = 2 * inch
        sheet.col(1).width = 5 * inch
        sheet.col(2).width = 5 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 3 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.col(16).width = 3 * inch
        sheet.col(17).width = 3 * inch
        sheet.col(18).width = 3 * inch
        sheet.col(19).width = 3 * inch
        sheet.row(2).height = 400

        return {'data': book, 'directory_name': u'Ажилтны цагийн бүртгэлийн тайлан',
                'attache_name': u'Ажилтны цагийн бүртгэлийн тайлан'}