from . import payroll_prepayment_report
from . import payroll_report
from . import hr_hour_record_wizard
from . import year_of_employed_reference_wizard
from . import wallet_bank_payment_wizard
from . import wallet_bank_last_payment_wizard
from . import social_security_report_wizard
from . import employee_report
