# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Agile Business Group sagl (<http://www.agilebg.com>)
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta, datetime
from odoo import models, fields, api, _

import time
import xlwt
import openerp.netsvc

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

class YearOfEmployedReferenceWizard(models.TransientModel):
    _inherit = 'abstract.report.excel'
    _name = 'year.of.employed.reference.wizard'

    company_id = fields.Many2one('res.company', string='Компани', required=True,
                                 default=lambda self: self.env['res.company']._company_default_get('year.of.employed.reference.wizard'))
    department_id = fields.Many2one('hr.department', string='Хэлтэс', required=True)
    employee_id = fields.Many2one('hr.employee', string='Ажилтан')
    start_date = fields.Date(string='Эхлэх огноо', required=True, default=fields.Date.context_today)
    end_date = fields.Date(string='Дуусах огноо', required=True, default=fields.Date.context_today)
    is_employee = fields.Boolean(string=u'Хэлтэс/Ажилтан')

    def get_export_data(self):
        company = self.env['res.company'].browse(self.company_id.id)

        context = self._context
        ezxf = xlwt.easyxf
        styledict = self.get_easyxf_styles()
        book = xlwt.Workbook(encoding='utf8')
        sheet = book.add_sheet('Ажилтны цагийн бүртгэлийн тайлан')
        header = ezxf('font: bold on; align: wrap off, vert centre, horiz centre;font: height 220')
        header1 = ezxf('font: bold off; align: wrap on, vert centre, horiz left;font: height 200')
        print_dateL = ezxf('font: bold off; align: wrap off, vert centre, horiz left;font: height 200')
        print_dateR = ezxf('font: bold off; align: wrap off, vert centre, horiz right;font: height 200')
        line = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz left;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        line_data = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz right;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        line_center = ezxf(
            'font: bold on; align: wrap on, vert centre, horiz center;font: height 200; borders: left thin, right thin, top thin, bottom thin;')
        nonline = ezxf('font: bold off; align: wrap off, vert centre, horiz left;font: height 200;')
        data_center = ezxf(
            'font: bold off; align: wrap on, vert centre, horiz center; font: height 200;borders: left thin, right thin, top thin, bottom thin;',
            '#,##0.00_);(#,##0.00)')

        sheet.write(0, 0, u'Байгууллагын нэр: %s' % (company.name),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(0, 15, u'Байгууллагын нэр: %s' % (self.department_id.name),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(2, 0, u'Тайлан эхлэх огноо: %s' % (self.start_date),
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        # sheet.write(2, 15, u'Тайлан дуусах огноо: %s' % (self.end_date),
        #             ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))
        sheet.write(3, 6, u'Ажилтны цагийн бүртгэлийн тайлан',
                    ezxf('font:bold on;align:wrap off,vert centre,horiz left;font: height 180'))

        rowx = 5
        col = 3
        num = 1
        sheet.write(rowx, 0, u'№', styledict['heading_xf'])
        sheet.write(rowx, 1, u'Эцэг/эх/-ийн нэр', styledict['heading_xf'])
        sheet.write(rowx, 2, u'Нэр', styledict['heading_xf'])
        sheet.write(rowx, 3, u'Ажилтны код', styledict['heading_xf'])
        sheet.write(rowx, 4, u'Ажилтны нас', styledict['heading_xf'])
        sheet.write(rowx, 5, u'Төрөл', styledict['heading_xf'])
        sheet.write(rowx, 6, u'Ажилд орсон огноо', styledict['heading_xf'])
        sheet.write(rowx, 7, u'Компаний нэр', styledict['heading_xf'])
        sheet.write(rowx, 8, u'Байгууллагын нэгжийн нэр', styledict['heading_xf'])
        sheet.write(rowx, 9, u'Ажлын байрны нэр', styledict['heading_xf'])
        sheet.write(rowx, 10, u'Зэргийн код', styledict['heading_xf'])
        sheet.write(rowx, 11, u'Ажилласан жил', styledict['heading_xf'])
        sheet.write(rowx, 12, u'Улсад ажилласан жил', styledict['heading_xf'])
        sheet.write(rowx, 13, u'Байгууллагын нэр', styledict['heading_xf'])
        sheet.write(rowx, 14, u'Ажилд орсон огноо', styledict['heading_xf'])
        sheet.write(rowx, 15, u'Ажлаас гарсан огноо', styledict['heading_xf'])
        sheet.write(rowx, 16, u'НДШ төлсөн эсэх', styledict['heading_xf'])
        sheet.write(rowx, 17, u'Цалингийн хэмжээ', styledict['heading_xf'])
        sheet.write(rowx, 18, u'Албан тушаал', styledict['heading_xf'])
        sheet.write(rowx, 19, u'Ажлаас гарсан шалтгаан', styledict['heading_xf'])
        sheet.write(rowx, 20, u'Удирдах албан тушаалтны нэр', styledict['heading_xf'])
        sheet.write(rowx, 21, u'Удирдах албан тушаалтны утас', styledict['heading_xf'])

        # hr_emp_obj = self.env['hr.employee']
        #
        # if self.is_employee == True:
        #     if self.employee_id:
        #         shts_ids = shts_obj.search([('region_id','=',self.region_id.id)])

        self.env.cr.execute("""select e.last_name, e.name as first_name, e.employee_code, e.employee_age, y.name as emp_type, e.accession_date, c.name as company_id,  h.name as department_id, 
                            j. name as job_id, d.name as rank_code, e.working_year, e.working_year_other1, t.name, t.org_start, t.org_finish, t.is_ndd,t.salary_size, 
                            t.org_position, t.org_reason, t.supervisor_name, t.supervisor_phone
                            from hr_employee as e 
                            left join employment as t on t.hr_employee_id = e.id
                            left join hr_decree as d on d.hr_employee_id = e.id
                            left join emp_type as y on y.id = e.emp_type
                            left join res_company as c on c.id = e.company_id
                            left join hr_department as h on h.id = e.department_id
                            left join hr_job as j on j.id = e.job_id
                            where e.company_id =%d and e.department_id =%d and date(e.accession_date) >='%s'
                            """ % (self.company_id.id, self.department_id.id, self.start_date))
        employed_reference = self._cr.dictfetchall()

        rowx += 1
        if employed_reference != []:
            number = 0

            for emp in employed_reference:
                number += 1
                # if emp

                sheet.write(rowx, 0, number, data_center)
                sheet.write(rowx, 1, '%s' % emp['last_name'], data_center)
                sheet.write(rowx, 2, '%s' % emp['first_name'], data_center)
                sheet.write(rowx, 3, '%s' % emp['employee_code'], data_center)
                sheet.write(rowx, 4, '%s' % emp['employee_age'], data_center)
                sheet.write(rowx, 5, '%s' % emp['emp_type'], data_center)
                sheet.write(rowx, 6, '%s' % emp['accession_date'], data_center)
                sheet.write(rowx, 7, '%s' % emp['company_id'], data_center)
                sheet.write(rowx, 8, '%s' % emp['department_id'], data_center)
                sheet.write(rowx, 9, '%s' % emp['job_id'], data_center)
                sheet.write(rowx, 10, '%s' % emp['rank_code'], data_center)
                sheet.write(rowx, 11, '%s' % emp['working_year'], data_center)
                sheet.write(rowx, 12, '%s' % emp['working_year_other1'], data_center)
                sheet.write(rowx, 13, '%s' % emp['name'], data_center)
                sheet.write(rowx, 14, '%s' % emp['org_start'], data_center)
                sheet.write(rowx, 15, '%s' % emp['org_finish'], data_center)
                sheet.write(rowx, 16, '%s' % emp['is_ndd'], data_center)
                sheet.write(rowx, 17, '%s' % emp['salary_size'], data_center)
                sheet.write(rowx, 18, '%s' % emp['org_position'], data_center)
                sheet.write(rowx, 19, '%s' % emp['org_reason'], data_center)
                sheet.write(rowx, 20, '%s' % emp['supervisor_name'], data_center)
                sheet.write(rowx, 21, '%s' % emp['supervisor_phone'], data_center)


                rowx += 1
        inch = 1000
        sheet.col(0).width = 1 * inch
        sheet.col(1).width = 4 * inch
        sheet.col(2).width = 5 * inch
        sheet.col(3).width = 3 * inch
        sheet.col(4).width = 4 * inch
        sheet.col(5).width = 3 * inch
        sheet.col(6).width = 4 * inch
        sheet.col(7).width = 3 * inch
        sheet.col(8).width = 4 * inch
        sheet.col(9).width = 4 * inch
        sheet.col(10).width = 4 * inch
        sheet.col(11).width = 4 * inch
        sheet.col(12).width = 3 * inch
        sheet.col(13).width = 3 * inch
        sheet.col(14).width = 3 * inch
        sheet.col(15).width = 3 * inch
        sheet.col(16).width = 3 * inch
        sheet.col(17).width = 3 * inch
        sheet.col(18).width = 3 * inch
        sheet.col(19).width = 3 * inch
        sheet.row(2).height = 400

        return {'data': book, 'directory_name': u'Ажилтны цагийн бүртгэлийн тайлан',
                'attache_name': u'Ажилтны цагийн бүртгэлийн тайлан'}