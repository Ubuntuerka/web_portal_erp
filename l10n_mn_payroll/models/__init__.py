# -*- coding: utf-8 -*-
from . import hr_hour_record
from . import hr_payroll_prepayment
from . import absract_report
from . import hr_payroll
from . import hr_legal_leaves_payroll
