# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64

from odoo import api, fields, models
from odoo import tools, _

from datetime import date
from datetime import timedelta
from datetime import datetime
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
ATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class HrPayrollPrepayment(models.Model):
    _name = 'hr.payroll.prepayment'
    
    now_date = fields.Date(string='Register date',default=fields.Date.context_today)
    start_date = fields.Datetime('Start Date',required=True,default=fields.Date.context_today)
    end_date = fields.Datetime('End date',required=True,default=fields.Date.context_today)
    name = fields.Char(string=u'Move value',required=True)
    type = fields.Char(string=u'Move type',required=True)
    dep_id = fields.Many2one('hr.department',string='Department',)
    company_id = fields.Many2one('res.company',string='Company',required=True,default = lambda self: self.env['res.company']._company_default_get('hr.payroll.prepayment'))
    create_user = fields.Many2one('res.users','Employee',default=lambda self: self.env.user)
    state = fields.Selection([('draft','Draft'),
                              ('check','Check'),
                              ('confirm','Confirm')],string='State',default='draft')
    percent = fields.Float('Prepayment payroll percent')
    work_days = fields.Float(string='Work days',compute='_onchange_work_days')
    journal_id = fields.Many2one('account.journal','Journal')
    line_id = fields.One2many('hr.payroll.prepayment.line','prepayment_id','Hr payroll prepayment line')
    move_id = fields.Many2one('account.move',string='Account move')
    
    
    
    
    def workdaysub(uid,start_date, end_date):
        '''
        Calculate the number of working days between two dates inclusive
        (start_date <= end_date).
    
        The actual working days can be set with the optional whichdays
        parameter
        (default is MON-FRI)
        '''
        (MON, TUE, WED, THU, FRI, SAT, SUN) = range(7)
        whichdays=(MON,TUE,WED,THU,FRI)
        delta_days = (end_date - start_date).days + 1
        full_weeks, extra_days = divmod(delta_days, 7)
        num_workdays = (full_weeks + 1) * len(whichdays)
        for d in range(1, 8 - extra_days):
                    if (end_date + timedelta(d)).weekday() in whichdays:
                                    num_workdays -= 1
        return num_workdays
                
    @api.depends('start_date', 'end_date')
    def _onchange_work_days(self):
        for line in self:
            days = 0.0
            days = self.workdaysub(line.start_date,line.end_date)
            line.work_days = days



    def action_check(self):
        self.write({'state':'check'})
    def action_import_time(self):
        hr_emp = self.env['hr.employee']
        hr_cont = self.env['hr.contract']
        line = self.env['hr.payroll.prepayment.line']
        hr_leave_obj = self.env['hr.leave']
        decree_obj = self.env['hr.decree']
        obj = self.browse(self.id)
        if obj.dep_id.id != False:
            hr_emp_id = hr_emp.search([('company_id', '=', obj.company_id.id),('department_id', '=', obj.dep_id.id),('emp_status','=','emp_active')], order='department_id')
        else:
            hr_emp_id = hr_emp.search([('company_id', '=', obj.company_id.id),('emp_status','=','emp_active')], order='department_id')
        pre_pay = 0
        if hr_emp_id != []:
            for hr in hr_emp_id:
                work_time = 0.0
                work_day = 0.0
                leave_id = hr_leave_obj.search([('date_from', '>=', self.start_date), ('date_to', '<=', self.end_date),
                                                ('employee_id', '=', hr.id)])
                if leave_id:
                    for leave in leave_id:
                        if leave:
                            work_time += leave.number_of_hours_display
                            work_day += 1
                
                decree_id = decree_obj.search([('hr_employee_id','=',hr.id),('state','=','approved'),('decree_type_sel','in',('ajild_avah','jinhlen_ajiluulah','ajild_avah1')),('start_date','=',self.start_date),('end_date','=',self.end_date)],limit=1)
                if decree_id.id==False:
                    work_day = 0.0
                    work_time = 0.0
                    
                hr_cont_id = hr_cont.search([('employee_id', '=', hr.id), ('state','=','open'),('company_id', '=', obj.company_id.id),('contract_type','in',('SHTS','erhlegch','office_gtba','NHO'))],limit=1)
                if hr_cont_id.id != [] and hr_cont_id.id != False:
                    for payroll in hr_cont_id:
                        if payroll.contract_type == 'SHTS':
                            work_time = 0.0
                            work_day = 0.0
                            pre_pay = ((
                                                   payroll.matrits.amount + payroll.add_amount) / 100.0 * self.percent) / self.work_days * work_day
                            line_id = line.search([('prepayment_id', '=', self.id), ('name', '=', hr.id)])
                            if line_id.id == False or line_id.id == []:
                                line.create({
                                    'name': hr.id,
                                    'work_time': work_time,
                                    'work_days': work_day,
                                    'prepayment_id': self.id,
                                })
                        if payroll.decree_id.state == 'approved':
                            if payroll.shuud_udirdlaga==True:
                                work_time +=self.work_days*8
                                work_day +=self.work_days


                            pre_pay = ((
                                                   payroll.decree_id.matrits.amount + payroll.decree_id.add_amount) / 100.0 * self.percent) / self.work_days * work_day
                            line_id = line.search([('prepayment_id', '=', self.id), ('name', '=', hr.id)])
                            if line_id.id == False or line_id.id == []:
                                line.create({
                                    'name': hr.id,
                                    'work_time': work_time,
                                    'work_days': work_day,
                                    'prepayment_id': self.id,
                                })
                                
                else:
                    raise UserError(
                        _(u'%s -ны %s ажилтан дээр хөдөлмөрийн гэрээ үүсээгүй байна.') % (obj.company_id.name, hr.name))

        return True
    
    def action_compute_salary(self):

        hr_emp = self.env['hr.employee']
        hr_cont = self.env['hr.contract']
        line = self.env['hr.payroll.prepayment.line']
        hr_leave_obj = self.env['hr.leave']
        obj = self.browse(self.id)
        if obj.dep_id.id !=False:
            hr_emp_id = hr_emp.search( [('company_id','=',obj.company_id.id),('department_id','=',obj.dep_id.id),('emp_status','=','emp_active')], order='department_id')
        else:
            hr_emp_id = hr_emp.search( [('company_id','=',obj.company_id.id),('emp_status','=','emp_active')], order='department_id')
        pre_pay = 0
        if hr_emp_id != []:
            for hr in hr_emp_id:
                work_time = 0.0
                work_day = 0.0
                leave_id = hr_leave_obj.search([('date_from','>=',self.start_date),('date_to','<=',self.end_date),('employee_id','=',hr.id)])
                if leave_id:
                    for leave in leave_id:
                        if leave:
                            work_time +=leave.number_of_hours_display
                            work_day += 1

                hr_cont_id = hr_cont.search([('employee_id','=',hr.id),('company_id','=',obj.company_id.id)])
                if  hr_cont_id.id != [] and hr_cont_id.id !=False:
                    for payroll in hr_cont_id:
                        if payroll.decree_id.state=='approved':
                            pre_pay = ((payroll.decree_id.matrits.amount+payroll.decree_id.add_amount)/100.0*self.percent)/self.work_days*work_day
                            for lines in self.line_id:
                                if lines.name.id==hr.id:
                                    lines.write({
                                                  'prepayment_payroll':pre_pay,
                                                  })
                
                else:
                   raise UserError(_(u'%s -ны %s ажилтан дээр хөдөлмөрийн гэрээ үүсээгүй байна.')%(obj.company_id.name,hr.name))

                    
        return True
    
    def action_confirm(self):
        journal_obj = self.env['account.journal']
        move_obj = self.env['account.move']
        move_line_obj = self.env['account.move.line']
        obj = self.browse(self.id)
        date = obj.now_date
 
        self.write({'state':'confirm',

                    'name':self.env['ir.sequence'].next_by_code('hr.payroll.prepayment')})
        return True
    def action_cancel(self):
        moves = self.mapped('move_id')
        moves.button_draft()
        moves.line_ids.unlink()
        self.write({'state':'draft'})
        return True

    def action_print(self):
        context = dict(self._context)
        obj = self.browse(self.id)
        res = []
        for lin in obj.line_id:
            res.append(lin.id)
        context['start_date'] = obj.start_date
        context['end_date'] = obj.end_date
        context['line_id'] = res
        context['company'] = obj.company_id.name
        #context['create'] = u'%s'%obj.create_user.last_name+'.'+obj.create_user.name
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'payroll.prepayment.report',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context':context,
            'nodestroy': True,
        }



class HrPayrollPrepaymentLine(models.Model):
    _name = 'hr.payroll.prepayment.line'
    
    
    
    name = fields.Many2one('hr.employee',string='Employee name',required=True)
    is_bank = fields.Boolean(string='Is bank')
    prepayment_payroll = fields.Float(string='Prepayment payroll')
    other_payment = fields.Float('Other payment')
    work_days = fields.Float(string='Work days')
    work_time = fields.Float(string=u'Ажилласан цаг')
    hand_give_amount = fields.Float(string='At hand give',compute='_compute_amount_total', store=True)
    prepayment_id = fields.Many2one('hr.payroll.prepayment',string='Hr payroll prepayment')
                   
                   
    @api.depends('prepayment_payroll', 'other_payment')
    def _compute_amount_total(self):
        for line in self:
            line.hand_give_amount = line.prepayment_payroll-line.other_payment
        