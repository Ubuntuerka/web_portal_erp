# -*- coding: utf-8 -*-
import time
import math

from odoo.osv import expression
from odoo.tools.float_utils import float_round as round
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from dateutil.relativedelta import relativedelta
from pytz import timezone
from pytz import utc

from odoo import api, fields, models, tools, _
from datetime import date, datetime, time
from datetime import timedelta
import babel
from collections import defaultdict
from pytz import timezone, UTC
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

class HrLeave(models.Model):
    _inherit = 'hr.leave'
    
    
    request_hour_to = fields.Selection([
        ('0', '12:00 AM'), ('0.5', '0:30 AM'),
        ('1', '1:00 AM'), ('1.5', '1:30 AM'),
        ('2', '2:00 AM'), ('2.5', '2:30 AM'),
        ('3', '3:00 AM'), ('3.5', '3:30 AM'),
        ('4', '4:00 AM'), ('4.5', '4:30 AM'),
        ('5', '5:00 AM'), ('5.5', '5:30 AM'),
        ('6', '6:00 AM'), ('6.5', '6:30 AM'),
        ('7', '7:00 AM'), ('7.5', '7:30 AM'),
        ('8', '8:00 AM'), ('8.5', '8:30 AM'),
        ('9', '9:00 AM'), ('9.5', '9:30 AM'),
        ('10', '10:00 AM'), ('10.5', '10:30 AM'),
        ('11', '11:00 AM'), ('11.5', '11:30 AM'),
        ('12', '12:00 PM'), ('12.5', '0:30 PM'),
        ('13', '1:00 PM'), ('13.5', '1:30 PM'),
        ('14', '2:00 PM'), ('14.5', '2:30 PM'),
        ('15', '3:00 PM'), ('15.5', '3:30 PM'),
        ('16', '4:00 PM'), ('16.5', '4:30 PM'),
        ('17', '5:00 PM'), ('17.5', '5:30 PM'),
        ('18', '6:00 PM'), ('18.5', '6:30 PM'),
        ('19', '7:00 PM'), ('19.5', '7:30 PM'),
        ('20', '8:00 PM'), ('20.5', '8:30 PM'),
        ('21', '9:00 PM'), ('21.5', '9:30 PM'),
        ('22', '10:00 PM'), ('22.5', '10:30 PM'),
        ('23', '11:00 PM'), ('23.5', '11:30 PM'),
        ('23.99', '11:59 PM')], string='Hour to')

               
class ResourceCalendar(models.Model): 
    _inherit = 'resource.calendar'
    
    is_security = fields.Boolean(string=u'Харуулын ээлж эсэх')
    is_shift = fields.Boolean(string=u'Ээлжийн хуваарь эсэх')
    
class UrChadvar(models.Model):
    _name = 'ur.chadvar'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string=u'Компани')
    name = fields.Char(string='Ur chadvar')
    

class HrZeregRegister(models.Model):
    _name = 'hr.zereg.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    name = fields.Char(string=u'Зэрэг')
    per = fields.Float(string=u'Хувь')
    company_id = fields.Many2one('res.company',string=u'Компани')
    
    
class FoodMoney(models.Model):
    _name = 'food.money.register'
    _inherit = ['mail.thread']
    
    code = fields.Char(string=u'Код')
    name = fields.Char(string=u'Нэр')
    amount = fields.Integer(string=u'Дүн')
    additional = fields.Integer(string=u'Нэмэгдэл')
    

    
    
class HrDevRegister(models.Model):
    _name = 'hr.dev.register'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    name = fields.Char(string=u'Дэв')
    per = fields.Float(string=u'Хувь')
    company_id = fields.Many2one('res.company',string=u'Компани')
    

class ZeregDevMatrits(models.Model):
    _name = 'zereg.dev.matrits'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    
    company_id = fields.Many2one('res.company',string=u'Компани')
    name = fields.Char(string='Name')
    zereg_id = fields.Many2one('hr.zereg.register',string='Zereg')
    dev_id = fields.Many2one('hr.dev.register',string='Dev')
    amount = fields.Integer(string='Amount')
    
    


class hr_contract(models.Model):
    _inherit = 'hr.contract'
    
    phone_add = fields.Float('Phone add')
    other_add = fields.Float(string='Other add')
    zereg_id  = fields.Many2one('hr.zereg.register',string=u'Зэрэг/Дэв')
    dev_id = fields.Many2one('hr.dev.register',string=u'Дэв')
    ur_chadvar_id = fields.Many2one('ur.chadvar',string=u'Ур чадвар')
    ur_chadvar_per = fields.Integer(string=u'Ур чадвар хувь')
    shuud_udirdlaga = fields.Boolean(string=u'Шууд удирдлага')
    hudulmur_chadvar_aldsan = fields.Boolean(string=u'Хөдөлмөрийн чадвараа түр алдсан эсэх')
    jiremsnii_amralt = fields.Boolean(string=u'Жирэмсний амралттай эсэх')
    huuhed_asrah_chuluu = fields.Boolean(string=u'Хүүхэд асрах чөлөөтэй эсэх')



class hr_payslip(models.Model):
    
    _inherit = "hr.payslip"
    
    is_return = fields.Boolean('Is return',default=False)
    
    
    
    def refund_sheet(self):
        for payslip in self:
            if  payslip.credit_note == True:
                raise UserError(_(u'Буцаалт хийгдсэн тул дахин буцаалт хийхгүй.'))
            copied_payslip = payslip.copy({'credit_note': True, 'name': _('Refund: ') + payslip.name})
            copied_payslip.compute_sheet()
            copied_payslip.action_payslip_done()
            payslip.write({'is_return':True})
        formview_ref = self.env.ref('hr_payroll_community.view_hr_payslip_form', False)
        treeview_ref = self.env.ref('hr_payroll_community.view_hr_payslip_tree', False)
        
        return {
            'name': ("Refund Payslip"),
            'view_mode': 'tree, form',
            'view_id': False,
            'res_model': 'hr.payslip',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': "[('id', 'in', %s)]" % copied_payslip.ids,
            'views': [(treeview_ref and treeview_ref.id or False, 'tree'), (formview_ref and formview_ref.id or False, 'form')],
            'context': {}
        }
        


    