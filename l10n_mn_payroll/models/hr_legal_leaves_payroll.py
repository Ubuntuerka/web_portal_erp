from odoo import api, fields, models
from odoo import tools, _

from datetime import date
from datetime import timedelta
from datetime import datetime
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class HrPayrollLegalLeaves(models.Model):
    _name = 'hr.payroll.legal.leaves'

    now_date = fields.Date(u'Бүртгэсэн огноо', default=fields.Date.context_today)
    start_date = fields.Date(u'Эхлэх огноо', required=True, default=fields.Date.context_today)
    end_date = fields.Date(u'Дуусах огноо', required=True, default=fields.Date.context_today)
    dep_id = fields.Many2one('hr.department', u'Хэлтэс', )
    company_id = fields.Many2one('res.company', u'Компани', required=True,default=lambda self: self.env['res.company']._company_default_get('hr.payroll.legal.leaves'))
    state = fields.Selection([('draft', u'Ноорог'),
                              ('confirm', u'Баталсан')], string='State', default='draft')
    create_user = fields.Many2one('res.users', u'Бүртгэсэн ажилтан',default=lambda self: self.env.user)
    line_id = fields.One2many('hr.payroll.legal.leaves.line', 'legal_leaves_id', 'Hr payroll legal leaves line')

    def workdaysub(uid, start_date, end_date):
        '''
        Calculate the number of working days between two dates inclusive
        (start_date <= end_date).

        The actual working days can be set with the optional whichdays
        parameter
        (default is MON-FRI)
        '''
        (MON, TUE, WED, THU, FRI, SAT, SUN) = range(7)
        whichdays = (MON, TUE, WED, THU, FRI)
        delta_days = (end_date - start_date).days + 1
        full_weeks, extra_days = divmod(delta_days, 7)
        num_workdays = (full_weeks + 1) * len(whichdays)
        for d in range(1, 8 - extra_days):
            if (end_date + timedelta(d)).weekday() in whichdays:
                num_workdays -= 1
        return num_workdays

    @api.depends('start_date', 'end_date')
    def _onchange_work_days(self):
        for line in self:
            days = 0.0
            days = self.workdaysub(line.start_date,line.end_date)
            line.work_days = days

    def compute_salary_per_day(self):

        hr_emp = self.env['hr.employee']
        payslip_obj = self.env['hr.payslip']
        hr_leave_obj = self.env['hr.leave']
        leave_line_obj = self.env['hr.payroll.legal.leaves.line']
        obj = self.browse(self.id)
        if obj.dep_id.id != False:
            hr_emp_id = hr_emp.search([('company_id', '=', obj.company_id.id), ('department_id', '=', obj.dep_id.id)])
        else:
            hr_emp_id = hr_emp.search([('company_id', '=', obj.company_id.id)])

        if hr_emp_id:

            for emp in hr_emp_id:
                total_salary = 0.0
                total_work_day = 0.0
                give_one_day = 0.0
                total_mounth = 0.0
                leaves_amount_payroll = 0.0
                legal_leaves_day = 0.0
                payroll_ids = payslip_obj.search([('employee_id','=',emp.id),('date_from','>=',self.start_date),('date_to','<=',self.end_date)])
                if payroll_ids:
                    total_mounth +=1
                    for pr in payroll_ids.line_ids:
                        if pr.code=='DCCM':
                            total_work_day +=pr.amount
                        if pr.code =='T':
                            total_salary +=pr.amount
                days_ids = hr_leave_obj.search([('employee_id','=',emp.id),('request_date_from','>=',self.start_date),('request_date_to','<=',self.end_date),('state','=','validate')])
                if days_ids:
                    for days in days_ids:
                        if days.holiday_status_id.new_type=='legal_leave':
                            legal_leaves_day +=days.number_of_days
                if total_work_day > 0.0:
                    give_one_day = total_salary/total_work_day
                    
                if legal_leaves_day > 0.0:
                    leaves_amount_payroll = give_one_day*legal_leaves_day
                    
                
                leave_ids = leave_line_obj.search([('name','=',emp.id),('legal_leaves_id','=',self.id)])
                if not leave_ids:
                    leave_line_obj.create({
                                            'name':emp.id,
                                            'amount_payroll':total_salary,
                                            'work_days':total_work_day,
                                            'give_one_day':give_one_day,
                                            'legal_leaves_id':self.id,
                                            'total_mounth':total_mounth,
                                            'legal_leaves_day':legal_leaves_day,
                                            'leaves_amount_payroll':leaves_amount_payroll
                                            })
                else:
                    leave_ids.write({
                                            'amount_payroll':total_salary,
                                            'work_days':total_work_day,
                                            'give_one_day':give_one_day,
                                            'total_mounth':total_mounth,
                                            'legal_leaves_day':legal_leaves_day,
                                            'leaves_amount_payroll':leaves_amount_payroll
                                            })

    def action_confirm(self):
        for line in self.line_id:
            if line.total_mounth < 0.0:
                raise UserError(_(u'%s-ын сүүлийн 6 сарын цалин бодогдоогүй байна.')%line.name.name)
        self.write({'state':'confirm'})

    def action_cancel(self):
        hr_emp = self.env['hr.employee']
        payslip_obj = self.env['hr.payslip']
        hr_leave_obj = self.env['hr.leave']
        leave_line_obj = self.env['hr.payroll.legal.leaves.line']
        obj = self.browse(self.id)
        self.write({'state': 'draft'})
        return True

class HrPayrollLegalLeavesLine(models.Model):
    _name = 'hr.payroll.legal.leaves.line'

    name = fields.Many2one('hr.employee', 'Ажилтан', required=True)
    amount_payroll = fields.Float(u'Нийт цалин')
    total_mounth = fields.Float(u'Нийт ажилласан сар')
    work_days = fields.Float(string='Ажилласан өдөр')
    give_one_day = fields.Float(u'Өдөрт олгох цалин')
    legal_leaves_day = fields.Float(string='Ээлжийн амралтын хоног')
    leaves_amount_payroll = fields.Float(string='Ээлжийн амралтын нийт цалин')
    legal_leaves_id = fields.Many2one('hr.payroll.legal.leaves', u'Ээлжийн амралт/цалин/')


