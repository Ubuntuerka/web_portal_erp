# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import logging

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo import tools, _

from collections import namedtuple

_logger = logging.getLogger(__name__)

from datetime import date
from datetime import timedelta
from datetime import datetime
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

from odoo.addons.resource.models.resource import float_to_time, HOURS_PER_DAY
from pytz import timezone, UTC
import math

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"

DummyAttendance = namedtuple('DummyAttendance', 'hour_from, hour_to, dayofweek, day_period, week_type')


class HolidaysRequest(models.Model):
    _inherit = "hr.leave"
    
    
    @api.model_create_multi
    def create(self, vals_list):
        """ Override to avoid automatic logging of creation """
        if not self._context.get('leave_fast_create'):
            leave_types = self.env['hr.leave.type'].browse([values.get('holiday_status_id') for values in vals_list if values.get('holiday_status_id')])
            mapped_validation_type = {leave_type.id: leave_type.validation_type for leave_type in leave_types}

            for values in vals_list:
                employee_id = values.get('employee_id', False)
                leave_type_id = values.get('holiday_status_id')
                # Handle automatic department_id
                if not values.get('department_id'):
                    values.update({'department_id': self.env['hr.employee'].browse(employee_id).department_id.id})

                # Handle no_validation
                if mapped_validation_type[leave_type_id] == 'no_validation':
                    values.update({'state': 'confirm'})

                # Handle double validation
                if mapped_validation_type[leave_type_id] == 'both':
                    self._check_double_validation_rules(employee_id, values.get('state', False))

        holidays = super(HolidaysRequest, self.with_context(mail_create_nosubscribe=True)).create(vals_list)

        for holiday in holidays:
            if self._context.get('import_file'):
                holiday._onchange_leave_dates()
            if not self._context.get('leave_fast_create'):
                # FIXME remove these, as they should not be needed
                if employee_id:
                    holiday.with_user(SUPERUSER_ID)._sync_employee_details()
                if 'number_of_days' not in values and ('date_from' in values or 'date_to' in values):
                    holiday.with_user(SUPERUSER_ID)._onchange_leave_dates()

                # Everything that is done here must be done using sudo because we might
                # have different create and write rights
                # eg : holidays_user can create a leave request with validation_type = 'manager' for someone else
                # but they can only write on it if they are leave_manager_id
                holiday_sudo = holiday.sudo()
                holiday_sudo.add_follower(employee_id)
                if holiday.holiday_status_id.new_type!='work':
                    if holiday.validation_type == 'manager':
                        holiday_sudo.message_subscribe(partner_ids=holiday.employee_id.leave_manager_id.partner_id.ids)
                    if holiday.holiday_status_id.validation_type == 'no_validation':
                        # Automatic validation should be done in sudo, because user might not have the rights to do it by himself
                        holiday_sudo.action_validate()
                        holiday_sudo.message_subscribe(partner_ids=[holiday_sudo._get_responsible_for_approval().partner_id.id])
                        holiday_sudo.message_post(body=_("The time off has been automatically approved"), subtype="mt_comment") # Message from OdooBot (sudo)
                    elif not self._context.get('import_file'):
                        holiday_sudo.activity_update()
        return holidays
    
    
    def action_approve(self):
        # if validation_type == 'both': this method is the first approval approval
        # if validation_type != 'both': this method calls action_validate() below
        if any(holiday.state != 'confirm' for holiday in self):
            raise UserError(_('Time off request must be confirmed ("To Approve") in order to approve it.'))

        current_employee = self.env.user.employee_id
        self.filtered(lambda hol: hol.validation_type == 'both').write({'state': 'validate1', 'first_approver_id': current_employee.id})


        # Post a second message, more verbose than the tracking message
        #for holiday in self.filtered(lambda holiday: holiday.employee_id.user_id):
        #    holiday.message_post(
        #       body=_('Your %s planned on %s has been accepted') % (holiday.holiday_status_id.display_name, holiday.date_from),
        #        partner_ids=holiday.employee_id.user_id.partner_id.ids)

        self.filtered(lambda hol: not hol.validation_type == 'both').action_validate()
        if not self.env.context.get('leave_fast_create'):
            self.activity_update()
        return True
    
    
    def name_get(self):
        res = []
        for leave in self:
            if self.env.context.get('short_name'):
                if leave.leave_type_request_unit == 'hour' and leave.holiday_status_id.new_type == 'work':
                    
                    new_date_from = datetime.strptime(str(leave.date_from), DATETIME_FORMAT) + timedelta(hours=8)
                    new_date_to = datetime.strptime(str(leave.date_to), DATETIME_FORMAT) + timedelta(hours=8)
                    res.append(
                        (leave.id,
                        _("Орсон: %s - Гарсан: %s") %
                        (str(new_date_from)[10:], str(new_date_to)[10:]))
                    )
                else:
                    new_date_from = datetime.strptime(str(leave.date_from), DATETIME_FORMAT) + timedelta(hours=8)
                    new_date_to = datetime.strptime(str(leave.date_to), DATETIME_FORMAT) + timedelta(hours=8)
                    if leave.holiday_status_id.new_type != 'work':
                        ch_type = ''
                        if leave.holiday_status_id.new_type == 'legal_leave':
                            ch_type = 'Ээлжийн амралт'
                        elif leave.holiday_status_id.new_type == 'sick':
                            ch_type = 'Өвчтэй чөлөө'
                        elif leave.holiday_status_id.new_type == 'out_work':
                            ch_type = 'Гадуур ажил'
                        elif leave.holiday_status_id.new_type == 'no_finger':
                            ch_type = 'Хуруу бүртгүүлээгүй'
                        elif leave.holiday_status_id.new_type == 'payroll_half':
                            ch_type = 'Цалинтай чөлөө (50%)'
                        elif leave.holiday_status_id.new_type == 'unpayroll':
                            ch_type = 'Цалингүй чөлөө'
                        elif leave.holiday_status_id.new_type == 'payroll':
                            ch_type = 'Цалинтай чөлөө'
                        elif leave.holiday_status_id.new_type == 'more_time':
                            ch_type = 'Илүү цаг'
                        elif leave.holiday_status_id.new_type == 'public_holiday':
                            ch_type = 'Тэмдэглэлт баярын өдөр'
                        elif leave.holiday_status_id.new_type == 'assignment':
                            ch_type = 'Томилолт'
                        elif leave.holiday_status_id.new_type == 'training':
                            ch_type = 'Сургалт'
                        elif leave.holiday_status_id.new_type == 'family_day':
                            ch_type = 'Гэр бүлйин өдөр'
                        elif leave.holiday_status_id.new_type == 'more_times2':
                            ch_type = 'Шөнийн илүү цаг'
                        elif leave.holiday_status_id.new_type == 'patch_day':
                            ch_type = 'Нөхөж амралт'
                        elif leave.holiday_status_id.new_type == 'sul_zogsolt':
                            ch_type = 'Сул зогсолт'
                        elif leave.holiday_status_id.new_type == 'online':
                            ch_type = 'Гэрээс ажилласан'
                        res.append(
                            (leave.id,
                            _("%s %s %s") %
                            (str(ch_type),str(new_date_from)[10:], str(new_date_to)[10:]))
                        )
                    else:
                        res.append(
                        (leave.id,
                        _("Орсон: %s - Гарсан: %s") %
                        (str(new_date_from)[10:], str(new_date_to)[10:]))
                    )
            else:
                if leave.holiday_type == 'company':
                    target = leave.mode_company_id.name
                elif leave.holiday_type == 'department':
                    target = leave.department_id.name
                elif leave.holiday_type == 'category':
                    target = leave.category_id.name
                else:
                    target = leave.employee_id.name
                if leave.leave_type_request_unit == 'hour' and leave.holiday_status_id.new_type == 'work': 
                    new_date_from = datetime.strptime(str(leave.date_from), DATETIME_FORMAT) + timedelta(hours=8)
                    new_date_to = datetime.strptime(str(leave.date_to), DATETIME_FORMAT) + timedelta(hours=8)
                    res.append(
                        (leave.id,
                        _("Орсон: %s - Гарсан: %s") %
                        (str(new_date_from)[10:], str(new_date_to)[10:]))
                    )
                else:
                    ch_type = ''
                    if leave.holiday_status_id.new_type == 'legal_leave':
                        ch_type = 'Ээлжийн амралт'
                    elif leave.holiday_status_id.new_type == 'sick':
                        ch_type = 'Өвчтэй чөлөө'
                    elif leave.holiday_status_id.new_type == 'out_work':
                        ch_type = 'Гадуур ажил'
                    elif leave.holiday_status_id.new_type == 'no_finger':
                        ch_type = 'Хуруу бүртгүүлээгүй'
                    elif leave.holiday_status_id.new_type == 'payroll_half':
                        ch_type = 'Цалинтай чөлөө (50%)'
                    elif leave.holiday_status_id.new_type == 'unpayroll':
                        ch_type = 'Цалингүй чөлөө'
                    elif leave.holiday_status_id.new_type == 'payroll':
                        ch_type = 'Цалинтай чөлөө'
                    elif leave.holiday_status_id.new_type == 'more_time':
                        ch_type = 'Илүү цаг'
                    elif leave.holiday_status_id.new_type == 'public_holiday':
                        ch_type = 'Тэмдэглэлт баярын өдөр'
                    elif leave.holiday_status_id.new_type == 'assignment':
                        ch_type = 'Томилолт'
                    elif leave.holiday_status_id.new_type == 'training':
                        ch_type = 'Сургалт'
                    elif leave.holiday_status_id.new_type == 'family_day':
                        ch_type = 'Гэр бүлйин өдөр'
                    elif leave.holiday_status_id.new_type == 'more_times2':
                        ch_type = 'Шөнийн илүү цаг'
                    elif leave.holiday_status_id.new_type == 'patch_day':
                        ch_type = 'Нөхөж амралт'
                    elif leave.holiday_status_id.new_type == 'sul_zogsolt':
                        ch_type = 'Сул зогсолт'
                    elif leave.holiday_status_id.new_type == 'online':
                        ch_type = 'Гэрээс ажилласан'
                    res.append(
                        (leave.id,
                        _("Чөлөөний төрөл: %s") %
                        (str(ch_type)))
                    )
        return res

    
    


#===============================================================================
#     @api.onchange('request_date_from_period', 'request_hour_from', 'request_hour_to',
#                   'request_date_from', 'request_date_to',
#                   'employee_id')
#     def _onchange_request_parameters(self):
#         if not self.request_date_from:
#             self.date_from = False
#             return
# 
#         if self.request_unit_half or self.request_unit_hours:
#             self.request_date_to = self.request_date_from
# 
#         if not self.request_date_to:
#             self.date_to = False
#             return
# 
#         resource_calendar_id = self.employee_id.resource_calendar_id or self.env.company.resource_calendar_id
#         domain = [('calendar_id', '=', resource_calendar_id.id), ('display_type', '=', False)]
#         attendances = self.env['resource.calendar.attendance'].read_group(domain, ['ids:array_agg(id)', 'hour_from:min(hour_from)', 'hour_to:max(hour_to)', 'week_type', 'dayofweek', 'day_period'], ['week_type', 'dayofweek', 'day_period'], lazy=False)
# 
#         # Must be sorted by dayofweek ASC and day_period DESC
#         attendances = sorted([DummyAttendance(group['hour_from'], group['hour_to'], group['dayofweek'], group['day_period'], group['week_type']) for group in attendances], key=lambda att: (att.dayofweek, att.day_period != 'morning'))
# 
#         default_value = DummyAttendance(0, 0, 0, 'morning', False)
# 
#         if resource_calendar_id.two_weeks_calendar:
#             # find week type of start_date
#             start_week_type = int(math.floor((self.request_date_from.toordinal() - 1) / 7) % 2)
#             attendance_actual_week = [att for att in attendances if att.week_type is False or int(att.week_type) == start_week_type]
#             attendance_actual_next_week = [att for att in attendances if att.week_type is False or int(att.week_type) != start_week_type]
#             # First, add days of actual week coming after date_from
#             attendance_filtred = [att for att in attendance_actual_week if int(att.dayofweek) >= self.request_date_from.weekday()]
#             # Second, add days of the other type of week
#             attendance_filtred += list(attendance_actual_next_week)
#             # Third, add days of actual week (to consider days that we have remove first because they coming before date_from)
#             attendance_filtred += list(attendance_actual_week)
# 
#             end_week_type = int(math.floor((self.request_date_to.toordinal() - 1) / 7) % 2)
#             attendance_actual_week = [att for att in attendances if att.week_type is False or int(att.week_type) == end_week_type]
#             attendance_actual_next_week = [att for att in attendances if att.week_type is False or int(att.week_type) != end_week_type]
#             attendance_filtred_reversed = list(reversed([att for att in attendance_actual_week if int(att.dayofweek) <= self.request_date_to.weekday()]))
#             attendance_filtred_reversed += list(reversed(attendance_actual_next_week))
#             attendance_filtred_reversed += list(reversed(attendance_actual_week))
# 
#             # find first attendance coming after first_day
#             attendance_from = attendance_filtred[0]
#             # find last attendance coming before last_day
#             attendance_to = attendance_filtred_reversed[0]
#         else:
#             # find first attendance coming after first_day
#             attendance_from = next((att for att in attendances if int(att.dayofweek) >= self.request_date_from.weekday()), attendances[0] if attendances else default_value)
#             # find last attendance coming before last_day
#             attendance_to = next((att for att in reversed(attendances) if int(att.dayofweek) <= self.request_date_to.weekday()), attendances[-1] if attendances else default_value)
# 
#         compensated_request_date_from = self.request_date_from
#         compensated_request_date_to = self.request_date_to
# 
#         if self.request_unit_half:
#             if self.request_date_from_period == 'am':
#                 hour_from = float_to_time(attendance_from.hour_from)
#                 hour_to = float_to_time(attendance_from.hour_to)
#             else:
#                 hour_from = float_to_time(attendance_to.hour_from)
#                 hour_to = float_to_time(attendance_to.hour_to)
#         elif self.request_unit_hours:
#             hour_from = float_to_time(float(self.request_hour_from))
#             hour_to = float_to_time(float(self.request_hour_to))
#         elif self.request_unit_custom:
#             hour_from = self.date_from.time()
#             hour_to = self.date_to.time()
#             compensated_request_date_from = self._adjust_date_based_on_tz(self.request_date_from, hour_from)    
#             compensated_request_date_to = self._adjust_date_based_on_tz(self.request_date_to, hour_to)  
#         else:
#             hour_from = float_to_time(attendance_from.hour_from)
#             hour_to = float_to_time(attendance_to.hour_to)
# 
#         date_from = timezone(self.tz).localize(datetime.combine(compensated_request_date_from, hour_from)).astimezone(UTC).replace(tzinfo=None)
#         date_to = timezone(self.tz).localize(datetime.combine(compensated_request_date_to, hour_to)).astimezone(UTC).replace(tzinfo=None)
#         #TS talaas haasan kod
#         #self.update({'date_from': date_from, 'date_to': date_to})
#         self._onchange_leave_dates()
#===============================================================================

    @api.depends('date_from')
    def _compute_date_from(self):
        for holiday in self:
            added_8hours = datetime.strptime(str(holiday.date_from), DATETIME_FORMAT) + timedelta(hours=8)
            self.request_date_from = added_8hours.date()

    @api.depends('date_to')
    def _compute_date_to(self):
        for holiday in self:
            added_8hours = datetime.strptime(str(holiday.date_to), DATETIME_FORMAT) + timedelta(hours=8)
            self.request_date_to = added_8hours.date()
    
    request_date_from = fields.Date('Request Start Date', compute='_compute_date_from')
    request_date_to = fields.Date('Request End Date', compute='_compute_date_to')

class HrLeaveType(models.Model):
    _inherit = "hr.leave.type"
        
    new_type = fields.Selection([
                        ('legal_leave', 'Ээлжийн амралт'),
                        ('sick', 'Өвчний чөлөө'),
                        ('out_work', 'Гадуур ажил'),
                        ('no_finger', 'Хуруу бүртгүүлээгүй'),
                        ('payroll_half', 'Цалинтай чөлөө (50%)'),
                        ('payroll', 'Цалинтай чөлөө'),
                        ('unpayroll', 'Цалингүй чөлөө'),
                        ('more_time', u'Илүү цаг'),
                        ('public_holiday', 'Тэмдэглэлт баярын өдөр'),
                        ('work','Ажлын цаг'),
                        ('assignment','Томилолт'),
                        ('training','Сургалт'),
                        ('family_day','Гэр бүлийн өдөр'),
                        ('more_times2', 'Шөнийн илүү цаг'),
                        ('patch_day','Нөхөж амралт'),
                        ('sul_zogsolt','Сул зогсолт'),
                        ('online', 'Гэрээс ажилласан'),
                    ], string='Төрөл', required=True)
    
class Mochlog(models.Model):
    _name = 'hour.mochlog.register'
    
    name = fields.Char(string=u'Нэр')
    start_date = fields.Date(string=u'Эхлэх огноо')
    end_date = fields.Date(string=u'Дуусах огноо')
    is_active = fields.Boolean(string=u'Идэвхтэй')


class HrHourRecord(models.Model):
    """ АЖИЛЧДЫН ЦАГИЙН БҮРТГЭЛ """
    _name = 'hr.hour.record'
    _description = "Employee hour record"
    
    department = fields.Many2one('hr.department',string='Department')
    name = fields.Char(string='Working time register')
    company_id = fields.Many2one('res.company', string='Company', required=True,default=lambda self: self.env['res.company']._company_default_get('hr.hour.record') )
    m_hour_id = fields.Many2one('hour.mochlog.register',string=u'Цаг бүртгэлийн мөчлөг')
    start_date = fields.Date(string='Start date',required=True, default=fields.Date.context_today)
    end_date = fields.Date(string='End date',required=True, default=fields.Date.context_today)
    work_day = fields.Float(string="Work day")
    holiday_day = fields.Float(string='Holiday day')
    is_compute = fields.Boolean(string=u'Тооцоолсон эсэх')
    work_days = fields.Float(string='Work days',compute='_onchange_work_days')
    line_id = fields.One2many('hr.hour.record.line', 'parentd_id', string='Hour record')
    state = fields.Selection([
                        ('draft', 'Draft'),
                        ('confirmed', 'Confirmed'),
                    ], string='States', default='draft',track_visibility='onchange',readonly=True, help="Help...", select=True )
    @api.onchange('m_hour_id')
    def onchange_date(self):
        res = {}
        list = []
        if self.m_hour_id:
            res['start_date'] = self.m_hour_id.start_date
            res['end_date'] = self.m_hour_id.end_date
            
        return {'value':res}
    def print_hour_record(self):
        # Хэвлэх үйлдлийг энд гүйцэтгэнэ (PDF үүсгэх гэх мэт)
        return self.env.ref('l10n_mn_payroll.report_hr_hour_record').report_action(self)
    def unlink(self):
        for hour in self:
            if hour.state!='draft':
                raise UserError(_("You cannot delete an entry which has been posted once."))
            hour.line_id.unlink()
        return super(HrHourRecord, self).unlink()
    
    @api.depends('start_date', 'end_date')
    def _onchange_work_days(self):
        for line in self:
            days = 0.0
            days = self.workdaysub(line.start_date,line.end_date)
            line.work_days = days
    
    def hour_record(self):
        hr_obj = self.env['hr.hour.record']
        hr_data = self.browse(self.id)
        emp_info = self.env['hr.hour.record.line']
        emp_obj = self.env['hr.employee']
        att_obj = self.env['hr.leave']
        contract_obj = self.env['hr.contract']
        decree_obj = self.env['hr.decree']
        holiday_obj = self.env['hr.leave']
        type_obj = self.env['hr.leave.type']
        mochlog_hour_obj = self.env['hour.mochlog.register']
        overtime_obj = self.env['tbab.overtime']
        mach_obj = self.env['attendance.mssql.connect']
        mach_id = mach_obj.search([('company_id','=',hr_data.company_id.id)])
        cel_obj = self.env['celebration.day']
        
        cel_id = cel_obj.search( [('start_date','>=',self.start_date),('start_date','<=',self.end_date)])
        if len(cel_id)>0:
            self.write ({'holiday_day': len(cel_id)})
        
        if mach_id !=[]:
            mach = mach_obj.browse(mach_id.id)
        else:
            raise UserError(_(u"Хүний нөөцийн төхөөрөмж дээр тохиргоо хийнэ үү"))
        self.delete_lines()
        emp_id = []
        if self.department.id != False:
            emp_id = emp_obj.search( [('company_id','=',hr_data.company_id.id),('department_id','=',self.department.id),('emp_status','=','emp_active')], order='department_id')
        else:
            emp_id = emp_obj.search([('company_id','=',hr_data.company_id.id),('emp_status','=','emp_active')], order='department_id')
            
        
        server_time_start = datetime.strptime(str(hr_data.start_date),DATE_FORMAT) - timedelta(hours=8)
        server_time_end = datetime.strptime(str(hr_data.end_date),DATE_FORMAT).replace(hour=23,minute=59,second=59)
        start_date_month_converted = datetime.strptime(str(hr_data.start_date),DATE_FORMAT) + timedelta(hours=8)
        start_date_month = start_date_month_converted.replace(hour=0,minute=0,second=0)
        end_date_month = datetime.strptime(str(hr_data.end_date),DATE_FORMAT).replace(hour=23,minute=59,second=59)
        days = []
        idate = datetime.strptime(str(hr_data.start_date),DATE_FORMAT)
        while idate <= datetime.strptime(str(hr_data.end_date),DATE_FORMAT):
            days.append(idate)
            idate += timedelta(days = 1)
            
        #print ('DDDDDDDDDDDDd',days,emp_id)
        for line in emp_id:
            f_time = 0.0
            f_ntime = 0.0
            f_fuel_sale = 0.0
            f_gas = 0.0
            f_qty = 0.0
            f_acc_qty = 0.0
            night_time = 0.0
            be_worked_hours = 0.0
            be_worked_hours2 = 0.0
            be_worked_days2 = 0.0
            dayofhours = 0.0
            hours_per_day2 = 0.0
            days_per_month = 0.0
            worked_days = 0.0
            worked_days_total = 0.0
            hm_days_total = 0.0
            hm_days = 0.0
            worked_hours = 0.0
            worked_hours_total = 0.0
            
            this_day_sick_hours = 0.0
            this_month_sick_hours_total = 0.0
            this_month_all_sick_hours_total = 0.0
            this_day_sick_hours_total = 0.0
            
            this_day_legal_hours = 0.0
            this_month_legal_hours_total = 0.0
            
            
            hotsorson_tsag_ogloo_sariinh = 0.0
            hotsorson_tsag_oroi_sariinh = 0.0
            
            hotsorson_tsag_niit_sariinh = 0.0
            hotsorson_tsag_niit_sariinh_ogloo = 0.0
            
            this_day_payroll_hours = 0.0
            this_month_payroll_hours_total = 0.0
            this_month_all_payroll_hours_total = 0.0
            
            this_day_no_finger_hours = 0.0
            this_month_no_finger_hours_total = 0.0
            this_month_all_no_finger_hours_total = 0.0
            
            
            this_day_unpayroll_hours = 0.0
            this_month_unpayroll_hours_total = 0.0
            this_month_all_unpayroll_hours_total = 0.0
            
            this_day_assignment_hours = 0.0
            this_month_assignment_hours_total = 0.0
            this_month_all_assignment_hours_total = 0.0
            
            this_day_outwork_hours = 0.0
            this_month_outwork_hours_total = 0.0
            this_month_all_outwork_hours_total = 0.0
            
            this_day_patch_day_hours = 0.0
            this_month_patch_day_hours_total = 0.0
            this_month_all_patch_day_hours_total = 0.0
            
            this_day_sul_zogsolt_hours = 0.0
            this_month_sul_zogsolt_hours_total = 0.0
            this_month_all_sul_zogsolt_hours_total = 0.0
            
            this_day_online_hours = 0.0
            this_month_online_hours_total = 0.0
            this_month_all_online_hours_total = 0.0
            
            this_day_training_hours = 0.0
            this_month_training_hours_total = 0.0
            this_month_all_training_hours_total = 0.0
            
            
            this_day_family_day_hours = 0.0
            this_month_family_day_hours_total = 0.0
            this_month_all_family_day_hours_total = 0.0


            
            this_day_overtime = 0.0
            this_month_overtime = 0.0

            this_day_moretimes = 0.0
            this_month_moretimes = 0.0

            this_day_moretimes2 = 0.0
            this_month_moretimes2 = 0.0

            
            this_day_public = 0.0
            this_month_overtime = 0.0
            this_month_public_total = 0.0
            this_month_all_public_total = 0.0
            
            
            hotsorson_tsag_ogloo = 0.0
            more_time = 0.0
            due_time = 0.0
            worked_hours_total = 0.0
            #print ('PPPPPPPPP', line.contract_id, line.contract_id.state, line.resource_calendar_id, line.id)
           
            if line.contract_id and line.contract_id.state == 'open' and line.resource_calendar_id:
                    #ШТС-ын ажилтнууд
                    if line.resource_calendar_id.is_shift == True:
                        self._cr.execute(""" SELECT  sl.employee_id as emp, sum(sl.time) as udur, sum(sl.n_time) as shunu, sum(sl.fuel_sale) as shathuun, sum(sl.gas) as hii, sum(sl.qty) as tos, sum(sl.acc_qty) as acc
                                            FROM shift_working_register_line sl
                                            LEFT JOIN shift_working_register s on (s.id = sl.shift_id)
                                            WHERE
                                            sl.employee_id = %s and s.shift_date >= '%s' 
                                            and s.shift_date <= '%s'  
                                            group by sl.employee_id
                                    """%(line.id,self.start_date,self.end_date,))
                        f_time_data  = self._cr.dictfetchall()
                        
                     
                        
                        if f_time_data !=None:
                            for ftime in f_time_data:
                                f_time +=ftime['udur']
                                f_ntime +=ftime['shunu']
                            night_time = f_ntime
                            worked_hours_total = f_ntime + f_time
                            worked_days_total = worked_hours_total/12
                            fl = (worked_hours_total%12)
                            if fl > 0:
                                worked_days_total = worked_days_total + 1
                                 
                        for day in days:
                            if day.weekday() not in (5,6):
                                hours_per_day2 += line.resource_calendar_id.hours_per_day
                                days_per_month += 1
                                #print ('KKKKKKKKK',hours_per_day2)
                            #else:
                            #    hours_per_day2 = 0.0
                            #    days_per_month = 0.0
                        #Ajillah estoi honog oloh
                        be_worked_days2 = days_per_month 
                        #Ajillah estoi tsag oloh
                        be_worked_hours2 =  hours_per_day2
                    else:
                        #Хуруу дардаг ажилтнууд
                        if line.device_id:
                            for day in days:
                            #=======================================================
                            # for lineeeeee in line.resource_calendar_id.attendance_ids:
                            #     print ('WWWWW', lineeeeee.dayofweek, lineeeeee.day_period, lineeeeee.hour_from, lineeeeee.hour_to)
                            # print ('DDDDD',day.weekday())
                            #=======================================================
                                if day.weekday() not in (5,6):
                                    hours_per_day2 = line.resource_calendar_id.hours_per_day
                                    days_per_month = 1
                                #print ('KKKKKKKKK',hours_per_day2)
                                else:
                                    hours_per_day2 = 0.0
                                    days_per_month = 0.0
                                #Ajillah estoi honog oloh
                                be_worked_days2 = be_worked_days2 + days_per_month 
                                #Ajillah estoi tsag oloh
                                be_worked_hours2 = be_worked_hours2 + hours_per_day2
                            
                                #Tuhain odriin zaag ololt
                                signin_day_b = datetime.strptime(str(day), DATETIME_FORMAT)
                                signin_day = signin_day_b.replace(hour=0, minute=0, second=1) 
                                signout_day = signin_day_b.replace(hour=23, minute=59, second=59) 
                                signin_day = datetime.strptime(str(signin_day),DATETIME_FORMAT) - timedelta(hours=8)
                                signout_day = datetime.strptime(str(signout_day),DATETIME_FORMAT) - timedelta(hours=8)
                                # Чөлөө хэсгээс өдөр бүрийн даралт татах
                                att_type_id = type_obj.search( [('new_type','=','work')],limit=1)
                                if att_type_id:
                                    att_id_one_day = att_obj.search([('employee_id','=',line.id),('date_from','>=',signin_day),('date_to','<=',signout_day),('holiday_status_id','=',att_type_id.id)])
                            
                                hotsorson_tsag_ogloo1 = 0.0
                                hotsorson_tsag_oroi = 0.0
                                this_day_holiday_hours = 0.0
                                this_day_holiday_hours_total = 0.0
                                for att_id_one in att_id_one_day:
                                    if att_id_one.number_of_hours_display == 0.0 and att_id_one:
                                        
                                        
                                        att_id_one_day1 = att_obj.search([('employee_id','=',line.id),('date_from','>=',signin_day),('date_to','<=',signout_day),('holiday_status_id','!=',att_type_id.id),('state','in',('validate','validate1'))])
                                        for att_id_one_day in att_id_one_day1:
                                            
                                            if att_id_one_day and att_id_one_day.holiday_status_id.new_type != 'more_time':
                                                if att_id_one_day.number_of_hours_display < 8.0:
                                                   worked_hours_total +=4
                                                   hm_days += 1
                                            else:
                                                if not att_id_one_day:
                                                    hm_days += 1
                                                    worked_hours_total +=4
                                        
                                        
                                    if att_id_one.number_of_hours_display != 0.0 and day.weekday() not in (5,6):
                                        #worked_days += 1
                                        worked_days_total +=1
                                        
                                        converted_in = datetime.strptime(str(att_id_one.date_from), DATETIME_FORMAT) + timedelta(hours=8)
                                        converted_out = datetime.strptime(str(att_id_one.date_to), DATETIME_FORMAT).replace(second=0) + timedelta(hours=8)
                                        converted_diff = datetime.strptime(str(converted_out), DATETIME_FORMAT) - datetime.strptime(str(converted_in), DATETIME_FORMAT)
                                        worked_hours2= 0
                                        
                                        
                                        #Tasalsan tsag ololt
                                        ireh_estoi_tsag = 0.0
                                        ywah_estoi_tsag = 0.0
                                        
                                        
                                        cal_morning_hour_from = 0.0
                                        cal_morning_hour_to = 0.0
                                        cal_afternoon_hour_from = 0.0
                                        cal_afternoon_hour_to = 0.0
                                        cal_name1 = ''
                                        cal_name2 = ''
                                        for line_cal in line.resource_calendar_id.attendance_ids:
                                            if line_cal.dayofweek == str(day.weekday()) and line_cal.day_period == 'morning':
                                                cal_morning_hour_from = line_cal.hour_from
                                                cal_morning_hour_to = line_cal.hour_to
                                                cal_name1 = line_cal.name
                                            elif line_cal.dayofweek == str(day.weekday()) and line_cal.day_period == 'afternoon':
                                                cal_afternoon_hour_from = line_cal.hour_from
                                                cal_afternoon_hour_to = line_cal.hour_to
                                                cal_name2 = line_cal.name
                                             
                                        
                                        only_hour_morn = int(cal_morning_hour_from)
                                        minutes = cal_morning_hour_from*60
                                        hours, minutes = divmod(minutes, 60)
                                        only_minut_morn = int(minutes)
                                        #tsag bodohdoo ireh_estoi_tsag talbariig ashiglaj bga
                                        ireh_estoi_tsag = datetime.strptime(str(converted_in), DATETIME_FORMAT).replace(hour=only_hour_morn, minute=only_minut_morn,second=0)
                                        #print ('BBBBBB', ireh_estoi_tsag)
                                        #Serveriin tsag buyu Londongiin tsagaas shvvlt hiihdee ireh_estoi_tsag2 talbariig ashiglaj bga
                                        ireh_estoi_tsag2 = datetime.strptime(str(converted_in) , DATETIME_FORMAT).replace(hour=only_hour_morn, minute=only_minut_morn,second=0) - timedelta(hours=16)
                                        #print ('BBBBBB', ireh_estoi_tsag2)
                                        
                                        #converted_in2 = datetime.strptime(str(att_id_one_day.check_in), DATETIME_FORMAT).replace(second=0) + timedelta(hours=8) - timedelta(hours=8)
                                        
                                        #converted_in2 = datetime.strptime(str(att_id_one.date_from), DATETIME_FORMAT).replace(hour=23, minute=59,second=59) - timedelta(hours=8)
                                        
                                        #ireh_estoi_tsag = datetime.strptime(str(att_id_one_day.check_in), DATETIME_FORMAT).replace(hour=only_hour_morn, minute=only_minut_morn)
                                        hotsorson_tsag_ogloo12 = 0.0
                                        only_hour_aft = int(cal_afternoon_hour_to)
                                        minutes2 = cal_afternoon_hour_to*60
                                        hours2, minutes2 = divmod(minutes2, 60)
                                        only_minut_aft = int(minutes2)
                                        ywah_estoi_tsag = datetime.strptime(str(converted_out), DATETIME_FORMAT).replace(hour=only_hour_aft,minute=only_minut_aft,second=0)
                                       
                                        if converted_out < ywah_estoi_tsag:
                                            if round((ywah_estoi_tsag - converted_out).seconds / 3600, 2) > 5.0:
                                                hotsorson_tsag_oroi = round((ywah_estoi_tsag - converted_out).seconds / 3600, 2) - 1
                                            else:
                                                hotsorson_tsag_oroi = round((ywah_estoi_tsag - converted_out).seconds / 3600, 2)
                                            hotsorson_tsag_oroi_sariinh = hotsorson_tsag_oroi_sariinh + hotsorson_tsag_oroi
                                        
                                        #hotsrol tootsooloh
                                        if converted_in > ireh_estoi_tsag and day.weekday() not in (5,6):
                                            #print ('OOOOOOO', converted_in, ireh_estoi_tsag, converted_in2, ireh_estoi_tsag2)
                                            hotsorson_tsag_ogloo12 = round((converted_in - ireh_estoi_tsag).seconds / 3600, 2)
                                            if hotsorson_tsag_ogloo12 > 8.0:
                                                hotsorson_tsag_ogloo1 = 8.0
                                            else:
                                                hotsorson_tsag_ogloo1 = hotsorson_tsag_ogloo12
                                            
                                            #holiday_this_day = holiday_obj.search([('employee_id','=',line.id),('state','in',('validate','validate1')),('date_from','>=',ireh_estoi_tsag2),('date_from','<=',converted_in2)])
                                            
                                            
                                            ireh_estoi_tsag3 = datetime.strptime(str(ireh_estoi_tsag), DATETIME_FORMAT) - timedelta(hours=8)
                                            holiday_this_day = holiday_obj.search([('employee_id','=',line.id),('state','in',('validate','validate1')),('date_from','>=',ireh_estoi_tsag2),('date_from','<=',ireh_estoi_tsag3)])
                                            
                                            for lin255 in holiday_this_day:
                                                this_day_holiday_hours = lin255.number_of_hours_display
                                                this_day_holiday_hours_total = this_day_holiday_hours_total + this_day_holiday_hours
                                            
                                            if this_day_holiday_hours_total >= hotsorson_tsag_ogloo1:
                                                hotsorson_tsag_ogloo = 0.0
                                            elif this_day_holiday_hours_total < hotsorson_tsag_ogloo1:
                                                if ywah_estoi_tsag > converted_in:
                                                    hotsorson_tsag_ogloo = hotsorson_tsag_ogloo1 - this_day_holiday_hours_total
                                            #if line.id==968:
                                            #    print("ogloonii tsag hotsorson ",converted_in,hotsorson_tsag_ogloo_sariinh,hotsorson_tsag_ogloo,hotsorson_tsag_ogloo12,converted_out)
                                            hotsorson_tsag_ogloo_sariinh = hotsorson_tsag_ogloo_sariinh + hotsorson_tsag_ogloo
                                            
                                            #print ('VVVVVVVVVVVVVV', hotsorson_tsag_ogloo, hotsorson_tsag_ogloo_sariinh)
                                        
                                        #Оффисын ээлжийн ажилтнууд ШЗА    
                                        if line.resource_calendar_id.is_security == True:
                                            yavah_yostoi_tsag_shift = datetime.strptime(str(ywah_estoi_tsag), DATETIME_FORMAT)+timedelta(hours=16)
                                            if yavah_yostoi_tsag_shift >= converted_in >= ireh_estoi_tsag and ireh_estoi_tsag <= converted_out <= yavah_yostoi_tsag_shift and day.weekday() not in (5,6):
                                                real_diff = (datetime.strptime(str(converted_out), DATETIME_FORMAT) - datetime.strptime(str(converted_in), DATETIME_FORMAT)).total_seconds()/3600
                                                if real_diff > 5.0:
                                                    worked_hours2 = real_diff - 1
                                                else:
                                                    worked_hours2 = real_diff
                                            elif converted_in < ireh_estoi_tsag and converted_out > yavah_yostoi_tsag_shift and day.weekday() not in (5,6):
                                                real_diff = (datetime.strptime(str(yavah_yostoi_tsag_shift), DATETIME_FORMAT) - datetime.strptime(str(ireh_estoi_tsag), DATETIME_FORMAT)).total_seconds()/3600
                                                if real_diff > 5.0:
                                                    worked_hours2 = real_diff - 1
                                                else:
                                                    worked_hours2 = real_diff
                                            elif converted_in < ireh_estoi_tsag and ireh_estoi_tsag <= converted_out <= yavah_yostoi_tsag_shift and day.weekday() not in (5,6):
                                                real_diff = (datetime.strptime(str(converted_out), DATETIME_FORMAT) - datetime.strptime(str(ireh_estoi_tsag), DATETIME_FORMAT)).total_seconds()/3600
                                                if real_diff > 5.0:
                                                    worked_hours2 = real_diff - 1
                                                else:
                                                    worked_hours2 = real_diff
                                                
                                            elif yavah_yostoi_tsag_shift >= converted_in >= ireh_estoi_tsag and converted_out > yavah_yostoi_tsag_shift and day.weekday() not in (5,6):
                                                real_diff = (datetime.strptime(str(yavah_yostoi_tsag_shift), DATETIME_FORMAT) - datetime.strptime(str(converted_in), DATETIME_FORMAT)).total_seconds()/3600
                                                if real_diff > 5.0:
                                                    worked_hours2 = real_diff - 1
                                                else:
                                                    worked_hours2 = real_diff
                                                
                                            elif converted_out < ireh_estoi_tsag and converted_out < ireh_estoi_tsag and day.weekday() not in (5,6):
                                                worked_hours2 = 0.0
                                            elif converted_in > yavah_yostoi_tsag_shift and converted_in > yavah_yostoi_tsag_shift and day.weekday() not in (5,6):
                                                worked_hours2 = 0.0
                                            
                                            
                                            worked_hours = round((worked_hours2), 2)
                                           
                                            
                                        else:
                                            
                                            #tuhain odriin ajillasan tsag oloh
                                            diff_sec = ( datetime.strptime(str(att_id_one.date_to), DATETIME_FORMAT)-datetime.strptime(str(att_id_one.date_from), DATETIME_FORMAT))
            
                                            if ywah_estoi_tsag >= converted_in >= ireh_estoi_tsag and ireh_estoi_tsag <= converted_out <= ywah_estoi_tsag and day.weekday() not in (5,6):
                                                real_diff = (datetime.strptime(str(converted_out), DATETIME_FORMAT) - datetime.strptime(str(converted_in), DATETIME_FORMAT)).total_seconds()/3600
                                                if real_diff > 5.0:
                                                    worked_hours2 = real_diff - 1
                                                else:
                                                    worked_hours2 = real_diff
                                            elif converted_in < ireh_estoi_tsag and converted_out > ywah_estoi_tsag and day.weekday() not in (5,6):
                                                real_diff = (datetime.strptime(str(ywah_estoi_tsag), DATETIME_FORMAT) - datetime.strptime(str(ireh_estoi_tsag), DATETIME_FORMAT)).total_seconds()/3600
                                                if real_diff > 5.0:
                                                    worked_hours2 = real_diff - 1
                                                else:
                                                    worked_hours2 = real_diff
                                            elif converted_in < ireh_estoi_tsag and ireh_estoi_tsag <= converted_out <= ywah_estoi_tsag and day.weekday() not in (5,6):
                                                real_diff = (datetime.strptime(str(converted_out), DATETIME_FORMAT) - datetime.strptime(str(ireh_estoi_tsag), DATETIME_FORMAT)).total_seconds()/3600
                                                if real_diff > 5.0:
                                                    worked_hours2 = real_diff - 1
                                                else:
                                                    worked_hours2 = real_diff
                                                
                                            elif ywah_estoi_tsag >= converted_in >= ireh_estoi_tsag and converted_out > ywah_estoi_tsag and day.weekday() not in (5,6):
                                                real_diff = (datetime.strptime(str(ywah_estoi_tsag), DATETIME_FORMAT) - datetime.strptime(str(converted_in), DATETIME_FORMAT)).total_seconds()/3600
                                                if real_diff > 5.0:
                                                    worked_hours2 = real_diff - 1
                                                else:
                                                    worked_hours2 = real_diff
                                            
                                            if real_diff>0:
                                                hm_days += 1
                                                
                      
                                                
                                            elif converted_out < ireh_estoi_tsag and converted_out < ireh_estoi_tsag and day.weekday() not in (5,6):
                                                worked_hours2 = 0.0
                                            elif converted_in > ywah_estoi_tsag and converted_in > ywah_estoi_tsag and day.weekday() not in (5,6):
                                                worked_hours2 = 0.0
                                         
                                            worked_hours = round((worked_hours2), 2)
                                            worked_hours_total +=worked_hours
                                    
                                    
                                    else:
                                        
                                        converted_in = datetime.strptime(str(att_id_one.date_from), DATETIME_FORMAT) + timedelta(hours=8)
                                        converted_out = datetime.strptime(str(att_id_one.date_to), DATETIME_FORMAT).replace(second=0) + timedelta(hours=8)
                                        converted_diff = datetime.strptime(str(converted_out), DATETIME_FORMAT) - datetime.strptime(str(converted_in), DATETIME_FORMAT)
                                        worked_hours2= 0
                                        
                                        
                                        #Tasalsan tsag ololt
                                        ireh_estoi_tsag = 0.0
                                        ywah_estoi_tsag = 0.0
                                        
                                        
                                        cal_morning_hour_from = 0.0
                                        cal_morning_hour_to = 0.0
                                        cal_afternoon_hour_from = 0.0
                                        cal_afternoon_hour_to = 0.0
                                        cal_name1 = ''
                                        cal_name2 = ''
                                        for line_cal in line.resource_calendar_id.attendance_ids:
                                            if line_cal.dayofweek == str(day.weekday()) and line_cal.day_period == 'morning':
                                                cal_morning_hour_from = line_cal.hour_from
                                                cal_morning_hour_to = line_cal.hour_to
                                                cal_name1 = line_cal.name
                                            elif line_cal.dayofweek == str(day.weekday()) and line_cal.day_period == 'afternoon':
                                                cal_afternoon_hour_from = line_cal.hour_from
                                                cal_afternoon_hour_to = line_cal.hour_to
                                                cal_name2 = line_cal.name
                                             
                                        
                                        only_hour_morn = int(cal_morning_hour_from)
                                        minutes = cal_morning_hour_from*60
                                        hours, minutes = divmod(minutes, 60)
                                        only_minut_morn = int(minutes)
                                        #tsag bodohdoo ireh_estoi_tsag talbariig ashiglaj bga
                                        ireh_estoi_tsag = datetime.strptime(str(converted_in), DATETIME_FORMAT).replace(hour=only_hour_morn, minute=only_minut_morn,second=0)
                                        #print ('BBBBBB', ireh_estoi_tsag)
                                        #Serveriin tsag buyu Londongiin tsagaas shvvlt hiihdee ireh_estoi_tsag2 talbariig ashiglaj bga
                                        ireh_estoi_tsag2 = datetime.strptime(str(converted_in) , DATETIME_FORMAT).replace(hour=only_hour_morn, minute=only_minut_morn,second=0) - timedelta(hours=16)
                                        #print ('BBBBBB', ireh_estoi_tsag2)
                                        
                                        #converted_in2 = datetime.strptime(str(att_id_one_day.check_in), DATETIME_FORMAT).replace(second=0) + timedelta(hours=8) - timedelta(hours=8)
                                        
                                        converted_in2 = datetime.strptime(str(att_id_one.date_from), DATETIME_FORMAT).replace(hour=23, minute=59,second=59) - timedelta(hours=8)
                                        
                                        
                                        
                                        #ireh_estoi_tsag = datetime.strptime(str(att_id_one_day.check_in), DATETIME_FORMAT).replace(hour=only_hour_morn, minute=only_minut_morn)
                                        hotsorson_tsag_ogloo12 = 0.0
                                        only_hour_aft = int(cal_afternoon_hour_to)
                                        minutes2 = cal_afternoon_hour_to*60
                                        hours2, minutes2 = divmod(minutes2, 60)
                                        only_minut_aft = int(minutes2)
                                        ywah_estoi_tsag = datetime.strptime(str(converted_out), DATETIME_FORMAT).replace(hour=only_hour_aft,minute=only_minut_aft,second=0)
                                        
                                        converted_inn = datetime.strptime(str(ireh_estoi_tsag), DATETIME_FORMAT) + timedelta(hours=4)
                                        if att_id_one.number_of_hours_display == 0.0 and day.weekday() not in (5,6) and converted_in < converted_inn:
                                            hotsorson_tsag_ogloo12 = round((converted_in - ireh_estoi_tsag).seconds / 3600, 2)
                                            if hotsorson_tsag_ogloo12 > 8.0:
                                                hotsorson_tsag_ogloo1 = 8.0
                                            else:
                                                hotsorson_tsag_ogloo1 = hotsorson_tsag_ogloo12
                                            ireh_estoi_tsag3 = datetime.strptime(str(ireh_estoi_tsag), DATETIME_FORMAT) - timedelta(hours=8)
                                            holiday_this_day = holiday_obj.search([('employee_id','=',line.id),('state','in',('validate','validate1')),('date_from','>=',ireh_estoi_tsag2),('date_from','<=',ireh_estoi_tsag3)])
                                            
                                            for lin255 in holiday_this_day:
                                                this_day_holiday_hours = lin255.number_of_hours_display
                                                this_day_holiday_hours_total = this_day_holiday_hours_total + this_day_holiday_hours
                                            if this_day_holiday_hours_total >= hotsorson_tsag_ogloo1:
                                                hotsorson_tsag_ogloo = 0.0
                                            elif this_day_holiday_hours_total < hotsorson_tsag_ogloo1:
                                                if ywah_estoi_tsag > converted_in:
                                                    hotsorson_tsag_ogloo = hotsorson_tsag_ogloo1 - this_day_holiday_hours_total
                                            #if line.id==968:
                                            #print("ogloonii tsag hotsorson ",converted_in,hotsorson_tsag_ogloo_sariinh,hotsorson_tsag_ogloo,hotsorson_tsag_ogloo12,converted_out)
                                            hotsorson_tsag_ogloo_sariinh = hotsorson_tsag_ogloo_sariinh + hotsorson_tsag_ogloo
                                            
                        
                        else:
                            
                            self._cr.execute(""" SELECT  srl.employee_id as emp, sum(srl.time) as udur 
                                                FROM shift_working_register_line srl
                                                LEFT JOIN shift_working_register sw on (sw.id = srl.shift_manager_id)
                                                WHERE
                                                srl.employee_id = %s and sw.shift_date >= '%s' 
                                                and sw.shift_date <= '%s'  
                                                group by srl.employee_id
                                        """%(line.id,self.start_date,self.end_date,))
                            f_time_data  = self._cr.dictfetchall()
                            
                         
                            
                            if f_time_data !=None:
                                for ftime in f_time_data:
                                    f_time +=ftime['udur']
                                worked_hours_total = f_time
                                worked_days_total = worked_hours_total/8
                            for day in days:
                                if day.weekday() not in (5,6):
                                    hours_per_day2 += line.resource_calendar_id.hours_per_day
                                    days_per_month += 1
                            #Ajillah estoi honog oloh
                            be_worked_days2 = days_per_month 
                            #Ajillah estoi tsag oloh
                            be_worked_hours2 =  hours_per_day2
                    
                                    
        
                                #worked_hours_total = worked_hours_total + worked_hours
                                
                        
                        
                    #hotsorson_tsag_niit_sariinh = hotsorson_tsag_niit_sariinh + hotsorson_tsag_oroi_sariinh
                    #hotsorson_tsag_niit_sariinh_ogloo = hotsorson_tsag_niit_sariinh_ogloo + hotsorson_tsag_ogloo_sariinh
                    
                    #Owchnii choloo ololt - songoson ognoonii hoorondoh shvv
                    holiday_type_sick = type_obj.search([('new_type','=','sick')])
                    for line_sick in holiday_type_sick:
                        holiday_sick = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',holiday_type_sick.id),('state','in',('validate','validate1')),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for sick_holiday in holiday_sick:
                            if  float(sick_holiday.number_of_hours_display) > 8.0:
                                this_day_sick_hours = sick_holiday.number_of_hours_display-1
                            else:
                                this_day_sick_hours = sick_holiday.number_of_hours_display
                            this_day_sick_hours_total = this_day_sick_hours_total + this_day_sick_hours
                    this_month_sick_hours_total = this_month_sick_hours_total + this_day_sick_hours_total
                        
                    
                    #Iluu tsag
                    holiday_type_more = type_obj.search([('new_type','=','more_time')],limit=1)
                    holiday_type_more_time = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',holiday_type_more.id),('state','=','validate'),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                    for more in holiday_type_more_time:
                        #if sick_holiday.request_unit_half == False and sick_holiday.request_unit_hours == False:
                        this_day_more_hours = more.number_of_hours_display
                        this_month_overtime = this_month_overtime + this_day_more_hours
                        


                    # Shuniin Iluu tsag
                    holiday_type_more = type_obj.search([('new_type', '=', 'more_times2')], limit=1)
                    holiday_type_more_times2 = holiday_obj.search([('employee_id', '=', line.id),('holiday_status_id', '=', holiday_type_more.id),('state','=','validate'), ('date_from', '>=', server_time_start),('date_to', '<=', server_time_end)])
                    for more in holiday_type_more_times2:
                        # if sick_holiday.request_unit_half == False and sick_holiday.request_unit_hours == False:
                        this_day_more_hours = more.number_of_hours_display
                        this_month_moretimes2 = this_month_moretimes2 + this_day_more_hours
                        
                    #Eeljiin amralt ololt - songoson ognoonii hoorondoh shvv
                    holiday_type_legal = type_obj.search([('new_type','=','legal_leave')])
                    holiday_legal = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',holiday_type_legal.id),('state','in',('validate','validate1')),('date_from','>=',start_date_month),('date_to','<=',end_date_month)])
                    for legal_holiday in holiday_legal:
                        this_day_legal_hours = legal_holiday.number_of_days
                        this_month_legal_hours_total = this_month_legal_hours_total + this_day_legal_hours
                        
                    #Tsalintai choloo ololt - songoson ognoonii hoorondoh shvv
                    holiday_type_payroll = type_obj.search([('new_type','=','payroll')])
                    for line_payroll in holiday_type_payroll:
                        holiday_payroll = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',line_payroll.id),('state','in',('validate','validate1')),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for payroll_holiday in holiday_payroll:
                            if  float(payroll_holiday.number_of_hours_display) > 8.0 and float(payroll_holiday.number_of_hours_display) <=9.0:
                                this_day_payroll_hours = payroll_holiday.number_of_hours_display-1
                            else:
                                if payroll_holiday.number_of_hours_display < 8.0 and payroll_holiday.number_of_hours_display >= 4.0:
                                    this_day_payroll_hours = 4
                                else:
                                    this_day_payroll_hours = payroll_holiday.number_of_hours_display
                            this_month_payroll_hours_total = this_month_payroll_hours_total + this_day_payroll_hours
                            hm_days += payroll_holiday.number_of_days
                    this_month_all_payroll_hours_total = this_month_all_payroll_hours_total + this_month_payroll_hours_total
                    
                    #Tsalin - gvi choloo ololt - songoson ognoonii hoorondoh shvv
                    holiday_type_unpayroll = type_obj.search([('new_type','=','unpayroll')])
                    for line_unpayroll in holiday_type_unpayroll:
                        holiday_unpayroll = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',line_unpayroll.id),('state','in',('validate','validate1')),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for unpayroll_holiday in holiday_unpayroll:
                            if  float(unpayroll_holiday.number_of_hours_display) > 8.0:
                                this_day_unpayroll_hours = unpayroll_holiday.number_of_hours_display-1
                            else:
                                if unpayroll_holiday.number_of_hours_display < 8.0 and unpayroll_holiday.number_of_hours_display >= 4.0:
                                    this_day_unpayroll_hours = 4
                                else:
                                    this_day_unpayroll_hours = unpayroll_holiday.number_of_hours_display
                            this_month_unpayroll_hours_total = this_month_unpayroll_hours_total + this_day_unpayroll_hours
                    this_month_all_unpayroll_hours_total = this_month_all_unpayroll_hours_total + this_month_unpayroll_hours_total
                    
                    #Gaduur ajil - songoson ognoonii hoorondoh shvv
                    holiday_type_outwork = type_obj.search([('new_type','=','out_work')])
                    for line_outwork in holiday_type_outwork:
                        holiday_outwork = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',line_outwork.id),('state','in',('validate','validate1')),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for outwork_holiday in holiday_outwork:
                            if  float(outwork_holiday.number_of_hours_display) > 8.0 and float(outwork_holiday.number_of_hours_display) <=9.0:
                                this_day_outwork_hours = outwork_holiday.number_of_hours_display-1
                            else:
                                if outwork_holiday.number_of_hours_display < 8.0 and outwork_holiday.number_of_hours_display >= 4.0:
                                    this_day_outwork_hours = 4
                                else:
                                    this_day_outwork_hours = outwork_holiday.number_of_hours_display
                            
                            this_month_outwork_hours_total = this_month_outwork_hours_total + this_day_outwork_hours
                            hm_days += outwork_holiday.number_of_days
                    this_month_all_outwork_hours_total = this_month_all_outwork_hours_total + this_month_outwork_hours_total
                    
                    #nohon amralt - songoson ognoonii hoorondoh shvv
                    holiday_type_patch_day = type_obj.search([('new_type','=','patch_day')])
                    for line_patch_day in holiday_type_patch_day:
                        holiday_patch_day = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',line_patch_day.id),('state','in',('validate','validate1')),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for patch_day_holiday in holiday_patch_day:
                            if  float(patch_day_holiday.number_of_hours_display) > 8.0:
                                this_day_patch_day_hours = patch_day_holiday.number_of_hours_display-1
                            else:
                                if patch_day_holiday.number_of_hours_display < 8.0 and patch_day_holiday.number_of_hours_display >= 4.0:
                                    this_day_patch_day_hours = 4
                                else:
                                    this_day_patch_day_hours = patch_day_holiday.number_of_hours_display
                            this_month_patch_day_hours_total = this_month_patch_day_hours_total + this_day_patch_day_hours
                    this_month_all_patch_day_hours_total = this_month_all_patch_day_hours_total + this_month_patch_day_hours_total
                    
                    #sul_zogsolt - songoson ognoonii hoorondoh shvv
                    holiday_type_sul_zogsolt = type_obj.search([('new_type','=','sul_zogsolt')])
                    for line_sul_zogsolt in holiday_type_sul_zogsolt:
                        holiday_sul_zogsolt = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',line_sul_zogsolt.id),('state','in',('validate','validate1')),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for sul_zogsolt_holiday in holiday_sul_zogsolt:
                            if  float(sul_zogsolt_holiday.number_of_hours_display) > 8.0:
                                this_day_sul_zogsolt_hours = sul_zogsolt_holiday.number_of_hours_display-1
                            else:
                                this_day_sul_zogsolt_hours = sul_zogsolt_holiday.number_of_hours_display
                            this_month_sul_zogsolt_hours_total = this_month_sul_zogsolt_hours_total + this_day_sul_zogsolt_hours
                    this_month_all_sul_zogsolt_hours_total = this_month_all_sul_zogsolt_hours_total + this_month_sul_zogsolt_hours_total
                    
                    #online - songoson ognoonii hoorondoh shvv
                    holiday_type_online = type_obj.search([('new_type','=','online')])
                    for line_online in holiday_type_online:
                        holiday_online = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',line_online.id),('state','in',('validate','validate1')),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for online_holiday in holiday_online:
                            if  float(online_holiday.number_of_hours_display) > 8.0 and float(online_holiday.number_of_hours_display) <=9.0:
                                this_day_online_hours = online_holiday.number_of_hours_display-1
                            else:
                                if online_holiday.number_of_hours_display < 8.0 and online_holiday.number_of_hours_display >= 4.0:
                                    this_day_online_hours = 4
                                else:
                                    this_day_online_hours = online_holiday.number_of_hours_display
                            this_month_online_hours_total = this_month_online_hours_total + this_day_online_hours
                            hm_days += online_holiday.number_of_days
                    this_month_all_online_hours_total = this_month_all_online_hours_total + this_month_online_hours_total
                    
                    #Tomilolt - songoson ognoonii hoorondoh shvv
                    holiday_type_assignment = type_obj.search([('new_type','=','assignment')])
                    for line_assignment in holiday_type_assignment:
                        holiday_assignment = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',line_assignment.id),('state','in',('validate','validate1')),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for assignment_holiday in holiday_assignment:
                            if  float(assignment_holiday.number_of_hours_display) > 8.0 and float(assignment_holiday.number_of_hours_display) <=9.0:
                                this_day_assignment_hours = assignment_holiday.number_of_hours_display-1
                            else:
                                if assignment_holiday.number_of_hours_display < 8.0 and assignment_holiday.number_of_hours_display >= 4.0:
                                    this_day_assignment_hours = 4
                                else:
                                    this_day_assignment_hours = assignment_holiday.number_of_hours_display
                            this_month_assignment_hours_total = this_month_assignment_hours_total + this_day_assignment_hours
                            hm_days += assignment_holiday.number_of_days
                    this_month_all_assignment_hours_total = this_month_all_assignment_hours_total + this_month_assignment_hours_total
                    
                    #Training - songoson ognoonii hoorondoh shvv
                    holiday_type_training = type_obj.search([('new_type','=','training')])
                    for line_training in holiday_type_training:
                        holiday_training = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',line_training.id),('state','in',('validate','validate1')),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for training_holiday in holiday_training:
                            if  float(training_holiday.number_of_hours_display) > 8.0 and float(training_holiday.number_of_hours_display) <=9.0:
                                this_day_training_hours = training_holiday.number_of_hours_display-1
                            else:
                                if training_holiday.number_of_hours_display < 8.0 and training_holiday.number_of_hours_display >= 4.0:
                                    this_day_training_hours = 4
                                else:
                                    this_day_training_hours = training_holiday.number_of_hours_display
                            this_month_training_hours_total = this_month_training_hours_total + this_day_training_hours
                            hm_days += training_holiday.number_of_days
                    this_month_all_training_hours_total = this_month_all_training_hours_total + this_month_training_hours_total
                    
                    #family_day - songoson ognoonii hoorondoh shvv
                    holiday_type_family_day = type_obj.search([('new_type','=','family_day')])
                    for line_family_day in holiday_type_family_day:
                        holiday_family_day = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',line_family_day.id),('state','in',('validate','validate1')),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for family_day_holiday in holiday_family_day:
                            if  float(family_day_holiday.number_of_hours_display) > 8.0:
                                this_day_family_day_hours = family_day_holiday.number_of_hours_display-1
                            else:
                                if family_day_holiday.number_of_hours_display < 8.0 and family_day_holiday.number_of_hours_display >= 4.0:
                                    this_day_family_day_hours = 4
                                else:
                                    this_day_family_day_hours = family_day_holiday.number_of_hours_display
                            this_month_family_day_hours_total = this_month_family_day_hours_total + this_day_family_day_hours
                            hm_days += family_day_holiday.number_of_days
                    this_month_all_family_day_hours_total = this_month_all_family_day_hours_total + this_month_family_day_hours_total
                    
                    #No_finger - songoson ognoonii hoorondoh shvv
                    holiday_type_no_finger = type_obj.search([('new_type','=','no_finger')])
                    for line_no_finger in holiday_type_no_finger:
                        holiday_no_finger = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',line_no_finger.id),('state','in',('validate','validate1')),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for no_finger_holiday in holiday_no_finger:
                            if  float(no_finger_holiday.number_of_hours_display) > 8.0 and float(no_finger_holiday.number_of_hours_display) <=9.0:
                                this_day_no_finger_hours = no_finger_holiday.number_of_hours_display-1
                            else:
                                if no_finger_holiday.number_of_hours_display < 8.0 and no_finger_holiday.number_of_hours_display >= 4.0:
                                    this_day_no_finger_hours = 4
                                else:
                                    this_day_no_finger_hours = no_finger_holiday.number_of_hours_display
                            this_month_no_finger_hours_total = this_month_no_finger_hours_total + this_day_no_finger_hours
                            hm_days += no_finger_holiday.number_of_days
                    this_month_all_no_finger_hours_total = this_month_all_no_finger_hours_total + this_month_no_finger_hours_total
                    
                    #Ilvv tsag - songoson ognoonii hoorondoh shvv
                    #overtime_of_emp = overtime_obj.search([('employee_id','=',line.id),('state','=','approved'),('date_start','>=',server_time_start),('date_end','<=',server_time_end)])
                    #for line_overtime in overtime_of_emp:
                    #    this_day_overtime = line_overtime.hours
                    #    this_month_overtime = this_month_overtime + this_day_overtime
                    
                    
                    #Public holiday - songoson ognoonii hoorondoh shvv
                    holiday_type_public = type_obj.search([('new_type','=','public_holiday')])
                    for line_public in holiday_type_public:
                        holiday_public = holiday_obj.search([('employee_id','=',line.id),('holiday_status_id','=',line_public.id),('state','=','validate'),('date_from','>=',server_time_start),('date_to','<=',server_time_end)])
                        for public_holiday in holiday_public:
                            if  float(public_holiday.number_of_hours_display) > 8.0 and float(public_holiday.number_of_hours_display) <=9.0:
                                this_day_public = public_holiday.number_of_hours_display-1
                            else:
                                this_day_public = public_holiday.number_of_hours_display
                            this_month_public_total = this_month_public_total + this_day_public
                    this_month_all_public_total = this_month_all_public_total + this_month_public_total
                    
                    if self.holiday_day > 0.0:
                        com_day = be_worked_days2 - self.holiday_day
                        com_time = be_worked_hours2 - ( self.holiday_day*8)
                    else:
                        com_day = be_worked_days2
                        com_time = be_worked_hours2
                     
                    
                    
                    total_time = worked_hours_total + this_month_overtime + this_month_moretimes2 + this_month_all_public_total + this_month_all_outwork_hours_total + this_month_all_no_finger_hours_total + this_month_all_family_day_hours_total + this_month_all_training_hours_total + this_month_all_assignment_hours_total + this_month_all_online_hours_total + this_month_all_payroll_hours_total
                    tasalsan =  com_time - total_time
                    
                    hour_record_line_id = self.env['hr.hour.record.line'].search([('parentd_id','=',self.id),('employee_id','=',line.id)])
                    #if total_time > com_time:
                        #this_month_overtime = total_time - com_time
                    if hour_record_line_id.id==False:
                        if com_time < total_time:
                           total_time = com_time
                           tasalsan = 0
                           
                           
                           
                           
                    
                        if line.resource_calendar_id.is_shift == True:
                            worked_days_total = total_time/12
                            if worked_hours_total > com_time:
                                this_month_overtime = worked_hours_total - com_time
                        else:
                             worked_days_total = total_time/8
                       
                        
                        if line.contract_id.shuud_udirdlaga == True:
                             worked_days_total = com_day  
                             worked_hours_total = com_time
                             total_time = com_time
                             tasalsan = 0
                                    
                        emp_info.create({
                                      'employee_id': line.id,
                                      'parentd_id': self.id,
                                      'position': line.job_id.id,
                                      'due_day': com_day,
                                      'due_time': com_time,
                                      'work_day':worked_days_total,
                                      'work_time':worked_hours_total,
                                      'hm_day':hm_days,
                                      'night_time':f_ntime,
                                      'sick_time':this_month_sick_hours_total,
                                      'shift_holiday':this_month_legal_hours_total,
                                      'miss_time_morning':hotsorson_tsag_ogloo_sariinh,
                                      #'miss_time':hotsorson_tsag_niit_sariinh,
                                      'free_time1':this_month_all_payroll_hours_total,
                                      'free_time2':this_month_all_unpayroll_hours_total,
                                      'more_time':this_month_overtime,
                                      'more_times2': this_month_moretimes2,
                                      'more_time1':this_month_all_public_total,
                                      'no_finger':this_month_all_no_finger_hours_total,
                                      'assignment':this_month_all_assignment_hours_total,
                                      'out_work':this_month_all_outwork_hours_total,
                                      'patch_day':this_month_all_patch_day_hours_total,
                                      'sul_zogsolt':this_month_all_sul_zogsolt_hours_total,
                                      'online':this_month_all_online_hours_total,
                                      'training':this_month_all_training_hours_total,
                                      'family_day':this_month_all_family_day_hours_total,
                                      'tasalsan':tasalsan,
                                      'total_time':total_time,
                                      })
                    else:
                        if com_time < total_time:
                           total_time = com_time
                           tasalsan = 0
                        if line.resource_calendar_id.is_shift == True:
                           worked_days_total = total_time/12
                           if worked_hours_total > com_time:
                                   this_month_overtime = worked_hours_total - com_time
                        else:
                            worked_days_total = total_time/8
                        #worked_days_total%1   таслалаас хойших орон авах
                        
                        if line.contract_id.shuud_udirdlaga == True:
                                worked_days_total = com_day  
                                worked_hours_total = com_time
                                total_time = com_time
                                tasalsan = 0
                                
                        hour_record_line_id.write({
                                      'employee_id': line.id,
                                      'parentd_id': self.id,
                                      'position': line.job_id.id,
                                      'due_day': com_day,
                                      'due_time': com_time,
                                      'work_day':round(worked_days_total,0),
                                      'work_time':worked_hours_total,
                                      'hm_day':hm_days,
                                      'night_time':f_ntime,
                                      'sick_time':this_month_sick_hours_total,
                                      'shift_holiday':this_month_legal_hours_total,
                                      'miss_time_morning':hotsorson_tsag_ogloo_sariinh,
                                      #'miss_time':hotsorson_tsag_niit_sariinh,
                                      'free_time1':this_month_all_payroll_hours_total,
                                      'free_time2':this_month_all_unpayroll_hours_total,
                                      'more_time':this_month_overtime,
                                      'more_times2': this_month_moretimes2,
                                      'more_time1':this_month_all_public_total,
                                      'no_finger':this_month_all_no_finger_hours_total,
                                      'assignment':this_month_all_assignment_hours_total,
                                      'out_work':this_month_all_outwork_hours_total,
                                      'patch_day':this_month_all_patch_day_hours_total,
                                      'sul_zogsolt':this_month_all_sul_zogsolt_hours_total,
                                      'online':this_month_all_online_hours_total,
                                      'training':this_month_all_training_hours_total,
                                      'family_day':this_month_all_family_day_hours_total,
                                      'tasalsan':tasalsan,
                                      'total_time':total_time,
                                      })
                
        return True
    
    def delete_lines(self):
        self_obj = self.env['hr.hour.record']
        line_pool = self.env['hr.hour.record.line']
        for invoice in self_obj.browse(self.id):
            line_ids = [line.id for line in invoice.line_id]
            line_pool.unlink()
        return True
    
    def workdaysub(uid,start_date, end_date):
        '''
        Calculate the number of working days between two dates inclusive
        (start_date <= end_date).
    
        The actual working days can be set with the optional whichdays
        parameter
        (default is MON-FRI)
        '''
        (MON, TUE, WED, THU, FRI, SAT, SUN) = range(7)
        whichdays=(MON,TUE,WED,THU,FRI)
        delta_days = (end_date - start_date).days + 1
        full_weeks, extra_days = divmod(delta_days, 7)
        num_workdays = (full_weeks + 1) * len(whichdays)
        for d in range(1, 8 - extra_days):
                    if (end_date + timedelta(d)).weekday() in whichdays:
                                    num_workdays -= 1
        return num_workdays
    
    def action_cancel(self):
        return {
            self.write( {'state' : 'draft',})
        }
        
    def action_confirm(self):
         hr_obj = self.env['hr.hour.record']
         seq = self.env['ir.sequence'].next_by_code('hr.hour.record')
         self.write({
                      'name':seq,
                      'state' : 'confirmed',
                  })
         return True

    def action_print(self):
        datas = {
            'ids': self._context.get('active_ids', [])
        }
        datas['model'] = 'hr.hour.record'
        datas['form'] = self
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hr.hour.record.report',
            'datas': datas,
            'nodestroy': True
        }
        
    def action_compute(self):
        obj = self.browse(self.id)
        line_obj = self.env['hr.hour.record.line']
        total_time = 0.0
        tasalsan = 0.0
        start_date_month = datetime.strptime(str(self.start_date),DATE_FORMAT)
        end_date_month = datetime.strptime(str(self.end_date),DATE_FORMAT)
        wday = self.workdaysub(start_date_month,end_date_month)
        for line in obj.line_id:
            total_time = line.work_time+line.night_time+line.free_time1
            worked_hours_total = 0.0
            if line.parentd_id.holiday_day >0.0:
                due_days = wday-line.parentd_id.holiday_day
                due_time = due_days*8
            else:
                due_days = wday
                due_time = due_days*8
            if line.work_time == 0.0:
                worked_hours_total = line.work_day*8
            if worked_hours_total > 0.0:
                line.write({
                             'total_time':total_time,
                             'due_day':due_days,
                             'due_time':due_time,
                             'work_time':worked_hours_total,
                             })
            else:
                line.write({
                             'total_time':total_time,
                             'due_day':due_days,
                             'due_time':due_time,
                             })
        self.write({'is_compute':True})


class HrHourHecordLine(models.Model):
    '''
    Ажилтаны цагийн бүртгэлийн мөр
    '''
    _name = "hr.hour.record.line"
    _order = "position"
    _description = 'Information on a employee'
    
    employee_id = fields.Many2one('hr.employee', 'Employee name')
    position = fields.Many2one('hr.job','Position')
    due_day = fields.Integer('Due day')
    due_time = fields.Integer('Due time')
    work_day = fields.Integer('Work day')
    work_time = fields.Float('Work time')
    hm_day = fields.Integer('ХМ өдөр')
    sick_time = fields.Float('Sick time')
    shift_holiday = fields.Float('Shift holiday')
    miss_time_morning = fields.Float('Хоцорсон цаг /өглөө/')
    #miss_time = fields.Float('Miss time')
    night_time = fields.Float('Шөнийн цаг')
    free_time1 = fields.Float('Free time1')
    free_time2 = fields.Float('Free time2')
    more_time = fields.Float('More time')
    no_finger = fields.Float(string='No finger')
    more_time1 = fields.Float('Legal holiday')
    assignment = fields.Float('Томилолт')
    out_work = fields.Float('Гадуур ажилласан цаг')
    training = fields.Float('Сургалт')
    family_day = fields.Float('Гэр бүлийн өдөр')
    more_times2 = fields.Float('Шөнийн илүү цаг')
    patch_day = fields.Float('Нөхөж амралт')
    sul_zogsolt = fields.Float('Сул зогсолт')
    online = fields.Float('Гэрээс ажилласан')
    tasalsan = fields.Float('Тасалсан')
    #late_minut = fields.Float('Late minut')
    total_time = fields.Float('Total compute time',compute='_compute_amount_total', store=True)
    #fine_size = fields.Integer('Attendance  bonus')
    #execution_percent = fields.Integer('Execution percent')
    parentd_id = fields.Many2one('hr.hour.record')
        
        
    def onchange_change_time(self, day):
        obj = self.browse(self.id)
        due_time = 0.0
        miss_time = 0.0
        over_time = 0.0
        diff_time = 0.0
        if day:
            due_time = day*8
            diff_time = obj.work_time-due_time
            if diff_time > 0.0:
                over_time = diff_time
            else:
                diff_time = -miss_time
        res = {'value':{
                        'due_time':due_time,
                        'miss_time':miss_time,
                        'more_time':over_time,
                            }}
        return res
     
    def onchange_work_time(self, work):
        obj = self.browse(self.id)
        miss_time = 0.0
        over_time = 0.0
        diff_time = 0.0
        if work:
            diff_time = work-obj.due_time
            if diff_time > 0.0:
                over_time = diff_time
            else:
                miss_time = -diff_time
        res = {'value':{
                        'work_time':work,
                        'miss_time':miss_time,
                        'more_time':over_time,
                            }}
        return res
   


    
    