# -*- coding: utf-8 -*-
##############################################################################
#
#    ShineERP, Enterprise Management Solution    
#    Copyright (C) 2007-2014 ShineERP Co.,ltd (<http://www.serp.mn>). All Rights Reserved
#
#    Address : Suhkbaatar District, National ITPark, ShineERP LLC
#    Email : info@serp.mn
#    Phone : 976 + 88001963
#
##############################################################################


import time
from collections import OrderedDict
from odoo import api, fields, models, _
from odoo.osv import expression
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from odoo.tools.misc import formatLang, format_date
from odoo.tools import float_is_zero, float_compare
from odoo.tools.safe_eval import safe_eval
from odoo.addons import decimal_precision as dp
from lxml import etree


class product_template(models.Model):
    _inherit = 'product.template'
    

    vat_categ_id = fields.Many2one('vat.category',string=u'Татварын ангилал')
    barcode = fields.Char('Barcode', oldname='ean13', related='product_variant_ids.barcode')
    default_code = fields.Char(
        'Internal Reference', compute='_compute_default_code',
        inverse='_set_default_code', store=True)
   
    
    
class product_product(models.Model):
    _inherit = 'product.product'
    
   

class product_category(models.Model):
    _inherit = 'product.category'
    
    _order = 'code'
    

    
    code = fields.Char(string='Category code',size=7)
    

    
    _sql_constraints = [('code_uniq', 'unique(code)', 'Code must be unique!')]
    
    @api.model
    def create(self, vals):
        if vals.get('code'):
            code_id = self.search([('code','=',vals.get('code'))])
            if code_id:
                raise UserError(_(u'Анхааруулга!'), _(u'%s таны оруулсан кодтай ангилал өмнө нь үүссэн байна!.') % (vals.get('code')))
                #raise UserError(_('The account %s (%s) is deprecated !') %(account.name, account.code))
        return super(product_category, self).create(vals)
    

class vat_category(models.Model):
    _name = 'vat.category'

    _order = 'code'
    
    name = fields.Char(string='Category name')
    code = fields.Char(string='Category code',size=7)
    parent_id = fields.Many2one('vat.category',string='Parent')
    company_id = fields.Many2one('res.company',string='Company')


    _sql_constraints = [
        ('code_uniq', 'unique(code)', 'Code must be unique!'),
    ]
    