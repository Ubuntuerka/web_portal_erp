# -*- coding: utf-8 -*-
##############################################################################
#
#    Tengersoft, Enterprise Management Solution    
#    Copyright (C) 2014-2020 Tengersoft Co.,ltd (<http://www.tengersoft.mn>). All Rights Reserved
#
#    Address : Bayanzurkh District, Borkh Center, 312, Tengersoft LLC
#    Email : tengersoft@gmail.com
#    Phone : 976 + 99345677
#
##############################################################################

{
    "name" : "Product category GSI standard",
    "version" : "1.0",
    "author" : "Tengersoft Development Team",
    "category" : "",
    "description": """
    """,
    "depends" : [
                'product',
                'point_of_sale',
                'stock'
                ],
    "demo_xml" : [],
    "update_xml" : [
                    'security/ir.model.access.csv',
                    'models/product_category_view.xml',
                    'data/product_category_data.xml',
                    'data/product_category_data_two.xml',
                    'data/product_category_data_three.xml',
                    'data/product_category_data_four.xml',
                    'data/product_category_data_five.xml',
                    ],                    
    "auto_install": False,
    "application": True,
    "installable": True,
    "icon": '/l10n_mn_product_category/static/description/icon.png',
}